trigger ContactUpdateAffId on Contact (before update) {
    for(Contact ct : trigger.new){
        if(trigger.oldmap.get(ct.Id).PAP_refid__c != null && trigger.oldmap.get(ct.Id).PAP_refid__c != '' && ct.PAP_refid__c != null && ct.PAP_refid__c != ''){
            if(trigger.oldmap.get(ct.Id).PAP_refid__c != ct.PAP_refid__c){
                ct.PAP_CheckRefId__c = true;
            }
        }
    }
}