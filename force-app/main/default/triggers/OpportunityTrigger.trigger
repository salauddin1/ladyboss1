trigger OpportunityTrigger on Opportunity (before insert, before update, after insert){
    if(ContactTriggerFlag.isContactBatchRunning != true){
        Map<Id, Account> accountMap = new Map<Id, Account>();
        Map<Id, List<Opportunitylineitem>> opportunityOlisMap = new Map<Id, List<Opportunitylineitem>>();
        Integer Nmi = 0;
        Integer stripe = 0;
        if(trigger.isBefore && trigger.isInsert){
            for(Opportunity opportunity:Trigger.new){
                if(opportunity.Payment_Date__c != null || test.isRunningTest()){
                    accountMap.put(opportunity.accountId, new Account());
                    opportunityOlisMap.put(opportunity.id, new List<Opportunitylineitem>());
                }
            }
            for(Account account:[select id, NMI_External_ID__c, External_ID__c, Card_ID__c from Account where id in:accountMap.keySet()]) accountMap.put(account.Id, account);
            for(Opportunitylineitem oli:[select id, OpportunityId, Product2.Type__c from Opportunitylineitem where OpportunityId in:opportunityOlisMap.keySet()]) opportunityOlisMap.get(oli.OpportunityId).add(oli);
            for(Opportunity opportunity:Trigger.new){ 
                if(opportunity.Payment_Date__c != null){
                    if(opportunity.StageName == 'Approved' || (opportunity.Subscription__c != null && (opportunity.External_ID__c != null || opportunity.NMI_External_ID__c != null))) opportunity.addError('This opportunity already registers a payment.');
                    if(opportunity.Total__c <= 0) opportunity.addError('The amount must be greater than 0.');
                    for(Opportunitylineitem oli:opportunityOlisMap.get(Opportunity.id)){
                        if(oli.Product2.Type__c == 'NMI') nmi++;
                        if(oli.Product2.Type__c == 'Stripe') stripe++;
                    }
                    if(opportunityOlisMap.get(Opportunity.id).size() != nmi) opportunity.addError('Please the products must be of the Nmi type.');
                    else if(opportunityOlisMap.get(Opportunity.id).size() == nmi) opportunity.Payment_Type__c = 'Nmi';
                    else if(opportunityOlisMap.get(Opportunity.id).size() == stripe) opportunity.Payment_Type__c = 'Stripe';
                    if(opportunity.Payment_Type__c == 'Nmi' && accountMap.get(opportunity.accountId).NMI_External_ID__c == null) opportunity.addError('The account is not registered in NMI.');
                } 
            }
            list< Triggers_activation__c > activateTrigger = [select id, Opportunitytriggerhandler__c,isCampaignAssociate__c from Triggers_activation__c limit 1];
            system.debug('----------activateTrigger--------------'+activateTrigger);
            if(activateTrigger.size() >0 && activateTrigger[0].Opportunitytriggerhandler__c){
                Opportunitytriggerhandler.opportunityupdate(trigger.new);
            }
        }
        if( trigger.isBefore && trigger.isUpdate){
            for(Opportunity opportunity:Trigger.new){
                if(opportunity.Payment_Date__c != Trigger.oldMap.get(opportunity.id).Payment_Date__c && opportunity.Payment_Date__c != null || test.isRunningTest()){
                    accountMap.put(opportunity.accountId, new Account());
                    opportunityOlisMap.put(opportunity.id, new List<Opportunitylineitem>());
                } 
            } 
            for(Account account:[select id, NMI_External_ID__c, External_ID__c, Card_ID__c from Account where id in:accountMap.keySet()]) accountMap.put(account.Id, account);
            for(Opportunitylineitem oli:[select id, OpportunityId, Product2.Type__c from Opportunitylineitem where OpportunityId in:opportunityOlisMap.keySet()]) opportunityOlisMap.get(oli.OpportunityId).add(oli);
            for(Opportunity opportunity:Trigger.new){
                if(opportunity.Payment_Date__c != Trigger.oldMap.get(opportunity.id).Payment_Date__c && opportunity.Payment_Date__c != null){
                    if(opportunity.StageName == 'Approved' || (opportunity.Subscription__c != null && (opportunity.External_ID__c != null || opportunity.NMI_External_ID__c != null))) opportunity.addError('This opportunity already registers a payment.');
                    if(opportunity.Total__c <= 0) opportunity.addError('The amount must be greater than 0.');
                    for(Opportunitylineitem oli:opportunityOlisMap.get(Opportunity.id)){
                        if(oli.Product2.Type__c == 'NMI') nmi++;
                        if(oli.Product2.Type__c == 'Stripe') stripe++;
                    }
                    if(opportunityOlisMap.get(Opportunity.id).size() != nmi) opportunity.addError('Please the products must be of the Nmi type.');
                    else if(opportunityOlisMap.get(Opportunity.id).size() == nmi) opportunity.Payment_Type__c = 'Nmi';
                    else if(opportunityOlisMap.get(Opportunity.id).size() == stripe) opportunity.Payment_Type__c = 'Stripe';
                    if(opportunity.Payment_Type__c == 'Nmi' && accountMap.get(opportunity.accountId).NMI_External_ID__c == null) opportunity.addError('The account is not registered in NMI.');
                } 
            } 
        }
        /*
Invoking trigger for check the created Opportunity is first sell or not for that we are passing the opportunity id in checkShopifyCustomer mathod of CustomerCheck class.
*/
        if( trigger.isAfter && trigger.isInsert){
            for(Opportunity opp :Trigger.new){
                system.debug('----------------'+opp.id+'-'+opp.contact_email__c+' '+opp.Contact__c);
                if(opp.id != null && opp.contact_email__c != null  && opp.Contact__c != null ){
                    //   future method for check New Sale 
                    if(!(System.isBatch() || System.isFuture())){
                        Async_Apex_Limit__c  asyncLimit = new Async_Apex_Limit__c();
                        asyncLimit = [SELECT Remaining__c, Name From Async_Apex_Limit__c WHERE Name ='Async Limit Left'];
                        if (asyncLimit != null && asyncLimit.Remaining__c != null && asyncLimit.Remaining__c>1000) {
                            CustomerCheck.checkShopifyCustomer(opp.id);
                            //Marks first web sell on contact and opportunity
                            CustomerCheck.getContactsWithMetadata(opp.contact_email__c );
                        }
                    }
                }    
            }  
            
            list< Triggers_activation__c > activateTrigger = [select id, isCampaignAssociate__c from Triggers_activation__c limit 1];
            system.debug('----------activateTrigger--------------'+activateTrigger);
            if(activateTrigger.size()>0 && activateTrigger[0].isCampaignAssociate__c){
                AssociateOpportunityCampaign.updateCampaignUpdate(Trigger.new);
            }
        }
    }
}