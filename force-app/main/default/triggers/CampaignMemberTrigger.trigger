trigger CampaignMemberTrigger on CampaignMember (after insert) {
    CoachingAppliedBookShowCampaign.UpdateFlagmanullyAddtoCampaign(Trigger.new);
    if(ContactTriggerFlag.isContactBatchRunning != true  ){
      List<Campaign_Auto_SMS__mdt> lstCampignConfigs = [SELECT Campaign_Name__c, EmailTemplate_Name__c, EndingHours__c, StartingHours__c,From_Number__c FROM Campaign_Auto_SMS__mdt];
      List<String> lstCampignNames = new List<String>();
      Map<Id,String> mapEmailTemplateNames = new Map<Id,String>();
      Map<String,String> mapEmailNameTemplateNames = new Map<String,String>();  
      Map<String,Map<String,Integer>> startendHourMap = new Map<String,Map<String,Integer>>(); 
      Map<Id,Map<String,Integer>> startendHourwithcmpMap = new Map<Id,Map<String,Integer>>();
      Map<String,String> mapFromNumber = new Map<String,String>();
      Map<Id,String> mapIdFromNumber = new Map<Id,String>();
      for(Campaign_Auto_SMS__mdt cas : lstCampignConfigs)  {
        Map<String,Integer> valueofstartendMap= new  Map<String,Integer>();
        valueofstartendMap.put('start', Integer.valueOf(cas.StartingHours__c));
        valueofstartendMap.put('end', Integer.valueOf(cas.EndingHours__c));
        lstCampignNames.add(cas.Campaign_Name__c);
        mapEmailNameTemplateNames.put(cas.Campaign_Name__c.toLowerCase(),cas.EmailTemplate_Name__c.toLowerCase());
        startendHourMap.put(cas.Campaign_Name__c.toLowerCase(),valueofstartendMap);
        mapFromNumber.put(cas.Campaign_Name__c.toLowerCase(), cas.From_Number__c);
      }
      List<CampaignMember> lstCampignMember = Trigger.New;
      List<Campaign> lstCmp = [select id,Name from Campaign where name in :lstCampignNames];
      system.debug('--camp--'+lstCmp);
      List<String> lstEmailTemplateNames = new List<String>();
      for(Campaign cmp : lstCmp)  {
        if(mapEmailNameTemplateNames.get(cmp.name.toLowerCase()) != null)  {
          mapEmailTemplateNames.put(cmp.Id , mapEmailNameTemplateNames.get(cmp.name.toLowerCase()));
          lstEmailTemplateNames.add(mapEmailNameTemplateNames.get(cmp.name.toLowerCase()));
          startendHourwithcmpMap.put(cmp.Id , startendHourMap.get(cmp.name.toLowerCase()));
          mapIdFromNumber.put(cmp.Id, mapFromNumber.get(cmp.name.toLowerCase()));
        }
      }
      
      List<EmailTemplate> lstEmailTemps = [Select id,body,DeveloperName from EmailTemplate where DeveloperName in :lstEmailTemplateNames];
      Map<String,Id> mapEmails = new Map<String,Id>();
      for(EmailTemplate et : lstEmailTemps)  {
        mapEmails.put(et.DeveloperName.toLowerCase(),et.Id);
      }
      if(lstCmp.size() > 0)  {
        Savepoint sp = Database.setSavepoint();
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<Contact> dummyupdate = new List<Contact>();
        
        Map<CampaignMember,Messaging.SingleEmailMessage> cmMessageMap = new Map<CampaignMember,Messaging.SingleEmailMessage>();
        for(CampaignMember cmp : lstCampignMember)  {
          if(mapEmailTemplateNames.get(cmp.CampaignID) != null && cmp.ContactId != null)  {
            System.debug('--here check 1---');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            System.debug(mapEmails.get(mapEmailTemplateNames.get(cmp.CampaignID)));
            mail.setTemplateID(mapEmails.get(mapEmailTemplateNames.get(cmp.CampaignID))); 
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(cmp.contactId);
            mail.setWhatId(cmp.Id);
            emails.add(mail);   
            if(!Test.isRunningTest()){
              Contact ct = new Contact(Id=cmp.ContactId);
              ct.Email = 'dummy@dummy.com';
              dummyupdate.add(ct);
            }
            cmMessageMap.put(cmp,mail);
            
            
            
          }
        }
        update dummyupdate;
        Messaging.sendEmail(emails,false);
        Database.rollback(sp);
        List<ScheduleSMS__c> sSMSList = new List<ScheduleSMS__c>();
        List<CampaignMemberTriggerHandler.campaignMessageWrapper> cmpWrapper = new List<CampaignMemberTriggerHandler.campaignMessageWrapper>();
        for(CampaignMember cmp : cmMessageMap.keyset())  {
          System.debug('--here check 2---');
          Messaging.SingleEmailMessage mail = cmMessageMap.get(cmp);
          String messageData = mail.getPlainTextBody();
          System.debug('--here check 3 messageData---'+messageData);
          Integer t1= startendHourwithcmpMap.get(cmp.CampaignID).get('start');
          Integer t2= startendHourwithcmpMap.get(cmp.CampaignID).get('end');
          Time timeTenAM = Time.newInstance(t1,  0, 0, 0 );
          Time timeEightPM = Time.newInstance(t2, 0, 0, 0 );
          Datetime timenow = datetime.now();
          Time currentTime = Time.newInstance(timenow.hour(), timenow.minute(), timenow.second(), 0);
          if(currentTime>timeTenAM && currentTime<timeEightPM){
            
            ScheduleSMS__c ssms = new ScheduleSMS__c();
                        ssms.Contact__c = cmp.ContactId;
            ssms.CampaignMemberId__c = cmp.Id;
            ssms.MessageData__c = messageData;
            ssms.IsSchedule__c = true;
            if(mapIdFromNumber.get(cmp.CampaignId)!=null){
              ssms.From_Number__c = mapIdFromNumber.get(cmp.CampaignId);
            }
            sSMSList.add(ssms);
            //CampaignMemberTriggerHandler.sendSMS(cmp.Id,messageData,mapIdFromNumber.get(cmp.CampaignId));
          }else{
            System.debug('--here check 4 schedule---');
            ScheduleSMS__c ssms = new ScheduleSMS__c();
            ssms.Contact__c = cmp.ContactId;
            ssms.CampaignMemberId__c = cmp.Id;
            ssms.MessageData__c = messageData;
            ssms.IsSchedule__c = true;
            if(mapIdFromNumber.get(cmp.CampaignId)!=null){
              ssms.From_Number__c = mapIdFromNumber.get(cmp.CampaignId);
            }
            DateTime dttime = DateTime.now();
            if(dttime.hour() < t1)  {
              //Schedule it to be 8 am today
            }else  {
              //since we are past 8 pm schedule it to be 8 am next day
              dttime = dttime.addDays(1);
            }
             dttime = dttime.addHours(t1 - dttime.hour());
            dttime = dttime.addMinutes(0 - dttime.minute());
            dttime = dttime.addSeconds(0 - dttime.second());
            ssms.Schedule_Time__c = dttime;
            
            sSMSList.add(ssms);
            //insert ssms;
          }
        }
        
        insert sSMSList;
      } 
    }
        
    /*
* Description : Invoking trigger to link Created Opportunity to the Campaigns.
*/
    if(trigger.isAfter && trigger.isInsert){ 
        Set<id> camMemberIdSet = new Set<id>(); 
        Set<id> conIdSet = new Set<id>(); 
        
        for(CampaignMember camMem : Trigger.new){
            if(camMem.ContactId != null && camMem.campaignId != null){
                camMemberIdSet.add(camMem.id); 
                conIdSet.add(camMem.ContactId);
                System.debug('test camMem.Contact__c ===== '+camMem.ContactId);
            }    
        }
        if(! Test.isrunningTest() && camMemberIdSet != null && !camMemberIdSet.isEmpty()){
            CampaignConnectionHelper.getCampaignConnection(camMemberIdSet,conIdSet);
        }
    }

}