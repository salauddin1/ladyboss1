trigger TaxCodeNASet on Product2 (After update) {
    if(ContactTriggerFlag.isContactBatchRunning != true){
        Set<Id> CanRecordIds = trigger.newMap.keySet();
        List<Product2> listofproduct = [select id,Tax_Code__c from Product2 where Tax_Code__c=null and ID In :CanRecordIds];
        for(Product2 pro :listofproduct){
            pro.Tax_Code__c='NT';
        }
        if(listofproduct.size() >0) {
            update listofproduct;
        }
    }
}