trigger StripeCreateProductSKU on Product2 (after insert, after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        if(Trigger.isInsert){
            //create product id and sku id on stripe for products for order and update product
            List<Product2> products = [SELECT id, Name, Currency__c, Price__c, IsActive,Stripe_Plan_Id__c,Length__c,Stripe_Product_Id__c,Stripe_SKU_Id__c FROM Product2 WHERE (Stripe_Plan_Id__c = null) AND (Stripe_SKU_Id__c =null) AND (Stripe_Product_Id__c =null) AND ID IN :Trigger.new ];
            if(products.size()>0){
                system.enqueueJob(new StripeCreateProduct(products));
            }
        }
        
        if(Trigger.isUpdate){  
            //create product id and sku id on stripe for products for order and update product
            List<Product2> prodskuUpdateList = [SELECT id, Name, Currency__c, Price__c, IsActive,Stripe_Plan_Id__c,Length__c,Stripe_Product_Id__c,Stripe_SKU_Id__c FROM Product2 WHERE (Stripe_Plan_Id__c = null)  AND (Stripe_Product_Id__c =null) AND ID IN :Trigger.new ];
            if (prodskuUpdateList.size()>0) {
                system.enqueueJob(new StripeCreateProduct(prodskuUpdateList));
            }
            //create skuid for product on stripe and update product
            List<Product2> skuUpdateList = [SELECT id, Name, Currency__c,Length__c,Height__c,Width__c,Weight__c, Price__c, IsActive,Stripe_Plan_Id__c,Stripe_Product_Id__c,Stripe_SKU_Id__c FROM Product2 WHERE (Stripe_Plan_Id__c = null) AND (Stripe_SKU_Id__c =null) AND (Stripe_Product_Id__c !=null) AND ID IN :Trigger.new ];
            for (Product2 pr: skuUpdateList) {
                StripeCreateSKU.createsku('usd', 'infinite', pr.Price__c, pr.Stripe_Product_Id__c, pr.IsActive, pr.id, pr.Length__c,pr.Height__c,pr.Width__c, pr.Weight__c);
            }
            //setting tax code and updating sku on stripe
            List<Product2> prodToSetTaxCode = [SELECT id, Name, Currency__c,Length__c,Height__c,Width__c,Weight__c, Price__c, IsActive,Stripe_Plan_Id__c,Stripe_Product_Id__c,Stripe_SKU_Id__c,Tax_Code__c FROM Product2 WHERE (Stripe_Plan_Id__c = null) AND (Stripe_SKU_Id__c !=null) AND (Stripe_Product_Id__c !=null) AND ID IN :Trigger.new];
            List<Product2> prodList = new List<Product2>();
            for (Product2 pr: prodToSetTaxCode) {
                if (trigger.oldMap.get(pr.id).Tax_Code__c != trigger.newMap.get(pr.id).Tax_Code__c) {
                    prodList.add(pr);
                }
                if (trigger.oldMap.get(pr.id).Price__c != trigger.newMap.get(pr.id).Price__c) {
                    StripeUpdateSKU.updateSKU(pr.Stripe_SKU_Id__c, 'usd', 'infinite', pr.Price__c, pr.IsActive,pr.Length__c,pr.Height__c,pr.Width__c,pr.Weight__c);
                }
            }
            if (prodList.size()>0) {
                system.enqueueJob(new StripeCreateProduct(prodToSetTaxCode));   
            }

            //to update product with tax code for subscription products
            List<Product2> subProdToSetTaxCode = [Select Id, Name, Tax_Code__c, Stripe_Plan_Id__c From Product2 WHERE (Stripe_Plan_Id__c != null) AND (Tax_Code__c = null) AND ID IN : Trigger.new];
            for (Product2  prod: subProdToSetTaxCode) {
                prod.Tax_Code__c = 'NT';
            }
            if (subProdToSetTaxCode.size()>0) {
                update subProdToSetTaxCode;
            }            
        } 
    }
    
}