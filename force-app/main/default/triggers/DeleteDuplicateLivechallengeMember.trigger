trigger DeleteDuplicateLivechallengeMember on CampaignMember (after insert) {
    List<CampaignMember> newIds = new List<CampaignMember>();
    for(CampaignMember cm : Trigger.new){
        newIds.add(cm);
    }
    
    List<CampaignMember> newCampaignMember = [select CampaignId,Campaign.Product__c,Id,contactId from CampaignMember where Id =: newIds ];
    
    Set<Id> newMemeberCampaignIds = new Set<Id>();    
    for(CampaignMember cm:newCampaignMember){
        newMemeberCampaignIds.add(cm.CampaignId);
    }
    Set<Id> workflowConfigurationsCampaignIds = new Set<Id>();
    
    List<Workflow_Configuration__c> workflowConfigurations = [SELECT Target_Campaign__c FROM Workflow_Configuration__c where 
                                                              Products__c != null and Source_Campaign__c != null and 
                                                              Consider_For_CM_Movement__c = true and Target_Campaign__c =: newMemeberCampaignIds];
    
    
    for(Workflow_Configuration__c workflowConfiguration:workflowConfigurations){
        workflowConfigurationsCampaignIds.add(workflowConfiguration.Target_Campaign__c);
    }
    
    Set<Id> needTofindInOtherCampaings = new Set<Id>();
    
    for(CampaignMember cm : newCampaignMember){
        if(workflowConfigurationsCampaignIds != null && workflowConfigurationsCampaignIds.size()>0 && workflowConfigurationsCampaignIds.contains(cm.CampaignId)){
            needTofindInOtherCampaings.add(cm.ContactId);
        }
    }
    
    if(needTofindInOtherCampaings.size()>0){
        
        List<Workflow_Configuration__c> unlimitedOrSupplementCampaignsConfiguration = [SELECT Target_Campaign__c FROM Workflow_Configuration__c 
                                                     where Products__c != null and Source_Campaign__c != null 
                                                     and Consider_For_CM_Movement__c = true and Target_Campaign__r.Product__c in ('Unlimited','Supplement')];
        List<Workflow_Configuration__c> liveChallengeCampaignsConfiguration = [SELECT Target_Campaign__c FROM Workflow_Configuration__c 
                                                     where Products__c != null and Source_Campaign__c != null 
                                                     and Consider_For_CM_Movement__c = true and Target_Campaign__r.Product__c in ('LIVE Challenge')];
        Set<Id> unlimitedOrSupplementCampaignIds = new Set<Id>();
        Set<Id> liveChallengeCampaignIds = new Set<Id>();
        for(Workflow_Configuration__c workflow:unlimitedOrSupplementCampaignsConfiguration){
            unlimitedOrSupplementCampaignIds.add(workflow.Target_Campaign__c);
        }
        for(Workflow_Configuration__c workflow:liveChallengeCampaignsConfiguration){
            liveChallengeCampaignIds.add(workflow.Target_Campaign__c);
        }
        
        List<CampaignMember> unlimitedOrSupplementCampaignMembers = [select id,contactId from CampaignMember where
                                              campaignId =:unlimitedOrSupplementCampaignIds and ContactId =:needTofindInOtherCampaings];
        
        if(unlimitedOrSupplementCampaignMembers.size()>0){
            
            List<CampaignMember> liveChallengeCampaignMembers = [select id,contactId from CampaignMember where
                                              campaignId =:liveChallengeCampaignIds and ContactId =:needTofindInOtherCampaings];
        
            
            Set<Id> unlimitedMembers = new Set<Id>(); 
            for(CampaignMember cm:unlimitedOrSupplementCampaignMembers){
                unlimitedMembers.add(cm.ContactId);
            }
            
            List<CampaignMember> toDeleteCmList = new List<CampaignMember>();
            
            for(CampaignMember toBeRemove:liveChallengeCampaignMembers){
                if(unlimitedMembers.contains(toBeRemove.contactId)){
                    toDeleteCmList.add(toBeRemove);
                }
            }
            
            if(toDeleteCmList.size() > 0){
                //enqueue this as when we delete campaignmembers from batch , it calls a future method in five9 which can fail as we can't call future method from batch.
                System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>(toDeleteCmList),new List<CampaignMember>(),new List<CampaignMember>()));
            }
        }
        
    }
    
}