trigger EmailCaseComment on EmailMessage (after insert) {
    if(Trigger.isInsert && Trigger.isAfter){
        
        
        try{
            set<Id> caseIds = new set<Id>();
            map<Id,Case> caseMap = new map<Id,Case>();
            List<CaseComment>   csCommentList = new List<CaseComment>();
            List<Case>   csList = new List<Case>();
            
            for(EmailMessage message : trigger.new){
                if(message.Incoming == false){
                    if(message.ParentId != null && message.ParentId.getSObjectType().getDescribe().getName() == 'Case'){
                        caseIds.add(message.ParentId);
                        System.debug(' test '+message.ParentId);
                    }
                    
                }
            }
            if(caseIds != null && !caseIds.isEmpty()){
                caseMap =  new map<Id,Case>([Select id ,Is_Site_User_Case__c from case where id =: caseIds and	Is_Site_User_Case__c = true]);
            }
            if(caseMap != null && !caseMap.keySet().isEmpty()){
                for(EmailMessage message : trigger.new){
                    if(message.Incoming == false){
                        if(message.ParentId != null && message.ParentId.getSObjectType().getDescribe().getName() == 'Case'){
                            if(caseMap != null && caseMap.keySet() != null && !caseMap.keySet().isEmpty() && caseMap.keySet().contains(message.ParentId)){
                                if( message.textBody != null &&  message.textBody != ''){
                                    CaseComment   tComment = new CaseComment();
                                    tComment.ParentId = message.ParentId;
                                    tComment.CommentBody =  message.textBody ;
                                    tComment.IsPublished = TRUE;
                                    
                                    csCommentList.add(tComment);
                                    System.debug(' test '+csCommentList);
                                }
                            }
                        }
                        
                    }
                }
            }
            if(csCommentList != null && !csCommentList.isEmpty()){
                Insert csCommentList;    
            }
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('Trigger','EmailCaseComment','Id' ,ex));
            System.debug('***Get Exception***'+ex);        
        }
        
    }
}