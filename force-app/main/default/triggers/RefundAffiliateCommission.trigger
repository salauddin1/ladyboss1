trigger RefundAffiliateCommission on Pap_Commission__c (after insert) {
    Set<String> commissionIdSet = new Set<String>();
    for(Pap_Commission__c comm : trigger.new){
        if(comm.Contact__c != null &&  comm.Commission__c != null){
            if(comm.Commission_type__c  != null && (comm.Commission_type__c == 'refund' || comm.Commission_type__c == 'repeated'  )){
            	commissionIdSet.add(comm.id);
            }
        }
    }
    if(commissionIdSet.size()>0){
        RefundAffiliateCommissionHandler.updateAffiliateCommission(commissionIdSet);
    }
}