/* 
* Developer Name : Tirth Patel
* Description : Merged Triggers opportunityAfterUpdateTrigger,OpportunityBeforeTrigger,OpportunityTimeDiff and OpportunityWeekTrigger,SuccessPaymentWithCaseLink,OpportunityTrigger,OpportunityRollUp
* Test classes : opportunityAfterUpdateTriggerTest,OpportunityBeforeTriggerTest,OpportunityTimeDiff_Test,OpportunityWeekTriggerTest,SuccessPaymentWithCaseLinkTest,OpportunityTrigger_Test
*/
trigger OpportunitysTrigger on Opportunity (before insert,after insert,before update,after update,after delete, after undelete) {
    try{ 
        if(ContactTriggerFlag.isContactBatchRunning != true){
            if(Trigger.isAfter && Trigger.isUpdate){
                OpportunityAfterUpdateHandler.firstSellCheck(Trigger.newMap,Trigger.oldMap);
                OpportunityAfterUpdateHandler.salesPersonChange(Trigger.newMap,Trigger.oldMap);
                OpportunitysTriggerHandler.OpportunityWeekHandler(Trigger.new);
                OpportunitysTriggerHandler.rollupAfterUpdate(Trigger.new, trigger.oldMap);
            }
            if(Trigger.isBefore && Trigger.isInsert){
                OpportunityUpdateContact.updateOppContact(Trigger.new);
                OpportunitysTriggerHandler.stripeChargeCreation(Trigger.new);
                OpportunitysTriggerHandler.NMIStripeBeforeInsert(Trigger.new);
                CampaignConnectionHandler.getCampaignConnection(Trigger.new);
                //OpportunitysTriggerHandler.updatePapCommision2(Trigger.new);
            }
            if(Trigger.isBefore && Trigger.isUpdate){
                OpportunitysTriggerHandler.NMIStripeBeforeUpdate(Trigger.new, Trigger.oldMap);
            }
            if(Trigger.isAfter && Trigger.isInsert && OpportunityUpdateContact.oppflag == false){
                for(Opportunity opp: trigger.new){
                    System.debug('------opp.selected Free Bonus------'+opp.Selected_Free_Bonus_Products__c);
                    if(opp.Selected_Free_Bonus_Products__c != null && opp.Selected_Free_Bonus_Products__c != '') {
                        FreeBonusTaskCreate.createFreeRetentionTask(trigger.new);
                    }
                }
                
                OpportunitysTriggerHandler.OpportunityWeekHandler(Trigger.new);
                OpportunitysTriggerHandler.OpportunityTimeDiffHandler(Trigger.new,Trigger.newMap);
                OpportunitysTriggerHandler.SuccessPaymentWithCaseLink(Trigger.new);
                OpportunitysTriggerHandler.firstSellCheckHandler(Trigger.new);
                OpportunitysTriggerHandler.rollupInsertUndelete(Trigger.new);
                OpportunitysTriggerHandler.updatePapCommision(Trigger.new);
            }
            if(Trigger.isAfter &&Trigger.isDelete){
                OpportunitysTriggerHandler.rollupDelete(Trigger.old);
            }
            if(Trigger.isAfter &&Trigger.isUndelete){
                OpportunitysTriggerHandler.rollupInsertUndelete(Trigger.new);
            }
        }
        if(Test.isRunningTest()){
            System.debug(1/0);
        }
    }catch(Exception e){
        ApexDebugLog apex=new ApexDebugLog();
        apex.createLog(
            new ApexDebugLog.Error('opportunityAfterUpdateTrigger','execute',null,e));
    }
}