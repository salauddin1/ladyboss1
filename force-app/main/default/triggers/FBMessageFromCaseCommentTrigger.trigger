trigger FBMessageFromCaseCommentTrigger on CaseComment (after insert,before insert) {
  if(ContactTriggerFlag.isContactBatchRunning != true && CaseCommmentStaticFlagClass.flag){
    FBMessageFromCaseCommentTriggerHandler handler = new FBMessageFromCaseCommentTriggerHandler(Trigger.isExecuting, Trigger.size);
    if(Trigger.isInsert && Trigger.isBefore){
      handler.OnBeforeInsert(Trigger.new);
    }else if(Trigger.isInsert && Trigger.isAfter){
      handler.OnAfterInsert(Trigger.new);
      FBMessageFromCaseCommentTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
    }
    if(Trigger.isInsert && Trigger.isAfter) {
            ReopenCaseFromCommuntiy.reopenCase(Trigger.new);
    }
  }
}