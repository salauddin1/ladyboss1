trigger HistoryTracking on OpportunityLineItem (after update) {
    List<OLI_History_Tracking__c> hisTrackList = new List<OLI_History_Tracking__c>();
    Map<id, String> oliFieldMap = new Map<id, String>();
    for (OpportunityLineItem oli: Trigger.new){
        if (oli.Status__c != Trigger.oldMap.get(oli.Id).Status__c) {
            oliFieldMap.put(oli.Id, 'Status');
        }
        if (oli.Cancel_Subscription_At__c != Trigger.oldMap.get(oli.Id).Cancel_Subscription_At__c) {
            oliFieldMap.put(oli.Id, 'Cancel Subscription at');
        }
    }
    for (OpportunityLineItem oli : [SELECT id,Product2.Name,OpportunityId,Status__c,Cancel_Subscription_At__c FROM OpportunityLineItem WHERE Id IN: oliFieldMap.keySet()]) {
        String name = oli.Product2.Name;
        if(name.length()>80){
            name = name.substring(0, 75);
        }
        OLI_History_Tracking__c oht = new OLI_History_Tracking__c();
        oht.Name = name;
        oht.Record_Id__c = oli.OpportunityId;
        oht.Field_Name__c = oliFieldMap.get(oli.Id);
        if (oht.Field_Name__c == 'Status'){
            oht.Value__c = oli.Status__c;
        } else {
            oht.Value__c = String.valueOf(oli.Cancel_Subscription_At__c);
        }
        oht.Modified_Date_Time__c = System.now();
        oht.Modified_By__c = UserInfo.getUserId();
        oht.OpportunityLineItem_Name__c = oli.Product2.Name;
        hisTrackList.add(oht);
    }
    if(!hisTrackList.isEmpty()) {
        insert hisTrackList;
    }
}