/*
* Devloper : Sourabh Badole
* Description : Invoking trigger to calculate customer value index of Created Opportunity/Order on Contact object.
*/
trigger OpportunityRollUp on Opportunity (before insert, after insert, after update, after Delete, after undelete) {
    if((trigger.isafter && trigger.isInsert) || (trigger.isafter && trigger.isUpdate) || (trigger.isAfter && trigger.isDelete) || (trigger.isAfter && trigger.isUndelete) ){
        Opportunity[] objects = null;   
        
        if (Trigger.isDelete) {
            List<Opportunity>  oppList = new List<Opportunity>(); 
            for(Opportunity opp : Trigger.old) {
                if(opp != null && opp.contact__c != null && opp.Amount != null) {
                    oppList.add(opp);
                }
            }
            if(oppList != null && !oppList.isEmpty()){
                objects = oppList ;
            }
        }
        else {
            /*
Handle any filtering required, specially on Trigger.isUpdate event. If the rolled up fields
are not changed, then please make sure you skip the rollup operation.
We are not adding that for sake of similicity of this illustration.
*/ 
            List<Opportunity>  oppList = new List<Opportunity>(); 
            for(Opportunity opp : Trigger.new ) {
                if(opp != null && opp.contact__c != null && opp.Amount != null) {
                    if(trigger.isafter && trigger.isUpdate ){
                        if( trigger.oldMap.get(opp.id).amount != opp.amount || trigger.oldMap.get(opp.id).Refund_Amount__c != opp.Refund_Amount__c) {
                            oppList.add(opp);
                        }
                    }
                    else{
                        oppList.add(opp);
                    }
                }
            }
            if(oppList != null && !oppList.isEmpty()){
                objects = oppList;
            }
        }
        
        /*
First step is to create a context for LREngine, by specifying parent and child objects and
lookup relationship field name
*/
        LREngine.Context ctx = new LREngine.Context(Contact.SobjectType, // parent object
                                                    Opportunity.SobjectType,  // child object
                                                    Schema.SObjectType.Opportunity.fields.Contact__c // relationship field name
                                                   );     
        /*
Next, one can add multiple rollup fields on the above relationship. 
Here specify 
1. The field to aggregate in child object
2. The field to which aggregated value will be saved in master/parent object
3. The aggregate operation to be done i.e. SUM, AVG, COUNT, MIN/MAX
*/
        ctx.add(
            new LREngine.RollupSummaryField(
                Schema.SObjectType.Contact.fields.Total_Spend_Amount__c,
                Schema.SObjectType.Opportunity.fields.Amount,
                LREngine.RollupOperation.Sum 
            )); 
        ctx.add(
            new LREngine.RollupSummaryField(
                Schema.SObjectType.Contact.fields.Spend_Without_Refund__c,
                Schema.SObjectType.Opportunity.fields.Spend_Amount__c,
                LREngine.RollupOperation.Sum 
            )); 
        /* 
Calling rollup method returns in memory master objects with aggregated values in them. 
Please note these master records are not persisted back, so that client gets a chance 
to post process them after rollup
*/ 
        if(objects != null ){
            Sobject[] masters = LREngine.rollUp(ctx, objects);    
            
            // Persiste the changes in master
            update masters;
        }
    }    
    
    
        /*
* Description : Invoking trigger to link Created Opportunity to the Campaigns.
*/
    if(trigger.isBefore && trigger.isInsert){
        CampaignConnectionHandler.getCampaignConnection(Trigger.new);
    }
    
}