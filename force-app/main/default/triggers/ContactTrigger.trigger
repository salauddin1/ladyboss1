/*
* Developer Name: Tirth Patel
* Discription : ContactBeforeTrigger,ContactCoachingTrigger,ContactAddressUpdateTrigger,ContactCampaignUpdateTrigger,MC_CampaignGenearator and MailofContactTrigger are merged.
ContactCampaignUpdateTrigger_Test,MC_CampaignGenearator_Test,CampaignMemberBeforeTriggerTest,CoachingTriggerHeplerTest,ContactCampaignUpdateTrigger_Test,MailOfContactTriggerTest,ContactSiteLoginEmailTest are test
classes for this merged trigger.
*/
trigger ContactTrigger on Contact (before insert,after insert,before update,after update) {
    try{
        if(!ContactTriggerFlag.isContactBatchRunning){
            if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) && OrderFormControllerChanges.invoiceflag == false){
                //ensures that marketing cloud tag should always be null for amazon contacts.
                ContactTriggerHandler.checkAmazonCustomer(Trigger.New);
            }
            
            if(Trigger.isAfter && Trigger.isInsert && OrderFormControllerChanges.invoiceflag == false){
                ContactTriggerHandler.addressHandler(Trigger.New);
                ContactTriggerHandler.PartnerContactInsert(Trigger.new);//To create contact as a campaign member
                
                if(!System.isBatch()){
                    ContactTriggerHandler.ContactFIve9Update(Trigger.new,Trigger.newMap);
                }
            }
            if(Trigger.isBefore && Trigger.isUpdate && OrderFormControllerChanges.invoiceflag == false){
                ContactTriggerHandler.checkDnc(Trigger.New);
                if(!System.isBatch()){
                    ContactTriggerHandler.campaignUpdate(Trigger.new,Trigger.newMap,Trigger.oldMap);
                    ContactTriggerHandler.checkfive9DNC(Trigger.new,Trigger.newMap,Trigger.oldMap);
                }
            }
            if(Trigger.isAfter && Trigger.isUpdate){
                ContactTriggerHandler.campaignHandler(Trigger.new,Trigger.newMap,Trigger.oldMap);
                ContactTriggerHandler.SendEmail(Trigger.new, Trigger.oldMap);//For sending Site Login email for LadyBossHub
                if(Trigger.newMap.get(trigger.new[0].Id).Enable_Product_Access_In_Hub__c != Trigger.oldMap.get(trigger.new[0].Id).Enable_Product_Access_In_Hub__c) {
                    ContactTriggerHandler.doCharge(trigger.new, trigger.old);
                }
                ContactTriggerHandler.updateRefCheckID(Trigger.New,Trigger.NewMap,Trigger.OldMap);
            }
            if((Trigger.isBefore && Trigger.IsUpdate) ||(Trigger.IsAfter && Trigger.IsInsert)){
                List<Contact> TriggerOld = new List<Contact>();
                if(Trigger.IsUpdate){
                    TriggerOld = Trigger.old;
                }
                ContactTriggerHandler.MailofContact(Trigger.isInsert, Trigger.New, TriggerOld, Trigger.NewMap);
            }
            if(Trigger.isAfter && Trigger.isInsert) {
                ContactTriggerHandler.addPapToContact(Trigger.new);
                //ContactTriggerHandler.abandonedCartObject(Trigger.new);
            }
            if(Trigger.isBefore && Trigger.isInsert){
                ContactTriggerHandler.setMasterContact(Trigger.new);
            }
            if(Trigger.isAfter && Trigger.isUpdate) { 
                for(Contact con : trigger.new) {
                    if(Trigger.oldMap.get(con.Id).PAP_refid__c != Trigger.newMap.get(con.Id).PAP_refid__c) {
                        ContactTriggerHandler.addPapToContact(new List<contact>{Trigger.newMap.get(con.Id)});
                    }
                }
            }
        }
        if(Trigger.isAfter && ( Trigger.isUpdate || Trigger.isInsert ) && OrderFormControllerChanges.invoiceflag == false){
            ContactTriggerHandler.coachingCommission(Trigger.New);
            Map<Id,Contact> trigOldMap =new Map<Id,Contact>();
            if(Trigger.isUpdate){
                trigOldMap = Trigger.oldMap;
            }
            ContactTriggerHandler.MC_CampaignGenearator(Trigger.isInsert,Trigger.isUpdate,Trigger.New,Trigger.NewMap,trigOldMap);                
            
        }
        /*if(Test.isRunningTest()){ 
System.debug(1/0);
}*/
    }Catch(Exception ex){
        ApexDebugLog apex=new ApexDebugLog(); 
        apex.createLog(
            new ApexDebugLog.Error('ContactTrigger','ContactTrigger',' ',ex));
    }
}