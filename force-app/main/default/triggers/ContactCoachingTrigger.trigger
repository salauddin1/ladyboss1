trigger ContactCoachingTrigger on Contact (before insert , after insert , before Update ,after Update) {
    /*
    if(Trigger.isBefore && ( Trigger.isUpdate || Trigger.isInsert ) ){
        for(Contact con :Trigger.new){
            if(con != null ){
                if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c == 'Full Pay'){
                    con.Full_Pay__c = 'M1C1;M1C2;M2C1;M2C2;M3C1;M3C2;M4C1;M4C2;M5C1;M5C2;M6C1;M6C2;M7C1;M7C2;M8C1;M8C2;M9C1;M9C2;M10C1;M10C2;M11C1;M11C2;M12C1;M12C2';
                } 
            }
        }      
    }*/
    if(ContactTriggerFlag.isContactBatchRunning != true && Trigger.isAfter && ( Trigger.isUpdate || Trigger.isInsert ) ){
        Set<Id> conIds = new Set<Id>();
        for(Contact con :Trigger.new){
            if(con != null ){
                conIds.add(con.Id);
            }
        }
        List<Coaching_Commissions__c > oldCoachComm = new List <Coaching_Commissions__c>();
        List<Coaching_Commissions__c > coachDataList = new List <Coaching_Commissions__c>();
        
        if(conIds != null  && !conIds.isEmpty()){
            oldCoachComm = [select id, createdDate ,Contact__c,Coaching_Payment_Option__c,Commission_Parent_Type__c ,Commission_Year__c ,Commission_Amount__c ,Commission_Type__c from Coaching_Commissions__c where ( (Contact__c =: conIds) AND (Commission_Amount__c != 0) AND (Commission_Type__c != null ) AND (Contact__c != null) )]; 
            System.debug('**oldCoachComm**'+oldCoachComm);
        }
        
        Map<id ,Map<String, List<String>> > oldCommissionMap = new Map<id ,Map<String, List<String>> >();
        
        if(oldCoachComm != null && !oldCoachComm.isEmpty()){
            for(Coaching_Commissions__c coachingCom : oldCoachComm ){
                if(coachingCom.Contact__c != null && coachingCom.Commission_Type__c != null && coachingCom.Commission_Parent_Type__c != null ){
                    if(oldCommissionMap.keySet().contains(coachingCom.Contact__c)){
                        Map<String, List<String>>   childCommMap = new Map<String, List<String>> ();
                        childCommMap = oldCommissionMap.get(coachingCom.Contact__c);
                        if(childCommMap.keySet().contains(coachingCom.Commission_Parent_Type__c)){
                            String str = coachingCom.Commission_Type__c+';'+coachingCom.Commission_Year__c;
                            childCommMap.get(coachingCom.Commission_Parent_Type__c).add(str);
                        }   
                        else{
                            List<String> commissionVals = new List<String>();
                            String str = coachingCom.Commission_Type__c+';'+coachingCom.Commission_Year__c;
                            commissionVals.add(str);
                            childCommMap.put(coachingCom.Commission_Parent_Type__c , commissionVals);
                            
                        }
                    }
                    else{    
                        
                        Map<String, List<String>>   childCommMap = new Map<String, List<String>> ();
                        
                        List<String> commissionVals = new List<String>();
                        String str = coachingCom.Commission_Type__c+';'+coachingCom.Commission_Year__c;
                        commissionVals.add(str);
                        childCommMap.put(coachingCom.Commission_Parent_Type__c , commissionVals);
                        oldCommissionMap.put(coachingCom.Contact__c ,childCommMap);         
                    }
                }
            }   
        }
        
        for(Contact con :Trigger.new){
            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
            returnCommission =  CoachingTriggerHepler.getCommission(con , oldCommissionMap);
            if(returnCommission != null && !returnCommission.isEmpty()){
                coachDataList.addAll(returnCommission);
            }
        }
        if(coachDataList != null  && !coachDataList.isEmpty()){
            try{
                Insert coachDataList;
                System.debug('**coachDataList**'+coachDataList);
            }
            catch(Exception ex){
                String getConIds = String.valueOf(trigger.newMap.keySet());
                ApexDebugLog apex=new ApexDebugLog(); 
                apex.createLog(
                    new ApexDebugLog.Error('ContactCoachingTrigger',
                                           'getCommission',getConIds,ex));

                System.debug('** Get Exception **'+ ex);
            }
        }
        
    }            
}