trigger OlitoAvalaraTrigger on OpportunityLineItem (after update) { 
    if((ContactTriggerFlag.isContactBatchRunning != true && !System.isBatch()) || Test.isRunningTest()){
        try {
            Map<Id,Opportunity> prodOpp = new Map<Id,Opportunity>();
            Map<Id,OpportunityLineItem> prodOppline = new Map<Id,OpportunityLineItem>();
            List<Id> oppId = new List<Id>();
            List<Id> oppIdCharge = new List<Id>();
            List<Id> conId = new List<Id>();
            Map<id,Address__c> addCon = new Map<id,Address__c>();
            Map<id,Contact> conIdCon = new Map<id,Contact>();
            String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
            if (Trigger.isUpdate) {
                for (OpportunityLineItem oli : Trigger.new) {
                    if((Trigger.newMap.get(oli.id).Last_Charge_Id__c != Trigger.oldMap.get(oli.id).Last_Charge_Id__c)&&Trigger.newMap.get(oli.id).subscription_id__c!=null&&Trigger.newMap.get(oli.id).Tax_Amount__c != null && Trigger.newMap.get(oli.id).Tax_Amount__c != 0.00){
                        prodOppline.put(oli.Product2Id, oli);
                        system.debug('prodOppline ' +prodOppline);
                        oppId.add(oli.OpportunityId);
                    }
                }
                if(prodOppline.size()>0 && oppId.size()>0){
                    
                    List<String> prodidtoberemoved = new List<String>();
                    for(Opportunity opp: [SELECT id,Created_Using__c,Stripe_Charge_Id__c,RecordType.DeveloperName,Contact__c,(SELECT id,Product2Id FROM OpportunityLineItems) FROM Opportunity WHERE id IN :oppId]){
                        system.debug('opp'+opp);
                        for(OpportunityLineItem opli:opp.OpportunityLineItems){
                            if(!prodOpp.containsKey(opli.Product2Id)){
                                prodOpp.put(opli.Product2Id, opp);
                                conId.add(opp.Contact__c);
                                if (opp.Created_Using__c == 'WooCommerce' && prodOppline.get(opli.Product2Id).Stripe_charge_id__c == prodOppline.get(opli.Product2Id).Last_Charge_Id__c) {
                                    prodidtoberemoved.add(opli.Product2Id);
                                }
                            }
                        }
                    }
                    system.debug(prodOpp);
                    system.debug(conId);
                    for (String prodid : prodidtoberemoved) {
                        prodOppline.remove(prodid);
                        prodOpp.remove(prodid);
                    }
                   
                    for(Address__c add: [SELECT Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c,Contact__c FROM Address__c WHERE Contact__c IN : conId and Contact__c!=null  AND Primary__c = true]){
                        addCon.put(add.Contact__c, add);
                    }
                    for (Contact con : [SELECT Id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry FROM Contact WHERE MailingCity != null AND Id IN:conId AND MailingState != null]) {
                        conIdCon.put(con.Id,con);
                    }
                    for(Product2 prod:[SELECT Id,Name,Price__c,Tax_Code__c,Statement_Descriptor__c FROM Product2 WHERE id IN :prodOppline.keySet()]){
                        String statDesc;
                        if(prod.Statement_Descriptor__c != null){
                            statDesc = prod.Statement_Descriptor__c;
                        }else {
                            statDesc = prod.Name;
                        }
                        String shippingStreet = '10010 Indian School Rd. NE';
                        String shippingCity = 'Albuquerque';
                        String shippingState = 'NM';
                        String shippingZipCode = '87112';
                        String shippingCountry = 'US';
                        if (addCon.containsKey(prodOpp.get(prod.Id).Contact__c)) {
                            shippingStreet = addCon.get(prodOpp.get(prod.Id).Contact__c).Shipping_Street__c;
                            shippingCity = addCon.get(prodOpp.get(prod.Id).Contact__c).Shipping_City__c;
                            shippingState = addCon.get(prodOpp.get(prod.Id).Contact__c).Shipping_State_Province__c;
                            shippingZipCode = addCon.get(prodOpp.get(prod.Id).Contact__c).Shipping_Zip_Postal_Code__c;
                            shippingCountry = addCon.get(prodOpp.get(prod.Id).Contact__c).Shipping_Country__c;
                        }else if (conIdCon.containsKey(prodOpp.get(prod.Id).Contact__c)) {
                            shippingStreet = conIdCon.get(prodOpp.get(prod.Id).Contact__c).MailingStreet;
                            shippingCity = conIdCon.get(prodOpp.get(prod.Id).Contact__c).MailingCity;
                            shippingState = conIdCon.get(prodOpp.get(prod.Id).Contact__c).MailingState;
                            shippingZipCode = conIdCon.get(prodOpp.get(prod.Id).Contact__c).MailingPostalCode;
                            shippingCountry = conIdCon.get(prodOpp.get(prod.Id).Contact__c).MailingCountry;
                        }
                        if (shippingCountry == null || shippingCountry == 'USA' || shippingCountry == 'United States' || shippingCountry.contains('United')) {
                            shippingCountry = 'US';
                        }
                        TaxjarTaxCalculate.createTransaction(shippingStreet,shippingCity,shippingState,shippingCountry,shippingZipCode,prod.Price__c, 1,prodOppline.get(prod.Id).Tax_Amount__c, statDesc,prodOppline.get(prod.Id).Last_Charge_Id__c, prod.Name,prodOppline.get(prod.Id).Shipping_Cost__c,prodOpp.get(prod.Id).Created_Using__c);
                    }
                }
            }
            
        } catch(Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OlitoAvalaraTrigger','trigger','',ex) );
        }
    }
}