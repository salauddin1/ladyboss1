//Trigger to update case Owner and Case Status when Closed case is responded by the customer 
trigger OmniCaseStatusTrigger on EmailMessage (before insert) {
    List<case> csListUpdate = new List<case>();
    List<Id> caseownerid = new List<Id>();
    Set<Id> caseId = new Set<Id>();
    for(EmailMessage em : Trigger.New ) {
        if(em.ParentId!=null && em.Incoming){
            if(em.ParentId.getsobjecttype()!=null){                
                if(String.valueOf(em.ParentId.getsobjecttype()) =='Case') {
                    caseId.add(em.ParentId);                     
                }
            }
        }        
    }
    //Only update Owner and Status of cases who's Status is "Closed"
    for(Case cs:[select id,Routed_From__c,ownerId,Status from case where Id IN :caseId AND Routed_From__c != null AND Status = 'Closed']){
    //  cs.ownerId = cs.Routed_From__c;     
        cs.Status = 'Re-Received';
        csListUpdate.add(cs);
        caseownerid.add(cs.OwnerId);
    }
    
    Map<ID,String>  userQueueNameMap = new Map<ID,String>();
    Map<String,Id>  queueNameIdMap = new Map<String,Id>();
    For(User u: [SELECT id,Reopen_queue__c FROM USER where id in:caseownerid and Reopen_queue__c != null]){
    	userQueueNameMap.put(u.Id, u.Reopen_queue__c); 
    }
    for(Group g:[Select id,Name From Group WHERE Name in :userQueueNameMap.values() ]){
    	queueNameIdMap.put(g.Name,g.id);
    }
    for(case cs : csListUpdate){
        if(userQueueNameMap.containsKey(cs.OwnerId) && userQueueNameMap.get(cs.OwnerId) != null && queueNameIdMap.containsKey(userQueueNameMap.get(cs.OwnerId)) && queueNameIdMap.get(userQueueNameMap.get(cs.OwnerId)) != null){
            cs.OwnerId = queueNameIdMap.get(userQueueNameMap.get(cs.OwnerId));
        }
       
    }
    if(csListUpdate.size()>0){
        update csListUpdate;
    }
}