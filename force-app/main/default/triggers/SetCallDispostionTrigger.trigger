trigger SetCallDispostionTrigger on Task (before insert) {
    for(Task tsk:Trigger.new){
		if(tsk.Five9__Five9AgentExtension__c != null && tsk.CallDisposition != null){
			String subject = tsk.Subject + ', ' + tsk.CallDisposition;
			if (subject.length()>255) {
				subject = subject.substring(0,255);
			}
    		tsk.Subject = subject;
    	}
    }
}