trigger CaseAgeTrigger on Case (after insert, after Update) {
    if(trigger.isAfter && trigger.isinsert){
        if(Trigger.new != null && !Trigger.new.isEmpty()){
            CaseAgeTriggerHelper.getCaseAgeInsert(Trigger.new);
        }
    }
    if(trigger.isAfter && trigger.isUpdate){
        if(Trigger.new != null && !Trigger.new.isEmpty() && Trigger.oldMap != null && Trigger.oldMap.keySet() != null ){
            CaseAgeTriggerHelper.getCaseAgeUpdate(Trigger.new, Trigger.oldMap);
        } 
    }
}