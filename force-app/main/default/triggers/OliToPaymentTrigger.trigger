trigger OliToPaymentTrigger on OpportunityLineItem (after update,after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        try{
            if(Trigger.isUpdate){
                List<OpportunityLineItem> oliToUpdateList = new List<OpportunityLineItem>();
                List<Payment__c> paymentList = new List<Payment__c>();
                Map<id,Decimal> oli_Ids_New = new Map<id,Decimal>();
                Map<id,Decimal> oli_Ids_Old = new Map<id,Decimal>();
                List<OpportunityLineItem> oliList = [SELECT id,Opportunity.Sales_Person_Id__c,Opportunity.week__c,Opportunity.stripe_charge_id__c FROM OpportunityLineItem WHERE id in : Trigger.new];
                Map<id,Decimal> oppAndWeekMap = new Map<id,Decimal>();
                Map<id,id> oppSpMap = new Map<id,id>();
                for(OpportunityLineItem ol : oliList){
                    if(ol.Opportunity.week__c > 0){
                        oppAndWeekMap.put(ol.id,ol.Opportunity.week__c);
                    }
                    if(ol.Opportunity.Sales_Person_Id__c != null){
                        oppSpMap.put(ol.id,ol.Opportunity.Sales_Person_Id__c);
                    }
                }
                for(OpportunityLineItem  oli : Trigger.new){
                    if(oli.refund__c == null)  {
                        oli_Ids_New.put(oli.id,0);
                    }else  {
                        oli_Ids_New.put(oli.id,oli.refund__c );
                    }                
                }
                for(OpportunityLineItem  oli : Trigger.old){
                    Decimal ref = (oli.refund__c == null)?0:oli.refund__c;
                    oli_Ids_Old.put(oli.id,ref);
                    
                }
                for(OpportunityLineItem oliToPay : Trigger.new){
                    Payment__c pay = new Payment__c();
                    pay.Opportunity__c = oliToPay.OpportunityId;
                    pay.week__c = oppAndWeekMap.get(oliToPay.id);
                    pay.Payment_Created_By__c = oppSpMap.get(oliToPay.id);
                    pay.Product__c = oliToPay.Product2Id;
                    if((oli_Ids_Old.get(oliToPay.id) > oli_Ids_New.get(oliToPay.id)) && Trigger.newmap.get(oliToPay.id).Consider_For_Commission__c==true){
                        //Trigger.newmap.get(oliToPay.id).addError('bad');
                    }
                    if((oli_Ids_Old.get(oliToPay.id) < oli_Ids_New.get(oliToPay.id)) && Trigger.newmap.get(oliToPay.id).Consider_For_Commission__c==true) {  
                        pay.Amount__c = Trigger.oldmap.get(oliToPay.id).current_commissionable_amount__c -Trigger.newmap.get(oliToPay.id).current_commissionable_amount__c; 
                        pay.Actual_amount__c = (oli_Ids_Old.get(oliToPay.id) - oli_Ids_New.get(oliToPay.id));
                        paymentList.add(pay);
                    }else if(Trigger.newmap.get(oliToPay.id).Consider_For_Commission__c==true && Trigger.oldmap.get(oliToPay.id).Consider_For_Commission__c == false ){
                        if(PaymentToOliTriggerFlag.runPaymentToOliTrigger==true){
                            pay.Amount__c = oliToPay.Total_Commissionable_amount__c;
                            pay.Actual_amount__c = oliToPay.TotalPrice;
                            paymentList.add(pay);
                        }
                    }
                    system.debug('---'+oliToPay.Number_Of_Payments__c+'----'+oliToPay.Cancellation_Days__c);
                    if(oliToPay.Success_Failure_Message__c != 'customer.subscription.deleted' && oliToPay.Number_Of_Payments__c > 0 && oliToPay.Cancellation_Days__c > 0 &&(oliToPay.Number_Of_Payments__c == oliToPay.Cancellation_Days__c) ){
                        system.debug('---'+oliToPay.Number_Of_Payments__c+'----'+oliToPay.Cancellation_Days__c);
                        if(!System.isBatch() && !System.isFuture()){
                        ActiveSubscriptionLineItems.cancelSubscriptionFuture(oliToPay.Id);
                        }
                    }
                    if((Trigger.oldmap.get(oliToPay.id).Subscription_Id__c == null || Trigger.oldmap.get(oliToPay.id).Subscription_Id__c == '') && (oliToPay.Subscription_Id__c != null || oliToPay.Subscription_Id__c != '') && (oliToPay.Subscription_Id__c != Trigger.oldmap.get(oliToPay.id).Subscription_Id__c)){
                        oliToUpdateList.add(oliToPay);
                    }
                } 
                if(paymentList.size()>0){
                    insert paymentList;
                    System.debug('inserted paymentList is : '+paymentList);
                }
                if(oliToUpdateList.size()>0 && ContactTriggerFlag.isOliUpdateLogicRunning!=true){
                    ContactTriggerFlag.isOliUpdateLogicRunning = true;
                    InvoiceToChargeUpdateHandler.OliUpdater(oliToUpdateList);
                }
            }
            if(Trigger.isInsert){
                System.debug('Trigger.new '+Trigger.new);
                List<Task> taskList = new List<Task>();
                List<OpportunityLineItem> oliList = [SELECT Product2.Name,Product2.Id,Product2.isMonthProduct__c,quantity,OpportunityId,CreatedDate,Opportunity.Contact__c,Opportunity.Sales_Person_Id__c,Opportunity.Contact__r.email FROM OpportunityLineItem WHERE id in : Trigger.new AND Product2.isMonthProduct__c=true];
                if(oliList.size()>0){
                    List<Id> prodIdList = new List<Id>();
                    for(OpportunityLineItem oli : oliList ){
                        prodIdList.add(oli.Product2Id);
                    }
                    Map<id,Integer> product_and_index = new Map<id,Integer>();
                    List<Product2> prodList = [SELECT id,Switch_To_Product_two_month__c,Switch_To_Product_three_month__c,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 WHERE Switch_To_Product_two_month__c!=null OR Switch_To_Product_three_month__c!=null OR Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c!=null];
                    for(Id prodId : prodIdList){
                        for(Product2 prod : prodList){
                            if(prod.Switch_To_Product_two_month__c == prodId){
                                product_and_index.put(prodId,2);
                                break;
                            }else if(prod.Switch_To_Product_three_month__c == prodId){
                                product_and_index.put(prodId,3);
                                break;
                            }else if(prod.Switch_To_Product_four_month__c == prodId){
                                product_and_index.put(prodId,4);
                                break;
                            }else if(prod.Switch_To_Product_five_month__c == prodId){
                                product_and_index.put(prodId,5);
                                break;
                            }else if(prod.Switch_To_Product_six_month__c == prodId){
                                product_and_index.put(prodId,6);
                                break;
                            }
                        }
                        
                    }
                    System.debug('product_and_index : '+product_and_index);
                    for(OpportunityLineItem oppLineItem : oliList){
                        Integer loopLength = 0;
                        if(oppLineItem.Product2.isMonthProduct__c==true && product_and_index.keySet().size() > 0 && oppLineItem.Opportunity.Sales_Person_Id__c!=null){
                            if (product_and_index.get(oppLineItem.Product2Id) >= 4) {
                                loopLength = 3;
                            }else {
                                loopLength = 2;
                            }
                            for (Integer i = 0; i < loopLength; i++) {
                                Task tsk = new Task();
                                tsk.Subject = 'Sold '+oppLineItem.Product2.Name+' | '+oppLineItem.Opportunity.Contact__r.email;
                                tsk.Contact__c = oppLineItem.Opportunity.contact__c;
                                tsk.Opportunity__c = oppLineItem.OpportunityId;
                                tsk.OwnerId = oppLineItem.Opportunity.Sales_Person_Id__c;
                                if (i == 0) {
                                    tsk.ActivityDate = System.today().addMonths(Integer.valueOf(product_and_index.get(oppLineItem.Product2Id))).addDays(-10);
                                }else if (i == 1) {
                                    tsk.ActivityDate = System.today().addDays(10);
                                }else if (i == 2) {
                                    tsk.ActivityDate = System.today().addDays(90);
                                }
                                taskList.add(tsk);
                            }
                            
                        }
                    }      
                    if(taskList.size()>0){
                        insert taskList; 
                        System.debug('tasklist : '+taskList);
                    }
                }
            }     
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OliToPaymentTrigger','trigger',trigger.new[0].id,ex) );
        }
    }
}