/*
* Developer Name : Tirth Patel
* Description : This trigger gets executed when oli status is updated to "InActive" upon cancellation of subscription from 
salesforce or stripe.Then it updates status of all account recovery cases to "Closed-AR-Expired-Subscription" for customer.
*/
trigger OliAfterUpdateTrigger on OpportunityLineItem (after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        try{
            List<Id> oppIds = new List<Id>();
            List<Id> oliListBalance = new List<Id>();
            Set<String>  emailID = new Set<String>();
            List<Id> opliIdList = new List<Id>();
            for(OpportunityLineItem oli : Trigger.new){
                if(Trigger.newMap.get(oli.id).Status__c == 'InActive' && Trigger.oldMap.get(oli.id).Status__c != 'InActive'){
                    oppIds.add(oli.OpportunityId);
                }
                system.debug('new '+Trigger.newMap.get(oli.id).Add_Tax_To_Subscription__c +' old '+Trigger.oldMap.get(oli.id).Add_Tax_To_Subscription__c+' '+oli.Id);
                if(Trigger.newMap.get(oli.id).Add_Tax_To_Subscription__c != Trigger.oldMap.get(oli.id).Add_Tax_To_Subscription__c){
                    opliIdList.add(oli.Id);
                } 
                if (oli.Balance_Applied_on_Last_Invoice__c != null && oli.Balance_Applied_on_Last_Invoice__c != 0  && (Trigger.oldMap.get(oli.Id).Balance_Applied_on_Last_Invoice__c != oli.Balance_Applied_on_Last_Invoice__c || oli.Same_Balance_as_Before__c == true)) {
                    oliListBalance.add(oli.Id);
                }               
            }
            if (opliIdList.size()>0) {
                Map<String, Id> subConMap = new Map<String, Id>();
                Map<String, Id> subProdMap = new Map<String, Id>();
                Map<Id, Address__c> addMap = new Map<Id, Address__c>();
                Map<Id, Card__c> cardMap = new Map<Id, Card__c>();
                Map<String, OpportunityLineItem> opliMap = new Map<String, OpportunityLineItem>();
                Map<Id, String> taxCode = new Map<Id, String>();
                for (OpportunityLineItem opli: [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c,totalPrice,Add_Tax_to_subscription__c FROM OpportunityLineItem where Id in : opliIdList AND Opportunity.Contact__c != null AND Opportunity.Created_Using__c != null AND Opportunity.RecordType.Name = 'Subscription']) {
                    subConMap.put(opli.Subscription_Id__c,opli.Opportunity.Contact__c);
                    subProdMap.put(opli.Subscription_Id__c,opli.Product2Id);
                    opliMap.put(opli.Subscription_Id__c, opli);
                }
                if (subConMap.size()>0) {
                    for (Address__c add: [SELECT id, Shipping_City__c, Shipping_Street__c, Shipping_Country__c, Shipping_State_Province__c, Contact__c, Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c IN :subConMap.values()]) {
                        addMap.put(add.Contact__c, add);
                    } 
                    for (Card__c add: [SELECT id, Billing_City__c, Billing_Street__c, Billing_Country__c, Billing_State_Province__c, Contact__c, Billing_Zip_Postal_Code__c FROM Card__c WHERE Contact__c IN :subConMap.values()]) {
                        cardMap.put(add.Contact__c, add);
                    } 
                    for (Product2 prod: [SELECT id,Tax_Code__c FROM Product2 WHERE id IN : subProdMap.values() AND Tax_Code__c!=null]) {
                        taxCode.put(prod.Id, prod.Tax_Code__c);
                    }
                    for (String sub: subConMap.keySet()) {
                        String street = 'LadyBoss Weight Loss 10010 Indian School Rd. NE';
                        String state = 'NM';
                        String city = 'Albuquerque';
                        String country = 'US';
                        String zipcode = '87112';
                        if (addMap.containsKey(subConMap.get(sub))) {
                            street = addMap.get(subConMap.get(sub)).Shipping_Street__c;
                            state = addMap.get(subConMap.get(sub)).Shipping_State_Province__c;
                            city = addMap.get(subConMap.get(sub)).Shipping_City__c;
                            country = addMap.get(subConMap.get(sub)).Shipping_Country__c;
                            zipcode = addMap.get(subConMap.get(sub)).Shipping_Zip_Postal_Code__c;
                        }else if (cardMap.containsKey(subConMap.get(sub)) && cardMap.get(subConMap.get(sub)).Billing_Street__c != null && cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c != null && cardMap.get(subConMap.get(sub)).Billing_City__c != null && cardMap.get(subConMap.get(sub)).Billing_State_Province__c != null && cardMap.get(subConMap.get(sub)).Billing_Country__c != null) {
                            street =  cardMap.get(subConMap.get(sub)).Billing_Street__c;
                            state = cardMap.get(subConMap.get(sub)).Billing_State_Province__c;
                            city = cardMap.get(subConMap.get(sub)).Billing_City__c;
                            country = cardMap.get(subConMap.get(sub)).Billing_Country__c;
                            zipcode = cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c;
                        }
                        String codetax;
                        if (taxCode.get(subProdMap.get(sub)) == null) {
                            codetax = '99999';
                        }else {
                            codetax = taxCode.get(subProdMap.get(sub));
                        }
                        Decimal amount = opliMap.get(sub).TotalPrice;
                        if (opliMap.get(sub).Add_Tax_to_subscription__c) {  
                            TaxjarTaxCalculate.ratemethodForOppUpdate(street, city, state, country, zipcode, amount, 1, codetax,sub); 
                        }else {
                            Subscriptions.updateSubscriptionForOppUpdate(sub);
                        }
                    }
                }
            }
            if(oppIds.size() > 0)  {
                List<Opportunity> oppList = [SELECT id,Contact__c,contact__r.email FROM Opportunity WHERE id in : oppIds];
                List<Id> cntIds = new List<Id>();
                for(Opportunity opp : oppList){
                    if(opp.Contact__c != null){
                        cntIds.add(opp.Contact__c);
                        emailID.add(opp.Contact__r.email);
                    }
                }
                List<Case> cseList = [SELECT id,Status FROM Case WHERE ContactId in : cntIds and Type='Account Recovery' and Status!='Closed-AR-Expired-Subscription'];
                for(Case cs : cseList){
                    cs.Status = 'Closed-AR-Expired-Subscription';
                }
                if(cseList.size()>0){
                    update cseList;
                }
                
                List<Case> caseList = [select id ,UpdateCardStatus__c,Contact.email, Successful_Payment_DateTime__c,CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c from case where  IsFailed_Payment__c=true and (Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c!='') and ( Contact.Email!=null and  Contact.Email IN :emailID) and type='Account Recovery' and UpdateCardStatus__c!='Payment Successful' limit 10000 ];
                for (Case cse : caseList) {
                    cse.UpdateCardStatus__c = 'Payment Successful';
                }
                
                if (caseList.size()>0) {
                    update caseList;
                }
                
            }
            List<OpportunityLineItem> opliBalanceList = [SELECT id,Opportunity.Contact__c,Same_Balance_as_Before__c FROM OpportunityLineItem WHERE Id IN :oliListBalance];
            if (opliBalanceList.size()>0) {
                for (OpportunityLineItem opli : opliBalanceList) {
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    message.setTargetObjectId(opli.Opportunity.Contact__c);
                    message.setOrgWideEmailAddressId([SELECT Id FROM OrgWideEmailAddress WHERE Address = 'support@ladyboss.com' LIMIT 1].Id);
                    message.setTemplateID([SELECT Id FROM EmailTemplate WHERE DeveloperName ='Stripe_Balance_Applied' LIMIT 1].Id);
                    message.setWhatId(opli.id);
                    message.setSaveAsActivity(false); 
                    Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    if (results[0].success) {
                    System.debug('The email was sent successfully.');
                    } else{
                    System.debug('The email failed to send: ' + results[0].errors[0].message);
                    }
                    opli.Same_Balance_as_Before__c = false;
                }                
                update opliBalanceList;
            }
            
                if(Test.isRunningTest()){
                    System.debug(1/0);
                }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OliAfterUpdateTrigger','trigger',trigger.new[0].id,e) );
        }
    }
}