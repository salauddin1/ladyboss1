trigger StripeBalance on Stripe_Profile__c  (after update) {
    List<String> conIdList  = new List<String>();
    for (Stripe_Profile__c sp : Trigger.new) {
        if (sp.Balance__c != null && sp.Balance__c != Trigger.oldMap.get(sp.Id).Balance__c) {
            conIdList.add(sp.Customer__c);
        }
    }
    if (conIdList.size()>0) {
        List<Contact> conList = [SELECT id,Stripe_Balance__c, (SELECT id,Balance__c FROM Stripe_Profiles__r WHERE Balance__c != null) FROM Contact WHERE id IN:conIdList];
        for (Contact con : conList) {
            Decimal balance = 0;
            for (Stripe_Profile__c sp : con.Stripe_Profiles__r) {
                balance += sp.Balance__c;
            }
            con.Stripe_Balance__c = balance;
        }
        update conList;
    }
}