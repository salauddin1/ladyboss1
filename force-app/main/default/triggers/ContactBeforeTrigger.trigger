/*
    Developer Name : Tirth Patel
    Description : This trigger ensures that marketing cloud tag should always be null for amazon contacts.
*/
trigger ContactBeforeTrigger on Contact (before insert, before update) {
    for(Contact ct : Trigger.new){
        if(ct.Amazon_customer__c && ct.Marketing_Cloud_Tags__c != null && ct.Marketing_Cloud_Tags__c!=''){
            ct.addError('Amazon Customer can not have Marketing Cloud Tag');
        }
    }    
}