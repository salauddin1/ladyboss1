trigger OliInsertTrigger on OpportunityLineItem (after insert,after update) {
    System.debug('coming here');
    if(Trigger.isInsert){
        System.debug('Insert------');
    }else{
        System.debug('Update------');
    }
    Set<String> contemails = new Set<String>();
    List<OpportunityLineItem> oliList = [SELECT id,Subscription_Plan_ID__c,Opportunity.Contact__r.email FROM OpportunityLineItem WHERE id in : Trigger.new AND Opportunity.Contact__r.email!=null AND Subscription_Plan_ID__c!=null];
    for(OpportunityLineItem ol : Trigger.New){
        contemails.add(ol.Opportunity.Contact__r.email);
    }
    System.debug(contemails);
    List<OpportunityLineItem> oliAllList = [SELECT id,Status__c,Subscription_Plan_ID__c,Opportunity.Contact__c,Opportunity.Contact__r.email FROM OpportunityLineItem WHERE Status__c in ('active','trialing') and Opportunity.Contact__r.email in : contemails and Subscription_Plan_ID__c!=null];
    Map<String,Map<String,Integer>> contPlanCountMap = new Map<String,Map<String,Integer>>();
    for(OpportunityLineItem ol : oliAllList){
        if(!contPlanCountMap.containskey(ol.Opportunity.Contact__r.email)) {
            contPlanCountMap.put(ol.Opportunity.Contact__r.email,new Map<String,Integer>());
        }
        if(!contPlanCountMap.get(ol.Opportunity.Contact__r.email).containskey(ol.Subscription_Plan_ID__c)){
            contPlanCountMap.get(ol.Opportunity.Contact__r.email).put(ol.Subscription_Plan_ID__c,0);
        }else{
            Integer ct = contPlanCountMap.get(ol.Opportunity.Contact__r.email).get(ol.Subscription_Plan_ID__c)+1;
            contPlanCountMap.get(ol.Opportunity.Contact__r.email).put(ol.Subscription_Plan_ID__c,ct);
        }
    }
    System.debug('contPlanCountMap : '+contPlanCountMap);
    List<Case> caseList = new List<Case>();
    for(OpportunityLineItem oli : oliList){
        if(contPlanCountMap.get(oli.Opportunity.Contact__r.email)!=null){
            if(contPlanCountMap.get(oli.Opportunity.Contact__r.email).get(oli.Subscription_Plan_ID__c)>1){
                Case cs = new Case();
                cs.ContactId = oli.Opportunity.Contact__c;
                cs.Subject = 'Dupilcate subscription : '+oli.Opportunity.Contact__r.email;
                cs.Description = 'Stripe plan id : '+oli.Subscription_Plan_ID__c;
                cs.Status = 'open';
                caseList.add(cs);
            }
        }
    }
    insert caseList;
}