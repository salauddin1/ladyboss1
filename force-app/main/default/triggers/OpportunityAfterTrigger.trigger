trigger OpportunityAfterTrigger on Opportunity (after update) {
    Map<Id,Opportunity> TriggerNewMap = Trigger.newMap;
    Map<Id,Opportunity> TriggerOldMap = Trigger.oldMap;
    System.debug(TriggerNewMap);
    System.debug(TriggerOldMap);
    System.debug('Set of opportunity '+TriggerNewMap.keySet());
    
    List<Id> oppIds = new List<Id>();
    for(Id oppId:TriggerNewMap.keySet()){
        if(TriggerOldMap.get(oppId).Stripe_Charge_Id__c==null && TriggerNewMap.get(oppId).Stripe_Charge_Id__c!=null){
            oppIds.add(oppId);
        }
    }
    List<Payment__c> payList = new List<Payment__c>();
    
    for(Opportunity opps : [SELECT id,Stripe_Charge_Id__c,(SELECT id,OpportunityId,Total_Commissionable_amount__c,Product2Id FROM OpportunityLineItems) FROM Opportunity where id in :oppIds]){
        for(OpportunityLineItem oli : opps.OpportunityLineItems){
            Payment__c pay = new Payment__c();
            pay.Opportunity__c = oli.OpportunityId;
            pay.Status__c = 'Succeeded';
            pay.Amount__c = oli.Total_Commissionable_amount__c;
            pay.Paid__c = true;
            pay.Product__c = oli.Product2Id;
            payList.add(pay);
        }
    }
        if(payList.size()>0){
            System.debug(payList);
            try{
                insert payList;
            }catch(Exception e){
                System.debug(e.getMessage());
            }
    }
}