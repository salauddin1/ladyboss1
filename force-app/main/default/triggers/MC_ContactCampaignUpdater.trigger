trigger MC_ContactCampaignUpdater on CampaignMember (after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true && CampaignMemberStopRecursionFlag.StopRecursionFlag == false){
        
        set<Id> idset1 = new set<id>();
        for(CampaignMember cm : Trigger.New) {
            idset1.add(cm.id);
        }
        IF(idset1.size() > 0){
            CampaignMemberStopRecursionFlag.StopRecursionFlag = true;
            MC_ContactCampaignUpdaterHdlr.updateContact(idset1 );
            ClickFunnelContactCampaignHndlr.checkListClickFunnel(idset1);
        }
    }
}