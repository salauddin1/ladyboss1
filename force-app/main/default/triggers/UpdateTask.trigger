Trigger UpdateTask on Task(before insert, before update,after insert,after update){
    if(ContactTriggerFlag.isContactBatchRunning != true  ){
        if(trigger.isbefore && (trigger.isInsert || trigger.isUpdate )){
            Map<Id, List<Task>> whoIdsMap = new Map<Id, List<Task>>();
            
            for(Task t : trigger.new){
                
                if(t.WhoId != null){
                    
                    if(!whoIdsMap.containsKey(t.WhoId)){
                        
                        List<Task> temp = new List<Task>();
                        
                        temp.add(t);
                        
                        whoIdsMap.put(t.WhoId, temp);
                    }else{
                        
                        whoIdsMap.get(t.WhoId).add(t);
                    }
                }
            }
            
            
            for(Contact con : [Select Id, Name,email,phone  from Contact where Id in :whoIdsMap.keySet()]){
                
                for(Task t :whoIdsMap.get(con.Id)){
                    t.email__c = con.email;
                    t.phone__c = con.phone;
                }
            }
            
        }
        Set<Id> contactId=new Set<Id>();
        for(Task tsk:Trigger.new){
            if(tsk.WhoId!=null){
            contactId.add(tsk.WhoId);
            }
        }
        try{
            if(!contactId.isEmpty()) {
                Map<Id, Contact> conMap = new Map<Id, Contact>([select id, DoNotCall, Phone_State_DNC__c, Phone_National_DNC__c, Mobile_State_DNC__c, Other_Phone_National_DNC__c,  Other_Phone_State_DNC__c from Contact where id IN : contactId]);
                if(!conMap.isEmpty()){
                    if(trigger.isAfter && trigger.isInsert){
                        for(Task tsk : trigger.new){
                            if(tsk.Stop_Correspondence__c == true && tsk.WhoId != null){
                                conMap.get(tsk.WhoId).DoNotCall=true;
                                conMap.get(tsk.WhoId).Phone_State_DNC__c=true;
                                conMap.get(tsk.WhoId).Phone_National_DNC__c=true;
                                conMap.get(tsk.WhoId).Mobile_State_DNC__c=true;
                                conMap.get(tsk.WhoId).Other_Phone_National_DNC__c=true;
                                conMap.get(tsk.WhoId).Other_Phone_State_DNC__c=true;
                            }
                            else if(tsk.Stop_Correspondence__c == false && tsk.WhoId != null){
                                conMap.get(tsk.WhoId).DoNotCall=false;
                                conMap.get(tsk.WhoId).Phone_State_DNC__c=false;
                                conMap.get(tsk.WhoId).Phone_National_DNC__c=false;
                                conMap.get(tsk.WhoId).Mobile_State_DNC__c=false;
                                conMap.get(tsk.WhoId).Other_Phone_National_DNC__c=false;
                                conMap.get(tsk.WhoId).Other_Phone_State_DNC__c=false;
                            }    
                        }     
                        update conMap.values();
                    }
                    if(Trigger.isAfter && Trigger.isUpdate){
                        for(Task tsk : trigger.new){
                            if(tsk.Stop_Correspondence__c == true && tsk.WhoId != null){
                                
                                conMap.get(tsk.WhoId).DoNotCall=true;
                                conMap.get(tsk.WhoId).Phone_State_DNC__c=true;
                                conMap.get(tsk.WhoId).Phone_National_DNC__c=true;
                                conMap.get(tsk.WhoId).Mobile_State_DNC__c=true;
                                conMap.get(tsk.WhoId).Other_Phone_National_DNC__c=true;
                                conMap.get(tsk.WhoId).Other_Phone_State_DNC__c=true;
                            }
                            else if(tsk.Stop_Correspondence__c == false && tsk.WhoId != null){
                                conMap.get(tsk.WhoId).DoNotCall=false;
                                conMap.get(tsk.WhoId).Phone_State_DNC__c=false;
                                conMap.get(tsk.WhoId).Phone_National_DNC__c=false;
                                conMap.get(tsk.WhoId).Mobile_State_DNC__c=false;
                                conMap.get(tsk.WhoId).Other_Phone_National_DNC__c=false;
                                conMap.get(tsk.WhoId).Other_Phone_State_DNC__c=false;
                            }    
                        }
                        update conMap.values();
                    }
                }
            }
        }
        catch(System.DmlException e){}
    }
}