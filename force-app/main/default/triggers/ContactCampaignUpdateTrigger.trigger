trigger ContactCampaignUpdateTrigger on Contact (after insert,before update,after update) {
    if(!ContactTriggerFlag.isContactBatchRunning)  { 
        if(trigger.isBefore){ 
            if(trigger.isUpdate){
                if(!system.isBatch()){
                    Map<Id,Contact> newValMap = new Map<Id,Contact>();
                    Map<Id,Contact> oldValMap = new Map<Id,Contact>();
                    List<Contact> newList = new List<Contact>();
                    newList.add(trigger.new[0]);
                    newValMap.put(trigger.new[0].Id, trigger.newMap.get(trigger.new[0].Id));
                    oldValMap.put(trigger.new[0].Id, trigger.oldMap.get(trigger.new[0].Id));
                    if(trigger.newMap.get(trigger.new[0].Id).Product__c != null || trigger.newMap.get(trigger.new[0].Id).Website_Highest_Product__c != null || trigger.newMap.get(trigger.new[0].Id).Potential_Buyer__c != null) {
                        if(trigger.newMap.get(trigger.new[0].Id).Product__c != trigger.oldMap.get(trigger.new[0].Id).Product__c || trigger.newMap.get(trigger.new[0].Id).Potential_Buyer__c != trigger.oldMap.get(trigger.new[0].Id).Potential_Buyer__c) {
                            if(trigger.newMap.get(trigger.new[0].Id).Product__c != null && trigger.newMap.get(trigger.new[0].Id).Product__c != trigger.oldMap.get(trigger.new[0].Id).Product__c){
                                ContactCampaignUpdateTrigger_Handler.movetoPipelineCampaigns(trigger.NewMap, trigger.OldMap); 
                            }                              
                        }
                        if(trigger.newMap.get(trigger.new[0].Id).Product__c != null || trigger.newMap.get(trigger.new[0].Id).Website_Highest_Product__c != null || trigger.newMap.get(trigger.new[0].Id).Potential_Buyer__c != null) {
                            if(trigger.newMap.get(trigger.new[0].Id).Website_Products__c != null && trigger.newMap.get(trigger.new[0].Id).Website_Products__c != trigger.oldMap.get(trigger.new[0].Id).Website_Products__c){
                                ContactCampaignUpdateTrigger_Handler.doBeforeUpdate(trigger.New, trigger.OldMap); 
                            }    
                        }
                    }
                } 
            }
        }
        
        
        if(trigger.isBefore && trigger.isUpdate && !System.isBatch()){
            for(Contact con : trigger.New){
                if(con.MobilePhone!=null && con.DoNotCall==true && trigger.newMap.get(con.id).DoNotCall!= trigger.oldMap.get(con.id).DoNotCall){
                    Five9Helper.five9DNC(con.Id,'Mobile','addToDNC');
                }
                if(con.Phone!=null && con.Phone_National_DNC__c ==true && trigger.newMap.get(con.id).Phone_National_DNC__c!= trigger.oldMap.get(con.id).Phone_National_DNC__c){
                    Five9Helper.five9DNC(con.Id,'Phone','addToDNC');
                }
                if(con.OtherPhone!=null && con.Other_Phone_National_DNC__c ==true && trigger.newMap.get(con.id).Other_Phone_National_DNC__c!= trigger.oldMap.get(con.id).Other_Phone_National_DNC__c){
                    Five9Helper.five9DNC(con.Id,'OtherPhone','addToDNC');
                } 
                
                if(con.MobilePhone!=null && con.DoNotCall==false && trigger.newMap.get(con.id).DoNotCall!= trigger.oldMap.get(con.id).DoNotCall){
                    Five9Helper.five9DNC(con.Id,'Mobile','removeFromDNC');
                }
                if(con.Phone!=null && con.Phone_National_DNC__c ==false && trigger.newMap.get(con.id).Phone_National_DNC__c!= trigger.oldMap.get(con.id).Phone_National_DNC__c){
                    Five9Helper.five9DNC(con.Id,'Phone','removeFromDNC');
                }
                if(con.OtherPhone!=null && con.Other_Phone_National_DNC__c ==false && trigger.newMap.get(con.id).Other_Phone_National_DNC__c!= trigger.oldMap.get(con.id).Other_Phone_National_DNC__c){
                    Five9Helper.five9DNC(con.Id,'OtherPhone','removeFromDNC');
                }   
                
            }    
        }
        
        if(trigger.isAfter){
            if(trigger.isInsert){
                if(!system.isBatch()){
                    Map<Id,Contact> newValMap = new Map<Id,Contact>();
                    List<Contact> newList = new List<Contact>();
                    newList.add(trigger.new[0]);
                    newValMap.put(trigger.new[0].Id, trigger.newMap.get(trigger.new[0].Id));
                    ContactCampaignUpdateTrigger_Handler.doAfterInsert(trigger.New);  
                    //To link the contact and card if card gets created first and contact later
                    //CardContactLinkHandler.linkcardContact(trigger.New);  
                    //TO link contact to the five9 list if contact is created
                    ContactFIve9ItemUpdateTrigger_Handler.doAfterInsert(trigger.New); 
                    if(newList[0].Product__c!=null || newList[0].Potential_Buyer__c!=null){
                        Five9Master.addtoMasterListI(newList,newValMap);
                    }
                }    
            }
        }   
    }        
}