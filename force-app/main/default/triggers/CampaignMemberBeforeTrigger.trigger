/*
    Developer Name : Tirth Patel
    Description : This trigger shows error if amazon contact is added as campaign member.
*/
trigger CampaignMemberBeforeTrigger on CampaignMember (before insert) {
    List<String> cntIdList = new List<String>();
    for(CampaignMember cm : Trigger.new){
        if(cm.ContactId != null){
        cntIdList.add(cm.ContactId);
        }
    }
    Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT id,Amazon_customer__c FROM Contact WHERE id in : cntIdList]);
    for(CampaignMember cm : Trigger.new){
        if(contactMap.get(cm.ContactId) != null && contactMap.get(cm.ContactId).Amazon_customer__c){
            cm.addError('Amazon customer can not be added as campaign member');
        }
    }  
}