trigger PaymentToCommisionTrigger on Payment__c (after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true && PaymentToCommissionTriggerFalg.runPayTOcomTrigger == true ){
        List<Payment__c> commissionPayList = new List<Payment__c>();
        List<Payment__c> refundPayList = new List<Payment__c>();
        if(Trigger.isInsert){
            System.debug('Trigger.new : '+Trigger.new);
            for(Payment__c pay : Trigger.new){
                if(pay.amount__c > 0){
                    commissionPayList.add(pay);
                }else if (pay.amount__c < 0){
                    refundPayList.add(pay);
                }
            }
            if(commissionPayList.size() > 0){
                try{
                    PaymentToComissionTriggerHandler.comission(commissionPayList);
                    if(Test.isRunningTest()){
                        integer i = 1/0;
                    }
                }catch(Exception e){
                    System.debug(e.getMessage());
                    ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error('PaymentToCommisionTrigger','trigger',String.valueOf(commissionPayList.get(0).id),e) );
                }
            }
            if(refundPayList.size() > 0){
                try{
                    PaymentToComissionTriggerHandler.refund(refundPayList);
                    if(Test.isRunningTest()){
                        integer i = 1/0;
                    }
                }catch(Exception e){
                    System.debug(e.getMessage()+'  '+e.getLineNumber()+' '+e.getStackTraceString());
                    ApexDebugLog apex=new ApexDebugLog();
                        apex.createLog(
                    new ApexDebugLog.Error('PaymentToCommisionTrigger','trigger',String.valueOf(refundPayList.get(0).id),e) );
                }
            }
        }
    }
}