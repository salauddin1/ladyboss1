trigger CardTrigger on Card__c (after insert, after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true){
        //Get related contact data 
        Set<Id> contactIds= new Set<Id>();
        for(Card__c card:trigger.new){
            contactIds.add(card.Contact__c);    
        }
        Map<Id,Contact> mapContact = new Map<Id,Contact>();
        for(Contact con:[select id,Stripe_Customer_Id__c from Contact where id in :contactIds]){
            mapContact.put(con.Id,Con);   
        }
        if(trigger.isInsert && trigger.isAfter){
            for(Card__c card:trigger.new){
                if(card.isBatchCard__c!=true && mapContact!=null && mapContact.keyset().contains(card.Contact__c)){
                    if(card.Token__c != null){
                        //StripeIntegrationHandlerChanges.cardWithTokenStripeHandler(card.Id);
                    }else{
                        StripeIntegrationHandler.cardStripeHandler(card.Id);
                    }
                }
                if(card.Contact__c == null){
                    WebhookTableHandler.cardHandler(card);
                }
            }
        }
        
        if(trigger.isUpdate && trigger.isAfter && !system.isFuture()){
            for(Card__c card:trigger.new){
                if(card.isBatchCard__c!=true && mapContact!=null && mapContact.keyset().contains(card.Contact__c) && (card.Error_Message__c == null || card.Error_Message__c == '') && (string.isBlank(card.Stripe_Card_Id__c) || card.Stripe_Card_Id__c ==null)){
                    if(card.Token__c != null){
                        //StripeIntegrationHandlerChanges.cardWithTokenStripeHandler(card.Id);
                    }else{
                        StripeIntegrationHandler.cardStripeHandler(card.Id);
                    }
                }
            }
        }
    }
    
}