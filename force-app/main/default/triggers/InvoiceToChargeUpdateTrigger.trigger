trigger InvoiceToChargeUpdateTrigger on OpportunityLineItem (after insert,after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true && Trigger.isInsert){
        system.debug('here for oli insert---'+Trigger.new);
        InvoiceToChargeUpdateHandler.OliUpdater(Trigger.new);
        InvoiceToChargeUpdateHandler.updateStartDate(Trigger.new);
    }
    if(ContactTriggerFlag.isContactBatchRunning != true && Trigger.isUpdate && !OliUpdateTriggerFlag.stopTrigger){
        OliUpdateTriggerFlag.stopTrigger = true;
        InvoiceToChargeUpdateHandler.updateStartDate(Trigger.new);
    }
}