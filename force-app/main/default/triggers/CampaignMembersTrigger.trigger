/*
 Developer Name : Tirth Patel
 Description : Merged CampaignMemberBeforeTrigger,DeleteDuplicateLivechallengeMember,CampaignMemberTrigger and MC_ContactCampaignUpdater triggers.
 Test Classes are CampaignMemberBeforeTriggerTest,CampaignMemberTrigger_Test and MC_ContactCampaignUpdater_Test.
*/
trigger CampaignMembersTrigger on CampaignMember (before insert, after insert,before update,after update,Before Delete) {
    if(!ContactTriggerFlag.isContactBatchRunning){
        if(Trigger.isBefore && Trigger.isInsert){
            CampaignMembersTriggerHandler.checkAmazonCustomer(Trigger.new);
        }
        if(Trigger.IsAfter && Trigger.isInsert ){
            if(!CampaignMemberStopRecursionFlag.StopRecursionFlag && System.IsBatch() == false && System.isFuture() == false){
            CampaignMembersTriggerHandler.MC_ContactCampaignUpdater(Trigger.new);
            }
            CoachingAppliedBookShowCampaign.UpdateFlagmanullyAddtoCampaign(Trigger.new);
            CampaignMembersTriggerHandler.DeleteDuplicateLivechallengeMember(Trigger.new);
            CampaignMembersTriggerHandler.campaignOnOpp(Trigger.new);
            CampaignMembersTriggerHandler.sendSms(Trigger.new);
        }
         
        
         if(Trigger.isBefore && Trigger.isDelete){
               if(ContactTriggerFlag.isContactBatchRunning != true && CampaignMemberStopRecursionFlag.StopRecursionFlag1!=true ){
                    DeleteCampaignTriggerHandler.handlerMethod(Trigger.old);
               }
        }
    }
}