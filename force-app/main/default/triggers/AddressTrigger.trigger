trigger AddressTrigger on Address__c (before insert , before update,after insert,after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true){
        Set<Id> contactId = new Set<Id>();
        for(Address__c  address:trigger.new){
            if(address.Primary__c == true){
                contactId.add(address.Contact__c);
            }
        }
        
        if (Trigger.isInsert || Trigger.isupdate) {
            if (Trigger.isAfter) {
                AddressTriggerHandler.primaryAddress(trigger.new);
            }
        }
        
        if (Trigger.isAfter&&(Trigger.isUpdate || Trigger.isInsert)) {
            for(Address__c  address:trigger.new){
                if(address.Primary__c==true){
                    SubscriptionAddressHandler.setTaxPercentage(address);
                }
                
            }
        }
        
        
        if (Trigger.isAfter&&Trigger.isInsert) {
            for(Address__c  address:trigger.new){
                if(address.Contact__c==null){
                    WebhookTableHandler.addressHandler(address);
                }
            }        
        }
    }
}