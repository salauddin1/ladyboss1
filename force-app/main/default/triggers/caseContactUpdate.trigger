trigger caseContactUpdate on Case (after insert) {
    List<String> emailList = new List<String>();
    List<Id> caseIdsList = new List<Id>();
    List<Case> cseList = new List<Case>();
    List<String> five9mailList = new List<String>{'noreply@five9.com','user-noreply@five9.com','voicemail-noreply@five9.com','voicemail1@ladyboss.com','voicemail2@ladyboss.com','voicemail3@ladyboss.com','voicemail4@ladyboss.com','tirthtest@gmail.com'};
        try{
            for(Case cs : Trigger.new){
                if(cs.suppliedEmail!=null && cs.ContactId==null && !five9mailList.contains(cs.suppliedEmail.toLowerCase())){
                    cseList.add(cs);
                    emailList.add(cs.suppliedEmail);
                    caseIdsList.add(cs.Id);
                }
            }
            List<Contact> existCntList = [SELECT id,email FROM Contact WHERE email in : emailList];
            Map<String,Contact> emailContMap = new Map<String,Contact>();
            List<String> existEmailList = new List<String>();
            for(Contact ct : existCntList){
                existEmailList.add(ct.email.toLowerCase());
            }
            List<Contact> contInsertList = new List<Contact>();
            for(Case cs : cseList){
                if(!existEmailList.contains(cs.suppliedEmail.toLowerCase())){
                    
                    Contact ct = new Contact();
                    if(cs.SuppliedName != null ){
                        Integer ind = cs.SuppliedName.indexOf(' ');
                        if(ind!=-1){
                            ct.FirstName = cs.SuppliedName.substring(0,ind);
                            ct.LastName = cs.SuppliedName.substring(ind+1,cs.SuppliedName.length());
                        }else{ 
                            ct.LastName = cs.SuppliedName;
                        }
                    }else{
                        ct.LastName = 'Not Available';
                    }
                    ct.email = cs.SuppliedEmail;
                    if(cs.SuppliedPhone!=null){
                        ct.Phone = cs.SuppliedPhone;
                    }
                    contInsertList.add(ct);
                }
            }
            Database.Insert(contInsertList,false);
            existCntList = [SELECT id,email FROM Contact WHERE email in : emailList];
            for(Contact cnt : existCntList){
                emailContMap.put(cnt.Email.toLowerCase(), cnt);
            }
            cseList = [SELECT id,SuppliedEmail FROM CASE WHERE id in : caseIdsList];
            for(Case cs : cseList){
                if(emailContMap.get(cs.SuppliedEmail.toLowerCase())!=null){
                	cs.ContactId = emailContMap.get(cs.SuppliedEmail.toLowerCase()).Id;
                }
            }
            Database.update(cseList,false);
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('caseContactUpdate','trigger',String.valueOf(caseIdsList.get(0)),e));
        }
}