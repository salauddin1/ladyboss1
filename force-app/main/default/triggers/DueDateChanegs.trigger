trigger DueDateChanegs on Case (before insert,before update) {
    List<case> updateCase = new List<case>();
   // AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfCase' );
      List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,isDueDateChange__c,Index__c,Number_Of_Days_To_Move__c from MoveCaseDueDate__c];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        
     if(trigger.isbefore  && trigger.isInsert &&  ChangeCaseDueDateControllerV4.RunOnce){
        for(Case cs: Trigger.new) {
            if(cs.DueDate__c!=null && (cs.type=='Account Recovery' || cs.Owner.Name=='Account Recovery')){
            system.debug('--cs--'+cs);
            system.debug('--indexMap.size()--'+indexMap.size());
            for(Integer i=1; i <= indexMap.size();i++){
                system.debug('--field--'+indexMap.get(i).Field_Api_Name__c);
                system.debug('--val--'+cs.get(indexMap.get(i).Field_Api_Name__c));
                if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && cs.get(indexMap.get(i).Field_Api_Name__c) == true){
          cs.put(indexMap.get(i).Field_Api_Name__c,true);
                    cs.DueDate__c = cs.DueDate__c + (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                    break;
                   
                }
            }
            
                }
       // updateCase.add(cs);
    }
    }
    if(trigger.isbefore  && trigger.isUpdate &&  ChangeCaseDueDateControllerV4.RunOnce){
        List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,isDueDateChange__c,Index__c,Number_Of_Days_To_Move__c from MoveCaseDueDate__c];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        for(Case cs: Trigger.new) {
        
            
             if(cs.DueDate__c!=null && (cs.type=='Account Recovery' || cs.Owner.Name=='Account Recovery')){
            system.debug('--cs--'+cs);
            system.debug('--indexMap.size()--'+indexMap.size());
            for(Integer i=1; i <= indexMap.size();i++){
                system.debug('--field--'+indexMap.get(i).Field_Api_Name__c);
                system.debug('--val--'+cs.get(indexMap.get(i).Field_Api_Name__c));
                if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && cs.get(indexMap.get(i).Field_Api_Name__c) == true &&  cs.get(indexMap.get(i).Field_Api_Name__c)!= trigger.oldMap.get(cs.Id).get(indexMap.get(i).Field_Api_Name__c)){
          //cs.put(indexMap.get(i).Field_Api_Name__c,true);
                   if(indexMap.get(i).isDueDateChange__c==true){
                    cs.DueDate__c = cs.DueDate__c + (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                    break;
                    }
                   
                }
                else if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && cs.get(indexMap.get(i).Field_Api_Name__c) == false &&  cs.get(indexMap.get(i).Field_Api_Name__c)!= trigger.oldMap.get(cs.Id).get(indexMap.get(i).Field_Api_Name__c)){
                if(indexMap.get(i).isDueDateChange__c==true){
                    cs.DueDate__c = cs.DueDate__c - (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                    break;
                    }
                }
            }
             }
        //updateCase.add(cs);
    }
    }
    //update updateCase;
}