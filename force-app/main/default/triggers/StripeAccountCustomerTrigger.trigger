trigger StripeAccountCustomerTrigger on Account (before insert, before update,after insert, after update) {
    
    if(ContactTriggerFlag.isContactBatchRunning != true && ApexUtil.isTriggerInvoked==false){
        if(trigger.isBefore){
            if(trigger.isInsert){
                for(Account acc : trigger.new){
                    acc.DML_From_Salesforce__c = true;
                }
            }
            
            if(trigger.isUpdate){
                for(Account acc : trigger.new){
                    if(acc.Name!=trigger.oldMap.get(acc.Id).Name 
                       || acc.Description!=trigger.oldMap.get(acc.Id).Description 
                       || acc.Phone!=trigger.oldMap.get(acc.Id).Phone 
                       || acc.Email__c!=trigger.oldMap.get(acc.Id).Email__c
                       || acc.ShippingStreet!=trigger.oldMap.get(acc.Id).ShippingStreet 
                       || acc.ShippingState!=trigger.oldMap.get(acc.Id).ShippingState
                       || acc.ShippingCity!=trigger.oldMap.get(acc.Id).ShippingCity
                       || acc.ShippingPostalCode!=trigger.oldMap.get(acc.Id).ShippingPostalCode
                       || acc.ShippingCountry!=trigger.oldMap.get(acc.Id).ShippingCountry
                       || acc.Exp_Month__c!=trigger.oldMap.get(acc.Id).Exp_Month__c
                       || acc.Exp_Year__c!=trigger.oldMap.get(acc.Id).Exp_Year__c
                       || acc.Credit_Card_Number__c!=trigger.oldMap.get(acc.Id).Credit_Card_Number__c
                       || acc.Cvc__c!=trigger.oldMap.get(acc.Id).Cvc__c
                      )
                    {
                        acc.DML_From_Salesforce__c = true;
                    }
                }
            }
        }
        
        if(trigger.isAfter){
            Map<String,String> mapCustomer;
            String firstname;
            String lastname;
            if(trigger.isInsert){
                for(Account acc : trigger.new){
                    mapCustomer = new Map<String,String>();
                    mapCustomer.put('metadata[name]',acc.Name);
                    if(acc.Name.indexOf(' ')>0){
                        firstname = acc.Name.substring(0,acc.Name.indexOf(' '));
                        lastname = acc.Name.substring(acc.Name.indexOf(' '));
                    }else{
                        firstname = acc.Name;
                        lastname = '';
                    }
                    mapCustomer.put('metadata[first_name]',firstname);
                    mapCustomer.put('metadata[last_name]',lastname);
                    if(acc.Phone!='' && acc.Phone!=null) mapCustomer.put('metadata[phone]',acc.Phone); else mapCustomer.put('metadata[phone]','');
                    mapCustomer.put('metadata[salesforce_id]',acc.Id);
                    if(acc.Description!='' && acc.Description!=null) mapCustomer.put('description',acc.Description); else mapCustomer.put('description','');
                    if(acc.Email__c!='' && acc.Email__c!=null) mapCustomer.put('email',acc.Email__c); else mapCustomer.put('email','');
                    mapCustomer.put('shipping[name]',acc.Name);
                    if(acc.ShippingStreet!='' && acc.ShippingStreet!=null) mapCustomer.put('shipping[address][line1]',acc.ShippingStreet); else mapCustomer.put('shipping[address][line1]','');
                    if(acc.ShippingState!='' && acc.ShippingState!=null) mapCustomer.put('shipping[address][state]',acc.ShippingState); else mapCustomer.put('shipping[address][state]','');
                    if(acc.ShippingCity!='' && acc.ShippingCity!=null) mapCustomer.put('shipping[address][city]',acc.ShippingCity); else mapCustomer.put('shipping[address][city]','');
                    if(acc.ShippingPostalCode!='' && acc.ShippingPostalCode!=null) mapCustomer.put('shipping[address][postal_code]',acc.ShippingPostalCode); else mapCustomer.put('shipping[address][postal_code]','');
                    if(acc.ShippingCountry!='' && acc.ShippingCountry!=null) mapCustomer.put('shipping[address][country]',acc.ShippingCountry); else mapCustomer.put('shipping[address][country]','');
                    //if(!Test.isRunningTest()) StripeConnection.HttpSend('customers','',acc.Id, mapCustomer);
                }
            }
            
            if(trigger.isUpdate){
                for(Account acc : trigger.new){
                    if(acc.Name!=trigger.oldMap.get(acc.Id).Name 
                       || acc.Description!=trigger.oldMap.get(acc.Id).Description 
                       || acc.Phone!=trigger.oldMap.get(acc.Id).Phone 
                       || acc.Email__c!=trigger.oldMap.get(acc.Id).Email__c
                       || acc.ShippingStreet!=trigger.oldMap.get(acc.Id).ShippingStreet 
                       || acc.ShippingState!=trigger.oldMap.get(acc.Id).ShippingState
                       || acc.ShippingCity!=trigger.oldMap.get(acc.Id).ShippingCity
                       || acc.ShippingPostalCode!=trigger.oldMap.get(acc.Id).ShippingPostalCode
                       || acc.ShippingCountry!=trigger.oldMap.get(acc.Id).ShippingCountry
                       || acc.Exp_Month__c!=trigger.oldMap.get(acc.Id).Exp_Month__c
                       || acc.Exp_Year__c!=trigger.oldMap.get(acc.Id).Exp_Year__c
                       || acc.Credit_Card_Number__c!=trigger.oldMap.get(acc.Id).Credit_Card_Number__c
                       || acc.Cvc__c!=trigger.oldMap.get(acc.Id).Cvc__c
                      ){
                        mapCustomer = new Map<String,String>();
                        mapCustomer.put('metadata[name]',acc.Name);
                        if(acc.Name.indexOf(' ')>0){
                            firstname = acc.Name.substring(0,acc.Name.indexOf(' '));
                            lastname = acc.Name.substring(acc.Name.indexOf(' '));
                        }else{
                            firstname = acc.Name;
                            lastname = '';
                        }
                        mapCustomer.put('metadata[first_name]',firstname);
                        mapCustomer.put('metadata[last_name]',lastname);
                        if(acc.Phone!='' && acc.Phone!=null) mapCustomer.put('metadata[phone]',acc.Phone); else mapCustomer.put('metadata[phone]','');
                        mapCustomer.put('metadata[salesforce_id]',acc.Id);
                        if(acc.Description!='' && acc.Description!=null) mapCustomer.put('description',acc.Description); else mapCustomer.put('description','');
                        if(acc.Email__c!='' && acc.Email__c!=null) mapCustomer.put('email',acc.Email__c); else mapCustomer.put('email','');
                        mapCustomer.put('shipping[name]',acc.Name);
                        if(acc.ShippingStreet!='' && acc.ShippingStreet!=null) mapCustomer.put('shipping[address][line1]',acc.ShippingStreet); else mapCustomer.put('shipping[address][line1]','');
                        if(acc.ShippingState!='' && acc.ShippingState!=null) mapCustomer.put('shipping[address][state]',acc.ShippingState); else mapCustomer.put('shipping[address][state]','');
                        if(acc.ShippingCity!='' && acc.ShippingCity!=null) mapCustomer.put('shipping[address][city]',acc.ShippingCity); else mapCustomer.put('shipping[address][city]','');
                        if(acc.ShippingPostalCode!='' && acc.ShippingPostalCode!=null) mapCustomer.put('shipping[address][postal_code]',acc.ShippingPostalCode); else mapCustomer.put('shipping[address][postal_code]','');
                        if(acc.ShippingCountry!='' && acc.ShippingCountry!=null) mapCustomer.put('shipping[address][country]',acc.ShippingCountry); else mapCustomer.put('shipping[address][country]','');
                        //if(!Test.isRunningTest()) StripeConnection.HttpSend('customers',acc.External_ID__c,acc.Id, mapCustomer);
                    }
                }
            }
        }
    }
}