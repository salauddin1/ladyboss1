//Trigger for create the camapaign member if the Marketing_Cloud_Tags__c has the value
trigger MC_CampaignGenearator on Contact (after insert,after update) {
    if(!ContactTriggerFlag.isContactBatchRunning)  { 
        try{
            set<string> tempSet = new set<string>();
            set<id> conIdset= new set<id>();
            List<Contact>  conListUpdate = new List<Contact>();
            //On insert if we add tag
            if(trigger.isInsert) {
                for(contact c: Trigger.New){
                    
                    if(c.Marketing_Cloud_Tags__c!=null){
                        conIdset.add(c.id);
                        List<String> temp= c.Marketing_Cloud_Tags__c.split(';');
                        for(string str :temp){
                            tempSet.add(str);
                        }
                    }
                }
            }
            //On update if we add tag
            if(Trigger.isUpdate){
                for(contact c: Trigger.New){
                    contact  oldOpp= Trigger.oldMap.get(c.Id);
                    
                    if(c.Marketing_Cloud_Tags__c!=null && oldOpp.Marketing_Cloud_Tags__c!=c.Marketing_Cloud_Tags__c){
                        conIdset.add(c.id);
                        List<String> temp= c.Marketing_Cloud_Tags__c.split(';');
                        for(string str :temp){
                            tempSet.add(str);
                        }
                    }
                }
                
            }
            
            
            System.debug(tempSet);
            //To check only for the picklist value
            Schema.DescribeFieldResult field = Contact.Marketing_Cloud_Tags__c.getDescribe();
            Set<String> myPicklist = new Set<String>();
            for (Schema.PicklistEntry f : field.getPicklistValues()){
                myPicklist.add(f.getLabel());
                
            }
            
            map<string,string> campMap = new map<string,string>();
            List<campaign> lstcamp = [select id,name from campaign where name IN : tempSet ];
            Set<String> custName = new set<String> ();
            for(campaign c: lstcamp) {
                campMap.put(c.name,c.id);
                custName.add(c.name);
            }
            
            //Check for existing camp member
            List<CampaignMember > campList = [SELECT Id, CampaignId,Campaign.Name, ContactId, Status, Name FROM CampaignMember where ContactId!=null and ContactId IN : conIdset];
            Map<id,Set<String>> campMapSet = new Map<id,Set<String>>();
            set<id> idset= new set<id>();
            for(CampaignMember cm : campList) {
                
                idset.add(cm.ContactId);
                if(!campMapSet.containsKey(cm.ContactId)){
                    Set<String> cList = new Set<String>();
                    cList.add(cm.Campaign.Name);
                    campMapSet.put(cm.ContactId,cList);
                }else{
                    Set<String> cList = campMapSet.get(cm.ContactId);
                    cList.add(cm.Campaign.Name);
                    campMapSet.put(cm.ContactId,cList);
                }
            }
            
            
            List<CampaignMember > cmList = new List<CampaignMember > ();
            //To get only selected markektin tags
            List<Contact> conList = [SELECT Id,Marketing_Cloud_Tags__c FROM Contact where ID IN : conIdset];
            for(contact c: conList ){
                Set<String> idSetCampMember = new  Set<String> ();
                Map<String,String> mapContainCamp= new  Map<String,String> ();
                if(campMapSet.ContainsKey(c.id)){
                    idSetCampMember  =campMapSet.get(c.id);
                    for(String st : idSetCampMember  ){
                        mapContainCamp.put(st,st);
                    }
                }
                if(c.Marketing_Cloud_Tags__c!=null){
                    List<String> temp= new List<String>();
                    temp= c.Marketing_Cloud_Tags__c.split(';');
                    for(string str :temp){
                        if(myPicklist.size()> 0){
                            if( campMap.ContainsKey(str) && myPicklist.Contains(str) && !mapContainCamp.ContainsKey(str)) {
                                CampaignMember newCM = new CampaignMember(CampaignId = campMap.get(str),ContactId = c.Id,status = 'Sent' );
                                cmList.add(newCM); 
                                
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            if(cmList.size() > 0  ) {
                //Insert the camp member
                database.insert (cmList, false);
            }
            if(test.isRunningtest()){
                integer a=10/0;
                
            }
        }
        catch(Exception e){
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('MC_CampaignGenearator ','On Insert and update','Trigger for campaign', e));
        }
        
    } 
    
    
    
    
}