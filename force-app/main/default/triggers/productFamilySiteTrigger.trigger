trigger productFamilySiteTrigger on Product2 (after update,after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true  ){
        Map<id,Product2> prodMap = new Map<id,Product2>();
        List<Product2> prodListToBeUpdated = new List<Product2>();
        Map<String,String> prodNameWithSite = new Map<String,String>();
        
        if(RecursiveTriggerHandler.isFirstTime){
            RecursiveTriggerHandler.isFirstTime = false;
            for(Product2 pd : Trigger.new){
                if(Trigger.isUpdate){
                    Product2 oldProd = Trigger.oldMap.get(pd.Id);
                    if(oldProd.sitePopUp__c != pd.sitePopUp__c){
                        prodNameWithSite.put(oldProd.Family, pd.sitePopUp__c);
                    }
                }else if(Trigger.isInsert){
                    if(pd.sitePopUp__c != null){
                        prodNameWithSite.put(pd.Family, pd.sitePopUp__c);
                    }
                }
            }
            
            List<Product2> allProdList = [select id,sitePopUp__c,Family from Product2 where family in : prodNameWithSite.keyset()];
            
            for(Product2 updateProd : allProdList){
                if(prodNameWithSite.containsKey(updateProd.Family)){
                    if(updateProd.sitePopUp__c != prodNameWithSite.get(updateProd.Family)){
                        updateProd.sitePopUp__c = prodNameWithSite.get(updateProd.Family);
                        prodListToBeUpdated.add(updateProd);
                    }
                }
                
            }
            
            if(!prodListToBeUpdated.isEmpty()){
                update prodListToBeUpdated;
            }
            
        }
        
        integer i=0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
}