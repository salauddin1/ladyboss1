trigger CreateSHopifyOrders on Shopify_Orders__c (after Update) {
    if(ShopifyOrderCreation.isrecursive == false) {
        if(Trigger.isAfter && Trigger.isUpdate) {
           ShopifyOrderCreation.UpdateOder(Trigger.new[0].Id);
        }
    }
    if(Trigger.newMap.get(Trigger.new[0].id).Status__c != Trigger.oldMap.get(Trigger.new[0].id).Status__c) {
        if(Trigger.newMap.get(Trigger.new[0].id).Status__c == 'canceled'){
          ShopifyOrderCreation.cancelledOrder(Trigger.new[0].Id);
        }
    }
}