/*
 * Developer Name : Tirth Patel
 * Description : This trigger checks the type of WhoId value and then using type of WhoId it sets whoId value to Contact or Lead on Task and sets whoId to null. And It
 * also checks for WhatId value type and then sets WhatId to Account or Opportunity or Case using What id type on Task and sets whatId to null.  
 */
trigger TaskTrigger on Task (after insert,after update) {
    List<Task> tskList = [SELECT id,whoid,whatid,Contact__c,Lead__c,Account__c,Opportunity__c,Case__c,TaskSubtype FROM Task WHERE id IN : Trigger.new];
    if(taskAfterTriggerFlag.stopTrigger==false){
        taskAfterTriggerFlag.stopTrigger = true;
        for(Task tk : tskList){
            if(tk.WhoId!=null && String.valueOf(tk.WhoId).startsWith('003') && tk.TaskSubtype != 'Call'){
                tk.Contact__c = tk.WhoId;
                tk.WhoId = null;
            }else if(tk.WhoId!=null && String.valueOf(tk.WhoId).startsWith('00Q') && tk.TaskSubtype != 'Call'){
                tk.Lead__c = tk.WhoId;
                tk.WhoId = null;
            }
            if(tk.WhatId!=null && String.valueOf(tk.WhatId).startsWith('001') && tk.TaskSubtype != 'Call'){
                tk.Account__c = tk.WhatId;
                tk.WhatId = Null;
            }else if(tk.WhatId!=null && String.valueOf(tk.WhatId).startsWith('500') && tk.TaskSubtype != 'Call'){
                tk.case__c = tk.WhatId;
                tk.WhatId = Null;
            }else if(tk.WhatId!=null && String.valueOf(tk.WhatId).startsWith('006') && tk.TaskSubtype != 'Call'){
                tk.Opportunity__c = tk.WhatId;
                tk.WhatId = null;
            }
        }
        update tskList;
    }
}