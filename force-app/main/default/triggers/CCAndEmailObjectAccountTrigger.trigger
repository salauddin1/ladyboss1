trigger CCAndEmailObjectAccountTrigger on Account (before update,after insert) {
    for(Account act : trigger.new) {
        if(act.Ready_in_Tipalti__c == true)
        ContactAccountCrossObjectWorkflows.UpdateContactChekBox(Trigger.new);
    }
    
    List<Account> accountList = Trigger.isInsert?Trigger.new:Trigger.old;
    Map<Id,Account> accountMap = Trigger.newMap; 
    List<Account_Old_Email__c> oldEmailList = new List<Account_Old_Email__c>();
    List<Account_Old_CC__c> oldCCList = new List<Account_Old_CC__c>();
    List<Account_Old_Stripe_Id__c> oldStripeList = new List<Account_Old_Stripe_Id__c>();
    List<Mail_Address__c> mailAddressA = new List<Mail_Address__c>();
    List<Account> accountToUpdate = new List<Account>();
    Account_Old_CC__c oldCC;
    Account_Old_Stripe_Id__c oldStripe;
    Account_Old_Email__c oldEmail;
    Mail_Address__c oldMailA;
    
    List<Id> accountIdList = new List<Id>();
    for(Account act : accountList){
        accountIdList.add(act.id);
    }
    
    List<Account_Old_CC__c> ccFromDbList = [SELECT id,Last_CC_Num__c,Account__c FROM Account_Old_CC__c WHERE Account__c IN :accountIdList];
    List<Account_Old_Email__c> emailFromDbList = [SELECT id,Last_Email__c,Account__c FROM Account_Old_Email__c WHERE Account__c IN :accountIdList];
    List<Account_Old_Stripe_Id__c> stripeFromDBList = [SELECT id,Last_Stripe_Id__c,Account__c FROM Account_Old_Stripe_Id__c WHERE Account__c IN :accountIdList];
    List<Mail_Address__c> mailFromDbListA = [SELECT Last_Mail_Modified_Date__c,Last_Mail__c,Account__c FROM Mail_Address__c WHERE Account__c IN : accountIdList];
    Map<id,Map<String,String>> accountCCMap = new Map<id,Map<String,String>>();
    Map<id,Map<String,String>> accountEmailMap = new Map<id,Map<String,String>>();
    Map<id,Map<String,String>> accountStripeMap = new Map<id,Map<String,String>>();
    Map<id,Map<String,String>> accountMailMap = new Map<id,Map<String,String>>();
    for(Account_Old_CC__c oldCcFromDb : ccFromDbList){
        if(!accountCCMap.containsKey(oldCcFromDb.Account__c)){
            accountCCMap.put(oldCcFromDb.Account__c, new Map<String,String>());
        }
        accountCCMap.get(oldCcFromDb.Account__c).put(oldCcFromDb.Last_CC_Num__c,oldCcFromDb.id);
    }
    
    for(Account_Old_Stripe_Id__c stripeFromDB : stripeFromDBList){
        if(!accountStripeMap.containsKey(stripeFromDB.Account__c)){
            accountStripeMap.put(stripeFromDB.Account__c, new Map<String,String>());
        }
        accountStripeMap.get(stripeFromDB.Account__c).put(stripeFromDB.Last_Stripe_Id__c,stripeFromDB.id);
    }
    
    for(Account_Old_Email__c oldEmailFromDb : emailFromDbList){
        if(!accountEmailMap.containsKey(oldEmailFromDb.Account__c)){
            accountEmailMap.put(oldEmailFromDb.Account__c, new Map<String,String>());
        }
        accountEmailMap.get(oldEmailFromDb.Account__c).put(oldEmailFromDb.Last_Email__c,oldEmailFromDb.id);
    }
    for(Mail_Address__c oldMailFromDbA : mailFromDbListA){
        if(!accountMailMap.containsKey(oldMailFromDbA.Account__c)){
            accountMailMap.put(oldMailFromDbA.Account__c, new Map<String,String>());
        }
        accountMailMap.get(oldMailFromDbA.Account__c).put(oldMailFromDbA.Last_Mail__C,oldMailFromDbA.id);
    }
    
    for(Account act : accountList){
        Account account = new Account(id=act.id);
        if(accountMap.get(act.id).Email__c!= null && ( Trigger.isInsert || accountMap.get(act.id).Email__c != act.Email__c)){
            oldEmail = new Account_Old_Email__c();  
            oldEmail.Last_Email__c = accountMap.get(act.id).Email__c;
            if(accountEmailMap.get(act.id)!= null && accountEmailMap.get(act.id).containsKey(oldEmail.Last_Email__c)){
                oldEmail.id = accountEmailMap.get(act.id).get(oldEmail.Last_Email__c);
            }
            oldEmail.Account__c = act.id;
            oldEmail.Name = act.Name;
            oldEmail.Last_Email_Modified_Date__c = Datetime.now();
            if(Trigger.isInsert) {
                account.Last_Email_Modified_Date__c=oldEmail.Last_Email_Modified_Date__c;
            }else{
                accountMap.get(act.id).Last_Email_Modified_Date__c = oldEmail.Last_Email_Modified_Date__c;                
            }
            
            oldEmailList.add(oldEmail);
        }
        
        if(accountMap.get(act.id).External_ID__c!= null && (Trigger.isInsert || accountMap.get(act.id).External_ID__c != act.External_ID__c)){
            oldStripe = new Account_Old_Stripe_Id__c();
            oldStripe.Account__c = act.id;
            oldStripe.Last_Stripe_Id__c = accountMap.get(act.id).External_ID__c;
            if(accountStripeMap.get(act.id)!= null && accountStripeMap.get(act.id).containsKey(oldStripe.Last_Stripe_Id__c)){
                oldStripe.id = accountStripeMap.get(act.id).get(oldStripe.Last_Stripe_Id__c);
            }
            oldStripe.Last_Stripe_Modified_Date__c = Datetime.now();
            
            if(Trigger.isInsert) {
                account.Last_Stripe_Modified_Date__c=oldStripe.Last_Stripe_Modified_Date__c;
            } else {
                accountMap.get(act.id).Last_Stripe_Modified_Date__c = oldStripe.Last_Stripe_Modified_Date__c;
                
            }
            oldStripe.Name = act.Name;
            oldStripeList.add(oldStripe);
        }
        
        if(accountMap.get(act.id).Credit_Card_Number__c!= null && (Trigger.isInsert || accountMap.get(act.id).Credit_Card_Number__c != act.Credit_Card_Number__c)){
            oldCC = new Account_Old_CC__c();
            oldCC.Account__c = act.id;
            oldCC.Last_CC_Num__c = accountMap.get(act.id).Credit_Card_Number__c;
            if(accountCCMap.get(act.id)!=null && accountCCMap.get(act.id).containsKey(oldCC.Last_CC_Num__c)){
                oldCC.id = accountCCMap.get(act.id).get(oldCC.Last_CC_Num__c);
            }
            oldCC.Last_CC_Modified_Date__c = Datetime.now();
            
            if(Trigger.isInsert) {
                account.Last_CC_Modified_Date__c=oldCC.Last_CC_Modified_Date__c;
            } else {
                accountMap.get(act.id).Last_CC_Modified_Date__c = oldCC.Last_CC_Modified_Date__c;    
            }
            
            oldCC.Name = act.Name;
            oldCCList.add(oldCC);
        }
        
        if((accountMap.get(act.id).BillingCountry!= null && (Trigger.isInsert ||  accountMap.get(act.id).BillingCountry!=act.BillingCountry)) || (accountMap.get(act.id).BillingState!= null && (Trigger.isInsert || accountMap.get(act.id).BillingState!=act.BillingState)) || (accountMap.get(act.id).BillingStreet!= null  && (Trigger.isInsert || accountMap.get(act.id).BillingStreet!=act.BillingStreet)) || (accountMap.get(act.id).BillingCity!= null && (Trigger.isInsert || accountMap.get(act.id).BillingCity!=act.BillingCity)) || (accountMap.get(act.id).BillingPostalCode!= null && (Trigger.isInsert || accountMap.get(act.id).BillingPostalCode!=act.BillingPostalCode))){
            oldMailA = new Mail_Address__c();  
            oldMailA.Last_Mail__c = String.valueOf(accountMap.get(act.id).BillingStreet) + ', '+ String.valueOf(accountMap.get(act.id).BillingCity) + ', '+String.valueOf(accountMap.get(act.id).BillingState) + ', '+ String.valueOf(accountMap.get(act.id).BillingCountry)+ ' - ' + String.valueOf(accountMap.get(act.id).BillingPostalCode);
            if(accountMailMap.get(act.id)!= null && accountMailMap.get(act.id).containsKey(oldMailA.Last_Mail__c)){
                oldMailA.id = accountMailMap.get(act.id).get(oldMailA.Last_Mail__c);
            }  
            oldMailA.Account__c = act.id;
            oldMailA.Name = act.Name;
            oldMailA.Address_type__c = 'Billing Address';
            oldMailA.Last_Mail_Modified_Date__c = Datetime.now();
            if(Trigger.isInsert) {
                account.Last_Mail_Modified_Date__c=oldMailA.Last_Mail_Modified_Date__c;
            }else{
                                    accountMap.get(act.id).Last_Mail_Modified_Date__c = oldMailA.Last_Mail_Modified_Date__c;                
            } 
            mailAddressA.add(oldMailA);     
        }
        
        if((accountMap.get(act.id).ShippingCountry!= null && (Trigger.isInsert ||  accountMap.get(act.id).ShippingCountry!=act.ShippingCountry)) || (accountMap.get(act.id).ShippingState!= null && (Trigger.isInsert || accountMap.get(act.id).ShippingState!=act.ShippingState)) || (accountMap.get(act.id).ShippingStreet!= null  && (Trigger.isInsert || accountMap.get(act.id).ShippingStreet!=act.ShippingStreet)) || (accountMap.get(act.id).ShippingCity!= null && (Trigger.isInsert || accountMap.get(act.id).ShippingCity!=act.ShippingCity)) || (accountMap.get(act.id).ShippingPostalCode!= null && (Trigger.isInsert || accountMap.get(act.id).ShippingPostalCode!=act.ShippingPostalCode))){
            oldMailA = new Mail_Address__c();  
            oldMailA.Last_Mail__c = String.valueOf(accountMap.get(act.id).ShippingStreet) + ', '+ String.valueOf(accountMap.get(act.id).ShippingCity) + ', '+String.valueOf(accountMap.get(act.id).ShippingState) + ', '+ String.valueOf(accountMap.get(act.id).ShippingCountry)+ ' - ' + String.valueOf(accountMap.get(act.id).ShippingPostalCode);
            //System.debug(oldMailA.Last_Mail__c = (String.valueOf(accountMap.get(act.id).ShippingCountry!=act.ShippingCountry) +  String.valueOf(accountMap.get(act.id).ShippingState!=act.ShippingState) + String.valueOf(accountMap.get(act.id).ShippingStreet!=act.ShippingStreet) + String.valueOf(accountMap.get(act.id).ShippingCity!=act.ShippingCity) + String.valueOf(accountMap.get(act.id).ShippingPostalCode!=act.ShippingPostalCode)));
            if(accountMailMap.get(act.id)!= null && accountMailMap.get(act.id).containsKey(oldMailA.Last_Mail__c)){
                oldMailA.id = accountMailMap.get(act.id).get(oldMailA.Last_Mail__c);
            }  
            oldMailA.Account__c = act.id;
            oldMailA.Name = act.Name;
            oldMailA.Last_Mail_Modified_Date__c = Datetime.now();
            oldMailA.Address_type__c = 'Shipping Address';
            if(Trigger.isInsert) {
                account.Last_Mail_Modified_Date__c=oldMailA.Last_Mail_Modified_Date__c;
            }else{
                accountMap.get(act.id).Last_Mail_Modified_Date__c = oldMailA.Last_Mail_Modified_Date__c;                
            } 
            mailAddressA.add(oldMailA);     
        }
        
        accountToUpdate.add(account);
    }
    
    upsert oldCCList;
    upsert oldStripeList;
    upsert oldEmailList;
    upsert mailAddressA;
    
    if (!StopRecursiveTrigger.isTriggerAccountExecuted && Trigger.isInsert){
        StopRecursiveTrigger.isTriggerAccountExecuted = true;
        if(accountToUpdate.size()>0) {
            update accountToUpdate;
        }      
        
    }
    
}