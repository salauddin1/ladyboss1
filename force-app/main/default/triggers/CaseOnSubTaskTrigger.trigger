trigger CaseOnSubTaskTrigger on Subsequent_Task__c (before insert) {
    try{
        List<String> caseIdlist = new List<String>();
        for(Subsequent_Task__c cs : Trigger.New){
            if(cs.Case_Id__c != null){
                caseIdlist.add(cs.Case_Id__c);
            }
        }
        Map<String,Case> idCaseMap = new Map<String,Case>([SELECT id FROM CASE WHERE id in : caseIdlist]);
        System.debug('idCaseMap : '+idCaseMap);
        for(Subsequent_Task__c sb : Trigger.New){
            if(sb.Case_Id__c!=null && idCaseMap.get(sb.Case_Id__c)!=null){
                sb.Case__c	= idCaseMap.get(sb.Case_Id__c).Id;
            }
        }
        if(Test.isRunningTest()){
            System.debug(1/0);
        }
    }Catch(Exception e){
        ApexDebugLog apex=new ApexDebugLog();
        apex.createLog(
            new ApexDebugLog.Error('CaseOnSubTaskTrigger','trigger',trigger.new[0].id,e) );
    }
}