trigger AccountRecoveryCaseEmail on EmailMessage (before insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        List<case> csListUpdate = new List<case>();
        Set<Id> caseId = new Set<Id>();
        for(EmailMessage em : Trigger.New ) {
            if(em.ParentId!=null){
                if(em.ParentId.getsobjecttype()!=null){
                    
                    if(String.valueOf(em.ParentId.getsobjecttype()) =='Case') {
                        caseId.add(em.ParentId); 
                        
                    }
                }
            }
            
        }
        Group gr = [SELECT CreatedDate,DeveloperName FROM Group where DeveloperName =: 'Account_Recovery'];
        
        List<Case> csList = [select id,IsFailed_Payment__c,ownerId from case where Id IN :caseId];
        if(csList.size () > 0) {
            for(Case cs : csList) {
                if(gr!=NULL) {
                    if(gr.id!=null && cs.IsFailed_Payment__c) {
                        cs.OwnerId = gr.id;
                        csListUpdate.add(cs);
                    }
                    
                }
            }
        }
        
        
        if(csListUpdate.size() > 0) {
            
            update csListUpdate;
            
        } 
    }
}