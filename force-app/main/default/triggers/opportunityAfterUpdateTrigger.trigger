trigger opportunityAfterUpdateTrigger on Opportunity (after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true && Trigger.isAfter&&Trigger.isUpdate){
        try{
        OpportunityAfterUpdateHandler.firstSellCheck(Trigger.newMap,Trigger.oldMap);
        OpportunityAfterUpdateHandler.salesPersonChange(Trigger.newMap,Trigger.oldMap);
            if(Test.isRunningTest()){
                integer i=1/0;
            }
        }catch(Exception e){
                System.debug(e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'opportunityAfterUpdateTrigger',
                        'execute',
                        null,
                        e
                    )
                );
        }
    }
}