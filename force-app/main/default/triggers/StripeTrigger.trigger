trigger StripeTrigger on Stripe_Profile__c (after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true){
        Set<string> profIdSet = new Set<string>();
        map<String,Stripe_Profile__c> cussprofileMAP = new map<String,Stripe_Profile__c>(); 
        map<String,String> cussContactMAP = new map<String,String>();  
        for(Stripe_Profile__c cs : Trigger.New) {
            if(cs.Stripe_Customer_Id__c !=null ){
                profIdSet.add(string.valueOf(cs.Stripe_Customer_Id__c));
                cussprofileMAP.put(cs.Stripe_Customer_Id__c,cs);
            }
            
        }
        List<card__c> cardList = [SELECT id,Card_ID__c,customerid__c,Credit_Card_Number__c,createddate ,Initiated_From__c,Stripe_Card_Id__c,contact__c,lastmodifiedbyid,Stripe_Profile__c FROM Card__c where  (contact__c=null OR Stripe_Profile__c=null) and CustomerID__c = : profIdSet order by createddate desc];
        if(cardList.size() > 0){
            
            List<card__c> cardListToUpdated = new List<card__c>();
            for(card__c ca : cardList ){
                if(cussprofileMAP.containsKey(ca.customerid__c)){
                    if(cussprofileMAP.get(ca.customerid__c).id!=null) {
                        ca.Stripe_Profile__c = cussprofileMAP.get(ca.customerid__c).id;
                    }
                    if(cussprofileMAP.get(ca.customerid__c).Customer__c !=null) {
                        ca.contact__c = cussprofileMAP.get(ca.customerid__c).Customer__c;
                    }
                    cardListToUpdated.add(ca);
                }
                
            }
            
            if(cardListToUpdated.size() > 0){
                update cardListToUpdated;
                System.debug('===='+cardListToUpdated);
            }
        }
    }
}