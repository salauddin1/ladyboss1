/*
* Developer Name : Tirth Patel
* Discription : If Contact with same email as toAddress on EmailMessage is Amazon Contact then Email will not be sent.
*/
trigger SendEmail on EmailMessage (before insert) {
    List<String> emailList = new List<String>();
    for(EmailMessage em : Trigger.new){
        if(em.ToAddress!=null){
            emailList.add(em.ToAddress);
        }
    }
    List<Contact> contList = [SELECT id,Amazon_Customer__c,email FROM Contact WHERE email in : emailList];
    Map<String,Contact> emailContMap = new Map<String,Contact>(); 
    for(Contact ct : contList){
        emailContMap.put(ct.email.toLowerCase(), ct);
    }    
    for(EmailMessage em : Trigger.new){
        if(em.ToAddress!=null){
            if(emailContMap.get(em.ToAddress.toLowerCase())!=null && emailContMap.get(em.ToAddress.toLowerCase()).Amazon_Customer__c){
                em.addError('Currently we do not allow to send email to amazon contact');
            }
        }
    }
}