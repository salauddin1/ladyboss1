trigger LinkCardOpportunity on Card__c (after insert) {
    Set<String> cardIdSet = new Set<String>();
    Map<String,Card__c> cardIdMap = new  Map<String,Card__c> ();
    if(Trigger.isInsert){
        for(Card__c card:Trigger.New){
            if(card.Card_ID__c !=null) {
                cardIdSet.add(card.Card_ID__c);
                cardIdMap.put(card.Card_ID__c,card);
            }
        }
        List<opportunity> lstOpps = [select id,CardId__c from opportunity where Card__c=null and CardId__c IN :cardIdSet limit 5000];
        List<opportunity> lstOppsUpdate = new  List<opportunity>();
        for(opportunity op : lstOpps) {
            if(op.CardId__c!=null && cardIdMap.containsKey(op.CardId__c)) {
                op.Card__c = cardIdMap.get(op.CardId__c).id;
                lstOppsUpdate.add(op);
            }
        }
        if(lstOppsUpdate.size() > 0){
             update lstOppsUpdate;
        }
       
    }
}