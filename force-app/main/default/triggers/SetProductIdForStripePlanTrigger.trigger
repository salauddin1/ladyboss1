trigger SetProductIdForStripePlanTrigger on Product2 (after insert,after update) {
   
    List<ID> prodIdListing = new List<ID>();
    List<ID> prodIdList = new List<ID>();
    for (Product2 prod : Trigger.new) {
        if (prod.Stripe_Plan_Product_Id__c == null && prod.Stripe_Plan_Id__c != null) {
            prodIdList.add(prod.Id);
        }else if ((prod.Stripe_Plan_Id__c == null || prod.Stripe_Plan_Id__c == '') && prod.Subscription__c && prod.Price__c !=null && prod.Interval__c !=null  && prod.Interval_Count__c != null) {
            prodIdListing.add(prod.Id);
        }
    }
    if (prodIdList.size()>0) {
        StripeGetPlan.getPlan(prodIdList);
    }else if (prodIdListing.size()>0) {
        StripeGetPlan.createPlan(prodIdListing);
    }
}