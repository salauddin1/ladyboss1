trigger NMIAccountCustomerTrigger on Account (after insert, after update) {
    
    if(ContactTriggerFlag.isContactBatchRunning != true && ApexUtil.isTriggerInvoked==false){
        if(trigger.isAfter){
            Map<String,String> mapCustomer;
            NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
            String firstname;
            String lastname;
            String expmonth;
            String expyear;
            if(trigger.isInsert){
                for(Account acc : trigger.new){
                    expyear = '';
                    if(acc.Name.indexOf(' ')>0){
                        firstname = acc.Name.substring(0,acc.Name.indexOf(' '));
                        lastname = acc.Name.substring(acc.Name.indexOf(' '));
                    }else{
                        firstname = acc.Name;
                        lastname = '';
                    }
                    
                    expmonth = String.isNotBlank(acc.Exp_Month__c) && acc.Exp_Month__c.length() < 2 ? '0' + acc.Exp_Month__c : acc.Exp_Month__c;
                    
                    if(acc.Exp_Year__c!=null){
                        expyear = String.valueOf(acc.Exp_Year__c);
                        expyear = expyear.subString(2,expyear.length());
                    }
                    
                    mapCustomer = new Map<String,String>();
                    mapCustomer.put('customer_vault','add_customer');
                    mapCustomer.put('username',nmip.User__c);
                    mapCustomer.put('password',nmip.Password__c);
                    mapCustomer.put('ccnumber',acc.Credit_Card_Number__c);
                    mapCustomer.put('ccexp',expmonth + expyear);
                    mapCustomer.put('first_name',firstname);
                    mapCustomer.put('last_name',lastname);
                    if(acc.phone!=null) mapCustomer.put('phone',acc.phone);
                    if(acc.Email__c!=null) mapCustomer.put('email',acc.Email__c);
                    if(acc.Fax!=null) mapCustomer.put('fax',acc.Fax);
                    if(acc.BillingStreet!=null) mapCustomer.put('address1',acc.BillingStreet);
                    if(acc.BillingCity!=null) mapCustomer.put('city',acc.BillingCity);
                    if(acc.BillingState!=null) mapCustomer.put('state',acc.BillingState);
                    if(acc.BillingPostalCode!=null) mapCustomer.put('zip',acc.BillingPostalCode);
                    if(acc.BillingCountry!=null) mapCustomer.put('country',acc.BillingCountry);
                    mapCustomer.put('shipping_firstname',firstname);
                    mapCustomer.put('shipping_lastname',lastname);
                    if(acc.ShippingStreet!=null) mapCustomer.put('shipping_address1',acc.ShippingStreet);
                    if(acc.ShippingCity!=null) mapCustomer.put('shipping_city',acc.ShippingCity);
                    if(acc.ShippingState!=null) mapCustomer.put('shipping_state',acc.ShippingState);
                    if(acc.ShippingPostalCode!=null) mapCustomer.put('shipping_zip',acc.ShippingPostalCode);
                    if(acc.ShippingCountry!=null) mapCustomer.put('shipping_country',acc.ShippingCountry);
                    if(!Test.isRunningTest()) PaymentGatewayNMI.HttpDirectPost('customer',acc.Id, mapCustomer);
                }
            }
            
            if(trigger.isUpdate){
                for(Account acc : trigger.new){
                    if(acc.Name!=trigger.oldMap.get(acc.Id).Name 
                       || acc.Phone!=trigger.oldMap.get(acc.Id).Phone 
                       || acc.Email__c!=trigger.oldMap.get(acc.Id).Email__c
                       || acc.Fax!=trigger.oldMap.get(acc.Id).Fax
                       || acc.BillingStreet!=trigger.oldMap.get(acc.Id).BillingStreet 
                       || acc.BillingState!=trigger.oldMap.get(acc.Id).BillingState
                       || acc.BillingCity!=trigger.oldMap.get(acc.Id).BillingCity
                       || acc.BillingPostalCode!=trigger.oldMap.get(acc.Id).BillingPostalCode
                       || acc.BillingCountry!=trigger.oldMap.get(acc.Id).BillingCountry
                       || acc.ShippingStreet!=trigger.oldMap.get(acc.Id).ShippingStreet 
                       || acc.ShippingState!=trigger.oldMap.get(acc.Id).ShippingState
                       || acc.ShippingCity!=trigger.oldMap.get(acc.Id).ShippingCity
                       || acc.ShippingPostalCode!=trigger.oldMap.get(acc.Id).ShippingPostalCode
                       || acc.ShippingCountry!=trigger.oldMap.get(acc.Id).ShippingCountry
                       || acc.Exp_Month__c!=trigger.oldMap.get(acc.Id).Exp_Month__c
                       || acc.Exp_Year__c!=trigger.oldMap.get(acc.Id).Exp_Year__c
                       || acc.Credit_Card_Number__c!=trigger.oldMap.get(acc.Id).Credit_Card_Number__c
                      ){
                    	if(acc.Name.indexOf(' ')>0){
                            firstname = acc.Name.substring(0,acc.Name.indexOf(' '));
                            lastname = acc.Name.substring(acc.Name.indexOf(' '));
                        }else{
                            firstname = acc.Name;
                            lastname = '';
                        }
                    
                        mapCustomer = new Map<String,String>();
                    	if(acc.NMI_External_ID__c==null){
                    		mapCustomer.put('customer_vault','add_customer');
                        }else{
                            mapCustomer.put('customer_vault','update_customer');
                            mapCustomer.put('customer_vault_id',acc.NMI_External_ID__c);
                        }
                    
                        expmonth = String.isNotBlank(acc.Exp_Month__c) && acc.Exp_Month__c.length() < 2 ? '0' + acc.Exp_Month__c : acc.Exp_Month__c;
                        if(acc.Exp_Year__c!=null){
                            expyear = String.valueOf(acc.Exp_Year__c);
                            expyear = expyear.subString(2,expyear.length());
                        }
                        mapCustomer.put('username',nmip.User__c);
                        mapCustomer.put('password',nmip.Password__c);
                        mapCustomer.put('ccnumber',acc.Credit_Card_Number__c);
                        mapCustomer.put('ccexp',expmonth + expyear);
                        mapCustomer.put('first_name',firstname);
                        mapCustomer.put('last_name',lastname);
                        if(acc.phone!=null) mapCustomer.put('phone',acc.phone);
                        if(acc.Email__c!=null) mapCustomer.put('email',acc.Email__c);
                        if(acc.Fax!=null) mapCustomer.put('fax',acc.Fax);
                        if(acc.BillingStreet!=null) mapCustomer.put('address1',acc.BillingStreet);
                        if(acc.BillingCity!=null) mapCustomer.put('city',acc.BillingCity);
                        if(acc.BillingState!=null) mapCustomer.put('state',acc.BillingState);
                        if(acc.BillingPostalCode!=null) mapCustomer.put('zip',acc.BillingPostalCode);
                        if(acc.BillingCountry!=null) mapCustomer.put('country',acc.BillingCountry);
                        mapCustomer.put('shipping_firstname',firstname);
                        mapCustomer.put('shipping_lastname',lastname);
                        if(acc.ShippingStreet!=null) mapCustomer.put('shipping_address1',acc.ShippingStreet);
                        if(acc.ShippingCity!=null) mapCustomer.put('shipping_city',acc.ShippingCity);
                        if(acc.ShippingState!=null) mapCustomer.put('shipping_state',acc.ShippingState);
                        if(acc.ShippingPostalCode!=null) mapCustomer.put('shipping_zip',acc.ShippingPostalCode);
                        if(acc.ShippingCountry!=null) mapCustomer.put('shipping_country',acc.ShippingCountry);
                       
                        if(!Test.isRunningTest()) PaymentGatewayNMI.HttpDirectPost('customer',acc.Id, mapCustomer);
                    }
                }
            }
        }
    }
}