trigger PAPTrigger on Pap_Commission__c (before insert,before update) {
    List<String> orderIdList = new List<String>();
    if (Trigger.isInsert) {
        for (Pap_Commission__c pc : Trigger.new) {
            if ( pc.Opportunity__c != null ) {
                orderIdList.add(pc.Opportunity__c);
            }
        }        
    }
    if (Trigger.isUpdate) {
        for (Pap_Commission__c pc : Trigger.new) {
            if ((Trigger.oldMap.get(pc.Id).Opportunity__c == null  && pc.Opportunity__c != null) || (pc.Opportunity__c != null && Trigger.oldMap.get(pc.Id).Processed__c == false)) {
                orderIdList.add(pc.Opportunity__c);
            }
        }
    }
    if (orderIdList.size()>0) {
        List<Opportunity> oppList = [SELECT id,Contact__c,WC_Order_Number__c ,WC_Product_Id__c,RecordTypeId FROM Opportunity WHERE id IN:orderIdList];
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Map<String,Decimal> conIdcommisionMap = new Map<String,Decimal>();
        Map<Id,String> conIdcommisionTypeMap = new Map<Id,String>();
        List<OpportunityLineItem> oliList = [select id,opportunityId,product2.Commission_Enabled_Club_Product__c from OpportunityLineItem where opportunityId in : oppList];
        Map<Id,OpportunityLineItem> oliMap = new Map<Id,OpportunityLineItem>();
        for(OpportunityLineItem oli:oliList){
            oliMap.put(oli.OpportunityId, oli);
        }
        for (Pap_Commission__c pc : Trigger.new) {
            for (Opportunity opp : oppList) {
                if (pc.opportunity__c == opp.id) {
                    if(pc.Contact__c != null && (pc.Contact__c != opp.Contact__c || opp.RecordTypeId == recordTypeId) ){
                        if(opp.RecordTypeId != recordTypeId 
                           || (oliMap.containsKey(opp.Id) 
                               && oliMap.get(opp.Id).product2.Commission_Enabled_Club_Product__c)){
                                   Decimal com = 0;
                                   if(opp.contact__c == pc.contact__c){
                                       pc.commission__c = (pc.commission__c/33.34)*20;
                                   }
                                   pc.Processed__c = true;
                                   if(!conIdcommisionMap.containsKey(pc.Contact__c)){
                                       conIdcommisionMap.put(pc.Contact__c,pc.commission__c);
                                       conIdcommisionTypeMap.put(pc.Contact__c,pc.commission_type__c);
                                   }else{
                                       com = conIdcommisionMap.get(pc.Contact__c) + pc.commission__c;                            
                                       conIdcommisionMap.put(pc.Contact__c,com);
									   conIdcommisionTypeMap.put(pc.Contact__c,pc.commission_type__c);

                                   }
                               }
                        /*if (pc.Contact__c != opp.Contact__c) {
pc.Processed__c = true;
conIdcommisionMap.put(pc.Contact__c,pc.commission__c);
}*/
                    }
                }
            }
        }
        List<Contact> conList = [SELECT id,Available_Commission__c,Update_Balance_Reason__c FROM Contact WHERE Id IN:conIdcommisionMap.keySet()];
        for (Contact con : conList) {
            if (con.Available_Commission__c == null) {
                con.Available_Commission__c = 0;
                con.Update_Balance_Reason__c = ' ';
            }
            con.Available_Commission__c += conIdcommisionMap.get(con.Id);
                if(conIdcommisionTypeMap.get(con.Id) == 'initial'){
                    con.Update_Balance_Reason__c = 'Referred Purchase';
                }
                if(conIdcommisionTypeMap.get(con.Id) == 'refund'){
                    con.Update_Balance_Reason__c = 'Refund';
                }
                if(conIdcommisionTypeMap.get(con.Id) == 'repeated'){
                    con.Update_Balance_Reason__c = 'Subscription Renewal';
                }
        }
        if (conList.size()>0) {
            update conList;
        }
    }
}