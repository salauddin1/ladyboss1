/* Developer    : Prachi Khandagle
* Description : We are checking that recent created opportunity having 'LadyBoss-LVCH-$1-Dig' Or 'LadyBoss-LVCH-$27-Phy'
Created Date - 30/8/2019
*/
trigger OpportunityTimeDiff on Opportunity (after insert) {
    if(trigger.isAfter && trigger.isInsert) {
        Set<ID> conIdSet = new Set<Id>();
        Set<Id> currentOppId = new Set<Id>();
        for(Opportunity opp : trigger.new) {
            conIdSet.add(opp.Contact__c);
            currentOppId.add(opp.id);
        }
        List<Additional_Opportunity_Names__c> custOppName = new List<Additional_Opportunity_Names__c>();
        custOppName = Additional_Opportunity_Names__c.getall().values();
        Set<String> oppNames = new Set<String>();
        if(custOppName != null && !custOppName.isEmpty()) {
            for(Additional_Opportunity_Names__c oppNameVar : custOppName) {
                oppNames.add(oppNameVar.Opportunity_Name__c);
            }
        }
        List<Opportunity> oppList = new List<Opportunity>();
        boolean check = false;
        oppList = [Select id,Name,Opportunities_Time_Difference__c,CreatedDate,createdTime__c from Opportunity where id =: trigger.newMap.keySet() and Name =: oppNames];
        if(oppList != null && !oppList.isEmpty()){
            for(Opportunity opp : [Select id,Name,Contact__c,Statement_Descriptor__c,createdTime__c,CreatedDate from Opportunity where Contact__C != null and  Contact__c =: conIdSet and id !=: currentOppId and (Name = 'LadyBoss-LVCH-$1-Dig' OR Name = 'LadyBoss-LVCH-$27-Phy')]) {
                for(Opportunity op : oppList) {
                    Long milliseconds = op.CreatedDate.getTime() - opp.CreatedDate.getTime();
                    Long seconds = milliseconds / 1000;
                    Long minutes = seconds / 60;
                    if(minutes <= 10) {
                        op.Opportunities_Time_Difference__c = true;
                    }
                    check = true ;
                }
            }
        }
        if(check == true){
            update oppList;    
        }
    }
}