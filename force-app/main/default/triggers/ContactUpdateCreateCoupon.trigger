trigger ContactUpdateCreateCoupon on Contact (after update) {
    List<Contact> createCouponList = new List<Contact>();
    List<Contact> createCouponList2 = new List<Contact>();
    List<Contact> updateCouponList = new List<Contact>();
    List<Contact> updateaffIdList = new List<Contact>();
    if(System.isBatch() != true){
        for(Contact ct : trigger.new){
            if(trigger.oldmap.get(ct.Id).PAP_refid__c == null && ct.PAP_refid__c != null){
                if(ct.Coupon_Code_Id__c == null ){
                    createCouponList.add(ct);
                }
                updateaffIdList.add(ct);
            }
            if(trigger.oldmap.get(ct.Id).PAP_refid__c != null && ct.PAP_refid__c != null && trigger.oldmap.get(ct.Id).Coupon_Code_Id__c == null && ct.Coupon_Code_Id__c == null ){
                createCouponList2.add(ct);
            }
            if(trigger.oldmap.get(ct.Id).PAP_refid__c != null && ct.PAP_refid__c != null && trigger.oldmap.get(ct.Id).PAP_refid__c !=  ct.PAP_refid__c ){
                updateaffIdList.add(ct);
                if(trigger.oldmap.get(ct.Id).Coupon_Code_Id__c != null && ct.Coupon_Code_Id__c != null ){
                    updateCouponList.add(ct);
                }
            }
        }
    }
    if(createCouponList2.size()>0){
        ConnectToWooCommerceRest.createPostRequest(createCouponList2);
    }
    if(createCouponList.size()>0){
        ConnectToWooCommerceRest.createPostRequest(createCouponList);
    }
    if(updateCouponList.size()>0){
        ConnectToWooCommerceRest.updatePostRequest(updateCouponList);
    }
    if(updateaffIdList.size()>0){
        updateAffiliateId.updateAffiliate1(updateaffIdList);
    }
}