trigger CardContactLink on Card__c (after insert)  {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        Set<Id> cardIdSet = new Set<Id>();
        for(Card__c cs : Trigger.New) {
            if(cs.Stripe_Profile__c==null || cs.Contact__c==null){
                cardIdSet.add(cs.id);
            }
            
        }
        List<card__c> cardList = [SELECT id,Card_ID__c,customerid__c,Credit_Card_Number__c,createddate ,Initiated_From__c,Stripe_Card_Id__c,contact__c,lastmodifiedbyid,Stripe_Profile__c FROM Card__c where id=:cardIdSet and customerid__c!=null order by createddate desc];
        if(cardList.size() > 0){
            set<string> cusIDset = new set<string>();
            
            for(card__c ca : cardList ){
                cusIDset.add(ca.customerid__c);
            }
            
            list<Stripe_Profile__c> sprofilelist = [select id,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: cusIDset];
            
            map<String,Stripe_Profile__c> cussprofileMAP = new map<String,Stripe_Profile__c>();
            
            for(Stripe_Profile__c sp : sprofilelist){
                cussprofileMAP.put(sp.Stripe_Customer_Id__c,sp);
            }
            List<card__c> cardListToUpdated = new List<card__c>();
            for(card__c ca : cardList ){
                if(cussprofileMAP.containsKey(ca.customerid__c)){
                    ca.Stripe_Profile__c = cussprofileMAP.get(ca.customerid__c).id;
                    ca.contact__c = cussprofileMAP.get(ca.customerid__c).Customer__c;
                    cardListToUpdated.add(ca);
                }
                
            }
            
            if(cardListToUpdated.size() > 0){
                update cardListToUpdated;
            }
        }
    }
}