trigger ShipstationOrderMgmt on ShipStation_Orders__c (after insert, after update) {
    
    if(ShipstationOrderStatusShippedBatch.flag == false){
        if(Trigger.isAfter && Trigger.isUpdate) {
            for(ShipStation_Orders__c ord: Trigger.new){
                if(trigger.oldMap.get(ord.id).orderStatus__c !='shipped'){
                    if(trigger.newMap.get(ord.id).orderStatus__c != trigger.oldMap.get(ord.id).orderStatus__c ) {
                        ShipstationOrder.updateOrders(Trigger.new[0].Id);
                    }else if(true){
                        ShipstationOrder.updateOrders(Trigger.new[0].Id);
                    }
                } else{
                    ord.addError('Order is already shipped');
                }   
            }
        }
    }
}