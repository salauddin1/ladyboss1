//Trigger to update case status when agent accepts case through omnichannel
trigger AgentWork on AgentWork (after update) {
    
    List<Case> caseToUpdate =new List<Case>();
    List<Id> IdList = new List<Id>();
    
   
    for(AgentWork aw : Trigger.New)  {
        if(aw.WorkItemId.getSObjectType().getDescribe().getName() == 'Case'){
            if(Trigger.oldMap.get(aw.Id).AcceptDateTime == null){
                IdList.add(aw.WorkItemId);
                System.debug('hello'+aw);
            }
        }
    }
    
    //only update cases whose status is "Received" to "Open" OR "Re-Received" to "Reopened"
    for(Case cs:[Select id, Status,Routed_From__c From Case WHERE ID IN: IdList AND (Status = 'Received' OR Status = 'Re-Received')]){
        if(cs.Status == 'Received' ){   
            cs.Status  = 'Open';
        }else if(cs.Status == 'Re-Received'){
            cs.Status = 'Reopened';
        } 
        caseToUpdate.add(cs) ;
    }
    if(caseToUpdate.size()>0){
        update caseToUpdate;
    }
    
}