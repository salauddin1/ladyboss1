trigger OpportunityLineItemCouponCodeAfterInsert on OpportunityLineItem (after insert) {

    //List<OpportunityLineItem> oliList = [SELECT id,Product2.Id,Opportunity.Created_Using__c,Opportunity.Product__c, Opportunity.Coupon_Code__c FROM OpportunityLineItem WHERE id in : Trigger.new AND Opportunity.Created_Using__c!=null AND Opportunity.Created_Using__c!='' AND Opportunity.Created_Using__c=:'V13'AND Opportunity.Coupon_Code__c!=null AND Opportunity.Coupon_Code__c!=''];
    List<Id> productIds = new List<Id>();
    List<Id> opportunityIds = new List<Id>();
    Map<String,String> olicouponmap = new Map<String,String>();
    Map<String,Contact> couponconmap = new Map<String,Contact>();
    Map<id, Decimal> prodpriccemap = new Map<id, Decimal>();
    for(OpportunityLineItem oli: Trigger.new){
        opportunityIds.add(oli.OpportunityId);
    }
    List<Opportunity> oppList = [select id, Created_Using__c, Coupon_Code__c from Opportunity where id IN : opportunityIds AND Created_Using__c != null AND Created_Using__c != '' AND Created_Using__c = 'V13' AND Coupon_Code__c != null AND Coupon_Code__c != '' AND Stripe_Message__c	= 'Success' AND Scheduled_Payment_Date__c = null];
    for(Opportunity opportunity: oppList){
        for (OpportunityLineItem oli : Trigger.new) {
            if (oli.OpportunityId == opportunity.Id) {   
                productIds.add(oli.Product2Id);
                olicouponmap.put(oli.Id, opportunity.Coupon_Code__c);
            }
        }
    }
    for (Contact con : [SELECT id,Name,Email,Pap_RefId__c FROM Contact WHERE Pap_Refid__c IN:olicouponmap.values() AND Pap_Refid__c != null]) {
        couponconmap.put(con.PAP_refid__c, con);
    }
    for(Product2 prod:[SELECT id, Commission_Enabled_Club_Product__c,Price__c FROM Product2 WHERE id IN : productIds AND Commission_Enabled_Club_Product__c = true]) {
        prodpriccemap.put(prod.id,prod.Price__c);
    }
	
    List<Pap_Commission__c> paptoinsert = new List<Pap_Commission__c>();
    for(OpportunityLineItem oli: Trigger.new){
        if (oli.Product2Id != null && prodpriccemap.containsKey(oli.Product2Id)) {
            Pap_Commission__c pap= new Pap_Commission__c();
            pap.Contact__c = couponconmap.get(olicouponmap.get(oli.Id)).id;
            pap.commission__c = prodpriccemap.get(oli.Product2Id)* 33.34/100;
            pap.Commission_Type__c = 'initial';
            pap.Opportunity__c = oli.OpportunityId;
            pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
            pap.Campaign_Id__c = '2605c613';
            pap.affiliate_ref_id__c = olicouponmap.get(oli.Id);
            pap.data1__c = couponconmap.get(olicouponmap.get(oli.Id)).Email;
            pap.data2__c = couponconmap.get(olicouponmap.get(oli.Id)).Name;
            pap.data4__c = String.valueOf(prodpriccemap.get(oli.Product2Id));
            paptoinsert.add(pap);
        }
    }
    System.debug('paptoinsert ==> '+ paptoinsert);
    if (paptoinsert.size()>0) {
        insert paptoinsert;
    }
}