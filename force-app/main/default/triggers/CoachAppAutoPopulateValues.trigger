trigger CoachAppAutoPopulateValues on Coaching_Application__c (before insert) {
    if (trigger.isBefore && trigger.isinsert) {
        for(Coaching_Application__c  cApp : trigger.new) {
        	cApp.Are_You_Willing_To_Invest_In_Yourself__c = cApp.Are_you_willing_to_invest_TEXT__c;
            cApp.How_did_you_hear_about_PRC__c = cApp.How_did_you_hear_about_PRC_TEXT__c;
            cApp.Reasons_You_Appllied__c = cApp.Reason_You_Applied_TEXT__c;
        }
    }
}