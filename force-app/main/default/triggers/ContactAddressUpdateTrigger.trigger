trigger ContactAddressUpdateTrigger on Contact (after insert, before Update, after update) {
    public static boolean insertflag = false;
    if(!ContactTriggerFlag.isContactBatchRunning)  {    
        list<String> Contactlist =new List<String>();
        list<String> Contactlist2 =new List<String>();
        list<String> PhoneDNCList =new List<String>();
        list<String> MobileDNCList =new List<String>();
        if(Trigger.isAfter && Trigger.isInsert){
            insertflag = true;
            ContactAccountCrossObjectWorkflows.UpdateAccountChekBox(trigger.new);
            BlogCampaignMovement.doinsert(trigger.New); 
            RainBowDetoxBuyerCampaignMemberJob.addToRainbowCampaign(Trigger.New);
            ContactAddressHandler.addAddress(trigger.New); 
            
            //=========================Check phone and mobile fields============================
            for(Contact con:Trigger.new){
                List<String> mctagsList = new List<String> ();
                if(con.Marketing_Cloud_Tags__c != null )
                    mctagsList  = con.Marketing_Cloud_Tags__c.split(';');
                if(con.Coaching_Booked_But_Didn_t_Show__c== true || mctagsList.contains('Coaching-Booked But Did Not Show')) {
                    CoachingAppliedBookShowCampaign.addtocochAppliedShowCampJob(Trigger.New);
                }
                
                if(con.Coaching_Applied_But_Didn_t_Book__c == true || mctagsList.contains('Coaching-Applied But Did Not Book')) {
                    CoachingAppliedBookShowCampaign.addtocochAppliedBookCampjob(Trigger.New);
                }
                if(con.MobilePhone!=null){
                    Contactlist.add(con.Id);
                    System.debug('----------------'+Contactlist);
                }
                if(con.Phone!=null){
                    Contactlist2.add(con.Id);
                }
                
            }
            //==========================Check for Phone DNC=======================================
            if(!Contactlist2.isEmpty()){  
                //RpvDNCBatchforPhone rpvPhone=new RpvDNCBatchforPhone(Contactlist2);
                //Database.executeBatch(rpvPhone);
            }
            //===========================Check for Mobile DNC=====================================
            if(!Contactlist.isEmpty()){
                //RpvDNCBatchforMobile rpvMobile= new RpvDNCBatchforMobile(Contactlist);
                //Database.executeBatch(rpvMobile);
            }  
        }
        if(Trigger.isBefore && Trigger.isUpdate){
            
            for(Contact connew : Trigger.new){
                
                if(Trigger.oldMap.get(connew.Id).Phone != Trigger.newMap.get(connew.Id).Phone && connew.Phone!=null) {
                    PhoneDNCList.add(connew.Id);  
                    if(!PhoneDNCList.isEmpty()){  
                        //RpvDNCBatchforPhone rpvPhone=new RpvDNCBatchforPhone(PhoneDNCList);
                        //Database.executeBatch(rpvPhone);            
                    }
                }
                else if(connew.Phone == null){
                    connew.Phone_National_DNC__c=false;
                    connew.Phone_State_DNC__c=false;
                }
            }
            
            for(Contact connew : Trigger.new){
                if(Trigger.oldMap.get(connew.Id).MobilePhone != Trigger.newMap.get(connew.Id).MobilePhone && connew.MobilePhone!=null) {
                    MobileDNCList.add(connew.Id);
                    if(!MobileDNCList.isEmpty()){
                        //RpvDNCBatchforMobile rpvMobile= new RpvDNCBatchforMobile(MobileDNCList);
                        //Database.executeBatch(rpvMobile);
                    }               
                }
                else if(connew.MobilePhone == null){
                    connew.DoNotCall=false;
                    connew.Mobile_State_DNC__c=false;
                }
            }
        }
        
    }  
    if(trigger.isAfter){ 
        if(trigger.isUpdate){
            for(Contact connew : Trigger.new){
                if(Trigger.oldMap.get(connew.Id).sf4twitter__Origin__c != Trigger.newMap.get(connew.Id).sf4twitter__Origin__c&& connew.sf4twitter__Origin__c!=null) {
                    
                    //ContactCampaignUpdateTrigger_Handler.doBeforeUpdate(trigger.New, trigger.OldMap);  
                    
                    BlogCampaignMovement.doinsert(trigger.New);
                }
                if(Trigger.oldMap.get(connew.Id).Marketing_Cloud_Tags__c != Trigger.newMap.get(connew.Id).Marketing_Cloud_Tags__c && connew.Marketing_Cloud_Tags__c!=null) {
                    RainBowDetoxBuyerCampaignMemberJob.addToRainbowCampaign(Trigger.New);
                    CoachingAppliedBookShowCampaign.addtocochAppliedBookCampjob(Trigger.New);
                    CoachingAppliedBookShowCampaign.addtocochAppliedShowCampJob(Trigger.New);
                }
                if(Trigger.oldMap.get(connew.Id).Coaching_Applied_But_Didn_t_Book__c != Trigger.newMap.get(connew.Id).Coaching_Applied_But_Didn_t_Book__c && connew.Coaching_Applied_But_Didn_t_Book__c == true) {
                    CoachingAppliedBookShowCampaign.addtocochAppliedBookCampjob(Trigger.New);
                }
                if(Trigger.oldMap.get(connew.Id).Coaching_Booked_But_Didn_t_Show__c != Trigger.newMap.get(connew.Id).Coaching_Booked_But_Didn_t_Show__c && connew.Coaching_Booked_But_Didn_t_Show__c== true) {
                    CoachingAppliedBookShowCampaign.addtocochAppliedShowCampJob(Trigger.New);
                }
                if(insertflag == false && Trigger.oldMap.get(connew.Id).Ready_in_Tipalti__c != Trigger.newMap.get(connew.Id).Ready_in_Tipalti__c && connew.Ready_in_Tipalti__c == true) {
                    ContactAccountCrossObjectWorkflows.UpdateAccountChekBox(trigger.new);
                }
            }
        }
    }
}