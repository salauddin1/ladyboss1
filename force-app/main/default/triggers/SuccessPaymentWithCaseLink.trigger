//Trigger on after update updatecardstatus field to payment successful 
trigger SuccessPaymentWithCaseLink on Opportunity (after insert) {
    List<Triggers_activation__c> op = [select id, SuccessPaymentWithCaseLink__c,isCampaignAssociate__c from Triggers_activation__c];
    try{
        
        //This is to stop trigger firing or to avoid updates or to go into the triger body execution
        if(op[0].SuccessPaymentWithCaseLink__c){
            Set<Id> caseIdSet = new set<id>();
            Set<Id> oppIdSet = new set<id>();
            Set<string> custId  = new set<String>();
            Set<String>  emailID = new Set<String>();
            Set<Decimal>  amountSet = new Set<Decimal>();
            Map<string,List<case>> mapcaseList = new Map<string,List<case>>();
            Map<string,List<case>> mapCoachingCaseList = new Map<string,List<case>>();
            for(Opportunity opp : Trigger.new) {
                oppIdSet.add(opp.id);
            }
            List<Opportunity> oppList = [select id,CustomerID__c, Amount, Stripe_Charge_Id__c,contact__r.email,Scheduled_Payment_Date__c from opportunity where ID IN:oppIdSet];
            for(Opportunity opp : oppList) {
                if((opp.Contact__r.email!=null && opp.CustomerID__c!=null && opp.CustomerID__c!='') && ((opp.Stripe_Charge_Id__c!=null && opp.Stripe_Charge_Id__c!='') || opp.Scheduled_Payment_Date__c !=null)){
                    emailID.add(opp.Contact__r.email);
                    amountSet.add(opp.Amount);
                    System.debug(' test data emailID opp.Contact__r.email '+opp.Contact__r.email);
                }
                
            }
            
            //   System.debug(' test data emailID '+emailID);
            
            
            
            //Get all the cases belongs to the customer
            //List<Case> caseList = [select id ,UpdateCardStatus__c, CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c from case where contact.Name!=null and IsFailed_Payment__c=true and (Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c!='') and Stripe_Customer_Id__c IN:custId and type='Account Recovery' and UpdateCardStatus__c!='Payment Successful' limit 10000 ];
            
            List<Case> caseList = [select id ,UpdateCardStatus__c,Contact.email, Successful_Payment_DateTime__c,CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c from case where  IsFailed_Payment__c=true and (Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c!='') and ( Contact.Email!=null and  Contact.Email IN :emailID) and type='Account Recovery' and UpdateCardStatus__c!='Payment Successful' limit 10000 ];
            List<Case> coachingCaseList = new List<Case>();
            coachingCaseList = [select id,UpdateCardStatus__c, Successful_Payment_DateTime__c, Contact.email, Charge_Id__c, CoachingChargeSucceeded__c, Statement_Descriptor__c, Subject, Opportunity__c, Invoice_Id__c, Failed_Amount__c, ContactEmail from Case where Statement_Descriptor__c != null and CoachingChargeSucceeded__c = false and Failed_Amount__c != null and Failed_Amount__c =: amountSet and opportunity__c = null  and Is_Coaching_Failed__c = true  and ContactEmail =: emailID and ContactEmail != null];
            System.debug(' test data coachingCaseList '+coachingCaseList);
            
            for(Case cs : caseList) {
                if(!mapcaseList.containsKey(cs.Contact.email)) {
                    List<Case> csList = new List<Case> ();
                    csList.add(cs);
                    mapcaseList.put(cs.Contact.email,csList);
                }else{
                    List<Case> csList = new List<Case> ();
                    csList = mapcaseList.get(cs.Contact.email);
                    csList.add(cs);
                    mapcaseList.put(cs.Contact.email,csList);
                }
            }
            for(Case cs : coachingCaseList) {
                if(!mapCoachingCaseList.containsKey(cs.Contact.email)) {
                    List<Case> csList = new List<Case> ();
                    csList.add(cs);
                    mapCoachingCaseList.put(cs.Contact.email,csList);
                }else{
                    List<Case> csList = new List<Case> ();
                    csList = mapCoachingCaseList.get(cs.Contact.email);
                    csList.add(cs);
                    mapCoachingCaseList.put(cs.Contact.email,csList);
                }
            }
            List<Case> caseListUpdate = new List<Case>();
            List<Case> coachingCaseListUpdate = new List<Case>();
            //Updates if successful payment
            for(Opportunity opp : oppList) {
                
                if(mapcaseList.containsKey(opp.Contact__r.email)) {
                    List<Case> caseList1  = new List<Case>();
                    caseList1 =mapcaseList.get(opp.Contact__r.email);
                    if(caseList1 != null && !caseList1.isEmpty()){
                        
                        for(Case c : caseList1) {
                            
                            c.UpdateCardStatus__c = 'Payment Successful';
                            c.Opportunity__c = opp.id;
                            c.Successful_Payment_DateTime__c = System.Now();
                            caseListUpdate.add(c);
                            
                        }
                    }
                }
                
                if(mapCoachingCaseList != null && !mapCoachingCaseList.keySet().isEmpty() && mapCoachingCaseList.containsKey(opp.Contact__r.email)) {
                    List<Case> caseList1  = new List<Case>();
                    caseList1 =mapCoachingCaseList.get(opp.Contact__r.email);
                    System.debug(' test data caseList1 '+caseList1);
                    if(caseList1 != null && !caseList1.isEmpty()){
                        for(Case c : caseList1) {
                            
                            c.UpdateCardStatus__c = 'Payment Successful';
                            c.Opportunity__c = opp.id;
                            c.Successful_Payment_DateTime__c = System.Now();
                            c.CoachingChargeSucceeded__c = true;
                            coachingCaseListUpdate.add(c);
                            
                        }
                    }
                }
            }
            
            if(caseListUpdate.size()>0){
                update caseListUpdate;
            }
            if(coachingCaseListUpdate.size()>0){
                update coachingCaseListUpdate;
            }
            
        }
        if(op[0].isCampaignAssociate__c){
            AssociateOpportunityCampaign.updateCampaignUpdate(Trigger.new);
        }
    }catch(Exception e){
        ApexDebugLog apex=new ApexDebugLog();
        apex.createLog(new ApexDebugLog.Error('SuccessPaymentWithCaseLink','SuccessPaymentWithCaseLink','',e));
    }
}