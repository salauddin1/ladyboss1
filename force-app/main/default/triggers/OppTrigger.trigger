trigger OppTrigger on Opportunity (after insert,after update) {
    System.debug('Coming Here');
    if(Trigger.isInsert){
        System.debug('---insert---');
    }
    if(Trigger.isUpdate){
        System.debug('---update---');
    }
    Set<Id> contIds = new Set<Id>();
    for(Opportunity op : Trigger.New){
        contIds.add(op.Contact__c);
    }
    if(contIds.size()>0){
        Set<String> contemails = new Set<String>();
        List<Contact> cntList = [SELECT id,Email FROM Contact WHERE id in : contIds];
        for(Contact ct : cntList){
            contemails.add(ct.Email);
        }
    }
    
}