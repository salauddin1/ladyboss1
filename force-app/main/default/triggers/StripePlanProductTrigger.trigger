trigger StripePlanProductTrigger on Product2 (after insert, after update) {
    
    if(ContactTriggerFlag.isContactBatchRunning != true && ApexUtil.isTriggerInvoked==false){
        if(trigger.isInsert){
            List<Id> lstProductId = new List<Id>();
            for(Product2 pro : trigger.new){
                if(pro.Type__c=='Stripe'){
                    lstProductId.add(pro.Id);
                }
            }
            
            if(lstProductId.size()>0){
                Map<String,String> mapPbe = new Map<String,String>();
                for(PricebookEntry pbe : [Select Id, Product2.Id, Product2.External_ID__c From PricebookEntry WHERE Product2Id IN: lstProductId]){
                    mapPbe.put(pbe.Product2.Id,pbe.Id);
                }
                
                String pbId = '';
                if(!Test.isRunningTest()){
                    pbId = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
                }else{
                    pbId = Test.getStandardPricebookId();
                }
                
                PricebookEntry pbeNew;
                List<PricebookEntry> lstPbe = new List<PricebookEntry>();
                for(Product2 pro : trigger.new){
                    if(mapPbe.get(pro.Id)==null){
                        if(pro.Price__c!=null){
                            pbeNew = new PricebookEntry();
                            pbeNew.IsActive = true;
                            pbeNew.Pricebook2Id = pbId;
                            pbeNew.Product2Id = pro.Id;
                            pbeNew.UnitPrice = pro.Price__c;
                            lstPbe.add(pbeNew);
                        }
                    }
                }
                
                if(lstPbe.size()>0) insert lstPbe;
                
                Map<String,String> mapPlan;
                for(Product2 pro : trigger.new){
                    if(pro.Type__c=='Stripe'){
                        mapPlan = new Map<String,String>();
                        mapPlan.put('metadata[salesforce_id]',pro.Id);
                        mapPlan.put('name',pro.Name);
                        mapPlan.put('currency',pro.Currency__c);
                        mapPlan.put('interval',pro.Interval__c);
                        if(pro.Interval_Count__c!=null) mapPlan.put('interval_count',String.valueOf(pro.Interval_Count__c));
                        if(pro.Price__c!=null) mapPlan.put('amount',String.valueOf(pro.Price__c).replace('.',''));
                       // if(pro.Statement_Descriptor__c!='' && pro.Statement_Descriptor__c!=null) mapPlan.put('statement_descriptor',pro.Statement_Descriptor__c);
                        //if(!Test.isRunningTest()) StripeConnection.HttpSend('plans','',pro.Id, mapPlan);
                    }
                }
            }
        }
        
        if(trigger.isUpdate){
            Map<String,String> mapPlan;
            for(Product2 pro : trigger.new){
                if(pro.Type__c=='Stripe'){
                    if(pro.External_ID__c==null){
                        mapPlan = new Map<String,String>();
                        mapPlan.put('metadata[salesforce_id]',pro.Id);
                        mapPlan.put('name',pro.Name);
                        mapPlan.put('currency',pro.Currency__c);
                        mapPlan.put('interval',pro.Interval__c);
                        if(pro.Interval_Count__c!=null) mapPlan.put('interval_count',String.valueOf(pro.Interval_Count__c));
                        if(pro.Price__c!=null) mapPlan.put('amount',String.valueOf(pro.Price__c).replace('.',''));
                        if(pro.Statement_Descriptor__c!='' && pro.Statement_Descriptor__c!=null) mapPlan.put('statement_descriptor',pro.Statement_Descriptor__c);
                        //if(!Test.isRunningTest()) StripeConnection.HttpSend('plans','',pro.Id, mapPlan);
                    }else{
                        if(pro.Name!=trigger.oldMap.get(pro.Id).Name || pro.Statement_Descriptor__c!=trigger.oldMap.get(pro.Id).Statement_Descriptor__c){
                            mapPlan = new Map<String,String>();
                            mapPlan.put('name',pro.Name);
                            if(pro.Statement_Descriptor__c!='' && pro.Statement_Descriptor__c!=null) mapPlan.put('statement_descriptor',pro.Statement_Descriptor__c);
                            //if(!Test.isRunningTest()) StripeConnection.HttpSend('plans',pro.External_ID__c,pro.Id, mapPlan);
                        }
                    }
                }
            }
        }
    }
}