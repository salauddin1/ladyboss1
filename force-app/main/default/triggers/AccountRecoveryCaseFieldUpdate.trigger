trigger AccountRecoveryCaseFieldUpdate on Case (before insert,before update) {
    
    
    if(trigger.isbefore  && trigger.isInsert){
        for(Case cs : trigger.new) {
            if(cs.Text_Message__c == true) {
                cs.Text_Message_LastModifiedDate__c = System.now();
            }
             if(cs.Recovered_Payment__c == true) {
                cs.Recovered_Payment_LastModified__c = System.now();
                
            }
        }
    }
     if(trigger.isbefore  && trigger.isUpdate){
        for(Case cs : trigger.new) {
            if(cs.Text_Message__c == true  && cs.Text_Message__c != trigger.oldMap.get(cs.Id).Text_Message__c) {
                cs.Text_Message_LastModifiedDate__c = System.now();
            }
             if(cs.Recovered_Payment__c == true && cs.Recovered_Payment__c != trigger.oldMap.get(cs.Id).Recovered_Payment__c) {
                cs.Recovered_Payment_LastModified__c = System.now();
                
            }
        }
    }
    
}