trigger CaseOwnerTrigger on Case (before update,before insert, after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true && trigger.isBefore){        
        if(trigger.isUpdate){
            CaseOwnerTriggerHandler.doBeforeUpdate(trigger.New);  
        }
        if (trigger.isInsert) {
            CaseOmniChannelHandler.setCaseOwner(trigger.new);
        }
    }else if (ContactTriggerFlag.isContactBatchRunning != true && trigger.isAfter) {
        if (trigger.isUpdate) {
            CaseOmniChannelHandler.updateCaseOwner(trigger.new, trigger.oldMap);
        }
    }
}