/*
 * Developer name : Tirth Patel
 * Description : This trigger stores stripe charge id at the time of opportunity creation.
 */
trigger OpportunityBeforeTrigger on Opportunity (before insert) {
    for(Opportunity opp : Trigger.new){
        if(opp.Stripe_Charge_Id__c !=null){
            opp.StripeChargeCreationId__c = opp.stripe_charge_id__c;
        }
    }
}