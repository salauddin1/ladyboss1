/*
 * Developer Name : Tirth Patel
 * Description : This trigger updates current week and current refund week fields on opportunity. The week and refund week are counted with respect to 31-12-2018 but current week and 
 * current refund week are the week values with respect to current year.
*/
trigger OpportunityWeekTrigger on Opportunity (after insert,after update) {
    if(ContactTriggerFlag.isContactBatchRunning != true){
        try{
            if(OpportunityWeekTriggerFlag.runOnce==true){
                List<Opportunity> oppList = [SELECT id,refund_week__c,Scheduled_Payment_Date__c,CreatedDate,Current_Week__c,Week__c,Refunded_Date__c,Current_Refund_Week__c FROM Opportunity WHERE Id in : Trigger.New];
                for( Opportunity opp : oppList){
                    DateTime dt;
                    Time tm = Time.newInstance(0, 0, 0, 0);
                    if(opp.Week__c>0){
                        if(opp.Current_Week__c==null || opp.Current_Week__c==0){
                            if(opp.Scheduled_Payment_Date__c!=null){
                                dt = DateTime.newInstance(opp.Scheduled_Payment_Date__c, tm);
                            }else{
                                dt = opp.CreatedDate;
                            }
                            opp.Current_Week__c = Integer.valueOf(dt.addDays(-1).format('w'));
                        }
                    }
                    if(opp.refund_week__c>0){
                        dt = DateTime.newInstance(opp.Refunded_Date__c, tm);
                        if(opp.Current_Refund_Week__c==null || opp.Current_Refund_Week__c==0){
                            opp.Current_Refund_Week__c = Integer.valueOf(dt.addDays(-1).format('w'));
                        }
                    }
                }
                OpportunityWeekTriggerFlag.runOnce=false;
                update oppList;
                if(Test.isRunningTest()){
                    System.debug(1/0);
                }
            }
        }Catch(Exception ex){
     ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OpportunityWeekTrigger','trigger',trigger.new[0].id,ex) );
        }
    }
    
}