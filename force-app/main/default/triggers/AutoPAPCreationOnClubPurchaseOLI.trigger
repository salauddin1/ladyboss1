trigger AutoPAPCreationOnClubPurchaseOLI on OpportunityLineItem (before insert,after insert) {
    System.debug('***********AutoPAPCreationOnClubPurchaseOLI**********');
    List<Id> contactIds = new List<Id>();
    List<Id> opportunityIds = new List<Id>();
    List<Id> productIds = new List<Id>();
    for(OpportunityLineItem opp: trigger.new){
        opportunityIds.add(opp.OpportunityId);
        productIds.add(opp.Product2Id);
    }
    List<Product2> productList = [select id,Is_Eligible_For_Affiliate_Creation__c,price__c,Commission_Enabled_Club_Product__c from Product2 where id in:productIds];
    Map<id,Product2> productMap = new Map<Id,Product2>(productList);
    List<Opportunity> oppList = [select id,Contact__c,wc_order_id__c,WC_Product_Id__c,RecordTypeId,WC_Order_Number__c  from Opportunity where id in: opportunityIds and Contact__c!=null];
    Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>(oppList);
    for(OpportunityLineItem opp: trigger.new){
        if(productMap.get(opp.Product2Id).Is_Eligible_For_Affiliate_Creation__c && oppMap.containsKey(opp.OpportunityId) ){
            contactIds.add(oppMap.get(opp.OpportunityId).Contact__c);
        }
    }
    List<Contact> contactList = [select id,email,PAP_refId__c from Contact where id in: contactIds];
    Map<Id,Contact> contactMap = new Map<Id,Contact>(contactList);
    List<String> emailList = new List<String>();
    for(Contact ct:contactList){
        emailList.add(ct.Email);
    }
    list<Contact> emailContactList = [select id,email,PAP_refid__c from Contact where PAP_refid__c != null AND email in :emailList];
    Map<String,List<Contact>> contactEmailMap = new Map<String,List<Contact>>();
    for(Contact ct : emailContactList){
        if(!contactEmailMap.containsKey(ct.Email)){
            contactEmailMap.put(ct.Email, new List<Contact>());
        }
        contactEmailMap.get(ct.Email).add(ct);
    }
    List<Opportunity> createAffiliateList = new List<Opportunity>();
    for(OpportunityLineItem opp: trigger.new){
        if(productMap.get(opp.Product2Id).Is_Eligible_For_Affiliate_Creation__c && oppMap.containsKey(opp.OpportunityId) && 
           oppMap.get(opp.OpportunityId).Contact__c!=null && contactMap.containsKey(oppMap.get(opp.OpportunityId).Contact__c)){
            String email = contactMap.get(oppMap.get(opp.OpportunityId).Contact__c).Email;
            if((!contactEmailMap.containsKey(email)) || (contactEmailMap.containsKey(email) && contactEmailMap.get(email).size()<1)){
                createAffiliateList.add(oppMap.get(opp.OpportunityId));
            }
        }
    }
    
    Map<Id,OpportunityLineItem> oliMap = new Map<Id,OpportunityLineItem>();
    for(OpportunityLineItem oli:Trigger.new){
        oliMap.put(oli.OpportunityId,oli);
    }
    List<Id> contactIds2 = new List<Id>();
    Map<Id,Id> oppContactIdsMap = new Map<Id,Id>();
    for(Opportunity opp:oppList){
        contactIds2.add(opp.Contact__c);
        oppContactIdsMap.put(opp.Contact__c,opp.Id);
    }
    List<Contact> contactList2 = [select id,PAP_refid__c,Email,Name,Available_Commission__c from Contact where id in : contactIds2];
    Map<Id,Contact> oppContactMap = new Map<Id,Contact>();
    for(Contact ct:contactList2){
        if(oppContactIdsMap.containsKey(ct.Id)){
            oppContactMap.put(oppContactIdsMap.get(ct.Id),ct);
        }
    }
    if(Trigger.isbefore && createAffiliateList.size()>0){
        String jsonBody = JSON.serialize(createAffiliateList);
        if(!test.isRunningTest()){
            ID jobID = System.enqueueJob(new createAffiliateController(jsonBody));
        }
        List<Pap_Commission__c> insertPapList = new List<Pap_Commission__c>();
        List<Contact> updateCommissionList = new List<Contact>();
        for(Opportunity opp:oppList){
            if(oppContactMap.containsKey(opp.Id) 
               && (oppContactMap.get(opp.Id).PAP_refid__c == null
                   || oppContactMap.get(opp.Id).PAP_refid__c == '')
               && productMap.get(oliMap.get(opp.Id).Product2Id).Commission_Enabled_Club_Product__c 
               && productMap.get(oliMap.get(opp.Id).Product2Id).Is_Eligible_For_Affiliate_Creation__c){
                   Pap_Commission__c pap = new Pap_Commission__c();
                   pap.Contact__c = oppContactMap.get(opp.Id).Id;
                   pap.commission__c = (productMap.get(oliMap.get(opp.Id).Product2Id).price__c * 20)/100;
                   pap.Commission_Type__c = 'initial';
                   pap.Processed__c = true;
                   pap.Opportunity__c = opp.Id;
                   pap.order_id__c = opp.wc_order_id__c;
                   pap.product_id__c = opp.WC_Product_Id__c;
                   pap.data1__c = oppContactMap.get(opp.Id).Email;
                   pap.data2__c = oppContactMap.get(opp.Id).Name;
                   pap.data4__c = String.valueOf(productMap.get(oliMap.get(opp.Id).Product2Id).price__c);
                   pap.data5__c = opp.WC_Order_Number__c;
                   pap.Campaign_Id__c = '2605c613';
                   pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
                   insertPapList.add(pap);
                   Contact ct = oppContactMap.get(opp.Id);
                   if (ct.Available_Commission__c == null) {
                       ct.Available_Commission__c = 0;
                       ct.Update_Balance_Reason__c  = ' ';
                   }
                   ct.Available_Commission__c = ct.Available_Commission__c + pap.commission__c;
                   ct.Update_Balance_Reason__c  = 'Self purchase';
                   updateCommissionList.add(ct);
               }
        }
        if(insertPapList.size()>0){
            insert insertPapList;
        }
        if(updateCommissionList.size()>0){
            update updateCommissionList;
        }
        
    }
    if(trigger.isafter){
        List<Pap_Commission__c> updatePapList = [select id,Opportunity__c from 
                                                 Pap_Commission__c where 
                                                 Opportunity__c in: opportunityIds 
                                                 order by createddate desc];
        Map<Id,Pap_Commission__c> lastCreatedPapMap = new Map<Id,Pap_Commission__c>();
        if(updatePapList.size()>0){
            for(Pap_Commission__c pap:updatePapList){
                if(!lastCreatedPapMap.containsKey(pap.Opportunity__c)){
                    lastCreatedPapMap.put(pap.Opportunity__c,pap);
                }
            }
            update lastCreatedPapMap.values();
        }
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        if(oppList.size()>0){
            List<Pap_Commission__c> insertPapList2 = new List<Pap_Commission__c>();
            for(Opportunity opp:oppList){
                if(opp.RecordTypeId == recordTypeId 
                   && oppContactMap.containsKey(opp.Id) 
                   && oppContactMap.get(opp.Id).PAP_refid__c != null 
                   && oppContactMap.get(opp.Id).PAP_refid__c != ''
                   && productMap.get(oliMap.get(opp.Id).Product2Id).Commission_Enabled_Club_Product__c){
                       Pap_Commission__c pap = new Pap_Commission__c();
                       pap.Contact__c = oppContactMap.get(opp.Id).Id;
                       pap.commission__c = (productMap.get(oliMap.get(opp.Id).Product2Id).price__c * 33.34)/100;
                       pap.Commission_Type__c = 'initial';
                       pap.Opportunity__c = opp.Id;
                       pap.affiliate_ref_id__c = oppContactMap.get(opp.Id).PAP_refid__c;
                       pap.order_id__c = opp.wc_order_id__c;
                       pap.product_id__c = opp.WC_Product_Id__c;
                       pap.data1__c = oppContactMap.get(opp.Id).Email;
                       pap.data2__c = oppContactMap.get(opp.Id).Name;
                       pap.data4__c = String.valueOf(productMap.get(oliMap.get(opp.Id).Product2Id).price__c);
                       pap.data5__c = opp.WC_Order_Number__c;
                       pap.Campaign_Id__c = '2605c613';
                       pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
                       insertPapList2.add(pap);
                   }
            }
            if(insertPapList2.size()>0){
                insert insertPapList2;
            }
        }
    }
}