//Trigger to send survey email to contacts and leads who were in phone call with support for more than 2 minutes
trigger SendEmailviaTask on Task (After insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true  ){
        //if(UserInfo.getUserName() == 'tirth@ladyboss.com' || Test.isRunningTest() ){
        map<Id,ID> mapId2Contact = new map<Id,ID>();
        map<Id,Id> mapId2Lead = new map<Id,Id>();
        List<Id> userIdList = new List<Id>();
        List<Id> conIdList = new List<Id>();
        Map<Id,Id> ownerConMap = new Map<Id,Id>();
        Map<Id,Id> userTaskMap = new Map<Id,Id>();
        List<Task> taskForJourney = new List<Task>();
        Map<String,Decimal> idDurationMap = new Map<String,Decimal>();
        List<Phone_2_minute_Survey_agents__c> ph2minlist  = [Select id, Call_Duration__c,Salesforce_ID__c FROM Phone_2_minute_Survey_agents__c];
        for(Phone_2_minute_Survey_agents__c p: ph2minlist){
        	for(Task tsk : trigger.new){
                if(tsk.WhoId!=null){
                	String whoID = ((String)tsk.WhoId).substring(0,3); 
        			if(String.valueOf(tsk.ownerId).contains(p.Salesforce_ID__c) && whoID == '003'){
        				taskForJourney.add(tsk);
	        			idDurationMap.put(tsk.OwnerId,p.Call_Duration__c);
    	    		}    
                }
	      	}
        }
        for(Task tsk : trigger.new){
        	if(!taskForJourney.contains(tsk)){
        		if(tsk.WhoId != null && tsk.Five9__Five9AgentExtension__c != null && tsk.ownerId != null && tsk.CallDurationInSeconds !=null && tsk.CallDurationInSeconds > 120){
	                userIdList.add(tsk.ownerId);
	                userTaskMap.put(tsk.Id,tsk.ownerId);
	                String whoID = ((String)tsk.WhoId).substring(0,3);  
	                if (whoID == '003') {
	                    mapId2Contact.put(tsk.Id,tsk.WhoId);
	                } else if (whoID == '00Q') {
	                    mapId2Lead.put(tsk.Id,tsk.WhoId);    
	                }
	            }  
        	}              
        }
        for(Task tsk:taskForJourney){
        	if(tsk.WhoId != null && tsk.Five9__Five9AgentExtension__c != null && tsk.ownerId != null && tsk.CallDurationInSeconds !=null && tsk.CallDurationInSeconds > idDurationMap.get(tsk.ownerId)){
                String whoID = ((String)tsk.WhoId).substring(0,3);  
                if (whoID == '003') {
                    conIdList.add(tsk.WhoId);
		    		ownerConMap.put(tsk.WhoId,tsk.ownerId);
                } 
            }
        }
        Map<Id,User> usersMap = new Map<Id,User>([SELECT id,username FROM USER WHERE Id IN :userIdList]);
        Set<String> userNameList = new Set<String>();
        for(String key : usersMap.keyset()){
            userNameList.add(usersMap.get(key).username);
        }
        List<Email_Templates_For_User__mdt> emailTemplateMetaList = [SELECT DeveloperName,Id,isActive__c,Label,Language,MasterLabel,NamespacePrefix,QualifiedApiName,Template_Name__c,Username__c FROM Email_Templates_For_User__mdt where isActive__c = true and Username__c IN :userNameList];
        if(Test.isRunningTest()){
            emailTemplateMetaList = [SELECT DeveloperName,Id,isActive__c,Label,Language,MasterLabel,NamespacePrefix,QualifiedApiName,Template_Name__c,Username__c FROM Email_Templates_For_User__mdt where Username__c IN :userNameList];
        }
        Map<String,Email_Templates_For_User__mdt> userNameEmaillMap = new Map<String,Email_Templates_For_User__mdt>();
        Set<String> emailTemplateData = new Set<String>();
        for(Email_Templates_For_User__mdt emailTempData : emailTemplateMetaList){
            userNameEmaillMap.put(emailTempData.Username__c,emailTempData);
            emailTemplateData.add(emailTempData.Template_Name__c);
        }
        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        Map<Id,Contact> contacttoSendEmailMap = new Map<Id,Contact>([Select firstname,lastname,email,id,name,MobilePhone, owner.email from Contact where id in : mapId2Contact.values()]);
        Map<Id,Lead> leadtoSendEmailMap = new Map<Id,Lead>([Select firstname,lastname,email,id,name,MobilePhone, owner.email from Lead where id in : mapId2Lead.values()]);
        List<EmailTemplate> emailTemplateList = [Select Body,DeveloperName,HtmlValue ,Id,Name,Subject from EmailTemplate where DeveloperName IN :emailTemplateData];
        Map<String,EmailTemplate> emailMap = new Map<String,EmailTemplate>();
        for(EmailTemplate email : emailTemplateList){
            emailMap.put(email.DeveloperName,email);
        }
        
        for(Id key : userTaskMap.keySet()){
            if(userNameEmaillMap.get(usersMap.get(userTaskMap.get(key)).userName) != null){
                EmailTemplate et = emailMap.get(userNameEmaillMap.get(usersMap.get(userTaskMap.get(key)).userName).Template_Name__c);
                if(et!=null){
                    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    mail.setSaveAsActivity(false);
                    mail.setSubject(et.Subject);
                    String body = et.HtmlValue;
                    if(body!=null){
                        String email = null;
                        String fname = null;
                        String lname = null;
                        List<String> toeamils = new List<String>();
                        if(contacttoSendEmailMap.get(mapId2Contact.get(key)) != null){
                            Contact cn = contacttoSendEmailMap.get(mapId2Contact.get(key));
                            toeamils.add(cn.Email);
                            email = cn.Email;
                            fname = cn.firstname;
                            lname = cn.lastname;
                        }
                        if(leadtoSendEmailMap.get(mapId2Lead.get(key)) != null){
                            Lead ld = leadtoSendEmailMap.get(mapId2Lead.get(key));
                            toeamils.add(ld.Email);
                            email = ld.Email;
                            fname = ld.firstname;
                            lname = ld.lastname;
                        }
                        
                        body = body.replaceAll('<<EMAIL>>',email!=null?email:'');
                        body = body.replaceAll('<<FNAME>>',fname!=null?fname:'');
                        body = body.replaceAll('<<LNAME>>',lname!=null?lname:'');
                        mail.setHtmlBody(body);
                        allmsg.add(mail);
                        mail.setToAddresses(toeamils);
                    } 
                }
                
            } 
        }
        if(allmsg.size() > 0)
            Messaging.sendEmail(allmsg,false);
        //Inserting Case  for Marketing cloud (Customer journey) for phone call more than 2 minutes survey
        List<Case> caseToInsert = new List<Case>();
        for(Contact con:[Select id,Email From Contact where ID IN: conIdList ]){
        	Case cs = new Case(); 
        	cs.Call_Greater_Than_2_Minutes__c = true;
	        cs.OwnerId = ownerConMap.get(con.id);
	       	cs.Status = 'Closed';
	       	cs.Subject = 'Phone Call Greater than 2 minutes';
	       	cs.ContactId = con.id;
	       	cs.Contact_Email__c = con.Email;
	       	caseToInsert.add(cs);
        }
        if(caseToInsert.size()>0){
        	insert caseToInsert;
        }
    }
}