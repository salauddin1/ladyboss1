trigger OliValidationTrigger on OpportunityLineItem (before insert) {
    Set<String> lstOpportunityId = new Set<String>();
    Set<String> lstPricebookEntry = new Set<String>();
    for(OpportunityLineItem oli : trigger.new){
        lstOpportunityId.add(oli.OpportunityId);
        lstPricebookEntry.add(oli.PricebookEntryId);
    }
    
    system.debug('###lstOpportunityId: '+lstOpportunityId);
    system.debug('###lstPricebookEntry: '+lstPricebookEntry);
    
    Map<String,String> mapProductId = new Map<String,String>();
    Map<String,String> mapPbe = new Map<String,String>();
    for(PricebookEntry pbe : [Select Id, Product2.Id From PricebookEntry WHERE Id IN: lstPricebookEntry]){
        mapProductId.put(pbe.Product2.Id,pbe.Id);
        mapPbe.put(pbe.Id,pbe.Product2.Id);
    }
    
    system.debug('###mapProductId: '+mapProductId);
    system.debug('###mapPbe: '+mapPbe);
    
    Map<String,Product2> mapProduct = new Map<String,Product2>();
    for(Product2 pro : [Select Id, Type__c, One_Time_Payment__c  From Product2 WHERE Id IN: mapProductId.keySet()]){
        mapProduct.put(pro.Id, pro);
    }
    
    system.debug('###mapProduct: '+mapProduct);
    
    Map<String,Boolean> mapOppValidation = new Map<String,Boolean>();
    Map<String,Boolean> mapOppValidationStripe = new Map<String,Boolean>();
    Boolean bStripe;
    Boolean bNMI;
    Boolean bOther;
    Integer contStripeOneTime;
    Integer contStripe;
    for(Opportunity opp : [Select Id, (Select Id, PricebookEntry.Product2.Type__c, PricebookEntry.Product2.One_Time_Payment__c From OpportunityLineItems) From Opportunity WHERE Id IN: lstOpportunityId]){
        contStripeOneTime = 0;
        contStripe = 0;
        bStripe = false;
        bNMI = false;
        bOther = false;
        for(OpportunityLineItem oli : opp.OpportunityLineItems){
            system.debug('---oli.PricebookEntry.Product2.Type__c: '+oli.PricebookEntry.Product2.Type__c);
            if(oli.PricebookEntry.Product2.Type__c=='Stripe'){
                bStripe = true;
                if(oli.PricebookEntry.Product2.One_Time_Payment__c){
                    contStripeOneTime++;
                }else{
                    contStripe++;
                }
            }else if(oli.PricebookEntry.Product2.Type__c=='NMI'){
                bNMI = true;
            }else{
                bOther = true;
            }
        }
        
        for(OpportunityLineItem oli : trigger.new){
            if(oli.OpportunityId == opp.Id){
                system.debug('---mapProduct.get(mapPbe.get(oli.PricebookEntryId): '+mapProduct.get(mapPbe.get(oli.PricebookEntryId)));
                if(mapProduct.get(mapPbe.get(oli.PricebookEntryId)).Type__c=='Stripe'){
                    bStripe = true;
                    if(mapProduct.get(mapPbe.get(oli.PricebookEntryId)).One_Time_Payment__c){
                        contStripeOneTime++;
                    }else{
                        contStripe++;
                    }
                }else if(mapProduct.get(mapPbe.get(oli.PricebookEntryId)).Type__c=='NMI'){
                    bNMI = true;
                }else{
                    bOther = true;
                }
            }
        }
        
        if(bStripe==true && bNMI==false && bOther==false){
            if(contStripeOneTime>0 && contStripe>0){
                mapOppValidationStripe.put(opp.Id,true);
            }
        }else if((bStripe==true && bNMI==true) || (bStripe==true && bOther==true) || (bNMI==true && bOther==true)){
            mapOppValidation.put(opp.Id,true);
        }
    }
    
    system.debug('###mapOppValidation: '+mapOppValidation);
    for(OpportunityLineItem oli : trigger.new){
        if(mapOppValidation.get(oli.OpportunityId)!=null){
            oli.addError('You can not add two types of products to this opportunity.');
        }
        if(mapOppValidationStripe.get(oli.OpportunityId)!=null){
            oli.addError('This opportunity can only have one time only products.');
        }
    }

}