trigger LeadCampaignUpdateTrigger on Lead (before insert, after insert,before update) {
    if(ContactTriggerFlag.isContactBatchRunning != true  ){
        if (Trigger.isbefore && Trigger.isInsert) {
            UpdatePicklistValueFromWufoo.populateAllPicklist(Trigger.new);
        }
        if(trigger.isAfter){
            if(trigger.isInsert){
                LeadCampaignUpdateTrigger_Handler.doAfterInsert(trigger.New);     
            }
        }
        if(trigger.isBefore){        
            if(trigger.isUpdate){
                LeadCampaignUpdateTrigger_Handler.doBeforeUpdate(trigger.New,trigger.OldMap);  
                for(lead l : trigger.new) {
                    if(Trigger.oldMap.get(l.Id).status != Trigger.newMap.get(l.Id).status && l.status == 'Prospect' && l.All_Documents_Collected__c == false) {
                        l.All_Documents_Collected__c = true;
                    }                  
                }
            }
        }
    }
}