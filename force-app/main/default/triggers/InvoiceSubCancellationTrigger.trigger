trigger InvoiceSubCancellationTrigger on Invoice__c (after insert) {
    if(ContactTriggerFlag.isContactBatchRunning != true ){
        try{
            Map<String,List<Invoice__c>> subinvMap = new Map<String,List<Invoice__c>>();
            Set<String> setSubIDs = new Set<String>();
            For(Invoice__c inv : Trigger.new){
                if(inv.Subscription_Id__c != null && inv.Charge_Id__c != null && inv.Subscription_Id__c != '' && inv.Charge_Id__c != ''){
                    setSubIDs.add(inv.Subscription_Id__c);
                }
            }
            List<Invoice__c> invList = [Select id,Subscription_Id__c,Charge_Id__c,CreatedDate,Invoice_Id__c from Invoice__c where (Charge_Id__c!= null and Charge_Id__c!='' and Subscription_Id__c!=null and Subscription_Id__c!='') and Subscription_Id__c in : setSubIDs];
            For(Invoice__c inv : invList){
                if(!subinvMap.containsKey(inv.Subscription_Id__c))  {
                    subinvMap.put(inv.Subscription_Id__c, new List<Invoice__c>());
                }
                subinvMap.get(inv.Subscription_Id__c).add(inv);
            }
            
            List<OpportunityLineItem> oppLineItemList = [SELECT id,OpportunityId,Subscription_Id__c,Number_Of_Payments__c,Stripe_Charge_Id__c FROM OpportunityLineItem where Subscription_Id__c != null and Subscription_Id__c in : subinvMap.keySet()];
            List<OpportunityLineItem> oppLineItemToUpdate = new List<OpportunityLineItem>();
            Set<Id> setIDs = new Set<Id>();
            For(OpportunityLineItem oppLItem : oppLineItemList){
                if(subinvMap.containsKey(oppLItem.Subscription_Id__c))  {
                    if(setIDs.add(oppLItem.Id))  {
                        decimal tempval = subinvMap.get(oppLItem.Subscription_Id__c).size();
                        system.debug('---temp---'+tempval);
                        datetime dt ;
                        String chId='';
                        for(Invoice__c invRec : subinvMap.get(oppLItem.Subscription_Id__c)){
                            if(dt == null){
                                dt = invRec.createddate;
                                chId = invRec.Charge_Id__c;
                            }else if(dt != null && dt < invRec.createddate){
                                dt = invRec.createddate;
                                chId = invRec.Charge_Id__c;
                            }
                        }
                        if(chId != null && chId != '' && oppLItem.Stripe_charge_id__c == null){
                            oppLineItemToUpdate.add(new OpportunityLineItem(Id=oppLItem.Id,Stripe_charge_id__c =chId,Number_Of_Payments__c=tempval)); 
                        }else{
                            oppLineItemToUpdate.add(new OpportunityLineItem(Id=oppLItem.Id,Number_Of_Payments__c=tempval)); 
                        }
                        
                    }
                }
            }
            if(oppLineItemToUpdate.size() > 0)  {
                //PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
                update oppLineItemToUpdate;
            }
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('InvoiceSubCancellationTrigger','trigger','',ex) );
        }
    }
}