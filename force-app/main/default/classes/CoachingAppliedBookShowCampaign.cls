// Developer Name :- Surendra Patidar
// class contains code about add contact to the 'Coaching-Applied But Did Not Book' AND 'Coaching-Booked But Did Not Show' campaign
// =================================================================================================================================
public class CoachingAppliedBookShowCampaign {
    public static void addtocochAppliedBookCampjob (List<Contact> con) {
        // veriable initialization
        try{
            Set<String> conSet = new Set<String> ();
            List<CampaignMember> cmember = new List<CampaignMember>();
            Map<String, CampaignMember> cmMap = new Map<String, CampaignMember>();
            Map<String, contact> conMap = new Map<String, Contact>();
            Set<String> conIdMap = new Set<String> ();
            List<String> mctagsList = new List<String> ();
            List<CampaignMember> cmrList = new  List<CampaignMember>() ;
            
            // filter all 'Coaching-Applied But Did't Book' tag contacts
            if(con != null && !con.isEmpty()){
                for(contact c : con){
                    if(c.Coaching_Applied_But_Didn_t_Book__c == true){
                        conMap.put(c.Id, c);
                        conset.add(c.Id);
                    }
                    if(c.Marketing_Cloud_Tags__c != null){
                        mctagsList = c.Marketing_Cloud_Tags__c.split(';');
                        System.debug('=========='+mctagsList);
                        if(mctagsList.contains('Coaching-Applied But Did Not Book')) {
                            conMap.put(c.Id, c);
                            conset.add(c.Id);
                        }
                    }
                }
            }
            System.debug('======consetconsetconsetconset======='+conset);
            // Query and check contact inside 'Coaching-Applied But Did Not Book' campaign
            if(conset != null && !conset.isEmpty()) {
                cmember = [SELECT Id, CampaignId,Campaign.Name, ContactId, Status, Name FROM CampaignMember where ContactId!=null and ContactId IN : conset and Campaign.Name = 'Coaching-Applied But Did Not Book'];
                if(cmember != null && !cmember.isEmpty()) {
                    for(CampaignMember cmp : cmember) {
                        cmMap.put(cmp.ContactId,cmp);
                    }
                    
                }
                for(String conId : conset) {
                    if(!cmMap.containsKey(conId)) {
                        conIdMap.add(conId);
                    }
                }
            }
            // Query 'Coaching-Applied But Did Not Book' campaign
            List<campaign> cmp = [select id,name,type from campaign where Type = 'Coaching Team Follow Up' AND name = 'Coaching-Applied But Did Not Book' limit 1];
            Map <String,campaign> cmpMap = new Map <String,campaign>();
            for(campaign cmpgn : cmp) {
                cmpMap.put(cmpgn.Name, cmpgn);
            }
            // add campaign member to the 'Coaching-Applied But Did Not Book' campaign
            if(cmp != null && !cmp.isEmpty() && conMap != null && !conMap.isEmpty()){
                if(conIdMap != null && !conIdMap.isEmpty()) {
                    for(String str : conIdMap) {
                        contact ct = conMap.get(str);
                        List<String> mcTags = new List<String>();
                        if(ct.Marketing_Cloud_Tags__c != null)
                            mcTags = ct.Marketing_Cloud_Tags__c.split(';');
                        if(ct.Coaching_Applied_But_Didn_t_Book__c == true || mcTags.contains('Coaching-Applied But Did Not Book')){
                            campaign cochBookCampaign = cmpMap.get('Coaching-Applied But Did Not Book');
                            CampaignMember newCM = new CampaignMember(CampaignId = cochBookCampaign.Id,ContactId = str,status = 'Sent' );
                            cmrList.add(newCM);
                        }
                    }
                }
            }
            System.debug('====cmrList====='+cmrList);
            if(cmrList.size() > 0  ) {
                //Insert the campaign member
                database.insert (cmrList, false);
                System.debug('====cmrList====='+cmrList);
            }
        }
        catch(Exception e){
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(con.size()>0) {   for(contact c : con) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('addtocochAppliedBookCampjob','CoachingAppliedBookShowCampaign',Ids, e));
        }
    }
    public static void addtocochAppliedShowCampJob (List<Contact> con) {
        
        // veriable initialization
        try{
            Set<String> conSet = new Set<String> ();
            List<CampaignMember> cmember = new List<CampaignMember>();
            Map<String, CampaignMember> cmMap = new Map<String, CampaignMember>();
            Map<String, contact> conMap = new Map<String, Contact>();
            Set<String> conIdMap = new Set<String> ();
            List<String> mctagsList = new List<String> ();
            List<CampaignMember> cmrList = new  List<CampaignMember>() ;
            
            // filter all 'Coaching-Booked But Did't Show' tag contact
            if(con != null && !con.isEmpty()){
                for(contact c : con){
                    if(c.Coaching_Booked_But_Didn_t_Show__c  == true ){
                        conMap.put(c.Id, c);
                        conset.add(c.Id);
                    }
                    if(c.Marketing_Cloud_Tags__c != null){
                        mctagsList = c.Marketing_Cloud_Tags__c.split(';');
                        System.debug('=========='+mctagsList);
                        if(mctagsList.contains('Coaching-Booked But Did Not Show')) {
                            conMap.put(c.Id, c);
                            conset.add(c.Id);
                        }
                    }
                }
            }
            //Query and check if any contact campaignmember is inside the 'Coaching-Booked But Did Not Show' campaign
            if(conset != null && !conset.isEmpty()) {
                cmember = [SELECT Id, CampaignId,Campaign.Name, ContactId, Status, Name FROM CampaignMember where ContactId!=null and ContactId IN : conset AND Campaign.Name = 'Coaching-Booked But Did Not Show'];
                if(cmember != null && !cmember.isEmpty()) {
                    for(CampaignMember cmp : cmember) {
                        cmMap.put(cmp.ContactId,cmp);
                    }
                    
                }
                for(String conId : conset) {
                    if(!cmMap.containsKey(conId)) {
                        conIdMap.add(conId);
                    }
                }
            }
            // Query 'Coaching-Booked But Did Not Show' campaign
            List<campaign> cmp = [select id,name,type from campaign where Type = 'Coaching Team Follow Up' AND name ='Coaching-Booked But Did Not Show' limit 1];
            Map <String,campaign> cmpMap = new Map <String,campaign>();
            for(campaign cmpgn : cmp) {
                cmpMap.put(cmpgn.Name, cmpgn);
            }
            // add campaign member to the 'Coaching-Booked But Did Not Show' campaign
            if(cmp != null && !cmp.isEmpty()){
                if(conIdMap != null && !conIdMap.isEmpty() && conMap != null && !conMap.isEmpty()) {
                    for(String str : conIdMap) {
                        contact ct = conMap.get(str);
                        List<String> mcTags = new List<String>();
                        if(ct.Marketing_Cloud_Tags__c != null)
                            mcTags = ct.Marketing_Cloud_Tags__c.split(';');
                        if(ct.Coaching_Booked_But_Didn_t_Show__c  == true || mcTags.contains('Coaching-Booked But Did Not Show')){
                            campaign cochBookCampaign2 = cmpMap.get('Coaching-Booked But Did Not Show');
                            CampaignMember newCM = new CampaignMember(CampaignId = cochBookCampaign2.Id,ContactId = str,status = 'Sent' );
                            cmrList.add(newCM);
                        }
                    }
                }
            }
            System.debug('====cmrList====='+cmrList);
            if(cmrList.size() > 0  ) {
                //Insert the campaign member
                database.insert (cmrList, false);
                System.debug('====cmrList====='+cmrList);
            }
        }
        catch(Exception e){
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(con.size()>0) {   for(contact c : con) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('addtocochAppliedShowCampJob ','CoachingAppliedBookShowCampaign',Ids, e));
        }
    }
    // this method is Update the contact checkboxes whenever someone joins the specified campaign's
    public static void UpdateFlagmanullyAddtoCampaign (List<CampaignMember> cmbr) {
        Set<String> idSet = new Set<String>();
        Set<String> conidSet = new Set<String>();
        Map<String, contact> conRMap = new Map<String, contact>();
        List<Contact> cmpList = new List<Contact> ();
        for(CampaignMember cm: cmbr) {
            idSet.add(cm.Id);
            conidSet.add(cm.ContactId);
        }
        // Query Contact and Campaignmember records
        List<Campaignmember> cmp = [select id, campaign.name,ContactId from campaignmember where Id IN: idSet AND (campaign.name ='Coaching-Booked But Did Not Show' OR campaign.name ='Coaching-Applied But Did Not Book') limit 2];
        List<Contact> con = [select id, Marketing_Cloud_Tags__c, Coaching_Booked_But_Didn_t_Show__c, Coaching_Applied_But_Didn_t_Book__c from contact where Id IN : conidSet];
        if(con.size()>0){
            for(contact cmpbr1 : con) {
                conRMap.put(cmpbr1.Id, cmpbr1);
            }
        }
        if(cmp.size()>0){
            for(campaignmember cmpbr : cmp) {
                if(cmpBr.Campaign.Name == 'Coaching-Booked But Did Not Show') {
                    contact conDetail = conRmap.get(cmpbr.ContactId);
                    if(conDetail.Coaching_Booked_But_Didn_t_Show__c == false) {
                        conDetail.Coaching_Booked_But_Didn_t_Show__c = true;
                    }
                    List<String> mctagList = new List<String> ();
                    if(conDetail.Marketing_Cloud_Tags__c != null)
                        mctagList = conDetail.Marketing_Cloud_Tags__c.split(';');
                    if(!mctagList.contains('Coaching-Booked But Did Not Show')) {
                        if(conDetail.Marketing_Cloud_Tags__c != null) {
                            conDetail.Marketing_Cloud_Tags__c = conDetail.Marketing_Cloud_Tags__c +'; Coaching-Booked But Did Not Show';
                        }
                        else if(conDetail.Marketing_Cloud_Tags__c == null) {
                            conDetail.Marketing_Cloud_Tags__c = 'Coaching-Booked But Did Not Show';
                        }
                        
                    }
                    cmpList.add(conDetail);
                    
                }
                if(cmpBr.Campaign.Name == 'Coaching-Applied But Did Not Book') {
                    contact conDetail = conRmap.get(cmpbr.ContactId);
                    if(conDetail.Coaching_Applied_But_Didn_t_Book__c == false) {
                        conDetail.Coaching_Applied_But_Didn_t_Book__c = true;
                    }
                    List<String> mctagList = new List<String> ();
                    if(conDetail.Marketing_Cloud_Tags__c != null)
                        mctagList = conDetail.Marketing_Cloud_Tags__c.split(';');
                    if(!mctagList.contains('Coaching-Applied But Did Not Book')) {
                        if(conDetail.Marketing_Cloud_Tags__c != null) {
                            conDetail.Marketing_Cloud_Tags__c = conDetail.Marketing_Cloud_Tags__c +'; Coaching-Applied But Did Not Book';
                        }
                        else if(conDetail.Marketing_Cloud_Tags__c == null) {
                            conDetail.Marketing_Cloud_Tags__c = 'Coaching-Applied But Did Not Book';
                        }
                    }
                    cmpList.add(conDetail);
                }
            }
        }
        try{
            if(cmpList.size()>0)
                update cmpList;
        }
        catch(exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(con.size()>0) {   for(campaignMember c : cmbr) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('UpdateFlagmanullyAddtoCampaign ','CoachingAppliedBookShowCampaign',Ids, e));
        }
    }
}