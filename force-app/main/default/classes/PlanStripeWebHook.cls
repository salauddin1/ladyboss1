@RestResource(urlMapping='/PlanStripeWebHook/*')
global without sharing class PlanStripeWebHook {
	@HttpPost
	global static void PlanStripe() {
		if(RestContext.request.requestBody!=null){
            String server = 'Production';
			system.debug('###body: '+RestContext.request.requestBody);
            try{
                StripePlan.StripePlanObject varPlan = (StripePlan.StripePlanObject) json.deserialize(RestContext.request.requestBody.toString().replace('object','object_data').replace('currency','currency_data'),StripePlan.StripePlanObject.class);
                
               	Product2 pro = new Product2();
                pro.Name = varPlan.data.object_data.name;
                pro.IsActive = true;
                pro.External_ID__c = varPlan.data.object_data.id;
                pro.Price__c = varPlan.data.object_data.amount*0.01;
                pro.Currency__c = varPlan.data.object_data.currency_data;
                pro.Interval__c = varPlan.data.object_data.interval;
                pro.Interval_Count__c = varPlan.data.object_data.interval_count;
                pro.Statement_Descriptor__c = varPlan.data.object_data.statement_descriptor;
                pro.Type__c = 'Stripe';
                
                ApexUtil.isTriggerInvoked = true;
                upsert pro External_ID__c;
                ApexUtil.isTriggerInvoked = false;    
                
                Map<String,String> mapPbe = new Map<String,String>();
                for(PricebookEntry pbe : [Select Id, Product2.Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c=: pro.External_ID__c]){
                    mapPbe.put(pbe.Product2.External_ID__c.toLowerCase(),pbe.Id);
                }
                
                String pbId = '';
                if(!Test.isRunningTest()){ pbId = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
                }else{ pbId = Test.getStandardPricebookId();
                }
                
                if(mapPbe.get(pro.External_ID__c.toLowerCase())==null){
                    PricebookEntry pbeNew;
                    pbeNew = new PricebookEntry();
                    pbeNew.IsActive = true;
                    pbeNew.Pricebook2Id = pbId;
                    pbeNew.Product2Id = pro.Id;
                    pbeNew.UnitPrice = pro.Price__c;
                    
                    ApexUtil.isTriggerInvoked = true;
                    insert pbeNew;
                    ApexUtil.isTriggerInvoked = false;
                }
            }catch(Exception e){
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); mail1.setSubject(server+': PlanStripeWebHook Request'); mail1.setHtmlBody(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + RestContext.request.requestBody.ToString()); Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); 
            }
		}
		else{
    		RestResponse res = RestContext.response; res.statusCode = 400;		    
		}
	}
}