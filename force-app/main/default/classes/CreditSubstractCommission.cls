public class CreditSubstractCommission {
    
    @AuraEnabled
    public static String submitCommission(Boolean isDirect,Decimal AmountThatManupulate,Decimal weekNum,String AgentId,String ReasonForAdjustment,Boolean isRefund,String conId,Decimal currentYear){
        Contact ct = [SELECT id FROM Contact WHERE id=:conId LIMIT 1];
        System.debug('COntact Id IS : '+ct);
        System.debug('isRefund : '+isRefund);
        Payment__c pay1 = new Payment__c();
        try{
            System.debug('---isDirect---'+isDirect);
            System.debug('---AmountThatManupulate---'+AmountThatManupulate);
            System.debug('---weekNum---'+weekNum);
            System.debug('---AgentId---'+AgentId);
            System.debug('---ReasonForAdjustment---'+ReasonForAdjustment);
            System.debug('-----currentYear------'+currentYear);
            PaymentToCommissionTriggerFalg.runPayTOcomTrigger = FALSE;
            ContactTriggerFlag.isContactBatchRunning = false;
            Integer calcWeek = Integer.valueOf(weekNum) + (Integer.valueOf(currentYear) - 2019)*52;
            System.debug('----calcWeek---'+calcWeek);
            if(isDirect){
                Comission__c comAgent = new Comission__c();
                comAgent.Reason_For_Adjustment__c = ReasonForAdjustment;
                comAgent.hierarchy__c= 'Direct';
                comAgent.Contact__c= ct.Id;
                comAgent.Current_Week_Non_Direct__c = weekNum;
                comAgent.User__c = AgentId;
                if(isRefund){
                	comAgent.Amount__c = AmountThatManupulate *(-1);
                }else{
                    comAgent.Amount__c =  AmountThatManupulate;
                } 
                comAgent.weeks__c = calcWeek;
                insert comAgent;
                System.debug('---comAgent---'+comAgent);
            }else{
                if(isRefund){
                	pay1.Amount__c = AmountThatManupulate * (-1);
                }else{
                    pay1.Amount__c = AmountThatManupulate;
                }
                pay1.week__c = calcWeek;
                pay1.Current_Week_Non_Direct__c	= weekNum;
                pay1.Payment_Created_By__c = AgentId;
                pay1.Reason_For_Adjustment__c = ReasonForAdjustment;
                pay1.Contact__c = ct.Id;
                PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
                ContactTriggerFlag.isContactBatchRunning = false;
                insert pay1;
                System.debug(pay1.Payment_Created_By__c);
                
                Payment__c p = [select id ,Payment_Created_By__c from Payment__c where id =:pay1.id ];
                System.debug(p.Payment_Created_By__c);
                if(pay1.amount__c > 0){
                    String returnVal = PaymentToComissionTriggerOPHandler.comission(pay1);
                    if(returnVal == null || returnVal == 'null'){
                        return 'no_hierarchy';
                    }else{
                        return returnVal;
                    }
                    //return PaymentToComissionTriggerOPHandler.comission(pay1);
                }else if (pay1.amount__c < 0){
                    String returnVal = PaymentToComissionTriggerOPHandler.refund(pay1);
                    if(returnVal == null || returnVal == 'null'){
                        return 'no_hierarchy';
                    }else{
                        return returnVal;
                    }
                    //return PaymentToComissionTriggerOPHandler.refund(pay1);
                } 
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
                        apex.createLog(new ApexDebugLog.Error('PaymentToCommisionTrigger','trigger',pay1.id,e) );
        }
       return null;
    }
    @AuraEnabled
    public static Decimal currentWeek(){
        Decimal weeks = Decimal.valueOf(System.now().addDays(-1).format('w'));
        /*List<first_monday_of_year__c> firstmonday = [SELECT day__c,month__c,year__c FROM first_monday_of_year__c];
        Date myDate = date.newinstance((Integer)firstmonday.get(0).year__c,(Integer)firstmonday.get(0).month__c, (Integer)firstmonday.get(0).day__c);
        Integer day = myDate.daysBetween(System.today());
        Integer weeks = day/7 + 1; */
        return weeks;
    }
}