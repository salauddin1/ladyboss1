@isTest
public class SmsContactRecord_Test {
    public static testMethod void test1(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jaso@ggmail.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            isActive = true
                           );
        insert usr;
        list<Contact> conList = new list<Contact>();
        contact con = new contact();
        con.lastName ='test';
        con.Phone = '88767675444';
        insert con;
        conList.add(con);
        
        Event sms = new Event();
        sms.Contact__c = conList[0].id;
        sms.Phone__c= '88767675444';
        sms.Sent_By__c = userinfo.getuserId();
        sms.SMS_Initiated_By__c = 'Agent';
        sms.DurationInMinutes = 122;
        sms.ActivityDateTime = system.now();
        sms.Description = 'test SMS';
        sms.isNew__c =true;
        insert sms;
        
        //insert mdt;
        test.startTest();   
        PageReference pageRef = Page.SmsContactRecord; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        SmsContactRecord sm = new SmsContactRecord();
        sm.messageData = 'test Message';
        sm.sendSMS();
        sm.showNumber();
        sm.sendSMSNew();
        sm.reload();
        sm.closePopup();
        sm.showPopup();
        Test.stopTest();
                Twilio_Configuration__mdt lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain' ];

    } 
    public static testMethod void test3(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jaso@ggmail.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            isActive = true
                           );
        insert usr;
        list<Contact> conList = new list<Contact>();
        contact con = new contact();
        con.lastName ='test';
        con.mobilePhone = '88767675444';
        insert con;
        conList.add(con);
        
        Event sms = new Event();
        sms.Contact__c = conList[0].id;
        sms.Phone__c= '88767675444';
        sms.Sent_By__c = userinfo.getuserId();
        sms.SMS_Initiated_By__c = 'Agent';
        sms.DurationInMinutes = 122;
        sms.ActivityDateTime = system.now();
        sms.Description = 'test SMS';
        sms.isNew__c =true;
        insert sms;
        
        //insert mdt;
        test.startTest();   
        PageReference pageRef = Page.SmsContactRecord; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        SmsContactRecord sm = new SmsContactRecord();
        sm.sendSMS();
        sm.showNumber();
        sm.sendSMSNew();
        sm.reload();
        Test.stopTest();
                Twilio_Configuration__mdt lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain' ];

    } 
    public static testMethod void test2(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jaso@ggmail.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            isActive = true
                           );
        insert usr;
        list<Contact> conList = new list<Contact>();
        contact con = new contact();
        con.lastName ='test';
        con.otherPhone = '88767675444';
        insert con;
        conList.add(con);
        
        Event sms = new Event();
        sms.Contact__c = conList[0].id;
        sms.Phone__c= '88767675444';
        sms.Sent_By__c = userinfo.getuserId();
        sms.SMS_Initiated_By__c = 'Agent';
        sms.DurationInMinutes = 122;
        sms.ActivityDateTime = system.now();
        sms.Description = 'test SMS';
        sms.isNew__c =true;
        insert sms;
        
        //insert mdt;
        test.startTest();   
        PageReference pageRef = Page.SmsContactRecord; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        SmsContactRecord sm = new SmsContactRecord();
        sm.sendSMS();
        sm.showNumber();
        sm.sendSMSNew();
        sm.reload();
        Test.stopTest();
                Twilio_Configuration__mdt lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain' ];

    } 
}