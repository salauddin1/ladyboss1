public class ShipstationOrderIterator implements Iterator<Integer>{
    public Integer page;
    public Shipstation_pages__c ord;
    public Integer count;
    public ShipstationOrderIterator(){
        ord = [select Pages__c,End_page__c,Start_Page__c from Shipstation_pages__c limit 1];
        //page =0;
       	page = Integer.valueOf(ord.Start_Page__c);
    }
    public boolean hasNext(){
        
        if(page >=Integer.valueOf(ord.End_page__c)) {
           return false;
       } else {
           return true;
       }
    }
    public Integer next(){
        if(page == Integer.valueOf(ord.End_page__c)){return null;}
        page++;
        return page;
    }
}