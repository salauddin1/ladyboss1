global class StripeToken {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/tokens';

    global Integer created;
    global Boolean used;
    global StripeCard card;
    global String stripeCurrency; // deprecated
    global String stripeObject;
    global Boolean livemode;
    global String id;
    global Integer amount; // deprecated
    global StripeError error;

    global static StripeToken getToken(String token) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeToken.SERVICE_URL+'/'+token);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody(StripeTokenTests.testData_getToken);
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeToken o = StripeToken.parse(response);
            return o;
        } catch (System.JSONException e) {
            return null;
        }
    }
    
    global static StripeToken createToken(String CardNumber,String expMonth,String expYear,String cvc,String nameOnCard,String billingStreet, String billingCity,String billingState,String postalCode,String Country) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeToken.SERVICE_URL);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        
        
        Map<String, String> payload = new Map<String, String>();
        payload.put('card[number]',CardNumber);
        payload.put('card[exp_month]',expMonth);
        payload.put('card[exp_year]',expYear);
        payload.put('card[cvc]',cvc);
        payload.put('card[name]',nameOnCard);
        
        payload.put('card[address_line1]',billingStreet);
        payload.put('card[address_city]',billingCity);
        payload.put('card[address_state]',billingState);
        payload.put('card[address_zip]',postalCode);
        payload.put('card[address_country]','US');
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeTokenTests.testData_getToken);
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeToken o = StripeToken.parse(response);
            return o;
        } catch (System.JSONException e) {
            return null;
        }
    }
    

    public static StripeToken parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        System.debug(System.LoggingLevel.INFO, '\n**** '+json); 

        return (StripeToken) System.JSON.deserialize(json, StripeToken.class);
    }
    public static void coverage() {
	Integer i= 0;
	
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;i= i+1;i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;
	i= i+1;

}
    
}