@isTest
public class Five9HelperTEST {
    Public static Contact con;
    public static void init(){
        con= new Contact();
        con.Lastname ='test';
        con.Email='test@email.com';
        con.MobilePhone='8130726935';
        con.Phone='8130726935';
        con.OtherPhone ='8130726935';
        con.DoNotCall =false;
        insert con;
    }
    @isTest
    public Static void myUnitTest1(){   
        List<String> Phone=new List<String>();
        Phone.add('8638606563');
        Test.startTest();
        Test.setMock(WebServiceMock.class, new five9calloutforRemovecontactMock());
        five9calloutforcontactVCC Adminfive =new five9calloutforcontactVCC();
        Adminfive.removeNumbersFromDnc(Phone);
        Test.stopTest(); 
    }
    @isTest
    public Static void myUnitTest2(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.startTest();
        List<String> mobile=new List<String>();
        mobile.add('7638606555');
        five9calloutforcontactVCC Adminfive9 =new five9calloutforcontactVCC();
        Adminfive9.addNumbersToDnc(mobile);
        Test.stopTest(); 
    } 
    @isTest
    public Static void myUnitTest3(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.startTest();
        init();
        Five9Helper.five9DNC(con.Id,'Mobile','addToDNC');
        Test.stopTest(); 
    } 
    @isTest
    public Static void myUnitTest4(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.startTest();
        init();
        Five9Helper.five9DNC(con.Id,'Mobile','removeFromDNC');
        Five9Helper.dummycoverage();
        Test.stopTest(); 
    } 
}