@isTest
public class DeleteDuplicateLivechallengeMemberTest {

    @isTest 
    public static void test1(){
        
        test.startTest();
        
        Campaign c = new Campaign();
        c.Name = 'LVChBuy-4-6 Days';
        c.Type = 'Phone Team';
        c.Product__c = 'LIVE Challenge';
        insert c;
        
        Campaign c2 = new Campaign();
        c2.Name = 'LVChBuy-4-8 Days';
        c2.Product__c='Unlimited';
        c2.Type = 'Phone Team';
        insert c2;
        
        
        Workflow_Configuration__c dc = new Workflow_Configuration__c();
        dc.Source_Campaign__c = c.id;
        dc.Target_Campaign__c = c2.id;
        dc.Consider_For_CM_Movement__c = true;
        dc.Products__c = 'Unlimited';
        dc.Active__c = true;
        insert dc;
        
        Workflow_Configuration__c dc2= new Workflow_Configuration__c();
        dc2.Source_Campaign__c = c2.id;
        dc2.Target_Campaign__c = c.id;
        dc2.Consider_For_CM_Movement__c = true;
        dc2.Products__c = 'LIVE Challenge';
        dc2.Active__c = true;
        insert dc2;
        
        Contact con1 = new Contact();
        con1.LastName = 'test lName';
        con1.Product__c = 'UTA';
        con1.Website_Products__c = 'Unlimited;LIVE Challenge';	
        con1.First_Website_Sell__c  = true;
        con1.is_process_using_batch__c = false;
        insert con1;
        
        CampaignMember cm2 = new CampaignMember();
        cm2.ContactId = con1.id;
        cm2.CampaignId = c2.id;
        insert cm2;
        
        CampaignMember cm = new CampaignMember();
        cm.ContactId = con1.id;
        cm.CampaignId = c.id;
        insert cm;
        
        test.stopTest();

    }
    
}