// Developer Name :- Surendra Patidar
// class contains code about create task whenever lead spent more then 1 week and one month with organization
// =================================================================================================================
public class AutomatTaskBatchUponLeadDay implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful,Schedulable   {
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //Query lead records basis of hours and hours flags
        return Database.getQueryLocator('select id, name, Age__c, Hours__c,ConvertedAccountId, ConvertedContactId, Email, Status, MobilePhone,Completed24Hours__c,Completed_72_Hours__c,Completed_1_Week__c, Completed_1_Months_And_Above__c from Lead where Age__c >= 7');
    }
    public void execute(SchedulableContext SC) {
    Database.executeBatch(new AutomatTaskBatchUponLeadDay());
   }

    public void execute(Database.BatchableContext BC, List<Lead> scope) {
        System.debug('=========='+scope.size()+'=========='+scope);
        List<task> tskList = new List<task>();
        // extraxts lead records and check for Day between 1 week and 1 monts.
        if (scope != null && scope.size()>0 && !scope.isEmpty()) {
            for (lead leadObj : scope) {
                System.debug('============'+Integer.valueOf(leadObj.Age__c));
                if(Integer.valueOf(leadObj.Age__c) > = 7 && Integer.valueOf(leadObj.Age__c) < 30 && leadObj.Completed_1_Week__c == false) {
                   task tsk = new task ();
                    leadObj.Completed24Hours__c = true;
                    leadObj.Completed_1_Week__c = true;
                    leadObj.Completed_72_Hours__c = true;
                    tsk.whoId = leadObj.Id;
                    tsk.Priority = 'Normal';
                    tsk.status = 'Open';
                    tsk.Subject = leadObj.Name+'spent 1 week with ladyboss';
                    tsk.Email__c = leadObj.Email;
                    tskList.add(tsk);
                }
                if(Integer.valueOf(leadObj.Age__c) > = 30 && Integer.valueOf(leadObj.Age__c) < 90 && leadObj.Completed_1_Months_And_Above__c == false) {
                    task tsk1 = new task ();
                    leadObj.Completed24Hours__c = true;
                    leadObj.Completed_1_Months_And_Above__c = true;
                    leadObj.Completed_1_Week__c = true;
                    leadObj.Completed_72_Hours__c = true;
                    tsk1.whoId = leadObj.Id;
                    tsk1.Priority = 'Normal';
                    tsk1.status = 'Open';
                    tsk1.Subject = leadObj.Name+' spent 1 months with ladyboss';
                    tsk1.Email__c = leadObj.Email;
                    tskList.add(tsk1);
                }
                if(Integer.valueOf(leadObj.Age__c) >= 90) {
                    task tsk2 = new task ();
                    leadObj.Completed24Hours__c = true;
                    leadObj.Completed_1_Months_And_Above__c = true;
                    leadObj.Completed_1_Week__c = true;
                    leadObj.Completed_72_Hours__c = true;
                    leadObj.Created_Date__c = Date.today();
                    tsk2.whoId = leadObj.Id;
                    tsk2.Priority = 'Normal';
                    tsk2.status = 'Open';
                    tsk2.Subject = leadObj.Name+' spent 3 months with ladyboss';
                    tsk2.Email__c = leadObj.Email;
                    tskList.add(tsk2);
                }
            }
            try {
                if(tskList.size()>0) {
                  insert tskList;
                }
                if(scope.size()>0) {
                    update scope;
                }
            }
            catch (exception e) {}
        }
    }
    public void finish(Database.BatchableContext BC) {
        
    }
}