@isTest
global class SubscriptionMock_Test implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id": "cus_EigLGygMwJRJY7", "object": "customer", "email": "sukesh343189@gmail.com", "invoice_prefix": "0BEA302", "invoice_settings": { "custom_fields": null, "footer": null }, "livemode": false, "metadata": {}, "shipping": null, "sources": { "object": "list", "data": [{ "id": "card_1EFF0DFzCf73siP0XnzFgsy9", "object": "card", "tokenization_method": null }], "url": "/v1/customers/cus_EigLGygMwJRJY7/sources" }, "tax_info_verification": null }');
        //res.setBody('{"object":"list","data":[{"id":"card_1DOmyyBwLSk1v1ohdxe48vqP","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_DqTws9VQgpg41e","cvc_check":"pass","dynamic_last4":null,"exp_month":4,"exp_year":2021,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null}],"has_more":false,"url":"/v1/customers/cus_DqTws9VQgpg41e/sources"}');
        res.setStatusCode(200);
        
        return res;
    }
}