//This class is to update the case for updated card survey
public class MC_OneClickSurvey_TrustPilot{
    public string recID;
    public string res;
    public string resServer;
    public string NoRecordIF;
    public string customerComment{get;set;}
    public string username{get;set;}
    public boolean showServerMessage{get;set;}
    public boolean isOther{get;set;}
    public string selectObject{get;set;}
    public string selectCoachingObject{get;set;}
    //Get the value of the picklist from the case
    public List<SelectOption> regions
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Case.OneClick_Customer_Chioce__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public List<SelectOption> goodregions
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Opportunity.OneClick_Customer_Choice_product__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public List<SelectOption> coachingOpt
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Case.OneClick_Customer_Chioce__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public MC_OneClickSurvey_TrustPilot(){
        showServerMessage =false;
        isOther =false;
    }
    //This Method redirects to the respective success or error page
    public PageReference successRedirect() {
        try{
            //Get the record ID and the value
            recID = ApexPages.currentPage().getParameters().get('id');
            res= ApexPages.currentPage().getParameters().get('res');
            Schema.DescribeFieldResult field = Case.One_Click_Survey__c.getDescribe();
            Set<String> myPicklist = new Set<String>();
            for (Schema.PicklistEntry f : field.getPicklistValues()){
                myPicklist.add(f.getLabel());
                
            }
            //Get the cse record
            List<Case> opRec = [select id,One_Click_Survey__c, Is_Coaching_Failed__c ,MC_EmailLinkClicked__c from Case where ID=: recID];
            
            //If case exists checks the survey already updated or not
            if(opRec.size() > 0) {
                if(opRec[0].One_Click_Survey__c!=null && myPicklist.Contains(res)){
                    System.debug('===opRec ==='+opRec );
                    PageReference myVFPage = new PageReference('/apex/MC_ClosedSurvey');
                    myVFPage.setRedirect(true);
                    // myVFPage.getParameters().put('myId', recID );
                    return myVFPage;
                }else{
                    System.debug('===opRec ==='+opRec );
                    //For email link clikced
                    opRec[0].One_Click_Survey__c =res;
                    opRec[0].MC_EmailLinkClicked__c = true;
                    update opRec;
                    if(res=='Excellent'){
                        PageReference myVFPage = new PageReference('/apex/SuccessPage_Amazing_TrustPilot');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('res', res );
                        myVFPage.getParameters().put('id', recID );
                        return myVFPage;
                    }else if(res=='Great'){
                        PageReference myVFPage = new PageReference('/apex/SuccessPage_Green_TrustPilot');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('res', res );
                        myVFPage.getParameters().put('id', recID );
                        return myVFPage;
                    }
                    else if(res=='Ok'){
                        PageReference myVFPage = new PageReference('/apex/SuccessPage_Yellow_TrustPilot');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('id', recID );
                        myVFPage.getParameters().put('res', res );
                        return myVFPage;
                        
                        
                    }else if(res=='Poor' && opRec[0].Is_Coaching_Failed__c==false){
                        PageReference myVFPage = new PageReference('/apex/SuccessPage_Red_TrustPilot');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('res', res );
                        myVFPage.getParameters().put('id', recID );
                        return myVFPage;
                    }
                }
            }else{
                //To redirect to the survey already completed
                PageReference myVFPage = new PageReference('/apex/MC_ClosedSurvey');
                myVFPage.setRedirect(true);
                myVFPage.getParameters().put('NoRecordIF', 'True');
                return myVFPage;
                
            }
            if(test.isRunningTest()){
                integer a = 10/0;  
            }
        }
        catch (Exception e) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_OneClickSurvey',
                    'successRedirect',
                    'recID'+recID,
                    e
                )
            );
            
            
        }
        return null;   
        
    }
    //To update comment for the poor survey
    public void save() {
        try{
            
            recID = ApexPages.currentPage().getParameters().get('id');
            res= ApexPages.currentPage().getParameters().get('res');
            system.debug('------------res----------------'+res);
            showServerMessage=true;
            List<Case> opRec = [select id,One_Click_Survey__c, Is_Coaching_Failed__c ,OneClick_Customer_Comment__c from Case where ID=: recID];
            
            if(opRec.size() > 0) {
                
                if(selectObject!='Other'){
                    if(res=='great'){
                        opRec[0].OneClick_Customer_Choice_product__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = '';
                        system.debug('------------if great--------------');
                    }
                    else{
                        opRec[0].OneClick_Customer_Chioce__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = '';
                    }
                }else{
                    if(res=='great'){
                        system.debug('----------------if great----------');
                        opRec[0].OneClick_Customer_Choice_product__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = username;
                    }
                    else{
                        opRec[0].OneClick_Customer_Chioce__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = username;
                    }
                }
                showServerMessage = true;
                update opRec;
            }
            
            if(test.isRunningTest()){
                integer a = 10/0;  
            }
        }catch (Exception e) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_OneClickSurvey',
                    'save',
                    'recID'+recID,
                    e
                )
            );
        }
        
    }
    //To hide and show the section
    public void changeEvent() {
        showServerMessage = false;
        System.debug('---'+selectObject);
        if(selectObject=='Other'){
            isOther =true;
        }else{
            isOther =false;
        }
        
    }
    
    
    
}