global class OrderShipping {
	global String first_name;
	global String last_name;
	global String company;
	global String address_1;
	global String address_2;
	global String city;
	global String state;
	global String postcode;
	global String country;
}