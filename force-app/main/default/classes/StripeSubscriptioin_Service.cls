@RestResource(urlMapping='/Stripe_SubscriptionService')
global class StripeSubscriptioin_Service {
    @HttpPost
    global static void createCustomer() {
        String customeride;
        if(RestContext.request.requestBody!=null){
            try {
                
                String str = RestContext.request.requestBody.toString();
                System.debug(str);
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                List<Map<String, Object>> data = new List<Map<String, Object>>();
                
                Map<String, Object>  lstCustomers = new Map<String, Object>();
                lstCustomers   = ( Map<String, Object> )results.get('data');
                Map<String, Object> aryCustomers   = new Map<String, Object>();
                aryCustomers   =   (Map<String, Object>) lstCustomers.get('object');
                
                Map<String, Object> itemsMap   = new Map<String, Object>();
                itemsMap   =   (Map<String, Object>) aryCustomers.get('items');
                Map<String, Object> planMap = new Map<String, Object>();
                planMap = (Map<String,Object>)aryCustomers.get('plan');
                String planIde = String.valueOf(planMap.get('id'));
                Decimal taxpercentage = 0.00;
                if(String.valueOf(aryCustomers.get('tax_percent'))!=null){
                    taxpercentage = Decimal.valueOf(String.valueOf(aryCustomers.get('tax_percent')));
                }
                
                List<Object> listPlan =  (List<Object>) itemsMap.get('data');
                Map<String, Object> itemsPlanMap   =  new Map<String, Object>();
                
                
                List<Object> listPlanTemp = new List<Object> ();
                System.debug(itemsPlanMap);
                for(object o : listPlan) {
                    
                    Map<String, Object> castedObject = (Map<String, Object>)o;
                    
                    Object sffFieldNames = castedObject.get('plan');
                    
                    listPlanTemp.add(sffFieldNames);
                }
                
                
                Map<String, Object> requestMap =  new  Map<String, Object>();
                requestMap  = (Map<String, Object>) results.get('request');
                
                String customerId =String.valueOf(aryCustomers .get('customer')); 
                customeride=String.valueOf(aryCustomers .get('id'));
                
                Map<String, Object> dataObj= new Map<String, Object>();
                dataObj= (Map<String, Object>) aryCustomers.get('plan');
                List<Product2> prodList = [SELECT id FROM Product2 WHERE Stripe_plan_Id__c  = :String.valueOf(dataObj.get('id'))];
                List<PricebookEntry> pbe = new List<PricebookEntry>();
                if (prodList.size()>0) {
                    pbe = [SELECT Id FROM PricebookEntry  where Product2ID=: prodList[0].id AND IsActive = true AND Pricebook2.isStandard = TRUE];  
                }
                
                /*List<Stripe_Product_Mapping__c> mcs = Stripe_Product_Mapping__c.getall().values();
Map<string,string> productMap = new Map<string,string>();
Set<String> prodNameSet  = new Set<String>();
for(Stripe_Product_Mapping__c spm: mcs){
if(spm.Stripe_Nick_Name__c!=null)
productMap.put(spm.Stripe_Nick_Name__c,spm.Salesforce_Product__c);
//prodNameSet.add(spm.Stripe_Nick_Name__c);
}

for(Object fld : listPlanTemp){    
Map<String,Object> planData = (Map<String,Object>)fld;
if(productMap.containsKey(String.valueOf(planData.get('nickname'))) ) {
prodNameSet.add(productMap.get(String.valueOf(planData.get('nickname'))));
}
}

//List<Product2> prd  = [SELECT Id, Name FROM Product2 where Name IN: prodNameSet];
Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
//List<PricebookEntry> pbe1 = [SELECT Id, Name, Pricebook2Id, Product2Id,Product2.Name FROM PricebookEntry  where Pricebook2ID=:standardPb.id and Product2ID=: prd.id];
List<PricebookEntry> pbe1 = [SELECT Id, Name, Pricebook2Id, Product2Id,Product2.Name FROM PricebookEntry  where Pricebook2ID=:standardPb.id  and isActive=true];
Map<string,string> priceBookMap = new Map<string,string>();
for(PricebookEntry pe : pbe1) {
priceBookMap.put(pe.Name,pe.ID);
} */
                
                
                
                List<Stripe_Profile__c> lstStripeProf = [select id,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =:customerId ];
                Map<String, Object>  metaDataMap       =   new Map<String, Object>();
                metaDataMap = (Map<String, Object> )aryCustomers.get('metadata');
                string subId = String.valueOf(aryCustomers .get('id')); 
                List<opportunity> lstOpportunity = new List<opportunity>();
                
                //lstOpportunity  = [ SELECT id ,RequestId__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where Subscription__c=: subId OR RequestId__c =: String.valueOf(requestMap.get('id'))];
                lstOpportunity  = [ SELECT id ,RequestId__c,recordtype.Name,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where Subscription__c=: subId];
                
                List<OpportunityLineItem> lstOpli = [SELECT Id,Name,OpportunityId, Subscription_Id__c FROM OpportunityLineItem where Subscription_Id__c =:subId ];
                set<Id> opIdSet = new set<Id>();
                for(OpportunityLineItem oli : lstOpli ) {
                    opIdSet.add(oli.OpportunityId);
                }
                //lstOpportunity  = [ SELECT id ,RequestId__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where Subscription__c=: subId OR ID IN: opIdSet OR RequestId__c =: String.valueOf(requestMap.get('id'))];
                lstOpportunity  = [ SELECT id ,RequestId__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where Subscription__c=: subId OR ID IN: opIdSet ];
                
                String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
                Boolean isSalesforceInitiated = false;
                if(!(metaDataMap== null) || !(metaDataMap.isEmpty())){
                    if(metaDataMap.get('Integration Initiated From') =='Salesforce'){
                        isSalesforceInitiated = true;
                        
                    }
                    
                }
        datetime dt5 = System.now();
                try {
                Long StartedDate = (Long)aryCustomers.get('start_date');
          dt5 = datetime.newInstance(StartedDate* Long.ValueOf('1000'));
                }catch(Exception ex)  {
                    //Ignore null pointer here.
                }
                if(lstOpli.size()>0){
                  lstOpli[0].Started_On__c = dt5.date();
                  update lstOpli;
                }else if(isSalesforceInitiated){
                    Invoice__c inv = new Invoice__c();
                    inv.Subscription_Id__c = subId;
                    inv.Started_On__c = dt5.date();
                    insert inv;
                }
                
                if(lstOpportunity.size() <=0 && lstOpli.size() <=0 && !isSalesforceInitiated && String.valueOf(aryCustomers.get('status')) != 'incomplete') {
                    
                    Opportunity op = new Opportunity ();
                    if(listPlanTemp.size() == 1){
                        String amt = String.ValueOf(dataObj.get('amount'));
                        op.Amount = decimal.valueOf(amt )*0.01;
                        String cardId  = StripeCard.getCard(customerId);
                        if(cardId!=null && cardId !=''){
                            op.cardId__c =cardId  ;
                            List<card__c> cardList = [select id,Stripe_Card_Id__c from card__c where Stripe_Card_Id__c !=null and Stripe_Card_Id__c=: cardId  ];
                            if(cardList.size() > 0){
                                op.card__c = cardList[0].id;
                            }
                        }
                        
                        
                    }
                    //op.Amount = Integer.valueOf(dataObj.get('amount'))*0.01;
                    if(lstStripeProf.size() > 0 ){
                        if(lstStripeProf[0].Customer__c!=null)
                            op.Contact__c = lstStripeProf[0].Customer__c;
                        if(lstStripeProf[0].id!=null)
                            op.Stripe_Profile__c = lstStripeProf[0].id;
                    }
                    
                    op.StageName = 'Closed Won';
                    string unixDatetime = String.valueOf(aryCustomers.get('current_period_end'));
                    datetime dt = datetime.newInstance(Long.ValueOf(unixDatetime )* Long.ValueOf('1000'));
                    op.closeDate = dt.date();
                    
                    string unixDatetime1 = String.valueOf(aryCustomers.get('current_period_start'));
                    datetime dt1 = datetime.newInstance(Long.ValueOf(unixDatetime1 )* Long.ValueOf('1000'));
                    op.Period_Start__c= dt1.date();
                    
                    string unixDatetime2 = String.valueOf(aryCustomers.get('current_period_end'));
                    datetime dt2 = datetime.newInstance(Long.ValueOf(unixDatetime2 )* Long.ValueOf('1000'));
                    op.Period_End__c= dt2.date();
                    //op.closeDate = date.newinstance( dT.month(), dT.day(),dT.year());
                    if(listPlanTemp.size() == 1){
                        op.Name= String.valueOf(dataObj.get('nickname'));
                    }else{
                        op.Name=String.valueOf(aryCustomers .get('id'));
                        
                    }
                    
                    op.Active__c= String.valueOf(aryCustomers.get('status'));
                    op.Subscription__c= String.valueOf(aryCustomers .get('id'));
                    //op.description =customerId ;
                    op.CustomerID__c  = customerId ;
                    op.Status__c =String.valueOf(aryCustomers .get('status')); 
                    op.Clubbed__c = true;
                    op.RequestId__c = String.valueOf(requestMap .get('id'));
                    op.Initiated_From__c = 'Stripe';
                    op.Quantity__c= listPlanTemp.size();
                    if(metaDataMap.containsKey('wc_order_number')){
                        op.Created_Using__c = 'WooCommerce';
                        op.WC_Order_Number__c = String.valueOf(metaDataMap.get('wc_order_number'));
                        op.WC_Order_Id__c = String.valueOf(metaDataMap.get('wc_order_id'));
                        op.Coupon_Code__c = string.valueOf(metaDataMap.get('couponcode'));
                        op.Core_Amount__c = Decimal.valueOf(String.valueOf(metaDataMap.get('Core Amount')));
                        
                   }
                    if(metaDataMap.containsKey('wc_product_id')){
                        op.WC_Product_Id__c = String.valueOf(metaDataMap.get('wc_product_id'));
                    }    
                    if (metaDataMap.containsKey('shipping')) {
                        op.Shipping_Cost__c = Decimal.valueOf(String.valueOf(metaDataMap.get('shipping')));
                    }                
                    if( aryCustomers.get('latest_invoice')!=null){
                        op.Invoice_Id__c  = String.valueOf(aryCustomers.get('latest_invoice'));
                    }
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        op.RecordTypeId = devRecordTypeId ;
                    if(op.Name!= null){
                        insert op;
                        if((op.cardId__c!=null ) && op.Invoice_Id__c !=null && op.card__c==null ) {
                            List<Card_Opp_Temporary__c > cdOppList =[select id,CardId__c,Invoice_Id__c from Card_Opp_Temporary__c  where (CardId__c!=null  and CardId__c=:op.cardId__c) or (Invoice_Id__c!= null and Invoice_Id__c=:op.Invoice_Id__c) ];
                            if(cdOppList.size() <= 0) {
                                Card_Opp_Temporary__c cot=  new Card_Opp_Temporary__c();
                                cot.Invoice_Id__c = op.Invoice_Id__c;
                                cot.CardId__c = op.cardId__c;
                                insert cot;
                            }
                        }
                    }
                    
                    
                    List<OpportunityLineItem> opplist = new List<OpportunityLineItem>();
                    if (pbe.size()>0) {
                        OpportunityLineItem oppli = new OpportunityLineItem(); 
                        
                        oppli.PricebookEntryId= pbe[0].id;
                        oppli.OpportunityId = op.Id;
                        
                        oppli.Subscription_Id__c = String.valueOf(aryCustomers .get('id')); 
                        
                        oppli.Quantity = Integer.valueOf(aryCustomers.get('quantity'));
                        String amt = String.ValueOf(dataObj.get('amount'));
                        oppli.TotalPrice =  decimal.valueOf(amt )*0.01;
                        oppli.Success_Failure_Message__c =String.valueOf(results.get('type'));
                        System.debug('Start Date : '+aryCustomers.get('start_date'));
                        //dt5 = datetime.newInstance(StartedDate* Long.ValueOf('1000'));
                        oppli.Started_On__c = dt5.date();
                        
                        if(aryCustomers.get('trial_start') !=null && aryCustomers.get('trial_end') !=null){
                            String datetime2 = String.valueOf(aryCustomers.get('trial_start'));
                            datetime dT4 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                            oppli.TrialPeriodStart__c =date.newinstance(dT4.year(), dT4.month(), dT4.day());    
                            String datetime3 = String.valueOf(aryCustomers.get('trial_end'));
                            datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                            oppli.TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day()); 
                        }else {
                            Datetime periodStart = datetime.newInstance(Long.ValueOf(String.valueOf(aryCustomers.get('current_period_start')) )* Long.ValueOf('1000'));
                            oppli.Start__c = date.newinstance(periodStart.year(), periodStart.month(), periodStart.day());  
                            Datetime periodEnd = datetime.newInstance(Long.ValueOf(String.valueOf(aryCustomers.get('current_period_end')) )* Long.ValueOf('1000'));
                            oppli.End__c = date.newinstance(periodEnd.year(), periodEnd.month(), periodEnd.day());
                        }
                        oppli.Status__c = String.valueOf(aryCustomers.get('status'));
                        oppli.Subscription_Plan_ID__c = planIde;
                        if (String.valueOf(aryCustomers.get('tax_percent')) != null) {
                            oppli.Tax_Percentage_s__c = taxpercentage;
                            oppli.Tax_Amount__c = Decimal.valueOf(amt)*taxpercentage*0.01*0.01;
                        }
                       
                        opplist.add( oppli);
                    }
                    
                    /* for(Object fld : listPlan){ 
Map<String, Object> castedObject = (Map<String, Object>)fld;
Object sffFieldNames = castedObject.get('plan');
Map<String,Object> planData = (Map<String,Object>)sffFieldNames;

system.debug('=======planData==='+planData);
OpportunityLineItem oppli = new OpportunityLineItem(); 

if(priceBookMap.containsKey(productMap.get(String.valueOf(planData.get('nickname')))) ) {

oppli.PricebookEntryId= priceBookMap.get(productMap.get(String.valueOf(planData.get('nickname'))));
oppli.OpportunityId = op.Id;

oppli.Subscription_Id__c =String.valueOf(castedObject .get('subscription')); 

oppli.Quantity = Integer.valueOf(castedObject.get('quantity'));
String amt = String.ValueOf(planData.get('amount'));
oppli.TotalPrice =  decimal.valueOf(amt )*0.01;
oppli.Success_Failure_Message__c =String.valueOf(results.get('type'));
opplist.add( oppli);
}
}*/
                    if (opplist.size()>0) {
                        System.debug('WE are IN');
                        insert opplist; 
                    }
                    
                    
                }else{
                    if(requestMap.get('id')!=null){
                        if(lstOpportunity.size()>0){
                            lstOpportunity[0].RequestId__c = String.valueOf(requestMap .get('id'));
                            
                            update lstOpportunity;
                        }
                    }
                    
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripeSubscriptioin_Service',
                        'createCustomer',
                        customeride
                    )
                );
            }catch(Exception ex) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeSubscriptioin_Service',
                        'createCustomer',
                        customeride,
                        ex
                    )
                );
                
                
            }
        }
        
    }
    
}