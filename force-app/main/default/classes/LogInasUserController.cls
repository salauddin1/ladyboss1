public class LogInasUserController {
    @AuraEnabled
    public static contactWrapper getLoginDetails(String conId){
        Contact con = [SELECT Id,Email,Password__c,Site_Crypto_Key__c,Name FROM Contact WHERE Id =:conId];
        contactWrapper cw = new contactWrapper();
        cw.Id = con.Id;
        cw.name = con.Name;
        cw.cryptoKey = con.Site_Crypto_Key__c;
        cw.Idparam = con.Id;
        cw.password = con.Password__c;
        return cw;
    }

    public class contactWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String Idparam;
        @AuraEnabled public String name;
        @AuraEnabled public String cryptoKey;
        @AuraEnabled public String password;
    }
    
    public Pagereference createCookies(){
        
            String idparam = ApexPages.currentPage().getParameters().get('conid');           
            PageReference pf= new Pagereference('https://tirthbox-ladyboss-support.cs22.force.com/LadyBossHub/LadyBossHubAccount');
            pf.setRedirect(false);
            pf.getParameters().put('idparam',idparam);
        
        return pf;
    }

}