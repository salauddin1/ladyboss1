// Created By   : Sourabh Badole
// Created Date : 20-03-2019
public class RecoveredAmountController {   
    public Decimal amount           {get;set;}
    public Decimal totalAmount      {get;set;}
    public String strSelectOption   {get;set;}
    public Integer endAmount        {get;set;}
    public Integer colorSetAmount   {get;set;}
    public Integer colorSetSecound  {get;set;}
    public Integer StepSetAmount    {get;set;}
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public List<userAmount> user_amount{get;set;}
    public Boolean showOpp{get;set;}
    
    public RecoveredAmountController (){
        endAmount =10000;
        StepSetAmount = (endAmount >= 1 )? Integer.valueOf(endAmount/10) : 0;
        colorSetAmount = (endAmount >= 1 )? Integer.valueOf(endAmount/20) : 0;
        colorSetSecound = (endAmount >= 1 )? Integer.valueOf(endAmount/5) : 0;
        startDate = System.today().toStartOfWeek().addDays(1);
        endDate = System.Today();
        
    }
    
    public list<SelectOption> getStudOption(){
        List<case> CaseList = [select owner.Name,owner.UserName from case where IsFailed_Payment__c=true and Charge_Id__c!=null AND (Product_Category__c =:'Unlimited' OR Product_Category__c =:'UTA' OR Product_Category__c =:'Supplement') And createdDate >= : startDate AND createdDate <=: endDate and isProcessedForReport__c=true and Product_Category__c != null  ];
        list<SelectOption>  lstOptions2 = new list<SelectOption>();
        lstOptions2.add(new SelectOption('All', 'All'));
        Set<String> OptionSet = new Set<String>();
        if(CaseList != null && !CaseList.isEmpty()){
            for(Case  getCs : CaseList ){
                OptionSet.add(getCs.owner.Name);
            }    
        }
        for(String  getOpt : OptionSet ){
            if(getOpt != null && getOpt != ''){
                lstOptions2.add(new SelectOption(getOpt, getOpt));
            }
        }
        return lstOptions2;
    }
    public void getData() {
        String dynamic_query;
        String dynamic_query2;
        amount =0;
        totalAmount = 0;
        if(strSelectOption =='All' || strSelectOption ==Null){
            dynamic_query = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
            dynamic_query2 = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where  createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Recovered_Payment__c !=false and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
        }else{
            dynamic_query = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where owner.Name=:strSelectOption  and createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
            dynamic_query2 = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where owner.Name=:strSelectOption  and createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Recovered_Payment__c !=false and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
        }
        AggregateResult[] groupedResults = Database.query(dynamic_query); 
        AggregateResult[] groupedResults2 = Database.query(dynamic_query2); 
        
        this.user_amount = new List<userAmount>();
        
        if(groupedResults.size()==0){
            showOpp = false;
        }
        else{
            showOpp = true;
        }
        Map<String,Decimal> RecoveredMap = new Map<String,Decimal>();
        for(AggregateResult res : groupedResults2){ 
            if(res.get('aver')!=null && res.get('Name')!=null){
                RecoveredMap.put(String.valueOf(res.get('Name')),Decimal.valueOf(String.valueOf(res.get('aver'))));
            }
        }
        for(AggregateResult res : groupedResults){
            userAmount ua ;
            if(res.get('aver')!=null && res.get('Name')!=null){
                Decimal checkvalue = 0;
                totalAmount = totalAmount + Decimal.valueOf(String.valueOf(res.get('aver')));
                if(RecoveredMap.containsKey(String.valueOf(res.get('Name')))){
                    checkvalue = RecoveredMap.get(String.valueOf(res.get('Name'))) ;
                    amount = amount + RecoveredMap.get(String.valueOf(res.get('Name'))) ;
                }
                ua = new userAmount(String.valueOf(res.get('Name')),Decimal.valueOf(String.valueOf(res.get('aver'))),checkvalue);
            user_amount.add(ua);
            }
        }
        if(endAmount != null && amount != null && endAmount < Integer.valueOf(amount)){
            endAmount = Integer.valueOf(amount) ;
        }
        StepSetAmount = (endAmount >= 1 )? Integer.valueOf(endAmount/10) : 0;
        colorSetAmount = (endAmount >= 1 )? Integer.valueOf(endAmount/20) : 0;
        colorSetSecound = (endAmount >= 1 )? Integer.valueOf(endAmount/5) : 0;
    }   
    
    public class userAmount{
        public string userName{get;set;}
        public decimal totalAmount{get;set;}
        public decimal recoverdAmount  {get;set;}
        
        public userAmount(string userName,decimal totalAmount,decimal  recoverdAmount  ){
            this.userName = userName;
            this.totalAmount = totalAmount;
            this.recoverdAmount  =recoverdAmount  ;
        }
    }
}