public class FIve9Listitem_Batch_Helper {
    public static batchWrapper updateFive9Item(List<Five9LSP__Five9_List__c> five9List , List<Five9LSP__Five9_List_Item__c> Five9ItemList){
               
        // Filter Five9LSP__Five9_List__c members to be changed and create a map of age       
        Map<Decimal , List<Five9LSP__Five9_List_Item__c>> changeList = new Map<Decimal , List<Five9LSP__Five9_List_Item__c>>();
        for(Five9LSP__Five9_List_Item__c cm : Five9ItemList){
            System.debug('=======================Age==========='+cm);
            if(cm.Five9LSP__Five9_List__r.Min_Age__c != null && cm.Five9LSP__Five9_List__r.Max_Age__c != null){
                if(!(cm.Age__c >= cm.Five9LSP__Five9_List__r.Min_Age__c && cm.Age__c <= cm.Five9LSP__Five9_List__r.Max_Age__c)){
                    if(changeList.containsKey(cm.Age__c)){
                        changeList.get(cm.Age__c).add(cm);
                    }else{
                        changeList.put(cm.Age__c, new List<Five9LSP__Five9_List_Item__c>{cm});
                    }
                }
            }else if(cm.Five9LSP__Five9_List__r.Min_Age__c != null && cm.Five9LSP__Five9_List__r.Max_Age__c == null){
                if(!(cm.Age__c >= cm.Five9LSP__Five9_List__r.Min_Age__c)){
                    if(changeList.containsKey(cm.Age__c)){
                        changeList.get(cm.Age__c).add(cm);
                    }else{
                        changeList.put(cm.Age__c, new List<Five9LSP__Five9_List_Item__c>{cm});
                    }
                }
            }
        }
        system.debug('*** changeList'+changeList);
        
        // Create age Five9LSP__Five9_List__c map for the Five9LSP__Five9_List__c members to changed
        Map<Decimal,String> ageFIve9ListMap = new Map<Decimal,String>();
        for(Five9LSP__Five9_List__c c : five9List){
            for(Decimal age : changeList.keySet()){
                system.debug(c +'' +' '+age);
                if(c.Min_Age__c != null && c.Max_Age__c != null){
                    if(age >= c.Min_Age__c && age <= c.Max_Age__c){
                        ageFIve9ListMap.put(age, c.Id);
                    }
                }else  if(c.Min_Age__c != null && c.Max_Age__c == null){
                    if(age >= c.Min_Age__c){
                        ageFIve9ListMap.put(age, c.Id);
                    }
                }
            }
        }
        system.debug('**** ageFIve9ListMap'+ageFIve9ListMap);
        
        
        // Create Five9LSP__Five9_List__c Members Insert List
        List<Five9LSP__Five9_List_Item__c> deleteList = new List<Five9LSP__Five9_List_Item__c>();
        List<Five9LSP__Five9_List_Item__c> insertList = new List<Five9LSP__Five9_List_Item__c>();
        Five9LSP__Five9_List_Item__c c = new Five9LSP__Five9_List_Item__c();
        for(Decimal age : ageFIve9ListMap.keySet()){
            for(Five9LSP__Five9_List_Item__c cm : changeList.get(age)){
                deleteList.add(cm);
                c= cm.clone();
                c.Five9LSP__Five9_List__c = ageFIve9ListMap.get(age);
                c.Moved_Date__c=Date.today();
                system.debug('*** updated member '+cm.Five9LSP__Five9_List__r.Name);
                insertList.add(c);
            }
        }
        System.debug('*** batch handler delete List*** '+deleteList.size()+' '+deleteList);
        System.debug('*** batch handler Insert List*** '+insertList.size()+' '+insertList);
        
        
        //Sent insert and delete list to queues 
        batchWrapper bw = new batchWrapper();
        bw.CMDeleteList = deleteList;
        System.debug('*** Wrapper deleteList*** '+bw.CMDeleteList.size()+' '+bw.CMDeleteList);
        bw.CMInsertList = insertList;
        System.debug('*** Wrapper InsertList*** '+bw.CMInsertList.size()+' '+bw.CMInsertList);
        return bw;
    }
    
    
    
    public class batchWrapper{
        public List<Five9LSP__Five9_List_Item__c> CMDeleteList;
        public List<Five9LSP__Five9_List_Item__c> CMInsertList;
    }
     public Static void Coverage(){
        Integer i=1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
    }    
}