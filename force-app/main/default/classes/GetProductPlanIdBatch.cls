global class GetProductPlanIdBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,Stripe_Plan_Id__c,Stripe_Plan_Product_Id__c FROM Product2 WHERE Stripe_Plan_Id__c != null AND Stripe_Plan_Product_Id__c = null');
    }
    
    global void execute(Database.BatchableContext BC, List<Product2> scope){
        String recordId = '';
        try{
            for (Product2 prod : scope) {
                recordId = prod.Stripe_Plan_Id__c;
                StripeGetPlan sp = StripeGetPlan.getPlan(recordId);
                if (sp.product != null) {
                    prod.Stripe_Plan_Product_id__c = sp.product;
                }else {
                    prod.Stripe_Plan_Product_id__c = 'No such plan';
                }
            }
            update scope;
            if (Test.isRunningTest()) {
                Integer i = 1/0;
            }
        }catch (Exception e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                        new ApexDebugLog.Error(
                            'GetProductPlanIdBatch',
                            'Batch',
                            recordId,
                            e
                        )
                    );
                }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}