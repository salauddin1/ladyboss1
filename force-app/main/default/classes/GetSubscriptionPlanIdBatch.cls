global class GetSubscriptionPlanIdBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT id,Subscription_id__c,Product_Plan_Id__c,Subscription_Plan_ID__c FROM OpportunityLineItem where Subscription_id__c like \'sub%\'  AND Subscription_Plan_ID__c = null');
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        for (OpportunityLineItem oli: scope) {
            oli.Subscription_Plan_ID__c = oli.Product_Plan_Id__c;
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}