public class ConnectToWooCommerceRest implements Queueable {
    public String jsonBody;
    public ConnectToWooCommerceRest(String jsbody){
        this.jsonBody = jsbody;
    }
    public void execute(QueueableContext context) {
        makePostCallout(jsonBody);
    }
    public static void createPostRequest(List<Contact> couponList) {
        Create_Coupon__c cc = [select id, Amount__c, Discount_Type__c, Product_Categories__c,Product_Exclude_Categories__c from Create_Coupon__c where name = 'default'];
        CouponWrapper cw = new CouponWrapper();
        cw.create = new List<Create>();
        for(Contact ct : couponList){
            Create cr = new Create();
            cr.code = ct.PAP_refid__c;
            if(ct.Coupon_Discount_Type__c != null){
                cr.discount_type = ct.Coupon_Discount_Type__c;
            }else{
                cr.discount_type = cc.Discount_Type__c;
            }
            if(ct.Coupon_Amount__c!= null){
                cr.amount = ct.Coupon_Amount__c;
            }else{
                cr.amount = cc.Amount__c;
            }
            cr.individual_use = true;
            cr.exclude_sale_items = true;
            if(cc.Product_Categories__c != null ){
                if(cc.Product_Categories__c.contains(',') ){
                    List<Integer> pcIdList = new List<Integer>();
                    List<String> pcList = cc.Product_Categories__c.split(',');
                    for(String pcval : pcList){
                        pcIdList.add( Integer.valueOf(pcval) );
                    }
                    if(pcIdList.size() > 0){
                        cr.product_categories = pcIdList;    
                    }
                }
                else{
                    cr.product_categories = new List<Integer>{Integer.valueOf( cc.Product_Categories__c)};
                        }
            }
            if(cc.Product_Exclude_Categories__c != null ){
                if(cc.Product_Exclude_Categories__c.contains(',') ){
                    List<Integer> pcIdList = new List<Integer>();
                    List<String> pcList = cc.Product_Exclude_Categories__c.split(',');
                    for(String pcval : pcList){
                        pcIdList.add( Integer.valueOf(pcval) );
                    }
                    if(pcIdList.size() > 0){
                        cr.excluded_product_categories = pcIdList;    
                    }
                }
                else{
                    cr.excluded_product_categories = new List<Integer>{Integer.valueOf( cc.Product_Exclude_Categories__c)};
                        }
            }
            cw.create.add(cr);
        }
        String body =JSON.serialize(cw);
        System.debug('---------------------Body:-'+Body);
        ID jobID = System.enqueueJob(new ConnectToWooCommerceRest(body));
        
    }
    public static void updatePostRequest(List<Contact> UpCouponList) {
        CouponWrapper cw = new CouponWrapper();
        cw.updateCode = new List<UpdateCode>();
        for(Contact ct : UpCouponList){
            UpdateCode up = new UpdateCode();
            if(ct.PAP_refid__c != null && ct.Coupon_Code_Id__c != null){
                up.id = ct.Coupon_Code_Id__c;
                up.code = ct.PAP_refid__c;
                cw.updateCode.add(up);
            }
        }
        String body =JSON.serialize(cw);
        body = body.replace('updateCode', 'update');
        ID jobID = System.enqueueJob(new ConnectToWooCommerceRest(body));
        
    }
    @future(callout=true)
    public static void makePostCallout(String jsonBody) {
        List<Create_Site_Coupon__c> siteCouponList = [select id,Name,Site_URL__c from Create_Site_Coupon__c];
        HttpResponse response=null;
        for(Create_Site_Coupon__c site: siteCouponList){
            System.debug('jsonBody:-'+jsonBody);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(site.Site_Url__c);
            request.setMethod('POST');
            request.setBody(jsonBody);
            request.setHeader('Content-Type', 'application/json');
            
            if(site.Name == 'Store'){
                response = http.send(request);
                
            }else{
                HttpResponse response2 = http.send(request);
                
            }
        } 
        if (response != null){
            if (response.getStatusCode() == 200) {
                if(response.getBody() != null && response.getBody() != ''){
                    System.debug('response:-'+response.getBody());
                    String body = response.getBody();
                    Map<String,Object> result = (Map<String,Object>)Json.deserializeUntyped(body);
                    if(result != null && result.keySet().contains('create')){
                        List<object> createdCoupons = (List<object>) result.get('create');
                        if(createdCoupons.size() > 0){
                            Map<String,String> pAPandCouponCodeMap = new Map<String,String>();
                            Map<String,String> coupDisTypeMap = new Map<String,String>();
                            Map<String,Decimal> coupAmountMap = new Map<String,Decimal>();
                            for(Object obj : createdCoupons ){
                                Map<String,object> getCodeMap = (Map<String,object>) obj;
                                if(getCodeMap != null && getCodeMap.keySet().size() > 0){
                                    System.debug('getCodeMap:-'+getCodeMap);
                                    if(getCodeMap.keySet().contains('code') && getCodeMap.keySet().contains('id')){
                                        if(getCodeMap.get('code') != null && getCodeMap.get('id') != null){
                                            String coupKey = String.valueOf(getCodeMap.get('code'));
                                            coupKey = coupKey.toLowerCase();
                                            pAPandCouponCodeMap.put( coupKey,String.valueOf(getCodeMap.get('id')));
                                            coupDisTypeMap.put( coupKey,String.valueOf(getCodeMap.get('discount_type')));
                                            coupAmountMap.put( coupKey,Decimal.valueOf(String.valueOf(getCodeMap.get('amount'))));
                                        }    
                                    }
                                }
                                
                            }
                            if(pAPandCouponCodeMap != null && pAPandCouponCodeMap.keyset().size() > 0){
                                List<Contact> ctList = new List<Contact>();
                                ctList = [SELECT id,Coupon_Code_Id__c,Coupon_Discount_Type__c,Coupon_Amount__c,PAP_refid__c,firstname,lastname FROM Contact WHERE PAP_refid__c != null  AND PAP_refid__c =: pAPandCouponCodeMap.keySet()];
                                if(ctList.size()>0){
                                    for(Contact ct : ctList){
                                        String papKey = ct.pap_refid__c ;
                                        papKey = papKey.toLowerCase();
                                        if(pAPandCouponCodeMap.keySet().contains(papKey) && pAPandCouponCodeMap.get(papKey) != null){
                                            ct.Coupon_Code_Id__c = pAPandCouponCodeMap.get(papKey);   
                                            ct.Coupon_Discount_Type__c = coupDisTypeMap.get(papKey);
                                            ct.Coupon_Amount__c = coupAmountMap.get(papKey);
                                        }
                                    }
                                    update ctList;
                                }
                            }
                        }
                        
                    }
                    if(result != null && result.keySet().contains('update')){
                        List<object> UpdateCoupons = (List<object>) result.get('update');
                        if(UpdateCoupons.size() > 0){
                            Map<String,String> pAPandCouponCodeMap = new Map<String,String>();
                            Map<String,String> coupDisTypeMap = new Map<String,String>();
                            Map<String,Decimal> coupAmountMap = new Map<String,Decimal>();
                            for(Object obj : UpdateCoupons ){
                                Map<String,object> getCodeMap = (Map<String,object>) obj;
                                if(getCodeMap != null && getCodeMap.keySet().size() > 0){
                                    if(getCodeMap.keySet().contains('code') && getCodeMap.keySet().contains('id')){
                                        if(getCodeMap.get('code') != null && getCodeMap.get('id') != null){
                                            String coupKey = String.valueOf(getCodeMap.get('code'));
                                            coupKey = coupKey.toLowerCase();
                                            pAPandCouponCodeMap.put( String.valueOf(getCodeMap.get('id')),coupKey);
                                            coupDisTypeMap.put( String.valueOf(getCodeMap.get('id')),String.valueOf(getCodeMap.get('discount_type')));
                                            coupAmountMap.put( String.valueOf(getCodeMap.get('id')),Decimal.valueOf(String.valueOf(getCodeMap.get('amount'))));
                                        }    
                                    }
                                }
                                
                            }
                            if(pAPandCouponCodeMap != null && pAPandCouponCodeMap.keyset().size() > 0){
                                List<Contact> ctList = new List<Contact>();
                                ctList = [SELECT id,Coupon_Code_Id__c,Coupon_Discount_Type__c,Coupon_Amount__c,firstname,lastname FROM Contact WHERE Coupon_Code_Id__c != null  AND Coupon_Code_Id__c =: pAPandCouponCodeMap.keySet()];
                                if(ctList.size()>0){
                                    for(Contact ct : ctList){
                                        String coupKey = ct.Coupon_Code_Id__c ;
                                        if(pAPandCouponCodeMap.keySet().contains(coupKey) && pAPandCouponCodeMap.get(coupKey) != null){
                                            ct.Coupon_Discount_Type__c = coupDisTypeMap.get(coupKey);
                                            ct.Coupon_Amount__c = coupAmountMap.get(coupKey);
                                        }
                                    }
                                    update ctList;
                                }
                            }
                        }
                    }
                }
                
            }
        }
    }
    public class CouponWrapper{
        List<Create> create{get;set;}
        List<UpdateCode> updateCode{get;set;}
        
    }
    public class Create{
        String code{get;set;}
        String discount_type{get;set;}
        Decimal amount{get;set;}
        Boolean individual_use{get;set;}
        Boolean exclude_sale_items{get;set;}
        List<Integer> product_categories {get;set;}
        List<Integer> excluded_product_categories {get;set;}
    }
    public class UpdateCode{
        String id{get;set;}
        String code{get;set;}
        
    }
}