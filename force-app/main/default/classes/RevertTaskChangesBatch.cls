global class RevertTaskChangesBatch implements Database.Batchable<SObject>{
	global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Id,whoid,whatId,Contact__c,Account__c,Opportunity__c,Case__c FROM Task where (Contact__c!= null or Account__c!=null or Case__c !=null or Opportunity__C!=null) and (whoid=null or whatid=null)]);
    }
    global void execute(Database.BatchableContext context, List<Task> records) {
        for(Task t : records){
            if(t.Contact__c != null && t.WhoId == null){
                t.WhoId = t.Contact__c;
            }
            if(t.Account__c != null && t.WhatId == null){
                t.WhatId = t.Account__c;
            }else if(t.Opportunity__c != null && t.WhatId == null){
                t.WhatId = t.Opportunity__c;
            }else if(t.Case__c!=null && t.WhatId == null){
                t.WhatId = t.Case__c;
            }
        }
        update records;
    }
    global void finish(Database.BatchableContext context) {
        
    }
}