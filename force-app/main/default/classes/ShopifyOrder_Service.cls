@RestResource(urlMapping='/ShopifyOrder_Service')
global class ShopifyOrder_Service {
    @HttpPost
    global static void getOrders() {
     if(RestContext.request.requestBody!=null  ){
        try {
            String str = RestContext.request.requestBody.toString();
            System.debug('-------'+str);
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
            Map<String, Object> customerMap = new Map<String, Object>();
            customerMap =  (Map<String, Object>)results.get('customer');
            List<Object> listPlan =  (List<Object>) results.get('line_items');
            List<Object> notesItem =  (List<Object>) results.get('note_attributes');
            System.debug('======================'+notesItem);
            Map<String, Object>  NotesMap = new Map<String, Object>();
            String FlagVar;
            if(!notesItem.isEmpty()){
                for(Object ob: notesItem){
                    NotesMap = (Map<String, Object>)ob;
                }
            }
            FlagVar = String.valueOf(NotesMap.get('name'));
            if(FlagVar != 'InitiatedFromSalesforce') {
                String customerEmail = '';
                customerEmail = String.valueOf(results.get('email')); 
                 List<Contact> conList = new   List<Contact>();
                if(customerEmail !='') {
                conList  = [select id from Contact where email =:customerEmail ];
                }
                String conId ='';   
                Map<String, Object>  custBillingMap = new Map<String, Object>();
                Boolean isShipping = false;
                Boolean isShippingVal = false;
                if(results.ContainsKey('shipping_address')) {
                    isShipping = true;
                }
                Map<String, Object>  custShippingMap = new Map<String, Object>();
                if(isShipping ){
                    if(results.get('shipping_address')!=null) {
                        custShippingMap   = ( Map<String, Object> )results.get('shipping_address');
                        isShippingVal= true;
                    } 
                }
                    if(conList.size() > 0) {
                       conId = conList[0].id;
                       if(isShippingVal){
                            conList[0].MailingStreet = String.valueOf(custShippingMap.get('address1')); 
                            conList[0].MailingCity = String.valueOf(custShippingMap.get('city')); 
                            conList[0].MailingState = String.valueOf(custShippingMap.get('province')); 
                            conList[0].MailingPostalCode = String.valueOf(custShippingMap.get('zip')); 
                            conList[0].MailingCountry = String.valueOf(custShippingMap.get('country')); 
                          }
                        update conList;
                    } else {
                        if(customerEmail !='' && String.valueOf(customerMap.get('last_name'))!=null && String.valueOf(customerMap.get('last_name'))!='') {
                            List<Contact> conListInsert = new  List<Contact>();
                            Contact con =  new Contact();
                            con.email = customerEmail ;
                            con.LastName = String.valueOf(customerMap.get('last_name')); 
                            con.FirstName = String.valueOf(customerMap.get('first_name')); 
                            if(isShippingVal){
                            con.MailingStreet = String.valueOf(custShippingMap.get('address1')); 
                            con.MailingCity = String.valueOf(custShippingMap.get('city')); 
                            con.MailingState = String.valueOf(custShippingMap.get('province')); 
                            con.MailingPostalCode = String.valueOf(custShippingMap.get('zip')); 
                            con.MailingCountry = String.valueOf(custShippingMap.get('country'));
                            con.shopify_customer__c = true;
                            }
                            conListInsert.add(con);
                            insert conListInsert;
                           conId = conListInsert[0].id;
                        }
                    } 
                    if(conId !='') {
                        Shopify_Orders__c order = new Shopify_Orders__c();
                        order.Order_Id__c = String.valueOf(results.get('id'));  
                        order.Contact__c= conId ;
                        order.Order_Number__c=String.valueOf(results.get('order_number'));  
                        String srcString= String.valueOf(results.get('created_at'));
                        String yyyy=srcString.subString(0,4);
                        String mm=srcString.subString(5,7);
                        String dd=srcString.subString(8,10);
                        order.created_at__c =  mm+'/'+dd +'/'+yyyy;
                        String subTotal = String.ValueOf(results.get('subtotal_price'));
                        order.Subtotal_Price__c = decimal.valueOf(subTotal );
                        order.Financial_Status__c= String.valueOf(results.get('financial_status'));
                        String Total_Price = String.ValueOf(results.get('total_price'));
                        order.Total_Price__c = decimal.valueOf(Total_Price );
                        String Total_Tax = String.ValueOf(results.get('total_tax'));
                        order.Total_Tax__c = decimal.valueOf(Total_Tax );
                        order.Status__c = 'Confirmed';
                        String Total_Line_Items_Price = String.ValueOf(results.get('total_line_items_price'));
                        order.Total_Line_Items_Price__c = decimal.valueOf(Total_Line_Items_Price );
                        order.First_Name__c = String.valueOf(custShippingMap.get('first_name'));
                        order.Last_Name__c = String.valueOf(custShippingMap.get('last_name'));
                        order.address1__c = String.valueOf(custShippingMap.get('address1'));
                        order.city__c = String.valueOf(custShippingMap.get('city'));
                        order.province__c = String.valueOf(custShippingMap.get('province'));
                        order.country__c = String.valueOf(custShippingMap.get('country'));
                        order.phone__c = String.valueOf(custShippingMap.get('phone'));
                        order.zip__c = String.valueOf(custShippingMap.get('zip'));
                        insert order;
                        List<ShopifyLineItem__c> lineItemList = new List<ShopifyLineItem__c>();
                        for (Object ob : listPlan ) {
                            ShopifyLineItem__c item = new ShopifyLineItem__c();
                            Map<String,Object> data = (Map<String,Object>)ob;
                            system.debug('======'+data.get('id'));
                            item.Item_Id__c = String.valueOf(data.get('id')); 
                            String PriceVal = String.ValueOf(data.get('price'));
                            item.Price__c = decimal.valueOf(PriceVal );
                            item.Contact__c= conId ;
                            item.Quantity__c = Integer.valueOf(data.get('quantity')); 
                            item.sku__c = String.valueOf(data.get('sku')); 
                            item.Title__c  = String.valueOf(data.get('title')); 
                            item.Name= String.valueOf(data.get('title')); 
                             item.Shopify_Orders__c = order.id;
                             order.Status__c = 'Success';
                             item.Fulfillable_Quantity__c =  Integer.valueOf(data.get('fulfillable_quantity')); 
                            lineItemList.add(item); 
                        }  
                        if(lineItemList.size() > 0) {
                            insert lineItemList;
                        }
                    }
            }
            }catch(Exception e) {System.debug('Exception e '+e);
            System.debug('Exception e '+e.getLineNumber());} 
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
 }