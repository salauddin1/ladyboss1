global class TaxjarTaxCalculate  {
    global cls_tax tax;
    global Decimal status;
    global String error;
    private static Integer count = 0;
	global class cls_tax {
		global Decimal order_total_amount;	//16.5
		global Decimal shipping;	//1.5
		global Decimal taxable_amount;	//15
		global Decimal amount_to_collect;	//1.35
		global Decimal rate;	//0.09
		global boolean has_nexus;
		global boolean freight_taxable;
		global String tax_source;	//destination
	}
    public static TaxjarTaxCalculate ratemethodHub(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint( 'https://api.taxjar.com/v2/taxes');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            if (taxCode == null) {
                taxCode = '99999';
            }
            string body='{ "to_country": "'+country+'", "to_zip": "'+postalCode+'", "to_state": "'+region+'", "to_city": "'+city+'", "to_street": "'+line1+'", "amount": '+amount*quantity+', "shipping": 0, "line_items": [ { "quantity": 1, "unit_price": '+amount*quantity+', "product_tax_code": '+taxCode+' } ] }';
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
            }else{
                hs.setStatusCode(200);
            }
            if(!test.isRunningTest()){
                response = hs.getBody();
                system.debug(response);
            }else{
                response='{ "tax": { "taxable_amount": 15.0, "tax_source": "origin", "shipping": 0.0, "rate": 0.07875, "order_total_amount": 15.0, "jurisdictions": { "state": "NM", "county": "BERNALILLO", "country": "US", "city": "ALBUQUERQUE" }, "has_nexus": true, "freight_taxable": true, "breakdown": { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_tax_rate": 0.05125, "state_tax_collectable": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_tax_collectable": 0.41, "shipping": { "taxable_amount": 0.0, "tax_collectable": 0.0, "state_taxable_amount": 0.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.0, "special_taxable_amount": 0.0, "special_tax_rate": 0.0275, "special_district_amount": 0.0, "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 }, "line_items": [ { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_amount": 0.41, "id": "1", "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 } ], "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_tax_collectable": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_tax_collectable": 0.0 }, "amount_to_collect": 1.18 } }';   
            }
            statusCode = hs.getStatusCode();
            TaxjarTaxCalculate o = TaxjarTaxCalculate.parse(response);
            return o;
        
        }catch (Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'ratemethodHub',
                    '',
                    ex
                )
            );
            return null;
        }
         
    }

    public static TaxjarTaxCalculate ratemethod(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint( 'https://api.taxjar.com/v2/taxes');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            if (taxCode == null) {
                taxCode = '99999';
            }
            string body='{ "to_country": "'+country+'", "to_zip": "'+postalCode+'", "to_state": "'+region+'", "to_city": "'+city+'", "to_street": "'+line1+'", "amount": '+amount*quantity+', "shipping": 0, "line_items": [ { "quantity": 1, "unit_price": '+amount*quantity+', "product_tax_code": '+taxCode+' } ] }';
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
            }else{
                hs.setStatusCode(200);
            }
            if(!test.isRunningTest()){
                response = hs.getBody();
                system.debug(response);
            }else{
                response='{ "tax": { "taxable_amount": 15.0, "tax_source": "origin", "shipping": 0.0, "rate": 0.07875, "order_total_amount": 15.0, "jurisdictions": { "state": "NM", "county": "BERNALILLO", "country": "US", "city": "ALBUQUERQUE" }, "has_nexus": true, "freight_taxable": true, "breakdown": { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_tax_rate": 0.05125, "state_tax_collectable": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_tax_collectable": 0.41, "shipping": { "taxable_amount": 0.0, "tax_collectable": 0.0, "state_taxable_amount": 0.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.0, "special_taxable_amount": 0.0, "special_tax_rate": 0.0275, "special_district_amount": 0.0, "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 }, "line_items": [ { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_amount": 0.41, "id": "1", "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 } ], "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_tax_collectable": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_tax_collectable": 0.0 }, "amount_to_collect": 1.18 } }';   
            }
            statusCode = hs.getStatusCode();
            TaxjarTaxCalculate o = TaxjarTaxCalculate.parse(response);
            
            if ((o.status == 400 || ( o.error != null && o.error != '')) && count == 0) {
                count++;
                o =  ratemethod('LadyBoss Weight Loss 10010 Indian School Rd. NE', 'Albuquerque', 'NM', 'US', '87112', amount, quantity, taxCode);
            }   
            return o;            
           
        } catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'ratemethod',
                    '',
                    ex
                )
            );
            return null;
        }
        
         
    }
    @Future(callout=true)
    public static void ratemethodForOppUpdate(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode,String sub){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint( 'https://api.taxjar.com/v2/taxes');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            string body='{ "to_country": "'+country+'", "to_zip": "'+postalCode+'", "to_state": "'+region+'", "to_city": "'+city+'", "to_street": "'+line1+'", "amount": '+amount*quantity+', "shipping": 0, "line_items": [ { "quantity": 1, "unit_price": '+amount*quantity+', "product_tax_code": '+taxCode+' } ] }';
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
            }else{
                hs.setStatusCode(200);
            }
            if(!test.isRunningTest()){
                response = hs.getBody();
                system.debug(response);
            }else{
                response='{ "tax": { "taxable_amount": 15.0, "tax_source": "origin", "shipping": 0.0, "rate": 0.07875, "order_total_amount": 15.0, "jurisdictions": { "state": "NM", "county": "BERNALILLO", "country": "US", "city": "ALBUQUERQUE" }, "has_nexus": true, "freight_taxable": true, "breakdown": { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_tax_rate": 0.05125, "state_tax_collectable": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_tax_collectable": 0.41, "shipping": { "taxable_amount": 0.0, "tax_collectable": 0.0, "state_taxable_amount": 0.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.0, "special_taxable_amount": 0.0, "special_tax_rate": 0.0275, "special_district_amount": 0.0, "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 }, "line_items": [ { "taxable_amount": 15.0, "tax_collectable": 1.18, "state_taxable_amount": 15.0, "state_sales_tax_rate": 0.05125, "state_amount": 0.77, "special_tax_rate": 0.0275, "special_district_taxable_amount": 15.0, "special_district_amount": 0.41, "id": "1", "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_amount": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_amount": 0.0 } ], "county_taxable_amount": 0.0, "county_tax_rate": 0.0, "county_tax_collectable": 0.0, "combined_tax_rate": 0.07875, "city_taxable_amount": 0.0, "city_tax_rate": 0.0, "city_tax_collectable": 0.0 }, "amount_to_collect": 1.18 } }';   
            }
            statusCode = hs.getStatusCode();
            TaxjarTaxCalculate o = TaxjarTaxCalculate.parse(response);
            if (o.status == 400 || ( o.error != null && o.error != '')) {
                o =  ratemethod('LadyBoss Weight Loss 10010 Indian School Rd. NE', 'Albuquerque', 'NM', 'US', '87112', amount, quantity, taxCode);
            }
            Decimal taxRate = o.tax.amount_to_collect;
            Decimal taxPercentage = o.tax.rate*100;
            Subscriptions.updateSubscription(sub,taxPercentage);
            
        } catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'ratemethodForOppUpdate',
                    '',
                    ex
                )
            );
        }
        
         
    }
    @Future(callout=true)
    public static void createTransaction(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,Decimal taxAmount, String statDesc,String chargeId, String prodName, Decimal shippingCost,String createdUsing){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint('https://api.taxjar.com/v2/transactions/orders');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            Date transDate = Date.today();
            string body;
            Decimal totalAmount;
            if (createdUsing == 'V10') {
                totalAmount = (amount*quantity)+shippingCost;
                body='{ "transaction_id": "'+chargeId+'", "transaction_date": "'+transDate+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": '+shippingCost+', "sales_tax": '+taxAmount+', "line_items": [ { "quantity": 1, "product_identifier": "'+statDesc+'", "description": "'+prodName+'", "unit_price": '+amount*quantity+', "sales_tax": '+taxAmount+' } ] }';
            }else {
                totalAmount = amount*quantity;
                body='{ "transaction_id": "'+chargeId+'", "transaction_date": "'+transDate+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": 0, "sales_tax": '+taxAmount+', "line_items": [ { "quantity": 1, "product_identifier": "'+statDesc+'", "description": "'+prodName+'", "unit_price": '+amount*quantity+', "sales_tax": '+taxAmount+' } ] }';
            }
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
                response = hs.getBody();
                system.debug(response);
            }      
        } catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'createTransaction',
                    '',
                    ex
                )
            );
        }
        
    }
    public static void createTransactionNotFuture(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,Decimal taxAmount, String statDesc,String chargeId, String prodName, Decimal shippingCost,String createdUsing){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint('https://api.taxjar.com/v2/transactions/orders');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            Date transDate = Date.today();
            string body;
            Decimal totalAmount;
            if(shippingCost == null){
                shippingCost = 0.00;
            }
            if (createdUsing == 'V10') {
                totalAmount = (amount*quantity)+shippingCost;
                body='{ "transaction_id": "'+chargeId+'", "transaction_date": "'+transDate+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": '+shippingCost+', "sales_tax": '+taxAmount+', "line_items": [ { "quantity": 1, "product_identifier": "'+statDesc+'", "description": "'+prodName+'", "unit_price": '+amount*quantity+', "sales_tax": '+taxAmount+' } ] }';
            }else {
                totalAmount = amount*quantity;
                body='{ "transaction_id": "'+chargeId+'", "transaction_date": "'+transDate+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": 0, "sales_tax": '+taxAmount+', "line_items": [ { "quantity": 1, "product_identifier": "'+statDesc+'", "description": "'+prodName+'", "unit_price": '+amount*quantity+', "sales_tax": '+taxAmount+' } ] }';
            }
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
                response = hs.getBody();
                system.debug(response);
            }  
        } catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'createTransactionNotFuture',
                    '',
                    ex
                )
            );
        }    
    }
    public static void refundTransaction(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,Decimal taxAmount, String planId,String chargeId, String prodName, Decimal shippingCost,String createdUsing){
        try {
            HttpRequest http = new HttpRequest();
            http.setEndpoint('https://api.taxjar.com/v2/transactions/refunds');
            http.setMethod('POST');
            http.setHeader('content-type', 'application/json');
            TaxJar__c  tj= TaxJar__c.getInstance(); 
            http.setHeader('Authorization', 'Bearer '+ tj.API_Key__c);
            Date transDate = Date.today();
            string body;
            Decimal totalAmount;
            if (createdUsing == 'V10') {
                totalAmount = -((amount*quantity)+shippingCost);
                body='{ "transaction_id": "'+chargeId+'-refund", "transaction_date": "'+transDate+'","transaction_reference_id": "'+chargeId+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": '+(-shippingCost)+', "sales_tax": '+(-taxAmount)+', "line_items": [ { "quantity": 1, "product_identifier": "'+planId+'", "description": "'+prodName+'", "unit_price": '+(-amount*quantity)+', "sales_tax": '+(-taxAmount)+' } ] }';
            }else {
                totalAmount = -(amount*quantity);
                body='{ "transaction_id": "'+chargeId+'-refund", "transaction_date": "'+transDate+'","transaction_reference_id": "'+chargeId+'", "to_street": "'+line1+'", "to_city": "'+city+'", "to_state": "'+region+'", "to_zip": "'+postalCode+'", "to_country": "'+country+'", "amount": '+totalAmount+', "shipping": 0, "sales_tax": '+(-taxAmount)+', "line_items": [ { "quantity": 1, "product_identifier": "'+planId+'", "description": "'+prodName+'", "unit_price": '+(-amount*quantity)+', "sales_tax": '+(-taxAmount)+' } ] }';
            }
            system.debug('Anydatatype_msg'+body);
            http.setBody(body);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!test.isRunningTest()){
                hs = con.send(http);
                response = hs.getBody();
                system.debug(response);
            }    
        } catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'TaxjarTaxCalculate',
                    'refundTransaction',
                    '',
                    ex
                )
            );
        }
          
    }
    public static TaxjarTaxCalculate parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (TaxjarTaxCalculate) System.JSON.deserialize(json, TaxjarTaxCalculate.class);
    }
}