/*
 * Developer Name : Tirth Patel
 * Description    : This class in a controller of vf page named as SendManualSMSFromCasePage. 
*/
public class SendManualSMSController {
    public String cntId {get;set;} 
    //public Contact cnt {get;set;}
    private ApexPages.StandardController stdController{get;set;}
    public CampaignMember cmpMember {get;set;}
    public String messageData {get;set;}
    public String toNumber {get;set;}
    public String selectedTemplate {get;set;}
    public List<EmailTemplate> lstEmailTemplates {get;set;}
    public Case caseObject{get;set;}
    public String TwilioNumber{get;set;}
    public String mobilePhone{get;set;}
    public String Phone{get;set;}
    public String OtherPhone{get;set;}
    public String do_not_call{get;set;}
    public String Phone_dnc{get;set;}
    public String otherPhone_dnc{get;set;}
    public String messageToShow2='This Contact has Unsubscribed';
    public String fromNum{get;set;}
    public String messageToShow='DNC is Enabled for';
    
    public SendManualSMSController()  {
    
    }
    
    public SendManualSMSController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        if(stdController.getRecord() instanceof Contact){        
            Contact cnt = (Contact)stdController.getRecord();
            cnt = [select id,firstname,DoNotCall,lastname,MobilePhone,Phone,OtherPhone,AssistantPhone,Phone_National_DNC__c,Other_Phone_National_DNC__c,Unsubscribe_Me__c from Contact where id =:cnt.Id ];
            if(cnt.MobilePhone!=null){
                mobilePhone = cnt.MobilePhone;
            }
            if(cnt.Phone!=null){
                Phone = cnt.Phone;
            }
            if(cnt.OtherPhone!=null){
                OtherPhone = cnt.OtherPhone;
            }
            if(cnt.DoNotCall!=null && cnt.DoNotCall){
                do_not_call = 'Yes';
                messageToShow = messageToShow + ' Mobile Phone';
            }else{
                do_not_call = 'No';
            }
            if(cnt.Phone_National_DNC__c!=null && cnt.Phone_National_DNC__c){
                Phone_dnc = 'Yes';
                if(messageToShow.length()>18){
                    messageToShow = messageToShow + ', Phone';
                }else{
                    messageToShow = messageToShow + ' Phone';
                }
            }else{
                Phone_dnc = 'No';
            }
            if(cnt.Other_Phone_National_DNC__c!=null && cnt.Other_Phone_National_DNC__c){
                otherPhone_dnc = 'Yes';
                if(messageToShow.length()>18){
                    messageToShow = messageToShow + ', Other Phone';
                }else{
                    messageToShow = messageToShow + ' Other Phone';
                }
            }else{
                otherPhone_dnc = 'No';
            }
            if(cnt.Unsubscribe_Me__c=='Yes'){ 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,messageToShow2));
            }
            System.debug(cnt.DoNotCall);
            if(messageToShow.length() > 18){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,messageToShow));
            }
            if(cnt.MobilePhone != null && !cnt.DoNotCall)  {
                toNumber = cnt.MobilePhone; 
            }else if(cnt.Phone != null && !cnt.Phone_National_DNC__c)  {
                toNumber = cnt.Phone;
            }else if(cnt.OtherPhone != null && !cnt.Other_Phone_National_DNC__c)  {
                toNumber = cnt.OtherPhone;
            }else if(cnt.AssistantPhone != null)  {
                toNumber = cnt.AssistantPhone;
            }
            lstEmailTemplates = [select id,Name from EmailTemplate where DeveloperName like 'ContactStandard%'];
        }else if(stdController.getRecord() instanceof Case){  
            caseObject = (Case)stdController.getRecord();
            caseObject = [select Id,Twilio_Number__c,toTwilioNumber__c,contactId,Contact.OtherPhone,Contact.donotCall,Contact.Phone_National_DNC__c,Contact.Other_Phone_National_DNC__c,Contact.AssistantPhone,ContactMobile,ContactPhone,Contact.Unsubscribe_Me__c from Case where id =:caseObject.Id ];
            if(caseObject.toTwilioNumber__c!=null){
            	fromNum = caseObject.toTwilioNumber__c;
                }
            System.debug('fromNum : '+fromNum);
            if(caseObject.ContactMobile != null){
                mobilePhone = caseObject.ContactMobile;
            }
            if(caseObject.ContactPhone != null){
                Phone = caseObject.ContactPhone;
            }
            if(caseObject.Contact.OtherPhone !=null){
                OtherPhone = caseObject.Contact.OtherPhone;
            }
            if(caseObject.Twilio_Number__c!=null){ 
                TwilioNumber = caseObject.Twilio_Number__c;
            }         
            if(caseObject.Contact.DoNotCall!=null && caseObject.Contact.DoNotCall){
                do_not_call = 'Yes';
                messageToShow = messageToShow + ' Mobile Phone';
            }else{
                 do_not_call = 'No';
            }
            if(caseObject.Contact.Phone_National_DNC__c!=null && caseObject.Contact.Phone_National_DNC__c){
                Phone_dnc = 'Yes';
                if(messageToShow.length()>18){
                    messageToShow = messageToShow + ', Phone';
                }else{
                    messageToShow = messageToShow + ' Phone';
                }
            }else{
                Phone_dnc = 'No';
            }
            if(caseObject.Contact.Other_Phone_National_DNC__c!=null && caseObject.Contact.Other_Phone_National_DNC__c){
                otherPhone_dnc = 'Yes';
                if(messageToShow.length()>18){
                    messageToShow = messageToShow + ', Other Phone';
                }else{
                    messageToShow = messageToShow + ' Other Phone';
                }
            }else{
                otherPhone_dnc = 'No';
            }
            if(caseObject.Contact.Unsubscribe_Me__c=='Yes'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,messageToShow2));
            }
            System.debug(caseObject.Contact.donotCall);
            if(messageToShow.length() > 18){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,messageToShow));
            }
            if(caseObject.Twilio_Number__c != null )  {
                toNumber = caseObject.Twilio_Number__c;
            }else if(caseObject.ContactMobile != null && !caseObject.Contact.donotCall )  {
                toNumber = caseObject.ContactMobile;
            }else if(caseObject.ContactPhone != null && !caseObject.Contact.Phone_National_DNC__c)  {
                toNumber = caseObject.ContactPhone;
            }else if(caseObject.Contact.OtherPhone != null && !caseObject.Contact.Other_Phone_National_DNC__c)  {
                toNumber = caseObject.Contact.OtherPhone;
            }else if(caseObject.Contact.AssistantPhone != null)  {
                toNumber = caseObject.Contact.AssistantPhone;
            }
            lstEmailTemplates = [select id,Name from EmailTemplate where DeveloperName like 'CaseStandard%'];            
        }
        if(toNumber != null)  {
            if(toNumber.indexof('+') != 0)  {
                    toNumber = '+'+toNumber; 
            }
        }
        if(lstEmailTemplates.size() > 0)  {
            selectedTemplate = lstEmailTemplates.get(0).Id;
        }
    }
    
    public List<SelectOption> getSMSTemplates()  {
        List<SelectOption> lstOptions = new List<SelectOption>();
        for(EmailTemplate emailT : lstEmailTemplates)  {
            SelectOption so = new SelectOption(emailT.Id,emailT.Name);
            lstOptions.add(so);
        }
        return lstOptions;
    }
    
    /*
            This method is used in CampaignMemberTrigger too, please test both if you change it.
    */
    public void getSMSText()   {
        //Code to get emailtemplate and get text from it
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        System.debug(selectedTemplate);
        EmailTemplate templateId = [Select id,body from EmailTemplate where Id =:selectedTemplate];
        Messaging.SingleEmailMessage ms = Messaging.renderStoredEmailTemplate(templateId.Id,null, stdController.getId());
		System.debug(ms.getPlainTextBody());    
        messageData = ms.getPlainTextBody(); 
        selectedTemplate = templateId.Id; 
    }
    
    public void sendSMS()  {
        Map<String,Object> mapData = sendSMSNew();
        if(mapData.get('success') != null && (Boolean)mapData.get('success'))  {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,(String)mapData.get('message')));
        }else  {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,(String)mapData.get('message')));
        }
    }
    
    /*
            This method is used in CampaignMemberTrigger too, please test both if you change it.
    */
    public Map<String,Object> sendSMSNew()  {  
            Map<String,Object> mapOutput = new  Map<String,Object>();
            List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain'];
        String accountSID = lstTwilliConfigs.get(0).AccountSID__c;  //'AC44099911ff7ec99c40bc97943ad6c0ca';
        String authToken = lstTwilliConfigs.get(0).AuthToken__c; //'6a386f07923a55ec0729eff283d747a3';
        String fromNumber = lstTwilliConfigs.get(0).From_Number__c; //'15053226116';
        if(fromNum!=null && fromNum.length()>0){
            fromNumber = fromNum;
        }
        String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody('To='+ EncodingUtil.urlEncode(toNumber,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(messageData,'UTF-8'));
        Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        TwillioResponse tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
        System.debug(tl);
        if(tl.sid != null)  {
                mapOutput.put('success',TRUE);
                mapOutput.put('message','SMS send succsufully');
            if(stdController.getRecord() instanceof Contact){
                Task t = new Task();
                t.Contact__c = stdController.getId();
                t.Description = this.messageData;
                t.Subject = 'SMS Sent to ' + this.toNumber;
                t.status = 'Completed';
                insert t;
                
                
            }else if(stdController.getRecord() instanceof Case){
                CaseComment caseCommentObject = new CaseComment();
                caseCommentObject.CommentBody = this.messageData;
                caseCommentObject.parentId = stdController.getId();
                insert caseCommentObject;
                Case cs = (Case)stdController.getRecord() ;
                List<case> csListUpdate = new List<case>();
                Group gr = [SELECT CreatedDate,DeveloperName FROM Group where DeveloperName =: 'Account_Recovery'];
                Case csRec = [select id,IsFailed_Payment__c, ownerId from case where Id = :cs.id];
                if( gr!=NULL && csRec !=null){
                     if(gr.id!=null && csRec.IsFailed_Payment__c) {
                        csRec.OwnerId = gr.id;
                        csListUpdate.add(csRec);
                      }
                    if(csListUpdate.size() > 0) {
                        update csListUpdate;
                    } 
                }
                
            }
        }else  {
                mapOutput.put('success',FALSE);
                mapOutput.put('message','SMS sending fail,Error from Twillio is : '+tl.message);
        }
        return mapOutput;
    }
    
    class TwillioResponse  {
        public String sid;  
        public String message;
    }
}