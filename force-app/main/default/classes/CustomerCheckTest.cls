@isTest
public class CustomerCheckTest {
    static testMethod void testHttpRequest1() { 
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce1@gmail.com',
        	shopify_customer__c = true);
        
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.StartTest(); 
        insert opp ;
        customercheck.checkShopifyCustomer(opp.id);
        customercheck.checkShopifyCustomerNotFuture(opp.id);
        Test.StopTest();
    }
    static testMethod void testHttpRequest12() { 
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce1@gmail.com',
        	shopify_customer__c = false);
        
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.StartTest(); 
        insert opp ;
        customercheck.checkShopifyCustomer(opp.id);
        customercheck.checkShopifyCustomerNotFuture(opp.id);
        Test.StopTest();
    }
    static testMethod void testHttpRequest2(){
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact( FirstName ='Sourabh', 
                                  LastName = 'Badole',
                                  Email = 'badole.salesforce1@gmail.com');
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.startTest();
        insert opp;
        CustomerCheck.getContactsWithMetadata('badole.salesforce1@gmail.com');
        Test.stopTest();
    }
    static testMethod void testHttpRequest3(){
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        contact con = new contact();
        con.FirstName = 'Sourabh';
        con.lastName='Badole';
        con.email = 'badole.salesforce1@gmail.com';
        con.Stripe_Customer_Id__c ='cus_EQS8i8VjKTRuTG';
        insert con;
        
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        insert opp;
        Test.setCreatedDate(opp.id, Datetime.now().addMinutes(-10));
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='123455';
        sp.Customer__c =  con.id;
        insert sp;
        
        
        Test.startTest();
        CustomerCheck.getContactsWithMetadata('badole.salesforce1@gmail.com');
        CustomerCheck.getContactsWithMetadataNotFuture('badole.salesforce1@gmail.com');
        Test.stopTest();
        
    }
    static testMethod void testHttpRequest4() { 
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(FirstName ='Sourabh',
                                  LastName = 'Badole',
                                  Email = 'badole.salesforce2@gmail.com');
        
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        Opportunity opp1 = new Opportunity();
        opp1.Contact__c = con.id ;
        opp1.Name = 'Test ' ;
        opp1.StageName = 'Qualification' ;
        opp1.CloseDate = Date.today();
        
        Test.StartTest(); 
        insert opp ;
        insert opp1 ;
        Test.StopTest();
    }  
    static testMethod void testHttpRequest5() { 
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(FirstName ='Sourabh', 
                                  LastName = 'Badole',
                                  Email = 'badole.salesforce1@gmail.com',
                                  shopify_customer__c = true);
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.StartTest(); 
        insert opp ;
        Test.StopTest();
    }    
    
    static testMethod void testHttpRequest6() {
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 10000;
        insert ap;
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',
                                  Created_At__c=system.today(),
                                  NMI_External_ID__c='1234567');
        insert acc;
        Opportunity opp = new Opportunity(Name='Test',
                                          CloseDate=system.today(),
                                          StageName='Closed Won',
                                          AccountId=acc.Id, 
                                          Payment_Date__c=system.today(),
                                          Amount=20);
        Test.startTest();
        insert opp;
        opp.Payment_Date__c = opp.Payment_Date__c.addDays(2);
        update opp;
        Test.stopTest();
    }
    
    
}