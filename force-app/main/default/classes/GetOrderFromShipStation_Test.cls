@isTest
public class GetOrderFromShipStation_Test {
    @isTest 
    Static void testme(){
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'test@gmail.com';
        con.Phone = '7067749234';
        insert con;
        
        ShipStation__c shipStation = new ShipStation__c();
        shipStation.Domain_Name__c = 'https://test.com';
        shipStation.userName__c = 'amit_439';
        shipStation.password__c = '123456';
        shipStation.IsLive__c = true;
        insert shipStation;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderNumber__c = 'ch_1FHabFFzCf73siP0ZyFwpP0i';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        insert shipOrder;
        
        ShipStation_Order_Items__c item = new ShipStation_Order_Items__c();
        item.orderItemId__c = '2345678889';
        insert item;
        
        Shipstation_pages__c pages = new Shipstation_pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        pages.Pages__c = 10;
        pages.Number_of_Days__c = 60;
        insert pages;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GetOrderFromShipStation_Mock());
        GetOrderFromShipStation order = new GetOrderFromShipStation();
        Id batchId = Database.executeBatch(order);
       ShipstationOrderIterator sp = new ShipstationOrderIterator();
        //ShipstationOrderIterator.hasNext();
        //ShipstationOrderIterator.next();
        // GetOrderFromShipStation.dummy();
        Test.stopTest();
        
       
        
    }
    
}