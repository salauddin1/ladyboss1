@isTest
public class ContactAccountCrossObjectWorkflowsTest {
	@isTest
    public static void testmethodwrkflw() {
        List<Contact> conlist = new List<Contact>();
        List<Account> actList = new List<Account>();
        Account act = new Account();
        act.Ready_in_Tipalti__c = true;
        act.Name = 'test account';
        act.Cvc__c = '123';
        act.Credit_Card_Number__c = '1234567890';
        act.Exp_Month__c = '11';
        act.Exp_Year__c = 2020;
        act.ShippingCountry = 'testCountry';
        act.External_ID__c = 'testId1234';
        act.Email__c = 'TestEmail@test.com';
        act.BillingCountry = 'testBillingCountry';
        insert act;
        act.Country__c = 'US';
        update act;
        actList.add(act);
        contact con1 = new contact ();
        con1.LastName = 'BlogTest';
        con1.FirstName = 'firstname';
        con1.sf4twitter__Origin__c = 'Blog';
        con1.Coaching_Applied_But_Didn_t_Book__c = true;
        con1.Ready_in_Tipalti__c = true;
        con1.AccountId = act.Id;
        con1.Ready_in_Tipalti__c = true;
        insert con1;
        con1.Marketing_Cloud_Tags__c = 'Coaching-Booked But Did Not Show';
        update con1;
        contact con = new contact ();
        con.LastName = 'BlogTest';
        con.FirstName = 'firstname';
        con.sf4twitter__Origin__c = 'Blog';
        con.Coaching_Applied_But_Didn_t_Book__c = true;
        con.Ready_in_Tipalti__c = true;
        con.AccountId = act.Id;
        insert con;
        conList.add(con);
        Test.startTest();
        ContactAccountCrossObjectWorkflows.UpdateAccountChekBox(conList);
        ContactAccountCrossObjectWorkflows.UpdateContactChekBox(actlist);
        Test.stopTest();
    }
}