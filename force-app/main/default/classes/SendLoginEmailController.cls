public without sharing class SendLoginEmailController {
    @AuraEnabled
    public static String getEmail(String conId){
        Contact con = [SELECT Email FROM Contact WHERE id =: conId];
        return con.Email;
    }
    @AuraEnabled
    public static string sendEmail(String conID){
        try {
            List<Contact> conUpdateList = [Select id,Password__c,Site_Password_Reset__c FROM Contact WHERE ID =:conID ORDER BY CreatedDate ASC LIMIT 1];
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            if(conUpdateList.size()>0){
                for(Contact con:conUpdateList){
                    Integer len = 10;
                    Blob blobKey = crypto.generateAesKey(128);
                    String key = EncodingUtil.convertToHex(blobKey);
                    String pwd = key.substring(0,len);
                    con.Password__c = pwd;
                    con.Site_Password_Reset__c = true;                    
                }
                update conUpdateList;
            }
            for(Contact con:conUpdateList){
                Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
                Id templateId = [select id, name from EmailTemplate where name = : 'SiteLoginEmail'].id;
                // Who you are sending the email to
                mail.setTargetObjectId(con.id);
                
                // The email template ID used for the email
                mail.setTemplateId(templateId);
                
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false);  
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                mailList.add(mail);
            }
            if (mailList.size()>0) {
                Messaging.sendEmail(mailList);
            } 
            return 'success';
        } catch (Exception e) {
            System.debug('Error '+ e.getMessage());
            return e.getMessage();
        }
        
    }
}