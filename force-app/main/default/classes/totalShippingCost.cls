public class totalShippingCost {
    public static Decimal getCost(List<id> productIds){
        Map<id,List<Decimal>> prod_shipMap = new Map<id,List<Decimal>>();
        System.debug('productIds'+productIds);
        List<Product2> prodList = [SELECT id,Available_For_Profiles_For_Search__c,Stripe_Product_Id__c,Shipping_cost_1__c,Shipping_cost_2__c,Shipping_cost_3__c,Shipping_cost_4__c ,Shipping_cost_5__c  FROM Product2 where id in : productIds and DNC_Shipping_Packing__c=false ];
        List<Product2> prodSwitchList = [SELECT id,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 where Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c !=null];
        Integer srchProdCount = 0;
        Decimal totalShipping=0;
        for(Product2 prd : prodList){
            if(prd.Available_For_Profiles_For_Search__c!=null && prd.Available_For_Profiles_For_Search__c.length()>0){
                srchProdCount++;
            }
        }
        if(prodList.size() == srchProdCount && srchProdCount >=3 && srchProdCount <=5){
            if(srchProdCount == 3){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_3_Prod__c;
            }else if(srchProdCount == 4){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_4_Prod__c;
            }else if(srchProdCount == 5){
                totalShipping =  sear_prod_shipping__c.getInstance('Shipping Cost').For_5_Prod__c;
            }
            return totalShipping;
        }
        List<id> prodWithMonthList = new List<id>(); 
        for(Product2 pd : prodList){
            List<Decimal> prList = new List<Decimal>();
            if(pd.Shipping_cost_1__c!=null){
                prList.add(pd.Shipping_cost_1__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_2__c!=null){
                prList.add(pd.Shipping_cost_2__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_3__c!=null){
                prList.add(pd.Shipping_cost_3__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_4__c!=null){
                prList.add(pd.Shipping_cost_4__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_5__c!=null){
                prList.add(pd.Shipping_cost_5__c);
            }else{
                prList.add(0);
            }
            prod_shipMap.put(pd.id,prList);
        }
        for(Product2 p1 : prodSwitchList){
            for(Product2 p2 : prodList){
                if(p2.id == p1.Switch_To_Product_four_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_five_month__c){
					totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_six_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                } 
            }
        }
        System.debug('prodWithMonthList'+prodWithMonthList);
        System.debug('prodWithMonthList'+prodList);
        for(id pm : prodWithMonthList){
            prod_shipMap.remove(pm);
        }
        integer index_start=0;
        Integer  loopCount = prod_shipMap.size();
        Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
        if(prodWithMonthList.size()>0){
            index_start = 1;
            loopCount++;                     
        }
        System.debug(prod_shipMap);
        if(prod_shipMap.size()>0){
            for(integer i=index_start; i<loopCount;i++){
                Decimal max=0;
                String maxPrId = '';
                    for(id ps : prod_shipMap.keySet()){
                        if(prod_shipMap.get(ps).get(i) >= max){
                            max = prod_shipMap.get(ps).get(i);
                            maxPrId = ps;
                        }
                    }
                totalShipping = totalShipping + max;
                prodShip.put(maxPrId,max);
                prod_shipMap.remove(maxPrId);
            }
        }
        System.debug('totalShipping : '+totalShipping);
        System.debug('per product shipping '+prodShip);
        return totalShipping;
    } 
    
    public static Decimal createShipping(List<id> productIds, String conID){
        Map<id,List<Decimal>> prod_shipMap = new Map<id,List<Decimal>>();
        System.debug('productIds'+productIds);
        List<Product2> prodList = [SELECT id,Available_For_Profiles_For_Search__c,Stripe_Product_Id__c,Shipping_cost_1__c,Shipping_cost_2__c,Shipping_cost_3__c,Shipping_cost_4__c ,Shipping_cost_5__c  FROM Product2 where id in : productIds and DNC_Shipping_Packing__c=false ];
        List<Product2> prodSwitchList = [SELECT id,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 where Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c !=null];
        Map<Id,String> stripeProd = new Map<Id,String>();
        List<Shipping_Cost_Table__c> sctList = new List<Shipping_Cost_Table__c>();
        Decimal totalShipping=0;
        Integer srchProdCount = 0;
        for(Product2 strPr : prodList){
            stripeProd.put(strPr.Id, strPr.Stripe_Product_Id__c);
            if(strPr.Available_For_Profiles_For_Search__c!=null && strPr.Available_For_Profiles_For_Search__c.length()>0){
                srchProdCount++;
            }
        }
        if(prodList.size() == srchProdCount && srchProdCount >=3 && srchProdCount <=5){
            if(srchProdCount == 3){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_3_Prod__c;
                addShippingTable(totalShipping, prodList,stripeProd,sctList,conId);
            }else if(srchProdCount == 4){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_4_Prod__c;
                addShippingTable(totalShipping, prodList,stripeProd,sctList,conId);
            }else if(srchProdCount == 5){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_5_Prod__c;
                addShippingTable(totalShipping, prodList,stripeProd,sctList,conId);
            }
            return totalShipping;
        }
        List<id> prodWithMonthList = new List<id>(); 
        for(Product2 pd : prodList){
            List<Decimal> prList = new List<Decimal>();
            if(pd.Shipping_cost_1__c!=null){
                prList.add(pd.Shipping_cost_1__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_2__c!=null){
                prList.add(pd.Shipping_cost_2__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_3__c!=null){
                prList.add(pd.Shipping_cost_3__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_4__c!=null){
                prList.add(pd.Shipping_cost_4__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_5__c!=null){
                prList.add(pd.Shipping_cost_5__c);
            }else{
                prList.add(0);
            }
            prod_shipMap.put(pd.id,prList);
        }
        for(Product2 p1 : prodSwitchList){
            for(Product2 p2 : prodList){
                if(p2.id == p1.Switch_To_Product_four_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_five_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_six_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                } 
            }
        }
        System.debug('prodWithMonthList'+prodWithMonthList);
        System.debug('prodWithMonthList'+prodList);
        for(id pm : prodWithMonthList){
            prod_shipMap.remove(pm);
        }
        integer index_start=0;
        Integer  loopCount = prod_shipMap.size();
        Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
        if(prodWithMonthList.size()>0){
            index_start = 1;
            loopCount++;                     
        }
        System.debug(prod_shipMap);
        if(prod_shipMap.size()>0){
            for(integer i=index_start; i<loopCount;i++){
                Decimal max=0;
                String maxPrId = '';
                    for(id ps : prod_shipMap.keySet()){
                        if(prod_shipMap.get(ps).get(i) >= max){
                            max = prod_shipMap.get(ps).get(i);
                            maxPrId = ps;
                        }
                    }
                totalShipping = totalShipping + max;
                prodShip.put(maxPrId,max);
                prod_shipMap.remove(maxPrId);
            }
        }
        System.debug('totalShipping : '+totalShipping);
        System.debug('per product shipping '+prodShip);
        
      /*  Map<Id,String> stripeProd = new Map<Id,String>();
        for(Product2 strPr:[SELECT id, Stripe_Product_Id__c FROM Product2 WHERE Id IN : prodship.keySet()]){
            stripeProd.put(strPr.Id, strPr.Stripe_Product_Id__c);
        }  */
        for(Id pk:prodShip.keySet()){
         Shipping_Cost_Table__c   sct = new Shipping_Cost_Table__c   ();
         sct.Product_Id__c = pk;
         if(stripeProd.containsKey(pk)){
            if(stripeProd.get(pk)!=null){
                sct.Stripe_Product_Id__c = stripeProd.get(pk);
            }
         }
         sct.ProductCount__c = prodShip.size();
         sct.Shipping_Cost__c = prodShip.get(pk);
         sct.Customer_Id__c = conID;
            sctList.add(sct);
        }
        if(sctList.size()>0){
            insert sctList;
        }
        return totalShipping;
    }
    
    public static void addShippingTable(Decimal amount,List<Product2> prodList,Map<Id,String> stripeProd,List<Shipping_Cost_Table__c> sctList,Id conID){
        for(Product2 pk:prodList){
         Shipping_Cost_Table__c   sct = new Shipping_Cost_Table__c();
         sct.Product_Id__c = pk.Id;
         if(stripeProd.containsKey(pk.id)){
            if(stripeProd.get(pk.id)!=null){
                sct.Stripe_Product_Id__c = stripeProd.get(pk.id);
            }
         }
           sct.ProductCount__c = prodList.size();
           sct.Shipping_Cost__c = amount/prodList.size();
           sct.Customer_Id__c = conID;
           sctList.add(sct);
        }
        if(sctList.size()>0){
            insert sctList;
        }
    }
    
    public static Map<Id,Decimal> getShippingCost(List<id> productIds){
        Map<id,List<Decimal>> prod_shipMap = new Map<id,List<Decimal>>();
        System.debug('productIds'+productIds);
        List<Product2> prodList = [SELECT id,Available_For_Profiles_For_Search__c,Stripe_Product_Id__c,Shipping_cost_1__c,Shipping_cost_2__c,Shipping_cost_3__c,Shipping_cost_4__c ,Shipping_cost_5__c  FROM Product2 where id in : productIds and DNC_Shipping_Packing__c=false ];
        List<Product2> prodSwitchList = [SELECT id,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 where Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c !=null];
        Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
        Decimal totalShipping=0;
        Integer srchProdCount = 0;
        for(Product2 strPr : prodList){
            //stripeProd.put(strPr.Id, strPr.Stripe_Product_Id__c);
            if(strPr.Available_For_Profiles_For_Search__c!=null && strPr.Available_For_Profiles_For_Search__c.length()>0){
                srchProdCount++;
            }
        }
        if(prodList.size() == srchProdCount && srchProdCount >=3 && srchProdCount <=5){
            if(srchProdCount == 3 ){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_3_Prod__c;
            }else if(srchProdCount == 4){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_4_Prod__c;
            }else if(srchProdCount == 5){
                totalShipping = sear_prod_shipping__c.getInstance('Shipping Cost').For_5_Prod__c;
            }
            for(Product2 strPr : prodList){
                prodShip.put(strPr.Id,totalShipping/srchProdCount);
            }
            return prodShip;
        }
        List<id> prodWithMonthList = new List<id>(); 
        for(Product2 pd : prodList){
            List<Decimal> prList = new List<Decimal>();
            if(pd.Shipping_cost_1__c!=null){
                prList.add(pd.Shipping_cost_1__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_2__c!=null){
                prList.add(pd.Shipping_cost_2__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_3__c!=null){
                prList.add(pd.Shipping_cost_3__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_4__c!=null){
                prList.add(pd.Shipping_cost_4__c);
            }else{
                prList.add(0);
            }
            if(pd.Shipping_cost_5__c!=null){
                prList.add(pd.Shipping_cost_5__c);
            }else{
                prList.add(0);
            }
            prod_shipMap.put(pd.id,prList);
        }
        for(Product2 p1 : prodSwitchList){
            for(Product2 p2 : prodList){
                if(p2.id == p1.Switch_To_Product_four_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_five_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                }else if(p2.id == p1.Switch_To_Product_six_month__c){
                    totalShipping = totalShipping + p2.Shipping_cost_1__c;
                    prodWithMonthList.add(p2.id);
                } 
            }
        }
        System.debug('prodWithMonthList'+prodWithMonthList);
        System.debug('prodWithMonthList'+prodList);
        for(id pm : prodWithMonthList){
            prod_shipMap.remove(pm);
        }
        integer index_start=0;
        Integer  loopCount = prod_shipMap.size();
        //Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
        if(prodWithMonthList.size()>0){
            index_start = 1;
            loopCount++;                     
        }
        System.debug(prod_shipMap);
        if(prod_shipMap.size()>0){
            for(integer i=index_start; i<loopCount;i++){
                Decimal max=0;
                String maxPrId = '';
                    for(id ps : prod_shipMap.keySet()){
                        if(prod_shipMap.get(ps).get(i) >= max){
                            max = prod_shipMap.get(ps).get(i);
                            maxPrId = ps;
                        }
                    }
                totalShipping = totalShipping + max;
                prodShip.put(maxPrId,max);
                prod_shipMap.remove(maxPrId);
            }
        }
        System.debug('totalShipping : '+totalShipping);
        System.debug('per product shipping '+prodShip);
        return prodShip;
    }
}