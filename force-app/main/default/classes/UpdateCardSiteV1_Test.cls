@isTest 
public class UpdateCardSiteV1_Test {
    public static testMethod void test(){
        system.Test.setMock(HttpCalloutMock.class, new MockUpdateCard());
        test.startTest();
        /*UpdateCardSite up = new UpdateCardSite();
up.openUpdateForm();

UpdateCardSite.updateCard1();
UpdateCardSite.getExpireDateValues();
up.openUpdateForm();
up.MyActionMethod();
UpdateCardSite.updateCustomerCard('card_1Dg8FHBwLSk1v1ohKVoU0I76','cus_A9GhZUFCrxmS0F','Cirty1','','CA','55555','5','2029','Sukesh G','','',true);
*/
        test.stopTest();
    }
    public static testMethod void test1(){
        system.Test.setMock(HttpCalloutMock.class, new MockUpdateCard());
        contact co = new contact();
        co.LastName ='Test';
        co.Stripe_Customer_Id__c='cus_E1EYecfsRNu1yD';
        insert co;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Charge_Processed__c = false;
        sp.Stripe_Customer_Id__c = 'cus_E1EYecfsRNu1yD';
        sp.Customer__c = co.Id;
        insert sp;
        Custom_Tracker__c  c= new Custom_Tracker__c  ();
        c.UniqueId__c = '123';
        insert c;
        Case cs = new Case();
        
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = co.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        cs.IsFailed_Payment__c =true;
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        cs.IsFailed_Payment__c =true;
        insert cs;
        
        PageReference myVfPage = Page.UpdateCardCloneCase;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id','cus_E1EYecfsRNu1yD');
        ApexPages.currentPage().getParameters().put('key','123');
        
        
        
        String id = ApexPages.currentPage().getParameters().get('id');
        test.startTest();
        UpdateCardSiteV1 up = new UpdateCardSiteV1();
        
        UpdateCardSiteV1.updateCustomerCard('card_1Dg8FHBwLSk1v1ohKVoU0I76','cus_E1EYecfsRNu1yD','Cirty1','','CA','55555','5','2029','Sukesh G','','',true,'123',cs.id);
        
        test.stopTest();
    }
    public static testMethod void test2(){
        system.Test.setMock(HttpCalloutMock.class, new MockUpdateCard());
        contact co = new contact();
        co.LastName ='Test';
        co.Stripe_Customer_Id__c='cus_E1EYecfsRNu1yD';
        insert co;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Charge_Processed__c = false;
        sp.Stripe_Customer_Id__c = 'cus_E1EYecfsRNu1yD';
        sp.Customer__c = co.Id;
        insert sp;
        Custom_Tracker__c  c= new Custom_Tracker__c  ();
        c.UniqueId__c = '123';
        insert c;
        
        PageReference myVfPage = Page.UpdateCardCloneCase;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id','cus_E1EYecfsRNu1yD');
        ApexPages.currentPage().getParameters().put('key','123');
        
        
        
        String id = ApexPages.currentPage().getParameters().get('id');
        test.startTest();
        UpdateCardSiteV1 up = new UpdateCardSiteV1();
        up.checkSelectedValue();
        up.MyActionMethod();
        UpdateCardSiteV1.updateCard1();
        //UpdateCardSiteV1.updateCustomerCard('card_1Dg8FHBwLSk1v1ohKVoU0I76','cus_E1EYecfsRNu1yD','Cirty1','','CA','55555','5','2029','Sukesh G','','',true,'123');
        UpdateCardSiteV1.getExpireDateValues();
        List<Address__c> aList = new List<Address__c> ();
        Address__c addr = new Address__c();
        addr.Contact__c =co.id;
        addr.Shipping_City__c ='address_city';
        addr.Shipping_State_Province__c = 'address_state';
        addr.Shipping_Street__c ='address_line1';
        addr.Shipping_Zip_Postal_Code__c = 'address_zip';
        addr.Shipping_Country__c= 'address_country';
        aList.add(addr);
        insert aList;
        
        UpdateCardSiteV1.insertAdress('dfg',co.id);
        test.stopTest();
    }
    public static testMethod void test4(){
        system.Test.setMock(HttpCalloutMock.class, new MockUpdateCard());
        contact co = new contact();
        co.LastName ='Test';
        co.Stripe_Customer_Id__c='cus_E1EYecfsRNu1yD';
        insert co;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Charge_Processed__c = false;
        sp.Stripe_Customer_Id__c = 'cus_E1EYecfsRNu1yD';
        sp.Customer__c = co.Id;
        insert sp;
        Custom_Tracker__c  c= new Custom_Tracker__c  ();
        c.UniqueId__c = '123';
        insert c;
        Case cs = new Case();
        
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = co.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        cs.IsFailed_Payment__c =true;
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        cs.IsFailed_Payment__c =true;
        insert cs;
        PageReference myVfPage = Page.UpdateCardCloneCase;
        Test.setCurrentPage(myVfPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id','cus_E1EYecfsRNu1yD');
        ApexPages.currentPage().getParameters().put('key','123');
        
        
        
        String id = ApexPages.currentPage().getParameters().get('id');
        test.startTest();
        UpdateCardSiteV1 up = new UpdateCardSiteV1();
        
        UpdateCardSiteV1.updateCustomerCard('card_1Dg8FHBwLSk1v1ohKVoU0I76','cus_E1EYecfsRNu1yD','Cirty1','','CA','55555','5','2029','Sukesh G','','',false,'123',cs.id);
        
        test.stopTest();
    }
}