@isTest
public class CloseCase_Test {
    @isTest
    public static void test(){
        
        Case cas = new Case(Status ='New', Priority = 'Medium', Origin = 'Email', Reason='new');
        insert cas;
        
        Test.startTest();

        CloseCase.updateCase(cas.id);
        CloseCase.updateStatus(cas.id, 'asdaf', 'fafadfa');
        CloseCase.transferOwnership(cas.id, 'Coaching_Queue');
        Test.stopTest();
    }
    

}