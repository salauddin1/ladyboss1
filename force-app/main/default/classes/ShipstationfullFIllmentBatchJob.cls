public class ShipstationfullFIllmentBatchJob implements Database.Batchable<Sobject>,Database.allowscallouts{
    public database.QueryLocator start(Database.batchableContext info){
        return Database.getQueryLocator('select id, orderId__c, orderStatus__c from ShipStation_Orders__c where orderStatus__c =\'shipped\' AND Email_Body_Updated__c = false AND orderId__c != null');
    } 
    public void execute (Database.BatchableContext bc, List<ShipStation_Orders__c> ord) {
        System.debug('=========='+ord);
        if(!ord.isEmpty()) {
            String storeId = '';
            String itemBody = '';
            String header = '';
            String address = '';
            String trackingUrl='';
            String ResponseBody = '';
            ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
            System.debug(ShipStationUser.userName__c+'=========='+ShipStationUser.password__c);
            if(ShipStationUser != null){
                header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
            }
            try{
                Map<String, Object> storeDetailMap = new Map<String, Object>();
                Map<String, Object> orderItemsMap = new Map<String, Object>();
                Map<String, Object> OrderMapForSToreId = new Map<String, Object>();
                Map<String, Object> advanceOption = new Map<String, Object>();
                Map<String, Object> ordResult = new Map<String, Object>();
                Map<String, Object> Shipaddress = new Map<String, Object>();
                Map<String, Object> FulfillmentsMAP = new Map<String, Object>();
                Map<String, Object> FulfillFormattedmap = new Map<String, Object>();
                Map<String, Object> SInglefulfillmentInstance = new Map<String, Object>();
                Set <String> orderIdset = new Set <String>();
                List<Object> orderItemsList = new List<Object>();
                List<Object> Fulfillments = new List<Object>();
                List <ShipStation_Orders__c> orderList = new List<ShipStation_Orders__c>();
                List <ShipStation_Orders__c> existingOrders = new List <ShipStation_Orders__c>();
                
                HttpRequest req = new HttpRequest();
                req.setEndpoint(ShipStationUser.Domain_Name__c+'/fulfillments?orderId='+ord[0].orderId__c);      //pageSize=1&page='+ord[0]);//379775136
                req.setMethod('GET');
                req.setHeader('Authorization', 'Basic '+header);
                HttpResponse res = new HttpResponse();
            if(!Test.isRunningTest()) { 
                res= new Http().send(req);
                ResponseBody = res.getBody();
            }
            else {
                res.setStatusCode(200);
                ResponseBody = '{"fulfillments": [{"orderId": 28694680,"orderNumber": "101","carrierCode": "USPS","userId": "c9f06d74-95de-4263-9b04-e87095cababf","trackingNumber": "783408231234","shipDate": "2016-06-07T00:00:00.0000000","shipTo": {"street1": "abc","city": "dewas","state":"MP","postalCode": "452001","country":"INDIA","phone":"1234567890"}}]}';
            }
                
                System.debug('===========ResponseBodyUp============='+ResponseBody); 
                
                if(ResponseBody != null && res.getStatusCode()==200) {
                    
                    //callout for getting order detail for store id and items
                    HttpRequest req1 = new HttpRequest();
                    req1.setEndpoint(ShipStationUser.Domain_Name__c+'/orders/'+ord[0].orderId__c);
                    req1.setMethod('GET');
                    req1.setHeader('Authorization', 'Basic '+header);
                    HttpResponse res1 = new HttpResponse();
                    if(!Test.isRunningTest()) { 
                        res1= new Http().send(req1);
                    }
                    else {
                        res1.setStatusCode(200);
                        res1.setBody('{"advancedOptions": {"storeId":12345},"items":[{"name": "TestItem","quantity": 2, "unitPrice": 3}]}');
                    }
                    String ResponseBody1 = res1.getBody();
                    System.debug('===========ResponseBodyUp============='+ResponseBody1);
                    
                    if(ResponseBody1 != null && res.getStatusCode()==200) {
                        OrderMapForSToreId = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody1);
                        orderItemsList = (List<Object>)OrderMapForSToreId.get('items');
                        itemBody = '<table border="1" style="border-collapse: collapse"><tr><th>Name</th><th>Quantity</th><th>UnitPrice</th></tr>'; 
                        if(!orderItemsList.isEmpty()){
                            for(Object itmobj : orderItemsList) {
                                orderItemsMap = (Map<String, Object>)itmobj;
                                itemBody += '<tr><td>' + String.valueOf(orderItemsMap.get('name')) + '</td><td>' + String.valueOf(orderItemsMap.get('quantity')) + '</td><td>' + String.valueOf(orderItemsMap.get('unitPrice')) + '</td></tr>';
                            }
                            itemBody += '</table>';
                        }
                        if(OrderMapForSToreId.containsKey('advancedOptions')) {
                            
                            advanceOption = (Map<String, Object>)OrderMapForSToreId.get('advancedOptions');
                            storeId = String.valueOf(advanceOption.get('storeId'));
                            // callout for Store detail
                            HttpRequest req2 = new HttpRequest();
                            req2.setEndpoint(ShipStationUser.Domain_Name__c+'/stores/'+storeId);
                            req2.setMethod('GET');
                            req2.setHeader('Authorization', 'Basic '+header);
                            HttpResponse res2 = new HttpResponse();
                            if(!Test.isRunningTest()) { 
                                res2 = new Http().send(req2);
                            }
                            else {
                                res2.setStatusCode(200);
                                res2.setBody('{"storeId": 12345,"storeName": "WooCommerce Store","marketplaceId": 36,"marketplaceName": "WooCommerce","accountName": null,"email": null,"integrationUrl": "http://shipstation-test.wpengine.com","active": true,"companyName": "","phone": "","publicEmail": "","website": ""}');
                            }
                            String ResponseBody2 = res2.getBody();
                            System.debug('===========ResponseBodyUp============='+ResponseBody2);
                            storeDetailMap = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody2);
                            
                        }
                    }
                    
                    ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
                    if(ordResult.containsKey('fulfillments')) {
                        Fulfillments = (List<Object>)ordResult.get('fulfillments');
                        for(Object obj : Fulfillments) {
                            FulfillmentsMAP = (Map<String, Object>)obj;
                            String singleordId = String.valueOf(FulfillmentsMAP.get('orderId'));
                            FulfillFormattedmap.put(singleordId, obj);
                            orderIdset.add(singleordId);
                            
                        }
                        
                        existingOrders = [select id,orderId__c from ShipStation_Orders__c where orderId__c IN : orderIdset];
                        System.debug('=====existingOrders====='+existingOrders);
                        
                        if(!existingOrders.isEmpty()) {
                            for(ShipStation_Orders__c shipord: existingOrders) {
                                SInglefulfillmentInstance = (Map<String, Object>)FulfillFormattedmap.get(shipord.orderId__c);
                                Shipaddress =(Map<String, Object>) SInglefulfillmentInstance.get('shipTo');
                                address = String.valueOf(Shipaddress.get('street1'))+' '+String.valueOf(Shipaddress.get('city'))+'<br/>'+String.valueOf(Shipaddress.get('state'))+' '+String.valueOf(Shipaddress.get('postalCode'))+' '+String.valueOf(Shipaddress.get('country'))+'<br/>'+String.valueOf(Shipaddress.get('phone'));
                                System.debug('=========String.valueOf(SInglefulfillmentInstance.get("orderId"))========='+String.valueOf(SInglefulfillmentInstance.get('orderId')));
                                if(!SInglefulfillmentInstance.values().isEMpty() && String.valueOf(SInglefulfillmentInstance.get('carrierCode')) != null) {
                                    if(String.valueOf(SInglefulfillmentInstance.get('orderId')) == shipord.orderId__c) {
                                        shipord.Tracking_Number__c = String.valueOf(SInglefulfillmentInstance.get('trackingNumber'));
                                        shipord.Carrier_Code__c = String.valueOf(SInglefulfillmentInstance.get('carrierCode'));
                                        
                                        if(String.valueOf(SInglefulfillmentInstance.get('carrierCode')).equalsIgnoreCase('ups')) {
                                            trackingUrl = '<a href="https://www.ups.com/track?loc=en_US&tracknum='+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'&requester=NES&agreeTerms=yes/trackdetails">'+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'</a>';
                                        }
                                        else if(String.valueOf(SInglefulfillmentInstance.get('carrierCode')).equalsIgnoreCase('USPS')) {
                                            trackingUrl = '<a href="https://tools.usps.com/go/TrackConfirmAction.action?tLabels='+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'">'+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'</a>';
                                        }
                                        else if(String.valueOf(SInglefulfillmentInstance.get('carrierCode')).equalsIgnoreCase('FedEx')) {
                                            trackingUrl = '<a href="https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'&cntry_code=us&locale=en_US">'+String.valueOf(SInglefulfillmentInstance.get('trackingNumber'))+'</a>';
                                        }
                                        String serviceCode = '';
                                        shipord.Fulfillment_Service_Code__c = String.valueOf(SInglefulfillmentInstance.get('fulfillmentServiceCode'));
                                        if(String.valueOf(SInglefulfillmentInstance.get('fulfillmentServiceCode')) != null) {
                                            serviceCode = String.valueOf(SInglefulfillmentInstance.get('fulfillmentServiceCode'));
                                        }
                                        Shipord.Tracking_Email_Body__c = String.valueOf('Dear <b>'+String.valueOf(Shipaddress.get('name'))+',</b><br/><br/>Thank you for your order from LadyBoss! We wanted to let you know that your order <b>(#'+String.valueOf(SInglefulfillmentInstance.get('orderNumber'))+')</b> was shipped via <b>'+String.valueOf(SInglefulfillmentInstance.get('carrierCode'))+'</b>, '+serviceCode+' on <b>'+String.valueOf(SInglefulfillmentInstance.get('shipDate'))+'</b>.  You can track your package at any time using the link below.<br/><br/><b>Shipped To:</b><br/>'+address+'<br/>null<br/><b>Track Your Shipment: </b>'+trackingUrl+'<br/><br/>This shipment includes the following items:<br/><br/>'+itemBody+' <br/><br/>Thank you for your business! We can\'t wait for you to get your package!<br/><br/><b>'+String.valueOf(storeDetailMap.get('companyName'))+'<br/>Phone: '+String.valueOf(storeDetailMap.get('phone'))+'<br/>Email: '+String.valueOf(storeDetailMap.get('publicEmail'))+'<br/>Website: '+String.valueOf(storeDetailMap.get('website')))+'</b>';
                                        Shipord.Tracking_Email_Body__c = Shipord.Tracking_Email_Body__c.replace('null', '');
                                        shipord.Ship_Date__c = String.valueOf(SInglefulfillmentInstance.get('shipDate'));
                                        shipord.Email_Body_Updated__c = true;
                                    }
                                }
                            }
                        }
                        ShipstationOrderStatusShippedBatch.flag = true;
                        update existingOrders;
                    }
                }
                
            }
            catch(Exception e) {
                ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShipstationTrackingEmail',
                                 'Update Shipstation Tracking Email',
                                    ord[0].Id,
                                     e
                                 ));
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { 'patidarsurender9@gmail.com' };
                message.optOutPolicy = 'FILTER';
                message.subject = 'Please pheck your field order';
                message.plainTextBody = 'Hi Ashish please Check your failed Tracking Email Job:- '+ e +'\n See salesforceId======:'+ord[0].Id+'\n OrderId : ' +ord[0].orderId__c+ '\n Please Process again this Job';
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
        }
        
    }
    public void finish (Database.BatchableContext bc) {
        
    }
}