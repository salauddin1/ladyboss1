@isTest(SeeAllData = False)
public class SendManualSMSControllerTest {
    
    @isTest static void sendingSMS() {    
              
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        insert cnt;
        
        Test.startTest();
                
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.cntId = 'cnt.id';
        sms.messageData = 'String';
        sms.getSMSTemplates();
        sms.getSMSText();
        sms.sendSMS();
        Test.stopTest();
    }
    
    @isTest static void sendingSMS1() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
    }

    @isTest static void sendingSMS2() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;

        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.getSMSTemplates();
        sms.getSMSText();
        SendManualSMSController s = new SendManualSMSController ();
    }

    @isTest static void sendingSMS3() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;

        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        caseObject.Twilio_Number__c  = '45454545';
        insert caseObject;
        
        
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.getSMSTemplates();
        sms.getSMSText();
        SendManualSMSController s = new SendManualSMSController ();
    }
    
    @isTest static void sendingSMS4() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
    }
    
    @isTest static void sendingSMS5() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
    }
    
    @isTest static void sendingSMS6() {    
              
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.DoNotCall = true;
        cnt.Phone = '2343242345';
        cnt.MobilePhone = '342343';
        cnt.OtherPhone = '1233324';
        cnt.Phone_National_DNC__c = true;
        cnt.Other_Phone_National_DNC__c = true;
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        Test.startTest();
                
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('abc'));
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.cntId = 'cnt.id';
        sms.messageData = 'String';
        sms.getSMSTemplates();
        sms.getSMSText();
        sms.sendSMS();
        Test.stopTest();
    }
    
    @isTest static void sendingSMS7() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.MobilePhone = '1151515611';
        cnt.AssistantPhone = '9876543210';
        cnt.DoNotCall = true;
        cnt.Phone_National_DNC__c = true;
        cnt.Other_Phone_National_DNC__c = true;
        insert cnt;

        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        
        PageReference pageRef = Page.SendManualSMSFromCasePage;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.getSMSTemplates();
        sms.getSMSText();
        SendManualSMSController s = new SendManualSMSController ();
    }
    @isTest static void sendingSMS9() {    
              
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        //cnt.DoNotCall = true;
        cnt.Phone = '2343242345';
        cnt.MobilePhone = '342343';
        cnt.OtherPhone = '1233324';
        cnt.Phone_National_DNC__c = true;
        cnt.Other_Phone_National_DNC__c = true;
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        Test.startTest();
                
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('abc'));
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.cntId = 'cnt.id';
        sms.messageData = 'String';
        sms.getSMSTemplates();
        sms.getSMSText();
        sms.sendSMS();
        Test.stopTest();
    }
    @isTest static void sendingSMS10() {    
              
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Unsubscribe_Me__c = 'Yes';
        cnt.MobilePhone = '9876543210';
        //cnt.DoNotCall = true;
        cnt.Phone = '2343242345';
        cnt.MobilePhone = '342343';
        cnt.OtherPhone = '1233324';
        //cnt.Phone_National_DNC__c = true;
        cnt.Other_Phone_National_DNC__c = true;
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
        Test.startTest();
                
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('abc'));
        PageReference pageRef = Page.SendManualSMS;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(cnt);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.cntId = 'cnt.id';
        sms.messageData = 'String';
        sms.getSMSTemplates();
        sms.getSMSText();
        sms.sendSMS();
        Test.stopTest();
    }
    @isTest static void sendingSMS11() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.MobilePhone = '1151515611';
        cnt.AssistantPhone = '9876543210';
        //cnt.DoNotCall = true;
        cnt.Phone_National_DNC__c = true;
        cnt.Other_Phone_National_DNC__c = true;
        insert cnt;

        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        
        PageReference pageRef = Page.SendManualSMSFromCasePage;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.getSMSTemplates();
        sms.getSMSText();
        SendManualSMSController s = new SendManualSMSController ();
    }
    @isTest static void sendingSMS12() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.MobilePhone = '1151515611';
        cnt.AssistantPhone = '9876543210';
        cnt.DoNotCall = true;
        cnt.Phone_National_DNC__c = true;
        //cnt.Other_Phone_National_DNC__c = false;
        cnt.Unsubscribe_Me__c = 'Yes';
        insert cnt;

        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        
        PageReference pageRef = Page.SendManualSMSFromCasePage;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
        
        SendManualSMSController sms = new SendManualSMSController(sc);
        sms.getSMSTemplates();
        sms.getSMSText();
        SendManualSMSController s = new SendManualSMSController ();
    }
}