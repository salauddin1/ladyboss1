@isTest
public class TagCategory_Test {

   public static testMethod void test(){
        
        Case cas = new Case(Status ='New',IsFailed_Payment__c =true, Priority = 'Medium', Origin = 'Email', Reason='new');
        insert cas;
        
        Test.startTest();

        TagCategory.getCase(cas.id);
        TagCategory.getTags();
        TagCategory.getValues('Other');
        TagCategory.saveCase(cas.id, '','','Tte');
        Test.stopTest();
    }
}