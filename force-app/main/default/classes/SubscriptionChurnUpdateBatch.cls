global class SubscriptionChurnUpdateBatch implements  Database.Batchable<sObject>,Database.AllowsCallouts {
    String whereCon;
    public SubscriptionChurnUpdateBatch(String whereCon){
        this.whereCon = whereCon;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT End__c,Start__c,Subscription_Id__c,TrialPeriodEnd__c,TrialPeriodStart__c,status__c,Canceled_on__c,Started_on__c FROM OpportunityLineItem WHERE '+whereCon);
    }
    
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> oliList){
        for(OpportunityLineItem oli : oliList ){
            SubscriptionChurnUpdateBatch.getSubscription(oli.Subscription_Id__c, oli);
        }
        update oliList;
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    public static void getSubscription(String subId,OpportunityLineItem oli){
        String endpoint = 'https://api.stripe.com/v1/subscriptions/'+subId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endpoint);	
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){
        	hs = con.send(http);
        }else{
            hs.setStatusCode(200);
            hs.setBody('{"canceled_at": null,"current_period_end": 1570708800,"current_period_start": 1566504571,"start_date": 1556674727,"status": "trialing","trial_end": 1570708800,"trial_start": 1560545242}');
        }
        System.debug(hs.getBody());
        System.debug(hs.getStatusCode());
        if (hs.getstatusCode() == 200){
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            //status
            String sts = String.valueOf(results.get('status'));
            if(oli.Status__c==null || (oli.Status__c.toLowercase() != sts.toLowerCase())){
                
                if(sts=='canceled'){
                    if(oli.Status__c!='InActive'){
                    	oli.Status__c = 'InActive';
                    }
                }else{
                    oli.Status__c = sts;
                }
            }
            //cancel date
            if(oli.Canceled_on__c==null && results.get('canceled_at') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('canceled_at')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.Canceled_on__c = dt.date();
            }
            //start date
            System.debug(results.get('start_date'));
            if(oli.Started_On__c==null && results.get('start_date') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('start_date')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.Started_On__c = dt.date();
            }
            //current period start 
            if(oli.Start__c==null && results.get('current_period_start') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('current_period_start')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.Start__c = dt.date();
            }
            //current period end
            if(oli.End__c==null && results.get('current_period_end') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('current_period_end')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.End__c = dt.date();
            }
            //trial start
            if(oli.TrialPeriodStart__c==null && results.get('trial_start') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('trial_start')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.TrialPeriodStart__c = dt.date();
            }
            //trial end
            if(oli.TrialPeriodEnd__c==null && results.get('trial_end') != null){
                Long amt = Long.ValueOf(String.valueOf(results.get('trial_end')));
                Datetime dt = DateTime.newInstance( amt * 1000);
                oli.TrialPeriodEnd__c = dt.date();
            }
        }
    }
}