public class StripePlan {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class StripePlanObject {
		public Integer created {get;set;} 
		public Boolean livemode {get;set;} 
		public String id {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String object_data {get;set;} // in json: object
		//public String request {get;set;} 
		public Integer pending_webhooks {get;set;} 
		public String api_version {get;set;} 
		public Data data {get;set;} 

		public StripePlanObject(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'id') {
							id = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'request') {
							//request = parser.getText();
						} else if (text == 'pending_webhooks') {
							pending_webhooks = parser.getIntegerValue();
						} else if (text == 'api_version') {
							api_version = parser.getText();
						} else if (text == 'data') {
							data = new Data(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Object_Z {
		public String id {get;set;} 
		public String object_data {get;set;} // in json: object
		public Integer amount {get;set;} 
		public Integer created {get;set;} 
		public String currency_data {get;set;} 
		public String interval {get;set;} 
		public Integer interval_count {get;set;} 
		public Boolean livemode {get;set;} 
		public Metadata metadata {get;set;} 
		public String name {get;set;} 
		public String statement_descriptor {get;set;} 
		public String trial_period_days {get;set;} 

		public Object_Z(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'amount') {
							amount = parser.getIntegerValue();
						} else if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'interval') {
							interval = parser.getText();
						} else if (text == 'interval_count') {
							interval_count = parser.getIntegerValue();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'statement_descriptor') {
							statement_descriptor = parser.getText();
						} else if (text == 'trial_period_days') {
							trial_period_days = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Object_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Metadata {

		public Metadata(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Metadata consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public Object_Z object_data {get;set;} // in json: object

		public Data(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object_data') {
							object_data = new Object_Z(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static StripePlanObject parse(String json) {
		return new StripePlanObject(System.JSON.createParser(json));
	}
}