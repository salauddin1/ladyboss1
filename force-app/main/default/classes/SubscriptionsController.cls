public  class SubscriptionsController {
    @AuraEnabled
    public static List<productWrapper> getAllSubscriptions(String conID){
        List<productWrapper> pwList = new List<productWrapper>();
        for (OpportunityLineItem oli: [SELECT id, Product2.Name, Start__c,TrialPeriodStart__c,TrialPeriodEnd__c,End__c,TotalPrice, Status__c FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null]) {
            productWrapper pw = new productWrapper();
            pw.Id = oli.id;
            pw.Name = oli.Product2.Name;
            pw.periodStart = oli.Start__c;
            pw.periodEnd = oli.End__c;
            pw.Trialstart = oli.TrialPeriodStart__c;
            pw.Trialend = oli.TrialPeriodEnd__c;
            pw.totalPrice = oli.TotalPrice;
            pw.status = oli.Status__c;
            pwList.add(pw);
        }
        for (Opportunity opp : [SELECT id, Scheduled_Payment_Date__c,Scheduled_Payment_Processed__c,Scheduled_Payment_Canceled__c,(SELECT id, Product2.Name, TotalPrice FROM OpportunityLineItems) FROM Opportunity WHERE RecordType.Name = 'Subscription' AND Contact__c=:conID AND Contact__c!=null AND Scheduled_Payment_Date__c != null AND (Scheduled_Payment_Processed__c = false OR Scheduled_Payment_Canceled__c = true)]) {
            for (OpportunityLineItem oli : opp.OpportunityLineItems) {
                productWrapper pw = new productWrapper();
                pw.Id = oli.id;
                pw.Name = oli.Product2.Name;
                pw.totalPrice = oli.TotalPrice;
                pw.status = 'Scheduled';
                pw.ScheduledPaymentDate = opp.Scheduled_Payment_Date__c;
                pw.ScheduleCanceled = opp.Scheduled_Payment_Canceled__c;
                pwList.add(pw);
            }
        }
        System.debug('Opportunityfvfv ==> ' + [SELECT id, Product2.Name, Start__c,TrialPeriodStart__c,TrialPeriodEnd__c,End__c,TotalPrice, Status__c,OpportunityId FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND OpportunityId != null]);
        for (OpportunityLineItem oli: [SELECT id, Product2.Name, Start__c,TrialPeriodStart__c,TrialPeriodEnd__c,End__c,TotalPrice, Status__c,OpportunityId FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND OpportunityId != null]) {
            for (Opportunity opp : [SELECT id, Is_PayPal__c, Paypal_Transaction_Id__c FROM Opportunity WHERE id =: oli.OpportunityId AND Is_PayPal__c = true AND Paypal_Transaction_Id__c != null]) {
                if(oli.OpportunityId == oli.id) {
                    productWrapper pw = new productWrapper();
                    pw.Id = oli.id;
                    pw.Name = oli.Product2.Name;
                    pw.periodStart = oli.Start__c;
                    pw.periodEnd = oli.End__c;
                    pw.Trialstart = oli.TrialPeriodStart__c;
                    pw.Trialend = oli.TrialPeriodEnd__c;
                    pw.totalPrice = oli.TotalPrice;
                    pw.status = oli.Status__c;
                    pwList.add(pw);
                }
            }
        }
        return pwList;
    }
    
    @AuraEnabled
    public static List<productWrapper> getSubscriptions(String conID, String listView){
        if (listView=='sAll') {
            return SubscriptionsController.getAllSubscriptions(conID);
        }else {
            return SubscriptionsController.getSelectedSubscriptions(conID,listView);
        }
    }
    @AuraEnabled
    public static List<productWrapper> getSelectedSubscriptions(String conID,String listView){
        List<productWrapper> pwList = new List<productWrapper>();
        for (OpportunityLineItem oli: [SELECT id, Product2.Name, Start__c, End__c,TrialPeriodStart__c,TrialPeriodEnd__c, TotalPrice, Status__c FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Status__c = :listView AND Opportunity.Contact__c!=null]) {
            productWrapper pw = new productWrapper();
            pw.Id = oli.id;
            pw.Name = oli.Product2.Name;
            pw.periodStart = oli.Start__c;
            pw.periodEnd = oli.End__c;
            pw.totalPrice = oli.TotalPrice;
            pw.Trialstart = oli.TrialPeriodStart__c;
            pw.Trialend = oli.TrialPeriodEnd__c;
            pw.status = oli.Status__c;
            pwList.add(pw);
        }
        return pwList;
    }
    @AuraEnabled
    public static void refreshSubscription(String conIDwhen){
        try{	
            List<OpportunityLineItem> oppLIs = new List<OpportunityLineItem>(); 
            Map<String,Object> subIDs = new Map<String,Object>();
            List<Object> ob = new List<Object>();
            for (Stripe_Profile__c sp: [SELECT Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Customer__c =: conIDwhen]) {
                if(sp.Stripe_Customer_Id__c!=null){
                    ob.addAll(getSubscriptionsFromStripe(sp.Stripe_Customer_Id__c));
                }
            }
            Map<String, Object> obmap1=new Map<String, Object>();
            Map<String,String> subIdSalesPersonMap = new Map<String,String>();
            system.debug('Data object--'+ob);
            for(Object ob1 : ob){
                obmap1=(Map<String, Object>)ob1;
                if((Map<String, Object>)obmap1.get('metadata')!=null && ((Map<String, Object>)obmap1.get('metadata')).get('Sales Person')!=null){
                    String sp = (String)((Map<String, Object>)obmap1.get('metadata')).get('Sales Person');
                    subIdSalesPersonMap.put(String.valueof(obmap1.get('id')),sp);
                }
                subIDs.put(String.valueof(obmap1.get('id')),ob1);
            }
            List<User> usrList = [select id,Name from user where name in : subIdSalesPersonMap.values()];
            Map<String,User> usrNameToIdMap = new Map<String,User>();
            for(User u : usrList){
                usrNameToIdMap.put(u.name,u);
            }
            Map<String,User> subIdSpMap = new Map<String,User>();
            for(String st : subIdSalesPersonMap.keyset()){
                subIdSpMap.put(st,usrNameToIdMap.get(subIdSalesPersonMap.get(st)));
            }
            System.debug('subIdSpMap : '+subIdSpMap);
            
            List<String> subid = new List<String>();
            subid.addAll(subIDs.keySet());
            Set<String> planIds1= new Set<String>();
            for (String subidFromStripe:subid) {
                Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
                String planID = String.valueOf(obj.get('id')).trim();
                planIds1.add(planID);//
                System.debug('All plan ids'+planIds1);
            }
            system.debug('subscription ids from stripe--'+subid);
            list<OpportunityLineItem> oliupdate=[select id,OpportunityId,start__c,End__c,Status__c,Subscription_Id__c,TrialPeriodStart__c,TrialPeriodEnd__c  from OpportunityLineItem where Subscription_Id__c IN :subid ];
            system.debug('oliupdate'+oliupdate);
            Map<String,OpportunityLineItem> existingSubOliMap = new Map<String,OpportunityLineItem>();
            for(OpportunityLineItem oli:oliupdate){
                if(!existingSubOliMap.containsKey(oli.Subscription_Id__c)){
                    existingSubOliMap.put(oli.Subscription_Id__c,oli);  
                    subid.remove(subid.indexOf(oli.Subscription_Id__c));
                }                       
            }
            List<OpportunityLineItem> opliToUpdate = new List<OpportunityLineItem>();
            system.debug('oliupdate'+oliupdate);
            for (String subidFromStripe:existingSubOliMap.keySet()) {            
                OpportunityLineItem opli = existingSubOliMap.get(subidFromStripe);
                Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                if (obmap!=null) {
                    if(obmap.get('status')=='canceled'){
                        opli.Status__c = 'InActive';
                    }else if (obmap.get('status')=='active'){
                        opli.Status__c = 'Active';
                    }else if (obmap.get('status')=='trialing'){
                        opli.Status__c = 'trialing';
                    }else if (obmap.get('status')=='past_due') {
                        opli.Status__c = 'Past Due';
                    }
                    opli.Number_Of_Payments__c = Invoices.getInvoice(subidFromStripe);
                    string unixDatetime1 = String.valueOf(obmap.get('current_period_start'));
                    datetime dT = datetime.newInstance(Long.ValueOf(unixDatetime1 )* Long.ValueOf('1000'));
                    opli.Start__c = date.newinstance(dT.year(), dT.month(), dT.day());  
                    string unixDatetime2 = String.valueOf(obmap.get('current_period_end'));
                    datetime dT1 = datetime.newInstance(Long.ValueOf(unixDatetime2 )* Long.ValueOf('1000'));
                    opli.End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                    if(obmap.get('trial_start') !=null && obmap.get('trial_end') !=null){
                        String datetime2 = String.valueOf(obmap.get('trial_start'));
                        datetime dT2 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                        opli.TrialPeriodStart__c =date.newinstance(dT2.year(), dT2.month(), dT2.day()); 
                        String datetime3 = String.valueOf(obmap.get('trial_end'));
                        datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                        opli.TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                    }
                    opliToUpdate.add(opli);
                }   
            }
            
            if (subid.size()>0) {
                List<Product2> productToUpdate = new List<Product2>();
                List<PricebookEntry> pricebookToInsert = new List<PricebookEntry>();
                List<product2> prods= [SELECT id,Name,Price__c,Stripe_plan_Id__c FROM Product2 WHERE Stripe_plan_Id__c IN :planIDs1];
                System.debug('prods'+prods);
                List<PriceBookEntry> priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_plan_Id__c FROM PriceBookEntry WHERE Product2Id in :prods AND PriceBook2.isStandard=true];
                System.debug('pricebookentery'+priceBookList);
                Map<String,PriceBookEntry> priceBookMap = new Map<String,PriceBookEntry>();
                Map<String,product2> prodMap = new Map<String,product2>();
                for(PriceBookEntry pbookEntry:priceBookList){
                    priceBookMap.put(pbookEntry.Product2.Stripe_plan_Id__c,pbookEntry);
                }
                for(product2 prod:prods){
                    prodMap.put(prod.Stripe_plan_Id__c,prod);
                }
                Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];
                for (String planID: planIds1) {
                    if (!prodMap.containsKey(planID)) {
                        StripeGetPlan splan = StripeGetPlan.getPlan(planID);
                        StripeCreateProduct scp = StripeCreateProduct.getProduct(splan.product);
                        Product2 product = new Product2();                  
                        product.Name = scp.name;                    
                        product.Price__c = splan.amount/100;
                        product.Stripe_Plan_Id__c = planID;
                        productToUpdate.add(product);
                    }
                }
                if (productToUpdate.size()>0) {
                    insert productToUpdate;
                }
                for (Product2 pr: productToUpdate) {
                    PricebookEntry pbe = new PricebookEntry (Pricebook2Id=pb.id, Product2Id=pr.id, IsActive=true, UnitPrice=pr.Price__c);
                    pricebookToInsert.add(pbe);             
                }
                if (pricebookToInsert.size()>0) {
                    insert pricebookToInsert;
                }
                for (Product2 sObj: productToUpdate) {
                    prodMap.put(sObj.Stripe_plan_Id__c, sObj);
                }
                for (PricebookEntry sObj: [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_plan_Id__c FROM PriceBookEntry WHERE ID in:pricebookToInsert]) {
                    priceBookMap.put(sObj.Product2.Stripe_plan_Id__c,sObj);
                }
                Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
                List<String> subNewOpp = new List<String>();
                for (Opportunity opp: [SELECT id,Subscription__c FROM Opportunity WHERE Subscription__c in :subid]) {
                    oppMap.put(opp.Subscription__c, opp);
                }
                for (String subidFromStripe: subid) {
                    if (!oppMap.containsKey(subidFromStripe)) {
                        subNewOpp.add(subidFromStripe);
                    }
                }
                if (subNewOpp.size()>0) {               
                    List<Opportunity> opps = new List<Opportunity>();
                    RecordType rec1 = [SELECT Id FROM RecordType WHERE Name = 'Subscription'];
                    for (String subidFromStripe: subNewOpp) {
                        Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                        Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
                        String planID = String.valueOf(obj.get('id')).trim();
                        Product2 prod= prodMap.get(PlanID);
                        Opportunity opp = new Opportunity();
                        opp.Name = prod.Name+'CLUB';
                        opp.Contact__c = conIDwhen;
                        opp.RecordTypeId=rec1.id;
                        if(subIdSpMap.get(subidFromStripe)!=null){
                            opp.Sales_Person_Id__c = subIdSpMap.get(subidFromStripe).Id;
                        }
                        opp.StageName = 'Closed Won';                   
                        opp.Subscription__c = subidFromStripe;
                        string unixDatetime = String.valueOf(obmap.get('current_period_end'));
                        datetime dt = datetime.newInstance(Long.ValueOf(unixDatetime )*Long.ValueOf('1000'));               
                        opp.CloseDate = dt.date();  
                        opps.add(opp);                  
                    }
                    if (opps.size()>0) { 
                        insert opps;
                    }
                    for(Opportunity opp:opps){
                        oppMap.put(opp.Subscription__c, opp);
                    }
                }
                for (String subidFromStripe:subid) { 
                    Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                    Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
                    String planID = String.valueOf(obj.get('id')).trim();
                    Product2 prod= prodMap.get(planID);
                    PriceBookEntry priceBook = priceBookMap.get(planID);
                    System.debug('PriceBookEntry'+priceBook);                       
                    Opportunity opp = oppMap.get(subidFromStripe);                          
                    OpportunityLineItem opline = new OpportunityLineItem();
                    opline.OpportunityId = opp.Id;
                    opline.PricebookEntryId=priceBook.Id;
                    opline.Product2Id = prod.Id;
                    if (String.valueOf(obmap.get('status')) =='active') {
                        opline.Status__c = 'Active';
                    }else if (String.valueOf(obmap.get('status')) =='canceled') {
                        opline.Status__c = 'InActive';
                    }else if (obmap.get('status')=='trialing'){
                        opline.Status__c = 'trialing';
                    }else if (obmap.get('status')=='past_due') {
                        opline.Status__c = 'Past Due';
                    }
                    string unixDatetime2 = String.valueOf(obmap.get('current_period_end'));
                    datetime dt2 = datetime.newInstance(Long.ValueOf(unixDatetime2 )* Long.ValueOf('1000'));    
                    opline.End__c = dt2.date();
                    opline.TotalPrice = prod.Price__c;
                    opline.Subscription_Id__c=subidFromStripe;
                    string unixDatetime1 = String.valueOf(obmap.get('current_period_start'));
                    datetime dt1 = datetime.newInstance(Long.ValueOf(unixDatetime1 )* Long.ValueOf('1000'));    
                    opline.Start__c= dt1.date();
                    if(obmap.get('trial_start') !=null && obmap.get('trial_end') !=null){
                        String datetime2 = String.valueOf(obmap.get('trial_start'));
                        datetime dT4 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                        opline.TrialPeriodStart__c =date.newinstance(dT4.year(), dT4.month(), dT4.day()); 
                        String datetime3 = String.valueOf(obmap.get('trial_end'));
                        datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                        opline.TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                    }
                    opline.quantity =1;              
                    opline.Plan_ID__c =planID;  
                    oppLIs.add(opline);                 
                }       
                if (oppLIs.size()>0) {
                    PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
                    for(OpportunityLineItem ol : oppLIs){
                        ol.Consider_for_commission__c = true;
                    }
                }
            }
            if(opliToUpdate.size()>0){
                oppLIs.addAll(opliToUpdate);
            }
            if(oppLIs.size()>0){
                upsert oppLIs;
            }
        }
        catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('refreshSubscription','SubscriptionsController','addId',ex));
            System.debug('***Get Exception***'+ex);
        }
    }
    public static List<Object> getSubscriptionsFromStripe(String customerID){
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/subscriptions?customer=' + customerID+ '&limit=100&status=all');
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{"data":[{ "id": "sub_EypY8AdfVmqvVo", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1463862018, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "created": 1463862018, "current_period_end": 1558470018, "current_period_start": 1526934018, "customer": "cus_Eug8juCETidS33", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_18RWRXFzCf73siP0wyYdrGUg", "object": "subscription_item", "billing_thresholds": null, "created": 1463862019, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_EypY8AdfVmqvVo"} ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_EypY8AdfVmqvVo" }, "latest_invoice": "in_1CUKVkFzCf73siP0ka6Oqdrp", "livemode": false, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1463862018, "status": "active", "tax_percent": null, "trial_end": 1463862018, "trial_start": 1463862018 }]}') ;
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
        System.debug('results'+results);
        List<Object> data = (List<Object>)results.get('data');
        system.debug('#### cards #### '+ data);
        List<String> subId = new List<String>();
        integer counter=0;
        for(Object ob : data){
            Map<String, Object> obmap = (Map<String, Object>)ob;
            subId.add(String.valueof(obmap.get('id')));
            counter=counter+1;
            if(counter==1)
                break;
        }
        return data;
    }
    
    public class productWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public Date periodStart;
        @AuraEnabled public Date periodEnd;
        @AuraEnabled public Date Trialstart;
        @AuraEnabled public Date Trialend;
        @AuraEnabled public Date ScheduledPaymentDate;
        @AuraEnabled public Boolean ScheduleCanceled = false;
        @AuraEnabled public String Id;
        @AuraEnabled public Decimal totalPrice;
        @AuraEnabled public String status;
    }
    @AuraEnabled
    public static string  setCancelDateOnSchedulePayment(String opplineItem,Date cancelDate){
        String returnSuccess = 'No Opportunity found';
        Date today = Date.today();
        if(today == cancelDate){
            List<OpportunityLineItem> opplineData =[ SELECT Id, Cancel_Subscription_At__c,OpportunityId FROM OpportunityLineItem  where Id =: opplineItem LIMIT 1 ];
            if (opplineData.size()>0) {
                List<Opportunity> oppData = [SELECT id,Scheduled_Payment_Processed__c,Scheduled_Payment_Canceled__c FROM Opportunity WHERE id =:opplineData[0].OpportunityId];
                if (oppData.size()>0) {
                    oppData[0].Scheduled_Payment_Processed__c = true;
                    oppData[0].Scheduled_Payment_Canceled__c = true;
                    opplineData[0].Cancel_Subscription_At__c = null;
                    update oppData;
                    update opplineData;
                    returnSuccess = 'Successfully Canceled Payment';
                }
            }
        }else{
            List<OpportunityLineItem> opplineData =[ SELECT Id,Cancel_Subscription_At__c, OpportunityId FROM OpportunityLineItem  where Id =: opplineItem LIMIT 1 ];
            opplineData[0].Cancel_Subscription_At__c = cancelDate;
            update opplineData;
            returnSuccess = 'Successfully Set cancellation date';
        }
        return returnSuccess;
    }
    @AuraEnabled
    public static string  cancelSubscription(String opplineItem){
        String returnSucess='Subscription not canceled';
        try{
            List<OpportunityLineItem> oli = [select id,Subscription_Id__c,opportunityId from OPPORTUNITYLINEITEM WHERE id=:opplineItem limit 1];
            Opportunity opp = [select id,Success_Failure_Message__c from Opportunity where Id =:oli[0].opportunityId];
            HttpRequest http = new HttpRequest();  
            http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli[0].Subscription_Id__c);
            http.setMethod('DELETE');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            try{
                hs = con.send(http);
            } 
            catch(Exception e) {
                System.debug('------------ Exception---------------'+e);   
            }
            Integer statusCode = hs.getStatusCode();
            
            if (hs.getstatusCode() == 200){
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                if(String.valueOf(results.get('status')).Contains('canceled')) {
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    oli[0].Status__c = 'InActive';
                }else 
                {
                    oli[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
                }
                oli[0].Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));
                update oli; 
                
                returnSucess= 'Subscription cancelled in stripe';
                
            }else{
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                returnSucess= String.valueOf(errorMap.get('message'));
                if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    update oli; 
                }
                
            }
            return returnSucess;
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('cancelSubscription','SubscriptionsController',opplineitem,ex));
            system.debug('## Exception ## '+ ex);
            return returnSucess;    
        }
    }
    @AuraEnabled
    public static string updateSchedulePayment(String LineItemId,String dateVal){
        String returnSuccess = 'No Opportunity found';
        List<OpportunityLineItem> opplineData =[ SELECT Id, OpportunityId FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        if (opplineData.size()>0) {
            List<Opportunity> oppData = [SELECT id,Scheduled_Payment_Date__c,Scheduled_Payment_Processed__c,Scheduled_Payment_Canceled__c FROM Opportunity WHERE id =:opplineData[0].OpportunityId];
            if (oppData.size()>0) {
                oppData[0].Scheduled_Payment_Date__c = Date.valueOf(dateVal);
                if (oppData[0].Scheduled_Payment_Processed__c) {
                    oppData[0].Scheduled_Payment_Processed__c = false;
                }
                if (oppData[0].Scheduled_Payment_Canceled__c) {
                    oppData[0].Scheduled_Payment_Canceled__c = false;
                }
                update oppData;
                returnSuccess = 'Successfully changed payment date';
            }
        }
        return returnSuccess;
    }
    @AuraEnabled
    public static string updateSubscription(String LineItemId,String dateVal){
        
        String returnSucess='Subscription not updated';
        try{
            Integer noOfDays ;
            if(dateVal!=null){
                noOfDays = Date.Today().daysBetween(Date.valueOf(dateVal));
            }
            List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c, End__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
            List<Opportunity> opp = [select id,Success_Failure_Message__c, Is_PayPal__c from Opportunity where Id =:oppData[0].opportunityId];
            String l1 = '';
            if(dateVal!=null){
                List<String> dateParts = dateVal.split('-');
                Datetime dateWithTime = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
                dateVal = dateWithTime.format('yyyy-MM-dd HH:mm:ss');
                Datetime d = Datetime.valueOf(dateVal);
                d= d.AddDays(1);
                d= d.AddDays(-1);
                Long l = d.getTime();
                system.debug(l);
                Long x = (Long)Datetime.valueOf(l).getTime();
                String a = string.valueOf(x);
                l1= a.substring(0,a.length()-3);
            }
            if(opp[0].Is_PayPal__c == true) {
                oppData[0].End__c = Date.valueOf(dateVal);
                update oppData; 
                returnSucess = 'Subscription Updated in PayPal';
                return returnSucess;
            } else {
                HttpRequest http = new HttpRequest();  
                http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppData[0].Subscription_Id__c);        
                http.setMethod('POST');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                
                String bodyVal;
                Boolean prorateBool = false;
                //bodyVal = '&quantity='+qty+'&prorate='+prorateBool;
                bodyVal = '&prorate='+prorateBool;
                bodyVal += '&trial_end='+l1;
                bodyVal += '&';
                System.debug(bodyVal );
                http.setBody(bodyVal );
                String response;
                Integer statusCode;
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                
                hs = con.send(http);
                if (hs.getstatusCode() == 200){
                    StripeListSubscriptionClone.Charges varCharges;
                    JSONParser parse;
                    system.debug('#### '+ 1);
                    
                    
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    oppData[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
                    update oppData; 
                    opp[0].OwnerId = UserInfo.getUserId();
                    update opp;
                    returnSucess = 'Subscription updated in stripe';
                    
                    
                }else{
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                    returnSucess= String.valueOf(errorMap.get('message'));
                    if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                        oppData[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                        update oppData; 
                    }
                }
                
                system.debug('#### '+ hs.getBody());
                return returnSucess;
            }
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('cancelSubscription','SubscriptionsController',LineItemId +'  GetData  ' +dateVal,ex));
            returnSucess = 'Please contact your administrator Subscription not updated';
            system.debug('## Exception ## '+ ex);
            return returnSucess;    
        }
    }
    @AuraEnabled
    public static string setCancelDate(String LineItemId,Date cancelDate,Boolean nullBalance){        
        List<OpportunityLineItem> oppData =[ SELECT Id, Cancel_Subscription_At__c,Opportunity.Contact__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        
        Date today = Date.today();
        system.debug('today ==> cancel date'+today+' '+cancelDate);
        String msg = '';
        if (cancelDate == today) {
            msg = cancelSubscription(LineItemId);
            
        }else {
            oppData[0].Cancel_Subscription_At__c  = cancelDate;
            update oppData; 
            msg = 'Succesfully set cancel date';
        }
        if (nullBalance) {
            Contact con = [SELECT Available_Commission__c FROM Contact WHERE id =:oppData[0].Opportunity.Contact__c];
            con.Available_Commission__c = 0;
            con.Update_Balance_Reason__c  = 'All subscription cancelled';
            update con;
        }
        return msg;
    }
    @AuraEnabled
    public static string payPastDueInvoice(String LineItemId){
        String message ;
        List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        if (oppData.size()>0) {
            if (oppData[0].Subscription_Id__c != null) {
                String invoiceId = Invoices.getOpenInvoice(oppData[0].Subscription_Id__c);
                if (invoiceId != null) {
                    message = Invoices.payInvoice(invoiceId);
                }
            }
        }
        return message;
    }
    @AuraEnabled
    public static contactWrapper checkIfAffiliate(String conID){
        List<OpportunityLineItem> oppList = [SELECT id FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND (Status__c = 'Active' OR Status__c = 'trialing')];
        Contact con = [SELECT id,Available_Commission__c FROM Contact WHERE id =:conID];
        contactWrapper cw = new contactWrapper();
        cw.commission = con.Available_Commission__c == null ? 0 : con.Available_Commission__c;
        cw.hasCommission = false;
        cw.isLastSub = false;
        if (cw.commission != 0) {
            cw.hasCommission = true;
        }
        if (oppList.size()==1) {
            cw.isLastSub = true;
        }
        return cw;
        
    }
    public class contactWrapper{
        @AuraEnabled public Decimal commission;
        @AuraEnabled public Boolean hasCommission;
        @AuraEnabled public Boolean isLastSub;
    }
}