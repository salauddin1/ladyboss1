@isTest
public class DueDateChangeTaskByComp_Test {
    @isTest
    public static void test() {
        Send_Email_for_Phone_2_min_survey__c s = new Send_Email_for_Phone_2_min_survey__c();
        s.Send_Email__c = false;
       	s.Name = 'test';
        insert s;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Donni';
        t.ActivityDate = date.valueof(system.now());
        t.Status='Not Started';
        t.Priority='Normal';
        insert t;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        
        askd.Not_interested__c= 1;
        askd.Not_Time_Yet__c= 1;
        askd.Reordered_Online__c= 1;
        askd.No_Answer__c= 1;
        askd.Closed_Sale__c= 1;
        askd.Do_Not_Call__c= 1;
        
        
        insert askd;
        DueDateChangeTaskByComp.getAllTask(t.Id);
        DueDateChangeTaskByComp.Changestatus(t.Id,'Closed_Sale__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Closed_Sale__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_interested__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_interested__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_Time_Yet__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_Time_Yet__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Do_Not_Call__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Do_Not_Call__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Reordered_Online__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Reordered_Online__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_Time_Yet__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Not_Time_Yet__c', 'false');
        DueDateChangeTaskByComp.Changestatus(t.Id,'No_Answer__c', 'true');
        DueDateChangeTaskByComp.Changestatus(t.Id,'Activitydate', string.valueof(system.now()));
        DueDateChangeTaskByComp.Changestatus(t.Id,'No_Answer__c', 'false');
    }
}