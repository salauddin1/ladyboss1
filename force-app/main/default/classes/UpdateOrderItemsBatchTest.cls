@isTest
public class UpdateOrderItemsBatchTest {
 	@isTest 
    public Static void testupdate(){
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email='Test@gmail.com';
        insert con;
        Shopify_Parameters__c shopData = new Shopify_Parameters__c();
        shopData.Access_Token__c = 'testToken12';
        shopData.Domain_Name__c = 'https://test.com';
        shopdata.Is_Active__c = true;
        insert shopData;
        Shopify_Orders__c shopOrder = new Shopify_Orders__c();
        shopOrder.phone__c = '2345678';
        shopOrder.address1__c = 'Indore city';
        shopOrder.city__c = 'Indore';
        shopOrder.country__c = 'India';
        shopOrder.Contact__c = con.Id;
        shopOrder.Status__c = 'Confirmed';
        insert shopOrder;
        ShopifyLineItem__c itm = new ShopifyLineItem__c ();
        itm.Shopify_Orders__c = shopOrder.Id;
        itm.Contact__c = con.Id;
        insert itm;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new OrderCalloutTest()) ;
		UpdateOrderItemsBatch ord = new UpdateOrderItemsBatch();
        Database.executeBatch(ord);
        Test.stopTest();
    }
}