global class GetOrderFromShipStation implements Database.Batchable<Integer>,Database.AllowsCallouts,Database.Stateful {
    global Integer pagenumber=0;
    Integer count =0;
    global Static Boolean flag = false;
    global Iterable<Integer> start(Database.batchableContext info){
        return new ShipstationIterable();
    } 
    global void execute(database.batchablecontext bd, List<Integer> scope){
        String header;
        String FaieldOrderId;
        pagenumber = scope[0];
        Shipstation_pages__c orddays = [select Pages__c,Number_of_Days__c from Shipstation_pages__c limit 1];
        System.debug('================pagenumber=================='+pagenumber);
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c,password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }   
        Datetime StartDate = DateTime.now();
        String StartDattime = String.valueOf(StartDate.addDays(-(Integer.valueOf(orddays.Number_of_Days__c))));
        StartDattime = StartDattime.replace(' ', 'T');
        String EndDate = String.valueOf(DateTime.now());
        EndDate = EndDate.replace(' ', 'T');
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?createDateStart='+StartDattime+'&createDateEnd='+EndDate+'&pageSize=1&page='+pagenumber);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Accept', 'application/json');
            req.setTimeout(60000);
            String ResponseBody;
            
            HttpResponse res = new Http().send(req);
            ResponseBody = res.getBody();
            System.debug('=================ResponseBody111==========='+ResponseBody);
            
            Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
            List<Object> orders = (List<Object>)ordResult.get('orders');
            System.debug('==========orders======='+Orders);
            Map<String, Object> orderMap = new Map<String, Object> ();
            Map<String, object> Customer = new Map<String, object> ();
            Map<String, Object> OrderIdMap = new Map<String, Object> ();
            Set<String> Emailset = new Set<String>();
            Set<String> orderId = new Set<String>();
            Set<String> conemail = new set<String>();
            set<Contact> insCustomerSet = new set<Contact>();
            List<Contact> insCustomer = new List<Contact>();
            Integer i=0;
            
            if(!orders.isEmpty()){
                for(Object obj : orders){
                    orderMap = (Map<String, Object>)obj;
                    orderId.add(String.valueOf(OrderMap.get('orderId')));
                    Emailset.add(String.valueOf(OrderMap.get('customerEmail')));
                    OrderIdMap.put(String.valueOf(OrderMap.get('orderId')) , obj);
                }
            }
            List<Contact> conlist = [select id,email from Contact where email IN: Emailset];
            for(contact con : conlist){
                if(String.valueOf(con.email) != null)
                    conemail.add(String.valueOf(con.email).toLowerCase());
                
            }	
            System.debug('=====Emailset======='+Emailset);
            System.debug('=====conemail======='+conemail);
            
            for(Object ord: orders){
                orderMap=(Map<String, object>)ord;
                Map<String, Object> addressArray =  (Map<String, Object>)orderMap.get('shipTo');
                System.debug('=====conemail======='+conemail+'==========='+String.valueOf(OrderMap.get('customerEmail')));
                if(OrderMap.get('customerEmail') != null){
                    if(!conemail.contains(String.valueOf(OrderMap.get('customerEmail')).toLowerCase())) {
                        String strs=String.valueOf(addressArray.get('name'));
                        Contact shipcontact = new Contact();
                        if(strs.contains(' ')){
                            List<String> StrName=strs.split(' ');
                            shipcontact.FirstName=strName[0];
                            shipcontact.lastName=strName[1];
                        }
                        else{
                            shipcontact.LastName=strs;  
                        }
                        if(!String.isNotBlank(shipcontact.LastName)){
                            shipcontact.LastName='missing';
                        }
                        shipcontact.Email = String.valueOf(OrderMap.get('customerEmail'));
                        shipcontact.ShipStation_customer__c = true;
                        shipcontact.Processed_from_shipStation_batch__c = true;
                        if(orderMap.containsKey('shipTo')) {
                            shipcontact.Phone = String.valueOf(addressArray.get('phone'));
                            shipcontact.MailingStreet = String.valueOf(addressArray.get('street1'));
                            shipcontact.MailingCity = String.valueOf(addressArray.get('city'));
                            shipcontact.MailingCountry = String.valueOf(addressArray.get('country'));
                            shipcontact.MailingState = String.valueOf(addressArray.get('state'));
                            shipcontact.MailingPostalCode = String.valueOf(addressArray.get('postalCode'));
                        }
                        insCustomerSet.add(shipcontact);
                        conemail.add(String.valueOf(OrderMap.get('customerEmail')));
                    }
                }
                else{
                    String strs=String.valueOf(addressArray.get('name'));
                    Contact shipcontact = new Contact();
                    if(strs.contains(' ')){
                        List<String> StrName=strs.split(' ');
                        shipcontact.FirstName=strName[0];
                        shipcontact.lastName=strName[1];
                    }
                    else{
                        shipcontact.LastName=strs;  
                    }
                    if(!String.isNotBlank(shipcontact.LastName)){
                        shipcontact.LastName='missing';
                    }
                    shipcontact.Email = String.valueOf(OrderMap.get('customerEmail'));
                    shipcontact.ShipStation_customer__c = true;
                    shipcontact.Processed_from_shipStation_batch__c = true;
                    if(orderMap.containsKey('shipTo')) {
                        shipcontact.Phone = String.valueOf(addressArray.get('phone'));
                        shipcontact.MailingStreet = String.valueOf(addressArray.get('street1'));
                        shipcontact.MailingCity = String.valueOf(addressArray.get('city'));
                        shipcontact.MailingCountry = String.valueOf(addressArray.get('country'));
                        shipcontact.MailingState = String.valueOf(addressArray.get('state'));
                        shipcontact.MailingPostalCode = String.valueOf(addressArray.get('postalCode'));
                    }
                    insCustomerSet.add(shipcontact);
                    conemail.add(String.valueOf(OrderMap.get('customerEmail')));  
                }
            }
            for(contact con : insCustomerSet){
                insCustomer.add(con);
            }
            flag = true;
            
            if(!insCustomer.isEmpty()) {
                
                insert insCustomer;
                System.debug('======insCustomer====='+insCustomer);
            }
            List <ShipStation_Orders__c> orderList = new List<ShipStation_Orders__c>();
            List<ShipStation_Order_Items__c> productItems   = new List<ShipStation_Order_Items__c>();
            List<ShipStation_Orders__c> existingOrders  = [select id, orderId__c from ShipStation_Orders__c where orderId__c IN : orderId];
            Set<String> existingOrderId = new Set<String>();
            for(ShipStation_Orders__c ordId : existingOrders) {
                existingOrderId.add(ordId.orderId__c);
            }
            
            if(!orders.isEmpty()){
                for(Object ordre: orders){
                    orderMap=(Map<String, object>)ordre;
                    System.debug('=========orderMap==========='+orderMap);
                    FaieldOrderId = String.valueOf(orderMap.get('orderId'));
                    Customer =(Map<String, object>)orderMap.get('customer');
                    Map<String, Object> addressArray =  (Map<String, Object>)orderMap.get('shipTo');
                    Map<String, Object> addressbillArray =  (Map<String, Object>)orderMap.get('billTo');
                    
                    String emails = String.valueOf(OrderMap.get('customerEmail'));
                    
                    System.debug('================'+existingOrderId+'======='+String.valueOf(orderMap.get('orderId')));
                    if(!existingOrderId.contains(String.valueOf(orderMap.get('orderId')))) {
                        System.debug('================'+existingOrderId+'======='+String.valueOf(orderMap.get('orderId')));
                        ShipStation_Orders__c ord = new ShipStation_Orders__c();
                        
                        for(contact con : insCustomer){
                            System.debug('=========con.email======'+con.Email);
                            if(con.Email == emails && !conlist.contains(con)) {
                                ord.Contact__c = con.Id;
                            }
                        }
                        for(Contact con : conlist){
                            if(con.Email == emails && !insCustomer.contains(con)) {
                                ord.Contact__c = con.Id;
                            }      
                        }
                        System.debug('=========orderMap========'+orderMap);
                        ord.Processed_from_shipStation_batch__c = true;
                        ord.orderId__c                 = String.valueOf(orderMap.get('orderId'));
                        ord.orderNumber__c             = String.valueOf(orderMap.get('orderNumber'));
                        ord.orderKey__c = String.valueOf(orderMap.get('orderKey'));
                        ord.orderTotal__c   = String.valueOf(orderMap.get('orderTotal'));
                        ord.paymentMethod__c              = String.valueOf(orderMap.get('paymentMethod'));
                        ord.paymentDate__c              = String.valueOf(orderMap.get('paymentDate'));
                        ord.modifyDate__c              = String.valueOf(orderMap.get('modifyDate'));
                        ord.CreateDate__c              = String.valueOf(orderMap.get('CreateDate__c'));
                        ord.customerEmail__c              = String.valueOf(orderMap.get('customerEmail'));
                        ord.customerUsername__c = String.valueOf(orderMap.get('customerUsername'));
                        ord.CreateDate__c   = String.valueOf(orderMap.get('createDate'));
                        ord.orderStatus__c  = String.valueOf(orderMap.get('orderStatus'));
                        if(orderMap.containsKey('shipTo')) {
                            ord.Shipname__c               = String.valueOf(addressArray.get('name'));
                            ord.Ship_Street1__c                 = String.valueOf(addressArray.get('street1'));
                            ord.ShipTo_Street2__c                 = String.valueOf(addressArray.get('street2'));
                            ord.Ship_To_Street3__c                 = String.valueOf(addressArray.get('street3'));
                            ord.Ship_Company__c  = String.valueOf(addressArray.get('company'));
                            ord.Ship_To_City__c                     = String.valueOf(addressArray.get('city'));
                            ord.Ship_To_State__c                     = String.valueOf(addressArray.get('state'));
                            ord.Ship_To_Country__c                  = String.valueOf(addressArray.get('country'));
                            ord.Ship_To_Phone__c                    = String.valueOf(addressArray.get('phone'));
                            ord.Ship_To_PostalCode__c                      = String.valueOf(addressArray.get('postalCode'));
                            ord.Ship_To_Residential__c                      = Boolean.valueOf(addressArray.get('residential'));
                            if(addressArray.get('addressVerified') != null){
                                ord.Ship_To_AddressVerified__c                      = String.valueOf(addressArray.get('addressVerified'));
                            }
                        }
                        
                        if(orderMap.containsKey('billTo')) {
                            ord.name__c               = String.valueOf(addressArray.get('name'));
                            ord.name__c               = String.valueOf(addressArray.get('company'));
                            ord.street1__c                 = String.valueOf(addressbillArray.get('street1'));
                            ord.street2__c                 = String.valueOf(addressbillArray.get('street2'));
                            ord.street3__c                 = String.valueOf(addressbillArray.get('street3'));
                            ord.city__c                     = String.valueOf(addressbillArray.get('city'));
                            ord.state__c                     = String.valueOf(addressbillArray.get('state'));
                            ord.country__c                  = String.valueOf(addressbillArray.get('country'));
                            ord.phone__c                    = String.valueOf(addressbillArray.get('phone'));
                            ord.postalCode__c               = String.valueOf(addressbillArray.get('postalCode'));
                            if(addressbillArray.get('residential') != null){
                                ord.residential__c               = Boolean.valueOf(addressbillArray.get('residential'));
                            }
                            if(addressbillArray.get('addressVerified') != null){
                                ord.addressVerified__c               = String.valueOf(addressbillArray.get('addressVerified'));
                            }
                        }
                        orderList.add(ord);
                        System.debug('==========orderList========='+orderList+'==================count====='+count++);
                    }
                }
                map<String ,id> shipId = new map<String ,id>();
                ShipstationOrder.isrecursive = true;
                if(!orderList.isEmpty()) {
                    insert orderList;
                    System.debug('=======orderList========='+orderList);
                    System.debug('==========+count========'+count);
                    
                }
                
                Set<String> itemId = new Set<String> ();
                Set<String> shopOrdId = new Set<String> ();
                for(ShipStation_Orders__c Shopiorder:existingOrders){
                    shopOrdId.add(Shopiorder.id);
                }
                List<ShipStation_Order_Items__c> exestingItems = [select id,orderItemId__c from ShipStation_Order_Items__c where ShipStation_Orders__c IN : shopOrdId];
                for(ShipStation_Order_Items__c orderitem:exestingItems) {
                    itemId.add(orderitem.orderItemId__c);
                }
                Map<String, ShipStation_Orders__c> shopOrders = new Map<String, ShipStation_Orders__c>();
                for(ShipStation_Orders__c ord : orderList) {
                    shopOrders.put(ord.OrderId__c, ord);
                    shipId.put(ord.OrderId__c , ord.id);
                    System.debug('========shopOrders========='+shopOrders);
                }
                Map<String, Object> itemMap = new Map<String, Object>();
                Map<String, Object> fulfillmentMap = new Map<String, Object>();
                
                
                if(!orders.isEmpty()){
                    for(Object ordr: orders){
                        String fulfillmentStatus;
                        orderMap=(Map<String, object>)ordr;
                        String orderIdVal = String.valueOf(orderMap.get('orderId'));
                        ShipStation_Orders__c ordid = shopOrders.get(String.valueOf(orderMap.get('orderId')));
                        List<Object> fulfillments = (List<Object>)orderMap.get('fulfillments');
                        
                        List<Object> LineItems = (List<Object>)orderMap.get('items');
                        
                        for(Object obj : LineItems) {
                            itemMap = (Map <String, Object>)obj;
                            if(!itemId.contains(String.valueOf(itemMap.get('orderItemId')))) {
                                ShipStation_Order_Items__c shipItems = new ShipStation_Order_Items__c();
                                System.debug('=======ordid========'+ordid);
                                if(ordid != null && shipId.keySet().contains(orderIdVal) ) {
                                    shipItems.ShipStation_Orders__c       = shipId.get(orderIdVal) ;
                                    System.debug('==========shipId.get(orderIdVal)========'+shipId.get(orderIdVal));
                                }
                                shipItems.Processed_from_shipStation_batch__c = true;
                                shipItems.orderItemId__c = String.valueOf(itemMap.get('orderItemId'));
                                shipItems.unitPrice__c  = Double.valueOf(itemMap.get('unitPrice'));
                                shipItems.quantity__c  = Double.valueOf(itemMap.get('quantity'));
                                shipItems.sku__c  = String.valueOf(itemMap.get('sku'));
                                shipItems.name__c  = String.valueOf(itemMap.get('name')); 
                                shipItems.weight__c  = String.valueOf(itemMap.get('weight')); 
                                shipItems.upc__c  = String.valueOf(itemMap.get('upc')); 
                                shipItems.adjustment__c  = Boolean.valueOf(itemMap.get('adjustment')); 
                                shipItems.modifyDate__c  = String.valueOf(itemMap.get('modifyDate')); 
                                shipItems.createDate__c  = String.valueOf(itemMap.get('createDate')); 
                                productItems.add(shipItems);
                            }
                        }
                    }
                    if(!orderList.isEmpty()) {
                        insert productItems;
                        System.debug('=======productItems========='+productItems);
                    }
                    
                    
                }
                
            }
        }
        catch(Exception e){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'patidarsurender9@gmail.com' };
                message.optOutPolicy = 'FILTER';
            message.subject = 'Please check your field order';
            message.plainTextBody = 'Hi Surendra please Check your failed orders Causes of This:- '+ e +'\n See page Number======:'+pagenumber+'\n OrderId : ' +FaieldOrderId+ '\n Please Process again this orders by batch';
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            
        }
        
    }
    
    global void finish(Database.batchableContext bc){ 
        
    }
    
    
}