@isTest
public class CampaignMemberTrigger_Test {
   
     @isTest static void CampMember() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
       
        Campaign_Auto_SMS__mdt cm = new Campaign_Auto_SMS__mdt();
        cm.Campaign_Name__c = 'New Campaign Test';
        
        
        
         
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.Email = 'test@test.com';
        insert cnt;
        
        
        
        Campaign cmp = new Campaign();
        cmp.Name = 'test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
        
        
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',cm.From_Number__c);
        Test.stopTest();
    }
    
    @isTest static void sendingSMS1() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        
        insert cnt;
        
        
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
		
    }
}