/*
 PaymentToComNewBatch pbatch = new PaymentToComNewBatch(1);
 Database.executeBatch(pbatch);

*/
global class PaymentToComNewBatch implements  Database.Batchable<sObject>, Database.Stateful{
    Integer weekNum;
    public PaymentToComNewBatch(Integer weekNum){
        this.weekNum = weekNum;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(weekNum);
        String query = 'SELECT id,OpportunityId FROM opportunityLineItem WHERE (opportunity.week__c >=: weekNum OR opportunity.refund_week__c >=: weekNum) and Opportunity.sales_person_id__r.name!=null';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<opportunityLineItem> oliList){
        System.debug('oliList : '+oliList.size());
        List<Id> oppIdList = new List<ID>(); 
        for(opportunityLineItem oli : oliList){
            oppIdList.add(oli.OpportunityId);
        }
        List<Payment__c> payList = [select id from payment__c WHERE opportunity__r.id in : oppIdList];
        List<Comission__c> comList = [select id from comission__c where payment__c in : payList];
        
        System.debug('payList : '+payList.size());
        System.debug('comList : '+comList.size());
        delete payList;
        delete comList;
    }
    global void finish(Database.BatchableContext bc){
        PaymentToCommissionBatch bt = new PaymentToCommissionBatch(weekNum);
        Database.executeBatch(bt,1);
    }
}