@isTest
public class CurrentUserSMSCommunication_Test {
 @isTest
    public Static Void testUtility () {
        MasterViewSetting__c mssetting = new MasterViewSetting__c();
        mssetting.Name = 'System Administrator';
        mssetting.UserProfiles__c= 'testprofile';
        insert mssetting;
        Profile pro = [SELECT Id FROM Profile WHERE Name= 'Account Recovery'];
        User usr = new User();
        usr.LastName = 'Badole';
        usr.FirstName='Sourabh';
        usr.Alias = 'sbad';
        usr.Email = 'Test@gmail.com';
        usr.Username = 'testuser1234567890@gmail.com';
        usr.ProfileId = pro.id;
        usr.TimeZoneSidKey = 'GMT';
        usr.LanguageLocaleKey = 'en_US';
        usr.EmailEncodingKey = 'UTF-8';
        usr.LocaleSidKey = 'en_US';
        usr.IsActive = true;
        insert usr;
        Event smsu = new Event();
        smsu.Sent_By__c =UserInfo.getUserId();
        smsu.Description = 'test' ;
        smsu.SMS_Initiated_By__c = 'Agent';
        smsu.phone__c = '+919902189392' ;
        smsu.DurationInMinutes =5;
        smsu.ActivityDateTime = system.now();
        
        insert smsu;
        Test.startTest();
        CurrentUserSMSCommunication.getNumberspic();
        CurrentUserSMSCommunication.allUserSms(String.valueOf(Date.today().addDays(-10)), String.valueOf(Date.today()),userinfo.getUserId());
        CurrentUserSMSCommunication.getNumbers('99');
        CurrentUserSMSCommunication.SMSCommunication('+919902189392', String.valueOf(Date.today().addDays(-10)), String.valueOf(Date.today()));
        CurrentUserSMSCommunication.sortByDateUserNum(usr.Name, '+919902189392', String.valueOf(Date.today().addDays(-10)), String.valueOf(Date.today()));
        CurrentUserSMSCommunication.sortByUser(usr.Name, String.valueOf(Date.today().addDays(-10)), String.valueOf(Date.today()));
        CurrentUserSMSCommunication.loadSMSCOntacts(userinfo.getUserId());
        CurrentUserSMSCommunication.ShowAllOnload();
        Test.stopTest();
    }
}