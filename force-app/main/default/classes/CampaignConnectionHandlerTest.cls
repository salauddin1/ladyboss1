/*
* Devloper : Sourabh Badole
* Description : This Class is use as test class to cover code of CampaignConnectionHandler.
*/
@isTest
public class CampaignConnectionHandlerTest {
    static testMethod void testHttpRequest1() { 
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce1@gmail.com');
        
        insert con; 
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.StartTest(); 
        insert opp ;
        Test.StopTest();
    }    
    static testMethod void testHttpRequest2() { 
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce2@gmail.com');
        
        insert con; 
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        Opportunity opp1 = new Opportunity();
        opp1.Contact__c = con.id ;
        opp1.Name = 'Test ' ;
        opp1.StageName = 'Qualification' ;
        opp1.CloseDate = Date.today();
        
        Test.StartTest(); 
        insert opp ;
        insert opp1 ;
        Test.StopTest();
    }  
    static testMethod void testHttpRequest3() { 
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce1@gmail.com',
            shopify_customer__c = true);
        
        insert con; 
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        
        
        Test.StartTest(); 
        insert opp ;
        Test.StopTest();
    }    
    
    static testMethod void testHttpRequest4() {
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id, Payment_Date__c=system.today(),Amount=20);
        Test.startTest();
        insert opp;
        opp.Payment_Date__c = opp.Payment_Date__c.addDays(2);
        update opp;
        Test.stopTest();
    }
    @isTest
    public static void testCamp(){
        Campaign camp1 = new Campaign(Name = 'oppCampaign', type = 'Phone Team' , IsActive = True);
        insert camp1;
        Campaign camp2 = new Campaign(Name = 'oppCampaign2',type = 'Phone Team' , IsActive = True);
        insert camp2;
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Email = 'badole.salesforce@gmail.com';
        insert con ; 
        
        CampaignMember newMember  = new CampaignMember(ContactId = con.id, status='Sent', campaignid = camp1.id);
        insert newMember;
        
        CampaignMember newMember2 = new CampaignMember(ContactId = con.id, status='Sent', campaignid = camp2.id);
        insert newMember2;
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        Opportunity testOpp = new Opportunity (Name = 'Test Name',                       
                                               StageName = 'Open',
                                               Amount = 5000.00,
                                               
                                               CloseDate = System.today(),
                                               Contact__c = con.id 
                                              );
        insert testOpp;
        
    }
    
}