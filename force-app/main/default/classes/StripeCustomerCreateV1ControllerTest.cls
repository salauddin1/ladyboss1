@isTest
public  class StripeCustomerCreateV1ControllerTest {
    @IsTest
    static void methodName(){
        ContactTriggerFlag.isContactBatchRunning = true;
        Contact c=new Contact();
        c.email='ashish.sharma.devsfdc75@gmail.com';
        c.LastName='sharma';
        insert c;
        Address__c add = new Address__c();
        add.Shipping_City__c = 'new York';
        add.Primary__c = true;
        add.Contact__c = c.Id;
        insert add;
        Test.startTest();
        StripeCustomerCreateV1Controller.getConDetails(c.id);
        StripeCustomerCreateV1Controller.createCustomer(c.Id);
        Test.stopTest();
        
    }
}