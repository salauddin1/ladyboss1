global  class ContactMergeForLVCHBatch implements Database.Batchable<SObject>,Database.Stateful,Database.AllowsCallouts{
    global Set<Id> alreadyDuplicateList;
    private String sessionId;
    
    global ContactMergeForLVCHBatch(String sessionId){
        alreadyDuplicateList = new Set<Id>();
        this.sessionId = sessionId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        Set<String> accFieldList = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet();
        List<String> strList = new List<String>();
        strList.addAll(accFieldList);
        String fields = string.join(strList,',');
        String dynamicquery = 'select '+fields+' from Contact where check_for_LVCH_dup__c = true order by lastmodifieddate asc ';
        return Database.getQueryLocator(dynamicquery);
        //return Database.getQueryLocator('select id,Marketing_Cloud_Tags__c,AccountId, product__c,Potential_Buyer__c,Email,Email_2__c,Email_3__c,Phone,OtherPhone,MobilePhone,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet from Contact where lastmodifiedDate= last_n_days:10 order by lastmodifieddate desc ');
    }
    
    //global void execute(SchedulableContext sc) {
    //ContactMergeBatch b = new ContactMergeBatch();
    //database.executebatch(b,1);
    //}
    
    
    global void execute(Database.BatchableContext context, List<SObject> records) {
        //System.debug('idList  : '+alreadyDuplicateList);
        ContactTriggerFlag.isContactBatchRunning = true;
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        Set<String> accFieldList = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet();
        List<String> strList = new List<String>();
        strList.addAll(accFieldList);
        String fields = string.join(strList,',');
        String dynamicquery = '';
        List<Contact> contactToUpdateList = new List<Contact>();
        for(Sobject obj : records)  {
            Contact cntObj = (Contact)obj;
            if(!alreadyDuplicateList.contains(cntObj.id)){
                List<Id> idList = new List<Id>();
                List<Id> idCntList = new List<Id>();
                Map<id,Contact> cntIdMap = new Map<id,Contact>();
                idCntList.add(cntObj.Id);
                List<Id> duplicateIdCntList = new List<Id>();
                duplicateIdCntList.add(cntObj.Id);
                cntIdMap.put(cntObj.id, cntObj);
                
                //Find my duplicates and my account ids 
                List<Datacloud.FindDuplicatesResult> lst = Datacloud.FindDuplicatesByIds.findDuplicatesByIds(idCntList);
                Map<Id,List<Id>> mapCntDups = new Map<Id,List<Id>>();
                mapCntDups.put(cntObj.id,new List<Id>());
                for(Datacloud.FindDuplicatesResult df : lst)  {
                    for (Datacloud.DuplicateResult dupeResult : df.getDuplicateResults()) {
                        for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                            for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                //Check if matchRecord is contact
                                sObject so = matchRecord.getRecord();
                                if(Schema.Contact.getSObjectType() == so.getSobjectType())  {
                                    duplicateIdCntList.add(((Contact)matchRecord.getRecord()).id);
                                    alreadyDuplicateList.add(((Contact)matchRecord.getRecord()).id);
                                    mapCntDups.get(cntObj.id).add(((Contact)matchRecord.getRecord()).id);
                                }
                            }
                        }
                    }
                }
                Map<id,Contact> dupCntIdMap = new Map<id,Contact>();
                if(duplicateIdCntList.size() == 1)  {
                    for(SObject ct : records){
                        Contact ctOb = (Contact)ct;
                        ctOb.check_for_LVCH_dup__c = false;
                    }
                    update records;
                    //Nothing to merge
                    continue;
                }else {
                    String conlistquery = 'select '+fields+' from contact where id in :duplicateIdCntList order by lastmodifiedDate asc ';
                    
                    List<Contact> lstcnt1 = Database.query(conlistquery);
                    //List<Contact> lstcnt1 = [select id,Marketing_Cloud_Tags__c,AccountId, Product__c,Potential_Buyer__c,Email,Email_2__c,Email_3__c,Phone,OtherPhone,MobilePhone,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet,OtherCity,OtherCountry,OtherPostalCode,OtherState,OtherStreet from Contact where id in :duplicateIdCntList ];
                    dupCntIdMap.putall(lstcnt1);
                    for(Contact cnt : lstcnt1)  {
                        if(cnt.accountId != null)  {
                            idList.add(cnt.AccountID);
                            //  dupCntIdMap.put(cnt.Id,cnt);
                        }
                    }
                }
                //System.debug('idList size is : ' + idList.size());
                //System.debug('idList size is : ' + idList);
                //All code for account mergings, since we don't want to lost opp, cc number and email on acocunt
                
                Map<id,Account> accountIdMap = new Map<id,Account>();
                
                Map<Id,Account> duplicateQueryMap = new Map<Id,Account>([select id,lastmodifieddate,Last_Stripe_Modified_Date__c,Last_Email_Modified_Date__c,Last_CC_Modified_Date__c from Account WHERE id in :idList order by lastmodifieddate desc]);
                for(Id i : duplicateQueryMap.keySet())  {
                    accountIdMap.put(i,duplicateQueryMap.get(i));
                }
                
                //System.debug(duplicateQueryMap);
                //System.debug(accountIdMap);
                //System.debug(fields);
                dupCntIdMap.putall(cntIdMap);
                //List<Contact> contactToUpdateList = new List<Contact>();
      
                Set<id> setIdsdup = new Set<id>();
                for(Id masterId : mapCntDups.keySet()) {
                    setIdsdup.add(masterId);
                    for(Id ids : mapCntDups.get(masterId)){
                        setIdsdup.add(ids);
                    }
                }
                
                String getAllCon = 'select '+fields+' from contact where id IN: setIdsdup';
                Map<Id,Contact> mapCon = new Map<Id,Contact>((List<Contact>)Database.query(getAllCon));
                Map<Id,List<Id>> mapCntDups2 = new Map<Id,List<Id>>();
                for(Id masterId : mapCntDups.keySet()) {
                    List<Id> listofOtherIds = new List<Id>();
                    Contact oldestCon = mapCon.get(masterId);
                    for(Id ids : mapCntDups.get(masterId)){
                        listofOtherIds.add(ids);
                        if(mapCon.get(ids).createddate < oldestCon.createddate){
                            if(listofOtherIds.indexOf(oldestCon.id) == -1){
                                listofOtherIds.add(oldestCon.id);
                            }
                            if(listofOtherIds.indexOf(ids) != -1){
                                listofOtherIds.remove(listofOtherIds.indexOf(ids));
                            }
                            oldestCon = mapCon.get(ids);
                        }
                    }
                    mapCntDups2.put(oldestCon.id, listofOtherIds);
                }
                mapCntDups = mapCntDups2;
                set<id> setid = mapCntDups.keySet(); 
                //System.debug('--setid--'+setid);
                
                dynamicquery = 'select '+fields+' from contact where id IN: setid';
                //System.debug(dynamicquery);
                // List<Contact> conlist = Database.query(dynamicquery);
                //System.debug(conlist);
                Map<Id,Contact> forPArentMap = new Map<Id,Contact>((List<Contact>)Database.query(dynamicquery));
                
                for(Id masterId : mapCntDups.keySet()) {
                    Contact contactToUpdate = new Contact(Id=masterId);
                    
                    for(Id ids : mapCntDups.get(masterId)){
                        system.debug('IDS:-'+ids);
                        Contact ct1 = dupCntIdMap.get(ids);
                        
                        for(String  fieldName : fieldMap.keySet()){
                            Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                            if(String.valueof(fielddataType) != 'MULTIPICKLIST' && String.valueof(fielddataType) != 'BOOLEAN' && !(fieldMap.get(fieldName).getDescribe().isCalculated()) && fieldMap.get(fieldName).getDescribe().isUpdateable()){
                                if(forPArentMap.get(masterId).get(fieldName) == null && ct1.get(fieldName)  != null){
                                    contactToUpdate.put(fieldName,ct1.get(fieldName));
                                }
                            }else if(String.valueof(fielddataType) == 'MULTIPICKLIST' && String.valueof(fielddataType) != 'BOOLEAN' && fieldMap.get(fieldName).getDescribe().isUpdateable()){
                                List<String> lstforPArentMap = new List<String>();
                                Set<String> setListAllVal = new Set<String>();
                                String multipicklistVal = '';
                               
                                if(forPArentMap.get(masterId).get(fieldName) != null){
                                    lstforPArentMap.addAll(String.Valueof(forPArentMap.get(masterId).get(fieldName)).split(';'));
                                }
                                if(ct1.get(fieldName)  != null){
                                    lstforPArentMap.addAll(String.Valueof(ct1.get(fieldName)).split(';'));
                                }
                                if(contactToUpdate.get(fieldName) != null){
                                    system.debug('--multipicklistVal--'+multipicklistVal);
                                    lstforPArentMap.addAll(String.Valueof(contactToUpdate.get(fieldName)).split(';'));
                                }
                                for(String s : lstforPArentMap){
                                    System.debug(s);
                                    if(setListAllVal.add(s.trim().tolowercase())){
                                        multipicklistVal += s+';'; 
                                    }
                                }
                                contactToUpdate.put(fieldName,multipicklistVal.removeEnd(';')) ;
                            }else if(String.valueof(fielddataType) == 'BOOLEAN' && fieldMap.get(fieldName).getDescribe().isUpdateable()){
                                if(forPArentMap.get(masterId).get(fieldName) == false && ct1.get(fieldName)  == true){
                                    contactToUpdate.put(fieldName,ct1.get(fieldName));
                                }
                            }
                        }
                    }
                    contactToUpdate.put('check_for_LVCH_dup__c',false);
                    contactToUpdateList.add(contactToUpdate);
                }
                List<Api_Requests__c> apiLimit = [SELECT Limit_Exceeded__c,Name FROM Api_Requests__c Where Name='Api Limit'];
                if(apiLimit!=null && apiLimit.size()==1 && apiLimit.get(0).Limit_Exceeded__c){
                    System.enqueueJob(new updateContactsQueuable(contactToUpdateList));
                }else{
                ContactPhoneUpdateFromMergeBatch.doCall(contactToUpdateList,sessionId);
                }
                if(accountIdMap.size () > 1 )  { //have account to merge 
                    System.debug('I am trying to merge accounts');
                    Map<Id,List<Id>> mapDuplicates = new Map<Id,List<Id>>();
                    Integer counter = 0;
                    
                    //Lets take first account as master and use same logic
                    Id masterKey = idList.get(counter);
                    Id skipMe = idList.get(counter);
                    Id tempMaterKey = masterKey;
                    Boolean isFromCC = false;
                    Boolean isFromStripe = false;
                    Boolean isFromEmail = false;
                    Set<Account> matches = new Set<Account>();    
                    Datetime oldestDatetime = Datetime.newInstance(1970, 12, 1, 12, 30, 2);
                    
                    for(Id actNewId : accountIdMap.keySet())  {
                        if(actNewId == skipMe)  {
                            continue;
                        }
                        Account act = accountIdMap.get(actNewId);
                        if((accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c == null ? oldestDatetime : accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c) != (duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c)){
                            isFromStripe = true;
                        }else{
                            if(!isFromStripe && (accountIdMap.get(masterKey).Last_CC_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_CC_Modified_Date__c )!= (duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c)){
                                isFromCC = true;
                            }else{
                                if(!isFromStripe && !isFromCC && (accountIdMap.get(masterKey).Last_Email_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_Email_Modified_Date__c) != (duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c)){
                                    isFromEmail = true;
                                }
                            }
                        }     
                    }
                    
                    //System.debug('isFromCC '+isFromCC);
                    //System.debug('isFromEmail '+isFromEmail);
                    //System.debug('masterKey '+masterKey);
                    
                    for(Id actNewId : accountIdMap.keySet()) {
                        if(actNewId == skipMe)  {
                            continue;
                        }
                        Account act = accountIdMap.get(actNewId);
                        accountIdMap.put(act.id,duplicateQueryMap.get(act.id));
                        System.debug('duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c'+duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c);
                        System.debug('accountIdMap.get(masterKey).Last_CC_Modified_Date__c'+accountIdMap.get(masterKey).Last_Email_Modified_Date__c);
                        if((accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c == null ? oldestDatetime : accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c) < (duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c)){
                            tempMaterKey = act.id;
                            isFromStripe = true;
                        }else{
                            if(!isFromStripe && (accountIdMap.get(masterKey).Last_CC_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_CC_Modified_Date__c ) < (duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c)){
                                tempMaterKey = act.id;
                                isFromCC = true;
                            }else{
                                if(!isFromStripe && !isFromCC && (accountIdMap.get(masterKey).Last_Email_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_Email_Modified_Date__c) < (duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c)){
                                    tempMaterKey = act.id;
                                    isFromEmail = true;
                                }else{
                                    if(!isFromStripe && !isFromEmail && !isFromCC && accountIdMap.get(masterKey).lastmodifieddate < duplicateQueryMap.get(act.id).lastmodifieddate){
                                        tempMaterKey = act.id;
                                    }
                                }
                            }
                        }
                        masterKey = tempMaterKey;
                    }
                    
                    //System.debug('masterKey '+masterKey);                    
                    mapDuplicates.put(masterKey,new List<Id>());
                    if(idList.get(counter) != masterKey){
                        System.debug('counter is : ' + counter);
                        mapDuplicates.get(masterKey).add(idList.get(counter));
                    }
                    
                    
                    for(Id actNewId : accountIdMap.keySet()) {
                        if(actNewId == skipMe)  {
                            continue;
                        }
                        Account act = accountIdMap.get(actNewId);
                        if(act.id != masterKey){
                            mapDuplicates.get(masterKey).add(act.Id);
                        }
                    }
                    
                    for(Id masterId : mapDuplicates.keySet()) {
                        Account act = new Account(Id = masterId);
                        for(Integer i = 0; i < mapDuplicates.get(masterId).size();i++){
                            List<Id> duplicateIds = new List<Id>();
                            duplicateIds.add(mapDuplicates.get(masterId).get(i));
                            if(i+1<mapDuplicates.get(masterId).size())
                                duplicateIds.add(mapDuplicates.get(masterId).get(i+1));
                            i++;
                            //System.debug('duplicateIds '+duplicateIds);
                            DataBase.merge(act, duplicateIds, true);
                        }    
                    }
                } 
                
                
                
                //Code to merge contact here : mapCntDups
                //Map<Id,Contact> contactMap=new Map<Id,Contact>([select id,Marketing_Cloud_Tags__c,Phone,OtherPhone,MobilePhone from Contact where id =:mapCntDups.keySet()]);
                //update five9 list before merging -->> start
                Map<Id,Id> mapChildToParent = new Map<Id,Id>();  
                
                for(Id masterId : mapCntDups.keySet()) {                

                    for(Integer i = 0; i < mapCntDups.get(masterId).size();i++){
                        mapChildToParent.put(mapCntDups.get(masterId).get(i),masterId );
                    }
                }
                
                List<Five9LSP__Five9_List_Item__c > lstfl = [select id,Five9LSP__Contact__c from Five9LSP__Five9_List_Item__c  where Five9LSP__Contact__c  in :mapChildToParent.keySet() ];
                for(Five9LSP__Five9_List_Item__c fl : lstfl)  {
                    if(mapChildToParent.containsKey(fl.Five9LSP__Contact__c))  {
                        fl.Five9LSP__Contact__c = mapChildToParent.get(fl.Five9LSP__Contact__c);
                    }
                }
                update lstfl;
                
                //update five9 list before merging -->> stop
                
                for(Id masterId : mapCntDups.keySet()) {
                    Contact ct = new Contact(Id = masterId);
                    
                    for(Integer i = 0; i < mapCntDups.get(masterId).size();i++){
                        List<Id> duplicateIds = new List<Id>();
                        if(mapCntDups.get(masterId).get(i) != masterId){
                            duplicateIds.add(mapCntDups.get(masterId).get(i));
                        }
                        if(i+1<mapCntDups.get(masterId).size() && mapCntDups.get(masterId).get(i+1) != masterId)
                            duplicateIds.add(mapCntDups.get(masterId).get(i+1));
                        i++;
                        //System.debug('duplicateIds '+duplicateIds);
                       
                        if(Limits.getQueries()+ 20 > Limits.getLimitQueries())  {
                            break;
                        }
                        if(duplicateIds.size() > 0){
                            DataBase.merge(ct, duplicateIds, true);
                        }
                    } 
                    
                }    
                
                
            }
        }
        //update contactToUpdateList;
    }
    
    global void finish(Database.BatchableContext context) {
        if(!test.isRunningTest())  {
            BatchFindDuplicatesForLVCH bt = new BatchFindDuplicatesForLVCH();
            Database.executeBatch(bt);
        }
    }  
}