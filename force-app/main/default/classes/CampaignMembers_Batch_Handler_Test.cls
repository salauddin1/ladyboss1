@isTest
private class CampaignMembers_Batch_Handler_Test {
    public testmethod Static void updateCampaignMembersTest(){
        
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new  Map<String,String>();
        
        
        Lead ld1 = new Lead(Status = 'New', LastName = 'test1Name',Company = 'testing1company');
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        List<Lead> ldList = new List<Lead>{ld1,ld2};
            insert ldList;
        
        List<Campaign> cList = new List<Campaign>();
        
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1-4';
        c1.Product__c = 'Book'; 
        c1.Max_Age__c = 4;
        c1.Min_Age__c = 0;
        c1.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c1);
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Buy Days 5-7';
        c2.Product__c = 'Book'; 
        c2.Max_Age__c = 10;
        c2.Min_Age__c = 5;
        c2.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c2);
        
        Campaign c3 =new  Campaign();
        c3.Name = 'Campaign Lead Days 8-10';
        c3.Product__c = 'Book'; 
        c3.Max_Age__c = 10;
        c3.Min_Age__c = 8;
        c3.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c3);
        Campaign c4 =new  Campaign();
        c4.Name = 'Campaign Lead Days 11-30';
        c4.Product__c = 'Book'; 
        c4.Max_Age__c = 30;
        c4.Min_Age__c = 11;
        c4.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c4);        
        insert cList;
        
        Map<Id,List<Workflow_Configuration__c>> mapWorkflowConfig = new Map<Id,List<Workflow_Configuration__c>>();
        
        
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c1.Id,LeadId = ld1.Id , Created_Date__c = d1);
        insert cm1;   
        
        
        Date d2 = dt.addDays(-12);
        CampaignMember cm2 = new CampaignMember(CampaignId = c3.Id,LeadId = ld2.Id , Created_Date__c = d2);
        insert cm2;    
        
        List<CampaignMember> cmList =[SELECT Id,Name, Age_In_Hours__c ,CampaignId,Marketing_Cloud_Tag__c,Product__c,Created_Date__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Name, Age__c,LeadId,ContactId,Campaign.Marketing_Cloud_Tags__c FROM CampaignMember ];
        List<CampaignMember> campaignLst = new List<CampaignMember>();
        //System.debug('cmList size --> '+cmList.size());
        List<Campaign> campaignList = [SELECT Id, Name, Product__c, Max_Age__c, Min_Age__c,Marketing_Cloud_Tags__c  FROM Campaign ];
        
        Campaign_Products__c setting = new Campaign_Products__c();
        setting.Name = 'Book';
        insert setting;
        Map<Id,List<CampaignMember>> conLeadCampMemberMap = new  Map<Id,List<CampaignMember>>();
        
    Test.startTest();
        CampaignMembers_Batch_Handler.updateCampaignMembers(campaignList, cmList , mapWorkflowConfig, conLeadCampMemberMap ,campaignLst);
        Test.stopTest();
        }
    
}