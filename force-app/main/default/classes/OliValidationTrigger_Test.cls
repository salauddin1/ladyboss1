@IsTest
public class OliValidationTrigger_Test {
    
	static testMethod void testHttpRequest() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        Product2 pro2 = new Product2(Name='Test',Type__c='Stripe',isActive=true);
        insert pro2;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        PricebookEntry pbe2 = new PricebookEntry(Product2Id=pro2.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe2;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        OpportunityLineItem oli2 = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
		insert oli2;
        
        Test.startTest();
            OpportunityLineItem oli3 = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe2.Id,Quantity=1,UnitPrice=20);
            try{
                insert oli3;
            }
        	catch(Exception e)
			{
                Boolean expectedExceptionThrown =  e.getMessage().contains('You can not add two types of products to this opportunity.') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
            }
        Test.stopTest();
    }
}