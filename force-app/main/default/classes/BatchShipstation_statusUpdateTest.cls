@isTest
public class BatchShipstation_statusUpdateTest {
    @isTest
    public static void test1(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResGeneratorforUpdate());
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'test@gmail.com';
        con.Phone = '7067749234';
        insert con;
        
        ShipStation__c shipStation = new ShipStation__c();
        shipStation.Domain_Name__c = 'https://test.com';
        shipStation.userName__c = 'amit_439';
        shipStation.password__c = '123456';
        shipStation.IsLive__c = true;
        insert shipStation;
        
        ShipStation_Orders__c ord= new ShipStation_Orders__c();
        ord.orderId__c = '370554822';
        insert ord;
        
        ShipStation_Order_Items__c item = new ShipStation_Order_Items__c();
        item.orderItemId__c = '0000005551';
        insert item;
        
        BatchShipstation_statusUpdate bs = new BatchShipstation_statusUpdate();
        database.executeBatch(bs,1);        
    }
}