@RestResource(urlMapping='/Stripe_SubscriptionCancelService')
global class StripeSubscriptioinUpdateCancel_Service {
    @HttpPost
    global static void createCustomer() {
        string CustomerIde;
        if(RestContext.request.requestBody!=null){
            try {
                
                String str = RestContext.request.requestBody.toString();
                System.debug(str);
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                System.debug(results.get('id'));
                CustomerIde                             = String.valueOf(results.get('id')); 
                
                List<Map<String, Object>> data = new List<Map<String, Object>>();
                
                Map<String, Object>  lstCustomers = new Map<String, Object>();
                lstCustomers   = ( Map<String, Object> )results.get('data');
                Map<String, Object> aryCustomers   = new Map<String, Object>();
                aryCustomers   =   (Map<String, Object>) lstCustomers.get('object');
                System.debug('===aryCustomers   ==='+aryCustomers   );
                String periodEnd = String.valueOf(aryCustomers.get('current_period_end'));
                String periodStart = String.valueOf(aryCustomers.get('current_period_start'));
                
                Map<String, Object> requestMap =  new  Map<String, Object>();
                requestMap  = (Map<String, Object>) results.get('request');
                
                String customerId =String.valueOf(aryCustomers .get('customer')); 
                
                Map<String, Object> dataObj= new Map<String, Object>();
                if(aryCustomers.get('plan')!=null){
                    dataObj= (Map<String, Object>) aryCustomers.get('plan');
                }
                System.debug('---dataObj---'+dataObj);
                List<Stripe_Profile__c> lstStripeProf = [select id,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =:customerId ];
                Map<String, Object>  metaDataMap       =   new Map<String, Object>();
                metaDataMap = (Map<String, Object> )aryCustomers.get('metadata');
                string subId = String.valueOf(aryCustomers .get('id')); 
                List<opportunity> lstOpportunity = new List<opportunity>();
                System.debug('----subId----'+subId);
                Integer numOfPayments = Invoices.getInvoice(subId);
                
                //System.debug('String.valueOf(requestMap.get)'+String.valueOf(requestMap.get('id')));
                //lstOpportunity  = [ SELECT id ,RequestId__c,Stripe_Charge_Id__c FROM opportunity where RequestId__c =: String.valueOf(requestMap.get('id'))];
                List<OpportunityLineItem> lstOpli = [SELECT Id,Name, Subscription_Id__c,opportunityId,Quantity,Success_Failure_Message__c,Status__c,Start__c,End__c,Number_Of_Payments__c FROM OpportunityLineItem where Subscription_Id__c =:subId ];
                String oppId = '';
                if(lstOpli.size() > 0) {
                    oppId = lstOpli[0].opportunityId;
                }
                
                
                
                
                
                if(requestMap.get('id')!=null){
                    lstOpportunity  = [ SELECT id ,RequestId__c,Active__c,Success_Failure_Message__c,Status__c,Quantity__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where id=:oppId  OR (Subscription__c=: subId OR RequestId__c =: String.valueOf(requestMap.get('id')))];
                }
                String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
                
                if(lstOpli.size() >0 && Integer.valueOf(aryCustomers.get('quantity'))!=null ){
                    lstOpli[0].Quantity =  aryCustomers.get('quantity')!='' && Integer.valueOf(aryCustomers.get('quantity'))!=0 ? Integer.valueOf(aryCustomers.get('quantity')):1;
                    lstOpli[0].Success_Failure_Message__c= String.valueOf(results.get('type'));
                    Datetime dT = DateTime.newInstance(Long.valueOf(periodStart) * 1000);
                    lstOpli[0].Start__c = date.newinstance(dT.year(), dT.month(), dT.day());    
                    Datetime dT1 = DateTime.newInstance(Long.valueOf(periodEnd) * 1000);
                    lstOpli[0].End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                    
                    if(aryCustomers.get('trial_start') !=null && aryCustomers.get('trial_end') !=null){
                        String datetime2 = String.valueOf(aryCustomers.get('trial_start'));
                        datetime dT2 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                        lstOpli[0].TrialPeriodStart__c =date.newinstance(dT2.year(), dT2.month(), dT2.day());	
                        String datetime3 = String.valueOf(aryCustomers.get('trial_end'));
                        datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                        lstOpli[0].TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                    }    
                    
                    
                    if (String.valueOf(aryCustomers.get('status')) =='canceled') {
                        Long SubCancelDate = (Long)aryCustomers.get('canceled_at');
                        datetime dt3 = datetime.newInstance(SubCancelDate* Long.ValueOf('1000'));
                        lstOpli[0].Canceled_on__c = dt3.date();
                        lstOpli[0].Status__c = 'InActive';
                    }else if (String.valueOf(aryCustomers.get('status')) =='active') {
                        lstOpli[0].Status__c ='Active';
                    }else if (String.valueOf(aryCustomers.get('status')) =='trialing') {
                        lstOpli[0].Status__c ='trialing';
                    }else if (String.valueOf(aryCustomers.get('status')) =='past_due') {
                        lstOpli[0].Status__c ='Past Due';
                    }
                    
                    lstOpli[0].Number_Of_Payments__c = numOfPayments;
                    update lstOpli;
                }
                
                if(  lstOpportunity.size() >0 ) {
                    // This condition for Refund charge
                    // System.debug('===='+lstOpportunity[0].Scheduled_Payment_Date__c);
                    if (lstOpportunity[0].Scheduled_Payment_Date__c==null   ) {
                        if(lstOpportunity.size() >0){
                            lstOpportunity[0].Quantity__c= Integer.valueOf(aryCustomers.get('quantity'));
                            if(dataObj.size()>0){
                                lstOpportunity[0].Active__c= String.valueOf(dataObj.get('active'));
                                lstOpportunity[0].Name= String.valueOf(dataObj.get('nickname'));
                                String amt = String.ValueOf(dataObj.get('amount'));
                                System.debug('===='+amt);
                            }
                            lstOpportunity[0].Status__c =String.valueOf(aryCustomers .get('status')); 
                            if(results.get('type') != null)
                                lstOpportunity[0].Success_Failure_Message__c= String.valueOf(results.get('type')); 
                            update lstOpportunity;
                            System.debug('==lstOpportunity=='+lstOpportunity);
                        }
                        
                        
                    }
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripeSubscriptioinUpdateCancel_Service',
                        'createCustomer',
                        CustomerIde
                    )
                ); 
                if(Test.isRunningTest())  
                    integer intTest =1/0;      
                
                
            }catch(Exception e) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeSubscriptioinUpdateCancel_Service',
                        'createCustomer',
                        CustomerIde,
                        e
                    )
                );
            }
        }
        
    }
}