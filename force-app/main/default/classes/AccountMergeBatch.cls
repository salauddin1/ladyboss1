global class AccountMergeBatch implements Database.Batchable<SObject>,Schedulable,Database.Stateful{
    global Set<Id> alreadyDuplicateList;
    
    global AccountMergeBatch(){
        alreadyDuplicateList = new Set<Id>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('select id,LastModifiedDate,Last_Stripe_Modified_Date__c,Last_Email_Modified_Date__c,Last_CC_Modified_Date__c from Account where createddate = today order by lastmodifieddate desc ');
    }
    
    global void execute(SchedulableContext sc) {
        AccountMergeBatch b = new AccountMergeBatch();
        database.executebatch(b,1);
    }
    
    
    global void execute(Database.BatchableContext context, List<SObject> records) {
        System.debug('idList : '+alreadyDuplicateList);
        for(Sobject obj : records)  {
            Account actObj = (Account)obj;
            if(!alreadyDuplicateList.contains(actObj.id)){
                List<Id> idList = new List<Id>();
                Map<id,Account> accountIdMap = new Map<id,Account>();
                idList.add(actObj.Id);
                accountIdMap.put(actObj.id, actObj);
                
                List<Datacloud.FindDuplicatesResult> lst = Datacloud.FindDuplicatesByIds.findDuplicatesByIds(idList);
                List<Id> duplicateIdList = new List<Id>();
                System.debug(idList.size()+' '+lst.size());
            // Integer newCounter = 0;
                //for(Integer i = 0; i < lst.size() ; i++)  {
                    //Datacloud.FindDuplicatesResult df = lst.get(i);
                for(Datacloud.FindDuplicatesResult df : lst)  {
                    for (Datacloud.DuplicateResult dupeResult : df.getDuplicateResults()) {
                        for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                            for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                duplicateIdList.add(((Account)matchRecord.getRecord()).id);
                            }
                        }
                    }
                    //newCounter++;
                }

                Map<Id,Account> duplicateQueryMap = new Map<Id,Account>([select id,lastmodifieddate,Last_Stripe_Modified_Date__c,Last_Email_Modified_Date__c,Last_CC_Modified_Date__c from Account WHERE id in :duplicateIdList order by lastmodifieddate desc]);
                Map<Id,List<Id>> mapDuplicates = new Map<Id,List<Id>>();
                Integer counter = 0;
                

                for(Datacloud.FindDuplicatesResult df : lst)  {
                    System.debug('I am : ' + idList.get(counter));
                    for (Datacloud.DuplicateResult dupeResult : df.getDuplicateResults()) {
                        
                        for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
                            //Boolean flag = false;
                            Id masterKey = idList.get(counter);
                            Id tempMaterKey = masterKey;
                            Boolean isFromCC = false;
                            Boolean isFromStripe = false;
                            Boolean isFromEmail = false;
                            Set<Account> matches = new Set<Account>();    
                            
                            //Map<Id,Account> latestCheckMap = new Map<Id,Account>();
                            //accountIdMap.put(idList.get(counter),accountIdMap.get(idList.get(counter)));
                            Datetime oldestDatetime = Datetime.newInstance(1970, 12, 1, 12, 30, 2);
                            for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                Account act = (Account)matchRecord.getRecord();
                                System.debug('master key : ' + masterKey + ' : duplicate is : ' + act.id);
                                if((accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c == null ? oldestDatetime : accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c) != (duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c)){
                                    isFromStripe = true;
                                }else{
                                    if(!isFromStripe && (accountIdMap.get(masterKey).Last_CC_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_CC_Modified_Date__c )!= (duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c)){
                                        isFromCC = true;
                                    }else{
                                        if(!isFromStripe && !isFromCC && (accountIdMap.get(masterKey).Last_Email_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_Email_Modified_Date__c) != (duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c)){
                                            isFromEmail = true;
                                        }
                                    }
                                }
                            }
                            System.debug('isFromCC '+isFromCC);
                            System.debug('isFromEmail '+isFromEmail);
                            System.debug('masterKey '+masterKey);
                            for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                Account act = (Account)matchRecord.getRecord();
                                //matches.add(act);
                                //alreadyDuplicateList.add(act.Id);
                                
                                accountIdMap.put(act.id,duplicateQueryMap.get(act.id));
                                System.debug('duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c'+duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c);
                                System.debug('accountIdMap.get(masterKey).Last_CC_Modified_Date__c'+accountIdMap.get(masterKey).Last_Email_Modified_Date__c);
                                if((accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c == null ? oldestDatetime : accountIdMap.get(masterKey).Last_Stripe_Modified_Date__c) < (duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Stripe_Modified_Date__c)){
                                    tempMaterKey = act.id;
                                    isFromStripe = true;
                                }else{
                                    if(!isFromStripe && (accountIdMap.get(masterKey).Last_CC_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_CC_Modified_Date__c ) < (duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_CC_Modified_Date__c)){
                                        tempMaterKey = act.id;
                                        isFromCC = true;
                                    }else{
                                        if(!isFromStripe && !isFromCC && (accountIdMap.get(masterKey).Last_Email_Modified_Date__c==null?oldestDatetime:accountIdMap.get(masterKey).Last_Email_Modified_Date__c) < (duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c==null?oldestDatetime:duplicateQueryMap.get(act.id).Last_Email_Modified_Date__c)){
                                            tempMaterKey = act.id;
                                            isFromEmail = true;
                                        }else{
                                            if(!isFromStripe && !isFromEmail && !isFromCC && accountIdMap.get(masterKey).lastmodifieddate < duplicateQueryMap.get(act.id).lastmodifieddate){
                                                tempMaterKey = act.id;
                                            }
                                        }
                                    }
                                }
                                masterKey = tempMaterKey;
                            }

                            
                            System.debug('size : '+matches.size());
                            System.debug('   '+matches);                    
                            System.debug('masterKey '+masterKey);                    
                            mapDuplicates.put(masterKey,new List<Id>());
                            if(idList.get(counter) != masterKey){
                                System.debug('counter is : ' + counter);
                                mapDuplicates.get(masterKey).add(idList.get(counter));
                            }
                            for(Datacloud.MatchRecord matchRecord : matchResult.getMatchRecords()) {
                                Account act = (Account)matchRecord.getRecord();
                                alreadyDuplicateList.add(act.id);
                                
                                System.debug('Duplicate Record: ' + act.id);
                                if(act.id != masterKey){
                                    mapDuplicates.get(masterKey).add(act.Id);
                                }
                            }
                        System.debug('alreadyDuplicateList '+alreadyDuplicateList);
                        }
                    }
                    counter++;
                }
                System.debug(mapDuplicates);
                for(Id masterId : mapDuplicates.keySet()) {
                    Account act = new Account(Id = masterId);
                    for(Integer i = 0; i < mapDuplicates.get(masterId).size();i++){
                        List<Id> duplicateIds = new List<Id>();
                        duplicateIds.add(mapDuplicates.get(masterId).get(i));
                        if(i+1<mapDuplicates.get(masterId).size())
                            duplicateIds.add(mapDuplicates.get(masterId).get(i+1));
                        i++;
                        System.debug('duplicateIds '+duplicateIds);
                        DataBase.merge(act, duplicateIds, true);
                    }    
                }
            }
        }
        //update records;
    }
    
    global void finish(Database.BatchableContext context) {
        System.debug(alreadyDuplicateList);
    }
    
    
}