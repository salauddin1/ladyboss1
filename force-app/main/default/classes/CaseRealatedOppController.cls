public with sharing class CaseRealatedOppController {
    
    @AuraEnabled
    public static Case getCase(string Ids) {
        //set<Id> setid = new set<Id>(); 
        // List <Contact> contacts = [Select Id, Email from Contact where Id=:Ids ] ;
        Case cases = [Select Id ,ContactEmail,ContactId from Case where Id =: Ids];
        return cases;     
    }
    
    @AuraEnabled
    public static List<Contact> getContact(String emailAdd) {
        //set<Id> setid = new set<Id>(); 
        List <Contact> Contacts = [Select Id, Name,Email from Contact WHERE Email =: emailAdd ]  ;
        
        return Contacts ;     
    }
    
    @AuraEnabled
    public static List<Opportunity> getOpportunitySub(String emailAdd) {
         RecordType recType= [select id from RecordType where name='Subscription'];
        List <Contact> Contacts = [Select Id, Name,Email from Contact WHERE Email =: emailAdd ]  ;
        set<Id> conSetId = new set<Id>();
        for(Contact con : Contacts ) {
            conSetId.add(con.id);
            
        }
        //set<Id> setid = new set<Id>(); 
        List <Opportunity> Opportunitys = [Select Id, Name, Contact__c from Opportunity WHERE Contact__c IN: conSetId and recordTypeId=:recType.id ]  ;
        
        return Opportunitys ; 
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityCharge(String emailAdd) {
         RecordType recType= [select id from RecordType where name='Charge'];
        List <Contact> Contacts = [Select Id, Name,Email from Contact WHERE Email =: emailAdd ]  ;
        set<Id> conSetId = new set<Id>();
        for(Contact con : Contacts ) {
            conSetId.add(con.id);
            
        } 
        List <Opportunity> Opportunitys = [Select Id, Name, Contact__c from Opportunity WHERE Contact__c IN: conSetId and recordTypeId=:recType.id and Stripe_Charge_Id__c!=null]  ;
        
        return Opportunitys ; 
        
    }
    @AuraEnabled
    public static List<Stripe_Profile__c> getStripeProfile(String emailAdd) {
        List <Contact> Contacts = [Select Id, Name,Email from Contact WHERE Email =: emailAdd ]  ;
        set<Id> conSetId = new set<Id>();
        for(Contact con : Contacts ) {
            conSetId.add(con.id);
            
        } 
        List <Stripe_Profile__c> StripeProfiles ;
        StripeProfiles = [Select Id, Name, Customer__c,Stripe_Customer_Id__c from Stripe_Profile__c WHERE Customer__c IN: conSetId ]  ;
        
        return StripeProfiles ; 
        
    }
    
    @AuraEnabled
    public static List<Card__c> getCard(String emailAdd) {
        List <Contact> Contacts = [Select Id, Name,Email from Contact WHERE Email =: emailAdd ]  ;
        set<Id> conSetId = new set<Id>();
        for(Contact con : Contacts ) {
            conSetId.add(con.id);
            
        } 
        List <Card__c> cards = [Select Id,Name, Contact__c from Card__c WHERE Contact__c IN: conSetId ]  ;
        
        return cards; 
        
    }
}