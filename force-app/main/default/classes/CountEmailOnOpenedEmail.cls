//This class to track the email sent from contact
public class CountEmailOnOpenedEmail{ 
    public CountEmailOnOpenedEmail(){
    }
    //This will update the count 
    public static void updateCount(){
        //Get the URL parameter 
        String urlParam = ApexPages.currentPage().getParameters().get('id');
        String uniqKey= ApexPages.currentPage().getParameters().get('key');
        System.debug('param : '+urlParam);
        System.debug('uniqKey : '+uniqKey);
        if(urlParam!=null){
            //List<Opportunity> oppList = [SELECT id,pageLoadCount__c FROM Opportunity WHERE id =: urlParam];
            //Check for the contact existance
            List<Contact> oppList = [SELECT id,Opened_Mail_Count__c FROM Contact WHERE id =: urlParam ];
            System.debug('op : '+oppList);
            System.debug('oppList : '+oppList.get(0).Opened_Mail_Count__c);
            if(oppList.size()>0){
                Decimal count=0;
                //If count null then update to 1 else increment it
                if(oppList.get(0).Opened_Mail_Count__c==null){
                    count = 1;
                }else{
                    count = oppList.get(0).Opened_Mail_Count__c +1;
                    System.debug('count 18 '+count);
                }
                System.debug('count '+count);
                oppList.get(0).Opened_Mail_Count__c= count;
            }
            System.debug('coming here '+oppList);
            update oppList;
            System.debug('after update '+oppList);
            //Used the custom tracker to add count of the email opned
            List<Custom_Tracker__c>  cTrack =[select id,Email_Open_count__c,Is_Email_Opened__c from Custom_Tracker__c  where UniqueId__c =:uniqKey];
            if(cTrack.size()>0) {
                cTrack[0].Is_Email_Opened__c =  true;
                cTrack[0].Email_Opened_DateTime__c = System.Now();
                if(cTrack[0].Email_Open_count__c!=null) {
                    cTrack[0].Email_Open_count__c =cTrack[0].Email_Open_count__c+1;
                }
                else
                {
                    cTrack[0].Email_Open_count__c =1;
                }
                if(cTrack.size()>0)
                   update cTrack;
            }
            
            
        }
    }
}