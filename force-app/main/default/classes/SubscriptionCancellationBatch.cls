global class SubscriptionCancellationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable  {
    global void execute(SchedulableContext ctx){
        SubscriptionCancellationBatch schbatch = new SubscriptionCancellationBatch();
        Database.executeBatch(schbatch,1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date todayDate = Date.today() ;
        System.debug(todayDate);
        String qry = 'SELECT id,Cancellation_Date__c,Subscription_Id__c FROM OpportunityLineItem WHERE Cancellation_Date__c=: todayDate AND disable_auto_cancellation__c!=true';
        System.debug(qry);
        return Database.getQueryLocator(qry);
    }
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> records){
        String endpoint = 'https://api.stripe.com/v1/subscriptions/';
        for ( integer i = 0; i< records.size(); i++ ){
            try{
                HttpRequest http = new HttpRequest();
                http.setEndpoint(endpoint+records.get(i).Subscription_Id__c);	
                String returnSucess='';
                http.setMethod('DELETE');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                String response;
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                if(Test.isRunningTest()==false){
                	hs = con.send(http);
                }else{
                    hs.setStatusCode(200);
                    hs.setBody('{"status":"cancelled ","quantity":8}');
                }
                Integer statusCode = hs.getStatusCode();
                if (hs.getstatusCode() == 200){
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    if(String.valueOf(results.get('status')).Contains('cancelled ')) {
                        records.get(i).Success_Failure_Message__c = 'customer.subscription.deleted';
                    }else{
                        records.get(i).Success_Failure_Message__c = String.valueOf(results.get('status'));
                    }
                    records.get(i).Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));
                    returnSucess= 'Subscription cancelled in stripe';
                }else{
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                    returnSucess= String.valueOf(errorMap.get('message'));
                    if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                        records.get(i).Success_Failure_Message__c = 'customer.subscription.deleted';
                    }
                }
            }catch(Exception e){
                System.debug(e.getMessage());  
            }
        }
        try{
            update records;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}