Public class CampaignMemberBatchDelQueue implements Queueable {
    List<CampaignMember> deleteRecords;
    List<CampaignMember> insertRecords;
    List<CampaignMember> delRecords;
    Set<CampaignMember> deleteSet = new Set<CampaignMember>();
    List<CampaignMember> deleteList = new List<CampaignMember>();
    public CampaignMemberBatchDelQueue(List<CampaignMember> deleteList , List<CampaignMember> insertList  ) {
        this.deleteRecords = deleteList;
        this.insertRecords = insertList;
    }
    
    public CampaignMemberBatchDelQueue(List<CampaignMember> deleteList , List<CampaignMember> insertList , List<CampaignMember> delList ) {
        this.deleteRecords = deleteList;
        this.insertRecords = insertList;
        this.delRecords = delList;
    }
    
    public void execute(QueueableContext context) {
        if(deleteRecords!=null && deleteRecords.size()>0){
            system.debug('++++ queue delete list +++++'+ deleteRecords);
            deleteSet.addAll(deleteRecords);
            //delete deleteRecords;
            
        }
        if(delRecords!=null && delRecords.size()>0){
            system.debug('***** queue delete list ******'+ delRecords);
            deleteSet.addAll(delRecords);
           // delete delRecords;               
        }
        if(deleteSet!=null && deleteSet.size()>0){
                deleteList.addAll(deleteSet);
        }
        
        if(deleteList!=null && deleteList.size()>0){
            system.debug('***** delete list ******'+ deleteList);
            delete deleteList;
        }
        if(!Test.isRunningTest() && insertRecords!=null && insertRecords.size()>0){
            system.debug('** Queue final insert list  '+insertRecords.size()+'    '+insertRecords);
            System.enqueueJob(new CampaignMemberBatchInsQueue(insertRecords));
        }
    }
}