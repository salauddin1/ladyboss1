@isTest
public class StripeBatchHandler_Test{
    
    public static TestMethod void testGetCallout(){
        Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
        op.StripeCard_Service__c = true;
        op.StripeCardDelete_Service__c = true;
        op.StripeCustomer_Service__c = true;
        insert op;
        //system.Test.setMock(HttpCalloutMock.class, new MockTest());
        Test.startTest(); 
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        list<Stripe_Profile__c> splist = new list<Stripe_Profile__c>();
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        sp.Customer__c =co.id;
        
        splist.add(sp);
        insert splist;
        // customer ='cus_DYWAlCN3vsdH4k';
        
        Card__c c = new Card__c ();
        c.Stripe_Profile__c=sp.id;
        c.contact__c=co.id;
        c.Billing_Zip_Postal_Code__c='6863';
        c.Card_ID__c ='79203';
        c.Cvc_Check__c='pass';
        c.Billing_Zip_Postal_Code__c ='121004';
        c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Cvc_Check__c =null;
        c.Billing_Street__c = 'HR';
        c.Billing_Country__c ='US';
        c.Billing_City__c = 'Delhi';
        c.Expiry_Year__c = '2021';
        c.Billing_State_Province__c='HR'; 
        c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Last4__c = '4242';
        c.Expiry_Month__c = '11';
        c.Name_On_Card__c ='Ashish Sharma';
        c.Brand__c = 'visa';
        insert c;
        
Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Stripe_Charge_Id__c='ch_1DWQkqBwLSk1v1ohDs0Llngf';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Refunded_Date__c =date.today();
        
        insert opp;
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://api.stripe.com/v1/subscriptions?customer=cus_DYWAlCN3vsdH4k&limit=20&status=all'; 
        String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        
        //StripeBatchHandler.dummyCover();
        StripeBatchHandler.getAllCard(splist, 'ahjh');
        //StripeBatchHandler.getAllSubscriptions(splist , '');
        Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout1(){
        Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
        op.StripeCard_Service__c = true;
        op.StripeCardDelete_Service__c = true;
        op.StripeCustomer_Service__c = true;
        insert op;
       // system.Test.setMock(HttpCalloutMock.class, new MockTest());
        Test.startTest(); 
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        insert co;
        list<Stripe_Profile__c> splist = new list<Stripe_Profile__c>();
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        sp.Customer__c =co.id;
        
        splist.add(sp);
        insert splist;
        // customer ='cus_DYWAlCN3vsdH4k';
        
        Card__c c = new Card__c ();
        c.Stripe_Profile__c=sp.id;
        c.contact__c=co.id;
        c.Billing_Zip_Postal_Code__c='6863';
        c.Card_ID__c ='79203';
        c.Cvc_Check__c='pass';
        c.Billing_Zip_Postal_Code__c ='121004';
        c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Cvc_Check__c =null;
        c.Billing_Street__c = 'HR';
        c.Billing_Country__c ='US';
        c.Billing_City__c = 'Delhi';
        c.Expiry_Year__c = '2021';
        c.Billing_State_Province__c='HR'; 
        c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Last4__c = '4242';
        c.Expiry_Month__c = '11';
        c.Name_On_Card__c ='Ashish Sharma';
        c.Brand__c = 'visa';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Stripe_Charge_Id__c='ch_1DWN6LBwLSk1v1ohg8oPkbcf';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Refunded_Date__c =date.today();
        
        insert opp;
        
        
       // RestRequest req = new RestRequest(); 
        //RestResponse res = new RestResponse();
        //req.requestURI = '/services/apexrest/Stripe_CardService'; 
        //req.requestURI ='https://api.stripe.com/v1/charges/';
       // String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        //String body = '{ "object": "list", "data": [ { "id": "ch_1DWQkqBwLSk1v1ohDs0Llngf","object": "charge", "amount": 100,"amount_refunded": 0,"application": null,"application_fee": null, "balance_transaction": "txn_1DWQkqBwLSk1v1ohhwXq7qPq","captured": true,"created": 1542211092,"currency": "usd","customer": "cus_DyNVX8mwbZvu5F","description": "$1 One Time Charge", "destination": null,"dispute": null,"failure_code": null,"failure_message": null,"fraud_details": {},"invoice": null,"livemode": false,"metadata": {"Billing Address": "cvb ghch cvb US cvb","Full Name": "null vbvbn","Email": "test@dhd.ch", "Sales Person": "LadyBoss Assistant","Integration Initiated From": "Salesforce","One Time Products Charge": "YES","Salesforce Contact Link": "https://ladyboss--ashish.cs53.my.salesforce.com/0030j00000FQlyFAAT","Fulfillment Product Name": "SF-$1 Test Secondary Statement Descriptor&#124;","Street": "ghch","City": "cvb", "State": "cvb","Postal Code": "cvb", "Country": "US" }, "on_behalf_of": null,"order": null,"outcome": {"network_status": "approved_by_network","reason": null,"risk_level": "normal","risk_score": 44,"seller_message": "Payment complete.", "type": "authorized" },"paid": true,"payment_intent": null,"receipt_email": null,"receipt_number": null, "refunded": false,  "refunds": {"object": "list","data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWQkqBwLSk1v1ohDs0Llngf/refunds"      }, "review": null,      "shipping": null,   "source": {        "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4",        "object": "card",        "address_city": "cvb",  "address_country": "US",  "address_line1": "ghch", "address_line1_check": "pass",  "address_line2": null,  "address_state": "cvb", "address_zip": "cvb",  "address_zip_check": "pass", "brand": "Visa", "country": "US","customer": "cus_DyNVX8mwbZvu5F", "cvc_check": "pass", GF2QMV6K3vhvUL3",        "funding": "credit",        "last4": "4242",        "metadata": {        },        "name": "ghch",        "tokenization_method": null      },      "source_transfer": null,      "statement_descriptor": "SF-$1 Test",      "status": "succeeded",      "transfer_group": null    }  ],  "has_more": false,  "url": "/v1/charges"} ' ;                                            
       
        //req.requestBody = Blob.valueOf(body);
        //req.httpMethod = 'GET';
        //req.addHeader('Content-Type', 'application/json'); 
        //RestContext.request = req;
        
        //RestContext.response = res;
        Id rcSub = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Id rcCharge= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
         //StripeBatchHandler.dummyCover();
        StripeBatchHandler.getAllCard(splist, 'card');
        StripeBatchHandler.getAllSubscriptions(splist ,rcSub );
        //StripeBatchHandler.getAllPurchases(splist, rcCharge);
        Test.stopTest(); 
        
    }  
     public static TestMethod void testGetCallout2(){
        Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
        op.StripeCard_Service__c = true;
        op.StripeCardDelete_Service__c = true;
        op.StripeCustomer_Service__c = true;
        insert op;
        //system.Test.setMock(HttpCalloutMock.class, new MockTest());
        Test.startTest(); 
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        insert co;
        list<Stripe_Profile__c> splist = new list<Stripe_Profile__c>();
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_DyNVX8mwbZvu5F';
        sp.Customer__c =co.id;
        
        splist.add(sp);
        insert splist;
        // customer ='cus_DYWAlCN3vsdH4k';
        
        Card__c c = new Card__c ();
        c.Stripe_Profile__c=sp.id;
        c.contact__c=co.id;
        c.Billing_Zip_Postal_Code__c='6863';
        c.Card_ID__c ='79203';
       
        c.Cvc_Check__c='pass';
        c.Billing_Zip_Postal_Code__c ='121004';
        c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Cvc_Check__c =null;
        c.Billing_Street__c = 'HR';
        c.Billing_Country__c ='US';
        c.Billing_City__c = 'Delhi';
        c.Expiry_Year__c = '2021';
        c.Billing_State_Province__c='HR'; 
        c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Last4__c = '4242';
        c.Expiry_Month__c = '11';
        c.Name_On_Card__c ='Ashish Sharma';
        c.Brand__c = 'visa';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Stripe_Charge_Id__c='ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Refunded_Date__c =date.today();
        opp.Card__c =c.id;
        opp.Stripe_Profile__c=  sp.id;
        insert opp;
        
        
       // RestRequest req = new RestRequest(); 
        //RestResponse res = new RestResponse();
        //req.requestURI = '/services/apexrest/Stripe_CardService'; 
        //req.requestURI ='https://api.stripe.com/v1/charges/';
       // String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        //String body = '{ "object": "list", "data": [ { "id": "ch_1DWQkqBwLSk1v1ohDs0Llngf","object": "charge", "amount": 100,"amount_refunded": 0,"application": null,"application_fee": null, "balance_transaction": "txn_1DWQkqBwLSk1v1ohhwXq7qPq","captured": true,"created": 1542211092,"currency": "usd","customer": "cus_DyNVX8mwbZvu5F","description": "$1 One Time Charge", "destination": null,"dispute": null,"failure_code": null,"failure_message": null,"fraud_details": {},"invoice": null,"livemode": false,"metadata": {"Billing Address": "cvb ghch cvb US cvb","Full Name": "null vbvbn","Email": "test@dhd.ch", "Sales Person": "LadyBoss Assistant","Integration Initiated From": "Salesforce","One Time Products Charge": "YES","Salesforce Contact Link": "https://ladyboss--ashish.cs53.my.salesforce.com/0030j00000FQlyFAAT","Fulfillment Product Name": "SF-$1 Test Secondary Statement Descriptor&#124;","Street": "ghch","City": "cvb", "State": "cvb","Postal Code": "cvb", "Country": "US" }, "on_behalf_of": null,"order": null,"outcome": {"network_status": "approved_by_network","reason": null,"risk_level": "normal","risk_score": 44,"seller_message": "Payment complete.", "type": "authorized" },"paid": true,"payment_intent": null,"receipt_email": null,"receipt_number": null, "refunded": false,  "refunds": {"object": "list","data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWQkqBwLSk1v1ohDs0Llngf/refunds"      }, "review": null,      "shipping": null,   "source": {        "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4",        "object": "card",        "address_city": "cvb",  "address_country": "US",  "address_line1": "ghch", "address_line1_check": "pass",  "address_line2": null,  "address_state": "cvb", "address_zip": "cvb",  "address_zip_check": "pass", "brand": "Visa", "country": "US","customer": "cus_DyNVX8mwbZvu5F", "cvc_check": "pass", GF2QMV6K3vhvUL3",        "funding": "credit",        "last4": "4242",        "metadata": {        },        "name": "ghch",        "tokenization_method": null      },      "source_transfer": null,      "statement_descriptor": "SF-$1 Test",      "status": "succeeded",      "transfer_group": null    }  ],  "has_more": false,  "url": "/v1/charges"} ' ;                                            
       
        //req.requestBody = Blob.valueOf(body);
        //req.httpMethod = 'GET';
        //req.addHeader('Content-Type', 'application/json'); 
        //RestContext.request = req;
        
        //RestContext.response = res;
        Id rcSub = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Id rcCharge= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
         //StripeBatchHandler.dummyCover();
        StripeBatchHandler.getAllCard(splist, 'card');
        StripeBatchHandler.getAllSubscriptions(splist ,rcSub );
        StripeBatchHandler.getAllPurchases('cus_DyNVX8mwbZvu5F');
        Test.stopTest(); 
        
    }  
    
     public static TestMethod void testGetCallout3(){
        Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
        op.StripeCard_Service__c = true;
        op.StripeCardDelete_Service__c = true;
        op.StripeCustomer_Service__c = true;
        insert op;
        //system.Test.setMock(HttpCalloutMock.class, new MockTest());
        Test.startTest(); 
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        insert co;
        list<Stripe_Profile__c> splist = new list<Stripe_Profile__c>();
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        sp.Customer__c =co.id;
        
        splist.add(sp);
        insert splist;
        // customer ='cus_DYWAlCN3vsdH4k';
        
        Card__c c = new Card__c ();
        c.Stripe_Profile__c=sp.id;
        c.contact__c=co.id;
        c.Billing_Zip_Postal_Code__c='6863';
        c.Card_ID__c ='79203';
       
        c.Cvc_Check__c='pass';
        c.Billing_Zip_Postal_Code__c ='121004';
        c.Card_ID__c = 'card_1DWN3qBwLSk1v1oh8j1v8Cuu';
         c.Stripe_Card_Id__c= 'card_1DWN3qBwLSk1v1oh8j1v8Cuu';
        
        c.Cvc_Check__c =null;
        c.Billing_Street__c = 'HR';
        c.Billing_Country__c ='US';
        c.Billing_City__c = 'Delhi';
        c.Expiry_Year__c = '2021';
        c.Billing_State_Province__c='HR'; 
        c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Last4__c = '4242';
        c.Expiry_Month__c = '11';
        c.Name_On_Card__c ='Ashish Sharma';
        c.CustomerID__c = co.id;
        c.Brand__c = 'visa';
        insert c;
        
        StripeBatchHandler.getAllCard(splist, 'card');
        Test.stopTest(); 
        
    }  
    public static TestMethod void testGetCallout4(){
        Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
        op.StripeCard_Service__c = true;
        op.StripeCardDelete_Service__c = true;
        op.StripeCustomer_Service__c = true;
        insert op;
        //system.Test.setMock(HttpCalloutMock.class, new MockTest());
        Test.startTest(); 
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        insert co;
        list<Stripe_Profile__c> splist = new list<Stripe_Profile__c>();
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_DyJg8dVaDdKrBp';
        sp.Customer__c =co.id;
        
        splist.add(sp);
        insert splist;
        // customer ='cus_DYWAlCN3vsdH4k';
        
        Card__c c = new Card__c ();
        c.Stripe_Profile__c=sp.id;
        c.contact__c=co.id;
        c.Billing_Zip_Postal_Code__c='6863';
        c.Card_ID__c ='79203';
       
        c.Cvc_Check__c='pass';
        c.Billing_Zip_Postal_Code__c ='121004';
        c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Cvc_Check__c =null;
        c.Billing_Street__c = 'HR';
        c.Billing_Country__c ='US';
        c.Billing_City__c = 'Delhi';
        c.Expiry_Year__c = '2021';
        c.Billing_State_Province__c='HR'; 
        c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
        c.Last4__c = '4242';
        c.Expiry_Month__c = '11';
        c.Name_On_Card__c ='Ashish Sharma';
        c.Brand__c = 'visa';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Stripe_Charge_Id__c='ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Refunded_Date__c =date.today();
        opp.Card__c =c.id;
        opp.Stripe_Profile__c=  sp.id;
        insert opp;
        
        
       // RestRequest req = new RestRequest(); 
        //RestResponse res = new RestResponse();
        //req.requestURI = '/services/apexrest/Stripe_CardService'; 
        //req.requestURI ='https://api.stripe.com/v1/charges/';
       // String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        //String body = '{ "object": "list", "data": [ { "id": "ch_1DWQkqBwLSk1v1ohDs0Llngf","object": "charge", "amount": 100,"amount_refunded": 0,"application": null,"application_fee": null, "balance_transaction": "txn_1DWQkqBwLSk1v1ohhwXq7qPq","captured": true,"created": 1542211092,"currency": "usd","customer": "cus_DyNVX8mwbZvu5F","description": "$1 One Time Charge", "destination": null,"dispute": null,"failure_code": null,"failure_message": null,"fraud_details": {},"invoice": null,"livemode": false,"metadata": {"Billing Address": "cvb ghch cvb US cvb","Full Name": "null vbvbn","Email": "test@dhd.ch", "Sales Person": "LadyBoss Assistant","Integration Initiated From": "Salesforce","One Time Products Charge": "YES","Salesforce Contact Link": "https://ladyboss--ashish.cs53.my.salesforce.com/0030j00000FQlyFAAT","Fulfillment Product Name": "SF-$1 Test Secondary Statement Descriptor&#124;","Street": "ghch","City": "cvb", "State": "cvb","Postal Code": "cvb", "Country": "US" }, "on_behalf_of": null,"order": null,"outcome": {"network_status": "approved_by_network","reason": null,"risk_level": "normal","risk_score": 44,"seller_message": "Payment complete.", "type": "authorized" },"paid": true,"payment_intent": null,"receipt_email": null,"receipt_number": null, "refunded": false,  "refunds": {"object": "list","data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWQkqBwLSk1v1ohDs0Llngf/refunds"      }, "review": null,      "shipping": null,   "source": {        "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4",        "object": "card",        "address_city": "cvb",  "address_country": "US",  "address_line1": "ghch", "address_line1_check": "pass",  "address_line2": null,  "address_state": "cvb", "address_zip": "cvb",  "address_zip_check": "pass", "brand": "Visa", "country": "US","customer": "cus_DyNVX8mwbZvu5F", "cvc_check": "pass", GF2QMV6K3vhvUL3",        "funding": "credit",        "last4": "4242",        "metadata": {        },        "name": "ghch",        "tokenization_method": null      },      "source_transfer": null,      "statement_descriptor": "SF-$1 Test",      "status": "succeeded",      "transfer_group": null    }  ],  "has_more": false,  "url": "/v1/charges"} ' ;                                            
       
        //req.requestBody = Blob.valueOf(body);
        //req.httpMethod = 'GET';
        //req.addHeader('Content-Type', 'application/json'); 
        //RestContext.request = req;
        
        //RestContext.response = res;
        Id rcSub = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Id rcCharge= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
         //StripeBatchHandler.dummyCover();
        StripeBatchHandler.getAllCard(splist, 'card');
        StripeBatchHandler.getAllSubscriptions(splist ,rcSub );
        StripeBatchHandler.getAllPurchases('cus_DyNVX8mwbZvu5F');
        Test.stopTest(); 
        
    }  
}