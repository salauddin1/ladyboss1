public class AmazonOrderHelper {
    public class AmazonOrder{
        public String AmazonOrderId;
        public String PurchaseDate;  
        public String OrderStatus; 
        public ShippingAddress ShippingAddress; 
        public String OrderTotal;  
        public String PaymentMethod; 
        public String PaymentMethodDetails; 
        public String MarketplaceId; 
        public String BuyerEmail; 
        public String BuyerName;  
        public String PurchaseOrderNumber; 
    }
    public class ShippingAddress{
        public String Name;
        public String AddressLine1;
        public String City;
        public String StateOrRegion;
        public String PostalCode;
        public String CountryCode;
        public String AddressType;
    }
    Public static void insertOrders(String XmlString){
        String namsSpace = 'https://mws.amazonservices.com/Orders/2013-09-01';
        DOM.Document doc=new DOM.Document();
        doc.load(XMLString);
        DOM.XmlNode rootNode=doc.getRootElement();
        String NextToken='';
        if(rootNode!=null&&rootNode.getChildElement('ListOrdersResult', namsSpace)!=null&&rootNode.getChildElement('ListOrdersResult', namsSpace).getChildElement('NextToken', namsSpace)!=null){
            NextToken = rootNode.getChildElement('ListOrdersResult', namsSpace).getChildElement('NextToken', namsSpace).getText();
        }else if(rootNode!=null&&rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace)!=null&&rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace).getChildElement('NextToken', namsSpace)!=null){
            NextToken = rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace).getChildElement('NextToken', namsSpace).getText();
        }
        List<AmazonOrder__c> orderData = [SELECT LastUpdatedAfter__c,NextToken__c FROM AmazonOrder__c where Name='Amazon Order Keys' LIMIT 1];
        if(NextToken==null || NextToken==''){
            orderData.get(0).NextToken__c=null;
            orderData.get(0).LastUpdatedAfter__c = DateTime.now();
        }else{
            orderData.get(0).NextToken__c = NextToken;
        }
        update orderData;   
        DOM.XmlNode ordersNode;
        if(rootNode.getChildElement('ListOrdersResult', namsSpace)!=null && rootNode.getChildElement('ListOrdersResult', namsSpace).getChildElement('Orders', namsSpace)!=null){
            ordersNode = rootNode.getChildElement('ListOrdersResult', namsSpace).getChildElement('Orders', namsSpace);
        }else if(rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace)!=null && rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace).getChildElement('Orders', namsSpace)!=null){
            ordersNode = rootNode.getChildElement('ListOrdersByNextTokenResult', namsSpace).getChildElement('Orders', namsSpace);
        }else{
            System.debug('No Order available');
            return;
        }
        List<DOM.XmlNode> orderList;
        if(ordersNode.getChildElements().size()>0){
            orderList = ordersNode.getChildElements();
        }else{
            return;
        }
        List<AmazonOrderHelper.AmazonOrder> orderList_1 = new List<AmazonOrderHelper.AmazonOrder>();
        for(DOM.XmlNode dm : orderList){ 
            AmazonOrderHelper.AmazonOrder odr = new AmazonOrderHelper.AmazonOrder();
            if(dm.getChildElement('BuyerEmail', namsSpace)!=null){
                odr.BuyerEmail = dm.getChildElement('BuyerEmail', namsSpace).getText();
            }
            if(dm.getChildElement('BuyerName', namsSpace)!=null){
                odr.BuyerName = dm.getChildElement('BuyerName', namsSpace).getText();
            }
            
            AmazonOrderHelper.ShippingAddress ship = new AmazonOrderHelper.ShippingAddress();
            if(dm.getChildElement('ShippingAddress', namsSpace)!=null){
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('Name', namsSpace)!=null){
                    ship.Name = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('Name', namsSpace).getText();
                }
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('AddressLine1', namsSpace)!=null){
                    ship.AddressLine1 = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('AddressLine1', namsSpace).getText();
                }
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('City', namsSpace)!=null){
                    ship.City = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('City', namsSpace).getText();
                }
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('StateOrRegion', namsSpace)!=null){
                    ship.StateOrRegion = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('StateOrRegion', namsSpace).getText();
                }
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('PostalCode', namsSpace)!=null){
                    ship.PostalCode = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('PostalCode', namsSpace).getText();
                }
                if(dm.getChildElement('ShippingAddress', namsSpace).getChildElement('CountryCode', namsSpace)!=null){
                    ship.CountryCode = dm.getChildElement('ShippingAddress', namsSpace).getChildElement('CountryCode', namsSpace).getText();
                }
            }
            odr.ShippingAddress = ship;
            orderList_1.add(odr);
        }
        System.debug(orderList_1);
        List<Contact> contactList = new List<Contact>();
        Map<String,Contact> emailContMapNew =new Map<String,Contact>();
        Map<String,Contact> emailContMap = new Map<String,Contact>();
        List<String> emailList = new List<String>();
        
        Pricebook2 pbook; 
        if(!Test.isRunningTest()){
            pbook = [select id, name from Pricebook2 where isStandard = true ];
        }
        
        for(AmazonOrderHelper.AmazonOrder order : orderList_1){
            Integer i=1;
            if(order.BuyerEmail!=null){
                emailList.add(order.BuyerEmail.toLowerCase());
            }
            Contact ct = new Contact();
            ct.Amazon_Customer__c = true;
            ct.LastName = 'not available';
            if(order.BuyerName!=null){
                if(order.BuyerName.containsWhitespace()){
                    ct.FirstName = order.BuyerName.substring(0, order.BuyerName.indexOf(' '));
                    ct.LastName = order.BuyerName.substring(order.BuyerName.indexOf(' ')+1,order.BuyerName.length());
                }else{
                    ct.LastName = order.BuyerName.toLowerCase();
                } 
            }
            
            if(ct.Email==null && order.BuyerEmail != null){
                ct.Email = order.BuyerEmail.toLowerCase();
            }else{
                ct.email = 'notavail@gmail.com';
            }
             if(order.ShippingAddress.AddressLine1!=null){
                    ct.MailingStreet = order.ShippingAddress.AddressLine1;
                }
                if(order.ShippingAddress.City!=null){
                    ct.MailingCity = order.ShippingAddress.City;
                }
               if(order.ShippingAddress.StateOrRegion!=null){
                    ct.MailingState = order.ShippingAddress.StateOrRegion;
                }
                if(order.ShippingAddress.PostalCode!=null){
                    ct.MailingPostalCode = order.ShippingAddress.PostalCode;
                }
                if(order.ShippingAddress.CountryCode!=null){
                    ct.MailingCountry = order.ShippingAddress.CountryCode;
                }
            if(ct.email!='notavail@gmail.com'){
              emailContMapNew.put(ct.Email, ct);
            }else{
              emailContMapNew.put(String.valueOf(i), ct);
                i++;
            }
            //contactList.add(ct);
        } 
        contactList = emailContMapNew.values();
        Map<String,Contact> emailContactMap = new Map<String,Contact>();
        for(Contact ct : [SELECT id,Email FROM Contact WHERE Email in : emailList]){
            emailContactMap.put(ct.Email.toLowerCase(),ct);
        }
        
        for(Contact con : contactList){
            if(emailContactMap.get(con.Email.toLowerCase())!= null){
                con.id = emailContactMap.get(con.Email.toLowerCase()).id;
            }
        }
        upsert contactList;
        System.debug(contactList.size());
    }
}