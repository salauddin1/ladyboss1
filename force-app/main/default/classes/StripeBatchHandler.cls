public Class StripeBatchHandler{
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    private static final String SERVICE_URL1 = 'https://api.stripe.com/v1/charges/';
    private static final String SERVICE_URL2 = 'https://api.stripe.com/v1/subscriptions/';
    
    
    //To get all the cards of the customer
    public static List<Card__c> getAllCard(List<Stripe_Profile__c> stripeProfList , String obj) {
        HttpRequest http = new HttpRequest();
        //check if profile exists or not 
        // List<Stripe_Profile__c> stripeProfList = [select id ,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
        String customerId = stripeProfList[0].Stripe_Customer_Id__c;
        Map<string,string> conStripeMap = new Map<string,string> ();
        Map<string,string> conStripeMap1 = new Map<string,string> ();
        for(Stripe_Profile__c con : stripeProfList ){
            conStripeMap.put(con.Stripe_Customer_Id__c,con.id );
            conStripeMap1.put(con.Stripe_Customer_Id__c,con.Customer__c);
        }
        
        
        
        http.setEndpoint(SERVICE_URL+customerId+'/sources?object='+obj);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                response = hs.getBody();
                statusCode = hs.getStatusCode();
            } catch (CalloutException e) {
                return null;
            }
        }
        system.debug('#### '+ hs);
        
        system.debug('response-- '+response);
        system.debug('statusCode-- '+statusCode);
        if (Test.isRunningTest()) {
            response = '{ "object": "list", "data": [ { "id": "card_1DWN3qBwLSk1v1oh8j1v8Cuu", "object": "card", "address_city": null, "address_country": null, "address_line1": null, "address_line1_check": null, "address_line2": null, "address_state": null, "address_zip": null, "address_zip_check": null, "brand": "Visa", "country": "US", "customer": "cus_DyJg8dVaDdKrBp", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 4, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": {}, "name": null, "tokenization_method": null } ], "has_more": false, "url": "/v1/customers/cus_DyJg8dVaDdKrBp/sources" }';
        }
        if(statusCode == 200 || Test.isRunningTest()){
            System.debug(response);
            try {
                //StripeCard o = (StripeCard.parse(response));
                //System.debug('---------StripeCard-o-----------'+o);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                List<Map<String, Object>> data = new List<Map<String, Object>>();
                for (Object instance : (List<Object>)results.get('data')){
                    data.add((Map<String, Object>)instance);
                }
                
                List<Object> lstCustomers = (List<Object>)results.get('data');
                Object[] aryCustomers = (Object[]) results.get('Customers');
                List<Card__c> cardList =  new List<Card__c>();
                String customerIdExst = '';
                String stripeId = '';
                List<card__c> crdListUpdate = new List<card__c>();
                Boolean isUpdate =true;
                for (Object customer : lstCustomers) {
                    isUpdate =true;
                    // now get attributes for this card.
                    Map<String, Object> customerAttributes = (Map<String, Object>)customer;
                    // now loop through our card attributes.
                    
                    customerId =  String.Valueof(customerAttributes.get('customer'));
                    List<card__c> lstUpdateCard = [select id,Stripe_Profile__c,CustomerID__c from card__c where (CustomerID__c =: customerId and CustomerID__c !=null) OR Stripe_Card_Id__c=: String.Valueof(customerAttributes.get('id'))];
                    if (lstUpdateCard.size() >0) {
                        /*for(card__c cd :lstUpdateCard  ){
                            //if(lstUpdateCard != null ) {
                            isUpdate =false;
                            cd.Stripe_Profile__c = conStripeMap.get(String.Valueof(customerAttributes.get('customer')));
                            crdListUpdate.add(cd);
                            //}
                        }*/
                    }
                    else{
                        card__c crd = new card__c();
                        crd.Stripe_Profile__c = conStripeMap.get(String.Valueof(customerAttributes.get('customer')));
                        stripeId = conStripeMap.get(String.Valueof(customerAttributes.get('customer')));
                        crd.contact__c = conStripeMap1.get(String.Valueof(customerAttributes.get('customer')));
                        crd.CustomerID__c = String.Valueof(customerAttributes.get('customer'));
                        
                        
                        
                        
                        crd.Billing_Zip_Postal_Code__c = String.Valueof(customerAttributes.get('address_zip'));
                        
                        crd.Card_ID__c = String.Valueof(customerAttributes.get('id'));
                        
                        crd.Cvc_Check__c = String.Valueof(customerAttributes.get('cvc_check'));
                        
                        crd.Billing_Street__c = String.Valueof(customerAttributes.get('address_state'));
                        
                        crd.Billing_Country__c = String.Valueof(customerAttributes.get('address_country'));
                        
                        crd.Billing_City__c = String.Valueof(customerAttributes.get('address_city'));
                        
                        crd.Expiry_Year__c = String.Valueof(customerAttributes.get('exp_year'));
                        
                        crd.Billing_State_Province__c= String.Valueof(customerAttributes.get('address_state'));
                        
                        crd.Stripe_Card_Id__c = String.Valueof(customerAttributes.get('id'));
                        
                        crd.Last4__c = String.Valueof(customerAttributes.get('last4'));
                        
                        
                        if((Integer.Valueof(customerAttributes.get('exp_month'))==11 ) || Integer.Valueof(customerAttributes.get('exp_month'))==12){
                            crd.Expiry_Month__c = String.Valueof(customerAttributes.get('exp_month'));
                        }else
                        {
                            crd.Expiry_Month__c = '0'+String.Valueof(customerAttributes.get('exp_month'));
                            
                        }
                        
                        crd.Name_On_Card__c = String.Valueof(customerAttributes.get('name'));
                        
                        crd.Brand__c= String.Valueof(customerAttributes.get('brand'));
                        
                        
                        if((String.Valueof(customerAttributes.get('brand')).contains('Visa')) || (String.Valueof(customerAttributes.get('brand')).contains('MasterCard')) ||(String.Valueof(customerAttributes.get('brand')).contains('Discover')) || (String.Valueof(customerAttributes.get('brand')).contains('JCB'))){
                            crd.Credit_Card_Number__c = '000000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        else if((String.Valueof(customerAttributes.get('brand')).contains('American Express'))){
                            crd.Credit_Card_Number__c = '00000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        else if((String.Valueof(customerAttributes.get('brand')).contains('Diners Club'))){
                            crd.Credit_Card_Number__c = '0000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        
                        
                        
                        crd.isBatchCard__c=true;
                        crd.Error_Message__c = '';
                        
                        if(isUpdate )
                            cardList.add(crd);
                    }
                    
                    
                    
                    
                }
                
                
                stripeProfList[0].Card_Processed__c=true;
                //update stripeProfList;
                if(crdListUpdate .size() > 0) {
                    //lstUpdateCard[0].Stripe_Profile__c= stripeId ;
                   /* Set<Card__c> myset = new Set<Card__c>();
                    List<Card__c> result = new List<Card__c>();
                    myset.addAll(crdListUpdate );
                    result.addAll(myset);
                    update result;*/
                    return null;
                }else{
                    return cardList;
                }
                
                
                // return null;
            } catch (System.JSONException e) {
                System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
                return null;
            }
            
        }
        return null;
    }
    
    //To get all the purchases (charges) of the customer
    public static string getAllPurchases( string custId) {
        HttpRequest http = new HttpRequest();
        
        //List<Stripe_Profile__c> stripeProfList = [select id ,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
        String customerId = custId;
        
        http.setEndpoint('https://api.stripe.com/v1/charges?customer='+customerId+'&limit=50' );
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                response = hs.getBody();
                system.debug('response-- '+response);
                statusCode = hs.getStatusCode();
                return response ;
            } catch (CalloutException e) {
                return null;
            }
        } 
        //}
        
        // system.debug('#### '+ hs.getBody());
        system.debug('#### '+ hs);
        if (Test.isRunningTest()) {
            // response ='{ "id": "ch_1DWN6LBwLSk1v1ohg8oPkbcf", "object": "charge", "amount": 27695, "amount_refunded": 0,  "created": 1542197049, "currency": "usd", "customer": "cus_DyJg8dVaDdKrBp", "description": null,  "fraud_details": {}, "invoice": "in_1DWN6LBwLSk1v1ohbovw2E31", "livemode": false, "metadata": {}, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "type": "authorized" }, "paid": true,  "refunds": { "object": "list", "data": [], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWN6LBwLSk1v1ohg8oPkbcf/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWN3qBwLSk1v1oh8j1v8Cuu", "object": "card",  "brand": "Visa", "country": "US", "customer": "cus_DyJg8dVaDdKrBp", "cvc_check": null, "dynamic_last4": null, "exp_month": 4, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": {}, "name": null, "tokenization_method": null }, "source_transfer": null, "statement_descriptor": null, "status": "succeeded", "transfer_group": null }';
            response  = '{ "object": "list", "data": [ { "id": "ch_1DWkgPBwLSk1v1oh9LMBcL3p", "object": "charge", "amount": 23700, "amount_refunded": 0, "application": null, "application_fee": null, "balance_transaction": "txn_1DWkgQBwLSk1v1ohDIAyuUQH", "captured": true, "created": 1542287697, "currency": "usd", "customer": "cus_DyNVX8mwbZvu5F", "description": null, "destination": null, "dispute": null, "failure_code": null, "failure_message": null, "fraud_details": { }, "invoice": "in_1DWkgPBwLSk1v1ohyhZQe4h7", "livemode": false, "metadata": { }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "risk_level": "normal", "risk_score": 14, "seller_message": "Payment complete.", "type": "authorized" }, "paid": true, "payment_intent": null, "receipt_email": null, "receipt_number": null, "refunded": false, "refunds": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWkgPBwLSk1v1oh9LMBcL3p/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4", "object": "card", "address_city": "cvb", "address_country": "US", "address_line1": "ghch", "address_line1_check": "pass", "address_line2": null, "address_state": "cvb", "address_zip": "cvb", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_DyNVX8mwbZvu5F", "cvc_check": null, "dynamic_last4": null, "exp_month": 1, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": { }, "name": "ghch", "tokenization_method": null }, "source_transfer": null, "statement_descriptor": null, "status": "succeeded", "transfer_group": null }, { "id": "ch_1DWQkqBwLSk1v1ohDs0Llngf", "object": "charge", "amount": 100, "amount_refunded": 0, "application": null, "application_fee": null, "balance_transaction": "txn_1DWQkqBwLSk1v1ohhwXq7qPq", "captured": true, "created": 1542211092, "currency": "usd", "customer": "cus_DyNVX8mwbZvu5F", "description": "$1 One Time Charge", "destination": null, "dispute": null, "failure_code": null, "failure_message": null, "fraud_details": { }, "invoice": null, "livemode": false, "metadata": { "Billing Address": "cvb ghch cvb US cvb", "Full Name": "null vbvbn", "Email": "test@dhd.ch", "Sales Person": "LadyBoss Assistant", "Integration Initiated From": "Salesforce", "One Time Products Charge": "YES", "Salesforce Contact Link": "https://ladyboss--ashish.cs53.my.salesforce.com/0030j00000FQlyFAAT", "Fulfillment Product Name": "SF-$1 Test Secondary Statement Descriptor|", "Street": "ghch", "City": "cvb", "State": "cvb", "Postal Code": "cvb", "Country": "US" }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "risk_level": "normal", "risk_score": 44, "seller_message": "Payment complete.", "type": "authorized" }, "paid": true, "payment_intent": null, "receipt_email": null, "receipt_number": null, "refunded": false, "refunds": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWQkqBwLSk1v1ohDs0Llngf/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4", "object": "card", "address_city": "cvb", "address_country": "US", "address_line1": "ghch", "address_line1_check": "pass", "address_line2": null, "address_state": "cvb", "address_zip": "cvb", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_DyNVX8mwbZvu5F", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 1, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": { }, "name": "ghch", "tokenization_method": null }, "source_transfer": null, "statement_descriptor": "SF-$1 Test", "status": "succeeded", "transfer_group": null } ], "has_more": false, "url": "/v1/charges" }';
             return response  ;
        }
       return null;
        
    }
    
    
    //To get all the subscriptions of the customer
    public static string getAllSubscriptions(List<Stripe_Profile__c > stripeProfList,String recTypeID  ) {
        String jsonString;
        HttpRequest http = new HttpRequest();
        String customerId = stripeProfList[0].Stripe_Customer_Id__c;
        http.setEndpoint('https://api.stripe.com/v1/subscriptions?customer='+customerId+'&limit=20&status=all' );
        
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                system.debug('#### ============================================================================='+ hs.getBody());
                response = hs.getBody();
                system.debug('response-- ---------------------------'+response);
                statusCode = hs.getStatusCode();
            } catch (CalloutException e) {
                return null;
            }
        } 
        
        // system.debug('#### ============================================================================='+ hs.getBody());
        
        if (Test.isRunningTest()) {
            response = '{ "object": "list", "data": [ { "id": "sub_DyJjwBxrWQKsZf", "object": "subscription",    "canceled_at": null, "created": 1542197049, "current_period_end": 1544789049, "current_period_start": 1542197049, "customer": "cus_DyJg8dVaDdKrBp",  "items": { "object": "list", "data": [ { "id": "si_DyJjlgRvmoShRc", "object": "subscription_item", "created": 1542197050, "metadata": {}, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan", "active": true, "aggregate_usage": null, "amount": 23700, "billing_scheme": "per_unit", "created": 1531344662, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "Transformation System-$237", "product": "prod_DDGQVEaH03NJ8p", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_DyJjwBxrWQKsZf" }, { "id": "si_DyJjKVqqzMPoja", "object": "subscription_item", "created": 1542197050, "metadata": {}, "plan": { "id": "plan_Cfm5v2MQnwKoEn", "object": "plan", "active": true,  "amount": 3995,  "created": 1523620686, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "LadyBoss BURN 1 Bottle Subscription-PT", "product": "prod_Cfm5LjVF043yUB", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_DyJjwBxrWQKsZf" } ], "has_more": false, "total_count": 2, "url": "/v1/subscription_items?subscription=sub_DyJjwBxrWQKsZf" }, "livemode": false, "metadata": {}, "plan": null, "quantity": null, "start": 1542197049, "status": "active",  "trial_start": null } ], "has_more": true, "url": "/v1/subscriptions" }';
        }
        
        if(statusCode==200 || Test.isRunningTest()){
            try {
                return response ;
            } catch (System.JSONException e) {
                System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
                return null;
            }
        }
        return null;
    }   
    
}