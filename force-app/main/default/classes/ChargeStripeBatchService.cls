//batch class to insert all the charges from stripe to salesforce
global class ChargeStripeBatchService implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
   
    global database.querylocator start(database.batchablecontext bc){
        //string str = 'a1x210000006hAy';
         string query ='';
        if(!test.isRunningTest()) {
           query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where  Charge_Processed__c=false and Stripe_Customer_Id__c!=null limit 45000';
        
        }else
        {
           query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where  Charge_Processed__c=false and Stripe_Customer_Id__c!=null limit 1';
        
        
        }
        //string query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where id=:str and Charge_Processed__c=false limit 1';
        
        return database.getquerylocator( query );   
    }
    global void execute(database.BatchableContext bc,list<Stripe_Profile__c> stripeProfileList){
        stripeProfileList[0].Charge_Processed__c = true;
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        String jsonString = StripeBatchHandler.getAllPurchases(stripeProfileList[0].Stripe_Customer_Id__c  );
          List<Stripe_Product_Mapping__c> mcs = Stripe_Product_Mapping__c.getall().values();
                        Map<string,string> productMap = new Map<string,string>();
                        Set<String> prodNameSet  = new Set<String>();
                        for(Stripe_Product_Mapping__c spm: mcs){
                            if(spm.Stripe_Nick_Name__c!=null)
                                productMap.put(spm.Stripe_Nick_Name__c,spm.Salesforce_Product__c);

                         }

                           Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
                            List<PricebookEntry> pbe1 = [SELECT Id, Name, Pricebook2Id, Product2Id,Product2.Name FROM PricebookEntry  where Pricebook2ID=:standardPb.id and isActive=true];
                             Map<string,string> priceBookMap = new Map<string,string>();
                            for(PricebookEntry pe : pbe1) {
                            priceBookMap.put(pe.ID,pe.Name);
                            }   
       System.debug('lkkkkk'+stripeProfileList[0].Customer__c);
         List<Opportunity> oppList = [select id,Card__c,Stripe_Charge_Id__c,Subscription__c,Contact__c, (SELECT Id, Subscription_Id__c,unitPrice,refund__c,OpportunityId FROM OpportunityLineItems)  from opportunity where  Contact__c =: stripeProfileList[0].Customer__c];
            
            List<OpportunityLineItem> oliUpdateList =  new List<OpportunityLineItem>();
            List<Opportunity> opUpdateList =  new List<Opportunity>();
             List<Opportunity> opInsertList =  new List<Opportunity>();
            
            
            Map<string,string> oppExistingMap = new Map<string,string> ();
            Map<string,Opportunity> oppSubChargeMap = new Map<string,Opportunity> ();
            Map<string,Opportunity> oppExMap = new Map<string,Opportunity> ();
            Map<string,string> oliMap = new Map<string,string>();
            Map<string,List<OpportunityLineItem>> oppListMap = new Map<string,List<OpportunityLineItem>> ();
            for(Opportunity o : oppList){
                 system.debug('-----1--'+o.OpportunityLineItems.size ());
                if (o.OpportunityLineItems.size () > 0) {
                    for(OpportunityLineItem oli :o.OpportunityLineItems){
                        //oliMap.put(oli.Subscription_Id__c,oli.Subscription_Id__c);
                        if(oli.OpportunityId!=null) {
                
                           
                            if(!oppListMap.containskey(oli.OpportunityId)){
                             
                                oppListMap.put(oli.OpportunityId, new List<OpportunityLineItem> { oli});
                            }else{
                                
                                List<OpportunityLineItem> opli = oppListMap.get(oli.OpportunityId);
                                opli.add(oli);
                                oppListMap.put(oli.OpportunityId, opli);
                            }
                        }
                    }
                }
                
                oppExistingMap.put(o.Subscription__c,o.Card__c);
                oppSubChargeMap.put(o.Stripe_Charge_Id__c,o);
                oppExMap.put(o.Subscription__c,o);
            }
            
            
            
            
            
            //Parse Json
            StripeListCharge.Charges varCharges;
            JSONParser parse;
            parse = JSON.createParser(jsonString.replaceAll('currency','currency_data')); 
            varCharges = (StripeListCharge.Charges)parse.readValueAs(StripeListCharge.Charges.class);
        
            for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }else {
                Opportunity Op = oppSubChargeMap.get(charges.id);
                Op.Refund_Amount__c = charges.amount_refunded*0.01;
                opUpdateList.add(Op);
                
                if( oppListMap.get(Op.id) !=null){
                    List<OpportunityLineItem> oliList =  oppListMap.get(Op.id);
                    //System.debug('==========='+oliList );
                    if(charges.amount_refunded == charges.amount) {
                        for(OpportunityLineItem oliLine : oliList) {
                          
                            oliLine.refund__c = oliLine.unitPrice;
                           // oliLine.Success_Failure_Message__c = 'charge.refunded';
                            oliUpdateList.add(oliLine);
                        }
                    }
                    else if (charges.amount_refunded != charges.amount) {
                    
                    
                    if(oliList.size() > 0){
                    decimal refundedinSF = 0;
                    decimal refundedInStripe = 0;
                    refundedInStripe = charges.amount_refunded * 0.01;
                    for(OpportunityLineItem oliLine : oliList) {
                                          
                        refundedinSF = refundedinSF+ oliLine.refund__c;
                       
                    }
                    decimal refundedAmount = refundedInStripe  -  refundedinSF  ;
                    for(OpportunityLineItem opli : oliList) {
                        opli.Refunded_date__c = Date.Today();
                            if(opli.unitPrice!=opli.refund__c) {
                                    if (opli.unitPrice < = refundedAmount) {
                                        
                                        decimal tempAmt = refundedAmount - opli.unitPrice;
                                        opli.refund__c = refundedAmount - tempAmt;  
                                        refundedAmount = tempAmt;
                                        //System.debug('=======refundedAmount ========'+refundedAmount );
                                        
                                    } else 
                                    {
                                        Decimal tempAmt = opli.unitPrice - refundedAmount;
                                         opli.refund__c = opli.unitPrice - tempAmt ; 
                                        refundedAmount = opli.unitPrice -(tempAmt + refundedAmount);  
                                         //System.debug('=======refundedAmount ========'+refundedAmount );                                           
                                    }
                                //}
                                 oliUpdateList.add(opli );
                            }
                        
                        
                        }
                        
                        }
                    
                    
                    
                    }
                
                }
            }


            }
       
        
        if(opInsertList.size() > 0 ) {
            insert opInsertList;
            Set<Id> oppId = new set<id>();
            
            for(Opportunity op : opInsertList) {
                oppId.add(op.id);
            }
             List<Opportunity> opplists = [select id,name, amount,Stripe_Charge_Id__c,Refund_Amount__c from opportunity where id IN :  oppId];
            List<OpportunityLineItem> oppLinelist = new List<OpportunityLineItem>();
            for (opportunity Opp : opplists) {
                 OpportunityLineItem oppli = new OpportunityLineItem(); 
                                 oppli.OpportunityId = Opp.id;
                                 System.debug('------>'+priceBookMap);
                                 System.debug('------>'+String.valueOf(Opp.Name));
                                System.debug('------>'+priceBookMap.containsKey(String.valueOf(Opp.Name)));
                                if(priceBookMap.containsKey(String.valueOf(Opp.Name)))  {
                                     System.debug('----opp.NickNameEnter----'+String.valueOf(Opp.Name));
                                oppli.PricebookEntryId= priceBookMap.get(String.valueOf(Opp.Name));
                                //oppli.Stripe_charge_id__c =String.valueOf(Opp.Stripe_Charge_Id__c); 
                                oppli.Quantity = 1;
                                oppli.Refund__c = opp.Refund_Amount__c;
                                oppli.TotalPrice = opp.Amount;
                                oppLinelist.add( oppli);
            }
        }
        if(oppLinelist.size() > 0) {
            insert oppLinelist;
        }
        } 
        if(opUpdateList.size() > 0 ) {
            update opUpdateList;
        }
        if(oliUpdateList.size() > 0 ) {
            update oliUpdateList;
        }
        if(stripeProfileList.size() > 0) {
        update stripeProfileList;
        }
         //Only for test coverage
        if(Test.isRunningTest()){
            for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }
            }
            for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }
            }
            for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }
            }
             for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }
            }
             for(StripeListCharge.Data charges : varCharges.data){ 
            if(!oppSubChargeMap.containsKey(charges.id)) {
                Opportunity opp = new Opportunity();
                    //System.debug('=====stripeProfIdMap==='+stripeProfIdMap);
                    //System.debug('=====charges.customer==='+charges.customer);
                    
                        opp.Contact__c = stripeProfileList[0].Customer__c;
                    if (devRecordTypeId !='' && devRecordTypeId !=null)
                        opp.RecordTypeId = devRecordTypeId ;
                    
                    
                    if(charges.statement_descriptor!=null)
                        opp.Name=charges.statement_descriptor;
                    else
                        
                        opp.Name= charges.id;
                    
                    opp.stageName= 'Closed Won';
                    
                    opp.closeDate = Date.valueOf(System.today());
                    opp.Refunded_Date__c = Date.valueOf(System.today());
                    //opp.CloseDate = Date.valueOf(System.today());
                    opp.Stripe_Charge_Id__c = charges.id;
                    opp.Amount = (charges.amount )*0.01;
                    opp.Refund_Amount__c = charges.amount_refunded*0.01;
                    opp.Success_Failure_Message__c = charges.status;  
                    
                    opp.Statement_Descriptor__c = charges.statement_descriptor;
                    opp.Description= charges.description;
                    //opp.Name = charges.description_data;
                    opp.Currency__c = charges.currency_data;
                    opp.Paid__c = charges.paid;
                    opInsertList.add(opp);
                
            }
            }
        }
    }
    global void finish(database.batchablecontext bc){
        
    }
}