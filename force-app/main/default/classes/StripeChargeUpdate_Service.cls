@RestResource(urlMapping='/Stripe_ChargeUpdateService')
global class StripeChargeUpdate_Service {
    @HttpPost
    global static void createCustomer() {
        string CustomerIde;
        if(RestContext.request.requestBody!=null){
            try {
                String str                              =   RestContext.request.requestBody.toString();
                
                Map<String, Object> results             =   (Map<String, Object>)JSON.deserializeUntyped(str);
                CustomerIde                             = String.valueOf(results.get('id'));                 List<Map<String, Object>> data          =   new List<Map<String, Object>>();
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('data');
                Map<String, Object> aryCustomers        =   (Map<String, Object>) lstCustomers.get('object');
                Map<String, Object>  metaDataMap        =   (Map<String, Object> )aryCustomers.get('metadata');
                String customerId                       =   String.valueOf(aryCustomers .get('customer')); 
                // CustomerIde                         =   String.valueOf(aryCustomers .get('Id')); 
                Map<String, Object> sourceMap           =   (Map<String, Object>) aryCustomers .get('source');
                String cardId ;
                if (sourceMap == null) {
                    cardId                           =   String.valueOf(aryCustomers .get('payment_method'));
                }else {
                    cardId                           =   String.valueOf(sourceMap.get('id')) ;
                }
                String chargeId                         =  String.valueOf(aryCustomers .get('id'));     
                
                
                String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
                String devRecordTypeIdSub = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
                List<Stripe_Profile__c> lstStripeProf   = [ select id,Customer__c,(select id from Cards__r where Stripe_Card_Id__c=:cardId  ) from Stripe_Profile__c  where Stripe_Customer_Id__c =:customerId  limit 1];
                
                List<Card__c> cardList= new List<Card__c> ();
                for(Stripe_Profile__c a : lstStripeProf){
                    
                    for(Card__c cardOb : a.Cards__r){
                        cardList.add(cardOb);
                        
                    }
                    
                }
                
                Map<String, Object> requestMap =  new  Map<String, Object>();
                requestMap  = (Map<String, Object>) results.get('request');
                
                List<opportunity> lstOpportunity = new List<opportunity>();
                
                List<OpportunityLineItem> opLineItemSubscription = [select id,unitPrice,refund__c,opportunityId,Stripe_charge_id__c from OpportunityLineItem where Stripe_charge_id__c = : chargeId limit 1000];
                if(opLineItemSubscription.size() <= 0) {
                    lstOpportunity  = [ SELECT id ,RequestId__c,Refunded_Date__c ,amount,Scheduled_Payment_Processed__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c FROM opportunity where Stripe_Charge_Id__c  =: chargeId OR RequestId__c=: String.valueOf(requestMap.get('id')) limit 1000];
                    
                }
                
                Boolean isSalesforceInitiated = false;
                Boolean isStripeInitiatedRefund = True;
                if(!(metaDataMap== null) || !(metaDataMap.isEmpty())){
                    if(metaDataMap.get('Integration Initiated From') =='Salesforce' || Test.isRunningTest()){
                        isSalesforceInitiated = true;
                        
                    }
                    
                }
                List<String> oppId = new List<String>();
                List<String> conId = new List<String>();
                String prodname;
                String stripeStatementDescriptor = '';
                String refundAmount= '';
                String totamt = '';
                refundAmount= String.ValueOf(aryCustomers.get('amount_refunded'));
                totamt= String.ValueOf(aryCustomers.get('amount'));
                Integer i = 0;
                Decimal amount = 0;
                Decimal taxAmount = 0;
                Decimal shippingAmount = 0;
                String createdUsing ;
                if (metaDataMap.get('Tax') != null && metaDataMap.get('Tax') != '0') {
                    if (Decimal.valueOf(totamt) != Decimal.valueOf(refundAmount)) {
                        amount = (Decimal.valueOf(refundAmount)/(1+(Decimal.valueOf(String.valueOf(metaDataMap.get('TaxPercentage')))*0.01)))*0.01;
                        amount = amount.setScale(2);
                        taxAmount = (Decimal.valueOf(refundAmount)*0.01)-amount;
                    }else {
                        amount  = Decimal.valueOf(String.valueOf(metaDataMap.get('Core Amount')));
                        taxAmount = Decimal.valueOf(String.valueOf(metaDataMap.get('Tax'))); 
                    }
                    for (Opportunity opp : [Select id,Contact__c,Created_Using__c From Opportunity WHERE (Stripe_Charge_Id__c =:chargeId OR Secondary_Stripe_Charge_Id__c =: chargeId) AND RecordTypeId = :devRecordTypeId ]) {
                        createdUsing = opp.Created_Using__c;
                        oppId.add(opp.Id);
                        conId.add(opp.Contact__c);
                    }
                    if (createdUsing == 'V10') {
                        shippingAmount = Decimal.valueOf(String.valueOf(metaDataMap.get('Shipping Cost')));
                    }
                    for (OpportunityLineItem oli : [SELECT id,Product2.Name,Product2.Statement_Descriptor__c FROM OpportunityLineItem WHERE OpportunityId IN:oppId]) {
                        prodname = oli.Product2.Name;
                        if (i==0) {
                            stripeStatementDescriptor += oli.Product2.Statement_Descriptor__c;
                        }else {
                            stripeStatementDescriptor += ' | ' + oli.Product2.Statement_Descriptor__c;
                        }
                        i++;
                    }
                }else{
                    for (OpportunityLineItem opli : [SELECT Id, Opportunity.Contact__c,OpportunityId,Product2.Price__c,Tax_Amount__c,Product2.Name,Product2.Statement_Descriptor__c,Opportunity.Created_Using__c,Shipping_Cost__c,Tax_Percentage_s__c  FROM OpportunityLineItem WHERE Last_Charge_Id__c =:chargeId AND Last_Charge_Id__c != null AND Opportunity.RecordTypeId = :devRecordTypeIdSub  AND Tax_Amount__c != null]) {
                        createdUsing = opli.Opportunity.Created_Using__c;
                        if (createdUsing == 'V10') {
                            shippingAmount = opli.Shipping_Cost__c;
                        }
                        conId.add(opli.Opportunity.Contact__c);
                        if (Decimal.valueOf(totamt) != Decimal.valueOf(refundAmount)) {
                            amount = (Decimal.valueOf(refundAmount)/(1+(opli.Tax_Percentage_s__c*0.01)))*0.01;
                            amount = amount.setScale(2);
                            taxAmount = (Decimal.valueOf(refundAmount)*0.01)-amount;
                        }else {
                            amount = opli.Product2.Price__c;
                            taxAmount = opli.Tax_Amount__c;                            
                        }
                        prodname = opli.Product2.Name;
                        stripeStatementDescriptor = opli.Product2.Statement_Descriptor__c;
                    }
                }
                
                
                if(conId.size()>0  && amount != 0 && taxAmount != 0){
                    List<Address__c> addressList = [SELECT Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c,Contact__c FROM Address__c WHERE Contact__c IN : conId and Contact__c!=null  AND Primary__c = true];                    
                    if (addressList[0].Shipping_Country__c == 'USA' || addressList[0].Shipping_Country__c.contains('United')) {
                        addressList[0].Shipping_Country__c = 'US';
                    }
                    TaxjarTaxCalculate.refundTransaction(addressList[0].Shipping_Street__c, addressList[0].Shipping_City__c, addressList[0].Shipping_State_Province__c, addressList[0].Shipping_Country__c, addressList[0].Shipping_Zip_Postal_Code__c, amount.setScale(2), 1, taxAmount, stripeStatementDescriptor, chargeId, prodname, shippingAmount, createdUsing);
                }
                
                //This code snippet if the refund from stripe for Subscription products  
                if(opLineItemSubscription.size() > 0) {
                    String refundAmt= '';
                    refundAmt= String.ValueOf(aryCustomers.get('amount_refunded'));
                    List<OpportunityLineItem > updateOLI = new List<OpportunityLineItem>();
                    List<Opportunity > updateOpp = new List<Opportunity>();
                    for(OpportunityLineItem oli : opLineItemSubscription ){
                        
                        if(oli.unitPrice!=oli.refund__c) {
                            if(oli.refund__c != null ){
                                if(refundAmt != null && refundAmt != ''){
                                    oli.refund__c = oli.refund__c + decimal.valueOf(refundAmt)*0.01;
                                }    
                            }else {
                                if(refundAmt != null && refundAmt != ''){
                                    oli.refund__c = decimal.valueOf(refundAmt)*0.01;
                                }
                            }
                            updateOLI.add(oli);
                        }
                    }
                    if(opLineItemSubscription[0].opportunityId!=null){
                        List<Opportunity> lstOpp= [select id,Refund_Amount__c from opportunity where Id=:opLineItemSubscription[0].opportunityId ];
                        if(lstOpp.size() > 0) {
                            
                            if(lstOpp[0].Refund_Amount__c != null){
                                if(refundAmt != null && refundAmt != ''){
                                    lstOpp[0].Refund_Amount__c = lstOpp[0].Refund_Amount__c + decimal.valueOf(refundAmt)*0.01;
                                }
                            }else {
                                if(refundAmt != null && refundAmt != ''){
                                    lstOpp[0].Refund_Amount__c  = decimal.valueOf(refundAmt)*0.01;
                                }
                            }
                            updateOpp.add(lstOpp[0]);
                        }
                    }
                    if(updateOLI.size() > 0 && isStripeInitiatedRefund) {
                        update updateOLI;
                    }
                    if(updateOpp.size() > 0 && isStripeInitiatedRefund){
                        update updateOpp;
                    }
                }else if( lstOpportunity.size() > 0 ) {
                    
                    if (lstOpportunity[0].Scheduled_Payment_Date__c==null || lstOpportunity[0].Scheduled_Payment_Processed__c== true ) {
                        Map<string,object> refundsMap = (Map<string,object>)aryCustomers.get('refunds');
                        List<Object> refundsDataList        =   (List<Object>) refundsMap.get('data');
                        
                        Map<object,object> refundAmountMap = new Map<object,object>();
                        Map<String, Object> objAccount = (Map<String, Object>)refundsDataList[0];
                        Integer totalAmount  = Integer.valueOf(aryCustomers.get('amount'));
                        Integer totalRefunded = 0;
                        
                        Map<String,Object> dataCharge = (Map<String,Object>)refundsDataList[0];
                        String checkMetadata='';
                        checkMetadata = String.valueOf(dataCharge.get('metadata'));
                        
                        if(dataCharge.get('metadata')!=null && dataCharge.get('metadata')!='' && checkMetadata.contains('Integration Initiated From')) {
                            
                            isStripeInitiatedRefund =false;
                            
                        }
                        
                        for(Object fld : refundsDataList){    
                            Map<String,Object> refunddata = (Map<String,Object>)fld ;
                            
                            totalRefunded  = totalRefunded  +Integer.valueOf(refunddata.get('amount'));
                            
                        }
                        Decimal totalRefundedAmt  = (totalAmount  -totalRefunded ) *0.01 ;
                        lstOpportunity[0].Refunded_Date__c = Date.Today();
                        lstOpportunity[0].Success_Failure_Message__c= String.valueOf(results.get('type'));
                        lstOpportunity[0].CardId__c =  String.valueOf(sourceMap.get('id')) ;
                        if(lstOpportunity[0].Success_Failure_Message__c=='charge.succeeded') {
                            lstOpportunity[0].Stripe_Message__c = 'Success';
                            lstOpportunity[0].Paid__c =true;
                        }else if (lstOpportunity[0].Success_Failure_Message__c=='charge.refunded') {
                            
                            lstOpportunity[0].Stripe_Message__c ='Success';
                            String amt = '';
                            amt = String.ValueOf(aryCustomers.get('amount_refunded'));
                            
                            Decimal oppAmount =  lstOpportunity[0].amount;
                            Decimal oppAmountTotalToupdate =  oppAmount - decimal.valueOf(amt )*0.01;
                            
                            lstOpportunity[0].amount= totalRefundedAmt  ;   
                            lstOpportunity[0].Refund_Amount__c= decimal.valueOf(amt )*0.01;
                            lstOpportunity[0].Description= '$'+decimal.valueOf(amt ) *0.01+' refunded due to '+String.ValueOf(objAccount.get('reason') )+' charge';
                            
                            
                        }
                        else
                        {
                            lstOpportunity[0].Stripe_Message__c ='Failure';
                            
                        }
                        if(lstStripeProf.size () > 0) 
                            lstOpportunity[0].Stripe_Profile__c = lstStripeProf[0].id;
                        
                        if(cardList.size () > 0)
                            lstOpportunity[0].card__c =cardList[0].id;
                        
                        update lstOpportunity; 
                        decimal refundedAmount = totalRefunded*0.01  ;
                        List<OpportunityLineItem> opLineItemUpdate = new List<OpportunityLineItem> ();
                        
                        List<OpportunityLineItem> opLineItem = [select id,unitPrice,refund__c from OpportunityLineItem where OpportunityId = : lstOpportunity[0].id];
                        for(OpportunityLineItem opli : opLineItem) {
                            if(opli.unitPrice!=opli.refund__c) {
                                if (opli.unitPrice < = refundedAmount) {
                                    decimal tempAmt = refundedAmount - opli.unitPrice;
                                    opli.refund__c = refundedAmount - tempAmt;  
                                    refundedAmount = tempAmt;
                                    
                                    
                                } else 
                                {
                                    Decimal tempAmt = opli.unitPrice - refundedAmount;
                                    opli.refund__c = opli.unitPrice - tempAmt ; 
                                    refundedAmount = opli.unitPrice -(tempAmt + refundedAmount);  
                                    
                                }
                                
                                opLineItemUpdate.add(opli );
                            }
                            
                            
                        }
                        
                        if(opLineItemUpdate.size() > 0 && isStripeInitiatedRefund  ) {
                            update opLineItemUpdate;
                        }
                        
                    }
                    
                }
                 ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeChargeUpdate_Service',
                 'createCustomer',
                 CustomerIde
                 )
                 ); 
                
                if(Test.isRunningTest())  
                    integer intTest =1/0;   
            }
            catch(Exception e) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeChargeUpdate_Service',
                        'createCustomer',
                        CustomerIde,
                        e
                    )
                ); 
            }
        }
        
    }
    
}