@isTest
public without sharing class Plaid_ACH_Test {

	@testSetup static void setup() {
		Plaid__c pl = new Plaid__c();
        pl.public_key__c = 'test';
        pl.client_id__c = 'test';
        pl.Secret_key__c = 'test';
        pl.Name = 'PlaidAPI';
        insert pl;    
        
    }

	static testmethod void  test1(){

		test.startTest();
		Contact con =new Contact();
        	con.LastName = 'test';
        	insert con;
		PageReference pageRef = Page.StripePlaidLink;
        Test.setCurrentPage(pageRef);

        Plaid_ACH pa = new Plaid_ACH();
        pa.public_token = 'public-sandbox-0179489d-5559-4310-a5b8-4dbd6ef28e89';
        pa.bank_name = 'Citizens Bank';
        pa.externalId=con.id;
        pa.isSafe=true;
        pa.isError=true;
        pa.messageError='Account exist already';
        pa.public_key='test';
        
        pa.insertRecord();
        pa.CallWebService();

        test.stopTest();
	}
	
}