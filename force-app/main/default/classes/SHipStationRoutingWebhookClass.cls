@Restresource(urlMapping='/SHipStationRoutingWebhookClass')
global class SHipStationRoutingWebhookClass {
    @HttpPost
    global static void shipStationRouted(){
        string str= RestContext.request.requestBody.toString();
        System.debug('=================='+str);
        Map<String, Object> ResultMap =(Map<String, Object>)Json.deserializeUntyped(str);
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        String Header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        String url=String.valueOf(ResultMap.get('resource_url'));
        String EventType=String.valueOf(ResultMap.get('resource_type'));
        try {
            if(url!=null) {
                HttpRequest req= new HttpRequest();
                req.setEndpoint(url);
                req.setMethod('GET');
                req.setHeader('Authorization', 'Basic '+Header);
                Http htp= new Http();
                HttpResponse res=htp.send(req);
                String Respons=res.getBody();
                if(res.getBody()!=null) {
                    Map<String,Object> responseMap=(Map<String, Object>)json.deserializeUntyped(Respons);
                    Map<String,Object> ordMap=new Map<String,Object>();
                    List<Object> myMapObjects = (List<Object>) responseMap.get('orders');
                    for (Object obj : myMapObjects) {
                        ordMap=(Map<String,Object>)obj;
                    }
                    String StationMail=String.valueOf(ordMap.get('customerEmail'));
                    Map<string, Object> ShippingInfo = (Map<String, Object>)ordMap.get('shipTo');
                    Map<string, Object> billToo = (Map<String, Object>)ordMap.get('billTo');
                    Map<string, Object> AdvancearrayData = (Map<String, Object>)ordMap.get('advancedOptions');
                    List<ShipStation_Orders__c> listOrders=new List<ShipStation_Orders__c>();
                    List<Contact> conData=[Select id,Email from Contact where Email=:StationMail];
                    List<Contact> CustomerContact = new List<Contact>();
                    if(conData.isEmpty()){
                        Contact con=new Contact();
                            String strs=String.valueOf(ShippingInfo.get('name'));
                        if(strs.contains(' ')){
                            List<String> StrName=strs.split(' ');
                            con.LastName=strName[0];
                            con.FirstName=strName[1];
                        }
                        else{
                          con.LastName=strs;  
                        }
                        con.Email=String.valueOf(ordMap.get('customerEmail'));
                        con.Phone=String.valueOf(ShippingInfo.get('phone'));
                        CustomerContact.add(con);
                        insert CustomerContact;
                        ShipStation_Orders__c shiporder=new ShipStation_Orders__c();
                        shiporder.Store_Id__c=String.valueOf(AdvancearrayData.get('storeId'));
                        shiporder.orderId__c=String.valueOf(ordMap.get('orderId'));
                        shiporder.customerEmail__c=String.valueOf(ordMap.get('customerEmail'));
                        for(Contact co : CustomerContact){
                            shiporder.Contact__c=co.id;
                        }
                        shiporder.createDate__c=String.valueOf(ordMap.get('createDate'));
                        shiporder.customerUsername__c=String.valueOf(ordMap.get('customerUsername'));
                        shiporder.customerId__c=String.valueOf(ordMap.get('customerId'));
                        shiporder.modifyDate__c=String.valueOf(ordMap.get('modifyDate'));
                        shiporder.orderDate__c=String.valueOf(ordMap.get('orderDate'));
                        shiporder.paymentDate__c=String.valueOf(ordMap.get('paymentDate'));
                        shiporder.shipByDate__c=String.valueOf(ordMap.get('shipByDate'));
                        shiporder.orderKey__c=String.valueOf(ordMap.get('orderKey'));
                        shiporder.orderNumber__c=String.valueOf(ordMap.get('orderNumber'));
                        shiporder.orderStatus__c=String.valueOf(ordMap.get('orderStatus'));
                        shiporder.Shipname__c=String.valueOf(ShippingInfo.get('name'));
                        shiporder.Ship_Company__c=String.valueOf(ShippingInfo.get('company'));
                        shiporder.Ship_Street1__c=String.valueOf(ShippingInfo.get('street1'));     
                        shiporder.Ship_Street1__c=String.valueOf(ShippingInfo.get('street1'));
                        shiporder.ShipTo_Street2__c=String.valueOf(ShippingInfo.get('street2'));
                        shiporder.Ship_To_Street3__c=String.valueOf(ShippingInfo.get('street3'));
                        shiporder.Ship_To_AddressVerified__c=String.valueOf(ShippingInfo.get('addressVerified'));
                        shiporder.Ship_To_City__c=String.valueOf(ShippingInfo.get('city'));
                        shiporder.Ship_To_Country__c=String.valueOf(ShippingInfo.get('country'));
                        shiporder.Ship_To_Phone__c=String.valueOf(ShippingInfo.get('phone'));
                        shiporder.Ship_To_PostalCode__c=String.valueOf(ShippingInfo.get('postalCode'));
                        shiporder.Ship_To_State__c=String.valueOf(ShippingInfo.get('state'));
                        shiporder.name__c=String.valueOf(billToo.get('name'));
                        shiporder.company__c=String.valueOf(billToo.get('company'));
                        shiporder.street1__c=String.valueOf(billToo.get('street1'));
                        shiporder.street2__c=String.valueOf(billToo.get('street2'));
                        shiporder.street3__c=String.valueOf(billToo.get('street3'));
                        shiporder.city__c=String.valueOf(billToo.get('city'));
                        shiporder.state__c=String.valueOf(billToo.get('state'));
                        shiporder.country__c=String.valueOf(billToo.get('country'));
                        shiporder.phone__c=String.valueOf(billToo.get('phone'));
                        shiporder.postalCode__c=String.valueOf(billToo.get('postalCode'));
                        shiporder.addressVerified__c=String.valueOf(billToo.get('addressVerified'));
                        listOrders.add(shiporder);
                    }
                    if(!conData.isEmpty()) {
                        for(Contact con:conData) {
                            ShipStation_Orders__c shiporder=new ShipStation_Orders__c();
                            shiporder.orderId__c=String.valueOf(ordMap.get('orderId'));
                            shiporder.customerEmail__c=String.valueOf(ordMap.get('customerEmail'));
                            shiporder.Contact__c=con.id;
                            shiporder.createDate__c=String.valueOf(ordMap.get('createDate'));
                            shiporder.Store_Id__c=String.valueOf(AdvancearrayData.get('storeId'));
                            shiporder.customerUsername__c=String.valueOf(ordMap.get('customerUsername'));
                            shiporder.customerId__c=String.valueOf(ordMap.get('customerId'));
                            shiporder.modifyDate__c=String.valueOf(ordMap.get('modifyDate'));
                            shiporder.orderDate__c=String.valueOf(ordMap.get('orderDate'));
                            shiporder.paymentDate__c=String.valueOf(ordMap.get('paymentDate'));
                            shiporder.shipByDate__c=String.valueOf(ordMap.get('shipByDate'));
                            shiporder.orderKey__c=String.valueOf(ordMap.get('orderKey'));
                            shiporder.orderNumber__c=String.valueOf(ordMap.get('orderNumber'));
                            shiporder.orderStatus__c=String.valueOf(ordMap.get('orderStatus'));
                            shiporder.Shipname__c=String.valueOf(ShippingInfo.get('name'));
                            shiporder.Ship_Company__c=String.valueOf(ShippingInfo.get('company'));
                            shiporder.Ship_Street1__c=String.valueOf(ShippingInfo.get('street1'));     
                            shiporder.ShipTo_Street2__c=String.valueOf(ShippingInfo.get('street2')); 
                            shiporder.Ship_To_Street3__c=String.valueOf(ShippingInfo.get('street3'));
                            shiporder.Ship_To_AddressVerified__c=String.valueOf(ShippingInfo.get('addressVerified'));
                            shiporder.Ship_To_City__c=String.valueOf(ShippingInfo.get('city'));
                            shiporder.Ship_To_Country__c=String.valueOf(ShippingInfo.get('country'));
                            shiporder.Ship_To_Phone__c=String.valueOf(ShippingInfo.get('phone'));
                            shiporder.Ship_To_PostalCode__c=String.valueOf(ShippingInfo.get('postalCode'));
                            shiporder.Ship_To_State__c=String.valueOf(ShippingInfo.get('state'));
                            shiporder.name__c=String.valueOf(billToo.get('name'));
                            shiporder.company__c=String.valueOf(billToo.get('company'));
                            shiporder.street1__c=String.valueOf(billToo.get('street1'));
                            shiporder.street2__c=String.valueOf(billToo.get('street2'));
                            shiporder.street3__c=String.valueOf(billToo.get('street3'));
                            shiporder.city__c=String.valueOf(billToo.get('city'));
                            shiporder.state__c=String.valueOf(billToo.get('state'));
                            shiporder.country__c=String.valueOf(billToo.get('country'));
                            shiporder.phone__c=String.valueOf(billToo.get('phone'));
                            shiporder.postalCode__c=String.valueOf(billToo.get('postalCode'));
                            shiporder.addressVerified__c=String.valueOf(billToo.get('addressVerified'));
                            listOrders.add(shiporder);
                        }
                    }
                    insert listOrders;
                    System.debug('===listOrders======'+listOrders);
                    List<ShipStation_Order_Items__c> ShipStationItem = new List<ShipStation_Order_Items__c>();
                    List<Object> OrderLineItems = (List<Object>) ordMap.get('items');
                    Map<String, Object> IemsMap = new Map<String, Object>();
                    for (Object items : OrderLineItems) {
                        IemsMap=(Map<String,Object>)items;
                        if(!IemsMap.isEmpty()){
                            ShipStation_Order_Items__c soitems =new ShipStation_Order_Items__c();
                            soitems.orderItemId__c=String.valueOf(IemsMap.get('orderItemId'));
                            soitems.createDate__c=String.valueOf(IemsMap.get('createDate'));
                            soitems.adjustment__c=boolean.valueOf(IemsMap.get('adjustment'));
                            soitems.fulfillmentSku__c=String.valueOf(IemsMap.get('fulfillmentSku'));
                            soitems.imageUrl__c=String.valueOf(IemsMap.get('imageUrl'));
                            soitems.lineItemKey__c=String.valueOf(IemsMap.get('lineItemKey'));
                            soitems.modifyDate__c=String.valueOf(IemsMap.get('modifyDate'));
                            soitems.name__c=String.valueOf(IemsMap.get('name'));
                            soitems.productId__c=String.valueOf(IemsMap.get('productId'));
                            soitems.quantity__c=Integer.valueOf(IemsMap.get('quantity'));
                            soitems.shippingAmount__c=Double.valueOf(IemsMap.get('shippingAmount'));
                            soitems.sku__c=String.valueOf(IemsMap.get('sku'));
                            soitems.taxAmount__c=Double.valueOf(IemsMap.get('taxAmount'));
                            soitems.unitPrice__c=Double.valueOf(IemsMap.get('unitPrice'));
                            soitems.upc__c=String.valueOf(IemsMap.get('upc'));
                            soitems.warehouseLocation__c=String.valueOf(IemsMap.get('warehouseLocation'));
                            soitems.weight__c=String.valueOf(IemsMap.get('weight'));
                            if(!listOrders.isEmpty()){
                                for(ShipStation_Orders__c Ids:listOrders){
                                    soitems.ShipStation_Orders__c=Ids.Id;
                                }
                            }
                            ShipStationItem.add(soitems); 
                        }
                    }
                    insert ShipStationItem;
                }
            }
        }
        catch(Exception e){}
    }
}