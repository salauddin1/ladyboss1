public class SmsContactRecord {
    public string toNumber{get;set;}
    public string toNum{get;set;}
    public string fromNumber{get;set;}
    public String lastReply {get;set;}
    public String phone {get;set;}
    public String conId {get;set;}
    public string messageData{get;set;}
    public string senderName{get;set;}
    public List<Event> smsUtil {get;set;}
    public List<Event> showSMSList{get;set;}
    public String smsId{get;set;} //capture value from assignTo
    Public Integer newSMSCount{get;set;}
    Public Boolean newSMSBool{get;set;}
    Public Boolean isFullCon{get;set;}
    Public Boolean isListSMS{get;set;}
    Public Boolean errorMsgFlag{get;set;}
    Public String errorMsg{get;set;}
    Public String BodyerrorMsg{get;set;}
    public List<Event> Util  = new List<Event>();
    //}
    //cunstuctor running when page load
    public SmsContactRecord(){
        
        List<User> userdata = [Select Id,Name,Email,Twillio_From_phone__c from User where isActive = true AND Id=:userinfo.getuserId() limit 1];
        if(userdata.size()>0 && userdata[0].Twillio_From_phone__c != null && userdata[0].Twillio_From_phone__c != '') {
            errorMsgFlag = false;
        }
        else {
            errorMsgFlag = true;
            errorMsg = 'You Dont have any number for send SMS Please contact your administrator to assign you a number';
        }
        reload();
    }
    public void sendSMS()  {
        // Show success and Error message on the Page and Send SMS
        Map<String,Object> mapData = sendSMSNew();
        
        if(mapData.get('success') != null && (Boolean)mapData.get('success'))  {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,(String)mapData.get('message')));
        }else  {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,(String)mapData.get('message')));
        }
        toNumber = toNum;
    }
    //To show selected number and its last reply in Text Box
    public void showNumber() {
        String phoneNum ;
        isFullCon =true;
        isListSMS =false;
        List<Event> showSMSListV1 =new  List<Event>();
        showSMSList =new  List<Event>();
        List<Contact> contList = new List<Contact>();
        contList = [select id,Name,Phone,mobilePhone,otherphone from Contact where id =: conId];
        for(Contact con : contList) {
            if(con.phone != null) {
                phoneNum = con.phone;
            }
            else  If(con.mobilePhone!=null){
                phoneNum=con.mobilePhone;
            }
            else If(con.otherphone !=null){
                phoneNum=con.otherphone;
            }
            senderName = con.Name;
        }
        //Query conversation to show
        showSMSListV1 = [SELECT SMS_Initiated_By__c, From_Phone_Number__c, isNew__c , Phone__c,Sent_By__r.name, Sent_By__c, Description,CreatedDate , Id FROM Event where Phone__c=:phoneNum AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate ASC Limit 50];
        
        for(Event num : showSMSListV1 ) {
            toNumber = num.Phone__c;
            lastReply = num.Description;
            if(num.isNew__c==true){
                num.isNew__c = false;
            }
            showSMSList.add(num);
        }  
        update showSMSList;
        Map<String,Event> lstMap  =  new Map<String,Event> ();
        smsUtil = new List<Event>();
        newSMSCount = 0;
        List<Event> lst = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c,Phone__c,Sent_By__r.name, Sent_By__c, Description,CreatedDate , Id FROM Event where Phone__c=:phoneNum and Sent_By__c =:userinfo.getuserId() Order By CreatedDate ASC Limit 100];
        for(Event s : lst){
            if(s.isNew__c==true){
                newSMSCount=newSMSCount+1;
            }
            if(newSMSCount == 0) {
                newSMSBool =false;
            }      
            if(!lstMap.containsKey(s.Phone__c)){
                lstMap.put(s.Phone__c,s);
            }
        }
        for(string str : lstMap.keySet()){
            smsUtil.add(lstMap.get(str));
        }
        smsId = '';
    }
    //Send Sms to twilio and return response
    public  Map<String,Object>  sendSMSNew(){
        Map<String,Object> mapOutput = new  Map<String,Object>();
        try{
            String str1 =messageData ; 
            String fromNumber;
            //Query twilio Credentials from salesforce metadata
            List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain' ];
            String accountSID = lstTwilliConfigs.get(0).AccountSID__c;  //'AC44099911ff7ec99c40bc97943ad6c0ca';
            String authToken = lstTwilliConfigs.get(0).AuthToken__c; //'6a386f07923a55ec0729eff283d747a3';
            USER currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Id=:userinfo.getuserId() limit 1];
            System.debug('===='+currentuser.Twillio_From_phone__c);
            if(currentuser != null && currentuser.Twillio_From_phone__c != null) {
                fromNumber = String.valueOf(currentuser.Twillio_From_phone__c);
            }
            
            if(str1 != null && str1 != '') {
                //Do a callout from here to the twilio webservice
                String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
                System.debug(str1 );
                HttpRequest req = new HttpRequest();
                req.setEndpoint(url);
                req.setMethod('POST');
                if(toNumber.contains('+1')){
                    req.setBody('To='+ EncodingUtil.urlEncode(toNumber ,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8'));
                }
                else {
                    req.setBody('To='+ EncodingUtil.urlEncode('+1'+toNumber ,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8')); 
                }
                // req.setBody('To='+ EncodingUtil.urlEncode('+1'+toNumber ,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8'));
                Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
                String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
                req.setHeader('Authorization', authorizationHeader);
                Http http = new Http();
                TwillioResponse tl;
                System.debug('tl==' );
                if(!test.isrunningTest() && toNumber !=''){
                    HTTPResponse res = http.send(req);
                    System.debug('===============res.getBody()====='+res.getBody());
                    tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
                    
                    if(tl.sid != null)  {
                        System.debug('tl=='+str1 );
                        //Configure Map to show SMS response from twilio on the page
                        mapOutput.put('success',TRUE);
                        mapOutput.put('message','SMS send succsufully');
                        // return 'SMS Send Successful';
                        Contact cont = [Select id,Phone,MobilePhone,OtherPhone from Contact where Phone =: toNumber or MobilePhone =: toNumber  or OtherPhone =: toNumber Limit 1];
                        System.debug('-------cont----'+cont);
                        Event smsu = new Event(DurationInMinutes = 1, ActivityDateTime = System.now(),Sent_By__c =UserInfo.getUserId(),Description = messageData,phone__c = toNumber,senddate__c = Date.Today(),From_Phone_Number__c=fromNumber,SMS_Initiated_By__c='Agent', Contact__c = cont.Id);
                        insert smsu;
                        toNumber = '';
                        messageData = '';
                        
                    }else  {
                        // if SMS sending is fail
                        mapOutput.put('success',FALSE);
                        mapOutput.put('message','SMS sending fail,Error from Twillio is : '+tl.message);
                    }
                }
                if(test.isRunningTest()){
                    mapOutput.put('success',FALSE);
                    mapOutput.put('message','SMS send succsufully');
                    Integer i=10/0;
                }
            }
            else {
                //If SMS Body is null and try to send sms then it return error
                BodyerrorMsg = 'Message body not contains any character';
                mapOutput.put('success',FALSE);
                mapOutput.put('message','SMS Not Send Due To Empty Body');
            }
            return mapOutput;
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'ContactSMSUtility',
                    'sendSMS',
                    '',
                    e
                )
            );  
            if(test.isRunningTest()){
                mapOutput.put('success',FALSE);
                mapOutput.put('message','SMS send succsufully');
            }
            return mapOutput;
        }
    } 
    class TwillioResponse  {
        public String sid;  
        public String message;
    }
    // this is reload the message on the page whenever a sms send or recived
    public void reload(){
        newSMSBool =true;
        newSMSCount = 0;
        isFullCon =true;
        //isListSMS =true;
        String phoneNum;
        String phoneNumform2;
        String currentId = ApexPages.currentPage().getParameters().get('id');
        System.debug('------currentId-----'+currentId);
        
        List<Contact> contList = new List<Contact>();
        contList = [select id,FirstName,Name,Phone,mobilePhone,otherphone from Contact where id =: currentId];
        conId = currentId;
        if(contList.size()>0 && contList[0].Name != null)
            senderName = contList[0].Name;
        for(Contact con : contList) {
            if(con.phone != null) {
                phoneNum = con.phone;
            }
            else if(con.mobilePhone != null){
                phoneNum=con.mobilePhone;
            }
            else if(con.otherphone !=null){
                phoneNum=con.otherphone;
            }
        }
        String phoneNumform3 = '';
        if (phoneNum!= null ) {
            phoneNumform2 = phoneNum.trim();
            
            if(phoneNumform2.contains('(') && phoneNumform2.contains(')')){
                phoneNumform2= phoneNumform2.replace('(','');
                
                phoneNumform2= phoneNumform2.replace(')','');
            }
            
            if(phoneNumform2.contains('-')){
                phoneNumform2= phoneNumform2.replace('-','');
            }
            if(!phoneNumform2.contains('+1'))
                phoneNumform2 = '+1'+phoneNumform2;
        	}
        	phoneNumform2 = phoneNumform2.replace(' ', '');
        if(phoneNumform2 != null) {
            phoneNumform3 = phoneNumform2.replace('+1', '');
        }
        System.debug('======phoneNumform2====='+phoneNumform2+'=======phoneNum======='+phoneNum);
        System.debug('----------phoneNum--------'+phoneNumform3);
        List<Event> lst = [SELECT SMS_Initiated_By__c, From_Phone_Number__c, senddate__c,isNew__c ,CreatedDate,  Phone__c, Sent_By__c, Description, Id FROM Event where Phone__c!=null AND Sent_By__c =:userinfo.getuserId() and (Phone__c =:phoneNum OR Phone__c =:phoneNumform2 OR Phone__c =:phoneNumform3) Order by createdDate ASC ];
        USER currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Id=:userinfo.getuserId() limit 1];
        System.debug('===='+currentuser.Twillio_From_phone__c);
        if(currentuser != null && currentuser.Twillio_From_phone__c != null) {
            fromNumber = String.valueOf(currentuser.Twillio_From_phone__c);
        }
        Map<String,Event> lstMap  =  new Map<String,Event> ();
        smsUtil = new List<Event>();
        system.debug(lst);
        for(Event s : lst){
            smsUtil.add(s);
            if(!lstMap.containsKey(s.Phone__c)){
                lstMap.put(s.Phone__c,s);
                displayPopup = true;
            }
            if(s.isNew__c==true){
                newSMSCount=newSMSCount+1;
            }
        }
        system.debug('========smsUtil======'+smsUtil);
        
        if(newSMSCount>0) {
            newSMSBool =true;
        }
        System.debug('-------ContList----'+ContList);
        for(Contact con : contList) {
            if(con.phone != null) {
                toNumber = con.phone;
            }
            else if(con.mobilePhone!=null){
                toNumber=con.mobilePhone;
            }
            else if(con.otherphone !=null){
                toNumber=con.otherphone;
            } 
        }
        toNum = toNumber;
    }
    public boolean displayPopup {get; set;}
    
    public void closePopup() {
        displayPopup = false;
    }
    
    public void showPopup() {
        displayPopup = true;
    }
}