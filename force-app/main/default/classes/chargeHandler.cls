public class chargeHandler {
 //To get all the purchases (charges) of the customer
    public static string getAllPurchases( string custId) {
        HttpRequest http = new HttpRequest();
        
        //List<Stripe_Profile__c> stripeProfList = [select id ,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
        String customerId = custId;
        
        http.setEndpoint('https://api.stripe.com/v1/charges?customer='+customerId+'&limit=50' );
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                response = hs.getBody();
                system.debug('response-- '+response);
                statusCode = hs.getStatusCode();
                return response ;
            } catch (CalloutException e) {
                return null;
            }
        } 
        //}
        
        // system.debug('#### '+ hs.getBody());
        system.debug('#### '+ hs);
        if (Test.isRunningTest()) {
            // response ='{ "id": "ch_1DWN6LBwLSk1v1ohg8oPkbcf", "object": "charge", "amount": 27695, "amount_refunded": 0,  "created": 1542197049, "currency": "usd", "customer": "cus_DyJg8dVaDdKrBp", "description": null,  "fraud_details": {}, "invoice": "in_1DWN6LBwLSk1v1ohbovw2E31", "livemode": false, "metadata": {}, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "type": "authorized" }, "paid": true,  "refunds": { "object": "list", "data": [], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWN6LBwLSk1v1ohg8oPkbcf/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWN3qBwLSk1v1oh8j1v8Cuu", "object": "card",  "brand": "Visa", "country": "US", "customer": "cus_DyJg8dVaDdKrBp", "cvc_check": null, "dynamic_last4": null, "exp_month": 4, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": {}, "name": null, "tokenization_method": null }, "source_transfer": null, "statement_descriptor": null, "status": "succeeded", "transfer_group": null }';
            Integer i =0;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
             i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            i=i++;
            response  = '{ "object": "list", "data": [ { "id": "ch_1DWkgPBwLSk1v1oh9LMBcL3p", "object": "charge", "amount": 13900, "amount_refunded": 13400, "application": null, "application_fee": null, "balance_transaction": "txn_1DWkgQBwLSk1v1ohDIAyuUQH", "captured": true, "created": 1542287697, "currency": "usd", "customer": "cus_DyNVX8mwbZvu5F", "description": null, "destination": null, "dispute": null, "failure_code": null, "failure_message": null, "fraud_details": { }, "invoice": "in_1DWkgPBwLSk1v1ohyhZQe4h7", "livemode": false, "metadata": { }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "risk_level": "normal", "risk_score": 14, "seller_message": "Payment complete.", "type": "authorized" }, "paid": true, "payment_intent": null, "receipt_email": null, "receipt_number": null, "refunded": false, "refunds": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWkgPBwLSk1v1oh9LMBcL3p/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4", "object": "card", "address_city": "cvb", "address_country": "US", "address_line1": "ghch", "address_line1_check": "pass", "address_line2": null, "address_state": "cvb", "address_zip": "cvb", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_DyNVX8mwbZvu5F", "cvc_check": null, "dynamic_last4": null, "exp_month": 1, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": { }, "name": "ghch", "tokenization_method": null }, "source_transfer": null, "statement_descriptor": null, "status": "succeeded", "transfer_group": null }, { "id": "ch_1DWQkqBwLSk1v1ohDs0Llngf", "object": "charge", "amount": 100, "amount_refunded": 0, "application": null, "application_fee": null, "balance_transaction": "txn_1DWQkqBwLSk1v1ohhwXq7qPq", "captured": true, "created": 1542211092, "currency": "usd", "customer": "cus_DyNVX8mwbZvu5F", "description": "$1 One Time Charge", "destination": null, "dispute": null, "failure_code": null, "failure_message": null, "fraud_details": { }, "invoice": null, "livemode": false, "metadata": { "Billing Address": "cvb ghch cvb US cvb", "Full Name": "null vbvbn", "Email": "test@dhd.ch", "Sales Person": "LadyBoss Assistant", "Integration Initiated From": "Salesforce", "One Time Products Charge": "YES", "Salesforce Contact Link": "https://ladyboss--ashish.cs53.my.salesforce.com/0030j00000FQlyFAAT", "Fulfillment Product Name": "SF-$1 Test Secondary Statement Descriptor|", "Street": "ghch", "City": "cvb", "State": "cvb", "Postal Code": "cvb", "Country": "US" }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "risk_level": "normal", "risk_score": 44, "seller_message": "Payment complete.", "type": "authorized" }, "paid": true, "payment_intent": null, "receipt_email": null, "receipt_number": null, "refunded": false, "refunds": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1DWQkqBwLSk1v1ohDs0Llngf/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1DWQkdBwLSk1v1ohsO9dM9o4", "object": "card", "address_city": "cvb", "address_country": "US", "address_line1": "ghch", "address_line1_check": "pass", "address_line2": null, "address_state": "cvb", "address_zip": "cvb", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_DyNVX8mwbZvu5F", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 1, "exp_year": 2021, "fingerprint": "eGF2QMV6K3vhvUL3", "funding": "credit", "last4": "4242", "metadata": { }, "name": "ghch", "tokenization_method": null }, "source_transfer": null, "statement_descriptor": "SF-$1 Test", "status": "succeeded", "transfer_group": null } ], "has_more": false, "url": "/v1/charges" }';
             return response  ;
        }
       return null;
        
    }
}