@isTest
public class StripeChargeUpdate_Service_Test {
  
 
    public static testMethod void test2(){
        ContactTriggerFlag.isContactBatchRunning = true;
        system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        test.startTest();
        Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        
   //     Stripe_Profile__c StripePro1 =new Stripe_Profile__c();
   //     StripePro1.Stripe_Customer_Id__c = 'cus_DlCwdwjlLiUHn1DxQ';
   //     //StripePro1.Customer__c = Con.id ;
   //     insert StripePro1 ;
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'UTA';
     
        insert Con;
        
        
        
        
        
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_DlCjlLiUHn1DxQ';
    //    StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
      
        Stripe_Product_Mapping__c StripeProMap = new Stripe_Product_Mapping__c();
        StripeProMap.Name = 'test';
        StripeProMap.Salesforce__c='tst';
        StripeProMap.Stripe__c ='test';
        insert StripeProMap;
        
        Card__c Card =new Card__c();
     Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
     Card.Contact__c =Con.Id;
     insert Card ;
     
    Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.amount = 100;
         op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.RequestId__c = null;
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_DlCjlLiUHn1DxQ';
        op.Stripe_Charge_Id__c = '123455';
        op.Contact__c = Con.Id;
        op.Success_Failure_Message__c = 'charge.refunded';
        op.CardId__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij'; 
        op.Stripe_Message__c = 'Success';
        op.Paid__c = true ;
        op.Scheduled_Payment_Date__c =  null ;
        insert op;
        System.debug('===========Card========'+Card);
        //     System.debug('===========Opportunity========'+op);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{"id":"evt_1DOlP7BwLSk1v1ohHQcCsuBQ","object":"event","api_version":"2018-02-06","created":1540383845,"data":{"object":{"id":"123455","object":"charge","amount":350,"amount_refunded":308,"application":null,"application_fee":null,"balance_transaction":"txn_1DM9SABwLSk1v1ohaOd0qrxP","captured":true,"created":1539761305,"currency":"usd","customer":"cus_DnkxCMqkQZdc4G","description":"Big Fat Lies Book","destination":null,"dispute":null,"failure_code":null,"failure_message":null,"fraud_details":{},"invoice":null,"livemode":false,"metadata":{"Billing Address":"asd asd asd US asd","Full Name":"null Refund","Email":"refund@refund.refund","Sales Person":"Sukesh G","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss--Devs.cs90.my.salesforce.com/0031F0000089kXXQAY","Fulfillment Product Name":"Big Fat Lies Book Secondary Descriptor&#124;","Street":"asd","City":"asd","State":"asd","Postal Code":"asd","Country":"US"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"approved_by_network","reason":null,"risk_level":"normal","risk_score":23,"seller_message":"Payment complete.","type":"authorized"},"paid":true,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[{"id":"re_1DOlP6BwLSk1v1ohH8aYRn23","object":"refund","amount":1,"balance_transaction":"txn_1DOlP6BwLSk1v1ohxrgmbtwK","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540383844,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"has_more":false,"total_count":5,"url":"/v1/charges/ch_1DM9S9BwLSk1v1ohEJOwT6RZ/refunds"},"review":null,"shipping":null,"source":{"id":"card_1DM9RyBwLSk1v1oh7r4cRhJN","object":"card","address_city":"asd","address_country":"US","address_line1":"asd","address_line1_check":"pass","address_line2":null,"address_state":"asd","address_zip":"asd","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_DnkxCMqkQZdc4G","cvc_check":"pass","dynamic_last4":null,"exp_month":1,"exp_year":2035,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"asd","tokenization_method":null},"source_transfer":null,"statement_descriptor":"Big Fat Lies Boooooooo","status":"succeeded","transfer_group":null},"previous_attributes":{"amount_refunded":307,"refunds":{"data":[{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"total_count":4}}},"livemode":false,"pending_webhooks":4,"request":{"id":"req_xtlRnjiO3ZHBi3","idempotency_key":null},"type":"charge.refunded"} ';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        StripeChargeUpdate_Service.createCustomer();
    //    StripeCharge_Service.dummyCover();
        test.stopTest();         
    }
    public static testMethod void test3(){
        ContactTriggerFlag.isContactBatchRunning = true;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        test.startTest();
        Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        
   //     Stripe_Profile__c StripePro1 =new Stripe_Profile__c();
   //     StripePro1.Stripe_Customer_Id__c = 'cus_DlCwdwjlLiUHn1DxQ';
   //     //StripePro1.Customer__c = Con.id ;
   //     insert StripePro1 ;
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'UTA';
     
        insert Con;
        
        
        
        
        
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_DlCjlLiUHn1DxQ';
    //    StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
      
        Stripe_Product_Mapping__c StripeProMap = new Stripe_Product_Mapping__c();
        StripeProMap.Name = 'test';
        StripeProMap.Salesforce__c='tst';
        StripeProMap.Stripe__c ='test';
        insert StripeProMap;
        
        Card__c Card =new Card__c();
     Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
     Card.Contact__c =Con.Id;
     insert Card ;
     
    Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.amount = 100;
         op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.RequestId__c = null;
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_DlCjlLiUHn1DxQ';
        op.Stripe_Charge_Id__c = '123455';
        op.Contact__c = Con.Id;
        op.Success_Failure_Message__c = 'charge.refunded';
        op.CardId__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij'; 
        op.Stripe_Message__c = 'Success';
        op.Paid__c = true ;
        op.Scheduled_Payment_Date__c =  null ;
        insert op;
          
        Product2 prod = new Product2(Name = 'Transformation System-$237', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
 
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
               System.debug('===========standardPrice========'+customPB.IsStandard);
 
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2;
        ol.refund__c =22;
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_DlAEjFZvBklMuQ';
        ol.refund__c = 137;
        insert ol;
        
        System.debug('===========Card========'+Card);
        //     System.debug('===========Opportunity========'+op);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{"id":"evt_1DOlP7BwLSk1v1ohHQcCsuBQ","object":"event","api_version":"2018-02-06","created":1540383845,"data":{"object":{"id":"123455","object":"charge","amount":350,"amount_refunded":308,"application":null,"application_fee":null,"balance_transaction":"txn_1DM9SABwLSk1v1ohaOd0qrxP","captured":true,"created":1539761305,"currency":"usd","customer":"cus_DnkxCMqkQZdc4G","description":"Big Fat Lies Book","destination":null,"dispute":null,"failure_code":null,"failure_message":null,"fraud_details":{},"invoice":null,"livemode":false,"metadata":{"Billing Address":"asd asd asd US asd","Full Name":"null Refund","Email":"refund@refund.refund","Sales Person":"Sukesh G","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss--Devs.cs90.my.salesforce.com/0031F0000089kXXQAY","Fulfillment Product Name":"Big Fat Lies Book Secondary Descriptor&#124;","Street":"asd","City":"asd","State":"asd","Postal Code":"asd","Country":"US"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"approved_by_network","reason":null,"risk_level":"normal","risk_score":23,"seller_message":"Payment complete.","type":"authorized"},"paid":true,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[{"id":"re_1DOlP6BwLSk1v1ohH8aYRn23","object":"refund","amount":1,"balance_transaction":"txn_1DOlP6BwLSk1v1ohxrgmbtwK","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540383844,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"has_more":false,"total_count":5,"url":"/v1/charges/ch_1DM9S9BwLSk1v1ohEJOwT6RZ/refunds"},"review":null,"shipping":null,"source":{"id":"card_1DM9RyBwLSk1v1oh7r4cRhJN","object":"card","address_city":"asd","address_country":"US","address_line1":"asd","address_line1_check":"pass","address_line2":null,"address_state":"asd","address_zip":"asd","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_DnkxCMqkQZdc4G","cvc_check":"pass","dynamic_last4":null,"exp_month":1,"exp_year":2035,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"asd","tokenization_method":null},"source_transfer":null,"statement_descriptor":"Big Fat Lies Boooooooo","status":"succeeded","transfer_group":null},"previous_attributes":{"amount_refunded":307,"refunds":{"data":[{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"total_count":4}}},"livemode":false,"pending_webhooks":4,"request":{"id":"req_xtlRnjiO3ZHBi3","idempotency_key":null},"type":"charge.refunded"} ';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        StripeChargeUpdate_Service.createCustomer();
    //    StripeCharge_Service.dummyCover();
        test.stopTest();         
    }
    public static testMethod void test4(){
        ContactTriggerFlag.isContactBatchRunning = true;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        test.startTest();
        Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        
   //     Stripe_Profile__c StripePro1 =new Stripe_Profile__c();
   //     StripePro1.Stripe_Customer_Id__c = 'cus_DlCwdwjlLiUHn1DxQ';
   //     //StripePro1.Customer__c = Con.id ;
   //     insert StripePro1 ;
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'UTA';
     
        insert Con;
        
        
        
        
        
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_DlCjlLiUHn1DxQ';
    //    StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
      
        Stripe_Product_Mapping__c StripeProMap = new Stripe_Product_Mapping__c();
        StripeProMap.Name = 'test';
        StripeProMap.Salesforce__c='tst';
        StripeProMap.Stripe__c ='test';
        insert StripeProMap;
        
        Card__c Card =new Card__c();
     Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
     Card.Contact__c =Con.Id;
     insert Card ;
     
    Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.amount = 100;
         op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.RequestId__c = null;
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_DlCjlLiUHn1DxQ';
        op.Stripe_Charge_Id__c = '123455';
        op.Contact__c = Con.Id;
        op.Success_Failure_Message__c = 'charge.refunded';
        op.CardId__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij'; 
        op.Stripe_Message__c = 'Success';
        op.Paid__c = true ;
        op.Scheduled_Payment_Date__c =  null ;
        insert op;
          
        Product2 prod = new Product2(Name = 'Transformation System-$237', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
 
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
               System.debug('===========standardPrice========'+customPB.IsStandard);
 
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2;
        ol.refund__c =22;
        ol.Stripe_charge_id__c='123455';
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_DlAEjFZvBklMuQ';
        ol.refund__c = 137;
        insert ol;
        
        System.debug('===========Card========'+Card);
        //     System.debug('===========Opportunity========'+op);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{"id":"evt_1DOlP7BwLSk1v1ohHQcCsuBQ","object":"event","api_version":"2018-02-06","created":1540383845,"data":{"object":{"id":"123455","object":"charge","amount":350,"amount_refunded":308,"application":null,"application_fee":null,"balance_transaction":"txn_1DM9SABwLSk1v1ohaOd0qrxP","captured":true,"created":1539761305,"currency":"usd","customer":"cus_DnkxCMqkQZdc4G","description":"Big Fat Lies Book","destination":null,"dispute":null,"failure_code":null,"failure_message":null,"fraud_details":{},"invoice":null,"livemode":false,"metadata":{"Billing Address":"asd asd asd US asd","Full Name":"null Refund","Email":"refund@refund.refund","Sales Person":"Sukesh G","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss--Devs.cs90.my.salesforce.com/0031F0000089kXXQAY","Fulfillment Product Name":"Big Fat Lies Book Secondary Descriptor&#124;","Street":"asd","City":"asd","State":"asd","Postal Code":"asd","Country":"US"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"approved_by_network","reason":null,"risk_level":"normal","risk_score":23,"seller_message":"Payment complete.","type":"authorized"},"paid":true,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[{"id":"re_1DOlP6BwLSk1v1ohH8aYRn23","object":"refund","amount":1,"balance_transaction":"txn_1DOlP6BwLSk1v1ohxrgmbtwK","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540383844,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"has_more":false,"total_count":5,"url":"/v1/charges/ch_1DM9S9BwLSk1v1ohEJOwT6RZ/refunds"},"review":null,"shipping":null,"source":{"id":"card_1DM9RyBwLSk1v1oh7r4cRhJN","object":"card","address_city":"asd","address_country":"US","address_line1":"asd","address_line1_check":"pass","address_line2":null,"address_state":"asd","address_zip":"asd","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_DnkxCMqkQZdc4G","cvc_check":"pass","dynamic_last4":null,"exp_month":1,"exp_year":2035,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"asd","tokenization_method":null},"source_transfer":null,"statement_descriptor":"Big Fat Lies Boooooooo","status":"succeeded","transfer_group":null},"previous_attributes":{"amount_refunded":307,"refunds":{"data":[{"id":"re_1DOkz7BwLSk1v1ohvezoSnZ6","object":"refund","amount":2,"balance_transaction":"txn_1DOkz7BwLSk1v1ohhcGTzInl","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382233,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DOkymBwLSk1v1oh2QlxNhhm","object":"refund","amount":2,"balance_transaction":"txn_1DOkymBwLSk1v1ohhyxCWnbp","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1540382212,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DMGC8BwLSk1v1ohOZ6zs6wl","object":"refund","amount":300,"balance_transaction":"txn_1DMGC8BwLSk1v1oh3qV7BraL","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539787220,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"},{"id":"re_1DM9SsBwLSk1v1ohz3aMBfGR","object":"refund","amount":3,"balance_transaction":"txn_1DM9SsBwLSk1v1ohm0RR9ojS","charge":"ch_1DM9S9BwLSk1v1ohEJOwT6RZ","created":1539761350,"currency":"usd","metadata":{},"reason":"duplicate","receipt_number":null,"source_transfer_reversal":null,"status":"succeeded"}],"total_count":4}}},"livemode":false,"pending_webhooks":4,"request":{"id":"req_xtlRnjiO3ZHBi3","idempotency_key":null},"type":"charge.refunded"} ';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        StripeChargeUpdate_Service.createCustomer();
    //    StripeCharge_Service.dummyCover();
        test.stopTest();         
    }public static testMethod void test5(){
        ContactTriggerFlag.isContactBatchRunning = true;
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        test.startTest();
        Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        
        //     Stripe_Profile__c StripePro1 =new Stripe_Profile__c();
        //     StripePro1.Stripe_Customer_Id__c = 'cus_DlCwdwjlLiUHn1DxQ';
        //     //StripePro1.Customer__c = Con.id ;
        //     insert StripePro1 ;
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'UTA';
        
        insert Con;
        
        
        
        
        
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_DlCjlLiUHn1DxQ';
        //    StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        Stripe_Product_Mapping__c StripeProMap = new Stripe_Product_Mapping__c();
        StripeProMap.Name = 'test';
        StripeProMap.Salesforce__c='tst';
        StripeProMap.Stripe__c ='test';
        insert StripeProMap;
        
        Card__c Card =new Card__c();
        Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
        Card.Contact__c =Con.Id;
        insert Card ;
        Address__c add = new Address__c();
        add.Primary__c = true;
        add.Contact__c = Con.id;
        insert add;
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.amount = 100;
        op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.RequestId__c = null;
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_DlCjlLiUHn1DxQ';
        op.Stripe_Charge_Id__c = 'ch_1FvjJAFzCf73siP0MrDWBtZn';
        op.Contact__c = Con.Id;
        op.Success_Failure_Message__c = 'charge.refunded';
        op.CardId__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij'; 
        op.Stripe_Message__c = 'Success';
        op.Paid__c = true ;
        op.Scheduled_Payment_Date__c =  null ;
        op.RecordTypeId = devRecordTypeId;
        insert op;
        
        Product2 prod = new Product2(Name = 'Transformation System-$237', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        System.debug('===========standardPrice========'+customPB.IsStandard);
        
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2;
        ol.refund__c =22;
        ol.Stripe_charge_id__c='ch_1FvjJAFzCf73siP0MrDWBtZn';
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_DlAEjFZvBklMuQ';
        ol.refund__c = 137;
        insert ol;
        
        System.debug('===========Card========'+Card);
        //     System.debug('===========Opportunity========'+op);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{ "id": "evt_1FvjKoFzCf73siP0KgbgXUXB", "object": "event", "api_version": "2018-02-28", "created": 1577793386, "data": { "object": { "id": "ch_1FvjJAFzCf73siP0MrDWBtZn", "object": "charge", "amount": 208, "amount_refunded": 208, "application": null, "application_fee": null, "application_fee_amount": null, "balance_transaction": "txn_1FvjJBFzCf73siP0QlHzThXR", "billing_details": { "address": { "city": "Albuquerque", "country": "US", "line1": "10010 Indian School Rd. NE", "line2": null, "postal_code": "87112", "state": "NM" }, "email": null, "name": "Grant Christopher", "phone": null }, "captured": true, "created": 1577793284, "currency": "usd", "customer": "cus_GNW2nfts2UXpC9", "description": "zTest $2 LABS", "destination": null, "dispute": null, "disputed": false, "failure_code": null, "failure_message": null, "fraud_details": { }, "invoice": null, "livemode": true, "metadata": { "Shipping Address": "10010 Indian School Rd. NE Albuquerque NM US 87112", "Full Name": "Grant Taxandplantest5", "Email": "grant+taxandplantest5@ladyboss.com", "Phone": "5055961895", "Tax": "0.08", "TaxPercentage": "3.9375", "Core Amount": "2.00", "Sales Person": "Jay Patel", "Integration Initiated From": "Salesforce", "One Time Products Charge": "YES", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/0032S00002A15FPQAZ", "Fulfillment Product Name": "TEST $2 ProductLABS|TEST $3 ProductLABS|", "Street": "10010 Indian School Rd. NE", "City": "Albuquerque", "State": "NM", "Postal Code": "87112", "Country": "US" }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "approved_by_network", "reason": null, "risk_level": "normal", "risk_score": 0, "seller_message": "Payment complete.", "type": "authorized" }, "paid": true, "payment_intent": null, "payment_method": "card_1Fql0UFzCf73siP0sX7ESVXM", "payment_method_details": { "card": { "brand": "amex", "checks": { "address_line1_check": "pass", "address_postal_code_check": "pass", "cvc_check": null }, "country": "US", "exp_month": 8, "exp_year": 2023, "fingerprint": "Zp34Ghmo06R08KxI", "funding": "credit", "installments": null, "last4": "2012", "network": "amex", "three_d_secure": null, "wallet": null }, "type": "card" }, "receipt_email": null, "receipt_number": null, "receipt_url": "https://pay.stripe.com/receipts/acct_157hCJFzCf73siP0/ch_1FvjJAFzCf73siP0MrDWBtZn/rcpt_GSecn26XlFSwlJjF0Hpy3FNY3sPcg8g", "refunded": true, "refunds": { "object": "list", "data": [ { "id": "re_1FvjKoFzCf73siP0u1B3mbMC", "object": "refund", "amount": 208, "balance_transaction": "txn_1FvjKoFzCf73siP0krl4kqcz", "charge": "ch_1FvjJAFzCf73siP0MrDWBtZn", "created": 1577793386, "currency": "usd", "metadata": { }, "payment_intent": null, "reason": "duplicate", "receipt_number": "3556-1250", "source_transfer_reversal": null, "status": "succeeded", "transfer_reversal": null } ], "has_more": false, "total_count": 1, "url": "/v1/charges/ch_1FvjJAFzCf73siP0MrDWBtZn/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1Fql0UFzCf73siP0sX7ESVXM", "object": "card", "address_city": "Albuquerque", "address_country": "US", "address_line1": "10010 Indian School Rd. NE", "address_line1_check": "pass", "address_line2": null, "address_state": "NM", "address_zip": "87112", "address_zip_check": "pass", "brand": "American Express", "country": "US", "customer": "cus_GNW2nfts2UXpC9", "cvc_check": null, "dynamic_last4": null, "exp_month": 8, "exp_year": 2023, "fingerprint": "Zp34Ghmo06R08KxI", "funding": "credit", "last4": "2012", "metadata": { }, "name": "Grant Christopher", "tokenization_method": null }, "source_transfer": null, "statement_descriptor": "TEST $2 ProductLABS", "statement_descriptor_suffix": null, "status": "succeeded", "transfer_data": null, "transfer_group": null }, "previous_attributes": { "amount_refunded": 0, "refunded": false, "refunds": { "data": [ ], "total_count": 0 } } }, "livemode": true, "pending_webhooks": 9, "request": { "id": "req_MUptlOo1Td3xs1", "idempotency_key": null }, "type": "charge.refunded" }';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        StripeChargeUpdate_Service.createCustomer();
        //    StripeCharge_Service.dummyCover();
        test.stopTest();         
    }
}