global class StripeCard_PostUpdateBatch  implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
    
  
   global database.querylocator start(database.batchablecontext bc){
       string query='select id,CustomerID__c,Contact__c  from card__c where  CustomerID__c != null  AND Stripe_Profile__c=\'\' limit 100000';
       return database.getquerylocator(query);   
   }
   global void execute(database.BatchableContext bc,list<card__c > stripeProfileList){
       List<card__c> lstp = updateCard(stripeProfileList);   
       if(lstp.size() > 0 ) { update lstp;}
        
   }
   global void finish(database.batchablecontext bc){
       
   }
      public static List<Card__c> updateCard(List<Card__c> listCard) {
        try{
        set<String> idSet = new set<String>();
        Map<string,string> mapStripeProf = new Map<string,string> ();
        Map<string,string> mapCon = new Map<string,string> ();
        for (Card__c cd : listCard) {
            idSet.add(cd.CustomerID__c);
        }
        List<Stripe_Profile__c> lstStripeProfile = [select id,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c = : idSet];
        for(Stripe_Profile__c sp : lstStripeProfile ) {
            mapStripeProf.put(sp.Stripe_Customer_Id__c,sp.id);
            mapCon.put(sp.Stripe_Customer_Id__c,sp.Customer__c );
        }
         for (Card__c cd : listCard) {
            if(mapStripeProf.ContainsKey(cd.CustomerID__c)) {
                cd.Stripe_Profile__c  =mapStripeProf.get(cd.CustomerID__c);
            }
            if(mapCon.ContainsKey(cd.CustomerID__c)) {
                cd.Contact__c=mapCon.get(cd.CustomerID__c);
            }
           
        }
        return listCard;
        }
        catch(exception ex) {return null;}
    }
   
   

  }