@isTest
public class PlanStripeWebHook_Test {
    @isTest static void test(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BcOxelyfXsr0Ki');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test opp',CloseDate=system.today(),StageName='Qualification',External_ID__c='inv_123456789');
        insert opp;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/PlanStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{'+
		'  \"created\": 1326853478,'+
		'  \"livemode\": false,'+
		'  \"id\": \"evt_00000000000000\",'+
		'  \"type\": \"plan.created\",'+
		'  \"object\": \"event\",'+
		'  \"request\": null,'+
		'  \"pending_webhooks\": 1,'+
		'  \"api_version\": \"2017-08-15\",'+
		'  \"data\": {'+
		'    \"object\": {'+
		'      \"id\": \"test1_00000000000000\",'+
		'      \"object\": \"plan\",'+
		'      \"amount\": 2500,'+
		'      \"created\": 1508947075,'+
		'      \"currency\": \"usd\",'+
		'      \"interval\": \"month\",'+
		'      \"interval_count\": 1,'+
		'      \"livemode\": false,'+
		'      \"metadata\": {'+
		'      },'+
		'      \"name\": \"Test Product 1\",'+
		'      \"statement_descriptor\": null,'+
		'      \"trial_period_days\": null'+
		'    }'+
		'  }'+
		'}';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	PlanStripeWebHook.PlanStripe();
        Test.stopTest();
    }
}