@isTest 
public class ChargeStripeBatchServiceTest {
  
    static testMethod void testSubscription(){
        Account acc = new Account();
         acc.Name = 'testAcc';
         acc.Credit_Card_Number__c = '8327' ;
         acc.Exp_Month__c = '6';
         acc.Exp_Year__c = 2040;
             
        contact con = new contact();
         con.LastName = 'vikas';
         insert con;
        
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        insert cd;
        
      Stripe_Profile__c stpro = new Stripe_Profile__c();
        stpro.Customer__c = con.id;
        stpro.Subscription_Processed__c = false;
         stpro.Stripe_Customer_Id__c = 'cus_DyNVX8mwbZvu5F';
        insert stpro;
        update stpro;
         Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = con.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        ol.refund__c = 137;
        insert ol;
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
        
      
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
       Test.startTest();
        
        //ChargeStripeBatchService s = new ChargeStripeBatchService();
       //Database.executeBatch(s,1);
     
       Test.stopTest();
 
    }
    
    
  
    static testMethod void testSubscription2(){
        Account acc = new Account();
         acc.Name = 'testAcc';
         acc.Credit_Card_Number__c = '8327' ;
         acc.Exp_Month__c = '6';
         acc.Exp_Year__c = 2040;
             
        contact con = new contact();
         con.LastName = 'vikas';
         insert con;
        
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        insert cd;
        
      Stripe_Profile__c stpro = new Stripe_Profile__c();
        stpro.Customer__c = con.id;
        stpro.Subscription_Processed__c = false;
         stpro.Stripe_Customer_Id__c = 'cus_DyNVX8mwbZvu5F';
        insert stpro;
        update stpro;
         Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Stripe_Charge_Id__c= 'ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = con.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        OpportunityLineItem ol = new OpportunityLineItem();
        ol.Stripe_charge_id__c = 'ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 139;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        ol.refund__c = 137;
        insert ol;
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
        Batch_Soql_Limit__c b = new Batch_Soql_Limit__c();
        b.Size__c =1;
        //insert b;
      
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
       Test.startTest();
        chargeHandler.getAllPurchases('cus_DyNVX8mwbZvu5F');
        RefundedChargeBatchService s = new RefundedChargeBatchService();
       Database.executeBatch(s,1);
     
       Test.stopTest();
 
    }
    
  
    static testMethod void testSubscription3(){
        Account acc = new Account();
         acc.Name = 'testAcc';
         acc.Credit_Card_Number__c = '8327' ;
         acc.Exp_Month__c = '6';
         acc.Exp_Year__c = 2040;
             
        contact con = new contact();
         con.LastName = 'vikas';
         insert con;
        
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        insert cd;
        
      Stripe_Profile__c stpro = new Stripe_Profile__c();
        stpro.Customer__c = con.id;
        stpro.Subscription_Processed__c = false;
         stpro.Stripe_Customer_Id__c = 'cus_DyNVX8mwbZvu5F';
        insert stpro;
        update stpro;
         Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Clubbed__c= true;
        opp.Stripe_Charge_Id__c= 'ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = con.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        OpportunityLineItem ol = new OpportunityLineItem();
        ol.Stripe_charge_id__c = 'ch_1DWkgPBwLSk1v1oh9LMBcL3p';
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 139;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        ol.refund__c = 137;
        insert ol;
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
        Batch_Soql_Limit__c b = new Batch_Soql_Limit__c();
        b.Size__c =1;
        //insert b;
      
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
       Test.startTest();
        chargeHandler.getAllPurchases('cus_DyNVX8mwbZvu5F');
        RefundedChargeBatchService s = new RefundedChargeBatchService();
       Database.executeBatch(s,1);
     
       Test.stopTest();
 
    }
 
}