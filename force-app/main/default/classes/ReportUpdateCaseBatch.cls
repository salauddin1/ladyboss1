// Created By : Sourabh Badole 
// Date       : 14-3-2019
global class ReportUpdateCaseBatch  implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select id,owner.Name,caseNumber,Charge_Id__c,isProcessedForReport__c,IsFailed_Payment__c, subject from case where isProcessedForReport__c != true and IsFailed_Payment__c != false and Charge_Id__c!=null  limit 10');
    }
    global void execute(Database.BatchableContext BC, List<Case> caseList){
        List<Stripe_Product_Mapping__c> msppingList =[select id,Salesforce__c,Salesforce_Product__c,Stripe__c,Stripe_Nick_Name__c from Stripe_Product_Mapping__c where salesforce__c IN ('Supplement','UTA','Unlimited') ];
        Map<String,String> sfDescMap = new Map<String,String>();
        Map<String,String> sfDescNickMap= new Map<String,String>();
        Map<String,String> sfDescNickMap1= new Map<String,String>();
        Set<String> sfSet = new  Set<String>();
        Set<String> sfNickSet = new  Set<String>();
        Set<String> failedCaseChargeId = new Set<String> ();
        Map<String,Case > failedCaseChargeMap = new Map<String,Case > ();
        
        for (Stripe_Product_Mapping__c mp : msppingList ) {
            sfDescMap.put(mp.Salesforce_Product__c,mp.Salesforce__c);
            sfSet.add(mp.Stripe__c);
            sfDescNickMap.put(mp.Stripe__c,mp.Salesforce__c );
            sfNickSet.add(mp.Stripe_Nick_Name__c);
            sfDescNickMap1.put(mp.Stripe_Nick_Name__c,mp.Salesforce__c );
        }
        
        for (Case cs: caseList ) {
            failedCaseChargeId.add(cs.Charge_Id__c);
            failedCaseChargeMap.put(cs.Charge_Id__c,cs);
        }
        
        
        //To get the failed all payments from the Stripe for put the product Category
        
        List<Case> caseListUpdate = new  List<Case>();
        
        for(String ids : failedCaseChargeMap.KeySet()) {
            if(ids != null && ids != ''){
                HttpRequest http = new HttpRequest();
                http.setEndpoint('https://api.stripe.com/v1/charges/'+ids);
                http.setMethod('GET');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
                String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                
                String response;
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                
                try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    System.debug('***Get Exception***'+e);
                }
                system.debug('####---StatusCode--- '+hs.getStatusCode());
                if(hs.getStatusCode()==200) {
                    response = hs.getBody();
                    if(response != null && response != ''){
                        system.debug('####---response--- '+response);
                        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                        
                        String statement_descriptor= String.valueOf(results.get('statement_descriptor'));
                        
                        Case cs = failedCaseChargeMap.get(ids);
                        if(cs != null){
                            system.debug('####---CaseId--- '+cs.id);
                            
                            cs.isProcessedForReport__c = true;
                            if(statement_descriptor != null && statement_descriptor != ''){
                                if(sfDescMap.containsKey(statement_descriptor)) {
                                    
                                    cs.Product_Category__c = sfDescMap.get(statement_descriptor);
                                }else if( sfDescNickMap1.containsKey(statement_descriptor) ) {
                                    cs.Product_Category__c = sfDescNickMap1.get(statement_descriptor);
                                } else if( sfDescNickMap.containsKey(statement_descriptor)) {
                                    cs.Product_Category__c = sfDescNickMap.get(statement_descriptor);
                                }
                            }
                            cs.Failed_Amount__c =  Integer.valueOf(results.get('amount'))*0.01;
                            caseListUpdate.add(cs);
                        }
                    }
                }
            }
        }
        try{
            if(caseListUpdate != null && !caseListUpdate.isEmpty()){
                update caseListUpdate;    
            }
        }
        catch(Exception ex){
            system.debug('####---Exception--- '+ex);
        }
        
    }
    global void finish(Database.BatchableContext BC){
    }
}