//Class with comp to update and delete subscription
public class ActiveSubscriptionLineItems {
//Method will ireturns the list of active subscriptions
@AuraEnabled
    public static List<OpportunityLineItem  > getOpportunityLineItem(String contactId ){
        List<OpportunityLineItem  > lstOpp = [SELECT Id, OpportunityId,name,product2.name,unitprice, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Opportunity.contact__c =:contactId  and (Success_Failure_Message__c !='customer.subscription.deleted' and Success_Failure_Message__c !='canceled') and Opportunity.recordType.Name='Subscription' and Subscription_Id__c !=null and Subscription_Id__c !=''];
       
        return lstOpp;
     } 
    
    @future(callout=true)
    public static void cancelSubscriptionFuture(String opplineItem){
        cancelSubscription(opplineItem);
    }
    //Method to cancel the subscription, if opp line item exists
      @AuraEnabled
    public static string  cancelSubscription(String opplineItem){
        //SOQL for selected oli
        List<OpportunityLineItem> oli = [select id,Subscription_Id__c,opportunityId from OPPORTUNITYLINEITEM WHERE id=:opplineItem limit 1];
        Opportunity opp = [select id,Success_Failure_Message__c from Opportunity where Id =:oli[0].opportunityId];
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli[0].Subscription_Id__c);
        String returnSucess='';
        //Delete method
        http.setMethod('DELETE');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        try{
        //Send the request
        hs = con.send(http);
        } 
        catch(Exception e) {
         System.debug('------------ Exception---------------'+e);   
        }
        Integer statusCode = hs.getStatusCode();
        //if response is success
        if (hs.getstatusCode() == 200){
            //Deserialise the JSON and get the status and update the oli
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            if(String.valueOf(results.get('status')).Contains('cancelled')) {
                oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
            }else 
            {
            oli[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
            }
            oli[0].Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));
            update oli; 
            
               returnSucess= 'Subscription cancelled in stripe';
            
        }else{
           
               //If not succrss or error then update the status with subscription deleted message
              Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
             Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
            returnSucess= String.valueOf(errorMap.get('message'));
           if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                update oli; 
            }
        
        }
        return returnSucess;
    } 
    //Method to invoke the subscriptio with datevalue and proration for oli
     @AuraEnabled
    public static string updateSubscription(String LineItemId,String dateVal,Boolean checkVal){
      
        String returnSucess='';
      
        Integer noOfDays ;
        if(dateVal!=null){
            noOfDays = Date.Today().daysBetween(Date.valueOf(dateVal));
         }
         //get oli
        List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        List<Opportunity> opp = [select id,Success_Failure_Message__c from Opportunity where Id =:oppData[0].opportunityId];
        HttpRequest http = new HttpRequest();  
        //Set the Subscription ID on the End point URL
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppData[0].Subscription_Id__c);        
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String bodyVal;
        Boolean prorateBool = false;
        //bodyVal = '&quantity='+qty+'&prorate='+prorateBool;
        //Set the string with prorate as false
        bodyVal = '&prorate='+prorateBool;
        if(dateVal!=null){
            
            //to move the next anchor date or charging date
            List<String> dateParts = dateVal.split('-');
            Datetime dateWithTime = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
            dateVal = dateWithTime.format('yyyy-MM-dd HH:mm:ss');
            
            Datetime d = Datetime.valueOf(dateVal);
            d= d.AddDays(1);
            d= d.AddDays(-1);
            Long l = d.getTime();
            system.debug(l);
            Long x = (Long)Datetime.valueOf(l).getTime();
            String a = string.valueOf(x);
            String l1= a.substring(0,a.length()-3);
            
            bodyVal += '&trial_end='+l1;
        }
        
        bodyVal += '&';
        System.debug(bodyVal );
        //set the request body
        http.setBody(bodyVal );
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        //send response
        hs = con.send(http);
        if (hs.getstatusCode() == 200){
            StripeListSubscriptionClone.Charges varCharges;
            JSONParser parse;
            system.debug('#### '+ 1);
            //if the response has success then return the success message
            
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            oppData[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
            update oppData; 
            returnSucess = 'Subscription updated in stripe';
            
            
        }else{
        //If the response contains the error then update status and return the error messsage.
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
             Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
            returnSucess= String.valueOf(errorMap.get('message'));
            if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                oppData[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                update oppData; 
            }
        }
        
        system.debug('#### '+ hs.getBody());
        return returnSucess;
    }
     @AuraEnabled
    public static string updateOpportunitySubscription(String oppId,String dateVal){
        String returnSucess='';
      
        Integer noOfDays ;
        if(dateVal!=null){
            noOfDays = Date.Today().daysBetween(Date.valueOf(dateVal));
         }
        
        List<Opportunity> opp = [select id,Subscription__c,Success_Failure_Message__c from Opportunity where Id =:oppId LIMIT 1] ;
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+opp[0].Subscription__c);        
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String bodyVal;
        Boolean prorateBool = false;
        //bodyVal = '&quantity='+qty+'&prorate='+prorateBool;
        bodyVal = '&prorate='+prorateBool;
        if(dateVal!=null){
            
            
            List<String> dateParts = dateVal.split('-');
            Datetime dateWithTime = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
            dateVal = dateWithTime.format('yyyy-MM-dd HH:mm:ss');
            
            Datetime d = Datetime.valueOf(dateVal);
            d= d.AddDays(1);
            d= d.AddDays(-1);
            Long l = d.getTime();
            system.debug(l);
            Long x = (Long)Datetime.valueOf(l).getTime();
            String a = string.valueOf(x);
            String l1= a.substring(0,a.length()-3);
            
            bodyVal += '&trial_end='+l1;
        }
        
        bodyVal += '&';
        System.debug(bodyVal );
        http.setBody(bodyVal );
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        hs = con.send(http);
        if (hs.getstatusCode() == 200){
            StripeListSubscriptionClone.Charges varCharges;
            JSONParser parse;
            
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            opp[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
            
            update opp; 
            
            returnSucess = 'Subscription updated in stripe';
            
            
        }else{
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
             Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
            returnSucess= String.valueOf(errorMap.get('message'));
            if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                opp[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                update opp; 
            }
        }
        
        system.debug('#### '+ hs.getBody());
        return returnSucess;
    }
    //To cancel the subscription provided the opportunity ID,If oli not created
    @AuraEnabled
    public static string  cancelOpportunitySubscription(String oppId){
    //Get the opportunity
        List<Opportunity> opp = [select id,Success_Failure_Message__c,Subscription__c from Opportunity where Id =:oppId];
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+opp[0].Subscription__c);
        String returnSucess='';
        //Set the delete method
        http.setMethod('DELETE');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        try{
        //Send the request
        hs = con.send(http);
        } catch(Exception e) {}
        Integer statusCode = hs.getStatusCode();
        //If response is success
        if (hs.getstatusCode() == 200){
        //Put the respose in map
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            if(String.valueOf(results.get('status')).Contains('cancelled ')) {
            //If response contains canceled then update
                opp[0].Success_Failure_Message__c = 'customer.subscription.deleted';
            }else 
            {
                opp[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
            }
            
            update opp; 
            //return the status
               returnSucess= 'Subscription cancelled in stripe';
            
        }else{
           
            //If Contains error then return the error message
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
            returnSucess= String.valueOf(errorMap.get('message'));
            if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                opp[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                update opp; 
            }
        
        }
        return returnSucess;
    } 
    //To get the Opporunity and Oli 
     @AuraEnabled
    public static List<Opportunity>  getAllOpp(String recordId){
    List<Opportunity> allOppList = new  List<Opportunity>();
        List<Opportunity> allOppList1 = [select id,name,Amount,(select id,Product2.name,Product2.Price__c from OpportunityLineItems),Card__r.CustomerID__c from Opportunity where Contact__c =: recordId and (Success_Failure_Message__c !='customer.subscription.deleted' and Success_Failure_Message__c !='canceled') and Subscription__c !=null and Subscription__c !=''];
        for(Opportunity op : allOppList1 ) {
            if(op.OpportunityLineItems.size() ==0) {
            allOppList.add(op);
            }
        }
        return allOppList;
    }
  
}