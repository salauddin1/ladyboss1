public class five9calloutforcontactVCC {
    public String endpoint_x ='https://api.five9.com:443/wsadmin/AdminWebService';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://ws-i.org/profiles/basic/1.1/xsd', 'wsIOrgProfilesBasic11XsdV2', 'http://service.admin.ws.five9.com/', 'serviceAdminWsFive9ComV2', 'http://jaxb.dev.java.net/array', 'jaxbDevJavaNetArrayV2'};
		public Integer removeNumbersFromDnc(String[] numbers) {
            five9calloutforcontactVCC.removeNumbersFromDnc request_x = new five9calloutforcontactVCC.removeNumbersFromDnc();
            request_x.numbers = numbers;
            five9calloutforcontactVCC.removeNumbersFromDncResponse response_x;
            Map<String, five9calloutforcontactVCC.removeNumbersFromDncResponse> response_map_x = new Map<String, five9calloutforcontactVCC.removeNumbersFromDncResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.admin.ws.five9.com/',
              'removeNumbersFromDnc',
              'http://service.admin.ws.five9.com/',
              'removeNumbersFromDncResponse',
              'five9calloutforcontactVCC.removeNumbersFromDncResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    public Integer addNumbersToDnc(String[] numbers) {
            five9calloutforcontactVCC.addNumbersToDnc request_x = new five9calloutforcontactVCC.addNumbersToDnc();
            request_x.numbers = numbers;
            five9calloutforcontactVCC.addNumbersToDncResponse response_x;
            Map<String, five9calloutforcontactVCC.addNumbersToDncResponse> response_map_x = new Map<String, five9calloutforcontactVCC.addNumbersToDncResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://service.admin.ws.five9.com/',
              'addNumbersToDnc',
              'http://service.admin.ws.five9.com/',
              'addNumbersToDncResponse',
              'five9calloutforcontactVCC.addNumbersToDncResponse'}
            );
            response_x = response_map_x.get('response_x');
            System.debug('========================'+response_x);
            return response_x.return_x;
        }
    public class addNumbersToDncResponse {
        public Integer return_x;
        @TestVisible private String[] return_x_type_info = new String[]{'return','http://service.admin.ws.five9.com/',null,'1','1','false'};
        @TestVisible private String[] apex_schema_type_info = new String[]{'http://service.admin.ws.five9.com/','false','false'};
        @TestVisible private String[] field_order_type_info = new String[]{'return_x'};
    }
     public class addNumbersToDnc {
        public String[] numbers;
        private String[] numbers_type_info = new String[]{'numbers','http://service.admin.ws.five9.com/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.admin.ws.five9.com/','false','false'};
        private String[] field_order_type_info = new String[]{'numbers'};
    }
    public class removeNumbersFromDnc {
        public String[] numbers;
        private String[] numbers_type_info = new String[]{'numbers','http://service.admin.ws.five9.com/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://service.admin.ws.five9.com/','false','false'};
        private String[] field_order_type_info = new String[]{'numbers'};
    }
    public class removeNumbersFromDncResponse {
        public Integer return_x;
        @TestVisible private String[] return_x_type_info = new String[]{'return','http://service.admin.ws.five9.com/',null,'1','1','false'};
        @TestVisible private String[] apex_schema_type_info = new String[]{'http://service.admin.ws.five9.com/','false','false'};
        @TestVisible private String[] field_order_type_info = new String[]{'return_x'};
    }
}