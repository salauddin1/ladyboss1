@isTest
public class HMAC_Test {
    
    public static TestMethod void testMethod1(){
        WebHookSecrets__c wb = new WebHookSecrets__c();
        wb.name = 'Stripe_FailedChargeService';
        wb.Webhook_Name__c = 'Stripe_FailedChargeService';
        wb.Webhook_Secret__c = 'whsec_cIdDwVu0vpFqtbk13jL35lUQ8RhG2zHS';
        insert wb;
    
       RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        //req.requestURI = '/services/apexrest/Stripe_CardService'; 
          String body = '{"id":"evt_1DmH36FzCf73siP0NSNIeFMv","object":"event","api_version":"2018-02-28","created":1545987032,"data":{"object":{"id":"ch_1DmH34FzCf73siP045FCerlh","object":"charge","amount":100,"amount_refunded":0,"application":null,"application_fee":null,"balance_transaction":null,"captured":false,"created":1545987030,"currency":"usd","customer":"cus_DCX5lXm1Qz7eHB","description":"zTest $1 LABS","destination":null,"dispute":null,"failure_code":"card_declined","failure_message":"Your card was declined.","fraud_details":{},"invoice":null,"livemode":true,"metadata":{"Shipping Address":"asdfsad tsare asdfs US asdfds","Full Name":"Grant Ashishtest12345","Email":"grant+ashishtest12345@ladyboss.com","Phone":"234-234-2342","Sales Person":"Tirth Patel","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss.my.salesforce.com/003f400000RYdktAAD","Fulfillment Product Name":"TEST $1 ProductLABS|","Street":"tsare","City":"asdfsad","State":"asdfs","Postal Code":"asdfds","Country":"US"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"declined_by_network","reason":"do_not_honor","risk_level":"normal","risk_score":6,"seller_message":"The bank returned the decline code `do_not_honor`.","type":"issuer_declined"},"paid":false,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/charges/ch_1DmH34FzCf73siP045FCerlh/refunds"},"review":null,"shipping":null,"source":{"id":"card_1Cm81IFzCf73siP04duSJyUB","object":"card","address_city":"asdfsad","address_country":"US","address_line1":"tsare","address_line1_check":"fail","address_line2":null,"address_state":"asdfs","address_zip":"asdfds","address_zip_check":"pass","brand":"American Express","country":"US","customer":"cus_DCX5lXm1Qz7eHB","cvc_check":null,"dynamic_last4":null,"exp_month":7,"exp_year":2022,"fingerprint":"KfQdByKUuKuf6iEL","funding":"credit","last4":"1014","metadata":{},"name":"tsare","tokenization_method":null},"source_transfer":null,"statement_descriptor":"TEST $1 ProductLABS","status":"failed","transfer_group":null}},"livemode":true,"pending_webhooks":7,"request":{"id":"req_PMRjQvvmXocntc","idempotency_key":null},"type":"charge.failed"}';
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Stripe-Signature', 't=1545993867,v1=b1c986fdd2d1297455f557fa232c4a84cb363fad62b42be6354f700554697904');
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
        
        Test.startTest(); 
       HMAC.generateSignature('t=1545993867,v1=b1c986fdd2d1297455f557fa232c4a84cb363fad62b42be6354f700554697904', 'Stripe_FailedChargeService');
        
        Test.stopTest(); 
        
    }
    
}