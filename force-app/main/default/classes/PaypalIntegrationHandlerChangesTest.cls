@isTest
public  class PaypalIntegrationHandlerChangesTest {
    static testMethod void testMethodPostive1WithoutAsync() 
    { 
        
        Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 0;
        insert apex;
        
		ContactTriggerFlag.isContactBatchRunning=true;
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;
        
        
        Map<String,Opportunity> oomap = new Map<String,Opportunity>();
        Map<String,List<OpportunityLineItem>> olmap = new Map<String,List<OpportunityLineItem>>();
        List<OpportunityLineItem> oplinelist = new List<OpportunityLineItem>();
        List<Opportunity> oplist = new List<Opportunity>();
        
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        oplist.add(op);
        
        oomap.put('club', op);
        //insert oomap;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.TotalPrice = 2;
        ol.PricebookEntryId = customPrice.Id;
        
        insert ol;
        
        oplinelist.add(ol);
        
        olmap.put('club', oplinelist);
        
        set<id> idset = new set<id>();
        idset.add(op.id);
        
        Paypal_Billing_Agreement__c pba = new Paypal_Billing_Agreement__c();
        pba.Billing_Agreement__c = 'billingAgreement';
        insert pba;
        PaypalAPICreds__c paypalApi = new PaypalAPICreds__c();
        paypalApi.Client_ID__c = 'test';
        paypalApi.Secret__c = 'test';
        paypalApi.Name = 'Test';
        insert paypalApi;

        PayPal_Subscription_Charge__c psc = new PayPal_Subscription_Charge__c();
		psc.Opportunity__c = ol.Opportunity_Name__c;
		psc.Subscribed_Product__c = ol.Product2Id;
		psc.Paypal_Transaction_Id__c = 'paypalT';
        insert psc;
        
        test.StartTest();
        PaypalIntegrationHandlerChanges.OpportunityPaypalHandler(pba.Id, oomap, olmap, false, 'salesPerson', 2.75, false, false);
        test.stopTest();
        
    }
}