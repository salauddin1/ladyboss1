global class OpportunityWithStipeChargeIdHandler {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/charges';
    
    public static void operationOnOpportunity(Map<id,Opportunity> idsAndOpportunityMap){
        //StripeCharge stCharge ;
        for(Opportunity opp : idsAndOpportunityMap.values()){
            createMetadataAndUpdate(opp);
        }
    }
    
    global static void createMetadataAndUpdate(Opportunity opp) {
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(addressList!=null && !addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
        }
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',BillingAddress);
        metadata.put('Full Name',opp.Contact__r.firstname+' '+opp.Contact__r.lastname);
        metadata.put('Email',opp.Contact__r.email);
        metadata.put('Phone',opp.Contact__r.phone); 
        if(opp.User_to_give_credit__c ==null){
            metadata.put('Sales Person',opp.Sales_Person__c);
        }else{
            User u = [SELECT Name FROM User WHERE Id = :opp.User_to_give_credit__c];
            metadata.put('Sales Person',u.Name);
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.Contact__r.id);
        metadata.put('Integration Initiated From','Salesforce');
       	if(addressList!=null && !addressList.isEmpty()){
        	metadata.put('Street',addressList[0].Shipping_Street__c);
        	metadata.put('City',addressList[0].Shipping_City__c);
	        metadata.put('State',addressList[0].Shipping_State_Province__c);
    	    metadata.put('Postal Code',addressList[0].Shipping_Zip_Postal_Code__c);
        	metadata.put('Country',addressList[0].Shipping_Country__c);
        }    
        
        system.debug('metadata'+metadata);
        if (System.isBatch()) {
            updateChargeNotFuture(opp.Stripe_Charge_Id__c,metadata);
        }else {
            updateCharge(opp.Stripe_Charge_Id__c,metadata);
        }
        
    }
    global static void createMetadataForOrder(String chargeId, String customerId, String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson){
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Billing Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
        metadata.put('Statement Descriptor',stripStatementDescriptor);
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
    
        system.debug('metadata'+metadata);
        updateChargeNotFuture(chargeId,metadata);
    }
    global static void createMetadataAndUpdateOLI(OpportunityLineItem oli,String salesPerson) {
        List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c,User_to_give_credit__c,User_to_give_credit__r.Name from Opportunity where id =: oli.OpportunityId ];
        Opportunity opp = oppList[0];
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
        }
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',BillingAddress);
        metadata.put('Full Name',opp.Contact__r.firstname+' '+opp.Contact__r.lastname);
        metadata.put('Email',opp.Contact__r.email);
        metadata.put('Phone',opp.Contact__r.phone);
        if(opp.User_to_give_credit__c ==null){
            metadata.put('Sales Person',salesPerson);
        }else {
            metadata.put('Sales Person', opp.User_to_give_credit__r.Name);    
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.Contact__r.id);
        metadata.put('Integration Initiated From','Salesforce');
        if(!addressList.isEmpty()){
        	metadata.put('Street',addressList[0].Shipping_Street__c);
        	metadata.put('City',addressList[0].Shipping_City__c);
	        metadata.put('State',addressList[0].Shipping_State_Province__c);
    	    metadata.put('Postal Code',addressList[0].Shipping_Zip_Postal_Code__c);
        	metadata.put('Country',addressList[0].Shipping_Country__c);
        }
        system.debug('metadata'+metadata);
        updateCharge(oli.Stripe_Charge_Id__c,metadata);
    }

    global static void createMetadataAndUpdateOLI2(OpportunityLineItem oli,String salesPerson, id oppId) {
        List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c,User_to_give_credit__c,User_to_give_credit__r.Name from Opportunity where id =: oppId ];
        Opportunity opp = oppList[0];
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
        }
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',BillingAddress);
        metadata.put('Full Name',opp.Contact__r.firstname+' '+opp.Contact__r.lastname);
        metadata.put('Email',opp.Contact__r.email);
        metadata.put('Phone',opp.Contact__r.phone);
        if(opp.User_to_give_credit__c ==null){
            metadata.put('Sales Person',salesPerson);
        }else {
            metadata.put('Sales Person', opp.User_to_give_credit__r.Name);    
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.Contact__r.id);
        metadata.put('Integration Initiated From','Salesforce');
        if(!addressList.isEmpty()){
        metadata.put('Street',addressList[0].Shipping_Street__c);
        metadata.put('City',addressList[0].Shipping_City__c);
        metadata.put('State',addressList[0].Shipping_State_Province__c);
        metadata.put('Postal Code',addressList[0].Shipping_Zip_Postal_Code__c);
        metadata.put('Country',addressList[0].Shipping_Country__c);
        }    
        
        
        system.debug('metadata'+metadata);
        if (System.isBatch()) {
            updateChargeNotFuture(opp.Stripe_Charge_Id__c,metadata);
        }else {
          updateCharge(oli.Stripe_Charge_Id__c,metadata);  
        }
        
    }
    
    global static void createMetadataAndUpdate1(Opportunity opp) {
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
            Map<String,String> metadata = new Map<String,String>();
            metadata.put('Shipping Address',BillingAddress);
            
            system.debug('metadata'+metadata);
            updateChargeNotFuture(opp.Stripe_Charge_Id__c,metadata);
        }
        
        
    }
    
    global static void createMetadataAndUpdate2(OpportunityLineItem oli) {
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:oli.Opportunity.Contact__c  and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
            Map<String,String> metadata = new Map<String,String>();
            metadata.put('Shipping Address',BillingAddress);
            
            system.debug('metadata'+metadata);
            updateChargeNotFuture(oli.Stripe_Charge_Id__c,metadata);
        }
        
        
    }
    
    global static void createMetadataAndUpdateSalesPerson(Opportunity opp,String salesperson) {

            Map<String,String> metadata = new Map<String,String>();
            metadata.put('Sales Person',salesperson);
            
            system.debug('metadata'+metadata);
            updateChargeNotFuture(opp.Stripe_Charge_Id__c,metadata);
        
    }
    
    global static void createMetadataAndUpdateSalesPersonOLI(OpportunityLineItem opp,String salesperson) {

            Map<String,String> metadata = new Map<String,String>();
            metadata.put('Sales Person',salesperson);
            
            system.debug('metadata'+metadata);
            updateChargeNotFuture(opp.Stripe_Charge_Id__c,metadata);
        
    }
    
    @Future(callout=true)
    global static void updateCharge(String chargeId, Map<String, String> metadata) {
        
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL+'/'+chargeId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    
    //@Future(callout=true)
    global static void updateChargeNotFuture(String chargeId, Map<String, String> metadata) {
        
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL+'/'+chargeId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }

     
    
   
    
}