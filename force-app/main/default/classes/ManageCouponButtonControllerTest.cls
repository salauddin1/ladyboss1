@isTest
public class ManageCouponButtonControllerTest {
    public static testMethod void testManageCoupon(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        Contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'testCoupon';
        insert ct;
        ct.PAP_refid__c = 'testCoupon';
        ct.Coupon_Code_Id__c = '234567';
        update ct;
        test.startTest();
        
        ManageCouponButtonController.getContact(String.valueOf(ct.id) );
        ManageCouponButtonController.updateCouponCode(String.valueOf(ct.id), '234567', 'coupCode');
        test.stopTest();
        
    }
    public static testMethod void ConnectToWooCommerce1(){
        Test.setMock(HttpCalloutMock.class, new ManageCouponHttpCalloutMock3());
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Store';
        cs.Create_Coupon_Site_URL__c = 'www.test.com';
        insert cs; 
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        insert cc;
        
        Contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'testCoupon';
        insert ct;
        ct.PAP_refid__c = 'testCoupon';
        update ct;
        
        test.startTest();
        ManageCouponButtonController.createCouponCode(String.valueOf(ct.id), 'testCoupon');
        test.stopTest();
        
    }
    public static testMethod void ConnectToWooCommerce2(){
        Test.setMock(HttpCalloutMock.class, new ManageCouponHttpCalloutMock3());
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Playground';
        cs.Create_Coupon_Site_URL__c = 'www.test.com';
        insert cs;   
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        insert cc;
        
        Contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'testCoupon';
        insert ct;
        ct.PAP_refid__c = 'testCoupon';
        update ct;
        
        test.startTest();
        ManageCouponButtonController.createCouponCode(String.valueOf(ct.id), 'testCoupon');
        test.stopTest();
        
    }
    
    
}