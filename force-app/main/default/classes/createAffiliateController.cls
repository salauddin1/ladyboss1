public class createAffiliateController implements Queueable,Database.AllowsCallouts { 
    public String oppList;
    public createAffiliateController(String jsbody){
        this.oppList = jsbody;
    }
    public void execute(QueueableContext context) {
        System.debug('Jsonn::-'+oppList);
        List<Opportunity> createAffiliateOppList = (List<Opportunity>) JSON.deserialize(oppList,List<Opportunity>.class);
        System.debug('createAffiliateOppList::::::-'+createAffiliateOppList);
        for(Opportunity opp:createAffiliateOppList){
            createAffiliate(opp.Contact__c);
        }
    }
    @AuraEnabled
    public static Contact ct{get;set;}
    @AuraEnabled
    public static Contact getContact(String conId){
        Contact ct = [SELECT id,Email,PAP_refid__c,PAPRPassword__c,PAPUserId__c,Coupon_Code_Id__c FROM Contact WHERE id=:conId];
        if(ct.PAP_refid__c == null || ct.PAP_refid__c ==''){
            List<Contact> ctList = [SELECT id,Email,PAP_refid__c,firstname,lastname,PAPRPassword__c,PAPUserId__c,Coupon_Code_Id__c FROM Contact WHERE PAP_refid__c != null AND email =: ct.Email];
            if(ctList.size()>0){
                if(ctList[0].PAPUserId__c != null && ctList[0].PAPUserId__c != ''){
                    ct.PAPUserId__c = ctList[0].PAPUserId__c;
                }
                if(ctList[0].PAPRPassword__c != null && ctList[0].PAPRPassword__c != ''){
                    ct.PAPRPassword__c = ctList[0].PAPRPassword__c;
                }
                if(ctList[0].Coupon_Code_Id__c != null && ctList[0].Coupon_Code_Id__c != ''){
                    ct.Coupon_Code_Id__c = ctList[0].Coupon_Code_Id__c;
                }
                ct.PAP_refid__c = ctList[0].pap_refid__c;
                if(!test.isRunningTest()){
                    update ct;
                } 
            }
        } 
        return ct;
    }
    @AuraEnabled
    public static String checkRefId(String conId){
        String newRefId='';
        ct = [SELECT id,Email,PAP_refid__c,firstname,lastname FROM Contact WHERE id=:conId];
        List<Contact> ctList = new List<Contact>();
        String firstName ='%'+ ct.FirstName+'%';
        
        ctList = [SELECT id,Email,PAP_refid__c,firstname,lastname FROM Contact WHERE PAP_refid__c != null AND PAP_refid__c Like :firstName];
        System.debug('ctList:-'+ctList);
        String str ='';
        integer count = 0;
        
        if(ctList.size()>0){
            Set<Integer> numberSet = new Set<Integer>();
            for(Contact cnt :ctList){
                String temp = '0';
                str = cnt.PAP_refid__c.toLowerCase();
                if(str != null && str != '' && str.substring((str.length()-2), str.length()) == '10'){
                    str = str.remove('10');
                }
                for(integer i = 0; i < str.length(); i++){
                    String st = str.substring(i,i+1);
                    if(st.isNumeric()){
                        temp = temp+st;
                    } 
                } 
                System.debug('str:='+str);
                System.debug('temp:='+temp);
                numberSet.add(integer.valueOf(temp));
            }
            List<Integer> sortedList = new List <Integer>();
            sortedList.addAll(numberSet);
            sortedList.sort(); 
            integer maxNo = sortedList.get(sortedList.size()-1);
            if(ct.FirstName != null ){ 
                if( ct.FirstName.contains(' ') ){
                    newRefId = ct.FirstName.remove(' ') + (maxNo + 1)+'10';
                }
                else{
                    newRefId = ct.FirstName + (maxNo + 1)+'10';
                }
            }
            else{
                newRefId =  (maxNo + 1)+'10';
            }
        }
        if(newRefId == null || newRefId == ''){
            if(ct.FirstName != null ){ 
                if( ct.FirstName.contains(' ') ){
                    newRefId = ct.FirstName.remove(' ') +'10';
                }
                else{
                    newRefId = ct.FirstName +'10';
                }
            }
            else{
                newRefId = '10';
            }
        }
        return newRefId;
    }
    @AuraEnabled
    public static String createAffiliate(String conId){
        String newRefId = createAffiliateController.checkRefId(conId);
        System.debug('newRefId:-'+newRefId);
        pap_credentials__c  pap_url = [select id,name,pap_url__c,username__c,password__c,X3FF_Campaign_Id__c from pap_credentials__c where Name='pap url' limit 1];
        Http http = new Http();
        HttpRequest ht = new HttpRequest();
        ht.setEndpoint(pap_url.pap_url__c);
        ht.setMethod('POST');
        ht.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Merchants_User_AffiliateForm","M":"add","isFromApi":"Y","fields":[["name","value","values","error"],["username","'+ct.Email+'",null,""],["firstname","'+ct.FirstName+'",null,""],["status","A",null,""],["lastname","'+ct.LastName+'",null,""],["refid","'+newRefId+'",null,""],["data25","3FF",null,""],["data24","'+newRefId+'",null,""],["agreeWithTerms","Y",null,""]]}]}';
        ht.setBody(body);
        System.debug('request body:-'+ht.getBody());
        HttpResponse res = http.send(ht);
        System.debug('res : '+res.getBody());
        List<Object> resp = (List<Object>) Json.deserializeUntyped(res.getBody());
        String msg='';
        for(Object ob : resp){
            System.debug('single res:-'+ob);
            Map<String,Object> ob1 = (Map<String,Object>)ob;
            if((String)ob1.get('message')!=null){
                msg = (String)ob1.get('message');
            }
            System.debug('msg '+ob1.get('message'));
        }
        if(msg=='Affiliate was successfully added'){
            ct.PAP_refid__c = newRefId;
            for(Object ob : resp){
                Map<String,Object> ob1 = (Map<String,Object>)ob;
                if(ob1.get('fields')!=null){
                    List<Object> fieldList = (List<Object>)ob1.get('fields');
                    for(Object field:fieldList){
                        List<Object> fields = (List<Object>)field;
                        if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('userid')){
                            ct.PAPUserId__c = (String)fields.get(1);
                        }
                        if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('rpassword')){
                            ct.PAPRPassword__c = (String)fields.get(1);
                        }
                    }
                }    
            }
            if( res !=  null && ct.PAP_refid__c != null && ct.PAP_refid__c != ''){
                String coupCode = ct.PAP_refid__c;
                if(conId != null && coupCode != null){
                    if( coupCode.contains(' ') ){
                        coupCode = coupCode.remove(' ') ;
                    }
                    List<Contact> conList1 = new List<Contact>();
                    conList1 = [select id, Coupon_Discount_Type__c, Coupon_Amount__c, Coupon_Code_Id__c, pap_refid__c from contact where id =: conId];
                    if(conList1.size() > 0){
                        if(conList1[0].Coupon_Code_Id__c == null){
                            Boolean individual_use = true;
                            Boolean exclude_sale_items = true;
                            String jsonBody = '';
                            
                            Create_Coupon__c cc = [select id, Amount__c, Discount_Type__c, Product_Categories__c,Product_Exclude_Categories__c from Create_Coupon__c where name = 'default'];
                            String coupAmount = String.valueOf(cc.Amount__c);
                            String coupDiscountType = cc.Discount_Type__c;
                            String prodCatbody = '';
                            String prodExCatbody = '';
                            if(cc.Product_Categories__c != null ){
                                if(cc.Product_Categories__c.contains(',') ){
                                    List<Integer> pcIdList = new List<Integer>();
                                    List<String> pcList = cc.Product_Categories__c.split(',');
                                    for(String pcval : pcList){
                                        pcIdList.add( Integer.valueOf(pcval) );
                                    }
                                    if(pcIdList.size() > 0){
                                        prodCatbody =JSON.serialize(pcIdList);
                                    }
                                }
                                else{
                                    prodCatbody =JSON.serialize(new List<Integer>{Integer.valueOf( cc.Product_Categories__c)});
                                }
                            }
                            if(cc.Product_Exclude_Categories__c != null ){
                                if(cc.Product_Exclude_Categories__c.contains(',') ){
                                    List<Integer> pcIdList = new List<Integer>();
                                    List<String> pcList = cc.Product_Exclude_Categories__c.split(',');
                                    for(String pcval : pcList){
                                        pcIdList.add( Integer.valueOf(pcval) );
                                    }
                                    if(pcIdList.size() > 0){
                                        prodExCatbody =JSON.serialize(pcIdList);
                                    }
                                }
                                else{
                                    prodExCatbody =JSON.serialize(new List<Integer>{Integer.valueOf( cc.Product_Exclude_Categories__c)});
                                }
                            }
                            jsonBody = '{ "code": "'+coupCode+'", "amount": "'+coupAmount+'", "discount_type": "'+coupDiscountType+'", "individual_use": '+individual_use+', "exclude_sale_items": '+exclude_sale_items+',"product_categories":'+prodCatbody+',"excluded_product_categories":'+prodExCatbody+' }';
                            System.debug('jsonBody:-'+jsonBody);
                            List<Create_Site_Coupon__c> siteCouponList = [select id,Name,Create_Coupon_Site_URL__c from Create_Site_Coupon__c];
                            HttpResponse response1=null;
                            for(Create_Site_Coupon__c site: siteCouponList){
                                System.debug('jsonBody:-'+jsonBody);
                                Http http1 = new Http();
                                HttpRequest request1 = new HttpRequest();
                                request1.setEndpoint(site.Create_Coupon_Site_URL__c);
                                request1.setMethod('POST');
                                request1.setBody(jsonBody);
                                request1.setHeader('Content-Type', 'application/json');
                                
                                if(site.Name == 'Store'){
                                    response1 = http.send(request1);
                                }
                                else{
                                    HttpResponse response2 = http.send(request1);
                                }
                            }
                            if(response1 != null){
                                if (response1.getStatusCode() == 200 || response1.getStatusCode() == 201 ) {
                                    if(response1.getBody() != null && response1.getBody() != ''){
                                        String body1 = response1.getBody();
                                        System.debug('Body1 =========test========== :-'+Body1);
                                        Map<String,Object> getCodeMap = new Map<String,Object>();
                                        if(!test.isRunningTest()){
                                            getCodeMap = (Map<String,Object>)Json.deserializeUntyped(body1);
                                            if(getCodeMap != null && getCodeMap.keySet().size() > 0){
                                                if(getCodeMap.keySet().contains('code') && getCodeMap.keySet().contains('id') && getCodeMap.get('code') != null && getCodeMap.get('id') != null){
                                                    ct.Coupon_Code_Id__c = String.valueOf(getCodeMap.get('id'));
                                                    ct.Coupon_Amount__c = Decimal.valueOf(coupAmount);
                                                    ct.Coupon_Discount_Type__c = coupDiscountType;
                                                    msg='Affiliate and Coupon was successfully added';
                                                }
                                            } 
                                        }
                                    }
                                }
                            }
                        }
                    }
                }   
            }
            
            /*  if( res !=  null && ct.PAPUserId__c != null && ct.PAPUserId__c != '' && pap_url.X3FF_Campaign_Id__c != null && pap_url.X3FF_Campaign_Id__c != '' ){
Http http2 = new Http();
HttpRequest ht2 = new HttpRequest();
ht2.setEndpoint(pap_url.pap_url__c);
ht2.setMethod('POST');
ht2.setHeader('Content-Type', 'application/x-www-form-urlencoded');
String body2 = 'D={"C":"Gpf_Rpc_Server", "M":"run", "requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Db_UserInCommissionGroup", "M":"addUsers", "campaignId":"'+pap_url.X3FF_Campaign_Id__c+'", "sendNotification":"N", "status":"A", "note":"", "merchantnote":"", "ids":["'+ct.PAPUserId__c+'"]}]}';
ht2.setBody(body2);
HttpResponse res2 = new HttpResponse();
if(!test.isRunningTest()){
res2 = http.send(ht2);
}
if( res2 !=  null && res2.getBody() != null){
System.debug('res2 : '+res2.getBody());
}
}*/
            
            List<Pap_Commission__c> papList = [select id,Contact__c,Opportunity__c,Opportunity__r.Contact__c from Pap_Commission__c where Contact__c=: ct.Id];
            List<Id> oppIds = new List<Id>();
            for(Pap_Commission__c pap:papList){
                if(pap.Contact__c == pap.Opportunity__r.Contact__c){
                    oppIds.add(pap.Opportunity__c);
                }
            }
            List<Opportunity> oppList = [select id,Coupon_Code__c from Opportunity where id in: oppIds];
            List<Opportunity> updateOppList = new List<Opportunity>();
            if(oppList.size()>0){
                for(Opportunity opp:oppList){
                    if(opp.Coupon_Code__c == null || opp.Coupon_Code__c == ''){
                        opp.Coupon_Code__c = ct.PAP_refid__c;
                        updateOppList.add(opp);
                    }
                }
            }
            if(!test.isRunningTest()){
                update ct;
                if(updateOppList.size()>0){
                    update updateOppList;
                }
            }
            
        }
        System.debug('msgs is : '+msg);
        return msg;
    }
    
    @AuraEnabled
    public static String updateAffiliate(String conId,string newrefId){
        if(newrefId != null ){ 
            if( newrefId.contains(' ') ){
                newRefId = newrefId.remove(' ') ;
            }
        }
        Contact ct = [SELECT id,Email,FirstName,LastName,PAP_refid__c,PAPRPassword__c,PAPUserId__c FROM Contact WHERE id=:conId];
        String msg='';
        msg= 'Affiliate and Coupon successfully saved';
        if(newrefId != null && newrefId != '' && ct.PAPUserId__c != null && ct.PAPUserId__c != ''){
            ct.PAP_refid__c = newrefId;
        }
        else{
            msg = 'Please fill PAP User Id filed';  
        }
        if(!test.isRunningTest()){
            update ct;
        }
        System.debug('msgs is : '+msg);
        return msg;
    }   
}