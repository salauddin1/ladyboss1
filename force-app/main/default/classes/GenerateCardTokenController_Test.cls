@IsTest
public class GenerateCardTokenController_Test {
    
    static testMethod void testParse() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
        insert addr;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Customer__c = con.id;
        insert sp;
        
        test.startTest();
        
        PageReference pageRef = Page.GenerateCardToken;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        
        GenerateCardTokenController gcc = new GenerateCardTokenController();
        gcc.cardJson  = '{"id":"card_1DVcQJFzCf73siP0gFJA614Z","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":"90001","address_zip_check":"unchecked","brand":"Visa","country":"US","cvc_check":"unchecked","dynamic_last4":null,"exp_month":4,"exp_year":2023,"funding":"credit","last4":"4242","metadata":{},"name":"test 2","tokenization_method":null}';
        gcc.token = '"'+'tok_1DVcQJFzCf73siP0aE2hzq0S'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
    
    static testMethod void testParse2() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
        insert addr;
       
        
        test.startTest();
        
        PageReference pageRef = Page.GenerateCardToken;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        ApexPages.currentPage().getParameters().put('using','V6');
        
        GenerateCardTokenController gcc = new GenerateCardTokenController();
        gcc.cardJson  = '{"id":"card_1DVcQJFzCf73siP0gFJA614Z","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":"90001","address_zip_check":"unchecked","brand":"Visa","country":"US","cvc_check":"unchecked","dynamic_last4":null,"exp_month":4,"exp_year":2023,"funding":"credit","last4":"4242","metadata":{},"name":"test 2","tokenization_method":null}';
        gcc.token = '"'+'tok_1DVcQJFzCf73siP0aE2hzq0S'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
    static testMethod void testParse3() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
        insert addr;
       
        
        test.startTest();
        
        PageReference pageRef = Page.GenerateCardToken;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        ApexPages.currentPage().getParameters().put('using','V8');
        
        GenerateCardTokenController gcc = new GenerateCardTokenController();
        gcc.cardJson  = '{"id":"card_1DVcQJFzCf73siP0gFJA614Z","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":"90001","address_zip_check":"unchecked","brand":"Visa","country":"US","cvc_check":"unchecked","dynamic_last4":null,"exp_month":4,"exp_year":2023,"funding":"credit","last4":"4242","metadata":{},"name":"test 2","tokenization_method":null}';
        gcc.token = '"'+'tok_1DVcQJFzCf73siP0aE2hzq0S'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
    static testMethod void testParse4() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        

        test.startTest();
        
        PageReference pageRef = Page.GenerateCardToken;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        ApexPages.currentPage().getParameters().put('using','V8');
        
        GenerateCardTokenController gcc = new GenerateCardTokenController();
        gcc.cardJson  = '{"id":"card_1DVcQJFzCf73siP0gFJA614Z","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":"90001","address_zip_check":"unchecked","brand":"Visa","country":"US","cvc_check":"unchecked","dynamic_last4":null,"exp_month":4,"exp_year":2023,"funding":"credit","last4":"4242","metadata":{},"name":"test 2","tokenization_method":null}';
        gcc.token = '"'+'tok_1DVcQJFzCf73siP0aE2hzq0S'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
}