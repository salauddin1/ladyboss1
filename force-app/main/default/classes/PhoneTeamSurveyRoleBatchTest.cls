@IsTest
public class PhoneTeamSurveyRoleBatchTest {
    @isTest
    public static void test() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

        
        Phone_2_minute_Survey_agents__c phone = new Phone_2_minute_Survey_agents__c();
        phone.Call_Duration__c = 120;
        phone.name = 'test';
        phone.Salesforce_ID__c = u.Id;
        insert phone;
        Test.startTest();
        PhoneTeamSurveyRoleBatch pt = new PhoneTeamSurveyRoleBatch();
        Database.executeBatch(pt);
        Test.stopTest();
    }

}