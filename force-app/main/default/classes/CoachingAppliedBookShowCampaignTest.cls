@isTest
public class CoachingAppliedBookShowCampaignTest {
    @isTest
    public static void testcamp1 () {
        List<Contact> conlist = new List<Contact>();
        contact con = new contact ();
        con.LastName = 'BlogTest';
        con.FirstName = 'firstname';
        con.sf4twitter__Origin__c = 'Blog';
        con.Coaching_Applied_But_Didn_t_Book__c = true; 
        insert con;
        conList.add(con);
        con.Coaching_Booked_But_Didn_t_Show__c = true;
        List<Contact> conlist2 = new List<Contact>();
        contact con2 = new contact ();
        con2.LastName = 'BlogTest';
        con2.FirstName = 'firstname';
        con2.sf4twitter__Origin__c = 'Blog';
        con2.Coaching_Booked_But_Didn_t_Show__c = true;
        insert con2;
        conList2.add(con2);
        campaign cmp = new campaign ();
        cmp.Name = 'Coaching-Applied But Did Not Book';
        cmp.Type = 'Coaching Team Follow Up';
        insert cmp;
        campaign cmp2 = new campaign ();
        cmp2.Name = 'Coaching-Booked But Did Not Show';
        cmp2.Type = 'Coaching Team Follow Up';
        insert cmp2;
        Test.startTest();
        CoachingAppliedBookShowCampaign.addtocochAppliedBookCampjob(conList);
        CoachingAppliedBookShowCampaign.addtocochAppliedShowCampJob(conList2);
        Test.stopTest();
    }
}