@isTest
private class GmailToSalesforceHandler_Test{
    static testMethod void test1() 
    {
       // Create a new email and envelope object.    
       
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       // Create Test record.
       Contact cont = new Contact(firstName='john', lastName='smith', Email='test@test.com', HasOptedOutOfEmail=false);
       insert cont ;
       
       // Test with the subject that matches the unsubscribe statement.
       email.subject = 'Test Contact Email';
       email.plainTextBody = 'Test Contact Email';

       email.fromAddress = 'test@test.com';
        email.toAddresses = new String[]{'jay9909559293@gmail.com'};
       GmailToSalesforceHandler obj= new GmailToSalesforceHandler();
       obj.handleInboundEmail(email, env );
                            
    }
    static testMethod void test2() 
    {
       // Create a new email and envelope object.    
       
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       // Create Test record.
       Contact cont = new Contact(firstName='john', lastName='smith', Email='test@test.com', HasOptedOutOfEmail=false);
       insert cont ;
       
       // Test with the subject that matches the unsubscribe statement.
       email.subject = 'Test Contact Email';
       email.plainTextBody = 'Test Contact Email';

       email.fromAddress = 'jay9909559293@gmail.com';
        email.toAddresses = new String[]{'test@test.com'};
       GmailToSalesforceHandler obj= new GmailToSalesforceHandler();
       obj.handleInboundEmail(email, env );
                            
    }
    static testMethod void test3() 
    {
       // Create a new email and envelope object.    
       
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       
       // Test with the subject that matches the unsubscribe statement.
       email.subject = 'Test Contact Email';
       email.plainTextBody = 'Test Contact Email';

       email.fromAddress = 'forwarding-noreply@google.com';
        email.toAddresses = new String[]{'jay9909559293@gmail.com'};
       GmailToSalesforceHandler obj= new GmailToSalesforceHandler();
       obj.handleInboundEmail(email, env );
                            
    }
     
}