/*
 * Developer Name : Tirth Patel
 * Discription : According to WhatID type of task this batch sets values of Account,Opportunity and Case lookup fields and sets whatId to null
 */
global  class updatewhattIdTaksBatch implements Database.Batchable<SObject>{
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Account__c,Case__c,Opportunity__c,WhatId,What.type FROM Task where What.type in ('Opportunity','Account','Case') and whatId!=null]);
    } 
    global void execute(Database.BatchableContext context, List<Task> records) {
        for(Task tsk : records ){
            if(tsk.What.type=='Account'){
                tsk.Account__c = tsk.WhatId;
            }else if(tsk.What.type=='Opportunity'){
                tsk.Opportunity__c = tsk.WhatId;
            }else if(tsk.What.type=='Case'){
                tsk.Case__c = tsk.WhatId;
            }
            tsk.whatid = null;
        }
        update records;
    }
    global void finish(Database.BatchableContext context) {
        
    }
}