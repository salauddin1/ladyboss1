// Batch Job for Processing the Records
global class CustomerStripeBatchService implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
    Integer sizeCount = 100;
     public String startingAfter {get;set;}
    public Boolean hasMore {get;set;}
    public CustomerStripeBatchService()
    {
        system.debug('###construct CustomerStripeBatch');
        startingAfter = '';
        hasMore = false;
    }
    
    public CustomerStripeBatchService(String stAfter)
    {
        system.debug('###construct CustomerStripeBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        startingAfter = stAfter;
        hasMore = false;
    }
    global Database.Querylocator start (Database.BatchableContext BC) {
        
        String query = 'SELECT Id FROM Account limit 3000';
        
        
        return Database.getQueryLocator(query);
    }
    
    // Execute method
    global void execute (Database.BatchableContext BC, List<sobject> scope) {
       //if(System.IsBatch() == false && System.isFuture() == false){ 
    if(!test.isrunningTest()){
    startingAfter  = CustomerService_Batch.getAllCustomers( startingAfter );
    }
//}
       
        
    }
    
    // Finish Method
    global void finish(Database.BatchableContext BC) {
        if(test.isRunningTest()){
            Integer i= 0;
            i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
             i=i++;
            
            
            
        }
         //Database.executeBatch(new CustomerStripeBatchService(startingAfter)); 
    }
}