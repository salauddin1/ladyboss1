//need is for contacts who are added to the campaigns(clickfunnel) to be added immediately also to the "Master List Campaign"  
public class ClickFunnelContactCampaignHndlr{
//Triggered from trigger
    public static void checkListClickFunnel(Set<Id> idset1) {
        set<Id> idset = new set<id>();
        Map<id,CampaignMember> campMap1 = new Map<id,CampaignMember>();
        Map<id,set<String>> campMap = new Map<id,set<String>>();
        //Get the list of camp members
        List<CampaignMember > cmListT = [SELECT Campaign.Type,Campaign.Name,type, ContactId, Description, Id, Name, State, Status FROM CampaignMember where id IN: idset1];
        
        Set<Id> contactIdSet = new Set<ID>();
        for(CampaignMember cm : cmListT ) {
            
            //Loop to put contactId and list of camp members and if and only if type clickFunnel will enter
            if(cm.ContactId!=null && (cm.Campaign.type  == 'ClickFunnels Campaigns' || cm.Campaign.Name=='SuplmtLead-1 Day' || cm.Campaign.Name=='TrialLead-1 Day-DO NOT USE'  || cm.Campaign.Name=='UnlimLead-1 Day' )) {
                idset.add(cm.ContactId);
                if(!campMap.containsKey(cm.ContactId)){
                    set<String> cList = new set<String>();
                    cList.add(cm.Campaign.Name);
                    campMap.put(cm.ContactId,cList);
                }else{
                    set<String> cList = campMap.get(cm.ContactId);
                    cList.add(cm.Campaign.Name);
                    campMap.put(cm.ContactId,cList);
                }
            }
            
            
        }
        
        List<CampaignMember > cmList = new List<CampaignMember > ();
        //List of contacts
        List<Contact> conList = [select id from contact where id IN : idset];  
        //To get the master list Id
        List<campaign> masrList = [select id, Type, name from campaign where name=:'Master List'];  
        for(Contact con : conList ) {
            Set<String> campName = campMap.get(con.id);
           
            if(masrList.size()>0 && !campName.Contains('Master List')){
            //Create record for camp member
                CampaignMember newCM = new CampaignMember(CampaignId = masrList[0].id,ContactId = con.id,status = 'Sent' );
               
                cmList.add(newCM); 
            }
            
        }
        
        //insert here
        if(cmList.size() > 0  ) {
            //Insert the camp member
           
            database.insert (cmList, false);
        }
        
    }
    
}