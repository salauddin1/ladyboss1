public class OpportunityAfterUpdateHandler {
    public static void firstSellCheck(Map<id,Opportunity> TriggerNewMap,Map<id,Opportunity> TriggerOldMap){
        List<Id> firstOppList = new List<Id>();
        Set<Id> fixComProduct = new Set<Id>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Comission_Configuration__c firstSellConfig;
        for(Comission_Configuration__c con : [SELECT id,product__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Vice_President__c,Percentage_Paid_to_Senior_Agent__c FROM Comission_Configuration__c WHERE Product__c!=null]){
                fixComProduct.add(con.Product__c);
        }
        Decimal week = TriggerNewMap.values().get(0).week__c;
        Set<Id> agentIdSet = new Set<Id>();
        for(Id oppId : TriggerNewMap.keyset()){
            if(TriggerNewMap.get(oppId).Is_First_Sell__c && !TriggerOldMap.get(oppId).Is_First_Sell__c){
                firstOppList.add(oppId);
                agentIdSet.add(TriggerNewMap.get(oppId).Sales_Person_Id__c);
            }
        }
        if(agentIdSet.size()==0){
            return;
        }
        Set<id> inactiveUsers = new Set<id>();
        Map<Id,String> user_TwoWeekTotalMap = OpportunityAfterUpdateTriggerHandler.twoWeekTotal(agentIdSet, week, fixComProduct);
        System.debug(firstOppList+' '+agentIdSet);
        List<Id> userIdList = new List<Id>();
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c in : agentIdSet] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Vice_President__c);
            userIdList.add(userhierarki.Senior_Agent__c);
        }
        System.debug(firstOppList);
        Map<id,User> specialcomMap = new Map<id,User>();
        for(User ur : [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE id in : userIdList ]){
            specialcomMap.put(ur.id,ur);
            if(!ur.IsActive){
                inactiveUsers.add(ur.id);
            }
        }
        
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = OpportunityAfterUpdateTriggerHandler.getTotalCommissionConfiguration();
        if(maxValAndComMap.size()==0){
            return;
        }
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap = PayToCommTriggerHelper.getOldPaymentsFS(agentIdSet, fixComProduct);
        Map<id,id> payAndcreditUser = PayToCommTriggerHelper.getOldPaymentCreditUserFS(agentIdSet, fixComProduct);
        List<comission__c> comToDelete = new List<Comission__c>();
        List<comission__c> comToInsert = new List<Comission__c>();
        List<Payment__c> payList  = [SELECT id,split_agent_payment__c,Opportunity__r.User_to_give_credit__c,Payment_Created_By__c,week__c,Amount__c,(SELECT id FROM comissions__r) FROM Payment__c WHERE opportunity__r.id in : firstOppList AND Product__c not in : fixComProduct];
        Map<id,Decimal> payOwnerAndTotal = new Map<id,Decimal>();
        for(Payment__c paymnt:payList){
                if(payOwnerAndTotal.get(paymnt.Payment_Created_By__c)==null){
                    payOwnerAndTotal.put(paymnt.Payment_Created_By__c,paymnt.Amount__c);
                }else{
                    payOwnerAndTotal.put(paymnt.Payment_Created_By__c,payOwnerAndTotal.get(paymnt.Payment_Created_By__c) + paymnt.Amount__c);
                } 
        }
        
        for(Payment__c pay : payList){
            if(pay.comissions__r!=null && pay.comissions__r.size()>0){
                comToDelete.addAll(pay.comissions__r);
            }
            if(pay.Payment_Created_By__c==null){
                continue;
            }
            UserHierarchy__c userhie = userHieMap.get(pay.Payment_Created_By__c); 
            Decimal twoWeekTotal = Decimal.valueOf(user_TwoWeekTotalMap.get(pay.Payment_Created_By__c));
            firstSellConfig = OpportunityAfterUpdateTriggerHandler.getCommissionConfiguration(twoWeekTotal, maxValAndComMap);
            System.debug(firstSellConfig);
            if(specialcomMap.get(pay.Payment_Created_By__c).IsActive){
                if(pay.split_agent_payment__c){
                    OpportunityAfterUpdateTriggerHandler.insertComission(pay.Opportunity__r.User_to_give_credit__c, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200), pay.week__c, comToInsert, firstSellConfig,'Credit User(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c/2);
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Agent__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200), pay.week__c, comToInsert, firstSellConfig,'Agent With Credit User(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c/2);
                }
                else{
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Agent__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Agent(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c);
                }
            }else{
                if(pay.split_agent_payment__c){
                    OpportunityAfterUpdateTriggerHandler.insertComission(pay.Opportunity__r.User_to_give_credit__c, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200), pay.week__c, comToInsert, firstSellConfig,'Credit User(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c/2);
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Vice_President__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200), pay.week__c, comToInsert, firstSellConfig,'Agent With Credit User(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c/2);
                }
                else{
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Vice_President__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Agent Com. To VP(First Sell)',firstSellConfig.Percentage_Paid_to_Agent_Making_Sell__c);
                }
            }
            
            if(userhie.Director__c!=null){
                OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Director__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Director__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Director(First Sell)',firstSellConfig.Percentage_Paid_to_Director__c);
            }
            if(userhie.Vice_President__c!=null){
                OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Vice_President__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Vice_President__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Vice President(First Sell)',firstSellConfig.Percentage_Paid_to_Vice_President__c);
            }
            if(userhie.Senior_Agent__c!=null && !pay.split_agent_payment__c ){
                if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Senior_Agent__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Senior Agent(First Sell)',firstSellConfig.Percentage_Paid_to_Senior_Agent__c);
                }else{
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Senior_Agent__r.id, pay.id, specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Senior Agent Com. Special(First Sell)',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                }
            }else if(userhie.Senior_Agent__c!=null){
                if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Vice_President__r.id, pay.id, firstSellConfig.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Senior Agent Com. To VP(First Sell)',firstSellConfig.Percentage_Paid_to_Senior_Agent__c);
                }else{
                    OpportunityAfterUpdateTriggerHandler.insertComission(userhie.Vice_President__r.id, pay.id, specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100), pay.week__c, comToInsert, firstSellConfig,'Senior Agent Com. To VP Special(First Sell)',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                }
            }
          /*  if(agentIdSet.contains(pay.Payment_Created_By__c)){
                    Decimal twoWeekTotalBefore = twoWeekTotal - payOwnerAndTotal.get(pay.Payment_Created_By__c);
                    Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                    if(comBefore.id != firstSellConfig.id){
                        System.debug('tier changed');
                        if(payUserAndComMap.size()>0){
                            PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMap.get(pay.Payment_Created_By__c), firstSellConfig, userhie, comToInsert, payAndcreditUser,specialcomMap,inactiveUsers);
                        } 
                    }
                    agentIdSet.remove(pay.Payment_Created_By__c);
                }   */
        }
        if(comToInsert.size()>0){
            upsert comToInsert;
        }
        if(comToDelete.size()>0){
            delete comToDelete;
        }
    }
    public static void salesPersonChange(Map<id,Opportunity> TriggerNewMap,Map<id,Opportunity> TriggerOldMap){
        List<Id> OppIdList = new List<Id>();
        for(Id oppId : TriggerNewMap.keyset()){
            if((TriggerNewMap.get(oppId).sales_person_id__c!=TriggerOldMap.get(oppId).sales_person_id__c)&&TriggerNewMap.get(oppId).sales_person_id__c!=null){
                OppIdList.add(oppId);
            } 
        }
        System.debug('sales person change oppList : '+OppIdList);
        if(OppIdList.size()==0){
            return;
        }
        List<Comission__c> comDeleteList = new List<Comission__c>();
        List<Payment__c> paymentDeleteList = [SELECT id,(Select id from Comissions__r) from Payment__c Where Opportunity__r.id in : OppIdList];
        for(Payment__c pay : paymentDeleteList){
            for(Comission__c com : pay.comissions__r){
                comDeleteList.add(com);
            }
        }
        System.debug(paymentDeleteList);
        System.debug(comDeleteList);
        if(paymentDeleteList.size()>0){
            delete paymentDeleteList;
        } 
        if(comDeleteList.size()>0){
            delete comDeleteList;
        }
        List<Payment__c> payList = new List<Payment__c>();
        List<OpportunityLineItem> oliList = [select id,OpportunityId,Product2Id,Total_Commissionable_amount__c,TotalPrice,Stripe_charge_id__c,Subscription_Id__c,Opportunity.recordType.name,Opportunity.Stripe_charge_id__c,opportunity.Sales_Person_id__r.name from opportunityLineItem Where Consider_For_Commission__c =true and OpportunityId in : OppIdList];
        Set<String> chargeUpdated = new Set<String>();
        for(OpportunityLineItem ol : oliList){
            if(!Test.isRunningTest()){
                if(ol.Opportunity.recordType.name == 'Subscription'){
                    System.debug('coming here');
                    InvoiceToOliChargeHelper.updateChargeCallout(ol,ol.opportunity.Sales_Person_id__r.name,ol.Stripe_charge_id__c);
                    InvoiceToOliChargeHelper.updateSubCallout(ol,ol.opportunity.Sales_Person_id__r.name,ol.Subscription_Id__c);
                }else{
                    if(!chargeUpdated.contains(TriggerNewMap.get(ol.OpportunityId).Stripe_charge_id__c)){
                        InvoiceToOliChargeHelper.updateChargeCallout(ol,ol.opportunity.Sales_Person_id__r.name,TriggerNewMap.get(ol.OpportunityId).Stripe_charge_id__c);
                        chargeUpdated.add(TriggerNewMap.get(ol.OpportunityId).Stripe_charge_id__c);
                    }
                }
            }
            system.debug(ol.opportunity.Sales_Person_id__r.name);
            Payment__c pay = new Payment__c();   
            pay.Opportunity__c = ol.OpportunityId;
            pay.week__c = TriggerNewMap.get(ol.OpportunityId).week__c;
            pay.Payment_Created_By__c = TriggerNewMap.get(ol.OpportunityId).sales_person_id__c;
            pay.Product__c = ol.Product2Id;
            pay.Amount__c = ol.Total_Commissionable_amount__c;
            pay.Actual_amount__c = ol.TotalPrice;
            payList.add(pay);
        }
        System.debug(payList);
        insert payList;
    }
}