global class ChangeStatustoHighPriority implements Database.Batchable<sobject> {
    global ChangeStatustoHighPriority(){}
        
        global Database.Querylocator start (Database.BatchableContext BC) {
        return Database.getQueryLocator([select CaseNumber,Subject,Description from case]);
        }
        
        global void execute (Database.BatchableContext BC, List<case> scope) {
        //list<case> c = [select CaseNumber,Subject,Description from case];
        Set<case> caSet = new Set<case>();
         
        string a = 'lawsuit';
        string a1 = 'sue';
        string a2 = 'litigation';
        string a3 = 'lawyer';
        string a4 = 'court';
        string a5 = 'angry';
        string a6 = 'upset';
        string a7 = 'BBB';
        string a8 = 'FaceBook';
        string a9 = 'Attorney general';
        list<case> clist = new list<case>();
        
        List<string> slist_main = new List<string>();
        
        slist_main.add(a1);
        slist_main.add(a2);
        slist_main.add(a3);
        slist_main.add(a4);
        slist_main.add(a5);
        slist_main.add(a6);
        slist_main.add(a7);
        slist_main.add(a8);
        slist_main.add(a9);
        slist_main.add(a);
        System.debug('=------------------------====================='+slist_main);
            for(case s:scope){
                if(s.Description!= null|| s.Subject!= null)
                    for(String s2:slist_main){
                        
                            if((s.Description!= null && s.Description.contains(s2)) || (s.Subject!=null && s.Subject.contains(s2))){
                                s.Priority='High';
                                caSet.add(s);
                                }
                            }
            }
            for(case ca: caSet){
                clist.add(ca);
            }
        update clist;
     }
        global void finish(Database.BatchableContext BC) {}
         
      
}