@IsTest
public class StripeCharge_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        /*String json = '{ \"id\": \"evt_1BGb19DiFnu7hVq7eWM8NykH\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508884623, \"data\": { \"object\": { \"id\": \"ch_1BGb19DiFnu7hVq7Vs7GihRu\", \"object\": \"charge\", \"amount\": 2000, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1BGb19DiFnu7hVq7vq3Tm0oX\", \"captured\": true, \"created\": 1508884623, \"currency\": \"usd\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"description\": \"null\", \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": { }, \"invoice\": null, \"livemode\": false, \"metadata\": { }, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1BGb19DiFnu7hVq7Vs7GihRu/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BGE5EDiFnu7hVq7MMj5y28q\", \"object\": \"card\", \"address_city\": null, \"address_country\": null, \"address_line1\": null, \"address_line1_check\": null, \"address_line2\": null, \"address_state\": null, \"address_zip\": null, \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 8, \"exp_year\": 2019, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": { }, \"name\": null, \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_i4OseGgOylA97N\", \"idempotency_key\": null }, \"type\": \"charge.succeeded\" }';
        StripeCharge.StripeChargeObject r = StripeCharge.StripeChargeObject.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.StripeChargeObject objJSON2Apex = new StripeCharge.StripeChargeObject(System.JSON.createParser(json));
        System.assert(objJSON2Apex != null);
        System.assert(objJSON2Apex.id == null);
        System.assert(objJSON2Apex.object_Z == null);
        System.assert(objJSON2Apex.api_version == null);
        System.assert(objJSON2Apex.created == null);
        System.assert(objJSON2Apex.data == null);
        System.assert(objJSON2Apex.livemode == null);
        System.assert(objJSON2Apex.pending_webhooks == null);
        System.assert(objJSON2Apex.request == null);
        System.assert(objJSON2Apex.type_Z == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Object_Z objObject_Z = new StripeCharge.Object_Z(System.JSON.createParser(json));
        System.assert(objObject_Z != null);
        System.assert(objObject_Z.id == null);
        System.assert(objObject_Z.object_Z == null);
        System.assert(objObject_Z.amount == null);
        System.assert(objObject_Z.amount_refunded == null);
        //System.assert(objObject_Z.application == null);
        //System.assert(objObject_Z.application_fee == null);
        System.assert(objObject_Z.balance_transaction == null);
        System.assert(objObject_Z.captured == null);
        System.assert(objObject_Z.created == null);
        System.assert(objObject_Z.currency_data == null);
        System.assert(objObject_Z.customer == null);
        System.assert(objObject_Z.description == null);
        System.assert(objObject_Z.destination == null);
        //System.assert(objObject_Z.dispute == null);
        //System.assert(objObject_Z.failure_code == null);
        //System.assert(objObject_Z.failure_message == null);
        //System.assert(objObject_Z.fraud_details == null);
        System.assert(objObject_Z.invoice == null);
        System.assert(objObject_Z.livemode == null);
        System.assert(objObject_Z.metadata == null);
        //System.assert(objObject_Z.on_behalf_of == null);
        //System.assert(objObject_Z.order == null);
        //System.assert(objObject_Z.outcome == null);
        System.assert(objObject_Z.paid == null);
        //System.assert(objObject_Z.receipt_email == null);
        //System.assert(objObject_Z.receipt_number == null);
        System.assert(objObject_Z.refunded == null);
        System.assert(objObject_Z.refunds == null);
        //System.assert(objObject_Z.review == null);
        //System.assert(objObject_Z.shipping == null);
        System.assert(objObject_Z.source == null);
        //System.assert(objObject_Z.source_transfer == null);
        System.assert(objObject_Z.statement_descriptor == null);
        System.assert(objObject_Z.status == null);
        //System.assert(objObject_Z.transfer_group == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Outcome objOutcome = new StripeCharge.Outcome(System.JSON.createParser(json));
        System.assert(objOutcome != null);
        System.assert(objOutcome.network_status == null);
        //System.assert(objOutcome.reason == null);
        System.assert(objOutcome.risk_level == null);
        System.assert(objOutcome.seller_message == null);
        System.assert(objOutcome.type_Z == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Refunds objRefunds = new StripeCharge.Refunds(System.JSON.createParser(json));
        System.assert(objRefunds != null);
        System.assert(objRefunds.object_Z == null);
        System.assert(objRefunds.data == null);
        System.assert(objRefunds.has_more == null);
        System.assert(objRefunds.total_count == null);
        System.assert(objRefunds.url == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Request objRequest = new StripeCharge.Request(System.JSON.createParser(json));
        System.assert(objRequest != null);
        System.assert(objRequest.id == null);
        //System.assert(objRequest.idempotency_key == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Data objData = new StripeCharge.Data(System.JSON.createParser(json));
        System.assert(objData != null);
        System.assert(objData.object_data == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Fraud_details objFraud_details = new StripeCharge.Fraud_details(System.JSON.createParser(json));
        System.assert(objFraud_details != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        StripeCharge.Source objSource = new StripeCharge.Source(System.JSON.createParser(json));
        System.assert(objSource != null);
        System.assert(objSource.id == null);
        System.assert(objSource.object_Z == null);
        System.assert(objSource.address_city == null);
        System.assert(objSource.address_country == null);
        System.assert(objSource.address_line1 == null);
        System.assert(objSource.address_line1_check == null);
        System.assert(objSource.address_line2 == null);
        System.assert(objSource.address_state == null);
        System.assert(objSource.address_zip == null);
        System.assert(objSource.address_zip_check == null);
        System.assert(objSource.brand == null);
        System.assert(objSource.country == null);
        System.assert(objSource.customer == null);
        System.assert(objSource.cvc_check == null);
        System.assert(objSource.dynamic_last4 == null);
        System.assert(objSource.exp_month == null);
        System.assert(objSource.exp_year == null);
        System.assert(objSource.fingerprint == null);
        System.assert(objSource.funding == null);
        System.assert(objSource.last4 == null);
        System.assert(objSource.metadata == null);
        System.assert(objSource.name == null);
        System.assert(objSource.tokenization_method == null);*/
        StripeCharge.getCharge('test232');
        StripeCharge.create('test232', 12.5,'test232','test232','test1234','test232','test232','test@test.com', '435345435','123456');
        StripeCharge.create('test232', 12.5,'test232','test232','test1234','test232','test232','test@test.com', '435345435','123456','','','','','','');
        StripeCharge.create('test232', 12.5,'test232','test232','test1234','test232','test232','test@test.com', '435345435','123456','','','','','','',7.75);
        StripeCharge.create('test232', 12.5,'test232','test232','test1234','test232','test232','test@test.com', '435345435','123456','','','','','','',7.75,0.00);
    }
}