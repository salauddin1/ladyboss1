public class Facebook_LoginController {
	public List<FacebookPage__c> facebookPageList{get;set;}
    public String clientId {get;set;}
    public String uri_redirect {get;set;}
	public Facebook_LoginController() {
        webhook_Id_Secret__mdt id_secret = [SELECT client_id__c,client_secret__c,DeveloperName,redirect_uri__c FROM webhook_Id_Secret__mdt where DeveloperName = 'webhookAppmdt'];
		clientId = id_secret.client_id__c;
        uri_redirect = id_secret.redirect_uri__c;
        System.debug(clientId+'     '+uri_redirect);
		facebookPageList = [SELECT id,about__c,category__c,username__c,Page_Id__c,Last_Fetch_Time__c,isActive__c,Page_Name__c,Page_Token__c FROM FacebookPage__c WHERE isActive__c = true];
		System.debug(facebookPageList);
    } 

	public void disConnect(){
		for(FacebookPage__c fb : facebookPageList){
			fb.isActive__c = false;
		}
		if(facebookPageList.size()>0){
			update facebookPageList;
		}
		facebookPageList = [SELECT id,about__c,category__c,username__c,Page_Id__c,Last_Fetch_Time__c,isActive__c,Page_Name__c,Page_Token__c FROM FacebookPage__c WHERE isActive__c = true];
	}
}