@isTest
private class CampaignMemberBatchDel_Queue_Test {
    static testmethod void test1() {
        Lead ld1 = new Lead(Status = 'New', LastName = 'test1Name',Company = 'testing1company');
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        Lead ld3 = new Lead(Status = 'New', LastName = 'test3Name',Company = 'testing3company');
        Lead ld4 = new Lead(Status = 'New', LastName = 'test4Name',Company = 'testing4company');
        Lead ld5 = new Lead(Status = 'New', LastName = 'test5Name',Company = 'testing5company');
        List<Lead> ldList = new List<Lead>{ld1,ld2,ld3,ld4,ld5};
            insert ldList;
        
        List<Campaign> cList = new List<Campaign>();
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1';        
        cList.add(c1);
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Lead Days 2-5';
        cList.add(c2);
       
        Campaign c3 =new  Campaign();
        c3.Name = 'Campaign Buy Day 1';        
        cList.add(c3);
        
        
        Campaign c4 =new  Campaign();
        c4.Name = 'Campaign Buy Day 1';        
        cList.add(c4);
        
        Campaign c5 =new  Campaign();
        c5.Name = 'Campaign Buy Day 1';        
        cList.add(c5);
        insert cList;
        
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c1.Id,LeadId = ld1.Id , Created_Date__c = d1);
        
        Date d2 = dt.addDays(-6);
        CampaignMember cm2 = new CampaignMember(CampaignId = c2.Id,LeadId = ld2.Id , Created_Date__c = d2);
        
         Date d3 = dt.addDays(-5);
        CampaignMember cm3 = new CampaignMember(CampaignId = c3.Id,LeadId = ld3.Id , Created_Date__c = d3);
        
        Date d4 = dt.addDays(-5);
        CampaignMember cm4 = new CampaignMember(CampaignId = c4.Id,LeadId = ld4.Id , Created_Date__c = d4);
        
        Date d5 = dt.addDays(-6);
        CampaignMember cm5 = new CampaignMember(CampaignId = c5.Id,LeadId = ld5.Id , Created_Date__c = d5);
        List<CampaignMember> cml= new List<CampaignMember>{cm1,cm2,cm3,cm4,cm5}; 
            insert cml;
        
        Test.startTest();        
        System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>{cm4}, new List<campaignMember>{cm5}));
        System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>{cm1}, new List<campaignMember>{cm2},new List<campaignMember>{cm3}));
        Test.stopTest();
        
    }
}