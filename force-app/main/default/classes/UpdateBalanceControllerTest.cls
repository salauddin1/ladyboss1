@isTest
public class UpdateBalanceControllerTest {
    public static testMethod void updateBal(){
        Contact ct = new Contact();
        ct.LastName = 'test';
        ct.Available_Commission__c = 400;
        insert ct;
        test.startTest();
        UpdateBalanceController.getContact(ct.Id);
        UpdateBalanceController.updateContact('reasonText', '403', ct.Id);
        test.stopTest();
    }
}