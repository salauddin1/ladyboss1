@isTest
public class MC_OneClickSurvey_PhoneTeam_Test {
    public static testMethod void test2(){
        //Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
        //cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        
        cs.Origin='My Origin';
        insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Poor');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test3(){
        //Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
        //cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        
        cs.Origin='My Origin';
        insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Excellent');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test4(){
       // Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
        //cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        
        cs.Origin='My Origin';
        insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Ok');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test5(){
       // Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
       // cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        
        cs.Origin='My Origin';
        insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Great');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test6(){
        //Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
        //cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        
        cs.Origin='My Origin';
        //insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id','8577757');
        pageRef.getParameters().put('res','Great');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test7(){
       // Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
        //cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        cs.One_Click_Survey__c = 'Ok';
        cs.Origin='My Origin';
        insert cs;
        MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Great');
        
        oppC.successRedirect();
        
        
    }
     public static testMethod void test8(){
       // Id ChargeRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        case cs =  new case();
       // cs.recordTypeId = ChargeRecordTypeId ;
        cs.Status='My Status';
        cs.One_Click_Survey__c = 'Ok';
        cs.Origin='My Origin';
        insert cs;
          
       MC_OneClickSurvey_PhoneTeam oppC = new MC_OneClickSurvey_PhoneTeam();
        PageReference pageRef = Page.MC_OneClickSurvey_PhoneTeam;
        oppC.customerComment = 'hello';
        oppC.selectObject = 'Other';
        oppC.showServerMessage=true;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',cs.id);
        pageRef.getParameters().put('res','Ok');
        oppC.changeEvent();
        
        oppC.save();
        
        
    }
    
}