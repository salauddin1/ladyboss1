//Getting subscription details from stripe  for given subscription id
global class Subscriptions {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/subscriptions/';
    
    global String id;
    global String status; // 'trialing', 'active', 'past_due', 'canceled', or 'unpaid'
    global Integer start;
    global StripePlan plan;
    global String customer_id;
    global Integer quantity;
    global Boolean cancel_at_period_end;
    global Long current_period_start;
    global Long current_period_end;
    global Long ended_at; 
    global Long trial_start; 
    global Long trial_end;
    global Long canceled_at;
    global StripeError error;
    
    global static Subscriptions getSubscription(String subscriptionId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + subscriptionId);
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{ "id": "sub_8UfvbTOhALKzNy", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1463862018, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "created": 1463862018, "current_period_end": 1558470018, "current_period_start": 1526934018, "customer": "cus_8UfvnhVxP6E7gy", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_18RWRXFzCf73siP0wyYdrGUg", "object": "subscription_item", "billing_thresholds": null, "created": 1463862019, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_8UfvbTOhALKzNy" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_8UfvbTOhALKzNy" }, "latest_invoice": "in_1CUKVkFzCf73siP0ka6Oqdrp", "livemode": false, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1463862018, "status": "active", "tax_percent": null, "trial_end": 1558470018, "trial_start": 1558470018 }') ;
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response );
        List<Object> cards = (List<Object>)results.get('data');
        system.debug('#### cards #### '+ cards);
        try {
            
            Subscriptions o = Subscriptions.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** Subscriptions object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    global static void updateSubscription(String subscriptionId,Decimal taxPercentage){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + subscriptionId);
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        http.setBody('tax_percent='+taxPercentage);
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        
        try {
            if (!Test.isRunningTest()) {
                hs = con.send(http);
            }
            for (OpportunityLineItem opli: [SELECT id,Tax_Percentage_s__c,totalPrice,Tax_Amount__c FROM OpportunityLineItem where Subscription_Id__c =:subscriptionId limit 1]){
                opli.Tax_Amount__c = opli.TotalPrice*taxPercentage*0.01;
                opli.Tax_Percentage_s__c = taxPercentage;
                update opli;
            }
        } catch (CalloutException e) {
            System.debug('ERROR  '+e.getMessage());
        }
        
        
    }
    global static void updateSubscriptionForBatch(String subscriptionId,Decimal taxPercentage){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + subscriptionId);
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        http.setBody('tax_percent='+taxPercentage);
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        
        try {
            if (!Test.isRunningTest()) {
                hs = con.send(http);
            }
        } catch (CalloutException e) {
            System.debug('ERROR  '+e.getMessage());
        }
        
        
    }
    @Future(callout=true)
    global static void updateSubscriptionForOppUpdate(String subscriptionId){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + subscriptionId);
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        http.setBody('tax_percent=');
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        
        try {
            if (!Test.isRunningTest()) {
                hs = con.send(http);
            }
            for (OpportunityLineItem opli: [SELECT id,Tax_Percentage_s__c,Tax_Amount__c FROM OpportunityLineItem where Subscription_Id__c =:subscriptionId limit 1]){
                opli.Tax_Percentage_s__c = 0.00;
                opli.Tax_Amount__c = 0.00;
                update opli;
            }
        } catch (CalloutException e) {
            System.debug('ERROR  '+e.getMessage());
        }
        
    }
    public static Subscriptions parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        return (Subscriptions) System.JSON.deserialize(json, Subscriptions.class);
    }
}