public class UpdateSpecificPlanId {
    public OpportunityLineItem oppitem {get;set;}
    public UpdateSpecificPlanId(ApexPages.StandardController stdController){
        this.oppitem =(OpportunityLineItem)stdController.getRecord();    
    }
    public void updatesubplan(){
        Map<String, Object> results = new Map<String, Object>();
        Map<String, String> subPlanMap = new Map<String, String>();
        Map<String, String> SubLineItemMap = new Map<String, String>();
        Map<String, String> oldNewplanMap = new Map<String, String>();
        String recordId = '';
        try{
            oppitem =[SELECT Id,Subscription_Id__c,Plan_Upgraded__c,Product_Plan_Id__c,Status__c FROM OpportunityLineItem where ID =:oppitem.Id and Status__c !='InActive'];   
            HttpRequest http = new HttpRequest();
            recordId =oppitem.Subscription_Id__c;
            http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppitem.Subscription_Id__c);
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            String response;
            Integer statusCode;
            If (!Test.isRunningTest()) {
                hs = con.send(http);
                response = hs.getBody();
                statusCode = hs.getStatusCode();
            }
            If (statusCode == 200) {
                results = (Map<String, Object>)JSON.deserializeUntyped(response);                    
                Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('items');
                List<Object> data = (List<Object>)lstCustomers.get('data');
                Map<String, Object> obmap = (Map<String, Object>)data[0];
                Map<String, Object> planmap =  (Map<String, Object>)results.get('plan');
                subPlanMap.put(oppitem.Subscription_Id__c, String.valueOf(planmap.get('id')));
                SubLineItemMap.put(oppitem.Subscription_Id__c, String.valueOf(obmap.get('id')));
            }
            if (subPlanMap.size()>0) {
                for (Plan_Upgrade__c pu : [SELECT id,New_Plan_ID__c,Old_Plan_ID__c FROM Plan_Upgrade__c WHERE Old_Plan_ID__c IN:subPlanMap.values() AND enabled__c = true]) {
                    oldNewplanMap.put(pu.Old_Plan_ID__c, pu.New_Plan_ID__c);
                }
            }
            if (oldNewplanMap.size()>0) {
                if (SubLineItemMap.containsKey(oppitem.Subscription_Id__c) && oldNewplanMap.containsKey(subPlanMap.get(oppitem.Subscription_Id__c))) {
                    HttpRequest http1 = new HttpRequest();
                    http1.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppitem.Subscription_Id__c);
                    http1.setMethod('POST');
                    Blob headerValuenew = Blob.valueOf(StripeAPI.ApiKey + ':');
                    String authorizationHeadernew = 'BASIC ' + EncodingUtil.base64Encode(headerValuenew);
                    http1.setHeader('Authorization', authorizationHeadernew);
                    http1.setHeader('content-type', 'application/x-www-form-urlencoded');
                    http1.setBody('items[0][id]='+SubLineItemMap.get(oppitem.Subscription_Id__c)+'&items[0][plan]='+oldNewplanMap.get(subPlanMap.get(oppitem.Subscription_Id__c))+'&prorate=false');
                    Http con1 = new Http();
                    HttpResponse hs1 = new HttpResponse();
                    if (!Test.isRunningTest()) {
                        hs1 = con1.send(http1);
                    }
                    if (hs1.getStatusCode()==200) {
                        oppitem.Plan_Upgraded__c = true;
                    }
                }
            }
            update oppitem;
        }catch(Exception e){
            system.debug('e.getMessage()-->'+e.getMessage());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'UpgradeSpecificPlanId',
                    'Batch',
                    recordId,
                    e
                )
            );
        }
    }
}