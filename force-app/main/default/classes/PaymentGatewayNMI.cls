global class PaymentGatewayNMI {
    /*private static String endpoint = 'https://secure.nmi.com/api/transact.php';
    private static String username = 'cloudcreations';
    private static String password = 'cloud2029';*/
    
    public static HttpResponse HttpRequest(Map<String,String> requestMap){
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        
        requestMap.put('username',nmip.User__c);
        requestMap.put('password',nmip.Password__c);
        String strGet = prepareRequest(requestMap);
        
        Http http = new Http();
        HttPRequest req = new HttPRequest();
        req.setEndpoint(endpoint + 'query.php?' + strGet);
        req.setMethod('GET');      
        HttpResponse res = new HttpResponse();
		
        res = http.send(req);            
        system.debug('PaymentGatewayNMI.HttpRequest res.getBody(): '+ res.getBody());		
		system.debug('PaymentGatewayNMI.HttpRequest res.getStatusCode(): '+res.getStatusCode());
        return res;
    }
    
    @future(callout=true)
    public static void HttpDirectPost(String obj, String objId, Map<String,String> requestMap) {       
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        String username = nmip.User__c;
        String password = nmip.Password__c;
        
        Http http = new Http();
        HttPRequest req = new HttPRequest();
        req.setEndpoint(endpoint + 'transact.php');
        req.setMethod('POST');      

        String strPost = prepareRequest(requestMap);
        system.debug('##strPost: '+strPost);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
        req.setBody(strPost);
        HttpResponse res = new HttpResponse();
		
        res = http.send(req);            
        if(res.getStatusCode()==200){
        	String bodyRes;
        	bodyRes = res.getBody();
        	//bodyRes = 'response=1&responsetext=Customer Added&authcode=&transactionid=&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1770269911';
            Map<String,String> mapValues = new Map<String,String>();
            List<String> lstResult = bodyRes.split('&');
            for(String key : lstResult)
            {
                 mapValues.put(key.substring(0,key.indexOf('=')),key.substring(key.indexOf('=')+1));
            }
        
        	if(mapValues.get('response')=='1'){
                if(obj=='customer'){
                    system.debug('###mapValues: '+mapValues);
                    Account acc = new Account();
                    acc.Id = objId;
                    acc.NMI_External_ID__c = mapValues.get('customer_vault_id');
                    acc.NMI_Invalid_Message__c = '';
                    ApexUtil.isTriggerInvoked = true;
                    update acc;
                    ApexUtil.isTriggerInvoked = false;
                }          
            }else if(mapValues.get('response')=='2' || mapValues.get('response')=='3'){ 
                if(obj=='customer'){ Account acc = new Account(); acc.Id = objId; acc.NMI_Invalid_Message__c = mapValues.get('responsetext'); ApexUtil.isTriggerInvoked = true; update acc; ApexUtil.isTriggerInvoked = false; }
            }
        }
        system.debug('PaymentGatewayNMI.HttpDirectPost res.getBody(): '+ res.getBody());		
		//system.debug('PaymentGatewayNMI.HttpDirectPost res.getStatusCode(): '+res.getStatusCode());
    }
    
    webservice static String pay(String oppId){
        Opportunity opp = [Select Id, Total__c, AccountId, Amount, External_ID__c, NMI_External_ID__c, Subscription__c, StageName, Account.NMI_External_ID__c From Opportunity WHERE Id=:oppId];
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        String username = nmip.User__c;
        String password = nmip.Password__c;
        
        if((opp.External_ID__c==null || opp.NMI_External_ID__c==null) && opp.Subscription__c==null && opp.StageName!='Approved'){
            if(opp.Total__c>0){
                if(opp.Account.NMI_External_ID__c!=null){
                    Boolean errorValidation = false;
                    List<OpportunityLineItem> lstOlis = [Select Id, Product2.ProductCode, Product2.External_ID__c, Product2.Name, Quantity, UnitPrice, PricebookEntry.Product2.Type__c From OpportunityLineItem WHERE OpportunityId=:opp.Id];
                    for(OpportunityLineItem oli : lstOlis){
                        if(oli.PricebookEntry.Product2.Type__c!='NMI'){
                            errorValidation = true;
                        }
                    }
                    
                    if(!errorValidation){
                        Http http = new Http();
                        HttPRequest req = new HttPRequest();
                        req.setEndpoint(endpoint + 'transact.php');
                        req.setMethod('POST');      
                
                        Map<String,String> requestMap = new Map<String,String>();
                        requestMap.put('username',nmip.User__c);
                        requestMap.put('password',nmip.Password__c);
                        requestMap.put('customer_vault_id',opp.Account.NMI_External_ID__c);
                        requestMap.put('amount',String.valueOf(opp.Total__c));
                        requestMap.put('merchant_defined_field_1',opp.Id);
                        Integer cont = 0;
                        for(OpportunityLineItem oli : lstOlis){
                            cont++;
                            requestMap.put('item_product_code_'+cont,oli.Product2.External_ID__c);
                            requestMap.put('item_description_'+cont,oli.Product2.Name);
                            requestMap.put('item_quantity_'+cont,String.valueOf(oli.Quantity));
                            requestMap.put('item_unit_cost_'+cont,String.valueOf(oli.UnitPrice));
                        }
                        
                        String strPost = prepareRequest(requestMap);
                        system.debug('##strPost: '+strPost);
                        req.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
                        req.setBody(strPost);
                        HttpResponse res = new HttpResponse();
                        
                        res = http.send(req);            
                        system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
                        if(res.getStatusCode()==200){
                            String bodyRes;
                            bodyRes = res.getBody();
                            //bodyRes = 'response=1&responsetext=Customer Added&authcode=&transactionid=&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1770269911';
                            Map<String,String> mapValues = new Map<String,String>();
                            List<String> lstResult = bodyRes.split('&');
                            for(String key : lstResult)
                            {
                                 mapValues.put(key.substring(0,key.indexOf('=')),key.substring(key.indexOf('=')+1));
                            }
                        
                            if(mapValues.get('response')=='1'){
                                system.debug('###mapValues: '+mapValues);
                                Opportunity oppUpdate = new Opportunity();
                                oppUpdate.Id = oppId;
                                oppUpdate.NMI_External_ID__c = mapValues.get('transactionid');
                                oppUpdate.StageName = 'Approved';
                                oppUpdate.Paid__c = true;
                                //oppUpdate.NMI_Message__c = bodyRes;
                                ApexUtil.isTriggerInvoked = true;
                                update oppUpdate;
                                   
                                Payment__c pay = new Payment__c();
                                pay.Opportunity__c = oppId;
                                pay.Account__c = opp.AccountId;
                                pay.Status__c = 'Succeeded';
                                pay.External_ID__c = mapValues.get('transactionid');
                                pay.Amount__c = opp.Total__c;
                                pay.Paid__c = true;
                                insert pay;
                                ApexUtil.isTriggerInvoked = false;  
                            }else if(mapValues.get('response')=='2' || mapValues.get('response')=='3'){
                                Opportunity oppUpdate = new Opportunity(Id = oppId,StageName = 'Declined',NMI_Message__c = mapValues.get('responsetext'));
                                ApexUtil.isTriggerInvoked = true;
                                update oppUpdate;
                                Payment__c pay = new Payment__c(Opportunity__c = oppId,Account__c = opp.AccountId,Status__c = 'Failed',External_ID__c = mapValues.get('transactionid'),Amount__c = opp.Total__c);
                                insert pay;
                                
                                ApexUtil.isTriggerInvoked = false;
                                return mapValues.get('responsetext');
                            }
                            
                            system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
                            system.debug('PaymentGatewayNMI.pay res.getStatusCode(): '+res.getStatusCode());
                        }
                    }else{ return 'Select the correct payment processing.'; }
                }else{ return 'The account is not registered in NMI.'; }
            }else{ return 'The amount must be greater than 0.'; }
        }else{ return 'This opportunity already registers a payment.'; }
        
        return 'Ok';
    }
    
    public static void HttpPayment(Opportunity opportunity, List<OpportunityLineItem> olis){
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        String username = nmip.User__c;
        String password = nmip.Password__c;
        HttPRequest request = new HttpRequest();
        request.setEndpoint(endpoint + 'transact.php');
        request.setMethod('POST');      
        Map<String,String> requestMap = new Map<String,String>();
        requestMap.put('username', nmip.User__c);
        requestMap.put('password', nmip.Password__c);
        requestMap.put('customer_vault_id', opportunity.Account.NMI_External_ID__c);
        requestMap.put('amount', String.valueOf(opportunity.Total__c));
        requestMap.put('merchant_defined_field_1', opportunity.Id);
        Integer cont = 0;
        for(OpportunityLineItem oli : olis){
            cont++;
            requestMap.put('item_product_code_'+cont, oli.Product2.External_ID__c);
            requestMap.put('item_description_'+cont, oli.Product2.Name);
            requestMap.put('item_quantity_'+cont, String.valueOf(oli.Quantity));
            requestMap.put('item_unit_cost_'+cont, String.valueOf(oli.UnitPrice));
        }
        String strPost = prepareRequest(requestMap);
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
        request.setBody(strPost);
        Http http = new Http();
        HttpResponse res = http.send(request);            
        system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
        if(res.getStatusCode()==200){
            String bodyRes;
            bodyRes = res.getBody();
            Map<String,String> mapValues = new Map<String,String>();
            List<String> lstResult = bodyRes.split('&');
            for(String key : lstResult) mapValues.put(key.substring(0,key.indexOf('=')),key.substring(key.indexOf('=')+1));
            Payment__c payment = new Payment__c();
            payment.Opportunity__c = opportunity.id;
            payment.Account__c = opportunity.AccountId;
            payment.External_ID__c = mapValues.get('transactionid');
            system.debug('###mapValues: '+mapValues);
            if(mapValues.get('response')=='1'){
                opportunity.NMI_External_ID__c = mapValues.get('transactionid');
                opportunity.StageName = 'Approved';
                opportunity.Paid__c = true;
                opportunity.NMI_Message__c = bodyRes;
                payment.Status__c = 'Succeeded';
                payment.Amount__c = opportunity.Total__c;
                payment.Paid__c = true;
            }
            else if(mapValues.get('response')=='2' || mapValues.get('response')=='3'){
                opportunity.StageName = 'Declined';
                opportunity.NMI_Message__c = mapValues.get('responsetext');
                payment.Status__c = 'Failed';
            }
            try{
                ApexUtil.isTriggerInvoked = true;
                update opportunity;
                insert payment;
                ApexUtil.isTriggerInvoked = false;
            }
            catch(DmlException e){
                System.debug('Error Executing PaymentGatewayNMI.HttpPayment');
            }
            system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
            system.debug('PaymentGatewayNMI.pay res.getStatusCode(): '+res.getStatusCode());
        }
    }
    
    @AuraEnabled
    public static String payLightning(Id oppId){
        Opportunity opp = [Select Id, Total__c, AccountId, Amount, External_ID__c, NMI_External_ID__c, Subscription__c, StageName, Account.NMI_External_ID__c From Opportunity WHERE Id=:oppId];
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        String username = nmip.User__c;
        String password = nmip.Password__c;
        
        if((opp.External_ID__c==null || opp.NMI_External_ID__c==null) && opp.Subscription__c==null && opp.StageName!='Approved'){
            if(opp.Total__c>0){
                if(opp.Account.NMI_External_ID__c!=null){
                    Boolean errorValidation = false;
                    List<OpportunityLineItem> lstOlis = [Select Id, Product2.ProductCode, Product2.External_ID__c, Product2.Name, Quantity, UnitPrice, PricebookEntry.Product2.Type__c From OpportunityLineItem WHERE OpportunityId=:opp.Id];
                    for(OpportunityLineItem oli : lstOlis){
                        if(oli.PricebookEntry.Product2.Type__c!='NMI'){
                            errorValidation = true;
                        }
                    }
                    
                    if(!errorValidation){
                        Http http = new Http();
                        HttPRequest req = new HttPRequest();
                        req.setEndpoint(endpoint + 'transact.php');
                        req.setMethod('POST');      
                
                        Map<String,String> requestMap = new Map<String,String>();
                        requestMap.put('username',nmip.User__c);
                        requestMap.put('password',nmip.Password__c);
                        requestMap.put('customer_vault_id',opp.Account.NMI_External_ID__c);
                        requestMap.put('amount',String.valueOf(opp.Total__c));
                        requestMap.put('merchant_defined_field_1',opp.Id);
                        Integer cont = 0;
                        for(OpportunityLineItem oli : lstOlis){
                            cont++;
                            requestMap.put('item_product_code_'+cont,oli.Product2.External_ID__c);
                            requestMap.put('item_description_'+cont,oli.Product2.Name);
                            requestMap.put('item_quantity_'+cont,String.valueOf(oli.Quantity));
                            requestMap.put('item_unit_cost_'+cont,String.valueOf(oli.UnitPrice));
                        }
                        
                        String strPost = prepareRequest(requestMap);
                        system.debug('##strPost: '+strPost);
                        req.setHeader('Content-Type', 'application/x-www-form-urlencoded'); 
                        req.setBody(strPost);
                        HttpResponse res = new HttpResponse();
                        
                        res = http.send(req);            
                        system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
                        if(res.getStatusCode()==200){
                            String bodyRes;
                            bodyRes = res.getBody();
                            //bodyRes = 'response=1&responsetext=Customer Added&authcode=&transactionid=&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1770269911';
                            Map<String,String> mapValues = new Map<String,String>();
                            List<String> lstResult = bodyRes.split('&');
                            for(String key : lstResult)
                            {
                                 mapValues.put(key.substring(0,key.indexOf('=')),key.substring(key.indexOf('=')+1));
                            }
                        
                            if(mapValues.get('response')=='1'){
                                system.debug('###mapValues: '+mapValues);
                                Opportunity oppUpdate = new Opportunity();
                                oppUpdate.Id = oppId;
                                oppUpdate.NMI_External_ID__c = mapValues.get('transactionid');
                                oppUpdate.StageName = 'Approved';
                                oppUpdate.Paid__c = true;
                                //oppUpdate.NMI_Message__c = bodyRes;
                                ApexUtil.isTriggerInvoked = true;
                                update oppUpdate;
                                   
                                Payment__c pay = new Payment__c();
                                pay.Opportunity__c = oppId;
                                pay.Account__c = opp.AccountId;
                                pay.Status__c = 'Succeeded';
                                pay.External_ID__c = mapValues.get('transactionid');
                                pay.Paid__c = true;
                                pay.Amount__c = opp.Total__c;
                                insert pay;
                                ApexUtil.isTriggerInvoked = false;  
                            }else if(mapValues.get('response')=='2' || mapValues.get('response')=='3'){
                                Opportunity oppUpdate = new Opportunity(Id = oppId,StageName = 'Declined',NMI_Message__c = mapValues.get('responsetext'));
                                ApexUtil.isTriggerInvoked = true;
                                update oppUpdate;
                                Payment__c pay = new Payment__c(Opportunity__c = oppId,Account__c = opp.AccountId,Status__c = 'Failed',External_ID__c = mapValues.get('transactionid'),Amount__c = opp.Total__c);
                                insert pay;
                                
                                ApexUtil.isTriggerInvoked = false;
                                return mapValues.get('responsetext');
                            }
                            
                            system.debug('PaymentGatewayNMI.pay res.getBody(): '+ res.getBody());		
                            system.debug('PaymentGatewayNMI.pay res.getStatusCode(): '+res.getStatusCode());
                        }
                    }else{ return 'Select the correct payment processing.'; }
                }else{ return 'The account is not registered in NMI.'; }
            }else{ return 'The amount must be greater than 0.';}
        }else{ return 'This opportunity already registers a payment.';}
        
        return 'Ok';
    }
    
    /*public static HttpResponse HttpSendPayment(String firstname, String lastname, String address, String city, String state, String zip, String amount, String ccnumber, String ccexp, String cvv) {       
        NMI_Parameters__c nmip = NMI_Parameters__c.getOrgDefaults();
        String endpoint = nmip.Endpoint__c;
        String username = nmip.User__c;
        String password = nmip.Password__c;
        
        Http http = new Http();
        HttPRequest req = new HttPRequest();
        req.setEndpoint(endpoint + 'transact.php');
        req.setMethod('POST');      

        Map<String,String> requestMap = new Map<String,String>();
        requestMap.put('username',username);
        requestMap.put('password',password);
        requestMap.put('firstname',firstname);
        requestMap.put('lastname',lastname);
        requestMap.put('address',address);
        requestMap.put('city',city);
        requestMap.put('state',state);
        requestMap.put('zip',zip);
        requestMap.put('payment','creditcard');
        requestMap.put('type','sale');
        requestMap.put('amount',amount);
        requestMap.put('ccnumber',ccnumber);
        requestMap.put('ccexp',ccexp);
        requestMap.put('cvv',cvv);
        
        String strPost = prepareRequest(requestMap);
        
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');  
        req.setBody(strPost);
        HttpResponse res = new HttpResponse();
		
        res = http.send(req);            
        system.debug('PaymentGatewayNMI.HttpSendPayment res.getBody(): '+ res.getBody());		
		system.debug('PaymentGatewayNMI.HttpSendPayment res.getStatusCode(): '+res.getStatusCode());
        return res;
    }*/

    public static String prepareRequest(Map<String, String> requestMap) {
        if (requestMap.size() == 0) return '';
        
        String buffer = '';          
        Integer aux = 1;
        
        for (String key: requestMap.keySet()) {
            if(aux==1){
        		buffer += key+'='+ requestMap.get(key);
            }
            else{
            	buffer += '&'+key+'='+ requestMap.get(key);                
            }
            aux++;
        }
        System.debug('String:'+buffer);
        return buffer;
    }
    
    public static List<Dom.XmlNode> XmlParser(String strXml) {
        List<Dom.XmlNode> childlist = new List<Dom.XmlNode>();
        Dom.Document doc = new Dom.Document();
        Integer childElementCount =0;
        doc.load(strXml);
        Dom.XMLNode rootElement = doc.getRootElement();
        String rootElementName = rootElement.getName();
        for(Dom.XmlNode childelement : rootElement.getChildElements()){
            childlist.add(childelement);
            childElementCount++;
        }
        return childlist;
    }
}