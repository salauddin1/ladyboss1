//batch class to insert/update  all the subscriptions from stripe to salesforce
global class  SubscriptionStripeBatchService implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
    
    global database.querylocator start(database.batchablecontext bc){
        string query ;
        if(!test.isRunningTest()){
        Batch_Soql_Limit__c limitSize  = [Select subscription_Batch_Size__c From Batch_Soql_Limit__c limit 1];
        integer sizeLim = Integer.valueOf(limitSize.subscription_Batch_Size__c)  ;
        query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where  Subscription_Processed__c=false  order by CreatedDate desc limit :sizeLim ';
        }
        if(test.isRunningTest()) {
         query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where   Subscription_Processed__c =false limit 1';
        
        }
        return database.getquerylocator( query ); 
    }
    global void execute(database.BatchableContext bc,list<Stripe_Profile__c> stripeProfileList){
        String jsonStr;
        String recTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        // To add the line item list to map from salesforce
        List<Stripe_Product_Mapping__c> mcs = Stripe_Product_Mapping__c.getall().values();
        Map<string,string> productMap = new Map<string,string>();
        Set<String> prodNameSet  = new Set<String>();
        for(Stripe_Product_Mapping__c spm: mcs){
            if(spm.Stripe_Nick_Name__c!=null)
                productMap.put(spm.Stripe_Nick_Name__c,spm.Salesforce_Product__c);
            
        }
        
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
        List<PricebookEntry> pbe1 = [SELECT Id, Name, Pricebook2Id, Product2Id,Product2.Name FROM PricebookEntry  where Pricebook2ID=:standardPb.id and isActive=true];
        Map<string,string> priceBookMap = new Map<string,string>();
        for(PricebookEntry pe : pbe1) {
            priceBookMap.put(pe.Name,pe.ID);
        }
        Map<string,string> stripeProfIdMap = new Map<string,string> ();
        Set<Id> setProfId = new Set<Id>();
        Set<Id> setContactId = new Set<Id>();
        Map<string,string> conStripeMap1 = new Map<string,string> ();
        Map<string,string> conStripeMap = new Map<string,string> ();
        for(Stripe_Profile__c sp : stripeProfileList) {
            stripeProfIdMap.put(sp.Stripe_Customer_Id__c,sp.id);
            setProfId.add(sp.id);
            conStripeMap1.put(sp.Stripe_Customer_Id__c,sp.id );
            conStripeMap.put(sp.Stripe_Customer_Id__c,sp.Customer__c );
            setContactId.add(sp.Customer__c);
            
            
        }
        List<Stripe_Profile__c> updateList = new  List<Stripe_Profile__c> ();
        
        for(Stripe_Profile__c stripeProf : stripeProfileList){
            stripeProf.Subscription_Processed__c = true;
            updateList.add(stripeProf);
            List< Opportunity > OppOb     = new List< Opportunity >();
            List<Stripe_Profile__c> lstStrp  = new List<Stripe_Profile__c>();
            lstStrp.add(stripeProf); 
            String customerId = lstStrp[0].Stripe_Customer_Id__c;
            jsonStr = StripeBatchHandler.getAllSubscriptions(lstStrp,recTypeID );
            List<card__c> lstCard = [select id,CustomerID__c,Stripe_Card_Id__c from card__c where (CustomerID__c = : customerId) OR (Stripe_Profile__c IN:setProfId )] ;
            set<Id> cardIdSet = new set<Id>();
            Map<string,string> cardMap = new Map<string,string> ();
            Map<string,string> cardProfMap = new Map<string,string> ();
            for (card__c cd : lstCard){
                cardIdSet.add(cd.id);
                cardMap.put(cd.CustomerID__c,cd.id);
                cardProfMap.put(cd.Stripe_Card_Id__c,cd.id);
            }
            
            List<Opportunity> oppList = [select id,Card__c,Stripe_Charge_Id__c,Subscription__c,Contact__c, (SELECT Id, Subscription_Id__c FROM OpportunityLineItems)  from opportunity where Card__c IN :cardIdSet or Contact__c in: setContactId];
            Map<string,string> oppExistingMap = new Map<string,string> ();
            Map<string,string> oppSubChargeMap = new Map<string,string> ();
            Map<string,Opportunity> oppExMap = new Map<string,Opportunity> ();
            Map<string,string> oliMap = new Map<string,string>();
            
            for(Opportunity o : oppList){
                if (o.OpportunityLineItems.size () > 0) {
                    for(OpportunityLineItem oli :o.OpportunityLineItems){
                        oliMap.put(oli.Subscription_Id__c,oli.Subscription_Id__c);
                    }
                }
                oppExistingMap.put(o.Subscription__c,o.Card__c);
                oppSubChargeMap.put(o.Stripe_Charge_Id__c,o.Card__c);
                oppExMap.put(o.Subscription__c,o);
            }
            
            
            System.debug('jsonStr'+jsonStr);
            StripeListSubscriptionClone.Charges varCharges;
            JSONParser parse;
            if(jsonStr!= null && jsonStr != ''){
                parse = JSON.createParser(jsonStr); 
                
                varCharges = (StripeListSubscriptionClone.Charges)parse.readValueAs(StripeListSubscriptionClone.Charges.class);
                
                List<Opportunity > oppUpdateList =  new List<Opportunity >();
                List<Opportunity > oppInsertList =  new List<Opportunity >();
                Datetime createdDate;
            
            
                
                for(StripeListSubscriptionClone.Data subscription : varCharges.data){  
                    if(subscription.id!=null){
                        
                        if(!oppExistingMap.ContainsKey(subscription.id) && !oppSubChargeMap.ContainsKey(subscription.id) && !oliMap.ContainsKey(subscription.id)){
                            // System.debug('==>'+subscription.plan.id );
                            Opportunity opp = new Opportunity();
                            //check for the contact
                            if(conStripeMap.containsKey(subscription.customer))
                                opp.Contact__c = conStripeMap.get(subscription.customer);
                            //chcek for the profile
                            if(stripeProfIdMap.containsKey(subscription.customer))
                                opp.Stripe_Profile__c = stripeProfIdMap.get(subscription.customer);
                            //check for the card
                            if(cardMap.ContainsKey(subscription.customer)!=null)
                                opp.Card__c = cardMap.get(subscription.customer);
                            if (recTypeID !='' && recTypeID !=null)
                                opp.RecordTypeId = recTypeID ;
                            opp.Name= subscription.id;
                            opp.stageName= 'Closed Won';
                            opp.CloseDate = Date.valueOf(System.today());
                            opp.Stripe_Charge_Id__c = subscription.id;
                            opp.Subscription__c = subscription.id;
                            //opp.Success_Failure_Message__c = subscription.status; 
                            opp.Status__c    =      subscription.status;     
                            opp.Statement_Descriptor__c = subscription.statement_descriptor;
                            opp.Description= subscription.description;
                            opp.Currency__c = subscription.currency_data;
                            
                            Integer quant = 0;
                            if(subscription.items!=null){
                                Integer amountSum = 0;
                                for (StripeListSubscriptionClone.Data_x dt : subscription.items.data) {
                                    if(dt.plan.nickname!=null || dt.plan.nickname !=''){
                                        opp.Name= dt.plan.nickname;
                                        
                                    }
                                    else if( subscription.statement_descriptor!=null) {
                                        opp.Name= subscription.statement_descriptor;
                                    } else {
                                        opp.Name= subscription.id;
                                    }
                                    quant =quant + Integer.valueOF(dt.quantity);
                                    Integer decimalAmount = 0;
                                    if(dt.plan.amount!=null || dt.plan.amount !=''){
                                        decimalAmount = amountSum + Integer.valueOF(dt.plan.amount);
                                        
                                    }
                                    opp.Amount= Decimal.valueOf(decimalAmount)*0.01;
                                }
                                
                            }
                            else
                            {
                                opp.Name= subscription.id;
                            }
                            
                            opp.Quantity__c= quant ;
                            if(subscription.status !='canceled' && opp.Name!=null )
                                oppInsertList.add(opp);
                        }
                        else{
                            
                            Opportunity opp = oppExMap.get(subscription.id);
                            if(opp!=null){
                                opp.Status__c    =      subscription.status; 
                                opp.Success_Failure_Message__c = subscription.status;  
                                if(subscription.plan!=null){
                                    opp.Amount = Decimal.valueOf(subscription.plan.amount)*0.01;
                                }                       
                                if(stripeProfIdMap.containsKey(subscription.customer)){
                                    oppUpdateList.add(opp);
                                }
                                
                            }
                            
                        }
                    }
                }
                
                if(oppInsertList.size() > 0){
                    
                    insert oppInsertList;
                    
                }   
                if(oppUpdateList.size() > 0){
                    
                    Upsert oppUpdateList;
                    
                }
                
                Map<string,Opportunity > oppSubIdMap = new Map<string,Opportunity >();
                Set<Id> oppIdSet = new Set<Id> ();
                for(Opportunity opp : oppInsertList) {
                    if(opp.Subscription__c != null){
                        oppSubIdMap.put(opp.Subscription__c,opp); 
                        oppIdSet.add(opp.id);
                    }
                }
                for(Opportunity opp : oppUpdateList) {
                    if(opp.Subscription__c != null){
                        oppSubIdMap.put(opp.Subscription__c,opp);
                        oppIdSet.add(opp.id);
                    }
                }
                List<OpportunityLineItem> exOppLine = [select id,Subscription_Id__c from OpportunityLineItem where OpportunityId IN : oppIdSet];
                Map<string,string> exsistingOppLineMap = new Map<string,string>();
                for(OpportunityLineItem oppLineItem :exOppLine) {
                    exsistingOppLineMap.put(oppLineItem.Subscription_Id__c,oppLineItem.Subscription_Id__c);
                }
                List<OpportunityLineItem> oppLinelist = new List<OpportunityLineItem>();
                for(StripeListSubscriptionClone.Data subscription : varCharges.data){  
                    if(subscription.id!=null){
                        if(subscription.items!=null){
                            //System.debug('--------de-'+subscription.items.data);
                            for (StripeListSubscriptionClone.Data_x dt : subscription.items.data) {
                                
                                if(dt.plan.nickname!=null){
                                    if(oppSubIdMap.containsKey(String.valueOf(subscription.id)) && !exsistingOppLineMap.containsKey(String.valueOf(subscription.id)) ) {
                                        OpportunityLineItem oppli = new OpportunityLineItem(); 
                                        oppli.OpportunityId = oppSubIdMap.get(String.valueOf(subscription.id)).id;
                                        if(subscription.status=='active'){
                                        oppli.Success_Failure_Message__c='customer.subscription.created' ;
                                        }
                                        if(priceBookMap.containsKey(productMap.get(String.valueOf(dt.plan.nickname))))  {
                                            System.debug('----opp.NickNameEnter----'+String.valueOf(dt.plan.nickname));
                                            oppli.PricebookEntryId= priceBookMap.get(productMap.get(dt.plan.nickname));
                                            oppli.Subscription_Id__c =String.valueOf(subscription.id); 
                                            oppli.Quantity = Integer.valueOf(dt.quantity);
                                            String amt = String.ValueOf(dt.plan.amount);
                                            oppli.TotalPrice =  decimal.valueOf(amt )*0.01;
                                            oppLinelist.add( oppli);
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                }
                if(oppLinelist.size() > 0) {
                    insert oppLinelist;
                }
                
            }
            if(updateList.size() > 0)
                    update updateList;
        }
        
        
    }
    global void finish(database.batchablecontext bc){
        
    }
}