@isTest
public class ScheduleAsyncLimitChecker_Test {
    @IsTest
    static void methodName(){
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        String cronExp = '0 15 * * * ?';
        Test.startTest();
        System.schedule('jobName', cronExp, new ScheduleAsyncLimitChecker());
        Test.stopTest();
        
    }
}