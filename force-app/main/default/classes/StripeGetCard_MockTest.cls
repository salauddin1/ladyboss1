@isTest
global class StripeGetCard_MockTest implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //res.setBody('{"object":"list","data":[{"id":"card_1D7P9xBwLSk1v1ohQezYknMQ","object":"card","address_city":"Delhi","address_country":"US","address_line1":"T5-1D","address_line1_check":"pass","address_line2":null,"address_state":"HR","address_zip":"121004","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_DYWAlCN3vsdH4k","cvc_check":null,"dynamic_last4":null,"exp_month":3,"exp_year":2021,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","name":"Ashish Sharma","tokenization_method":null}]}');
        res.setBody('{"object":"list","data":[{"id":"cus_DYoZR3370en1xo","object":"customer","account_balance":0,"email":"ashish.sharma.devsfdc75@gmail.com","sources":{"object":"list","data":[{"id":"card_1D7gx9BwLSk1v1ohm8EGFUHY","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"MasterCard","country":"US","customer":"cus_DYoZR3370en1xo","cvc_check":"pass","dynamic_last4":null,"exp_month":12,"exp_year":2023,"fingerprint":"5jbJsE5hvFew4hP5","funding":"credit","last4":"4444","metadata":{},"name":null,"tokenization_method":null}]}}],"has_more":true,"url":"/v1/customers"}');
        res.setStatusCode(200);
        
        return res;
    }
}