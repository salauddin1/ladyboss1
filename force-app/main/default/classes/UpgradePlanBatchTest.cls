@isTest
public class UpgradePlanBatchTest {
    @testSetup static void methodSetup(){
        
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        ContactTriggerFlag.isContactBatchRunning=true;
        
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        contact con = new contact();
        con.lastName='lastName';
        con.firstName='firstName';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_Eug8juCETidS33';
        sp.Customer__c = con.id;
        insert sp;
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        card.Stripe_Card_Id__c = 'sdfdfsdf';
        Date d = date.Today();
        card.Is_Customer_Default_Card__c = true;
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        card.Token__c = '';
        insert card;
        ACH_Account__c ac = new ACH_Account__c(); 
        ac.ACH_Token__c = null;
        ac.Name = 'test';
        ac.Stripe_Profile__c = sp.id;
        ac.Stripe_ACH_Id__c='achiddskfdsf';
        insert ac;
        
        Product2 pro = new Product2();
        pro.Name='pro';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmemberships';
        pro.IsActive=true;
        insert pro; 
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        opportunity op  = new opportunity();
        op.name=pro.Name+'CLUB';
        op.Contact__c =con.Id;
        op.Card__c = card.Id;
        op.StageName = 'Closed Won';
        op.RecordTypeId = devRecordTypeId;
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        OpportunityLineItem olpo = new OpportunityLineItem();        
        olpo.Product2Id =pro.id;
        olpo.OpportunityId = op.Id;
        olpo.Quantity = 3;
        olpo.UnitPrice = 2.00;
        olpo.Subscription_Id__c='sub_EypY8AdfVmqvVo';
        olpo.Subscription_Plan_ID__c = '1yrmembership';
        olpo.Start__c=System.today()-1;
        olpo.End__c=System.today();    
        olpo.Opportunity_Unique_Name__c = op.Id;
        olpo.PricebookEntryId = standardPrice.Id;
        olpo.Status__c='Active';
        olpo.Remind_Me_Later__c= true;
        Date d1 = Date.today().addDays(3);
        olpo.End__c = d1;
        insert olpo;
    }
    @IsTest
    static void methodName(){
        Test.startTest();
        UpgradePlanBatch b = new UpgradePlanBatch();
        database.executebatch(b,1);
        Test.stopTest();
        
    }
}