//@Developer - Ashish Sharma
//@Description - This job update campaignMember Created_Date_Time__c Field .

global class CampaignMemberAgeShiftBatch implements Database.Batchable<sObject> {
    public String query;
    global CampaignMemberAgeShiftBatch () {
        
    }    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        query =  'SELECT Id,Name,CampaignId,Product__c,Campaign.Type,Marketing_Cloud_Tag__c,Contact.Product__c,Created_Date__c,Created_Date_Time__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Marketing_Cloud_Tags__c,Campaign.Name, Age__c, Age_In_Hours__c ,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = false and Campaign.Marketing_Cloud_Tags__c = null and Contact.Amazon_Customer__c=false and Campaign.Type=\'Phone Team\'';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc,List<CampaignMember> campaignMember){
        for(CampaignMember campMember : campaignMember){
            if(campMember.Created_Date__c != null){
                campMember.Created_Date_Time__c =  campMember.Created_Date__c;
            }
        }
        Database.update(campaignMember) ;
    }
    global void finish(Database.BatchableContext bc){
        
    } 
}