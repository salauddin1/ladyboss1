public class Five9Listitem_Batch_UpdateQuable implements Schedulable {
     
    public Static void UpdateListItem() {
        Integer lim=Integer.valueOf(label.LimitLabelforFive9ListItem);
        List<Five9LSP__Five9_List_Item__c> UpdateItems=new List<Five9LSP__Five9_List_Item__c>();
        List<Five9LSP__Five9_List_Item__c> UpdatedList =  [SELECT Id,Name,Five9LSP__Five9_List__c,Product__c,Created_Date__c, Age__c,Five9LSP__Lead__c,Five9LSP__Contact__c,Custom_Sync__c FROM Five9LSP__Five9_List_Item__c where Five9LSP__Five9_List__r.List_Auto_Movement_Active__c = true AND Five9LSP__Sync_Status__c='Pending' ORDER BY Five9LSP__Five9_List__r.Five9_Processing_Priority__c limit:lim];
        for(Five9LSP__Five9_List_Item__c Items : UpdatedList){
            Items.Custom_Sync__c=true;
            UpdateItems.add(Items);
        }
          Update UpdateItems;
      }
    public void execute(SchedulableContext SC) {
        Five9Listitem_Batch_UpdateQuable.UpdateListItem();
    }
}