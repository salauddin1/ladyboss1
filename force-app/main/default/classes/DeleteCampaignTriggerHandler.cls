public class DeleteCampaignTriggerHandler {
    public static void handlerMethod(List<CampaignMember> oldVal) {
        List< Triggers_activation__c > trigList = [select id , CampaignMemberdeleteHelper__c from Triggers_activation__c limit 1];
        if(trigList.size() > 0 && trigList[0].CampaignMemberdeleteHelper__c){
           try{
                List<CampaignMember> camMemberIdSet = new List<CampaignMember>(); 
                Set<id> conIdSet = new Set<id>();
                Set<id> campMIdSet = new Set<id>(); 
                for(CampaignMember camMem : oldVal){
                    if(camMem.ContactId != null && camMem.campaignId != null){
                        //camMemberIdSet.add(camMem); 
                        campMIdSet.add(camMem.id);
                        conIdSet.add(camMem.ContactId);
                        
                    }    
                }
                camMemberIdSet = [select id,contactId,campaign.Name from CampaignMember where Campaign.type =: 'Phone Team' and ID IN :campMIdSet];
                 if( camMemberIdSet != null && !camMemberIdSet.isEmpty()){
                    CampaignMemberdeleteHelper.getCampOppByContactDeleteTrigger(camMemberIdSet,conIdSet);
                }
                if(Test.isRunningTest()){
                    System.debug(1/0);
                }
            }Catch(Exception ex){
                ApexDebugLog apex=new ApexDebugLog(); 
                apex.createLog(
                    new ApexDebugLog.Error('DeleteCampaignTriggerHandler','handlerMethod',' ',ex));
            }
        }
    }
}