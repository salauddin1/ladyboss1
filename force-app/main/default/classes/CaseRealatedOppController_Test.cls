@isTest
public class CaseRealatedOppController_Test {
    public static testmethod void test(){
        Test.startTest();
        Contact con = new Contact();
        con.lastName ='sharma';
        insert con;
        Case cases = new Case();
        cases.ContactId = con.Id;
       insert cases;
        Opportunity oppS = new Opportunity();
        oppS.Name ='ashishJi';
        oppS.Contact__c =con.Id;
        oppS.CloseDate = Date.Today();
        oppS.StageName = 'Pending';
        
        insert oppS;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Customer__c = con.Id;
        insert sp;
        Card__c card = new Card__c();
        card.Contact__c = con.Id;
        insert card;
        String ConId = String.valueOf(con.id) ;
        String CaseId1 = String.valueOf(cases.id) ;
        
        
        CaseRealatedOppController.getCase(CaseId1);
        CaseRealatedOppController.getCard(ConId);
        CaseRealatedOppController.getOpportunityCharge(ConId);
        CaseRealatedOppController.getOpportunitySub(ConId);
        CaseRealatedOppController.getStripeProfile(ConId);
       Test.stopTest();  
    }
}