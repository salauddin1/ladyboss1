global class BatchShipstation_statusUpdate implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([Select ID, orderStatus__c,orderId__c from ShipStation_Orders__c where orderStatus__c!=: 'shipped'  AND orderStatus__c!=: 'cancelled'  AND orderId__c !=: null]);
    }
    global void execute(Database.BatchableContext BC, List<ShipStation_Orders__c> records) {  
        system.debug('==============records================'+records);
        String endpoint;
        String header;
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        try {                  
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            
            endpoint = ShipStationUser.Domain_Name__c+'/orders/'+records[0].orderId__c;
            req.setEndpoint(endpoint);
            req.setHeader('Authorization','Basic ' +header);
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('GET'); 
            res = http.send(req);
            system.debug('=======res=========='+res);
            if(res.getStatusCode() == 200 && res.getBody() != null) {
                system.debug('=======res=res.getBody() !========='+res.getBody() );
                Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                if(String.valueOf(ordResult.get('orderStatus')) != null) {
                    records[0].orderStatus__c = String.valueOf(ordResult.get('orderStatus'));
                    system.debug('=======res=========='+String.valueOf(ordResult.get('orderStatus')));
                    ShipstationOrderStatusShippedBatch.flag = true;
                    update records;    
                }
                
            }
            
        }
        catch (Exception e) {         
            System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber() );           
        }
    } 
    
    global void finish(Database.BatchableContext BC){    
    }
}