@isTest 
public class RefreshSubscription_Test {
    static testMethod void testMethodPostive1() 
    { 
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='cus_E3vxtf5JjMiXv6';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_E3vxtf5JjMiXv6';
        sp.Customer__c =  con.id;
        insert sp;
        
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        
        Map<String,Opportunity> oomap = new Map<String,Opportunity>();
        Map<String,List<OpportunityLineItem>> olmap = new Map<String,List<OpportunityLineItem>>();
        List<OpportunityLineItem> oplinelist = new List<OpportunityLineItem>();
        List<Opportunity> oplist = new List<Opportunity>();
        
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        oplist.add(op);
        
        oomap.put('nonClub', op);
        //insert oomap;
        
        Pricebook2 standardPricebook = new Pricebook2(
    Id = Test.getStandardPricebookId(),
    IsActive = true
);

// Run an update DML on the Pricebook2 record
// This is the weird workaround that enables IsStandard to become true
// on the PricebookEntry record
update standardPricebook;

// Re-Query for the Pricebook2 record, for debugging
standardPricebook = [SELECT IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];

// This should return true now
system.assertEquals(true, standardPricebook.IsStandard, 'The Standard Pricebook should now return IsStandard = true');

// Create the Product
Product2 testProduct = new Product2(
    Name = 'Test Product', 
    IsActive = true
);
insert testProduct;

// Create the PricebookEntry
PricebookEntry testPbe = new PricebookEntry(
    Pricebook2Id = standardPricebook.Id,
    Product2Id = testProduct.Id,
    UnitPrice = 100,
    IsActive = true
);

insert testPbe;

// Re-Query the PBE
testPbe = [SELECT Id, Pricebook2.IsStandard FROM PricebookEntry];
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =testProduct.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = testPbe.Id;
        
        insert ol;
        
        oplinelist.add(ol);
        
        olmap.put('nonClub', oplinelist);
        
        set<id> idset = new set<id>();
        idset.add(op.id);
        
       
        test.StartTest();
        List<Stripe_Profile__c > lstProfAddOne =  new List<Stripe_Profile__c > ();
        lstProfAddOne.add(sp);
        string json ='{ "object": "list", "data": [ { "id": "sub_E3y8dz9J1rQcTc", "billing": "charge_automatically", "billing_cycle_anchor": 1543500542, "cancel_at_period_end": false, "canceled_at": null, "created": 1543500542, "current_period_end": 1546092542, "current_period_start": 1543500542, "customer": "cus_E3vxtf5JjMiXv6", "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_E3y8LCPUnNjIWP", "object": "subscription_item", "created": 1543500542, "metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan", "active": true, "aggregate_usage": null, "amount": 23700, "created": 1531344662, "currency": "usd", "metadata": { }, "nickname": "Transformation System-$237", "product": "prod_DDGQVEaH03NJ8p", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3y8dz9J1rQcTc" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3y8dz9J1rQcTc" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan", "active": true, "amount": 23700, "created": 1531344662, "currency": "usd", "metadata": { }, "nickname": "Transformation System-$237", "product": "prod_DDGQVEaH03NJ8p", "usage_type": "licensed" }, "quantity": 1, "start": 1543500542, "status": "active", "trial_start": null }, { "id": "sub_E3w06lbkkG5bXk", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1543492602, "cancel_at_period_end": false, "canceled_at": null, "created": 1543492602, "current_period_end": 1546084602, "current_period_start": 1543492602, "customer": "cus_E3vxtf5JjMiXv6", "days_until_due": null, "default_source": null, "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_E3w0Sih6FhFZWD", "object": "subscription_item", "created": 1543492603, "metadata": { }, "plan": { "id": "plan_Cfm5v2MQnwKoEn", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3995, "billing_scheme": "per_unit", "created": 1523620686, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss BURN 1 Bottle Subscription-PT", "product": "prod_Cfm5LjVF043yUB", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3w06lbkkG5bXk" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3w06lbkkG5bXk" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_Cfm5v2MQnwKoEn", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3995, "billing_scheme": "per_unit", "created": 1523620686, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss BURN 1 Bottle Subscription-PT", "product": "prod_Cfm5LjVF043yUB", "usage_type": "licensed" }, "quantity": 1, "start": 1543492602, "status": "active", "tax_percent": null, "trial_end": null, "trial_start": null }, { "id": "sub_E3vyfoeWH3pJyj", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1543492466, "cancel_at_period_end": false, "canceled_at": 1543492578, "created": 1543492466, "current_period_end": 1546084466, "current_period_start": 1543492466, "customer": "cus_E3vxtf5JjMiXv6", "days_until_due": null, "default_source": null, "discount": null, "ended_at": 1543492578, "items": { "object": "list", "data": [ { "id": "si_E3vyy0yd7O7BjF", "object": "subscription_item", "created": 1543492467, "metadata": { }, "plan": { "id": "plan_Cfm4Ixcq9kTs6R", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3994, "billing_scheme": "per_unit", "created": 1523620634, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss FUEL 1 Bottle Subscription-PT", "product": "prod_CflbLdbf4gT36M", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3vyfoeWH3pJyj" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3vyfoeWH3pJyj" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_Cfm4Ixcq9kTs6R", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3994, "billing_scheme": "per_unit", "created": 1523620634, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss FUEL 1 Bottle Subscription-PT", "product": "prod_CflbLdbf4gT36M", "usage_type": "licensed" }, "quantity": 1, "start": 1543492466, "status": "canceled" } ], "has_more": false, "url": "/v1/subscriptions" }';
       
       Id RecordTypeIdContact = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
       // RefreshSubscription.getAllSubscriptions(lstProfAddOne,json );
        RefreshSubscription.getContactId(con.id );
        //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
        test.stopTest();
        
    }
    static testMethod void testMethodPostive2() 
    { 
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='cus_E3vxtf5JjMiXv6';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_E3vxtf5JjMiXv6';
        sp.Customer__c =  con.id;
        insert sp;
        
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        
        Map<String,Opportunity> oomap = new Map<String,Opportunity>();
        Map<String,List<OpportunityLineItem>> olmap = new Map<String,List<OpportunityLineItem>>();
        List<OpportunityLineItem> oplinelist = new List<OpportunityLineItem>();
        List<Opportunity> oplist = new List<Opportunity>();
        
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        oplist.add(op);
        
        oomap.put('nonClub', op);
        //insert oomap;
        
        Pricebook2 standardPricebook = new Pricebook2(
    Id = Test.getStandardPricebookId(),
    IsActive = true
);

// Run an update DML on the Pricebook2 record
// This is the weird workaround that enables IsStandard to become true
// on the PricebookEntry record
update standardPricebook;

// Re-Query for the Pricebook2 record, for debugging
standardPricebook = [SELECT IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];

// This should return true now
system.assertEquals(true, standardPricebook.IsStandard, 'The Standard Pricebook should now return IsStandard = true');

// Create the Product
Product2 testProduct = new Product2(
    Name = 'Test Product', 
    IsActive = true
);
insert testProduct;

// Create the PricebookEntry
PricebookEntry testPbe = new PricebookEntry(
    Pricebook2Id = standardPricebook.Id,
    Product2Id = testProduct.Id,
    UnitPrice = 100,
    IsActive = true
);

insert testPbe;

// Re-Query the PBE
testPbe = [SELECT Id, Pricebook2.IsStandard FROM PricebookEntry];
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =testProduct.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = testPbe.Id;
        ol.Subscription_Id__c='sub_E3y8dz9J1rQcTc';
        
        insert ol;
        
        oplinelist.add(ol);
        
        olmap.put('nonClub', oplinelist);
        
        set<id> idset = new set<id>();
        idset.add(op.id);
        
       
        test.StartTest();
        List<Stripe_Profile__c > lstProfAddOne =  new List<Stripe_Profile__c > ();
        lstProfAddOne.add(sp);
        string json ='{ "object": "list", "data": [ { "id": "sub_E3y8dz9J1rQcTc", "billing": "charge_automatically", "billing_cycle_anchor": 1543500542, "cancel_at_period_end": false, "canceled_at": null, "created": 1543500542, "current_period_end": 1546092542, "current_period_start": 1543500542, "customer": "cus_E3vxtf5JjMiXv6", "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_E3y8LCPUnNjIWP", "object": "subscription_item", "created": 1543500542, "metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan", "active": true, "aggregate_usage": null, "amount": 23700, "created": 1531344662, "currency": "usd", "metadata": { }, "nickname": "Transformation System-$237", "product": "prod_DDGQVEaH03NJ8p", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3y8dz9J1rQcTc" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3y8dz9J1rQcTc" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan", "active": true, "amount": 23700, "created": 1531344662, "currency": "usd", "metadata": { }, "nickname": "Transformation System-$237", "product": "prod_DDGQVEaH03NJ8p", "usage_type": "licensed" }, "quantity": 1, "start": 1543500542, "status": "active", "trial_start": null }, { "id": "sub_E3w06lbkkG5bXk", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1543492602, "cancel_at_period_end": false, "canceled_at": null, "created": 1543492602, "current_period_end": 1546084602, "current_period_start": 1543492602, "customer": "cus_E3vxtf5JjMiXv6", "days_until_due": null, "default_source": null, "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_E3w0Sih6FhFZWD", "object": "subscription_item", "created": 1543492603, "metadata": { }, "plan": { "id": "plan_Cfm5v2MQnwKoEn", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3995, "billing_scheme": "per_unit", "created": 1523620686, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss BURN 1 Bottle Subscription-PT", "product": "prod_Cfm5LjVF043yUB", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3w06lbkkG5bXk" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3w06lbkkG5bXk" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_Cfm5v2MQnwKoEn", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3995, "billing_scheme": "per_unit", "created": 1523620686, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss BURN 1 Bottle Subscription-PT", "product": "prod_Cfm5LjVF043yUB", "usage_type": "licensed" }, "quantity": 1, "start": 1543492602, "status": "active", "tax_percent": null, "trial_end": null, "trial_start": null }, { "id": "sub_E3vyfoeWH3pJyj", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1543492466, "cancel_at_period_end": false, "canceled_at": 1543492578, "created": 1543492466, "current_period_end": 1546084466, "current_period_start": 1543492466, "customer": "cus_E3vxtf5JjMiXv6", "days_until_due": null, "default_source": null, "discount": null, "ended_at": 1543492578, "items": { "object": "list", "data": [ { "id": "si_E3vyy0yd7O7BjF", "object": "subscription_item", "created": 1543492467, "metadata": { }, "plan": { "id": "plan_Cfm4Ixcq9kTs6R", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3994, "billing_scheme": "per_unit", "created": 1523620634, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss FUEL 1 Bottle Subscription-PT", "product": "prod_CflbLdbf4gT36M", "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_E3vyfoeWH3pJyj" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_E3vyfoeWH3pJyj" }, "livemode": false, "metadata": { }, "plan": { "id": "plan_Cfm4Ixcq9kTs6R", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3994, "billing_scheme": "per_unit", "created": 1523620634, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "LadyBoss FUEL 1 Bottle Subscription-PT", "product": "prod_CflbLdbf4gT36M", "usage_type": "licensed" }, "quantity": 1, "start": 1543492466, "status": "canceled" } ], "has_more": false, "url": "/v1/subscriptions" }';
       
       Id RecordTypeIdContact = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        //RefreshSubscription.getAllSubscriptions(lstProfAddOne,json );
        RefreshSubscription.getContactId(con.id );
        //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
        test.stopTest();
        
    }
    }