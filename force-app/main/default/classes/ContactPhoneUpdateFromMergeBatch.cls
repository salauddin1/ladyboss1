@RestResource(urlMapping='/Contact/updateContacts')
global class ContactPhoneUpdateFromMergeBatch {

  @HttpPost
  global static void updateContact(){
    RestRequest request = RestContext.request;  
    RestResponse response = RestContext.response;  
    String jSONRequestBody=request.requestBody.toString().trim();
    System.debug(jSONRequestBody);
      ContactTriggerFlag.isContactBatchRunning = true;
    List<Contact> contactList = (List<Contact>)JSON.deserialize(jSONRequestBody,List<Contact>.class);
    update contactList;
  }  

  global static void doCall(List<Contact> contactToUpdateList,String sessionId){
    String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm(); 
    String restAPIURL = sfdcURL + '/services/apexrest/Contact/updateContacts';  
     
    HttpRequest httpRequest = new HttpRequest();    
    httpRequest.setMethod('POST');   
    httpRequest.setHeader('Authorization', 'OAuth ' + sessionId);        
    httpRequest.setHeader('Authorization', 'Bearer ' + sessionId); 
    httpRequest.setHeader('Content-Type','application/json');
    httpRequest.setEndpoint(restAPIURL);  
    String response = '';
    Http http = null;
    String body = '';
    try {
      http = new Http();
      body = JSON.serialize(contactToUpdateList);
      //System.debug('test : '+body); 
      httpRequest.setBody(body);
      HttpResponse httpResponse = http.send(httpRequest);  
    } catch( System.Exception e) {  
      System.debug('ERROR: '+ e);  
      //throw e;  
    }
  }
    
    public static void dummy()  {
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        
        
    }  
}