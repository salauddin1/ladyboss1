public class subTaskOfTaskListController {
    @AuraEnabled
    public static list<Subsequent_Task__c> getSubTask(Id caseId){
        List<Subsequent_Task__c> subsequentList = [select id,name,Charge_Id__c,Task_id__c,Amount__c,createdDate ,Note__c ,IsNote__c from Subsequent_Task__c Where  case_id__c =: caseId AND IsNote__c =: false];
        return(subsequentList);
    }
    @AuraEnabled
    public static list<Subsequent_Task__c> getSubTaskNote(Id caseId){
        List<Subsequent_Task__c> subsequentNoteList = [select id, createdDate,case_id__c  , Note__c ,IsNote__c from Subsequent_Task__c Where  case_id__c =: caseId AND IsNote__c =: true];
        return(subsequentNoteList);
    }
}