global class Testbatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
	global Testbatch(){
		Contact con = new Contact();
        con.Id = '0031700000yi358AAA';
        con.Description = 'testing batch';
        update con;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select id from Product2 limit 1');
    }
    global void execute(Database.BatchableContext BC, List<Product2> scope){
       HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/sub_GOCjnOjJVBtTxr');
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            
                hs = con.send(http);
           
        } else {
            hs.setBody('{ "id": "sub_8UfvbTOhALKzNy", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1463862018, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "created": 1463862018, "current_period_end": 1558470018, "current_period_start": 1526934018, "customer": "cus_8UfvnhVxP6E7gy", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_18RWRXFzCf73siP0wyYdrGUg", "object": "subscription_item", "billing_thresholds": null, "created": 1463862019, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_8UfvbTOhALKzNy" } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_8UfvbTOhALKzNy" }, "latest_invoice": "in_1CUKVkFzCf73siP0ka6Oqdrp", "livemode": false, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1463862018, "status": "active", "tax_percent": null, "trial_end": 1558470018, "trial_start": 1558470018 }') ;
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        Product2 p = new Product2();
        p.id = '01tf4000001Oxx6AAC';
        p.Height__c = 2.00;
        update p;
    }
    global void finish(Database.BatchableContext BC){
        
    }
}