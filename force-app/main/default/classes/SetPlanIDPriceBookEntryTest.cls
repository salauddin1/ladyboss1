@isTest
public class SetPlanIDPriceBookEntryTest {
    @IsTest
    static void methodName(){
        Product2 p = new Product2();
        p.Name = 'test';
        p.Stripe_Plan_Id__c = 'tesingy';
        p.Price__c = 2.00;
        Insert p;
        Id pbid= Test.getStandardPricebookId();
        PricebookEntry pbeNew = new PricebookEntry();
        pbeNew.IsActive = true;
        pbeNew.Pricebook2Id = pbId;
        pbeNew.Product2Id = p.Id;
        pbeNew.UnitPrice = p.Price__c;
        insert pbeNew;
        Test.startTest();
        SetPlanIDPriceBookEntry b = new SetPlanIDPriceBookEntry();
        Database.executeBatch(b);
        Test.stopTest();
        
    }
}