@isTest
public class BatchSendTaskEmailTest {
    public static testMethod void BatchSendTaskTest(){
        Contact ct = new Contact();
        ct.LastName = 'test Name';
        insert ct;
        Task tk = new Task();
        tk.Contact__c = ct.Id;
        tk.Status = 'open';
        tk.Subject = 'teast  Subject';
        tk.Priority = 'Normal';
        tk.OwnerId = userinfo.getUserId();
        tk.WhoId = ct.Id;
        tk.Five9__Five9AgentExtension__c = '10001';
        tk.CallDurationInSeconds = 150;
        insert tk;
        
        test.startTest();
        BatchSendTaskEmail be = new BatchSendTaskEmail();
        Database.executeBatch(be);
        String sch = '20 30 8 10 2 ?';
        System.schedule('jobName', sch , be);
        test.stopTest();
    }
}