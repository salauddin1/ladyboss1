public  class RESTLimitResource {
    public DailyAsyncApexExecutions dailyAsyncApexExecutions;
    public static RESTLimitResource getLimitsResp() {
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HttpResponse hs1 = new HttpResponse();
        req.setMethod('GET');
        String url = System.URL.getSalesforceBaseURL().toExternalForm() +'/services/data/v40.0/limits';
        String mySessionID = fetchUserSessionId();
        System.debug('Session id '+mySessionID);
        req.setHeader('Authorization', 'Bearer ' + mySessionID); 
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setEndpoint(url);
        if(!Test.isRunningTest()){
            try {
                    hs1 = http.send(req);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }
        
        String response;
        if (!Test.isRunningTest()) {
            response = hs1.getBody();
        }else {
            response = '{ "AnalyticsExternalDataSizeMB" : { "Max" : 40960, "Remaining" : 40960 }, "ConcurrentAsyncGetReportInstances" : { "Max" : 200, "Remaining" : 200 }, "ConcurrentSyncReportRuns" : { "Max" : 20, "Remaining" : 20 }, "DailyAnalyticsDataflowJobExecutions" : { "Max" : 60, "Remaining" : 60 }, "DailyAnalyticsUploadedFilesSizeMB" : { "Max" : 51200, "Remaining" : 51200 }, "DailyApiRequests" : { "Max" : 223000, "Remaining" : 205813, "Ant Migration Tool" : { "Max" : 0, "Remaining" : 0 }, "Chatter Desktop" : { "Max" : 0, "Remaining" : 0 }, "Chatter Mobile for BlackBerry" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Bulk" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Partner" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.com)" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.eu)" : { "Max" : 0, "Remaining" : 0 }, "Force.com IDE" : { "Max" : 0, "Remaining" : 0 }, "LiveMessage for Salesforce" : { "Max" : 0, "Remaining" : 0 }, "LiveText for Salesforce (QA)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Chatter" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Files" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Marketing Cloud" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Mobile Dashboards" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Social Customer Service (SCS)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Touch" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Android" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Outlook" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for iOS" : { "Max" : 0, "Remaining" : 0 }, "SalesforceA" : { "Max" : 0, "Remaining" : 0 }, "SalesforceIQ" : { "Max" : 0, "Remaining" : 0 }, "Workbench" : { "Max" : 0, "Remaining" : 0 } }, "DailyAsyncApexExecutions" : { "Max" : 325000, "Remaining" : 9999 }, "DailyBulkApiRequests" : { "Max" : 10000, "Remaining" : 9301, "Ant Migration Tool" : { "Max" : 0, "Remaining" : 0 }, "Chatter Desktop" : { "Max" : 0, "Remaining" : 0 }, "Chatter Mobile for BlackBerry" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Bulk" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Partner" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.com)" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.eu)" : { "Max" : 0, "Remaining" : 0 }, "Force.com IDE" : { "Max" : 0, "Remaining" : 0 }, "LiveMessage for Salesforce" : { "Max" : 0, "Remaining" : 0 }, "LiveText for Salesforce (QA)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Chatter" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Files" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Marketing Cloud" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Mobile Dashboards" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Social Customer Service (SCS)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Touch" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Android" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Outlook" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for iOS" : { "Max" : 0, "Remaining" : 0 }, "SalesforceA" : { "Max" : 0, "Remaining" : 0 }, "SalesforceIQ" : { "Max" : 0, "Remaining" : 0 }, "Workbench" : { "Max" : 0, "Remaining" : 0 } }, "DailyDurableGenericStreamingApiEvents" : { "Max" : 200000, "Remaining" : 199712 }, "DailyDurableStreamingApiEvents" : { "Max" : 200000, "Remaining" : 200000 }, "DailyGenericStreamingApiEvents" : { "Max" : 10000, "Remaining" : 10000, "Ant Migration Tool" : { "Max" : 0, "Remaining" : 0 }, "Chatter Desktop" : { "Max" : 0, "Remaining" : 0 }, "Chatter Mobile for BlackBerry" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Bulk" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Partner" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.com)" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.eu)" : { "Max" : 0, "Remaining" : 0 }, "Force.com IDE" : { "Max" : 0, "Remaining" : 0 }, "LiveMessage for Salesforce" : { "Max" : 0, "Remaining" : 0 }, "LiveText for Salesforce (QA)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Chatter" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Files" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Marketing Cloud" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Mobile Dashboards" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Social Customer Service (SCS)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Touch" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Android" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Outlook" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for iOS" : { "Max" : 0, "Remaining" : 0 }, "SalesforceA" : { "Max" : 0, "Remaining" : 0 }, "SalesforceIQ" : { "Max" : 0, "Remaining" : 0 }, "Workbench" : { "Max" : 0, "Remaining" : 0 } }, "DailyStandardVolumePlatformEvents" : { "Max" : 25000, "Remaining" : 25000 }, "DailyStreamingApiEvents" : { "Max" : 200000, "Remaining" : 199892, "Ant Migration Tool" : { "Max" : 0, "Remaining" : 0 }, "Chatter Desktop" : { "Max" : 0, "Remaining" : 0 }, "Chatter Mobile for BlackBerry" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Bulk" : { "Max" : 0, "Remaining" : 0 }, "Dataloader Partner" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.com)" : { "Max" : 0, "Remaining" : 0 }, "Five9 Single Sign-On (https://app.five9.eu)" : { "Max" : 0, "Remaining" : 0 }, "Force.com IDE" : { "Max" : 0, "Remaining" : 0 }, "LiveMessage for Salesforce" : { "Max" : 0, "Remaining" : 0 }, "LiveText for Salesforce (QA)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Chatter" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Files" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Marketing Cloud" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Mobile Dashboards" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Social Customer Service (SCS)" : { "Max" : 0, "Remaining" : 0 }, "Salesforce Touch" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Android" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for Outlook" : { "Max" : 0, "Remaining" : 0 }, "Salesforce for iOS" : { "Max" : 0, "Remaining" : 0 }, "SalesforceA" : { "Max" : 0, "Remaining" : 0 }, "SalesforceIQ" : { "Max" : 0, "Remaining" : 0 }, "Workbench" : { "Max" : 0, "Remaining" : 0 } }, "DailyWorkflowEmails" : { "Max" : 108000, "Remaining" : 108000 }, "DataStorageMB" : { "Max" : 73840, "Remaining" : 15912 }, "DurableStreamingApiConcurrentClients" : { "Max" : 1000, "Remaining" : 987 }, "FileStorageMB" : { "Max" : 242224, "Remaining" : 206602 }, "HourlyAsyncReportRuns" : { "Max" : 1200, "Remaining" : 1200 }, "HourlyDashboardRefreshes" : { "Max" : 200, "Remaining" : 200 }, "HourlyDashboardResults" : { "Max" : 5000, "Remaining" : 5000 }, "HourlyDashboardStatuses" : { "Max" : 999999999, "Remaining" : 999999999 }, "HourlyLongTermIdMapping" : { "Max" : 100000, "Remaining" : 100000 }, "HourlyODataCallout" : { "Max" : 20000, "Remaining" : 20000 }, "HourlyShortTermIdMapping" : { "Max" : 100000, "Remaining" : 100000 }, "HourlySyncReportRuns" : { "Max" : 500, "Remaining" : 500 }, "HourlyTimeBasedWorkflow" : { "Max" : 50, "Remaining" : 50 }, "MassEmail" : { "Max" : 5000, "Remaining" : 5000 }, "MonthlyPlatformEvents" : { "Max" : 750000, "Remaining" : 750000 }, "Package2VersionCreates" : { "Max" : 80, "Remaining" : 80 }, "PermissionSets" : { "Max" : 1500, "Remaining" : 1477, "CreateCustom" : { "Max" : 1000, "Remaining" : 981 } }, "SingleEmail" : { "Max" : 5000, "Remaining" : 4968 }, "StreamingApiConcurrentClients" : { "Max" : 1000, "Remaining" : 1000 } }';
        }
        RESTLimitResource o = RESTLimitResource.parse(response);
        System.debug('RESTLimitResource '+o);
        return o;
    }
    public static RESTLimitResource parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        System.debug('json '+json);
        
        return (RESTLimitResource) System.JSON.deserialize(json, RESTLimitResource.class);
    }
    public class DailyAsyncApexExecutions {
		public Integer Max;	//325000
		public Integer Remaining;	//305655
	}
    public static String fetchUserSessionId(){
        String sessionId = '';
        // Refer to the Page
        PageReference reportPage = Page.SessionIDpage;
        // Get the content of the VF page
        String vfContent='';
        if (!Test.isRunningTest()) {
            vfContent = reportPage.getContent().toString();
        }else {
            vfContent = 'Start_Of_Session_IdabcdEnd_Of_Session_Id'; 
        }
       
        System.debug('vfContent '+vfContent);
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
        endP = vfContent.indexOf('End_Of_Session_Id');
        // Get the Session Id
        sessionId = vfContent.substring(startP, endP);
        System.debug('sessionId '+sessionId);
        // Return Session Id
        return sessionId;
    }
}