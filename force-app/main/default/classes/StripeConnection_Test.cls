@IsTest
public class StripeConnection_Test {
    static{
        Stripe_Parameters__c sp = new Stripe_Parameters__c(Token__c='test');
        insert sp;
    }
    
	static testMethod void testHttpRequest() {
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        StripeConnection.HttpRequest('customers','?limit=10');
        Test.stopTest();
	}
    
    static testMethod void testHttpSend() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Exp_Month__c='5',Exp_Year__c=2018,Credit_Card_Number__c='1234567890',Cvc__c='123');
        insert acc;
        
        Map<String,String> mapCustomer = new Map<String,String>();
		mapCustomer.put('metadata[name]',acc.Name);
        mapCustomer.put('metadata[first_name]','test');
        mapCustomer.put('metadata[last_name]','test');
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        StripeConnection.HttpSend('customers','',acc.Id, mapCustomer);
        Test.stopTest();
	}
    
    static testMethod void testHttpSend2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Card_ID__c='crd_123456789',Exp_Month__c='5',Exp_Year__c=2018,Credit_Card_Number__c='1234567890',Cvc__c='123');
        insert acc;
        
        Map<String,String> mapCustomer = new Map<String,String>();
		mapCustomer.put('metadata[name]',acc.Name);
        mapCustomer.put('metadata[first_name]','test');
        mapCustomer.put('metadata[last_name]','test');
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        StripeConnection.HttpSend('customers','',acc.Id, mapCustomer);
        Test.stopTest();
	}
    
    static testMethod void testpay() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),External_ID__c='1234567',Card_ID__c='crd_1232432');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,Billing__c='charge_automatically');
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='Stripe',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 

        Test.startTest();
        StripeConnection.pay(opp.Id);
        Test.stopTest();
	}
    
    static testMethod void testpay2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),External_ID__c='1234567',Card_ID__c='crd_1232432');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,Billing__c='charge_automatically',Currency__c='USD');
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='Stripe',isActive=true,One_Time_Payment__c=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 

        Test.startTest();
        StripeConnection.pay(opp.Id);
        Test.stopTest();
	}
    
    static testMethod void testpayLightning() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),External_ID__c='1234567',Card_ID__c='crd_1232432');
        insert acc; 
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,Billing__c='charge_automatically');
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='Stripe',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 

        Test.startTest();
        StripeConnection.payLightning(opp.Id);
        Test.stopTest();
	}
    
    static testMethod void testpayLightning2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),External_ID__c='1234567',Card_ID__c='crd_1232432');
        insert acc; 
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,Billing__c='charge_automatically',Currency__c='USD');
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='Stripe',isActive=true,One_Time_Payment__c=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 

        Test.startTest();
        StripeConnection.payLightning(opp.Id);
        Test.stopTest();
	}
}