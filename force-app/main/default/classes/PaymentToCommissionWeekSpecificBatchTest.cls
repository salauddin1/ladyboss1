@isTest
public class PaymentToCommissionWeekSpecificBatchTest {
	@isTest 
    public static void test1(){
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Senior_Agent__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
       
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        ol1.Consider_For_Commission__c = true;
        insert oL1;
        
        Comission_Configuration__c config = new Comission_Configuration__c();
        config.Amount_Paid_to_Agent_Finalizing_Sale__c=1;
        config.Amount_Paid_to_Agent_Initiating_Sale__c=12;
        config.Amount_Paid_to_Director__c=23;
        config.Amount_Paid_to_Senior_Agent__c=23;
        config.Amount_Paid_to_Vice_President__c=12;
        config.Max_Value__c=100;
        config.Min_Value__c=0;
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        config.Agent_paid_on__c = 0;
        config.Director_paid_on__c = 0;
        config.Vp_paid_on__c = 0;
        config.Special_agent_paid_on__c = 0;
        insert config;
        
        System.debug('op.id : '+op1.id);
        Opportunity oppp = [select week__c from Opportunity where id=:op1.id];
        System.debug(oppp.Week__c);
        
        Commission_configuration_specific_week__c cfg = new Commission_configuration_specific_week__c();
        cfg.Max_Value__c = 1000;
        cfg.Min_Value__c = 0;
        cfg.Percentage_paid_to_agent__c = 10;
        cfg.Percentage_paid_to_director__c = 10;
        cfg.Percentage_paid_to_vice_president__c = 10;
        cfg.Percentage_paid_to_senior_agent__c = 10; 
        cfg.name = 'fdvfhvbh';
        cfg.weekRange__c = '1-2';
        insert cfg;
        
        payment__c pay = new payment__c();
        pay.OwnerId = userInfo.getUserId();
        pay.week__c = 1;
        pay.Product__c = prod.id;
        pay.Amount__c = 10;
        insert pay;
        
        Comission__c cm = new Comission__c();
        cm.Amount__c = 1;
        cm.weeks__c=1;
        cm.Payment__c = pay.id;
        cm.active__c = true;
        insert cm;
        
        test.startTest();
        PaymentToCommissionWeekSpecificBatch p = new PaymentToCommissionWeekSpecificBatch(1);
        Database.executeBatch(p);
        test.stopTest();
    }
}