global class GetCustomerBalance implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    String query;
    List<string> cusIdList;
    global GetCustomerBalance(List<string> cusIdList){
        if (cusIdList.size()>0) {
            this.cusIdList = cusIdList;
            this.query = 'SELECT id, Balance__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c!= null and Customer__c !=null AND Balance__c != null AND Customer__c IN:cusIdList';
        }else {
            this.query = 'SELECT id, Balance__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c!= null and Customer__c !=null AND Balance__c = null';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Stripe_Profile__c> scope){
        for (Stripe_Profile__c sp: scope) {
            HttpRequest http = new HttpRequest();
            http.setEndpoint('https://api.stripe.com/v1/customers/'+sp.Stripe_Customer_Id__c);
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' +
                EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            Integer statusCode;
            
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            
            if (!Test.isRunningTest()) {
                try {
                    hs = con.send(http);
                } catch (CalloutException e) { }
            } else {
                hs.setBody('{ "id": "cus_Gkdtp5Q3EsbZUm", "object": "customer", "account_balance": -200, "address": null, "balance": -200, "created": 1581942113, "currency": "usd", "default_source": "card_1GDSu7FzCf73siP0t3p0zYiV", "delinquent": false, "description": "Jay Patel", "discount": null, "email": "jay@ladyboss.com", "invoice_prefix": "39D1FB56", "invoice_settings": { "custom_fields": null, "default_payment_method": null, "footer": null }, "livemode": false, "metadata": { "Shipping Address": "PO Box 249 Albuquerque NM US 87103", "Full Name": "Jay Patel", "Email": "jay@ladyboss.com", "Sales Person": "Jay Patel", "Integration Initiated From": "Salesforce", "Salesforce Contact Link": "https://ladyboss--tirthbox--c.cs22.visual.force.com/00317000010cUNLAA2" }, "name": null, "phone": null, "preferred_locales": [], "shipping": null, "sources": { "object": "list", "data": [ { "id": "card_1GDSu7FzCf73siP0t3p0zYiV", "object": "card", "address_city": "Albuquerque", "address_country": "US", "address_line1": "PO Box 249", "address_line1_check": "pass", "address_line2": null, "address_state": "NM", "address_zip": "87103", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_Gkdtp5Q3EsbZUm", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 11, "exp_year": 2045, "fingerprint": "ewZijzHeax1h8UxX", "funding": "credit", "last4": "0341", "metadata": { "Salesforce Card Link": "https://tirthbox-ladyboss-support.cs22.force.com/a1q170000005n4rAAA" }, "name": null, "tokenization_method": null }, { "id": "card_1GD8bhFzCf73siP0S6PsVSLY", "object": "card", "address_city": "New York", "address_country": "US", "address_line1": "47th st 13th West", "address_line1_check": "pass", "address_line2": null, "address_state": "NY", "address_zip": "10011", "address_zip_check": "pass", "brand": "Visa", "country": "US", "customer": "cus_Gkdtp5Q3EsbZUm", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 11, "exp_year": 2021, "fingerprint": "vsalrcOqPbeQp4Jc", "funding": "credit", "last4": "4242", "metadata": { "Salesforce Card Link": "https://ladyboss--tirthbox--c.cs22.visual.force.com/a1q170000005mJ2AAI" }, "name": "Test", "tokenization_method": null } ], "has_more": false, "total_count": 2, "url": "/v1/customers/cus_Gkdtp5Q3EsbZUm/sources" }, "subscriptions": { "object": "list", "data": [ { "id": "sub_GkdvYXIcxf1RnF", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1581942248, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "collection_method": "charge_automatically", "created": 1581942248, "current_period_end": 1584447848, "current_period_start": 1581942248, "customer": "cus_Gkdtp5Q3EsbZUm", "days_until_due": null, "default_payment_method": "card_1GD8bhFzCf73siP0S6PsVSLY", "default_source": null, "default_tax_rates": [ { "id": "txr_1CqpI8FzCf73siP0Lvf5GdH2", "object": "tax_rate", "active": true, "created": 1532295636, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": false, "metadata": {}, "percentage": 0.0 } ], "discount": null, "ended_at": null, "invoice_customer_balance_settings": { "consume_applied_balance_on_void": true }, "items": { "object": "list", "data": [ { "id": "si_GkdvXjlHBZIYPO", "object": "subscription_item", "billing_thresholds": null, "created": 1581942249, "metadata": {}, "plan": { "id": "burn1", "object": "plan", "active": true, "aggregate_usage": null, "amount": 4926, "amount_decimal": "4926", "billing_scheme": "per_unit", "created": 1577369859, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "burn1", "product": "prod_FA2CuaYJkDpxha", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_GkdvYXIcxf1RnF", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_GkdvYXIcxf1RnF" }, "latest_invoice": "in_1GD8dwFzCf73siP0WFpnxWFK", "livemode": false, "metadata": { "Shipping Address": "47th st 13th West New York NY US 10011", "Full Name": "Jay Patel", "Email": "jay@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss--tirthbox.cs22.my.salesforce.com/00317000010cUNLAA2", "Integration Initiated From": "Salesforce", "Street": "47th st 13th West", "City": "New York", "State": "NY", "Postal Code": "10011", "Country": "US" }, "next_pending_invoice_item_invoice": null, "pending_invoice_item_interval": null, "pending_setup_intent": null, "pending_update": null, "plan": { "id": "burn1", "object": "plan", "active": true, "aggregate_usage": null, "amount": 4926, "amount_decimal": "4926", "billing_scheme": "per_unit", "created": 1577369859, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "burn1", "product": "prod_FA2CuaYJkDpxha", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1581942248, "start_date": 1581942248, "status": "active", "tax_percent": 0.0, "trial_end": null, "trial_start": null } ], "has_more": false, "total_count": 1, "url": "/v1/customers/cus_Gkdtp5Q3EsbZUm/subscriptions" }, "tax_exempt": "none", "tax_ids": { "object": "list", "data": [], "has_more": false, "total_count": 0, "url": "/v1/customers/cus_Gkdtp5Q3EsbZUm/tax_ids" }, "tax_info": null, "tax_info_verification": null }');
                hs.setStatusCode(200);
            }
            system.debug('#### '+ hs.getBody());
            
            response = hs.getBody();
            statusCode = hs.getStatusCode();
            system.debug('$$statusCode = '+hs.getStatusCode());
            if (statusCode == 200) {
                Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(response);
                String balance = String.valueOf(result.get('balance'));
                sp.Balance__c = - Decimal.valueOf(balance)*0.01;
                
            } 
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}