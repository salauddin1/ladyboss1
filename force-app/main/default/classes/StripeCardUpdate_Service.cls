//Rest resourcefor updating card and to update the check box to true if updated on stripe
@RestResource(urlMapping='/Stripe_CardUpdateService')
global class StripeCardUpdate_Service {
    @HttpPost
    global static void updateCard() {
        String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
        Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'Stripe_CardUpdateService');
        System.debug('-------timestamppp----'+timestamppp);
        System.debug('-------sigFromHmac----'+sigFromHmac);
                             
        if(sigFromHmac && sigFromHmac == true || Test.isRunningTest()){
            
        
        Stripe_webservice_setting__c op = [select id, StripeCardUpdate_Service__c,StripeCardDelete_Service__c,StripeCustomer_Service__c from Stripe_webservice_setting__c];
        string CustomerIde;
        //To stop exectuion
        if(RestContext.request.requestBody!=null  && op.StripeCardUpdate_Service__c){
            try {
                //parsing the JSON
                String str = RestContext.request.requestBody.toString();
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                System.debug(str);
                Map<String, Object> data = new map<String, Object>();
                List<String> fieldNames = new List<String>();
                Map<String, Object> account = (Map<String, Object>)results.get('data');
                Map<String,Object> strList =(Map<String,Object>)account.get('object');
                
                Map<String,Object> strMetadata = (Map<String,Object>)strList.get('metadata');
                String customerId = String.Valueof(strList.get('customer'));
               //get all the cards 
               List<Card__c> listOfExistingCard = [select id,Stripe_Card_Id__c,Expiry_Month__c ,Expiry_Year__c  from card__c where Stripe_Card_Id__c  !=null and Stripe_Card_Id__c  = : String.Valueof(strList.get('id'))];
               System.debug('listOfExistingCard ====='+listOfExistingCard );
                if(listOfExistingCard.size() > 0 && (listOfExistingCard[0].Expiry_Month__c!= String.Valueof(strList.get('exp_month')) || listOfExistingCard[0].Expiry_Year__c != String.Valueof(strList.get('exp_year')) )){
                if((Integer.Valueof(strList.get('exp_month'))==11 ) || Integer.Valueof(strList.get('exp_month'))==12){
                        
                        listOfExistingCard[0].Expiry_Month__c = String.Valueof(strList.get('exp_month'));
                    }else
                    {
                        listOfExistingCard[0].Expiry_Month__c = '0'+String.Valueof(strList.get('exp_month'));
                        
                    }
                     listOfExistingCard[0].Expiry_Year__c = String.Valueof(strList.get('exp_year'));
                     listOfExistingCard[0].Billing_Zip_Postal_Code__c = String.Valueof(strList.get('address_zip'));
                     listOfExistingCard[0].Billing_Street__c = String.Valueof(strList.get('address_line1'));
                     listOfExistingCard[0].Billing_Country__c = String.Valueof(strList.get('address_country'));
                     listOfExistingCard[0].Billing_City__c = String.Valueof(strList.get('address_city'));
                     listOfExistingCard[0].Billing_State_Province__c = String.Valueof(strList.get('address_state'));
                     //listOfExistingCard[0].isManuallyUpdated__c =true;
                    update listOfExistingCard;
                }
                //get all the cases for the custmer
                List<Case> caseList = [select id ,UpdateCardStatus__c, Manual_Updated_card_date_time__c, CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c,is_KickOut_From_MC__c from case where Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c=:customerId and is_KickOut_From_MC__c!=true and UpdateCardStatus__c!='Card Updated'];
                Boolean updateByVF = true;
                if(string.Valueof(strMetadata.get('Updated via'))=='Salesforce VF' ){
                updateByVF=false;
                }
                if(caseList.size() > 0 && updateByVF){
                for(case cs : caseList) {
                //To update the checkbox to true to kick out from the journey
                cs.is_KickOut_From_MC__c = true;
                cs.Manual_Updated_card_date_time__c =system.Now();
                }
                //Update the list
                update caseList;
                }
                
               
                 if(test.isRunningTest()){
                  integer a = 10/0;  
                }
                    
                }

               
       
            catch(Exception ex) {
            //this is to catch the exceptins
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                 ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                new ApexDebugLog.Error(
                 'StripeCardUpdate_Service',
                 'createCard',
                 CustomerIde,
                     ex
                 )
                 );
            }    
            
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
    } 
}