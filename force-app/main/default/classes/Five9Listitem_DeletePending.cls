public class Five9Listitem_DeletePending implements Schedulable{
    public static void deletePendingItems(){
        List<Five9LSP__Five9_List_Item__c> deletedFive9ItemList =  [SELECT Id, Name, Five9LSP__Five9_List__c, Product__c, Created_Date__c, Five9LSP__Five9_List__r.Min_Age__c, Five9LSP__Five9_List__r.Max_Age__c, Five9LSP__Five9_List__r.Product__c, Five9LSP__Five9_List__r.Name, Age__c, Five9LSP__Lead__c, Five9LSP__Contact__c FROM Five9LSP__Five9_List_Item__c where Five9LSP__Contact__r.Campaign_Movement_NOt_Allowed__c = false AND Five9LSP__Five9_List__r.List_Auto_Movement_Active__c = true AND Five9LSP__Sync_Status__c='-4' ORDER BY Five9LSP__Five9_List__r.Five9_Processing_Priority__c LIMIT 10000];
        delete deletedFive9ItemList;
    } 
    public void execute(SchedulableContext SC) {
        Five9Listitem_DeletePending.deletePendingItems();
    }
 
}