global class StripeChargeError {

 /*
    // updated with new version of stripe error message
    {
  "error": {
    "charge": "ch_1DKNGIFzCf73siP0ypSsxVGt",
    "code": "card_declined",
    "decline_code": "insufficient_funds",
    "doc_url": "https://stripe.com/docs/error-codes/card-declined",
    "message": "Your card has insufficient funds.",
    "type": "card_error"
  }
}
    */

	global String message;
    global String code;
    global String charge;
    global String decline_code;
    global String type;
    global String doc_url;

	 global static StripeChargeError parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);

        return (StripeChargeError) System.JSON.deserialize(json, StripeChargeError.class);
    }
}