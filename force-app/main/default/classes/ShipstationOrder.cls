public class ShipstationOrder {
    public Static Boolean isrecursive = false;
    
    @AuraEnabled
    public static List<Object> getContact(String contactId){
        List<Contact> con = new List<Contact>();
        List<ShipStation_Orders__c> shipVal = new List<ShipStation_Orders__c>();
        
        shipVal =[select id,Name,name__c,phone__c,state__c,city__c,Order_Number__c,street1__c,Ship_To_City__c,Ship_Street1__c,postalCode__c,orderId__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c,orderStatus__c,customerEmail__c from ShipStation_Orders__c where orderStatus__c =: 'Draft' And Contact__c =: contactId LIMIT 1 ]; 
        System.debug('================='+shipVal);
        con = [select id,Name,MobilePhone,Email,MailingStreet,MailingCity,MailingAddress,MailingState,MailingCountry,MailingPostalCode,Phone, (select id,Shipping_Street__c,Shipping_City__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c,Shipping_Country__c from Addresses__r where Primary__c=true) from Contact where Id =: contactId LIMIT 1 ]; 
        
        if(shipVal.isEmpty()){
            return con;
        }
        else{
            return shipVal;
        }
        
    }
    
    @AuraEnabled
    public static List<ShipStation_Orders__c> getShip(String contactId){
        List<ShipStation_Orders__c> shipVal = new List<ShipStation_Orders__c>();
        
        shipVal =[select id,Name,orderId__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c from ShipStation_Orders__c order by createdDate DESC  LIMIT 1 ]; 
        return shipVal;
    }
    
    @AuraEnabled
    public static void getOrderNew(String OrderIds,String contactId) {
        Shipstation_Orders__c order = new Shipstation_Orders__c();
        Map<String,Object> OrdMap =(Map<String,Object>)JSON.deserializeUntyped(OrderIds);
        order.customerEmail__c = String.valueOf(OrdMap.get('Email'));
        order.name__c = String.valueOf(OrdMap.get('Name'));
        order.street1__c = String.valueOf(OrdMap.get('street1__c'));
        order.city__c = String.valueOf(OrdMap.get('city__c'));
        order.state__c = String.valueOf(OrdMap.get('state__c'));
        order.country__c='US';
        order.postalCode__c = String.valueOf(OrdMap.get('Zip'));
        order.Phone__c = String.valueOf(OrdMap.get('phone__c'));
        order.Contact__c = contactId;
        order.orderStatus__c='Draft';
        order.country__c = 'US';
        insert order; 
        
    }
    @AuraEnabled
    public static String updateOrderShip(String ContactId,String OrderIds,String LineItems) {
        ShipStation_Orders__c shipVal = new ShipStation_Orders__c();
        System.debug('================='+OrderIds);
        shipVal =[select id,Name,orderId__c,Order_Number__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c from ShipStation_Orders__c where orderStatus__c =: 'Draft' And Contact__c =: ContactId  LIMIT 1 ]; 
        System.debug('==shipVal==='+shipVal);
        
        ShipstationWrapper OrderJsonBody = new ShipstationWrapper ();
        shipInformation Shipinf = new shipInformation();
        Map<String,Object> OrdMap =(Map<String,Object>)JSON.deserializeUntyped(OrderIds);
        OrderJsonBody.customerEmail = String.valueOf(OrdMap.get('customerEmail__c'));
        OrderJsonBody.customerUsername=String.valueOf(OrdMap.get('customerEmail__c'));
        String OrdDate =String.valueOf(DateTime.now());
        OrdDate = OrdDate.replace(' ', 'T');
        OrderJsonBody.orderDate=OrdDate;
        OrderJsonBody.paymentDate=OrdDate;
        //OrderJsonBody.orderNumber = shipVal.Name;
        OrderJsonBody.orderNumber = shipVal.Order_Number__c;
        OrderJsonBody.orderStatus='awaiting_shipment';
        OrderJsonBody.shipTo = new shipInformation();
        OrderJsonBody.billTo = new BillingInfo();
        OrderJsonBody.shipTo.name = String.valueOf(OrdMap.get('Name'));
        OrderJsonBody.shipTo.street1 = String.valueOf(OrdMap.get('street1__c'));
        OrderJsonBody.shipTo.city = String.valueOf(OrdMap.get('city__c'));
        OrderJsonBody.shipTo.state = String.valueOf(OrdMap.get('state__c'));
        OrderJsonBody.shipTo.country = 'US';
        OrderJsonBody.shipTo.postalCode = String.valueOf(OrdMap.get('Zip'));
        OrderJsonBody.shipTo.company = String.valueOf(OrdMap.get('company__c'));
        OrderJsonBody.shipTo.phone = String.valueOf(OrdMap.get('phone__c'));
        OrderJsonBody.shipTo.residential=true;
        
        OrderJsonBody.billTo.name = String.valueOf(OrdMap.get('Name'));
        OrderJsonBody.billTo.street1 = String.valueOf(OrdMap.get('street1__c'));
        OrderJsonBody.billTo.city = String.valueOf(OrdMap.get('city__c'));
        OrderJsonBody.billTo.state = String.valueOf(OrdMap.get('state__c'));
        OrderJsonBody.billTo.country = 'US';
        OrderJsonBody.billTo.postalCode = String.valueOf(OrdMap.get('Zip'));
        OrderJsonBody.billTo.company = String.valueOf(OrdMap.get('company__c'));
        OrderJsonBody.billTo.phone = String.valueOf(OrdMap.get('phone__c'));
        OrderJsonBody.billTo.residential=true;
        
        //OrderJsonBody.internationalOptions = new internationalOption();
        //OrderJsonBody.internationalOptions.contents = 'InitiatedFromSalesforce';
        //OrderJsonBody.internationalOptions.customsItems = 'InitiatedFromSalesforce';
        
        List<Object> prodList = (List <Object>)Json.deserializeUntyped(LineItems);
        OrderJsonBody.items= new List<LineItems>();
        Map<String, Object> prodMap = new Map<String, Object>();
        LineItems item;
        for(Object obj: prodList) {
            item = new LineItems();
            prodMap = (Map<String, Object>)obj;
            item.quantity = Integer.valueOf(prodMap.get('Quantity'));
            Map<String, Object> ItemsMap = (Map<String, Object>)prodMap.get('selectedItem');
            item.name=String.valueOf(ItemsMap.get('text'));
            item.sku=String.valueOf(ItemsMap.get('SKU'));
            item.unitPrice=Double.valueOf(ItemsMap.get('price'));
            OrderJsonBody.items.add(item);
        }
        String FinalJson = Json.serialize(OrderJsonBody);
        System.debug('==============FinalJson============'+FinalJson);
        String message;
        String header;
        String jsonBody;
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        try{
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders/createorder');
            req.setMethod('POST');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Content-Type', 'application/json');
            req.setBody(FinalJson);
            HttpResponse res = new Http().send(req);
            String ResponseBody = res.getBody();
            System.debug('===========ResponseBodyUp============='+ResponseBody); 
            if(res.getStatusCode() == 200) {
            Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody);
            List<Object> itemsList = (List<Object>)response.get('items');
            Map<String, Object> shippingAddress = (Map<String,Object>)response.get('shipTo');
            Map<String, Object> billAddress =  (Map<String, Object>)response.get('billTo');
            System.debug('===========shippingAddress============='+shippingAddress); 
            System.debug('===========billAddress============='+billAddress); 
            System.debug('=============OrdMap======='+OrdMap);
            List<Shipstation_Orders__c> orderList = new List<Shipstation_Orders__c>();
            Map<String,Object> IemsMap = new Map<String,Object>();
            if(!response.isEmpty()){
                String orderNumber = String.valueOf(response.get('orderNumber'));
                if(shipVal.Order_Number__c == orderNumber){
                    Shipstation_Orders__c order = new Shipstation_Orders__c();
                    shipVal.orderId__c = String.valueOf(response.get('orderId'));
                    shipVal.orderKey__c=String.valueOf(response.get('orderKey'));
                    shipVal.orderNumber__c=String.valueOf(response.get('orderNumber'));
                    shipVal.Contact__c=String.valueOf(OrdMap.get('Id'));
                    shipVal.city__c = String.valueOf(OrdMap.get('city__c'));
                    shipVal.company__c=String.valueOf(OrdMap.get('company__c'));
                    shipVal.country__c='US';
                    shipVal.CreateDate__c=String.valueOf(response.get('createDate'));
                    shipVal.customerEmail__c=String.valueOf(OrdMap.get('Email'));
                    shipVal.customerUsername__c=String.valueOf(OrdMap.get('customerEmail__c'));
                    shipVal.modifyDate__c=String.valueOf(response.get('modifyDate'));
                    shipVal.name__c=String.valueOf(OrdMap.get('Name'));
                    shipVal.orderDate__c=String.valueOf(response.get('orderDate'));
                    shipVal.phone__c = String.valueOf(OrdMap.get('phone__c'));
                    shipVal.postalCode__c=String.valueOf(OrdMap.get('Zip'));
                    shipVal.street1__c=String.valueOf(OrdMap.get('street1__c'));
                    shipVal.orderStatus__c='awaiting_shipment';
                    if(billAddress.get('addressVerified') != null){
                        shipVal.addressVerified__c = String.valueOf(billAddress.get('addressVerified'));
                    }
                    shipVal.Shipname__c=String.valueOf(shippingAddress.get('name'));
                    shipVal.Ship_Company__c=String.valueOf(shippingAddress.get('company'));
                    shipVal.Ship_Street1__c=String.valueOf(shippingAddress.get('street1'));     
                    shipVal.Ship_Street1__c=String.valueOf(shippingAddress.get('street1'));
                    shipVal.ShipTo_Street2__c=String.valueOf(shippingAddress.get('street2'));
                    shipVal.Ship_To_Street3__c=String.valueOf(shippingAddress.get('street3'));
                    if(shippingAddress.get('addressVerified') != null){
                        shipVal.Ship_To_AddressVerified__c=String.valueOf(shippingAddress.get('addressVerified'));
                    }
                    shipVal.Ship_To_City__c=String.valueOf(shippingAddress.get('city'));
                    shipVal.Ship_To_Country__c=String.valueOf(shippingAddress.get('country'));
                    shipVal.Ship_To_Phone__c=String.valueOf(shippingAddress.get('phone'));
                    shipVal.Ship_To_PostalCode__c=String.valueOf(shippingAddress.get('postalCode'));
                    shipVal.Ship_To_State__c=String.valueOf(shippingAddress.get('state'));
                    orderList.add(shipVal);
                    System.debug('======Name=========='+shipVal.Name);
                    System.debug('======orderNumber__c=========='+shipVal.Name);
                    
                    update orderList;
                }
                List<ShipStation_Order_Items__c> ShipStationItem = new List<ShipStation_Order_Items__c>();
                
                for(Object obj:itemsList){
                    IemsMap = (Map<String,Object>)obj;
                    if(!IemsMap.isEmpty()){
                        ShipStation_Order_Items__c soitems =new ShipStation_Order_Items__c();
                        soitems.orderItemId__c=String.valueOf(IemsMap.get('orderItemId'));
                        soitems.createDate__c=String.valueOf(IemsMap.get('createDate'));
                        soitems.adjustment__c=boolean.valueOf(IemsMap.get('adjustment'));
                        soitems.fulfillmentSku__c=String.valueOf(IemsMap.get('fulfillmentSku'));
                        soitems.imageUrl__c=String.valueOf(IemsMap.get('imageUrl'));
                        soitems.lineItemKey__c=String.valueOf(IemsMap.get('lineItemKey'));
                        soitems.modifyDate__c=String.valueOf(IemsMap.get('modifyDate'));
                        soitems.name__c=String.valueOf(IemsMap.get('name'));
                        soitems.productId__c=String.valueOf(IemsMap.get('productId'));
                        soitems.quantity__c=Integer.valueOf(IemsMap.get('quantity'));
                        soitems.shippingAmount__c=Double.valueOf(IemsMap.get('shippingAmount'));
                        soitems.sku__c=String.valueOf(IemsMap.get('sku'));
                        soitems.taxAmount__c=Double.valueOf(IemsMap.get('taxAmount'));
                        soitems.unitPrice__c=Double.valueOf(IemsMap.get('unitPrice'));
                        soitems.upc__c=String.valueOf(IemsMap.get('upc'));
                        soitems.warehouseLocation__c=String.valueOf(IemsMap.get('warehouseLocation'));
                        soitems.weight__c=String.valueOf(IemsMap.get('weight'));
                        if(!orderList.isEmpty()){
                            for(ShipStation_Orders__c Ids:orderList){
                                soitems.ShipStation_Orders__c=Ids.Id;
                            }
                        }
                        ShipStationItem.add(soitems); 
                    }
                }
                
                insert ShipStationItem;
            }
            }
            else {
                Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody);
                Map<String,Object> errormessage = (Map<String,Object>)response.get('ModelState');
                List<Object> error = (List<Object>)errormessage.get('apiOrder.items[0].unitPrice');
                System.debug('=========='+String.valueOf(error[0]));
                if(String.valueOf(error[0]).contains('Error converting value {null} to type \'System.Decimal\'. Path \'items[0].unitPrice\', line 1, position.')) {
                    message = 'Please Check Price Of Your Selected Products, It Should Not Be Null Or Undefined!';
                }
                else {
                    message = String.valueOf(error[0]);
                }
            }
        }
        catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShipstationOrder',
                                 'CreateshipStationOrder',
                                    orderIds,
                                     e
                                 ));
        }
        return  message;  
    }
    
    @AuraEnabled
    public static String getOrderShip1(String ContactId,String OrderIds,String LineItems) {
        String message;
        System.debug('===========LineItems============'+LineItems);
        ShipStation_Orders__c shipVal = new ShipStation_Orders__c();
        System.debug('========getOrderShip1========='+OrderIds);
        shipVal =[select id,Name,orderId__c,Order_Number__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c from ShipStation_Orders__c where orderStatus__c =: 'Draft' And Contact__c =: ContactId   LIMIT 1 ]; 
        System.debug('=========shipVal========'+shipVal);
        ShipstationWrapper OrderJsonBody = new ShipstationWrapper ();
        shipInformation Shipinf = new shipInformation();
        Map<String,Object> OrdMap =(Map<String,Object>)JSON.deserializeUntyped(OrderIds);
        OrderJsonBody.customerEmail = String.valueOf(OrdMap.get('customerEmail__c'));
        OrderJsonBody.customerUsername=String.valueOf(OrdMap.get('customerEmail__c'));
        String OrdDate =String.valueOf(DateTime.now());
        OrdDate = OrdDate.replace(' ', 'T');
        OrderJsonBody.orderDate=OrdDate;
        OrderJsonBody.paymentDate=OrdDate;
        OrderJsonBody.orderNumber = shipVal.Order_Number__c;
        OrderJsonBody.orderStatus='awaiting_shipment';
        OrderJsonBody.shipTo = new shipInformation();
        OrderJsonBody.billTo = new BillingInfo();
        OrderJsonBody.shipTo.name = String.valueOf(OrdMap.get('Name'));
        OrderJsonBody.shipTo.street1 = String.valueOf(OrdMap.get('street1__c'));
        OrderJsonBody.shipTo.city = String.valueOf(OrdMap.get('city__c'));
        OrderJsonBody.shipTo.state = String.valueOf(OrdMap.get('state__c'));
        OrderJsonBody.shipTo.country = 'US';
        OrderJsonBody.shipTo.postalCode = String.valueOf(OrdMap.get('Zip'));
        OrderJsonBody.shipTo.company = String.valueOf(OrdMap.get('company__c'));
        OrderJsonBody.shipTo.phone = String.valueOf(OrdMap.get('phone__c'));
        OrderJsonBody.shipTo.residential=true;
        
        OrderJsonBody.billTo.name = String.valueOf(OrdMap.get('Name'));
        OrderJsonBody.billTo.street1 = String.valueOf(OrdMap.get('street1__c'));
        OrderJsonBody.billTo.city = String.valueOf(OrdMap.get('city__c'));
        OrderJsonBody.billTo.state = String.valueOf(OrdMap.get('state__c'));
        OrderJsonBody.billTo.country = 'US';
        OrderJsonBody.billTo.postalCode = String.valueOf(OrdMap.get('Zip'));
        OrderJsonBody.billTo.company = String.valueOf(OrdMap.get('company__c'));
        OrderJsonBody.billTo.phone = String.valueOf(OrdMap.get('phone__c'));
        OrderJsonBody.billTo.residential=true;
        
        
        List<Object> prodList = (List <Object>)Json.deserializeUntyped(LineItems);
        OrderJsonBody.items= new List<LineItems>();
        Map<String, Object> prodMap = new Map<String, Object>();
        LineItems item;
        for(Object obj: prodList) {
            item = new LineItems();
            prodMap = (Map<String, Object>)obj;
            item.quantity = Integer.valueOf(prodMap.get('Quantity'));
            Map<String, Object> ItemsMap = (Map<String, Object>)prodMap.get('selectedItem');
            item.name=String.valueOf(ItemsMap.get('text'));
            item.sku=String.valueOf(ItemsMap.get('SKU'));
            item.unitPrice=Double.valueOf(ItemsMap.get('price'));
            OrderJsonBody.items.add(item);
        }
        String FinalJson = Json.serialize(OrderJsonBody);
        System.debug('==============FinalJson============'+FinalJson);
        
        String header;
        String jsonBody;
        
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders/createorder');
            req.setMethod('POST');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Content-Type', 'application/json');
            req.setBody(FinalJson);
            HttpResponse res = new Http().send(req);
            String ResponseBody = res.getBody();
            System.debug('=============getordership1 response==========='+ResponseBody);
            if(res.getStatusCode() == 200) {
            Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody);
            Map<String, Object> shippingAddress = (Map<String,Object>)response.get('shipTo');
            Map<String, Object> billAddress =  (Map<String, Object>)response.get('billTo');
            System.debug('=============shippingAddress response==========='+shippingAddress); 
            List<Object> itemsList = (List<Object>)response.get('items');
            List<Shipstation_Orders__c> orderList = new List<Shipstation_Orders__c>();
            Map<String,Object> IemsMap = new Map<String,Object>();
            if(!response.isEmpty()){
                String orderNumber = String.valueOf(response.get('orderNumber'));
                if(shipVal.Order_Number__c == orderNumber){
                    System.debug('=============orderNumber======='+orderNumber);
                    System.debug('=============OrdMap======='+OrdMap);
                    Shipstation_Orders__c order = new Shipstation_Orders__c();
                    shipVal.orderId__c = String.valueOf(response.get('orderId'));
                    shipVal.orderKey__c=String.valueOf(response.get('orderKey'));
                    shipVal.orderNumber__c=String.valueOf(response.get('orderNumber'));
                    shipVal.Contact__c=String.valueOf(OrdMap.get('Contact__c'));
                    shipVal.city__c = String.valueOf(OrdMap.get('city__c'));
                    shipVal.company__c=String.valueOf(OrdMap.get('company__c'));
                    shipVal.country__c='US';
                    shipVal.CreateDate__c=String.valueOf(response.get('createDate'));
                    shipVal.customerEmail__c=String.valueOf(OrdMap.get('Email'));
                    shipVal.customerUsername__c=String.valueOf(OrdMap.get('customerEmail__c'));
                    shipVal.modifyDate__c=String.valueOf(response.get('modifyDate'));
                    shipVal.name__c=String.valueOf(OrdMap.get('Name'));
                    shipVal.orderDate__c=String.valueOf(response.get('orderDate'));
                    shipVal.phone__c = String.valueOf(OrdMap.get('phone__c'));
                    shipVal.postalCode__c=String.valueOf(OrdMap.get('Zip'));
                    shipVal.street1__c=String.valueOf(OrdMap.get('street1__c'));
                    shipVal.orderStatus__c='awaiting_shipment';
                    if(billAddress.get('addressVerified') != null){
                        shipVal.addressVerified__c = String.valueOf(billAddress.get('addressVerified'));
                    }
                    shipVal.Shipname__c=String.valueOf(shippingAddress.get('name'));
                    shipVal.Ship_Company__c=String.valueOf(shippingAddress.get('company'));
                    shipVal.Ship_Street1__c=String.valueOf(shippingAddress.get('street1'));     
                    shipVal.Ship_Street1__c=String.valueOf(shippingAddress.get('street1'));
                    shipVal.ShipTo_Street2__c=String.valueOf(shippingAddress.get('street2'));
                    shipVal.Ship_To_Street3__c=String.valueOf(shippingAddress.get('street3'));
                    if(shippingAddress.get('addressVerified') != null ){
                        shipVal.Ship_To_AddressVerified__c=String.valueOf(shippingAddress.get('addressVerified'));
                    }
                    shipVal.Ship_To_City__c=String.valueOf(shippingAddress.get('city'));
                    shipVal.Ship_To_Country__c=String.valueOf(shippingAddress.get('country'));
                    shipVal.Ship_To_Phone__c=String.valueOf(shippingAddress.get('phone'));
                    shipVal.Ship_To_PostalCode__c=String.valueOf(shippingAddress.get('postalCode'));
                    shipVal.Ship_To_State__c=String.valueOf(shippingAddress.get('state'));
                    orderList.add(shipVal);
                    System.debug('======Name=========='+shipVal.Name);
                    System.debug('======orderNumber__c=========='+shipVal.Name);
                    
                    update orderList;
                }
                List<ShipStation_Order_Items__c> ShipStationItem = new List<ShipStation_Order_Items__c>();
                
                for(Object obj:itemsList){
                    IemsMap = (Map<String,Object>)obj;
                    if(!IemsMap.isEmpty()){
                        ShipStation_Order_Items__c soitems =new ShipStation_Order_Items__c();
                        soitems.orderItemId__c=String.valueOf(IemsMap.get('orderItemId'));
                        soitems.createDate__c=String.valueOf(IemsMap.get('createDate'));
                        soitems.adjustment__c=boolean.valueOf(IemsMap.get('adjustment'));
                        soitems.fulfillmentSku__c=String.valueOf(IemsMap.get('fulfillmentSku'));
                        soitems.imageUrl__c=String.valueOf(IemsMap.get('imageUrl'));
                        soitems.lineItemKey__c=String.valueOf(IemsMap.get('lineItemKey'));
                        soitems.modifyDate__c=String.valueOf(IemsMap.get('modifyDate'));
                        soitems.name__c=String.valueOf(IemsMap.get('name'));
                        soitems.productId__c=String.valueOf(IemsMap.get('productId'));
                        soitems.quantity__c=Integer.valueOf(IemsMap.get('quantity'));
                        soitems.shippingAmount__c=Double.valueOf(IemsMap.get('shippingAmount'));
                        soitems.sku__c=String.valueOf(IemsMap.get('sku'));
                        soitems.taxAmount__c=Double.valueOf(IemsMap.get('taxAmount'));
                        soitems.unitPrice__c=Double.valueOf(IemsMap.get('unitPrice'));
                        soitems.upc__c=String.valueOf(IemsMap.get('upc'));
                        soitems.warehouseLocation__c=String.valueOf(IemsMap.get('warehouseLocation'));
                        soitems.weight__c=String.valueOf(IemsMap.get('weight'));
                        if(!orderList.isEmpty()){
                            for(ShipStation_Orders__c Ids:orderList){
                                soitems.ShipStation_Orders__c=Ids.Id;
                            }
                        }
                        ShipStationItem.add(soitems); 
                        System.debug('===========ShipStationItem========='+ShipStationItem);
                    }
                }
                
                insert ShipStationItem;
            }
                
            }
            else {
                Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody);
                Map<String,Object> errormessage = (Map<String,Object>)response.get('ModelState');
                List<Object> error = (List<Object>)errormessage.get('apiOrder.items[0].unitPrice');
                System.debug('=========='+String.valueOf(error[0]));
                if(String.valueOf(error[0]).contains('Error converting value {null} to type \'System.Decimal\'. Path \'items[0].unitPrice\', line 1, position')) {
                    message = 'Please Check Price Of Your Selected Products, It Should Not Be Null Or Undefined!';
                }
                else {
                    message = String.valueOf(error[0]);
                }
            }
        }
        catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShipstationOrder',
                                 'CreateshipStationOrder',
                                    orderIds,
                                     e
                                 ));
        }
        return  message;  
    }
    @future(callout=true)
    public static void updateOrders(String orderId) {
        String header;
        List <ShipStation_Orders__c> orderList = [Select CreateDate__c,Store_Id__c,Order_notes__c,customerEmail__c,customerId__c,customerUsername__c,modifyDate__c,orderDate__c,orderId__c,orderKey__c,orderNumber__c,orderStatus__c,orderTotal__c,paymentDate__c,paymentMethod__c,shipByDate__c,city__c,company__c,country__c,street1__c,street2__c,street3__c,name__c,phone__c,postalCode__c,Ship_Company__c,Shipname__c,Ship_To_City__c,Ship_To_Country__c,Ship_To_Phone__c,Ship_To_PostalCode__c,Ship_To_State__c,Ship_Street1__c,ShipTo_Street2__c,Ship_To_Street3__c,state__c from ShipStation_Orders__c where id =: orderId];
        System.debug('==========orderList========'+orderList);
        List<ShipStation_Order_Items__c> orderLineItem = [select Id,orderItemId__c,name__c,quantity__c,sku__c,unitPrice__c,ShipStation_Orders__c from ShipStation_Order_Items__c where ShipStation_Orders__c =:orderId];
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        updateorder ordUpdate = new updateorder();
        ordUpdate.shipTo = new shipInformation();
        ordUpdate.billTo = new BillingInfo();
        if(!orderList.isEmpty()){
            for(ShipStation_Orders__c so : orderList) {
                ordUpdate.orderNumber=so.orderNumber__c;
                ordUpdate.orderKey=so.orderKey__c;
                String OrdDate =String.valueOf(DateTime.now());
                OrdDate = OrdDate.replace(' ', 'T');
                ordUpdate.orderDate=OrdDate;
                ordUpdate.paymentDate=OrdDate;
                //ordUpdate.orderStatus=so.orderStatus__c;
                ordUpdate.orderStatus=so.orderStatus__c;
                ordUpdate.customerId=so.customerId__c;
                ordUpdate.customerUsername=so.customerUsername__c;
                ordUpdate.internalNotes = so.Order_notes__c;
                ordUpdate.customerEmail=so.customerEmail__c;
                ordUpdate.shipTo.name = so.Shipname__c;
                ordUpdate.shipTo.street1 = so.Ship_Street1__c;
                ordUpdate.shipTo.city = so.Ship_To_City__c;
                ordUpdate.shipTo.state = so.Ship_To_State__c;
                ordUpdate.shipTo.country = 'US';
                ordUpdate.shipTo.postalCode = so.Ship_To_PostalCode__c;
                ordUpdate.shipTo.company = so.Ship_Company__c;
                ordUpdate.shipTo.phone = so.Ship_To_Phone__c;
                ordUpdate.billTo.name = so.name__c;
                ordUpdate.billTo.street1 = so.street1__c;
                ordUpdate.billTo.city = so.city__c;
                ordUpdate.billTo.state = so.state__c;
                ordUpdate.billTo.country = 'US';
                ordUpdate.billTo.postalCode = so.postalCode__c;
                ordUpdate.billTo.company = so.company__c;
                ordUpdate.billTo.phone = so.phone__c;
                
                ordUpdate.items = new List<LineItems>();
                LineItems orditm = new LineItems();
                for(ShipStation_Order_Items__c itm: orderLineItem) {
                    orditm.quantity = Integer.valueOf(itm.quantity__c);
                    orditm.name = itm.name__c;
                    orditm.sku = itm.sku__c;
                    orditm.unitPrice = itm.unitPrice__c;
                    ordUpdate.items.add(orditm);
                }
            }
        }
        String orderBody=JSON.serialize(new List<Updateorder>{ordUpdate});
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders/createorders');
            req.setMethod('POST');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Content-Type', 'application/json');
            req.setBody(orderBody);
            HttpResponse res = new Http().send(req);
            String ResponseBody = res.getBody();
        }
        catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShipstationOrderUpdate',
                                 'UpdateshipStationOrder',
                                    orderId,
                                     e
                                 ));
        }
    }
    public class ShipstationWrapper {
        public String orderNumber;
        public String orderDate;
        public String paymentDate;
        public String orderStatus;
        public String customerUsername;
        public String customerEmail;
        public String shipByDate;
        public shipInformation shipTo;
        public BillingInfo billTo;
        public List<LineItems> items;
    }
    public class shipInformation {
        public String name;
        public String company;
        public String street1;
        public String street2;
        public String street3;
        public String city;
        public String state;
        public String country;
        public String postalCode;
        public Boolean residential;
        public String phone;
    }
    public class BillingInfo {
        public String name;
        public String company;
        public String street1;
        public String street2;
        public String street3;
        public String city;
        public String state;
        public String country;
        public String postalCode;
        public Boolean residential;
        public String phone;
    }
    public class LineItems {
        public String sku;
        public String name;
        public Integer quantity;
        public Decimal unitPrice;   
    }
    public class Updateorder{
        public String orderNumber;
        public String orderKey;
        public String orderDate;
        public String paymentDate;
        public String orderStatus;
        public String customerId;
        public String customerUsername;
        public String customerEmail;
        public String internalNotes;
        public BillingInfo billTo;
        public shipInformation shipTo;
        public List<LineItems> items;
    }
}