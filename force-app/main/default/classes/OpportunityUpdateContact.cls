public class OpportunityUpdateContact {
    public static boolean oppflag = false;
    public static void updateOppContact(List<Opportunity> Triggernew){
        System.debug('-----class execute---');
        if(System.isBatch() != true && System.isFuture() != true){
            System.debug('----class in iff-----');
            Set<String> emailSet  = new Set<String>();
            Map<String,Id> conEmailIdMap = new Map<String,Id>();
            List<Contact> conList = new List<Contact>();
            
            for(Opportunity opp : Triggernew) {
                if(opp.Contact_Email_Upload__c!= null && opp.Contact_Email_Upload__c !=  ''){
                    emailSet.add(opp.Contact_Email_Upload__c);
                    System.debug('------emailSet----'+emailSet);
                }
            }
            if(emailSet != null && !emailSet.isEmpty()){
                conList = [Select id,Name,Email  from Contact where Email =: emailSet limit 50000];
                System.debug('---conList---'+conList);
            }
            for(Contact con : conList){
                if(con.Email != null && con.Email != ''){
                    conEmailIdMap.put(con.Email,con.Id);
                }
            }
            System.debug('----conEmailIdMap---'+conEmailIdMap);
            for(Opportunity oppNew : Triggernew){
                System.debug('---oppNew.Contact_Email_Upload__c---'+oppNew.Contact_Email_Upload__c);
                if(conEmailIdMap != null && !conEmailIdMap.isEmpty() && conEmailIdMap.containsKey(oppNew.Contact_Email_Upload__c)){
                    System.debug('----conEmailIdMap.get----'+conEmailIdMap.get(oppNew.Contact_Email_Upload__c));
                    if(conEmailIdMap.get(oppNew.Contact_Email_Upload__c) != null ){
                        System.debug('-------update-----');
                        oppNew.Contact__c = conEmailIdMap.get(oppNew.Contact_Email_Upload__c);
                        oppflag = true;
                        System.debug('-----oppNew-----'+oppNew);
                    }
                }
            }
            
        }
    }
}