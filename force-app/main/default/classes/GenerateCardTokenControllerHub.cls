public without sharing class GenerateCardTokenControllerHub {
    
    public String stipeApiKey { get; set; }
    public String calledUsing { get; set;}
    public Id contactId { get; set; }
    public String cardJson { get; set; }
    public String token { get; set; }
    public String messageError { get; set; }
    public String addressJson { get; set; }
    public boolean ValueReturned {get; set;}
    public boolean fakeCall {get; set;}
    public boolean isSafe {get; set;}
    public boolean isError {get; set;}
    Card__c newCard;
    
    public GenerateCardTokenControllerHub(){
        try{
            this.isError = false;
            isSafe = false;
            messageError ='';
            Map<String,String> addrMap = new Map<String,String>();
            this.stipeApiKey = StripeAPI.PublishableKey;
            this.contactId = Id.valueOf(ApexPages.currentPage().getParameters().get('cntId'));
            List<Address__c> addressList = new List<Address__c>();
            addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:contactId and Primary__c=true limit 1];
           // String shippingStreet= addressList[0].Shipping_Street__c!=null ? addressList[0].Shipping_Street__c.replace('\'','&quot;').replace('\""','&quot;') : '';

            if(addressList.size()>0){
                addrMap.put('city',addressList[0].Shipping_City__c);
                addrMap.put('country',addressList[0].Shipping_Country__c);
                addrMap.put('state',addressList[0].Shipping_State_Province__c);
                addrMap.put('zip',addressList[0].Shipping_Zip_Postal_Code__c);
                addrMap.put('street',addressList[0].Shipping_Street__c);
            }else{
                addrMap.put('city',null);
                addrMap.put('country',null);
                addrMap.put('state',null);
                addrMap.put('zip',null);
                addrMap.put('street',null);
                
            
            }
            this.addressJson = JSON.serialize(addrMap);
            
            system.debug('addressJson----'+addressJson);
        }catch(Exception ex){
            
        }
        
        
        
    }
    
    public PageReference InsertRecord() {
        try{
            messageError = '';
            this.isError = false;
            System.debug('cardJson-----'+cardJson);
            System.debug('token-----'+token);
            token  = (String)JSON.deserialize(token, String.class);
            System.debug('token 2-----'+token);
            StripeCard stripeCard = StripeCard.parse(cardJson);
            System.debug('stripeCard-----'+stripeCard);
           
            
            Card__c newCard = new Card__c();
            
            List<Stripe_Profile__c> existingStripeProfiles = new List<Stripe_Profile__c>();
            existingStripeProfiles =[select id,Customer__c,Customer__r.firstname from Stripe_Profile__c where Customer__c=:contactId order by createdDate limit 1];
            if(!existingStripeProfiles.isEmpty()){
                newCard.Stripe_Profile__c = existingStripeProfiles[0].id;        
            }else{
                Stripe_Profile__c newStriprProfile = new Stripe_Profile__c();
                newStriprProfile.Customer__c = contactId;
                insert newStriprProfile;
                newCard.Stripe_Profile__c = newStriprProfile.id;
            }
            
            newCard.Token__c = token;
            newCard.Contact__c = contactId;
            newCard.Initiated_From__c ='Salesforce';
            newCard.Last4__c = stripeCard.last4;
            newCard.Name_On_Card__c = stripeCard.name;
            newCard.Billing_City__c = stripeCard.address_city;
            newCard.Billing_Country__c = stripeCard.address_country;
            newCard.Billing_State_Province__c = stripeCard.address_state;
            newCard.Billing_Street__c = stripeCard.address_line1;
            newCard.Billing_Zip_Postal_Code__c = stripeCard.address_zip;
            newCard.Expiry_Year__c = String.valueOf(stripeCard.exp_year);
            newCard.Expiry_Month__c = String.valueOf(stripeCard.exp_month);
            newCard.Credit_Card_Number__c = '************'+stripeCard.last4;
            //newCard.Card_Type__c = stripeCard.brand;
            insert newCard;
            this.newCard = newCard;
            isSafe = true;
            system.debug('insert----'+newCard);
            
        }catch(exception ex){
            isSafe = false;
            //delete newCard;
            messageError = 'Salesforce Error - '+ex;
            this.isError = true;
        }
        return null;
    }
    
       
   
    
    public PageReference CallWebService() {
        String message;
        //this.isError = false;
        System.debug('------before callout-----');
        try{
            if(isSafe != null && isSafe){
                System.debug('newCard-----'+newCard.id);
                message = StripeIntegrationHandlerChanges.cardWithTokenStripeRealTimeHandler(newCard.Id);
                if(message != null && message !=''){
                    delete newCard;
                    messageError = message;
                    this.isError = true;
                }else{
                    this.isError = false;
                }
            }
        }catch(exception ex){
            delete newCard;
            messageError = 'Error--'+ex;
            this.isError = true;
        }
        return null;
    }
}