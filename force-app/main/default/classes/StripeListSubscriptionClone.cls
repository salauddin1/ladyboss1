public class StripeListSubscriptionClone {
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public class Charges {
        public String object_Z {get;set;} // in json: object
        public String url {get;set;} 
        public Boolean has_more {get;set;} 
        public List<Data> data {get;set;} 
       

        public Charges(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'object') {
                            object_Z = parser.getText();
                        } else if (text == 'url') {
                            url = parser.getText();
                        } else if (text == 'has_more') {
                            has_more = parser.getBooleanValue();
                        } else if (text == 'data') {
                            data = new List<Data>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                data.add(new Data(parser));
                            }
                        
                        } 
                        
                         else {
                            System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    

    
    public class Data {
        public String id {get;set;} 
        public String object_Z {get;set;} // in json: object
        public Integer amount {get;set;} 
        public Integer amount_refunded {get;set;} 
        //public Object application {get;set;} 
        //public Object application_fee {get;set;} 
        public String balance_transaction {get;set;} 
        public Boolean captured {get;set;} 
        public Integer created {get;set;} 
        public String currency_data {get;set;} 
        public String customer {get;set;} 
        public String description {get;set;} 
        public String quantity {get ; set ;}
        public String invoice {get;set;} 
        public Boolean livemode {get;set;} 
          public Plan plan {get;set;} 
        public Boolean paid {get;set;} 
        Public Item items {get; set;}
        public String statement_descriptor {get;set;} 
        public String status {get;set;}
       
    
        public Data(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'object') {
                            object_Z = parser.getText();
                        } else if (text == 'amount') {
                            amount = parser.getIntegerValue();
                        } else if (text == 'amount_refunded') {
                            amount_refunded = parser.getIntegerValue();
                        } else if (text == 'application') {
                            //application = new Object(parser);
                        } else if (text == 'application_fee') {
                            //application_fee = new Object(parser);
                        } else if (text == 'balance_transaction') {
                            balance_transaction = parser.getText();
                        } else if (text == 'captured') {
                            captured = parser.getBooleanValue();
                        } else if (text == 'created') {
                            created = parser.getIntegerValue();
                        } else if (text == 'currency_data') {
                            currency_data = parser.getText();
                        } else if (text == 'customer') {
                            customer = parser.getText();
                        } else if (text == 'description') {
                            description = parser.getText();
                        } else if (text == 'destination') {
                            //destination = new Object(parser);
                        } else if (text == 'dispute') {
                            //dispute = new Object(parser);
                        } else if (text == 'failure_code') {
                            //failure_code = new Object(parser);
                        } else if (text == 'failure_message') {
                            //failure_message = new Object(parser);
                        }  else if (text == 'invoice') {
                            invoice = parser.getText();
                        } else if (text == 'livemode') {
                            livemode = parser.getBooleanValue();
                        }  else if (text == 'on_behalf_of') {
                            //on_behalf_of = new Object(parser);
                        } else if (text == 'order') {
                            //order = new Object(parser);
                        } else if (text == 'outcome') {
                            //outcome = new Object(parser);
                        } else if (text == 'paid') {
                            paid = parser.getBooleanValue();
                        } else if (text == 'receipt_email') {
                            //receipt_email = new Object(parser);
                        } else if (text == 'receipt_number') {
                            //receipt_number = new Object(parser);
                        }  else if (text == 'review') {
                            //review = new Object(parser);
                        } else if (text == 'shipping') {
                            //shipping = new Object(parser);
                        }  else if (text == 'source_transfer') {
                            //source_transfer = new Object(parser);
                        } else if (text == 'statement_descriptor') {
                            statement_descriptor = parser.getText();
                        } else if (text == 'status') {
                            status = parser.getText();
                        } 
                        else if (text == 'plan') {
                            plan= new Plan(parser);
                        }
                        else if (text == 'items') {
                            items= new Item(parser);
                        }
                        else if (text == 'quantity') {
                            quantity  = parser.getText();
                        }
                        else if (text == 'transfer_group') {
                            //transfer_group = new Object(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
     public class Item{
        public List<Data_x > data {get;set;} 
       
        public Item(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                       if (text == 'data') {
                            data = new List<Data_x >();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                data.add(new Data_x (parser));
                            }
                        
                        } 
                   }
                }
            }
        }
    }
     public class Data_x {
        public String id {get;set;} 
        public String object_Z {get;set;} // in json: object
        public Integer created {get;set;} 
        
        public Plan plan {get;set;} 
        public Integer quantity {get;set;} 
        public String subscription {get;set;} 

        public Data_x (JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'object') {
                            object_Z = parser.getText();
                        } else if (text == 'created') {
                            created = parser.getIntegerValue();
                        } else if (text == 'plan') {
                            plan = new Plan(parser);
                        } else if (text == 'quantity') {
                            quantity = parser.getIntegerValue();
                        } else if (text == 'subscription') {
                            subscription = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class Plan{
        public String id {get;set;} 
        public String amount{get;set;}
         public String nickname {get;set;} 
        public Plan(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'amount') {
                            amount= parser.getText();
                        } 
                        else if (text == 'nickname') {
                            nickname = parser.getText();
                        } 
                   }
                }
            }
        }
    }

    public static Charges parse(String json) {
        return new Charges(System.JSON.createParser(json));
    }
    

}