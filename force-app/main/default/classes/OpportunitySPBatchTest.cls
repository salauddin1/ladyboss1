@isTest
public class OpportunitySPBatchTest {
    @isTest
    public static void test1(){
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        op1.Stripe_Charge_Id__c = 'ch_fjjfj';
        insert op1;
        Test.startTest();
        OpportunitySalesPersonUpdateBatch b = new OpportunitySalesPersonUpdateBatch();
        Database.executeBatch(b);
        Test.stopTest();
    }
}