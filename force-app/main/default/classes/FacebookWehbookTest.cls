@isTest
private class FacebookWehbookTest{
	@isTest
	static void itShould(){
		Test.setMock(HttpCalloutMock.class, new FacebookAPIMock(200,'Ok','{"first_name":"Sid","last_name":"Shah","id":"egegegeeg"}',new  Map<String, String>()));		
		FacebookPage__c fb = new FacebookPage__c();
		fb.isActive__c = true;
		fb.Last_Fetch_Time__c = Datetime.now();
		fb.Page_Id__c = 'testPageId';
		fb.Page_Token__c = 'token';
		fb.Page_Name__c = 'test name';
        fb.Last_Fetch_Time__c = DateTime.now().addDays(-1);
		insert fb;

		Case caseObject = new Case();
		caseObject.subject = 'Inbox Message from: ';
		caseObject.origin = 'facebook';
		caseObject.status = 'new';
		caseObject.Conversation_Id__c = 'ets';
		insert caseObject;

		Contact cnt = new Contact();
		cnt.Fb_User_Id__c = 'testPageId';
		cnt.lastName = 'test';
		insert cnt;

		FacebookWrappers.ImageData img = new FacebookWrappers.ImageData();
		img.url = 'testurl';
		img.preview_url = 'testurl';

		FacebookWrappers.AttachmentData att = new FacebookWrappers.AttachmentData();		
		att.image_data = img;
		att.id='ds';
		att.name='dss';

		FacebookWrappers.Category category = new FacebookWrappers.Category();		
		category.id = 'dsds';
		category.name = 'sds';

		FacebookWrappers.AccessToken accessToken = new FacebookWrappers.AccessToken();
		accessToken.token_type = 'asd';
		accessToken.expires_in = 'asd';

		FacebookWrappers fw = new FacebookWrappers ();

		FacebookWrappers.Cursor cur = new FacebookWrappers.Cursor();
		cur.after = 'dd';
		cur.before = 'ggdd';

		FacebookWrappers.Paging paging = new FacebookWrappers.Paging();
		paging.cursors = cur;
		paging.next = 'next';
		paging.next = 'previous';
 		
		List<FacebookWrappers.AttachmentData> attList = new List<FacebookWrappers.AttachmentData>();

		FacebookWrappers.MultipleAttachments multi = new FacebookWrappers.MultipleAttachments();		
		multi.data = attList;

		Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/FacebookWehbook/test';
		req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
		req.requestBody = Blob.valueOf('{"object": "page","entry": [{"id": "testPageId","time": 1458692752478,"messaging": [{"sender": {"id": "egegegeeg"},"recipient": {"id": "testPageId"},"timestamp": 1458692752478,"message": {"mid": "mid.1457764197618:41d102a3e1ae206a38","text": "hello, world!","quick_reply": {"payload": "<DEVELOPER_DEFINED_PAYLOAD>"}}}]}]}');
        RestContext.request = req;
        RestContext.response = res;
		FacebookWehbook.create();
		Test.stopTest();

	}
}