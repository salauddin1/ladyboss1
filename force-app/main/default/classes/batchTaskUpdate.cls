global class batchTaskUpdate implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //query on Task
        String query = 'SELECT id,WhoId,phone__c,email__c,ActivityDate,Status FROM Task where email__c =null and WhoId !=null';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Task> scope)
    {
         
         Map<Id, List<Task>> whoIdsMap = new Map<Id, List<Task>>();
    
    for(Task t : scope){
       
        if(t.WhoId != null){
            
            if(!whoIdsMap.containsKey(t.WhoId)){
                
                List<Task> temp = new List<Task>();
                
                temp.add(t);
               
                whoIdsMap.put(t.WhoId, temp);
            }else{
                
                whoIdsMap.get(t.WhoId).add(t);
            }
        }
    }
   
    
    for(Contact con : [Select Id, Name,email,phone from Contact where Id in :whoIdsMap.keySet()]){
        List<task> taskList =  new List<task> ();
        for(Task t :whoIdsMap.get(con.Id)){
             t.email__c = con.email;
              t.phone__c = con.phone;
              taskList.add(t);
        }
        update taskList;
         
         
    } 
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
}