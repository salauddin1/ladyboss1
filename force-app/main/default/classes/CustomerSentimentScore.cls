//To show the customer survey on contact layout
public class CustomerSentimentScore {
 List<Case> caseList ;
  public String conId {get;set;}
  public String errorMsg {get;set;}
  public Integer surveyPercent {get;set;}
  public Boolean div1{get;set;}
  public Boolean div2{get;set;}
   public Boolean div3{get;set;}
    public Boolean div4{get;set;}
     public Boolean div5{get;set;}
  
  
       public CustomerSentimentScore(ApexPages.StandardController controller){
       div1=false;
       div2=false;
        div3=false;
       div4=false;
       div5=false;
       
        //get the case ID
         conId= apexpages.currentpage().getparameters().get('id'); 
         //List of cases with the survey
         List<Case> caseList = [select id,One_Click_Survey__c  from case where contactId =: conId and One_Click_Survey__c!=null];
         Integer count=0;
         //counting the survey percenetage
         for(Case cs : caseList ) {
             if(cs.One_Click_Survey__c =='Excellent'){
             count = count+100;
             }else if(cs.One_Click_Survey__c =='Great'){
              count = count+75;
             }
             else if(cs.One_Click_Survey__c =='Ok'){
              count = count+25;
             }
             else if(cs.One_Click_Survey__c =='Poor'){
              count = count+0;
             }
         }
         if(caseList.size()>0){
         div2 =false;
         if(count==0){
         surveyPercent  =0;
         div1 =true;
         }else{
         surveyPercent  = count/caseList.size();
         if(surveyPercent>=0 && surveyPercent<49){
         div1 =true;
         }else if(surveyPercent>=50 && surveyPercent<79) {
         div3 =true;       
          }else if(surveyPercent>=80 && surveyPercent<89) {
         div4 =true;
         }else if(surveyPercent>=90 && surveyPercent<=100) {
         div5 =true;
         }
          
         }
         
         
        
         }else{
          div1 =false;
          div3=false;
       div4=false;
       div5=false;
       
           div2 =true;
         errorMsg = 'No Survey for this contact';
         }
         
         
         
         
    
    }

}