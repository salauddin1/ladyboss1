@isTest
public class ShipstationOrderStatusShippedBatch_test {
 @isTest
    public Static void unittest1(){
        ShipStation_Orders__c ordr = new ShipStation_Orders__c();
        ordr.orderStatus__c= 'awaiting_shipment';
        ordr.orderId__c='20291736';
        ordr.orderKey__c='manual-768d148d7dd54452bde6109bc9b3a85a';
        insert ordr;
        ordr.orderStatus__c= 'Shipped';
        update ordr;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
         ShipstationUpdate_pages__c pages = new ShipstationUpdate_pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        insert pages;
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new SHipStationRoutingWebhookClassMock());
       ShipstationOrderStatusShippedBatch srs=new ShipstationOrderStatusShippedBatch();
        Database.executeBatch(srs);
        System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new ShipstationShippesSchedular());
        Test.stopTest();
    }
}