global class ContactFromCaseBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        List<String> five9mailList = new List<String>{'noreply@five9.com','user-noreply@five9.com','voicemail-noreply@five9.com','voicemail1@ladyboss.com','voicemail2@ladyboss.com','voicemail3@ladyboss.com','voicemail4@ladyboss.com'};
            if(Test.isRunningTest()){
                return Database.getQueryLocator([SELECT id,SuppliedEmail,SuppliedName,SuppliedPhone  FROM Case WHERE SuppliedEmail!=null]);
            }else{
                return Database.getQueryLocator([SELECT id,SuppliedEmail,SuppliedName,SuppliedPhone  FROM Case WHERE SuppliedEmail!=null and ContactId=null and SuppliedEmail not in : five9mailList]);
            }
    }
    global void execute(Database.BatchableContext bc, List<Case> caseList){
        List<String> emailList = new List<String>();
        for(Case c : caseList){
            emailList.add(c.SuppliedEmail);
        }
        List<Contact> existContList = [Select id,Email from Contact WHERE Email in : emailList];
        Map<String,Contact> emailToContMap = new Map<String,Contact>();
        for(Contact cts : existContList){
            emailToContMap.put(cts.email.toLowerCase(),cts);
        } 
        List<Contact> conList = new List<Contact>();
        for(Case cs : caseList){
            if(emailToContMap.get(cs.SuppliedEmail.toLowerCase())==null){
                Contact ct = new Contact();
                if(cs.SuppliedName != null ){
                    Integer ind = cs.SuppliedName.indexOf(' ');
                    if(ind!=-1){
                        ct.FirstName = cs.SuppliedName.substring(0,ind);
                        ct.LastName = cs.SuppliedName.substring(ind+1,cs.SuppliedName.length());
                    }else{ 
                        ct.LastName = cs.SuppliedName;
                    }
                }else{
                    ct.LastName = 'Not Available';
                }
                ct.email = cs.SuppliedEmail;
                if(cs.SuppliedPhone!=null){
                    ct.Phone = cs.SuppliedPhone;
                }
                conList.add(ct);
            }
            cs.contactCreated__c = true;
        }
        insert conList;
        existContList = [Select id,Email from Contact WHERE Email in : emailList];
        for(Contact cts : existContList){
            emailToContMap.put(cts.email.toLowerCase(),cts);
        }
        for(Case cas : caseList){
            if(emailToContMap.get(cas.SuppliedEmail.toLowerCase())!=null){
            	cas.ContactId = emailToContMap.get(cas.SuppliedEmail.toLowerCase()).Id;
            }
        }
        update caseList;
        
        System.debug(caseList);
        System.debug(conList);
    }
    global void finish(Database.BatchableContext bc){ 
    }
}