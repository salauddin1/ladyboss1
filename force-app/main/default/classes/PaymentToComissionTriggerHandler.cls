/*
* Developer Name : Tirth Patel
* Description : This is a helper class to PaymentToCommissionTrigger
*/
public class PaymentToComissionTriggerHandler {  
    
    public static void comission(List<Payment__c> paymntList){
        List<Comission__c> comList = new List<Comission__c>();
        Set<id> agentId_Set = new  Set<id>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Map<id,id> oppAndInitAgent = new Map<id,id>();
        Set<id> oppIdSet = new Set<id>();
        Map<id,id> oppAndCreditUser = new Map<id,id>();
        Map<id,Decimal> payOwnerAndTotal = new Map<id,Decimal>();
        Set<id> inactiveUsers = new Set<id>(); 
        for(Payment__c paymnt:paymntList){
            oppIdSet.add(paymnt.Opportunity__c);
            agentId_Set.add(paymnt.Payment_Created_By__c);
        }
        for(Opportunity op : [SELECT id,Sales_Person_Id__c,AgentInitializingSale__c,User_to_give_credit__c FROM Opportunity WHERE id in : oppIdSet]){
            oppAndInitAgent.put(op.id,op.AgentInitializingSale__c);
            oppAndCreditUser.put(op.id,op.User_to_give_credit__c);
        }
        Map<id,Comission_Configuration__c> config_with_product_Map = new Map<id,Comission_Configuration__c>();
        for(Comission_Configuration__c comconfig : [SELECT id,Product__c,Amount_Paid_to_Agent_Finalizing_Sale__c,Amount_Paid_to_Agent_Initiating_Sale__c ,Amount_Paid_to_Director__c,Amount_Paid_to_Vice_President__c,Amount_Paid_to_Senior_Agent__c,Agent_paid_on__c,Director_paid_on__c,Vp_paid_on__c,Special_agent_paid_on__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Senior_Agent__c,Percentage_Paid_to_Vice_President__c,For_First_Sell__c FROM Comission_Configuration__c WHERE Product__c!=null]){
            config_with_product_Map.put(comconfig.Product__c,comconfig);
        }
        //Map for all NS Commission configurations
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = PayToCommTriggerHelper.getTotalCommissionConfiguration('Max_Value__c','asc');
        //Map for all FS Commission configurations
        Map<Decimal,Comission_Configuration__c> maxValAndComMap_FS = PayToCommTriggerHelper.getCommConfigsFS();
        List<Id> userIdList = new List<Id>();
        System.debug('agentId_Set : '+agentId_Set);
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c in : agentId_Set] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Senior_Agent__c);
            userIdList.add(userhierarki.Vice_President__c); 
        }
        List<User> userHieList = [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE Id in : userIdList];
        Map<id,User> userSpecialComMap = new Map<id,User>();
        for(User ur : userHieList){
            userSpecialComMap.put(ur.id, ur);
            if(!ur.IsActive){
                inactiveUsers.add(ur.id);
            }
        }
        for(Payment__c paymnt:paymntList){
            if(!config_with_product_Map.keyset().contains(paymnt.Product__c)){
                if(payOwnerAndTotal.get(paymnt.Payment_Created_By__c)==null){
                    payOwnerAndTotal.put(paymnt.Payment_Created_By__c,paymnt.Amount__c);
                }else{
                    payOwnerAndTotal.put(paymnt.Payment_Created_By__c,payOwnerAndTotal.get(paymnt.Payment_Created_By__c) + paymnt.Amount__c);
                } 
            }
        }
        if(userHieMap.size()==0){
            System.debug('No User Hierarchies');
            return ;
        }
        Map<id,String> userValueMap;
        if(agentId_Set.size()>0){
            userValueMap = PayToCommTriggerHelper.getTotalCommission(agentId_Set,config_with_product_Map.keyset());
        }
        // To get all previously created commissins for the agent
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap = PayToCommTriggerHelper.getOldPayments(agentId_Set, config_with_product_Map.keyset());
        Map<id,id> payAndcreditUser = PayToCommTriggerHelper.getOldPaymentCreditUser(agentId_Set, config_with_product_Map.keyset());
        
        //To get all previously created FS commissions for the agent
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap_FS = PayToCommTriggerHelper.getOldPaymentsFS(agentId_Set, config_with_product_Map.keyset());
        Map<id,id> payAndcreditUser_FS = PayToCommTriggerHelper.getOldPaymentCreditUserFS(agentId_Set, config_with_product_Map.keyset());
        for(Payment__c paymnt:paymntList){
            UserHierarchy__c userhie = userHieMap.get(paymnt.Payment_Created_By__c);
            if(userhie==null){
                System.debug('UserHierarchy does not exist for this user');
                continue;
            }
            Comission_Configuration__c com;
            if(config_with_product_Map.get(paymnt.Product__c)!=null){
                // commission for coaching product where com config has product on it
                com = config_with_product_Map.get(paymnt.Product__c);
                PayToCommTriggerHelper.fixedCommission(oppAndInitAgent.get(paymnt.Opportunity__c), paymnt, com, userhie, comList);
            }else{
                
                Decimal twoWeekTotal = Decimal.valueOf(userValueMap.get(paymnt.Payment_Created_By__c)); 
                com = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
                PayToCommTriggerHelper.tierBasedCommission(oppAndCreditUser.get(paymnt.Opportunity__c), userhie, paymnt, com, comList,userSpecialComMap,inactiveUsers);
                //check for tier changing
                if(agentId_Set.contains(paymnt.Payment_Created_By__c)){
                    Decimal twoWeekTotalBefore = twoWeekTotal - payOwnerAndTotal.get(paymnt.Payment_Created_By__c);
                    Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                    // updates all previous NS commissions to the changed tier
                    if(comBefore.id != com.id){
                        System.debug('tier changed');
                        if(payUserAndComMap.size()>0){
                            PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMap.get(paymnt.Payment_Created_By__c), com, userhie, comList, payAndcreditUser,userSpecialComMap,inactiveUsers);
                        } 
                    }
                    //update all previous FS commissions to the changed tier
                    Comission_Configuration__c com_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap_FS);
                    Comission_Configuration__c comBefore_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap_FS);
                    if(comBefore_FS.id != com_FS.id){
                        if(payUserAndComMap_FS.size()>0){
                            PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMap_FS.get(paymnt.Payment_Created_By__c), com_FS, userhie, comList, payAndcreditUser_FS,userSpecialComMap,inactiveUsers);
                        }
                    }
                    agentId_Set.remove(paymnt.Payment_Created_By__c);
                }
            }
        }
        upsert comList;
        
        System.debug('Commissions : '+comList.size()+' '+comList);
    }
    
    public static void refund(List<Payment__c> paymntList){
        List<Comission__c> comList = new List<Comission__c>();
        Set<id> inactiveUsers = new Set<id>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Payment__c paymnt = paymntList.get(0);
        Comission_Configuration__c emptyConfig = new Comission_Configuration__c();
        Comission_Configuration__c configForFirstSell = new Comission_Configuration__c();
        List<Opportunity> oppList =  [SELECT id,week__c,createdDate,User_to_give_credit__c,Sales_Person_Id__c,AgentInitializingSale__c  FROM opportunity where id =: paymnt.Opportunity__c];
        Map<id,Comission_Configuration__c> comConfigProdMap = new Map<id,Comission_Configuration__c>();
        for(Comission_Configuration__c comconfig : [SELECT id,Product__c,Amount_Paid_to_Agent_Finalizing_Sale__c,Amount_Paid_to_Agent_Initiating_Sale__c ,Amount_Paid_to_Director__c,Amount_Paid_to_Vice_President__c,Amount_Paid_to_Senior_Agent__c,Agent_paid_on__c,Director_paid_on__c,Vp_paid_on__c,Special_agent_paid_on__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Senior_Agent__c,Percentage_Paid_to_Vice_President__c,For_First_Sell__c FROM Comission_Configuration__c WHERE Product__c!=null]){
            comConfigProdMap.put(comconfig.Product__c,comconfig);
        }
        List<Id> userIdList = new List<Id>();
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c =: paymnt.Payment_Created_By__c] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Senior_Agent__c);
            userIdList.add(userhierarki.Vice_President__c);
        }
        List<User> userHieList = [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE Id in : userIdList];
        Map<id,User> userSpecialComMap = new Map<id,User>();
        for(User ur : userHieList){
            userSpecialComMap.put(ur.id, ur);
            if(!ur.IsActive){
                inactiveUsers.add(ur.id);
            }
        } 
        if(userHieMap.size()==0||inactiveUsers.contains(paymnt.Payment_Created_By__r.id)){
            return ;
        } 
        Map<id,id> payAndcreditUser = PayToCommTriggerHelper.getOldPaymentCreditUser(new Set<Id>{paymnt.Payment_Created_By__c}, comConfigProdMap.keyset());
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap = PayToCommTriggerHelper.getOldPayments(new Set<Id>{paymnt.Payment_Created_By__c}, comConfigProdMap.keyset());
        
        Map<id,id> payAndcreditUserFS = PayToCommTriggerHelper.getOldPaymentCreditUserFS(new Set<Id>{paymnt.Payment_Created_By__c}, comConfigProdMap.keyset());
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMapFS = PayToCommTriggerHelper.getOldPaymentsFS(new Set<Id>{paymnt.Payment_Created_By__c}, comConfigProdMap.keyset());
        
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = PayToCommTriggerHelper.getTotalCommissionConfiguration('Max_Value__c','asc');
        Map<Decimal,Comission_Configuration__c> maxValAndComMapFirstSell = PayToCommTriggerHelper.getCommConfigsFS();
        UserHierarchy__c userhie = userHieMap.get(paymnt.Payment_Created_By__c);
        if(userhie==null){
            System.debug('UserHierarchy does not exist for this user');
            return;
        }
        Comission_Configuration__c com;
        Decimal week = paymnt.week__c; 
        if(paymnt.is_First_Sell__c == true && !comConfigProdMap.keyset().contains(paymnt.product__c)){
            Map<id,String> userTotalMap = PayToCommTriggerHelper.getTotalRefFS(oppList,comConfigProdMap.keyset());
            Decimal twoWeekTotal = Decimal.valueOf(userTotalMap.get(paymnt.Payment_Created_By__c)); 
            com = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMapFirstSell);
            //first sell refund
            if(PayToCommTriggerHelper.isInTwoWeek(week)){
                //current 
                PayToCommTriggerHelper.tierBasedCommission(oppList.get(0).User_to_give_credit__c, userhie, paymnt, com, comList,userSpecialComMap,inactiveUsers);
                Decimal twoWeekTotalBefore = twoWeekTotal - paymnt.Amount__c;
                Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMapFirstSell);
                //check for first sell tier change
                if(comBefore.id != com.id){
                    PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMapFS.get(paymnt.Payment_Created_By__c), com, userhie, comList, payAndcreditUserFS,userSpecialComMap,inactiveUsers);
                }
                Comission_Configuration__c com_NS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
                Comission_Configuration__c comBefore_NS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                //check for Normal sell tier change
                if(comBefore_NS.id != com_NS.id){
                    PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMap.get(paymnt.Payment_Created_By__c), com_NS, userhie, comList, payAndcreditUser,userSpecialComMap,inactiveUsers);
                }
            }else{
                System.debug('Not in first sell two week');
                System.debug('Amount : '+paymnt.Amount__c);
                if(paymnt.week__c > 46){
                    Decimal twoWeekTotalBefore = twoWeekTotal - paymnt.Amount__c;
                    Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMapFirstSell);
                    if(com.id == comBefore.id){
                        PayToCommTriggerHelper.previoustwoWeekSameTier(oppList.get(0).User_to_give_credit__c,userhie, paymnt, com, comList,userSpecialComMap,inactiveUsers);
                    }else{ 
                        PayToCommTriggerHelper.previoustwoWeekTierChangeFS(oppList.get(0).User_to_give_credit__c, userhie, comBefore, paymnt, com, comList,comConfigProdMap.keyset(),userSpecialComMap);
                    }
                    
                    Comission_Configuration__c com_NS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
                    Comission_Configuration__c comBefore_NS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                    //logic to compensate NS refund
                    if(com_NS.Id != comBefore_NS.Id){
                        PayToCommTriggerHelper.previousWeekFSRefundChangesNSTier(userhie, comBefore_NS, paymnt, com_NS, comList, comConfigProdMap.keyset(), userSpecialComMap);
                    }
                }else{
                    //logic for first sell refund (non one bucket) 
                    Map<id,String> userTotalMapNB = PayToCommTriggerHelper.getTotalRefFSNB(oppList,comConfigProdMap.keyset());
                    Decimal twoWeekTotalNB = Decimal.valueOf(userTotalMapNB.get(paymnt.Payment_Created_By__c)); 
                    Decimal twoWeekTotalNB_Before = twoWeekTotalNB - paymnt.Amount__c;
                    Comission_Configuration__c com_NB = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalNB,maxValAndComMapFirstSell);
                    Comission_Configuration__c com_NB_Before = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalNB_Before,maxValAndComMapFirstSell);
                    if(com_NB.id == com_NB_Before.id){
                        PayToCommTriggerHelper.previoustwoWeekSameTier(oppList.get(0).User_to_give_credit__c,userhie, paymnt, com_NB, comList,userSpecialComMap,inactiveUsers);
                    }else{ 
                        PayToCommTriggerHelper.previoustwoWeekTierChangeFS(oppList.get(0).User_to_give_credit__c, userhie, com_NB_Before, paymnt, com_NB, comList,comConfigProdMap.keyset(),userSpecialComMap);
                    }
                }
            }
        }
        else if(comConfigProdMap.get(paymnt.Product__c)!=null){
            com = comConfigProdMap.get(paymnt.Product__c);
            PayToCommTriggerHelper.fixedCommissionRefund(oppList.get(0).AgentInitializingSale__c, paymnt, com, userhie, comList);
        }else{  
            Map<id,String> userValueMap = PayToCommTriggerHelper.getTotalCommissionRef(oppList,comConfigProdMap.keyset());
            Decimal twoWeekTotal = Decimal.valueOf(userValueMap.get(paymnt.Payment_Created_By__c)); 
            com = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
            if(PayToCommTriggerHelper.isInTwoWeek(week)){ 
                PayToCommTriggerHelper.tierBasedCommission(oppList.get(0).User_to_give_credit__c, userhie, paymnt, com, comList,userSpecialComMap,inactiveUsers);
                Decimal twoWeekTotalBefore = twoWeekTotal - paymnt.Amount__c;
                Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                if(comBefore.id != com.id){
                    if(payUserAndComMap.size()>0){
                        PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMap.get(paymnt.Payment_Created_By__c), com, userhie, comList, payAndcreditUser,userSpecialComMap,inactiveUsers);
                    }
                } 
                Comission_Configuration__c com_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMapFirstSell);
                Comission_Configuration__c comBefore_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMapFirstSell);
                if(comBefore_FS.id != com_FS.id){
                    PayToCommTriggerHelper.updatePrevCommissions(payUserAndComMapFS.get(paymnt.Payment_Created_By__c), com_FS, userhie, comList, payAndcreditUserFS,userSpecialComMap,inactiveUsers);
                }
            }else{
                System.debug('Not in two week');
                System.debug('Amount : '+paymnt.Amount__c);
                Decimal twoWeekTotalBefore = twoWeekTotal - paymnt.Amount__c;
                if(!PayToCommTriggerHelper.isWeekSpecific(week,comConfigProdMap.keyset())){
                    if(paymnt.week__c > 46){
                        Comission_Configuration__c comBefore = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                        if(com.id == comBefore.id){
                            PayToCommTriggerHelper.previoustwoWeekSameTier(oppList.get(0).User_to_give_credit__c,userhie, paymnt, com, comList,userSpecialComMap,inactiveUsers);
                        }else{ 
                            PayToCommTriggerHelper.previoustwoWeekTierChange(oppList.get(0).User_to_give_credit__c,userhie, comBefore, paymnt, com, comList,comConfigProdMap.keyset(),userSpecialComMap);
                        }
                        Comission_Configuration__c com_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMapFirstSell);
                        Comission_Configuration__c comBefore_FS = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMapFirstSell);
                        //logic to compensate FS tier change
                        if(com_FS.Id != comBefore_FS.Id){
                            PayToCommTriggerHelper.previousWeekNSRefundChangesFSTier(userhie, comBefore_FS, paymnt, com_FS, comList, comConfigProdMap.keyset(), userSpecialComMap);
                        }
                    }else{
                        //logic for Normal sell refund (non one bucket) 
                        Map<id,String> userTotalMapNB = PayToCommTriggerHelper.getTotalCommissionRefNB(oppList,comConfigProdMap.keyset());
                        Decimal twoWeekTotalNB = Decimal.valueOf(userTotalMapNB.get(paymnt.Payment_Created_By__c)); 
                        Decimal twoWeekTotalNB_Before = twoWeekTotalNB - paymnt.Amount__c;
                        Comission_Configuration__c com_NB = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalNB,maxValAndComMap);
                        Comission_Configuration__c com_NB_Before = PayToCommTriggerHelper.getCommissionConfiguration(twoWeekTotalNB_Before,maxValAndComMap);
                        if(com_NB.id == com_NB_Before.id){
                            PayToCommTriggerHelper.previoustwoWeekSameTier(oppList.get(0).User_to_give_credit__c,userhie, paymnt, com_NB, comList,userSpecialComMap,inactiveUsers);
                        }else{ 
                            PayToCommTriggerHelper.previoustwoWeekTierChange(oppList.get(0).User_to_give_credit__c, userhie, com_NB_Before, paymnt, com_NB, comList,comConfigProdMap.keyset(),userSpecialComMap);
                        }
                    }
                }else{ 
                    System.debug('specific week');
                    Commission_configuration_specific_week__c cm = PayToCommTriggerHelper.getCOmCgfWeekSpecific(twoWeekTotal, week);
                    Commission_configuration_specific_week__c cmBefore = PayToCommTriggerHelper.getCOmCgfWeekSpecific(twoWeekTotalBefore, week);
                    if(cm.id == cmBefore.id){
                        PayToCommTriggerHelper.previoustwoWeekSpecificSameTier(oppList.get(0).User_to_give_credit__c,userhie, cm, paymnt, comList);
                    }else{ 
                        PayToCommTriggerHelper.previoustwoWeekSpecificTierChange(oppList.get(0).User_to_give_credit__c,twoWeekTotalBefore, userhie, twoWeekTotal, cmBefore, cm, paymnt, comList,comConfigProdMap.keyset());
                    }
                }
            }
        }
        upsert comList;
        System.debug('Commissions : '+comList.size()+' '+comList);
    } 
}