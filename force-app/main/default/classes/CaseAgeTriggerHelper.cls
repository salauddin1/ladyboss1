public class CaseAgeTriggerHelper {
    public static void getCaseAgeInsert(List<Case> newTriggerVal){
        List<Case_Age__c> csAgeAddList = new List<Case_Age__c>();
        for(Case csNew : newTriggerVal){
            if(csNew.Status != null && csNew.Status == 'Open'){
                Case_Age__c csAge = new Case_Age__c();
                csAge.Current_Status__c = csNew.Status ;
                csAge.Last_Status__c  = 'Open' ;
                csAge.Case__c = csNew.id ;
                csAge.Age__c = 0;
                csAge.Total_Age__c = 0 ;
                csAgeAddList.add(csAge);
                
            }
        }
        try{
            insert csAgeAddList ;
        }
        catch (Exception ex) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('getCaseAgeInsert','Case trigger','test',ex));
            System.debug('***Get Exception***'+ex);
        }        
    }
    public static void getCaseAgeUpdate(List<Case> newTriggerVal, Map<id, Case> oldMapTriggerVal ){
        try{
            Set<Id> csIdSet = new Set<Id>(); 
            Map<Id,Decimal> csAgeUpdateMap = new Map<Id, Decimal>();
            Map<Id,String> csReopUpdateMap = new Map<Id, String>();
            
            for(Case csNew : newTriggerVal){
                csIdSet.add(csNew.id);
            }
            List<Case_Age__c> csAgeList = new List<Case_Age__c>();
            if(csIdSet != null && !csIdSet.isEmpty()){
                csAgeList = [select id, CreatedDate, Current_Status__c, Last_Status__c, Age__c, Case__c from  Case_Age__c where Case__c =: csIdSet and Case__c != null order by createdDate asc ] ;
            }
            Map<Id, List<Case_Age__c>> csAgeMap = new Map<Id, List<Case_Age__c>>();
            Map<Id, Boolean> csAgeCheckMap = new Map<Id, Boolean>();
            if(csAgeList != null && !csAgeList.isEmpty()){
                for(Case_Age__c csAg : csAgeList){
                    if(csAg != null && csAg.case__c != null ){
                        if(csAgeMap.keySet().contains(csAg.case__c)){
                            csAgeMap.get(csAg.case__c).add(csAg);
                        }
                        else{
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList.add(csAg);
                            csAgeMap.put(csAg.Case__c, csAgeNewList);
                        }
                    }
                }
            }
            if(csAgeMap != null && csAgeMap.keySet() != null && !csAgeMap.keySet().isEmpty()){
                for(Case csNew : newTriggerVal){
                    if(csAgeMap.keySet().contains(csNew.id)){
                        csAgeCheckMap.put(csNew.id, True);
                    }
                    else{
                        csAgeCheckMap.put(csNew.id, false);
                    }
                }  
            }
            List<Case_Age__c> csAgeAddList = new List<Case_Age__c>();
            
            for(Case csNew : newTriggerVal){
                Case csOld = new Case();
                csOld = oldMapTriggerVal.get(csNew.id);
                System.debug('  test new '+csNew);
                System.debug('  test Old '+csOld);
                if(csNew.Status != null && csOld.Status != null && csNew.Status !=  csOld.Status ){
                    if(csNew.Status == 'Open'){
                        
                        Boolean checkOpen = false ;
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c != null && csAg.Current_Status__c == 'open'){
                                        checkOpen = true ;
                                        break;
                                    }
                                }
                            }
                        }
                        if(checkOpen != true ){
                            Case_Age__c csAge = new Case_Age__c();
                            csAge.Current_Status__c = csNew.Status ;
                            csAge.Last_Status__c  = csOld.Status ;
                            csAge.Case__c = csNew.id ;
                            csAge.Age__c = 0;
                            csAge.Total_Age__c = 0 ;
                            csAgeAddList.add(csAge);
                            
                        }
                    }
                    else if( csNew.Status == 'Closed'  ){
                        Boolean checkOpen = false ;
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c != null && csAg.Current_Status__c == 'Closed'){
                                        DateTime closedDateTime = System.now().addSeconds(-4);
                                        if(csAg.CreatedDate > closedDateTime ){
                                            checkOpen = true ;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if(checkOpen != true ){
                            Case_Age__c csAge = new Case_Age__c();
                            csAge.Current_Status__c = csNew.Status ;
                            csAge.Last_Status__c  = csOld.Status ;
                            csAge.Case__c = csNew.id ;
                            Decimal  lastAge = 0 ;
                            Decimal  WorkAge = 0 ;
                            if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                                List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                                csAgeNewList = csAgeMap.get(csNew.id);
                                
                                if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                    Decimal  getTime = 0 ;
                                    DateTime checkDaTime = Datetime.newInstance(2008, 12, 1, 12, 30, 2) ;
                                    for(Case_Age__c csAg : csAgeNewList){
                                        if(csAg.Current_Status__c == 'open'){
                                            if(checkDaTime < csAg.createdDate || checkDaTime == csAg.createdDate  ){
                                                
                                                checkDaTime = csAg.createdDate    ;
                                                getTime =  csAg.createdDate.getTime();
                                                System.debug('  test csAg '+csAg );
                                            }
                                        }
                                        
                                    }
                                    if(getTime != null && getTime != 0){
                                        WorkAge = System.now().getTime () -  getTime ;
                                        System.debug('  test lastAge '+lastAge );
                                    }
                                }
                            }
                            if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                                List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                                csAgeNewList = csAgeMap.get(csNew.id);
                                
                                if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                    Decimal  getTime = 0;
                                    for(Case_Age__c csAg : csAgeNewList){
                                        if(csAg.Current_Status__c == 'open'){
                                            getTime =  csAg.createdDate.getTime();
                                            System.debug('  test csAg '+csAg );
                                        }
                                        
                                    }
                                    if(getTime != null && getTime != 0){
                                        lastAge = System.now().getTime () -  getTime ;
                                        System.debug('  test lastAge '+lastAge );
                                    }
                                }
                            }
                            csAge.Age__c = WorkAge/1000/60 ;
                            csAge.Total_Age__c = lastAge/1000/60 ;
                            if(csAge.Total_Age__c != null && csAge.Total_Age__c != 0){
                                csAgeUpdateMap.put(csNew.id, csAge.Total_Age__c);
                            }
                            csAgeAddList.add(csAge);
                        }
                    }
                    else if( csNew.Status == 'Reopened'  ){
                        Case_Age__c csAge = new Case_Age__c();
                        csAge.Current_Status__c = csNew.Status ;
                        csAge.Last_Status__c  = csOld.Status ;
                        csAge.Case__c = csNew.id ;
                        Decimal  lastAge = 0 ;
                        Decimal  WorkAge = 0 ;
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0 ;
                                DateTime checkDaTime = Datetime.newInstance(2008, 12, 1, 12, 30, 2) ;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'Closed'){
                                        if(checkDaTime < csAg.createdDate || checkDaTime == csAg.createdDate  ){
                                            
                                            checkDaTime = csAg.createdDate    ;
                                            getTime =  csAg.createdDate.getTime();
                                            System.debug('  test csAg '+csAg );
                                        }
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    WorkAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'open'){
                                        getTime =  csAg.createdDate.getTime();
                                        System.debug('  test csAg '+csAg );
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    lastAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        csAge.Age__c = WorkAge/1000/60 ;
                        csAge.Total_Age__c = lastAge/1000/60 ;
                        csAge.Status__c = 'Close to Reopened';
                        if(csAge.Total_Age__c != null && csAge.Total_Age__c != 0){
                            csAgeUpdateMap.put(csNew.id, csAge.Total_Age__c);
                        }
                        csAgeAddList.add(csAge);
                    }
                    else if( csNew.Status == 'Closed-From-Reopened'  ){
                        Case_Age__c csAge = new Case_Age__c();
                        csAge.Current_Status__c = csNew.Status ;
                        csAge.Last_Status__c  = csOld.Status ;
                        csAge.Case__c = csNew.id ;
                        Decimal  lastAge = 0 ;
                        Decimal  WorkAge = 0 ;
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0 ;
                                DateTime checkDaTime = Datetime.newInstance(2008, 12, 1, 12, 30, 2) ;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'Reopened'){
                                        if(checkDaTime < csAg.createdDate || checkDaTime == csAg.createdDate  ){
                                            
                                            checkDaTime = csAg.createdDate    ;
                                            getTime =  csAg.createdDate.getTime();
                                            System.debug('  test csAg '+csAg );
                                        }
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    WorkAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'open'){
                                        getTime =  csAg.createdDate.getTime();
                                        System.debug('  test csAg '+csAg );
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    lastAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        csAge.Age__c = WorkAge/1000/60 ;
                        csAge.Status__c = 'Reopened to Closed-From-Reopened';
                        csAge.Total_Age__c = lastAge/1000/60 ;
                        if(csAge.Total_Age__c != null && csAge.Total_Age__c != 0){
                            csAgeUpdateMap.put(csNew.id, csAge.Total_Age__c);
                        }
                        csAgeAddList.add(csAge);
                    }
                    else if( csNew.Status == 'Responded'  ){
                        Case_Age__c csAge = new Case_Age__c();
                        csAge.Current_Status__c = csNew.Status ;
                        csAge.Last_Status__c  = csOld.Status ;
                        csAge.Case__c = csNew.id ;
                        Decimal  lastAge = 0 ;
                        Decimal  WorkAge = 0 ;
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0 ;
                                DateTime checkDaTime = Datetime.newInstance(2008, 12, 1, 12, 30, 2) ;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'Reopened'){
                                        if(checkDaTime < csAg.createdDate || checkDaTime == csAg.createdDate  ){
                                            
                                            checkDaTime = csAg.createdDate    ;
                                            getTime =  csAg.createdDate.getTime();
                                            System.debug('  test csAg '+csAg );
                                        }
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    WorkAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        if(csAgeCheckMap!= null && csAgeCheckMap.keySet() != null && !csAgeCheckMap.keySet().isEmpty() && csAgeCheckMap.get(csNew.id) ){
                            List<Case_Age__c> csAgeNewList = new List<Case_Age__c>();  
                            csAgeNewList = csAgeMap.get(csNew.id);
                            
                            if(csAgeNewList != null && !csAgeNewList.isEmpty()){
                                Decimal  getTime = 0;
                                for(Case_Age__c csAg : csAgeNewList){
                                    if(csAg.Current_Status__c == 'open'){
                                        getTime =  csAg.createdDate.getTime();
                                        System.debug('  test csAg '+csAg );
                                    }
                                    
                                }
                                if(getTime != null && getTime != 0){
                                    lastAge = System.now().getTime () -  getTime ;
                                    System.debug('  test lastAge '+lastAge );
                                }
                            }
                        }
                        csAge.Age__c = WorkAge/1000/60 ;
                        csAge.Status__c = 'Reopened to Responded';
                        csAge.Total_Age__c = lastAge/1000/60 ;
                        if(csAge.Total_Age__c != null && csAge.Total_Age__c != 0){
                            csAgeUpdateMap.put(csNew.id, csAge.Total_Age__c);
                        }
                        csAgeAddList.add(csAge);
                    }
                    
                }    
            }
            if(csAgeAddList != null && !csAgeAddList.isEmpty()){
                try {
                    insert csAgeAddList;
                    if(csAgeUpdateMap != null && csAgeUpdateMap.keySet() != null && !csAgeUpdateMap.keySet().isEmpty()){
                        List <Case> csList = new List<Case>(); 
                        csList = [select id, CreatedDate,Reopened__c, Age__c from Case where id =: csAgeUpdateMap.keySet()] ;
                        for(Case cs : csList){
                            cs.Age__c =  csAgeUpdateMap.get(cs.id);
                        }
                        update csList ;
                    }
                }
                catch (Exception ex) {
                    String addId = '';
                    if(newTriggerVal != null && !newTriggerVal.isEmpty()){
                        for(case cs : newTriggerVal){ addId = cs.id +'   '+addId;}
                    }
                    
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog( new ApexDebugLog.Error('getCaseAgeUpdate','Case trigger','addId',ex));
                    System.debug('***Get Exception***'+ex);
                }
            }
        }
        catch (Exception ex) {
            String addId = '';
            if(newTriggerVal != null && !newTriggerVal.isEmpty()){
                for(case cs : newTriggerVal){ addId = cs.id +'   '+addId;}
            }
            
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('getCaseAgeUpdate','Case trigger','addId',ex));
            System.debug('***Get Exception***'+ex);
        }
    }
}