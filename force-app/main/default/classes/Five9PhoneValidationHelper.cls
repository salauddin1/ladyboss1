public class Five9PhoneValidationHelper implements Queueable,Database.AllowsCallouts {
    public list<String> Contactlist1;
    //============================get contact Phone no. DNC list from the trigger======================
    public Five9PhoneValidationHelper(list<String> Contactlist2){
        Contactlist1=Contactlist2;
    }
    public void execute(QueueableContext context) {
        list<Contact> contactList=[select Id,Phone from contact where Id IN:Contactlist1];
        List<Five9Credentials__c> Apikey=[select UserName__c,Password__c from Five9Credentials__c where isLive__c=:true];
        Five9Credentials__c Tokens=Apikey[0];   
        //===============================Check Phone Contact for dnc=================================== 
        String username = Tokens.UserName__c;
        String password = Tokens.Password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' +EncodingUtil.base64Encode(headerValue);
        //===============Invoke Soap API methods Using Phone no. from Contact records==================           
        five9calloutforcontactVCC  f9 = new five9calloutforcontactVCC();
        Map<String,String> headerMap = new Map<String,String>();
        headerMap.put('Accept-Encoding','gzip,deflate');
        headerMap.put('Content-Type','text/xml;charset=UTF-8');
        headerMap.put('SOAPAction','addNumbersToDnc');
        headerMap.put('Authorization', authorizationHeader);
        f9.inputHttpHeaders_x = headerMap;
        List<String> phone=new List<String>();
        if(!contactList.isEmpty()){
          for(contact con : contactList){
                phone.add(con.Phone);
            }
        }
        f9.addNumbersToDnc(phone);
    }
}