@isTest(seeAllData=true)
public class BatchCustomerMetaDataUpdateTEST{

    public static Temp_Card_Customer_Contact_link__c tempCard;
    public static void init(){
        tempCard = new Temp_Card_Customer_Contact_link__c();
        tempCard.EmailID__c ='test@gmail.com';
        insert tempCard;
    }
    @isTest
    public static void myUnitTest1(){
        Test.startTest();
        init();
        BatchCustomerMetaDataUpdate bc = new BatchCustomerMetaDataUpdate();
        Database.executeBatch(bc);
        Test.stopTest();
    }

}