global class AddressValidation  {
    private static final String SERVICE_URL='https://api.easypost.com/v2/addresses';
    global cls_error error;  
    global String id;  
    global String zip;  
    global String state;
    global static String resolveAddress(String street, String city, String state,String country,String zipcode){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        Easy_Post__c ep = Easy_Post__c.getInstance();
        system.debug('address validation'+ep.API_Key__c);
        http.setHeader('Authorization', 'Bearer '+ep.API_key__c);
        http.setBody('verify_strict[]=delivery&address[street1]='+street+'&address[city]='+city+'&address[state]='+state+'&address[zip]='+zipcode+'&address[country]=US&address[company]=LadyBoss');
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse(); 
        try {
                hs = con.send(http);
        } catch (CalloutException e) {
            system.debug('e.getMessage()-->'+e.getMessage());
        }
        system.debug('#### '+ hs.getBody());
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        try {
            AddressValidation o = AddressValidation.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** AddressValidation object: '+o); 
            if(o.error==null){
                if (!o.zip.contains(zipcode) || o.state != state) {
                    String errmsg =  'Please Correct the folowing: ' ;
                    if (!o.zip.contains(zipcode)) {
                        errmsg += 'ZipCode to ' + o.zip;
                    }
                    if (o.state != state) {
                        errmsg += ' State to ' + o.state;
                    }
                    return errmsg;
                }
                return 'No error';
            }else{
                String errmsg = o.error.message;
                Integer i = 0;
                for (cls_errors er : o.error.errors) {
                    if (i == 0) {
                        errmsg += '<br/>Error: ' + er.message;  
                    }else {
                        errmsg += ', ' + er.message;
                    }
                    i++;
                }
                return errmsg;
            }
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }       
    }
    
    public static AddressValidation parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        return (AddressValidation) System.JSON.deserialize(json, AddressValidation.class);
    }
    
    //Wrapper
    class cls_error {
        public String code;	//ADDRESS.VERIFY.FAILURE
        public String message;	//Unable to verify address.
        public cls_errors[] errors;
    }
    class cls_errors {
        public String code;	//E.ADDRESS.NOT_FOUND
        public String field;	//address
        public String message;	//Address not found
    }
}