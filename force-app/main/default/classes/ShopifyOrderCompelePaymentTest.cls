@isTest
public class ShopifyOrderCompelePaymentTest {

    public static TestMethod void test(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Card__c card = new Card__c();
        card.Last4__c = '1234';
        card.Card_ID__c = 'card_dw1ew4dadx2j';
        card.Contact__c = co.id;
        insert card;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'San Francisco';
        add.Shipping_Country__c = 'US';
        add.Shipping_State_Province__c = 'California';
        add.Shipping_Zip_Postal_Code__c = '94117';
        add.Shipping_Street__c = '215 Clayton St.';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.Email_Body__c = 'testeeMAIL';
        pro.Name = 'TestProduct';
        insert pro;
        Shopify_Parameters__c shopData = new Shopify_Parameters__c();
        shopData.Access_Token__c = 'testToken12';
        shopData.Domain_Name__c = 'https://test.com';
        shopdata.Is_Active__c = true;
        insert shopData;
        Shopify_Orders__c shopOrder = new Shopify_Orders__c();
        shopOrder.phone__c = '2345678';
        shopOrder.address1__c = 'Indore city';
        shopOrder.city__c = 'Indore';
        shopOrder.country__c = 'India';
        shopOrder.Status__c = 'Confirmed';
        insert shopOrder;
        shopOrder.Status__c = 'canceled';
        update shopOrder;
       
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new OrderCalloutTest()) ;
        String contactData ='{"sobjectType":"Shopify_Orders__c","Contact__c":"'+co.id+'","First_Name__c":"Ashish test Sharma test","Last_Name__c":"sharma","address1__c":"215 Clayton St.","city__c":"San Francisco","zip__c":"94117","province__c":"California","Send_Fulfillment_Receipt__c":true,"Send_Receipt__c":true,"Financial_Status__c":"pending","phone__c":"(505) 288-4551","Modify_Date__c":"2019-02-02","fulfillment_Status__c":"fulfilled","country__c":"US"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        ShopifyOrderCompelePayment.getContact(co.Id);
        ShopifyOrderCompelePayment.shipOrder('test');
        ShopifyOrderCompelePayment.getCard(co.id);
        ShopifyOrderCompelePayment.CreateShopifyOrders(contactData,productData,card.Id);
        ShopifyOrderCompelePayment.UpdateOder(shopOrder.Id);
        ShopifyOrderCompelePayment.cancelledOrder(shopOrder.Id);
        AddressValidation.resolveAddress('abc','test','abc','test','abc');
        Test.stopTest();
    }  
    
}