global class HMAC {
    
    public static Boolean generateSignature(String timestamppp ,String clientsecretVal) {
        String message = '';
        String[] parts = timestamppp.split(',');
        String[] part1 = parts[0].split('=');
        String[] part2 = parts[1].split('=');
        String valoftime = part1[1];
        System.debug('----->'+RestContext.request.requestBody.toString());
        Blob combined = Blob.valueOf(valoftime+'.'+RestContext.request.requestBody.toString());
        
        String signature = part2[1];
        String calc_sig = generateHmacSHA256Signature(combined,clientsecretVal);
        System.debug('Signature : ' + signature);
        System.debug(' sfdc Signature : ' + calc_sig);
        if (calc_sig == signature) {
            System.debug(System.LoggingLevel.ERROR, 'Signature matches');
            return true;
        } else {
            System.debug(System.LoggingLevel.ERROR, 'Signature does not match');
            return false;
        }
    }
    
    private static String generateHmacSHA256Signature(Blob combined, String clientsecretVal) {
        if(WebHookSecrets__c.getInstance(clientsecretVal) != null){
            WebHookSecrets__c myCustomSetting = WebHookSecrets__c.getInstance(clientsecretVal);
            if(myCustomSetting.Webhook_Secret__c != null && myCustomSetting.Webhook_Secret__c != ''){
                String calc_sig = EncodingUtil.convertToHex(Crypto.generateMac('hmacSHA256', combined, Blob.valueOf(myCustomSetting.Webhook_Secret__c)));
                return calc_sig;
            }
        }
        return 'Invalid';
    }
}