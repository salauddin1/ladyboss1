@isTest
public class ShopifyCardsControllerTest {
	@isTest
    public Static void TestMethodcontroller() {
     Contact con = new Contact();
     con.LastName = 'TestLastName';
        con.FirstName = 'TestFirstName';
        //con.Stripe_Customer_Id__c = 'cus__4dd23sees3we3x';
        con.Email = 'Test@gmail.com';
        con.Phone = '7067749234';
        con.MobilePhone = '1234567890';
        insert con;
        Card__c crd = new Card__c();
        crd.Token__c='tok_wejjdb2ee232ddx';
        crd.Credit_Card_Number__c = '1234567';
        crd.Card_ID__c = 'card__ id12345654';
        crd.Expiry_Month__c = '2';
        crd.Expiry_Year__c = '2020';
        crd.Shopify_Card__c = true;
        crd.Contact__c = con.Id;
        crd.Name_On_Card__c = 'TestCard';
        crd.Last4__c = '1234';
        insert crd;
        Test.startTest();
        ShopifyCardsController s = new ShopifyCardsController();
        s.cardInfo = '{"id":"tok_1ELTEDCBMFSb4PLxHXepaB9J","object":"token","card":{"id":"card_1ELTEDCBMFSb4PLxJTy1QHDH","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":"42424","address_zip_check":"unchecked","brand":"Visa","country":"US","cvc_check":"unchecked","dynamic_last4":null,"exp_month":4,"exp_year":2024,"funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null},"client_ip":"27.5.6.12","created":1554375329,"livemode":false,"type":"card","used":false}';
        s.customerID = 'cus__id123';
        s.showUpdateCard = false;
        s.selectedValue = 'testvalue';
        s.cardName = 'TestName';
        s.cardAction = 'TestAction';
        Test.setMock(HttpCalloutMock.class,new CustomerMockCLass()) ;
        ApexPages.currentPage().getParameters().put('id', String.valueOf(con.Id));
        
        s.openUpdateForm();
        Test.stopTest();
    }
}