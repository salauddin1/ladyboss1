@isTest
public class opportunityAfterUpdateTriggerTest {
    @isTest
    public static void insertTest1(){ 
        test.startTest();
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        Comission_Configuration__c config = new Comission_Configuration__c();
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        config.For_First_Sell__c = true;
        insert config;
        
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Senior_Agent__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.AccountId = acc.Id;
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.Payment_Date__c = null;
        op1.OwnerId= userinfo.getuserid();
        op1.Is_First_Sell__c = false;
        insert op1;
        Comission_Configuration__c config1 = new Comission_Configuration__c();
        config1.Min_Value__c = 0;
        config1.Max_Value__c = 100000;
        config1.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config1.Percentage_Paid_to_Director__c=12;
        config1.Percentage_Paid_to_Vice_President__c=12;
        config1.Percentage_Paid_to_Senior_Agent__c = 0;
        config1.For_First_Sell__c=true;
        insert config1;
        //test.startTest();
        Payment__c pay = new Payment__c();
        pay.Amount__c = 10;
        pay.week__c = 5;
        pay.Payment_Created_By__c = UserInfo.getUserId();
        pay.Product__c = prod.id;
        pay.Opportunity__c = op1.id;
        insert pay;
            Comission__c com1 = new Comission__c();
        com1.Payment__c = pay.id;
        com1.User__c = UserInfo.getUserId();
        com1.Amount__c = 12;
        insert com1;
        op1.Is_First_Sell__c = true;
        op1.Payment_Type__c = null;
        op1.Payment_Date__c = null;
        update op1;    
        test.stopTest();
    }
    @isTest
    public static void insertTest2(){ 
        PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        Comission_Configuration__c config = new Comission_Configuration__c();
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        config.For_First_Sell__c = true;
        insert config;
        
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        op1.Is_First_Sell__c = false;
        insert op1;
        Comission_Configuration__c config1 = new Comission_Configuration__c();
        config1.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config1.Percentage_Paid_to_Director__c=12;
        config1.Percentage_Paid_to_Vice_President__c=12;
        config1.Percentage_Paid_to_Senior_Agent__c = 0;
        //config1.Product__c = prod.id;
        insert config1;
        test.startTest();
        Payment__c pay = new Payment__c();
        pay.Amount__c = 10;
        pay.week__c = 5;
        pay.Product__c = prod.id;
        pay.Payment_Created_By__c = UserInfo.getUserId();
        pay.Opportunity__c = op1.id;
        insert pay;
            Comission__c com1 = new Comission__c();
        com1.Payment__c = pay.id;
        com1.User__c = UserInfo.getUserId();
        com1.Amount__c = 12;
        insert com1;
        op1.Is_First_Sell__c = true;
        op1.Payment_Type__c=null;
        update op1;    
        test.stopTest();
    }
    @isTest
    public static void insertTest3(){ 
        PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        Comission_Configuration__c config = new Comission_Configuration__c();
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        config.For_First_Sell__c = true;
        insert config;
        
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        //op1.Sales_Person_Id__c = userinf;
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        op1.Is_First_Sell__c = false;
        insert op1;
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        ol1.Consider_For_Commission__c = true;
        insert oL1;
        Comission_Configuration__c config1 = new Comission_Configuration__c();
        config1.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config1.Percentage_Paid_to_Director__c=12;
        config1.Percentage_Paid_to_Vice_President__c=12;
        config1.Percentage_Paid_to_Senior_Agent__c = 0;
        //config1.Product__c = prod.id;
        insert config1;
        test.startTest();
        Payment__c pay = new Payment__c();
        pay.Amount__c = 10;
        pay.week__c = 5;
        pay.Payment_Created_By__c = UserInfo.getUserId();
        pay.Product__c = prod.id;
        pay.Opportunity__c = op1.id;
        insert pay;
            Comission__c com1 = new Comission__c();
        com1.Payment__c = pay.id;
        com1.User__c = UserInfo.getUserId();
        com1.Amount__c = 12;
        insert com1;
        op1.Sales_Person_Id__c = userinfo.getUserId();
        update op1;    
        test.stopTest();
    }    
}