@IsTest
public class StripeListInvoice_Test {

	static testMethod void testParse() {
		String json = '{'+
		'    \"object\": \"list\",'+
		'    \"data\": ['+
		'        {'+
		'            \"id\": \"in_1BGucSDiFnu7hVq7UjEJWYAF\",'+
		'            \"object\": \"invoice\",'+
		'            \"amount_due\": 14700,'+
		'            \"application_fee\": null,'+
		'            \"attempt_count\": 1,'+
		'            \"attempted\": true,'+
		'            \"billing\": \"send_invoice\",'+
		'            \"charge\": \"ch_1BGupnDiFnu7hVq75QG3rA0X\",'+
		'            \"closed\": true,'+
		'            \"currency\": \"usd\",'+
		'            \"customer\": \"cus_BUTZMTLfmUyQ9H\",'+
		'            \"date\": 1508959972,'+
		'            \"description\": null,'+
		'            \"discount\": null,'+
		'            \"due_date\": 1511551972,'+
		'            \"ending_balance\": 0,'+
		'            \"forgiven\": false,'+
		'            \"lines\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": ['+
		'                    {'+
		'                        \"id\": \"sub_BeD21gvhlwqB2q\",'+
		'                        \"object\": \"line_item\",'+
		'                        \"amount\": 7500,'+
		'                        \"currency\": \"usd\",'+
		'                        \"description\": null,'+
		'                        \"discountable\": true,'+
		'                        \"livemode\": false,'+
		'                        \"metadata\": {},'+
		'                        \"period\": {'+
		'                            \"start\": 1508959972,'+
		'                            \"end\": 1511638372'+
		'                        },'+
		'                        \"plan\": {'+
		'                            \"id\": \"test1\",'+
		'                            \"object\": \"plan\",'+
		'                            \"amount\": 2500,'+
		'                            \"created\": 1508947075,'+
		'                            \"currency\": \"usd\",'+
		'                            \"interval\": \"month\",'+
		'                            \"interval_count\": 1,'+
		'                            \"livemode\": false,'+
		'                            \"metadata\": {},'+
		'                            \"name\": \"Test Product 1\",'+
		'                            \"statement_descriptor\": null,'+
		'                            \"trial_period_days\": null'+
		'                        },'+
		'                        \"proration\": false,'+
		'                        \"quantity\": 3,'+
		'                        \"subscription\": null,'+
		'                        \"subscription_item\": \"si_1BGucSDiFnu7hVq7nfW3kqaU\",'+
		'                        \"type\": \"subscription\"'+
		'                    },'+
		'                    {'+
		'                        \"id\": \"sub_BeD21gvhlwqB2q\",'+
		'                        \"object\": \"line_item\",'+
		'                        \"amount\": 6000,'+
		'                        \"currency\": \"usd\",'+
		'                        \"description\": null,'+
		'                        \"discountable\": true,'+
		'                        \"livemode\": false,'+
		'                        \"metadata\": {},'+
		'                        \"period\": {'+
		'                            \"start\": 1508959972,'+
		'                            \"end\": 1511638372'+
		'                        },'+
		'                        \"plan\": {'+
		'                            \"id\": \"test4\",'+
		'                            \"object\": \"plan\",'+
		'                            \"amount\": 3000,'+
		'                            \"created\": 1508959942,'+
		'                            \"currency\": \"usd\",'+
		'                            \"interval\": \"month\",'+
		'                            \"interval_count\": 1,'+
		'                            \"livemode\": false,'+
		'                            \"metadata\": {},'+
		'                            \"name\": \"Test 4\",'+
		'                            \"statement_descriptor\": null,'+
		'                            \"trial_period_days\": null'+
		'                        },'+
		'                        \"proration\": false,'+
		'                        \"quantity\": 2,'+
		'                        \"subscription\": null,'+
		'                        \"subscription_item\": \"si_1BGucSDiFnu7hVq7gC0SikUf\",'+
		'                        \"type\": \"subscription\"'+
		'                    }'+
		'                ],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 2,'+
		'                \"url\": \"/v1/invoices/in_1BGucSDiFnu7hVq7UjEJWYAF/lines\"'+
		'            },'+
		'            \"livemode\": false,'+
		'            \"metadata\": {},'+
		'            \"next_payment_attempt\": null,'+
		'            \"number\": \"2945c0ac44-0001\",'+
		'            \"paid\": true,'+
		'            \"period_end\": 1508959972,'+
		'            \"period_start\": 1508959972,'+
		'            \"receipt_number\": null,'+
		'            \"starting_balance\": 1200,'+
		'            \"statement_descriptor\": null,'+
		'            \"subscription\": \"sub_BeD21gvhlwqB2q\",'+
		'            \"subtotal\": 13500,'+
		'            \"tax\": null,'+
		'            \"tax_percent\": null,'+
		'            \"total\": 13500,'+
		'            \"webhooks_delivered_at\": 1508959973'+
		'        }'+
		'    ],'+
		'    \"has_more\": true,'+
		'    \"url\": \"/v1/invoices\"'+
		'}';
		StripeListInvoice.Invoices r = StripeListInvoice.Invoices.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Data_Z objData_Z = new StripeListInvoice.Data_Z(System.JSON.createParser(json));
		System.assert(objData_Z != null);
		System.assert(objData_Z.id == null);
		System.assert(objData_Z.object_Z == null);
		System.assert(objData_Z.amount_due == null);
		System.assert(objData_Z.application_fee == null);
		System.assert(objData_Z.attempt_count == null);
		System.assert(objData_Z.attempted == null);
		System.assert(objData_Z.billing == null);
		System.assert(objData_Z.charge == null);
		System.assert(objData_Z.closed == null);
		System.assert(objData_Z.currency_data == null);
		System.assert(objData_Z.customer == null);
		System.assert(objData_Z.date_data == null);
		System.assert(objData_Z.description == null);
		System.assert(objData_Z.discount == null);
		System.assert(objData_Z.due_date == null);
		System.assert(objData_Z.ending_balance == null);
		System.assert(objData_Z.forgiven == null);
		System.assert(objData_Z.lines == null);
		System.assert(objData_Z.livemode == null);
		System.assert(objData_Z.metadata == null);
		System.assert(objData_Z.next_payment_attempt == null);
		System.assert(objData_Z.number_Z == null);
		System.assert(objData_Z.paid == null);
		System.assert(objData_Z.period_end == null);
		System.assert(objData_Z.period_start == null);
		System.assert(objData_Z.receipt_number == null);
		System.assert(objData_Z.starting_balance == null);
		System.assert(objData_Z.statement_descriptor == null);
		System.assert(objData_Z.subscription == null);
		System.assert(objData_Z.subtotal == null);
		System.assert(objData_Z.tax == null);
		System.assert(objData_Z.tax_percent == null);
		System.assert(objData_Z.total == null);
		System.assert(objData_Z.webhooks_delivered_at == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Invoices objJSON2Apex = new StripeListInvoice.Invoices(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.object_Z == null);
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.has_more == null);
		System.assert(objJSON2Apex.url == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Metadata objMetadata = new StripeListInvoice.Metadata(System.JSON.createParser(json));
		System.assert(objMetadata != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Period objPeriod = new StripeListInvoice.Period(System.JSON.createParser(json));
		System.assert(objPeriod != null);
		System.assert(objPeriod.start == null);
		System.assert(objPeriod.end_data == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Data objData = new StripeListInvoice.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_Z == null);
		System.assert(objData.amount == null);
		System.assert(objData.currency_data == null);
		System.assert(objData.description == null);
		System.assert(objData.discountable == null);
		System.assert(objData.livemode == null);
		System.assert(objData.metadata == null);
		System.assert(objData.period == null);
		System.assert(objData.plan == null);
		System.assert(objData.proration == null);
		System.assert(objData.quantity == null);
		System.assert(objData.subscription == null);
		System.assert(objData.subscription_item == null);
		System.assert(objData.type_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Plan objPlan = new StripeListInvoice.Plan(System.JSON.createParser(json));
		System.assert(objPlan != null);
		System.assert(objPlan.id == null);
		System.assert(objPlan.object_Z == null);
		System.assert(objPlan.amount == null);
		System.assert(objPlan.created == null); 
		System.assert(objPlan.currency_data == null);
		System.assert(objPlan.interval == null);
		System.assert(objPlan.interval_count == null);
		System.assert(objPlan.livemode == null);
		System.assert(objPlan.metadata == null);
		System.assert(objPlan.name == null);
		System.assert(objPlan.statement_descriptor == null);
		//System.assert(objPlan.trial_period_days == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Lines objLines = new StripeListInvoice.Lines(System.JSON.createParser(json));
		System.assert(objLines != null);
		System.assert(objLines.object_Z == null);
		System.assert(objLines.data == null);
		System.assert(objLines.has_more == null);
		System.assert(objLines.total_count == null);
		System.assert(objLines.url == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Coupon objCoupon = new StripeListInvoice.Coupon(System.JSON.createParser(json));
		System.assert(objCoupon != null);
		System.assert(objCoupon.id == null);
		System.assert(objCoupon.object_data == null);
		System.assert(objCoupon.amount_off == null);
		System.assert(objCoupon.created == null);
		System.assert(objCoupon.currency_data == null);
        System.assert(objCoupon.duration == null);
        System.assert(objCoupon.duration_in_months == null);
        System.assert(objCoupon.livemode == null);
        System.assert(objCoupon.metadata == null);
        System.assert(objCoupon.percent_off == null);
        System.assert(objCoupon.times_redeemed == null);
        System.assert(objCoupon.valid == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListInvoice.Discount objDiscount = new StripeListInvoice.Discount(System.JSON.createParser(json));
		System.assert(objDiscount != null);
		System.assert(objDiscount.object_data == null);
		System.assert(objDiscount.coupon == null);
		System.assert(objDiscount.customer == null);
		System.assert(objDiscount.end_data == null);
		System.assert(objDiscount.start == null);
        System.assert(objDiscount.subscription == null);
	}
}