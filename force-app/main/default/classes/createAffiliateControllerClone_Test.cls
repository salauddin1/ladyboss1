@isTest
public class createAffiliateControllerClone_Test {
    @isTest
    public static void testMethod1(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        
        
        Contact ct = new Contact();
        ct.Email = 'tirth@test.com';
        ct.LastName = 'test';
        ct.FirstName = 'test';
        insert ct;
        Contact ct1 = new Contact();
        ct1.Email = 'tirth@test.com';
        ct1.LastName = 'test';
        ct1.FirstName = 'test';
        ct1.PAP_refid__c ='test1';
        insert ct1;
        
        Test.startTest();
        createAffiliateControllerClone.getContact(ct.id);
        createAffiliateControllerClone.createAffiliate(ct.id);
        createAffiliateControllerClone.updateAffiliate(ct.id,'abc');
        Test.stopTest();
    }
}