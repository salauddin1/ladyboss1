global class UpdateSubscriptionChargeDate implements Database.Batchable<sObject>, Database.AllowsCallouts {
    Long timestamp;
    global UpdateSubscriptionChargeDate (Long timestamp){
        this.timestamp = timestamp;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select id, Customer_Id__c, Period_End__c, Is_Processed__c, Period_Start__c, Plan_ID__c,Subscription_Id__c,Status__c,Trial_Start__c,Trial_End__c FROM Subscription__c WHERE IS_Processed__c =false');
	}

    global void execute(Database.BatchableContext jobId, List<Subscription__c> recordList) {
        for (Subscription__c sub: recordList) {
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://api.stripe.com/v1/subscriptions/'+sub.Subscription_Id__c);
            req.setMethod('POST');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('content-type', 'application/x-www-form-urlencoded');
            req.setBody('trial_end='+timestamp+'&prorate=false');
            if (!Test.isRunningTest()) {
                HttpResponse res = http.send(req);
                if(res.getStatusCode() == 200){
                    sub.Is_Processed__c = true;
                } 
            }
            
        }
        update recordList;
    }

    global void finish(Database.BatchableContext jobIdParam) {
       
    }
}