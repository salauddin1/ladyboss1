global class RpvDNCBatchforMobile  implements Database.Batchable<sobject>,Schedulable, Database.AllowsCallouts {
    global list<String> Contactlist1;     
    //=======================Cunstroctor for pass Contact Id in batch=========================
    global RpvDNCBatchforMobile (list<String> Contactlist2){
        Contactlist1=Contactlist2;
    }
    global RpvDNCBatchforMobile(){}
    global database.querylocator start(database.batchablecontext bc) {
        
            string query='select id, DoNotCall, Mobile_State_DNC__c, mobilephone,Mobile_Scrub_Processed__c from contact where mobilephone!=null  and Mobile_Scrub_Processed__c=false order by mobilephone desc limit 50000';
            return database.getquerylocator( query );
          
    }
    global void execute(database.BatchableContext bc,list<Contact> contactList) {
        Boolean flag=false;
        list<String> ContactID =new list<String> ();
        list<Contact> UpdateContactDNC=new list<Contact> ();
        List<Five9Credentials__c> Apikey=[select RealPhoneValidationKey__c from Five9Credentials__c where isLive__c=:true];
        for(Contact con : contactList) {
            ContactID.add(con.Id);
            String MobilePhonenumber= con.MobilePhone.replaceAll('\\D','');
            if(!Apikey.isEmpty() && Apikey!=null){
                Five9Credentials__c Tokens=Apikey[0];
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint('https://api.realvalidation.com/rpvWebService/DNCLookup.php?phone='+MobilePhonenumber+'&token='+Tokens.RealPhoneValidationKey__c);
                req.setMethod('GET');
                HttpResponse res = h.send(req);
                Dom.Document doc = res.getBodyDocument();
                System.debug('===============================Response'+res.getBody());
               //==================parse the response xml file and update contact ========================
                Dom.XMLNode DncNode = doc.getRootElement();
                String NationalDnc = DncNode.getChildElement('national_dnc', null).getText();
                String stateDnc = DncNode.getChildElement('state_dnc', null).getText();
                if(NationalDnc.equalsIgnoreCase('Y')){
                    con.DoNotCall=true;
                    flag=con.DoNotCall;
                }
                if(stateDnc.equalsIgnoreCase('Y')){
                    con.Mobile_State_DNC__c=true;
                    }
                if(NationalDnc.equalsIgnoreCase('N')) {
                    con.DoNotCall=false;
                }
                if(stateDnc.equalsIgnoreCase('N')){
                    con.Mobile_State_DNC__c=false;
                    }
                con.Mobile_Scrub_Processed__c=true;
                UpdateContactDNC.add(con);
            }
        }
            update UpdateContactDNC;
        //if(flag == true){
            //System.enqueueJob(new Five9MobileValidationHelper(ContactID));
        //}
    }
    global void finish(database.batchablecontext bc){}
    global void execute(SchedulableContext sc) {
        //RpvDNCBatchforMobile b = new RpvDNCBatchforMobile();
        //database.executebatch(b);
    }
}