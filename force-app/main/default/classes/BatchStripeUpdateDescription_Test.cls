@isTest
public class BatchStripeUpdateDescription_Test {
    @isTest
    public static void testMe(){
        
        Contact con = new Contact();
        con.FirstName ='ashish';
        con.LastName = 'sharma';
        con.Email = 'ashish.sharma@gmail.com';
        insert con;
        
        BatchSetting__c bc = new BatchSetting__c();
        bc.LimitStripe__c = '11';
        insert bc;
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        opp.Stripe_Charge_Id__c = 'ch_1EDWCFBwLSk1v1oh71gmP9yA';
        insert opp;
        
        Test.setMock(HttpCalloutMock.class, new StripeUpdateMock());
        BatchStripeUpdateDescription bs = new BatchStripeUpdateDescription();
        dataBase.executeBatch(bs, 1);
        
    }
}