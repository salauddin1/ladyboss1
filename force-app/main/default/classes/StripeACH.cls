global class StripeACH {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';

    global String stripeObject;
    global String account_holder_name;
    global String account_holder_type;
    global String bank_name;
    global String address_country;
    global String last4;
    global String currency_val;
    global String id;
    global StripeError error;
    global String customer_id;
    global String status;

    global static StripeACH createACHWithToken(String customerId, String token,String cardId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources');
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Salesforce Card Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+cardId);
        
        Map<String, String> payload = new Map<String, String>{
            'source' => token
        };
         if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeACH o = StripeACH.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeACH object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    
    global static StripeACH verifyACHAccount(String customerId,String stripeACHId,Decimal amount1,Decimal amount2) {
        System.debug('stripeACHId----'+stripeACHId);
        System.debug('amount1----'+amount1);
        system.debug('String.valueof(Integer.valueof(amount1))---'+String.valueof(Integer.valueof(amount1)));
        system.debug('String.valueof(Integer.valueof(amount2))---'+String.valueof(Integer.valueof(amount2)));
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources/' + stripeACHId + '/verify');
        System.debug('http.setEndpoint----'+http.getEndpoint());
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        Map<String,String> metadata = new Map<String,String>();
        //metadata.put('Salesforce Card Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+stripeACHId);
        Map<String, String> payload = new Map<String, String>{
            //'source' => token
            'amounts[0]' => String.valueof(Integer.valueof(amount1)),
   			'amounts[1]' => String.valueof(Integer.valueof(amount2))
                
        };
        
         /*if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }*/
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            System.debug('res---->'+response);
            StripeACH o = StripeACH.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeACH object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }


    public static StripeACH parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);

        return (StripeACH) System.JSON.deserialize(json, StripeACH.class);
    }
    
    
}