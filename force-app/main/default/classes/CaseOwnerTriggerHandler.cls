public class CaseOwnerTriggerHandler {
    
    public static void doBeforeUpdate(List<Case> caList){
        Case ca = caList[0];
        system.debug(ca.CaseNumber);
        If(ca.Status == 'Closed' && ca.ClosedDate == Null){
            system.debug(ca.OwnerId+' '+ca.Owner.Type);
            if(string.valueOf(ca.OwnerId).startsWith('00G')){
                system.debug('Owner --> Queue');
                List<GroupMember> gmList =  [SELECT Group.Name FROM GroupMember WHERE UserOrGroupId = :ca.LastModifiedById AND Group.Type = 'Queue'  AND GroupId = :ca.OwnerId];
                system.debug(gmList.isEmpty());
                system.debug(gmList.size());
                if(!gmList.isEmpty() && gmList.size() > 0){
                    ca.Is_Memeber_Of_Queue__c = true;
                    system.debug(ca);
                    //update ca;
                }else{
                    ca.Is_Memeber_Of_Queue__c = false;
                }
            }
        }
    }    
}