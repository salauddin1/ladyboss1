/*
 * Developer Name : Tirth patel
 * Description : This batch searches for all contacts with same emails and merge them all and remove duplicate contacts with stroing maximum information from all contacts.
 */
global  class ContactMergeBatch implements Database.Batchable<SObject>,Database.Stateful,Database.AllowsCallouts{
    global Set<Id> alreadyDuplicateList;
    private String sessionId;
    
    global ContactMergeBatch(String sessionId){
        alreadyDuplicateList = new Set<Id>();
        this.sessionId = sessionId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        Set<String> accFieldList = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet();
        List<String> strList = new List<String>();
        strList.addAll(accFieldList);
        String fields = string.join(strList,',');
        String dynamicquery = 'select '+fields+' from Contact where check_for_dup__c = true order by lastmodifieddate asc ';
        return Database.getQueryLocator(dynamicquery);
    }

    global void execute(Database.BatchableContext context, List<SObject> records) {
        ContactMergeBatchesHandler.mergeHandler(records, alreadyDuplicateList, sessionId, 'check_for_dup__c');
    }
    
    global void finish(Database.BatchableContext context) {
        List<Contact_Merge_Manage__c> runMergeJob = [SELECT run_contact_merge__c FROM Contact_Merge_Manage__c];
        if(!test.isRunningTest() && runMergeJob.size() == 1 && runMergeJob.get(0).run_contact_merge__c)  {
            BatchFindDuplicates bt = new BatchFindDuplicates();
            Database.executeBatch(bt);
        }
    }  
}