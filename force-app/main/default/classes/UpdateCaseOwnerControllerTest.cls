@isTest 
private class UpdateCaseOwnerControllerTest {
    static testMethod void testMethod1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='ladybossassi@testorg.com');
        
        Case c = new Case();
        c.Status = 'Open';
        c.Origin = 'Phone';
      
        
        Test.startTest();
        insert u;
        c.OwnerId = u.Id;
        system.debug(c);
            insert c;
            Test.setCurrentPage(Page.UpdateCaseOwnerButton);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(new List<Case> {c});
            stdSetController.setSelected(new List<Case>{c});
            UpdateCaseOwnerController up = new UpdateCaseOwnerController(stdSetController);
        	up.User = u.Id;
            up.closeModal();
            up.updateSelectedCases();
        Test.stopTest();
    }
}