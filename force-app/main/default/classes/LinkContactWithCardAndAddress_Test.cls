@isTest
public class LinkContactWithCardAndAddress_Test {
	@testSetup static void methodName() {
		StripeCustomer sc = StripeCustomer.create('test', 'test','test@test.com','1234 Main street Newyork Ny US','12345',null);
		Contact con = new Contact();
		con.LastName = 'test';
		con.Stripe_Customer_Id__c = sc.Id;
		insert con;
		Address__c address = new Address__c();
		address.CustomerId__c=sc.Id;
		insert address;
		Card__c card = new Card__c();
		card.CustomerId__c=sc.Id;
		card.Token__c='test';
        insert card;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c = sc.Id;
        sp.Customer__c = con.Id;
        insert sp;
	}
	static testmethod void test()  {
		Test.startTest();
		
        LinkContactWithCardAndAddress.dummyCoverage();
        LinkContactWithCardAndAddress lcwcaa = new LinkContactWithCardAndAddress();
		String sch = '0 0 23 * * ?';
		system.schedule('Test', sch, lcwcaa);
		Test.stopTest();

	}
}