global without sharing class PaypalIntegrationHandlerChanges {
	public static String OpportunityPaypalHandler(String PaypalBA, Map<String,Opportunity> oppList ,Map<String,List<OpportunityLineItem>> oppLineItemList,boolean isSchedule,String salesPerson,Decimal salesTaxPercentage, Boolean isErrorOrderForm,Boolean isAsyncError){
		try {
			String paypalTransactionId = '';
			String paypalPaymentId = '';
			Id opportunityClubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Subscription').getRecordTypeId();
			Id opportunityNonClubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Charge').getRecordTypeId();
			Paypal_Billing_Agreement__c pba = [SELECT id,Billing_Agreement__c,Billing_Token__c,Verified__c,Is_Canceled__c FROM Paypal_Billing_Agreement__c WHERE id = :PaypalBA];
			System.debug('PaypalBA:-'+PaypalBA);
			System.debug('oppList:-'+oppList);
			System.debug('oppLineItemList:-'+oppLineItemList);
			List<OpportunityLineItem> oplineitemlisttoinsert = new List<OpportunityLineItem>();
			List<OpportunityLineItem> oplineitemlistClub = new List<OpportunityLineItem>();
			List<OpportunityLineItem> oplineitemlisttoNonClub = new List<OpportunityLineItem>();
			for(String key:oppLineItemList.keyset()){
				if (key == 'club') {
					oplineitemlistClub.addAll(oppLineItemList.get(key));
				}else {
					oplineitemlisttoNonClub.addAll(oppLineItemList.get(key));
				}
				oplineitemlisttoinsert.addAll(oppLineItemList.get(key));
			}
			Decimal totalAmount = 0;
				Decimal tax = 0;
				Map<Id, Decimal> prodIdTaxMap = new Map<Id, Decimal>();
				Map<Id, Decimal> prodIdDurationMap = new Map<Id, Decimal>();
				for(OpportunityLineItem opli:oplineitemlisttoinsert){
					totalAmount += opli.TotalPrice;
					if (opli.Tax_Amount__c != null) {                    
						tax += opli.Tax_Amount__c;
						prodIdTaxMap.put(opli.Product2Id,opli.Tax_Amount__c);
					}else {
						prodIdTaxMap.put(opli.Product2Id,0);                    
					}
				}
				String listitem = '';
				for (Product2 prod : [SELECT Id,Name,Description,Price__c,PayPal_Subscription_Interval__c FROM Product2 WHERE Id IN:prodIdTaxMap.keySet()]) {
					listitem += '{ "sku": "'+prod.Name+'", "name": "'+prod.Name+'", "description": "'+prod.Description+'", "quantity": "1", "price": "'+prod.Price__c +'", "currency": "USD", "tax": "'+prodIdTaxMap.get(prod.Id)+'", "url": "https://tirthbox-ladyboss-support.cs22.force.com/" },';
					prodIdDurationMap.put(prod.Id,prod.PayPal_Subscription_Interval__c);
				}
				listitem = listitem.removeEnd(',');
			if (!isSchedule) {				
				PaypalAPICreds__c paypalApi = PaypalAPICreds__c.getInstance('Test');
				HttpRequest req = new HttpRequest();
				HttpResponse res = new HttpResponse();
				Http h = new Http();
				req.setEndpoint('https://api.sandbox.paypal.com/v1/payments/payment');
				req.setMethod('POST');
				req.setTimeout(120000);
				
				Blob headerValue = Blob.valueOf(paypalApi.Client_ID__c + ':' + paypalApi.Secret__c);
				String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
				req.setHeader('Authorization', authorizationHeader);  
				req.setHeader('Content-Type', 'application/json');
				req.setBody('{ "intent": "sale", "payer": { "payment_method": "PAYPAL", "funding_instruments": [ { "billing": { "billing_agreement_id": "'+pba.Billing_Agreement__c+'" } }] }, "transactions": [ { "amount": { "currency": "USD", "total": "'+(tax + totalAmount)+'", "details":{ "subtotal" : "'+totalAmount+'", "tax": "'+tax+'"} }, "description": "Payment transaction.", "custom": "Payment custom field.", "note_to_payee": "Note to payee field.", "item_list": { "items": [ '+listitem+'] } }], "redirect_urls": { "return_url": "https://tirthbox-ladyboss-support.cs22.force.com/PaypalReturnURL", "cancel_url": "https://tirthbox-ladyboss-support.cs22.force.com/PaypalCancelUrl" } }');
				try{
					if (!Test.isRunningTest()) {                    
						res = h.send(req);
					}else {
						res.setBody('{ "id": "PAY-8FF33419E3614440HK7WXAZA", "intent": "sale", "state": "approved", "payer": { "payment_method": "paypal", "status": "VERIFIED", "payer_info": { "email": "doe@example.com", "first_name": "John", "last_name": "Doe", "payer_id": "ZU7HZ76P4VL5U", "country_code": "US", "billing_address": { "line1": "7700 Eastport Pkwy", "line2": "", "city": "La Vista", "state": "NE", "postal_code": "68128", "country_code": "US" } } }, "transactions": [{ "amount": { "total": "1.00", "currency": "USD", "details": {} }, "payee": { "merchant_id": "F29HACJW4XYU4" }, "description": "Payment transaction.", "custom": "Payment custom field.", "note_to_payee": "Note to payee field.", "invoice_number": "GDAGDS5754YEK", "item_list": { "items": [{ "sku": "skuitemNo1", "name": "ItemNo1", "description": "The item description.", "quantity": "1", "price": "1.00", "currency": "USD", "tax": "0", "url": "https://example.com/" }] }, "related_resources": [{ "sale": { "id": "1B493786DU991744K", "state": "completed", "amount": { "total": "1.00", "currency": "USD", "details": { "subtotal": "1.00" } }, "payment_mode": "INSTANT_TRANSFER", "protection_eligibility": "ELIGIBLE", "protection_eligibility_type": "ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE", "transaction_fee": { "value": "0.10", "currency": "USD" }, "billing_agreement_id": "BA-8A802366G0648845Y", "parent_payment": "PAY-8FF33419E3614440HK7WXAZA", "create_time": "2016-09-29T19:49:58Z", "update_time": "2016-09-29T19:49:59Z", "links": [{ "href": "https://api.sandbox.paypal.com/v1/payments/sale/1B493786DU991744K", "rel": "self", "method": "GET" }, { "href": "https://api.sandbox.paypal.com/v1/payments/sale/1B493786DU991744K/refund", "rel": "refund", "method": "POST" }, { "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-8FF33419E3614440HK7WXAZA", "rel": "parent_payment", "method": "GET" } ] } }] }], "create_time": "2016-09-29T19:49:59Z", "links": [{ "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-8FF33419E3614440HK7WXAZA", "rel": "self", "method": "GET" }] }');
					}
					Map<String, Object> untypedMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
					System.debug('paypal payment response '+ untypedMap);
					
					if (String.valueOf(untypedMap.get('state')) == 'approved') {
						paypalPaymentId = String.valueOf(untypedMap.get('id'));
						List<Object> transactions = (List<Object>)untypedMap.get('transactions');
						Map<String, Object> obmap = new Map<String,Object>();
						for (Object o : transactions) {
							obmap = (Map<String, Object>)o;                       
						}            
						List<Object> related_resources = (List<Object>)obmap.get('related_resources');
						for (Object o : related_resources) {
							Map<String, Object> obmap1 = (Map<String, Object>)o;
							
							Map<String, Object> sale = (Map<String, Object>)obmap1.get('sale');
							
							System.debug('transacction '+String.valueOf(sale.get('id'))); 
							paypalTransactionId = String.valueOf(sale.get('id'));
						}

					}else if(String.valueOf(untypedMap.get('code')).contains('AGREEMENT_ALREADY_CANCELLED')){
						pba.Is_Canceled__c = true;
						update pba;
						return String.valueOf(untypedMap.get('message'));
					}          
				}catch(Exception e){
					return e.getMessage();
				}
			}
				
			Map<String, Id> NameIdMap = new Map<String, Id>();
			List<String> uniqueNameList = new List<String>();
            string datetimeValString = String.valueOf(Datetime.now());
            Set<Id> oppIdSet = new Set<Id>();
			for(String key:oppList.keyset()){
                Opportunity opp = oppList.get(key);
                
				if(opp.Name.contains('club')){
					opp.RecordTypeId  = opportunityClubRecordTypeId;
				}else{
					opp.RecordTypeId = opportunityNonClubRecordTypeId;
				} 
				opp.Is_PayPal__c = true;
				if (paypalPaymentId != null && paypalPaymentId != '') {						
					opp.Paypal_Charge_Id__c = paypalPaymentId;
					opp.Paypal_Transaction_Id__c = paypalTransactionId;
				}
				opp.Paypal_Billing_Agreement__c = pba.Id;
				if(opp.Id == null){
					if(opp.Name.length()>120){
						opp.Opportunity_Unique_Name__c = opp.name.subString(0,120) + '-'+ paypalPaymentId + '-'+datetimeValString;
					}else{
						opp.Opportunity_Unique_Name__c = opp.name + '-'+ paypalPaymentId + '-'+datetimeValString;
					}
					uniqueNameList.add(opp.Opportunity_Unique_Name__c);
				}
			}
			if (isSchedule || (paypalPaymentId != null && paypalPaymentId !='') ) {				
				upsert oppList.values();
			}
			List<PayPal_Subscription_Charge__c> pscList = new List<PayPal_Subscription_Charge__c>();
			Map<String, String> oppIdNameMap = new Map<String, String>();
			for (Opportunity op : [SELECT id,Name FROM Opportunity WHERE Opportunity_Unique_Name__c IN:uniqueNameList ]) {
				oppIdNameMap.put(op.Name, op.Id);
			}
			for (OpportunityLineItem opli : oplineitemlisttoinsert) {
				if (opli.OpportunityId != null || oppIdNameMap.containsKey(opli.Opportunity_Name__c)) {
					if (opli.OpportunityId == null) {
						opli.OpportunityId = oppIdNameMap.get(opli.Opportunity_Name__c);
                    }
                    oppIdSet.add(opli.OpportunityId);
					if (oplineitemlistClub.contains(opli) && !isSchedule) {

						PayPal_Subscription_Charge__c psc = new PayPal_Subscription_Charge__c();
						psc.Opportunity__c = opli.OpportunityId  != null ? opli.OpportunityId : oppIdNameMap.get(opli.Opportunity_Name__c);
						psc.Subscribed_Product__c = opli.Product2Id;
						psc.Paypal_Transaction_Id__c = paypalTransactionId;
						psc.Paypal_Charge_Id__c = paypalPaymentId;
						pscList.add(psc);
						opli.Status__c = 'Active';
						opli.Start__c = Date.today();
						if (prodIdDurationMap.get(opli.Product2Id) == null || prodIdDurationMap.get(opli.Product2Id) == 30) {
							opli.End__c = Date.today().addMonths(1);
						}else {
							opli.End__c = Date.today().addDays(Integer.valueOf(prodIdDurationMap.get(opli.Product2Id)));
						}
					}
				}
			}
			if (isSchedule || (paypalPaymentId != null && paypalPaymentId !='') ) {
				upsert oplineitemlisttoinsert;
				insert pscList;
            }
            if (!isSchedule) {
                StripeIntegrationHandlerChangesPaypal.sendmailInvoiceToCustomer(oppIdSet);
            }
			return 'Success';
			
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}