/*
 * Developer Name : Tirth Patel
 * Description : This batch sends maximum fourty messages in time period of every five minutes from all distinct from_numbers during business hours. 
*/

global class SchedulerOfSMSBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful ,Schedulable {
    global Map<String,Integer> fromNumCount = new Map<String,Integer>();
    //Get details of contact,message to be sent if IsSchedule__c is true
    global Database.QueryLocator start(Database.BatchableContext bc) {
        DateTime nowIs = System.now();
        List<Id> lstIds = new List<Id>();
        List<Campaign_Sms_Time__c> cst = [select id,Start_Hour__c,End_Hour__c from Campaign_Sms_Time__c];
        List<ScheduleSMS__c> lst1 = [SELECT Id, CampaignMemberId__c, MessageData__c, IsSchedule__c, From_Number__c ,Schedule_Time__c,Contact__c FROM ScheduleSMS__c WHERE IsSchedule__c = true and (Schedule_Time__c = null or Schedule_Time__c <= :nowIs)];
		for(ScheduleSMS__c sdsmsobj : lst1){
            if( (sdsmsobj.Schedule_Time__c == null 
                     && DateTime.now().hour() >= Integer.valueOf(cst[0].Start_Hour__c)  
                     && DateTime.now().hour() < Integer.valueOf(cst[0].End_Hour__c) )  
                   || sdsmsobj.Schedule_Time__c <= DateTime.now()) {
                       if(fromNumCount.get(sdsmsobj.From_Number__c)==null){
                           fromNumCount.put(sdsmsobj.From_Number__c,0);
                       }
                       if(fromNumCount.get(sdsmsobj.From_Number__c) < 40){
                           lstIds.add(sdsmsobj.Id);
                           fromNumCount.put(sdsmsobj.From_Number__c,fromNumCount.get(sdsmsobj.From_Number__c)+1);
                       }else{
                           continue;
                       }
                   }
            
        }                                                                                                                                                                                  
        return Database.getQueryLocator('SELECT Id, CampaignMemberId__c, MessageData__c, IsSchedule__c, From_Number__c ,Schedule_Time__c,Contact__c FROM ScheduleSMS__c WHERE id in :lstIds');
    }
    //Scheduler for the batch
    global void execute(SchedulableContext ctx) {
        if(Test.isRunningTest())  {
            SchedulerOfSMSBatch sdbatch = new SchedulerOfSMSBatch();
        	database.executebatch(sdbatch,40);
        }else {
        	SchedulerOfSMSBatch sdbatch = new SchedulerOfSMSBatch();
        	database.executebatch(sdbatch,1);
        }
    }   
    //Checks for whether Schedule time is given or not. If time is given and it is less or equal to now then message will be sent else it will check for current hours then send message.
    global void execute(Database.BatchableContext bc, List<ScheduleSMS__c> records){
        List<ScheduleSMS__c> schMsgDelList = new List<ScheduleSMS__c>();
        List<String> cmpMemIdList = new List<String>();
        for(ScheduleSMS__c sm : records){
            cmpMemIdList.add(sm.CampaignMemberId__c);
        }
        List<CampaignMember> cmbList = [select id,ContactId from CampaignMember where id in : cmpMemIdList];
        Map<String,CampaignMember> idCpMemMap = new Map<String,CampaignMember>();
        for(CampaignMember cm : cmbList){
            idCpMemMap.put(cm.Id,cm);
        }
        
        List<Campaign_Sms_Time__c> cst = [select id,Start_Hour__c,End_Hour__c from Campaign_Sms_Time__c];
        for(ScheduleSMS__c sdsmsobj : records){
            if(idCpMemMap.get(sdsmsobj.CampaignMemberId__c)!=null){
                 if(!Test.isRunningTest() && sdsmsobj.MessageData__c != null){
					CampaignMemberTriggerHandler.sendSMSwithoutFuture(sdsmsobj.CampaignMemberId__c,sdsmsobj.Contact__c , sdsmsobj.MessageData__c,sdsmsobj.From_Number__c);
				}
                sdsmsobj.IsSchedule__c = false;
            }else{
                schMsgDelList.add(sdsmsobj);
            }
        }
        update records;
        delete schMsgDelList;
    }    
    
    global void finish(Database.BatchableContext bc){
        for(String sf : fromNumCount.keySet()){
            if(fromNumCount.get(sf)>=40){
                DateTime dt = System.now().addMinutes(2);
                Integer sec = dt.second();
                Integer mnt = dt.minute();
                Integer hr = dt.hour();
                Integer day_month = dt.day();
                Integer month = dt.month();
                Integer year = dt.year();
                String CRON_EXP = sec+' '+mnt+' '+hr+' '+day_month+' '+month+' '+'? '+year;
                if(!Test.isRunningTest()){
                    SchedulerOfSMSBatch sdbatch = new SchedulerOfSMSBatch();
                    System.schedule('SchedulerOfSMSBatch '+System.now(), CRON_EXP, sdbatch);
                }
                break;
            }
        } 
    }    
}