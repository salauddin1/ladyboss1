global class RpvDNCBatchMockImpl implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
         string sessionBody = '<?xml version="1.0" encoding="UTF-8"?>'+
			'<response>'+
				'<RESPONSECODE>OK</RESPONSECODE>'+
				'<RESPONSEMSG></RESPONSEMSG>'+
				'<national_dnc>Y</national_dnc>'+
				'<state_dnc>Y</state_dnc>'+
				'<dma>N</dma>'+
				'<litigator>N</litigator>'+
				'<iscell>Y</iscell>'+
				'<id>15705626-C</id>'+
			'</response>';

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(sessionBody);
        res.setStatusCode(200);
        return res;
        }
}