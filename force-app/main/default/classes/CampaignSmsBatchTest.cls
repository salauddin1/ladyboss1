@isTest
public class CampaignSmsBatchTest {
    static testMethod void CampaignSmstest(){
        Campaign_Sms_Time__c cst = new Campaign_Sms_Time__c();
        cst.Name = 'cst Time';
        cst.Start_Hour__c = 8;
        cst.End_Hour__c = 16;
        insert cst;
        Contact ct = new Contact();
        ct.LastName = 'testse';
        ct.MobilePhone = '52452523';
        ct.Email = 'tirthtest1@gmail.com';
        ct.MailingPostalCode = '86029';
        insert ct;
        TimeZoneByZipCode__c tc = new TimeZoneByZipCode__c();
        tc.ZipCode__c = '86029';
        tc.City__c = 'tet';
        tc.Country__c = 'sdgssg';
        tc.Time_Zone__c = 'America/Shiprock';
        tc.Name ='testset';
        insert tc;
        Campaign c = new Campaign();
        c.Name ='testse';
        c.IsActive = true;
        c.Campaign_number__c = 'TwilioMain';
        insert c;
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.ContactId = ct.Id;
        cm.Created_Date__c = date.today()-2;
        cm.Created_Date_Time__c = system.now();
        insert cm;
        Campaign_Stage__c cs = new Campaign_Stage__c();
        cs.Age__c=0;
        cs.Name = 'test';
        cs.From_Number__c='24524535';
        cs.Campaign__c =c.Id;
        cs.SMS_Template__c='Campaign_Test_Template_4';
        insert cs;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        CampaignSmsBatch cb = new CampaignSmsBatch();
        Database.executeBatch(cb,1);
        SchedulableContext ctx=null;
        cb.execute(ctx);
        test.stopTest();
    }
    static testMethod void CampaignSmstest2(){
        Campaign_Sms_Time__c cst = new Campaign_Sms_Time__c();
        cst.Name = 'cst Time';
        cst.Start_Hour__c = 8;
        cst.End_Hour__c = 16;
        insert cst;
        Contact ct = new Contact();
        ct.LastName = 'testse';
        ct.MobilePhone = '52452523';
        ct.Email = 'tirthtest1@gmail.com';
        ct.otherPostalCode = '86029';
        insert ct;
        TimeZoneByZipCode__c tc = new TimeZoneByZipCode__c();
        tc.ZipCode__c = '86029';
        tc.City__c = 'tet';
        tc.Country__c = 'sdgssg';
        tc.Time_Zone__c = 'America/Shiprock';
        tc.Name ='testset';
        insert tc;
        
        
        //EmailTemplate et = [select id,DeveloperName,body from EmailTemplate limit 1];
        Campaign c = new Campaign();
        c.Name ='testse';
        c.IsActive = true;
        c.Campaign_number__c = 'TwilioMain';
        insert c;
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.ContactId = ct.Id;
        cm.Created_Date__c = date.today()-2;
        insert cm;
        Campaign_Stage__c cs = new Campaign_Stage__c();
        cs.Age__c=2;
        cs.Name = 'test';
        cs.From_Number__c='24524535';
        cs.Campaign__c =c.Id;
        cs.SMS_Template__c='Campaign_Test_Template_4';
        insert cs;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        CampaignSmsBatch cb = new CampaignSmsBatch();
        Database.executeBatch(cb,1);
        test.stopTest();
    }
}