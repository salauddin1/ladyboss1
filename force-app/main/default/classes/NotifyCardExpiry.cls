//This class is to notify the customer about their failed card
Public class NotifyCardExpiry{
    //Get all the cards of the contact
    @AuraEnabled
    public static List<card__c> getCards(String contactId ){
        List<card__c> cardList = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email,Contact__r.phone,Contact__r.OtherPhone,Contact__r.MobilePhone ,CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: contactId and Stripe_Card_Id__c!=null ];
        return cardList ;
    }
    //Get all the subscriptions of the contact
    @AuraEnabled
    public static List<OpportunityLineItem > getCardsOpportunity(String contactId ){
        List<card__c> cardList1 = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email,Contact__r.phone,Contact__r.OtherPhone,Contact__r.MobilePhone , CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: contactId and Stripe_Card_Id__c!=null ];
        Set<Id> cardSet = new Set<ID>();
        for(card__c c : cardList1 ) {
            
            cardSet.add(c.id);
            
        }
        //List<Opportunity> lstOpp = [select id , name,Card__c  from Opportunity where Card__c IN : cardSet and recordType.developername = 'Subscription' and     (Success_Failure_Message__c !='customer.subscription.deleted' ) ];
        List<OpportunityLineItem > lstOpp = [SELECT Id, Product2.name,Opportunity.Card__c  ,OpportunityId, Name FROM OpportunityLineItem where Opportunity.Card__c IN : cardSet  and  Opportunity.recordType.developername = 'Subscription'  and   (Opportunity.Success_Failure_Message__c !='customer.subscription.deleted' and Opportunity.Success_Failure_Message__c !='canceled')];
        return lstOpp ;
    }
    //Get all the subscriptions of the contact
     @AuraEnabled
    public static List<Opportunity> getCardsOpportunitySub(String contactId ){
        List<card__c> cardList1 = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email,Contact__r.phone,Contact__r.OtherPhone,Contact__r.MobilePhone , CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: contactId and Stripe_Card_Id__c!=null ];
        Set<Id> cardSet = new Set<ID>();
        for(card__c c : cardList1 ) {
            //Add the card ID to the set
            cardSet.add(c.id);
            
        }
        //To fetch the Subscriptio
        List<Opportunity> lstOpp = [select id , name,(select id from OpportunityLineItems),Card__c  from Opportunity where Card__c IN : cardSet and recordType.developername = 'Subscription' and     (Success_Failure_Message__c !='customer.subscription.deleted' ) ];
        //List<OpportunityLineItem > lstOpp = [SELECT Id, Product2.name,Opportunity.Card__c  ,OpportunityId, Name FROM OpportunityLineItem where Opportunity.Card__c IN : cardSet  and  Opportunity.recordType.developername = 'Subscription'  and   (Opportunity.Success_Failure_Message__c !='customer.subscription.deleted' and Opportunity.Success_Failure_Message__c !='canceled')];
        List<Opportunity> lstOppUp = new List<Opportunity> ();
        for(Opportunity op : lstOpp ) {
            if(op.OpportunityLineItems.size() <= 0) {
            lstOppUp.add(op);
            }
        
        }
        return lstOppUp ;
    }
    //This method used to send the email to the customer
    @AuraEnabled
    public static string sendEmail(string CardId,string EmailId,string customerId,string Last4Digit) {
       List<card__c> cardList = [SELECT Id, Expiry_Year__c,Credit_Card_Number__c,Stripe_Profile__r.Customer__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email,Contact__r.phone, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where id =: CardId];
        if(cardList.size() > 0){
            String str = cardList[0].Credit_Card_Number__c;
            //List<Card__c> cardL = [select id,isCardUpdateNotified__c,Stripe_Profile__r.Customer__c  from card__c where id=: CardId];
            List<Contact> con = [Select Id, FirstName,email,phone FROM Contact WHERE Id=: cardList[0].Stripe_Profile__r.Customer__c ];
            if (con.size()>0) {
                


               //FromAddressSetting__c mc = FromAddressSetting__c.getOrgDefaults();
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                
                if(EmailId!='' && customerId!=''){
                //To track the email
                    Custom_Tracker__c cusTrack = [SELECT Name FROM Custom_Tracker__c WHERE UniqueId__c = 'dummyTracker' FOR UPDATE];
                        Datetime currentDateTime = Datetime.now();
                        cardList[0].UniqueEmailId__c = String.valueOf(currentDateTime.getTime());
                        update cardList;
                        update cusTrack;
                        Custom_Tracker__c ct = new Custom_Tracker__c();
                        ct.Card__c = cardList[0].id;
                        ct.Contact__c = con[0].Id;
                        ct.Email_Open_count__c = 0;
                        ct.Is_Email_Opened__c = false;
                        ct.Link_Click_Count__c = 0;
                        ct.Is_Clicked__c = false;
                        ct.UniqueId__c = String.valueOf(currentDateTime.getTime());
                        insert ct;
                    //Populate from address
                    OrgWideEmailAddress owea = [ select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
                    //Get the email template
                    EmailTemplate emailTemplate = [select Id,developerName, Subject, HtmlValue, Body from EmailTemplate where developerName = 'Update_Card_Email_Template' limit 1];
                    
                    Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
                    
                    
                    email.setUseSignature(false);
                    email.setTargetObjectId(con[0].id);
                    email.setWhatId(cardList[0].id);
                    //if(mc.sOrgWideEmail__c){
                      //  email.setOrgWideEmailAddressId(owea.Id);
                    //}
                    email.setSaveAsActivity(false);
                    email.setTemplateId(emailTemplate.Id);
                    try{
                        if(!test.isrunningTest()){
                        //Sends the email 
                            Messaging.sendEmail(new Messaging.SingleEmailmessage[] {email});
                        }

                        return 'Email sent to '+EmailId;
                    }catch(exception e){
                        return 'Email not sent!!';
                    }
                    
                    
                } 
            }
            
            
           
        }   
         return null;
        }   
        
    //Send SMS through twilio to selected card's customer
    @AuraEnabled
    
    public static string sendSMS(string CardId,string EmailId,string customerId,string Last4Digit,string phoneNum) {
        //System.debug('==phoneNum===='+phoneNum);
        //if(phoneNum!=null) {
        //Get the card details
           List<card__c> cardList = [SELECT Id, Expiry_Year__c,Credit_Card_Number__c,Stripe_Profile__r.Customer__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where id =: CardId];
        
        if( phoneNum!='' && phoneNum!=null && cardList.size() > 0){
        
            Integer randNum = Math.round(Math.random()*10000000);
            
            String str = '';
            string LAST4 ='';
            if(cardList[0].Credit_Card_Number__c !=null && cardList[0].Credit_Card_Number__c!=''){
                str  = String.valueOf(cardList[0].Credit_Card_Number__c);
            }
            //Get last 4 digits
            if(str!=null && str != '') {
             LAST4  = str.right(4);
            }
            //SOQL on the contact to get the phone
            Contact c = [Select Id, FirstName,email,Phone,OtherPhone FROM Contact WHERE Id=: cardList[0].Stripe_Profile__r.Customer__c ]; 
            //cardList[0].UniqueEmailId__c = String.valueOf(randNum); 
           System.debug('phoneNumber'+phoneNum);
           if(phoneNum!=null && phoneNum!=''){
               if(phoneNum.length()==10){
                   phoneNum='+1'+phoneNum;
               }
           }
           //SMS body formation
            String str1 = 'Hello '+c.firstName+',\n We noticed that your card might need some lovin\'. Please Click this https://ladyboss-support.secure.force.com/UpdateCardClone?id='+customerId+' to update your card ending in '+LAST4 +'.\n This update method is 100% safe and secure, backed by the highest industry standards for credit/debit card security.\nThank you!\nTeam LadyBoss'; 
            
            Map<String,Object> mapOutput = new  Map<String,Object>();
            //Query from setting
            List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain'];
            String accountSID = '';
            String authToken = '';
            String fromNumber ='';
            String url = '';
            if(lstTwilliConfigs.size() > 0 ){
                accountSID = lstTwilliConfigs.get(0).AccountSID__c;  //'AC44099911ff7ec99c40bc97943ad6c0ca';
                authToken = lstTwilliConfigs.get(0).AuthToken__c; //'6a386f07923a55ec0729eff283d747a3';
                fromNumber = lstTwilliConfigs.get(0).From_Number__c; //'15053226116';
                url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
            
                HttpRequest req = new HttpRequest();
                req.setEndpoint(url);
                //Post method
                req.setMethod('POST');
                req.setBody('To='+ EncodingUtil.urlEncode(phoneNum,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8'));
                Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
                String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
                //Set the header of the request
                req.setHeader('Authorization', authorizationHeader);
                Http http = new Http();
                TwillioResponse tl;
                HTTPResponse res ;
                if(!test.isRunningTest()){
                //Send SMS
                    res = http.send(req);
                     tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
                    if(tl.sid != null && !test.isRunningTest())  {
                    
                    
                    return 'SMS Send Successful';
                    
                }else  {
                    return 'SMS Send Failed';
                }
                }
                
               return null;
                
                
            }
        } 
        
        //}
        
        return 'SMS Not sent!!';
    }
    class TwillioResponse  {
        public String sid;  
        public String message;
    }
    
    
}