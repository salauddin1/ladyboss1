@isTest
public class ShipStationOrderRelated_Test {
    public static TestMethod void test(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        //shipOrder.orderStatus__c ='Draft';
        insert shipOrder;
        Test.startTest();
        ShipStationOrderRelated.getShipStationOrder(co.Id);
        Test.stopTest();
    }
    }