// Developer Name : Sourabh Badole 
// Date       : 10-05-2019
@isTest
public class CoachingTriggerHeplerTest {
    // Test Methods for code coverage.
    @isTest(SeeAllData=true)
    public static void TestPickVal1(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Monthly_Subscription_Payment_Amount__c = '$297 Monthly Payment';
        con.X297_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$297 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal2(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Monthly_Subscription_Payment_Amount__c = '$397 Monthly Payment';
        con.X397_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$397 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal3(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Monthly_Subscription_Payment_Amount__c = '$497 Monthly Payment';
        con.X497_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$497 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal4(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Monthly_Subscription_Payment_Amount__c = '$447 Monthly Payment';
        con.X447_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$447 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal5(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $297 Monthly Payment';
        con.Down_Payment_and_297_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $297 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal6(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $397 Monthly Payment';
        con.Down_Payment_and_397_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $397 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal7(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $497 Monthly Payment';
        con.Down_Payment_and_497_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $497 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal8(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $447 Monthly Payment';
        con.Down_Payment_and_447_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $447 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal9(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$297 Subscription with Final Payment';
        con.X297_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$297 Subscription with Final Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal10(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$397 Subscription with Final Payment';
        con.X397_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$397 Subscription with Final Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
    @isTest(SeeAllData=true)
    public static void TestPickVal11(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$497 Subscription with Final Payment';
        con.X497_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$497 Subscription with Final Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
	@isTest(SeeAllData=true)
    public static void TestPickVal12(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$447 Subscription with Final Payment';
        con.X447_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$447 Subscription with Final Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
    }
     
	@isTest(SeeAllData=true)
    public static void TestPickVal13(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '$2997';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal14(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '$3997';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal15(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '$4997';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal16(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '$4497';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal17(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '1 Year Ascension';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal18(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '9 Month Retention';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal19(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '6 Month Retention';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal20(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = '3 Month Retention';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    @isTest(SeeAllData=true)
    public static void TestPickVal21(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Full_Pay__c = 'Launch Conference';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
    }   
    
    @isTest(SeeAllData=true)
    public static void TestPickVal(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $447 Monthly Payment';
        con.Down_Payment_and_447_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $447 Monthly Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        
        con.Down_Payment_and_447_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M5C2;';
        
        update con;
    }
        
}