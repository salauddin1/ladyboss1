public class createAffiliateControllerClone {
    @AuraEnabled
    public static Contact ct{get;set;}
    @AuraEnabled
    public static Contact getContact(String conId){
        Contact ct = [SELECT id,Email,PAP_refid__c,PAPRPassword__c,PAPUserId__c FROM Contact WHERE id=:conId];
        if(ct.PAP_refid__c == null || ct.PAP_refid__c ==''){
            List<Contact> ctList = [SELECT id,Email,PAP_refid__c,PAPUserId__c,PAPRPassword__c,firstname,lastname FROM Contact WHERE PAP_refid__c != null AND email =: ct.Email];
            if(ctList.size()>0){
                ct.PAP_refid__c = ctList[0].pap_refid__c;
                if(!test.isRunningTest()){
                    update ct;
                }
            }
        }
        return ct;
    }
    @AuraEnabled
    public static String checkRefId(String conId){
        String newRefId='';
        ct = [SELECT id,Email,PAP_refid__c,firstname,lastname FROM Contact WHERE id=:conId];
        List<Contact> ctList = new List<Contact>();
        String firstName ='%'+ ct.FirstName+'%';
        
        ctList = [SELECT id,Email,PAP_refid__c,firstname,lastname FROM Contact WHERE PAP_refid__c != null AND PAP_refid__c Like :firstName];
        System.debug('ctList:-'+ctList);
        String str ='';
        integer count = 0;
        
        if(ctList.size()>0){
            Set<Integer> numberSet = new Set<Integer>();
            for(Contact cnt :ctList){
                String temp = '0';
                str = cnt.PAP_refid__c.toLowerCase();
                if(str.contains('lb10')){
                    str = str.remove('lb10');
                }
                for(integer i = 0; i < str.length(); i++){
                    String st = str.substring(i,i+1);
                    if(st.isNumeric()){
                        temp = temp+st;
                    }
                }
                System.debug('str:='+str);
                System.debug('temp:='+temp);
                numberSet.add(integer.valueOf(temp));
            }
            List<Integer> sortedList = new List <Integer>();
            sortedList.addAll(numberSet);
            sortedList.sort(); 
            integer maxNo = sortedList.get(sortedList.size()-1);
            newRefId = ct.FirstName + (maxNo + 1)+'LB10';
        }
        if(newRefId == null || newRefId == ''){
            newRefId = ct.FirstName+'1LB10';
        }
        return newRefId;
    }
    @future(callout=true)
    public static void createAutoAffiliate(String oppList){
        System.debug('Jsonn::-'+oppList);
        List<Opportunity> createAffiliateOppList = (List<Opportunity>) JSON.deserialize(oppList,List<Opportunity>.class);
        System.debug('createAffiliateOppList::::::-'+createAffiliateOppList);
        for(Opportunity opp:createAffiliateOppList){
            createAffiliate(opp.Contact__c);
        }
    }
    
    @AuraEnabled
    public static String createAffiliate(String conId){
        String newRefId = createAffiliateController.checkRefId(conId);
        Contact ct = [SELECT id,Email,PAP_refid__c,PAPRPassword__c,PAPUserId__c,FirstName,LastName FROM Contact WHERE id=:conId];
        System.debug('newRefId:-'+newRefId);
        pap_credentials__c  pap_url = [select id,name,pap_url__c,username__c,password__c from pap_credentials__c where Name='pap url' limit 1];
        Http http = new Http();
        HttpRequest ht = new HttpRequest();
        ht.setEndpoint(pap_url.pap_url__c);
        ht.setMethod('POST');
        ht.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Merchants_User_AffiliateForm","M":"add","isFromApi":"Y","fields":[["name","value","values","error"],["username","'+ct.Email+'",null,""],["firstname","'+ct.FirstName+'",null,""],["Status","D",null,""],["lastname","'+ct.LastName+'",null,""],["refid","'+newRefId+'",null,""],["data25","3FF",null,""],["data24","'+newRefId+'",null,""],["agreeWithTerms","Y",null,""]]}]}';
        ht.setBody(body);
        HttpResponse res = http.send(ht);
        System.debug('res : '+res.getBody());
        List<Object> resp = (List<Object>) Json.deserializeUntyped(res.getBody());
        String msg='';
        for(Object ob : resp){
            Map<String,Object> ob1 = (Map<String,Object>)ob;
            if((String)ob1.get('message')!=null){
                msg = (String)ob1.get('message');
            }
            System.debug('msg '+ob1.get('message'));
        }
        if(msg=='Affiliate was successfully added'){
            ct.PAP_refid__c = newRefId;
            for(Object ob : resp){
                Map<String,Object> ob1 = (Map<String,Object>)ob;
                if(ob1.get('fields')!=null){
                    List<Object> fieldList = (List<Object>)ob1.get('fields');
                    for(Object field:fieldList){
                        List<Object> fields = (List<Object>)field;
                        if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('userid')){
                            ct.PAPUserId__c = (String)fields.get(1);
                        }
                        if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('rpassword')){
                            ct.PAPRPassword__c = (String)fields.get(1);
                        }
                    }
                }    
            }
            
            if(!test.isRunningTest()){
                update ct;
            }
            
        }
        System.debug('msgs is : '+msg);
        return msg;
    }
    
    @AuraEnabled
    public static String updateAffiliate(String conId,string newrefId){
        Contact ct = [SELECT id,Email,FirstName,LastName,PAP_refid__c,PAPRPassword__c,PAPUserId__c FROM Contact WHERE id=:conId];
        pap_credentials__c pap_url = [select id,name,pap_url__c,username__c,password__c from pap_credentials__c where Name='pap url' limit 1];
        Http http = new Http();
        HttpRequest ht = new HttpRequest();
        ht.setEndpoint(pap_url.pap_url__c);
        ht.setMethod('GET');
        ht.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        system.debug('====ct=='+ct);
        System.debug('newrefId:-'+newrefId);
        String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Merchants_User_AffiliateForm","M":"Save","isFromApi":"Y","fields":[["name","value","values","error"],["username","'+ct.Email+'",null,""],["firstname","'+ct.FirstName+'",null,""],["status","A",null,""],["lastname","'+ct.LastName+'",null,""],["refid","'+newrefId+'",null,""],["Id","'+ct.PAPUserId__c+'"],["rpassword","'+ct.PAPRPassword__c+'"]]}]}';
        ht.setBody(body);
        HttpResponse res = http.send(ht);
        System.debug('res : '+res.getBody());
        List<Object> resp = (List<Object>) Json.deserializeUntyped(res.getBody());
        String msg='';
        for(Object ob : resp){
            Map<String,Object> ob1 = (Map<String,Object>)ob;
            if((String)ob1.get('message')!=null){
                msg = (String)ob1.get('message');
            }
            System.debug('msg '+ob1.get('message'));
        }
        if(msg=='Affiliate Saved'){
            ct.PAP_refid__c = newrefId;
            if(!test.isRunningTest()){
                update ct;
            }
        }
        System.debug('msgs is : '+msg);
        return msg;
    }   
}