@istest
public class ActiveSubscriptionLineItems_test {
    public static testMethod void test(){
       system.Test.setMock(HttpCalloutMock.class, new MockTest1());
         
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.ServiceDate = date.today();
        insert ol;
        //String dateVal = String.valueOf(date.today());
        test.startTest();
        ActiveSubscriptionLineItems.getOpportunityLineItem(co.Id);
       // ActiveSubscriptionLineItems.cancelSubscription(ol.Id);
        test.stopTest();         
    }
     public static TestMethod void testGetCallout(){
        
            system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.ServiceDate = date.today();
        insert ol;
        
       String dateVal = String.valueOf(date.today());
    Test.startTest();
         ActiveSubscriptionLineItems.updateSubscription(ol.Id,dateVal,true);
         //ActiveSubscriptionLineItems.updateOpportunitySubscription(opp.Id,dateVal);
          //ActiveSubscriptionLineItems.cancelSubscription(ol.Id);
        
         //ActiveSubscriptionLineItems.cancelOpportunitySubscription(opp.Id);
       Test.stopTest(); 
        
    }
   
     public static TestMethod void testGetCallout2(){
        
            system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.ServiceDate = date.today();
        insert ol;
        
       String dateVal = String.valueOf(date.today());
    Test.startTest();
        // ActiveSubscriptionLineItems.updateSubscription(ol.Id,dateVal,true);
         ActiveSubscriptionLineItems.updateOpportunitySubscription(opp.Id,dateVal);
         // ActiveSubscriptionLineItems.cancelSubscription(ol.Id);
        
         //ActiveSubscriptionLineItems.cancelOpportunitySubscription(opp.Id);
       Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout3(){
        
            system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.ServiceDate = date.today();
        insert ol;
        
       String dateVal = String.valueOf(date.today());
    Test.startTest();
        // ActiveSubscriptionLineItems.updateSubscription(ol.Id,dateVal,true);
         //ActiveSubscriptionLineItems.updateOpportunitySubscription(opp.Id,dateVal);
         // ActiveSubscriptionLineItems.cancelSubscription(ol.Id);
        
         ActiveSubscriptionLineItems.cancelOpportunitySubscription(opp.Id);
       Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout4(){
        
            system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.ServiceDate = date.today();
        insert ol;
        
       String dateVal = String.valueOf(date.today());
    Test.startTest();
    ActiveSubscriptionLineItems.cancelSubscriptionFuture(ol.id);
        // ActiveSubscriptionLineItems.updateSubscription(ol.Id,dateVal,true);
         //ActiveSubscriptionLineItems.updateOpportunitySubscription(opp.Id,dateVal);
         // ActiveSubscriptionLineItems.cancelSubscription(ol.Id);
        
        // ActiveSubscriptionLineItems.cancelOpportunitySubscription(opp.Id);
       Test.stopTest(); 
        
    }
}