@isTest
public class AutoPAPCreationOnClubPurchaseOLITest {
    public static testMethod void AutoPAPCreationOnClubPurchaseOLI(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        Contact ct = new Contact();
        ct.FirstName = 'test';
        ct.LastName = 'test';
        ct.Email = 'test@test.com';
        insert ct;
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',Contact__c=ct.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true,Is_Eligible_For_Affiliate_Creation__c=true,
                                   Commission_Enabled_Club_Product__c=true,
                                   Price__c=13);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
    }
    public static testMethod void AutoPAPCreationOnClubPurchaseOLI2(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.name = 'default';
        insert cc;
        ContactTriggerFlag.isContactBatchRunning = true;
        Contact ct = new Contact();
        ct.FirstName = 'test';
        ct.LastName = 'test';
        ct.Email = 'test@test.com';
        ct.PAP_refid__c='fdghdfh';
        insert ct;
        
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), 
                                          StageName='Closed Won',Contact__c=ct.Id,
                                         RecordtypeId = recordTypeId);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true,Is_Eligible_For_Affiliate_Creation__c=true,
                                   Commission_Enabled_Club_Product__c=true,Price__c = 12);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
    }
}