// Developer Name : Sourabh Badole 
// Date       : 25-07-2019
// Description : VF page Controler for show the remaining commission values of Commission Years.

public class LTVCoachingCommissionYear2Cont {
    
    public String commissions {get;set;}
    public String showMessege {get;set;}
    public String commRemainAmo {get;set;}
    public String commRemainMon {get;set;}
    public String commfinalAmo {get;set;}
    public Boolean commRemainMonCheck {get;set;}
    public String commDownPay {get;set;}
    public Boolean commDownPayCheck {get;set;}
    public Boolean showcheck {get;set;}
    public Boolean checkCommis {get;set;}
    Decimal commRemainAmount = 0 ;
    List<Coaching_Commissions__c > coachComm ;
    Set <String> commisSet;
    String conId ;
        
    public LTVCoachingCommissionYear2Cont (ApexPages.StandardController controller){
        coachComm = new List <Coaching_Commissions__c>();   
        commisSet = new Set<String>();
        
        showcheck = false ;
        commRemainMonCheck = false ;
        commRemainMon = '';
        commfinalAmo = '';
        commDownPayCheck = false ;
        commDownPay = '';
        showMessege = '';
        commissions ='';
        
        commisSet.add('M1C1');
        commisSet.add('M1C2');
        commisSet.add('M2C1');
        commisSet.add('M2C2');
        commisSet.add('M3C1');
        commisSet.add('M3C2');
        commisSet.add('M4C1');
        commisSet.add('M4C2');
        commisSet.add('M5C1');
        commisSet.add('M5C2');
        commisSet.add('M6C1');
        commisSet.add('M6C2');
        commisSet.add('M7C1');
        commisSet.add('M7C2');
        commisSet.add('M8C1');
        commisSet.add('M8C2');
        commisSet.add('M9C1');
        commisSet.add('M9C2');
        commisSet.add('M10C1');
        commisSet.add('M10C2');
        commisSet.add('M11C1');
        commisSet.add('M11C2');
        commisSet.add('M12C1');
        commisSet.add('M12C2');
        commisSet.add('Launch Conference');
        commisSet.add('3 Month Retention');
        commisSet.add('6 Month Retention');
        commisSet.add('9 Month Retention');
        commisSet.add('1 Year Ascension');
        checkCommis = false ;   
        conId = apexpages.currentpage().getparameters().get('id'); 
        
        
    }
    public void redirect1(){
        this.redirect('year 1');
    }
    public void redirect2(){
        this.redirect('year 2');
    }
    public void redirect3(){
    	this.redirect('year 3');
    }
    public void redirect4(){
        this.redirect('year 4');
    }
    
    public void redirect(String commYear){
        
        Contact con = new Contact();
        String coachParent = '';
        if( conId != null && conId != '' ){
            con = [Select id ,Final_Payment_Amount__c , Coaching_Payment_Option__c,Down_Payment_Amount__c ,Payment_Month_Remaining__c ,Total_Amount_Remaining__c from Contact where id =: conId];
            coachComm = [select id ,Commission_Type__c ,Contact__c ,Commission_Parent_Type__c  from   Coaching_Commissions__c where Contact__c =: conId AND Contact__c != null and Commission_Year__c =: commYear ];
        }
        if(coachComm != null  && !coachComm.isEmpty()){
            for(Coaching_Commissions__c coachingCom : coachComm ){
                if(coachingCom.Contact__c != null && coachingCom.Commission_Type__c != null && coachingCom.Commission_Parent_Type__c != null ){
                    if(commisSet.contains(coachingCom.Commission_Type__c)){
                        commisSet.remove(coachingCom.Commission_Type__c);
                        coachParent = coachingCom.Commission_Parent_Type__c;
                        checkCommis = true ;
                    }                
                }
            }
            
        }
        Integer remPayCount = 0;
        Integer monPayCount = 0;
        
        System.debug('  tststs '+con.Final_Payment_Amount__c);
        if(commisSet != null  && !commisSet.isEmpty()){
            for(String str : commisSet){
                if(str != null && str != ''){
                    commissions = commissions + str +' ' ; 
                    remPayCount = remPayCount + 1;
                }
            }
        }
        else{
            commissions = 'All payment collected';
            
        }
        if(con.Final_Payment_Amount__c != null){
            commfinalAmo  = 'Final Payment Amount : '+con.Final_Payment_Amount__c;
        }
        else{
            commfinalAmo  = 'Please fill the "Final Payment Amount"';
        }
        if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c != '' &&  con.Coaching_Payment_Option__c == 'Subscription with Final Payment'){
            if(con.Payment_Month_Remaining__c !=  null && con.Payment_Month_Remaining__c != 0  ){
                commRemainMonCheck =  true ;
                commRemainMon = 'Total Remaining Month : '+ con.Payment_Month_Remaining__c ;
            }
            else{
                commRemainMonCheck =  true ;
                commRemainMon = 'Please fill the "Payment Month Remaining"';
            }
            if( con.Total_Amount_Remaining__c != null ){
                if (remPayCount != 0 && con.Total_Amount_Remaining__c == 0){
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c +'   Please fill the "Total Amount Remaining"'  ;
                }
                else{
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c ;
                }
            }
            else{
                commRemainAmo = 'Please fill the "Total Amount Remaining"';
            }
            if(con.Down_Payment_Amount__c != null ){
                commDownPayCheck = true ;
                commDownPay = 'The Down Payment Amount : '+ con.Down_Payment_Amount__c ;
            }
            else{
                commDownPayCheck = true ;
                commDownPay = 'Please fill the "Down Payment Amount"';
            }
            if (con.Payment_Month_Remaining__c !=  null && remPayCount == 0 && con.Payment_Month_Remaining__c == 0){
                commRemainMonCheck =  false ;
            }
            
        }
        else if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c != '' &&  con.Coaching_Payment_Option__c == 'Monthly Subscription'){
            if( con.Total_Amount_Remaining__c != null ){
                if (remPayCount != 0 && con.Total_Amount_Remaining__c == 0){
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c +'   Please fill the "Total Amount Remaining"'  ;
                }
                else{
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c ;
                }
            }
            else{
                commRemainAmo = 'Please fill the "Total Amount Remaining"';
            }
            
        }
        else if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c != '' &&  con.Coaching_Payment_Option__c == 'Down Payment with Subscription'){
            if(con.Payment_Month_Remaining__c !=  null && con.Payment_Month_Remaining__c != 0  ){
                commRemainMonCheck =  true ;
                commRemainMon = 'Total Remaining Month : '+ con.Payment_Month_Remaining__c ;
            }
            else{
                commRemainMonCheck =  true ;
                commRemainMon = 'Please fill the "Payment Month Remaining"';
            }
            if( con.Total_Amount_Remaining__c != null ){
                if (remPayCount != 0 && con.Total_Amount_Remaining__c == 0){
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c +'   Please fill the "Total Amount Remaining"'  ;
                }
                else{
                    commRemainAmo = 'Total Remaining Amount : '+ con.Total_Amount_Remaining__c ;
                }
            }
            else{
                commRemainAmo = 'Please fill the "Total Amount Remaining"';
            }
            if(con.Down_Payment_Amount__c != null  ){
                commDownPayCheck = true ;
                commDownPay = 'The Down Payment Amount : '+ con.Down_Payment_Amount__c ;
            }
            else{
                commDownPayCheck = true ;
                commDownPay = 'Please fill the "Down Payment Amount"';
            }
            if (con.Payment_Month_Remaining__c !=  null && remPayCount == 0 && con.Payment_Month_Remaining__c == 0){
                commRemainMonCheck =  false ;
            }
        }
        
        if(checkCommis == false){
            showcheck =true ;
            showMessege = 'No payment collected yet. Please fill out the commission form.' ; 
        }        
    }
    
}