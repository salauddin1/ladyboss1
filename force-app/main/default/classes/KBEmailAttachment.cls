public class KBEmailAttachment {
    
    @AuraEnabled
    public static map<String,List<Knowledge__kav>>  getArticles(String searchKeyWord){
        map<string,List<Knowledge__kav>> articleWithCategoryMap = new map<string,List<Knowledge__kav>>();
        map<Id,string> categoryMap = new map<Id,string>();
        for(Knowledge__DataCategorySelection k :  [SELECT ParentId,DataCategoryName FROM Knowledge__DataCategorySelection]) {
            categoryMap.put(k.ParentId,k.DataCategoryName);
        }
        
        for(Knowledge__kav k :  [SELECT Id,KnowledgeArticleId,title,ArticleTotalViewCount FROM Knowledge__kav WHERE PublishStatus = 'online' AND Language = 'en_US' order by ArticleTotalViewCount desc]) {
            if(categoryMap.get(k.Id) == null){
                categoryMap.put(k.Id,'Others');
            }
            if(searchKeyWord != null && searchKeyWord != '' && searchKeyWord.length() > 1 && (k.Title.toLowerCase().contains(searchKeyWord.toLowerCase()) || categoryMap.get(k.Id).toLowerCase().contains(searchKeyWord.toLowerCase()) )){
                system.debug('---un if--'+categoryMap.get(k.Id));
                if(articleWithCategoryMap.keySet().contains(categoryMap.get(k.Id))){
                    articleWithCategoryMap.get(categoryMap.get(k.Id)).add(k);
                } else{
                    if(categoryMap.get(k.Id) != null){
                        articleWithCategoryMap.put(categoryMap.get(k.Id),new List<Knowledge__kav>());
                        articleWithCategoryMap.get(categoryMap.get(k.Id)).add(k);
                    }else{
                        articleWithCategoryMap.put('Others',new List<Knowledge__kav>());
                        articleWithCategoryMap.get('Others').add(k);
                    }
                    
                }
            }else if(searchKeyWord == null || searchKeyWord == '' || searchKeyWord.length() == 0){
                system.debug('---un else--');
                if(articleWithCategoryMap.keySet().contains(categoryMap.get(k.Id))){
                    articleWithCategoryMap.get(categoryMap.get(k.Id)).add(k);
                } else{
                    if(categoryMap.get(k.Id) != null){
                        articleWithCategoryMap.put(categoryMap.get(k.Id),new List<Knowledge__kav>());
                        articleWithCategoryMap.get(categoryMap.get(k.Id)).add(k);
                    }else{
                        articleWithCategoryMap.put('Others',new List<Knowledge__kav>());
                        articleWithCategoryMap.get('Others').add(k);
                    }
                    
                }
            }
        }
        return articleWithCategoryMap;
    }
    
    @AuraEnabled
    public static String  getArticlesTitle(String KID){
        System.debug('----kid---'+KID);
        if(KID != null && KID != ''){
            Knowledge__kav k =  [SELECT Id,Answer__c FROM Knowledge__kav WHERE Id =:KID];
            if(k.Answer__c != null){
                return k.Answer__c;
            }else{
                return null;
            }
        }else{
            return null;
        }
        
            
    }
}