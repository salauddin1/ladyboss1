global class ScheduleStripeIHCBatch implements Schedulable{
    global void execute(SchedulableContext SC) {
      StripeIntegrationHandlerChangesBatch b = new StripeIntegrationHandlerChangesBatch(); 
      database.executebatch(b,30);
   }
}