@isTest
public class OpportunityWeekTriggerTest {
	@isTest
    public static void test1(){
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Stripe_Charge_Id__c = 'ch_123213';
        opp.Name = 'TestOpp';
        opp.Amount = 56;
        opp.AccountId = acc.id;
        opp.Refunded_Date__c = System.today();
        opp.StageName = 'Closed Won';
        opp.CloseDate = System.today();
        insert opp;
        OpportunityWeekTriggerFlag.runOnce=true;
        opp.Scheduled_Payment_Date__c = System.today();
        //OpportunityTriggerHandler.OpportunityWeekHandler(new List<Opportunity>{opp});
        update opp;
        //OpportunityTriggerHandler.OpportunityWeekHandler(new List<Opportunity>{opp});
        
    }
    @isTest
    public static void test2(){
        Opportunity opp = new Opportunity();
        opp.Stripe_Charge_Id__c = 'ch_123213';
        opp.Name = 'TestOpp';
        opp.Amount = 56;
        opp.Scheduled_Payment_Date__c = System.today();
        opp.Refunded_Date__c = System.today();
        opp.StageName = 'Closed Won';
        opp.CloseDate = System.today();
        insert opp;        
    }
}