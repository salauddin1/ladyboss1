global class CustomerStripeBatch implements Database.Batchable<Account>, Database.AllowsCallouts, Database.Stateful{
    
    public String startingAfter {get;set;}
    public Boolean hasMore {get;set;}
    public Map<String,Account> mapAcc {get;set;}
    
    public CustomerStripeBatch()
    {
        system.debug('###construct CustomerStripeBatch');
        startingAfter = '';
        hasMore = false;
    }
    
    public CustomerStripeBatch(String stAfter)
    {
        system.debug('###construct CustomerStripeBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        startingAfter = stAfter;
        hasMore = false;
    }
    
	public List<Account> start(Database.BatchableContext BC ){
        system.debug('###start CustomerStripeBatch');
		List<Account> lstAccount = new List<Account>();
        mapAcc = new Map<String,Account>();
        HttpResponse res = new HttpResponse();
        if(startingAfter!=null && startingAfter!=''){
        	res = StripeConnection.HttpRequest('customers','?limit=50&starting_after='+startingAfter);    
        }else{
            res = StripeConnection.HttpRequest('customers','?limit=50');
        }
        
        if(res.getStatusCode()==200){
            StripeListCustomer.Customers varCustomers;
            JSONParser parse;
            parse = JSON.createParser(res.getBody().replaceAll('object','object_data'));
            varCustomers = (StripeListCustomer.Customers)parse.readValueAs(StripeListCustomer.Customers.class);
            Account acc;
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern MyPattern;
            Matcher MyMatcher;
            for(StripeListCustomer.Data cust : varCustomers.data){
                //system.debug('###cust: '+cust);
                //system.debug('###cust.metadata: '+cust.metadata);
                acc = new Account();
                acc.External_ID__c = cust.id;
                acc.Default_Source__c = cust.default_source;
                if(cust.metadata!=null && cust.metadata.name!=null){
                    acc.Name = cust.metadata.name;
                    acc.First_Name__c = cust.metadata.first_name;
                    acc.Last_Name__c = cust.metadata.last_name;
                    acc.Phone = cust.metadata.phone;
                }else{
                    if(cust.email!=null && cust.email!=''){
                        acc.Name = cust.email;    
                    }else{
                        acc.Name = cust.id;    
                    }
                }
                //Validate Email
                if(cust.email!=null && cust.email!=''){
                    MyPattern = Pattern.compile(emailRegex);
                    MyMatcher = MyPattern.matcher(cust.email);
                    if (MyMatcher.matches()){
                        if(acc.Email__c==null) acc.Email__c = cust.email;
                    }
                }
                if(acc.Type==null) acc.Type = cust.object_data;
                acc.Description = cust.description;
                if(cust.shipping!=null){
                    if(cust.shipping.address!=null){
                        if(acc.ShippingCity==null) acc.ShippingCity = cust.shipping.address.city;
                        if(acc.ShippingCountry==null) acc.ShippingCountry = cust.shipping.address.country;
                        if(acc.ShippingPostalCode==null) acc.ShippingPostalCode = cust.shipping.address.postal_code;
                        if(acc.ShippingState==null) acc.ShippingState = cust.shipping.address.state;
                        if(acc.ShippingStreet==null) acc.ShippingStreet = cust.shipping.address.line1;
                    }
                }
                
                //system.debug('###cust.sources: '+cust.sources);
                if(cust.sources!=null){
                    for(StripeListCustomer.DataSource source : cust.sources.data){
                        system.debug('###cust.sources: '+source.id);
                        if(source.id.substring(0,3)=='src'){
                            acc.Source_ID__c = source.id;
                        }else{
                            acc.Card_ID__c = source.id;
                            acc.BillingCity = source.address_city;
                            acc.BillingCountry = source.address_country;
                            acc.BillingStreet = source.address_line1;
                            acc.BillingState = source.address_state;
                            acc.BillingPostalCode = source.address_zip;
                            acc.Brand__c = source.brand;
                            acc.Country__c = source.country;
                            acc.Cvc_Check__c = source.cvc_check;
                            acc.Exp_Month__c = String.valueOf(source.exp_month);
                            acc.Exp_Year__c = source.exp_year;
                            acc.Last4__c = source.last4;
                            break;
                        }
                    }
                }
                
                startingAfter = acc.External_ID__c;
                //acc.Check__c = true;
                lstAccount.add(acc);
                mapAcc.put(acc.External_ID__c.trim().toLowerCase(),acc);
            }
            system.debug('###has_more: '+ varCustomers.has_more);
            hasMore = varCustomers.has_more;
        }
        
        return lstAccount;
	}
	
	public void execute(Database.BatchableContext BC, List<Account> scope) {
        system.debug('###execute CustomerStripeBatch');
        if(scope.size()>0){
            //EDIT EXIST
            /*Map<String,String> mapAccExist = new Map<String,String>();
            for(Account a : [Select Id,External_ID__c From Account WHERE External_ID__c IN: mapAcc.keySet()]){
                mapAccExist.put(a.External_ID__c.trim().toLowerCase(),a.Id);
            }
            
            for(String key : mapAcc.keySet()){
                if(mapAccExist.get(key.trim().toLowerCase())==null){
                    mapAcc.remove(key);
                }
            }*/
            
            ApexUtil.isTriggerInvoked = true;
            upsert mapAcc.values() External_ID__c;
            ApexUtil.isTriggerInvoked = false;
            
            List<Contact> lstContact = new List<Contact>();
            Contact c;
            Account acc;
            for(String key : mapAcc.keySet()){
                acc = mapAcc.get(key);
                c = new Contact();
                c.External_ID__c = acc.External_ID__c;
                if(acc.Last_Name__c!=null && acc.Last_Name__c.trim()!=''){
                    c.Firstname = acc.First_Name__c;
                    c.LastName = acc.Last_Name__c;
                }else{
                    if(acc.First_Name__c!=null && acc.First_Name__c.trim()!=''){
                        c.LastName = acc.First_Name__c;
                    }else{
                        c.LastName = acc.Name;
                    }
                }
                c.Description = acc.Description;
                if(c.Email==null) c.Email = acc.Email__c;
                if(c.MailingCity==null) c.MailingCity = acc.BillingCity;
                if(c.MailingCountry==null) c.MailingCountry = acc.BillingCountry;
                if(c.MailingPostalCode==null) c.MailingPostalCode = acc.BillingPostalCode;
                if(c.MailingState==null) c.MailingState = acc.BillingState;
                if(c.MailingStreet==null) c.MailingStreet = acc.BillingStreet;
                c.OtherCity = acc.ShippingCity;
                c.OtherCountry = acc.ShippingCountry;
                c.OtherPostalCode = acc.ShippingPostalCode;
                c.OtherState = acc.ShippingState;
                c.OtherStreet = acc.ShippingStreet;
                c.AccountId = acc.Id;
                c.Fax = acc.Fax;
                if(c.Phone==null) c.Phone = acc.Phone;
                lstContact.add(c);
            }
            
            ApexUtil.isTriggerInvoked = true;
            if(lstContact.size()>0) upsert lstContact External_ID__c;
            ApexUtil.isTriggerInvoked = false;
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish CustomerStripeBatch');
        system.debug('###finish hasMore: '+hasMore);
        system.debug('###finish startingAfter: '+startingAfter);
        if(!Test.isRunningTest()){
            if(hasMore){ Database.executeBatch(new CustomerStripeBatch(startingAfter)); }
        }
    }

}