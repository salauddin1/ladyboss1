public class FreeBonusTaskCreate {
    public static Boolean ownerFlag = false;
    public static void createFreeRetentionTask(List<opportunity> oppList) {
        
        List<User> userList = new List<User>();
        List<String> bonusProdVal = new List<String>();
        List<Task> tskList = new List<Task>();
        for(Opportunity opp : oppList) {
            System.debug('-----opp.Contact_Owner__c---'+opp.Contact_Owner__c);
            System.debug('----opp.Owner.Name---'+opp.Contact_and_Opportunity_Owner__c);
            if(opp.Contact_and_Opportunity_Owner__c != null && opp.Contact_and_Opportunity_Owner__c != '' && (opp.Contact_and_Opportunity_Owner__c == 'Stripe Site Guest User'|| opp.Contact_and_Opportunity_Owner__c == 'LadyBossHub Site Guest User' || opp.Contact_and_Opportunity_Owner__c == 'TestCommunity Site Guest User')) {
                ownerFlag = true;
            }
        }
        if(ownerFlag == true) {
            userList = [Select id,Name from User where Name = 'Grant Christopher'];
            System.debug('----userList---'+userList);
        }
        for(Opportunity opp : oppList) {
            if(opp.Selected_Free_Bonus_Products__c != null && opp.Selected_Free_Bonus_Products__c != '') {
               bonusProdVal  = opp.Selected_Free_Bonus_Products__c.split(',');
                System.debug('---bonusProdVal-----'+bonusProdVal);
                if(!bonusProdVal.isEmpty()) {
                for(String prodVal : bonusProdVal) {
                    if(prodVal != null && prodVal != '' && opp.Success_Failure_Message__c != '' && opp.Success_Failure_Message__c != null && opp.Success_Failure_Message__c == 'charge.succeeded' && (prodVal == 'Free REST' || prodval == 'Free BURN')) {
                        System.debug('--prodVal----'+prodVal+'---opp.Success_Failure_Message__c-----'+opp.Success_Failure_Message__c);
                        
                        Task tsk = new Task();
                        if(prodVal == 'Free REST') {
                            tsk.Subject = 'Free REST Bonus Retention Team Product';
                        }
                        else if(prodVal == 'Free BURN') {
                            tsk.Subject = 'Free BURN Bonus Retention Team Product';
                        }
                        tsk.Status = 'Open';
                        tsk.Priority = 'Normal';
                        tsk.WhoId = opp.Contact__c;
                        tsk.Shipping_Address__c = opp.Shipping_Address__c;
                        if((opp.Contact_and_Opportunity_Owner__c == 'Stripe Site Guest User' || opp.Contact_and_Opportunity_Owner__c == 'LadyBossHub Site Guest User' || opp.Contact_and_Opportunity_Owner__c == 'TestCommunity Site Guest User') && userList != null && !userList.isEmpty()) {
                            tsk.Assigned_By__c = userList[0].Name ;
                            tsk.OwnerId = userList[0].Id;
                        }
                        else if (opp.Contact_and_Opportunity_Owner__c != null && opp.Contact_and_Opportunity_Owner__c != '' && opp.Contact_Owner__c != null && opp.Contact_Owner__c != ''){
                            tsk.Assigned_By__c = opp.Contact_and_Opportunity_Owner__c ;
                          tsk.OwnerId = opp.Contact_Owner__c;
                        }
                        tsk.Contact__c = opp.Contact__c;
                        tskList.add(tsk);
                    }
                }
                }
            }
        }
        if(tskList != null && !tskList.isEmpty()) {
            insert tskList;
            System.debug('-----inserted successfully tskList------'+tskList);
        }
    }
}