public class ManageCouponButtonController {
    @AuraEnabled
    public static Contact getContact(String conId){ 
        if(conId != null  && conId != ''){
            Contact ct = [SELECT id, Email, Coupon_Discount_Type__c, Coupon_Amount__c, Coupon_Code_Id__c,pap_refid__c FROM Contact WHERE id=:conId];
            return ct;
        } 
        return null;
    }
    @AuraEnabled
    public static String updateCouponCode(String conId, String coupCodeId, String coupCode) {
        if(conId != null && coupCode != null && coupCodeId != null){
            if( coupCode.contains(' ') ){
                coupCode = coupCode.remove(' ') ;
            }
            List<Contact> conList = new List<Contact>();
            conList = [select id,Coupon_Code_Id__c, Coupon_Discount_Type__c, Coupon_Amount__c, pap_refid__c from contact where id =: conId];
            if(conList.size() > 0){
                conList[0].pap_refid__c = coupCode;
                System.debug('');
                update conList;
                return 'Coupon Successfully Updated';
            }
        }
        return null ;
    }
    @AuraEnabled
    public static String createCouponCode(String conId, String coupCode) {
        if(conId != null && coupCode != null){
            if( coupCode.contains(' ') ){
                coupCode = coupCode.remove(' ') ;
            }
            List<Contact> conList1 = new List<Contact>();
            conList1 = [select id, Coupon_Discount_Type__c, Coupon_Amount__c, Coupon_Code_Id__c, pap_refid__c from contact where id =: conId and  pap_refid__c != null];
            if(conList1.size() > 0){
                if(conList1[0].Coupon_Code_Id__c == null){
                    Boolean individual_use = true;
                    Boolean exclude_sale_items = true;
                    String jsonBody = '';
                    
                    Create_Coupon__c cc = [select id, Amount__c, Discount_Type__c, Product_Categories__c from Create_Coupon__c where name = 'default'];
                    String coupAmount = String.valueOf(cc.Amount__c);
                    String coupDiscountType = cc.Discount_Type__c;
                    String prodCatbody = '';
                    if(cc.Product_Categories__c != null ){
                        if(cc.Product_Categories__c.contains(',') ){
                            List<Integer> pcIdList = new List<Integer>();
                            List<String> pcList = cc.Product_Categories__c.split(',');
                            for(String pcval : pcList){
                                pcIdList.add( Integer.valueOf(pcval) );
                            }
                            if(pcIdList.size() > 0){
                                prodCatbody =JSON.serialize(pcIdList);
                            }
                        }
                        else{
                            prodCatbody =JSON.serialize(new List<Integer>{Integer.valueOf( cc.Product_Categories__c)});
                        }
                    }
                    //String enp = 'https://ladyboss.com/PLAYGROUNDtestonlywfioevbou4y3g847rg7ryifvow8eyvfq97foufbq3874qvf7yfvi/wp-json/wc/v3/coupons?consumer_key=ck_59ade5ae29888b0f779ab12ebcefe38aab63367b&consumer_secret=cs_5a498e1a3aa905b1a857697f2c5f42e5e2740ac4';
                    jsonBody = '{ "code": "'+coupCode+'", "amount": "'+coupAmount+'", "discount_type": "'+coupDiscountType+'", "individual_use": '+individual_use+', "exclude_sale_items": '+exclude_sale_items+',"product_categories":'+prodCatbody+' }';
                    System.debug('jsonBody:-'+jsonBody);
                    List<Create_Site_Coupon__c> siteCouponList = [select id,Name,Create_Coupon_Site_URL__c from Create_Site_Coupon__c];
                    HttpResponse response=null;
                    for(Create_Site_Coupon__c site: siteCouponList){
                        System.debug('jsonBody:-'+jsonBody);
                        Http http = new Http();
                        HttpRequest request = new HttpRequest();
                        request.setEndpoint(site.Create_Coupon_Site_URL__c);
                        request.setMethod('POST');
                        request.setBody(jsonBody);
                        request.setHeader('Content-Type', 'application/json');
                        
                        if(site.Name == 'Store'){
                            response = http.send(request);
                        }
                        else{
                            HttpResponse response2 = http.send(request);
                        }
                    }
                    if(response != null){
                        if (response.getStatusCode() == 200 || response.getStatusCode() == 201 ) {
                            if(response.getBody() != null && response.getBody() != ''){
                                String body = response.getBody();
                                System.debug('Body1  :-'+Body);
                                Map<String,Object> getCodeMap = (Map<String,Object>)Json.deserializeUntyped(body);
                                if(getCodeMap != null && getCodeMap.keySet().size() > 0){
                                    if(getCodeMap.keySet().contains('code') && getCodeMap.keySet().contains('id')){
                                        if(getCodeMap.get('code') != null && getCodeMap.get('id') != null){
                                            List<Contact> conList = new List<Contact>();
                                            conList = [select id, Coupon_Discount_Type__c, Coupon_Amount__c, Coupon_Code_Id__c, pap_refid__c from contact where id =: conId and  pap_refid__c != null];
                                            if(conList.size() > 0){
                                                
                                                conList[0].Coupon_Code_Id__c = String.valueOf(getCodeMap.get('id'));
                                                conList[0].pap_refid__c = coupCode ;
                                                conList[0].Coupon_Amount__c = Decimal.valueOf(coupAmount);
                                                conList[0].Coupon_Discount_Type__c = coupDiscountType;
                                                update conList;
                                                return 'Coupon Successfully Created';
                                            }
                                        }
                                    }    
                                }
                            }
                        }
                    }
                }
            }   
        }
        return null ;
        
    }
    
}