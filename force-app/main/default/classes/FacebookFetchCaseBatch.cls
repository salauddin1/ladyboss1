global class FacebookFetchCaseBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable{
    
    global FacebookFetchCaseBatch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT id,Page_Id__c,Last_Fetch_Time__c,Page_Name__c,Page_Token__c FROM FacebookPage__c WHERE isActive__c=true');
    }

    global void execute(SchedulableContext sc) {
        FacebookFetchCaseBatch facebookFetchBatch = new FacebookFetchCaseBatch();
        database.executebatch(facebookFetchBatch,1);
    }

    global void execute(Database.BatchableContext BC, List<FacebookPage__c> scope) {
        Map<String,String> paramMap = null;
        //List<FacebookWrappers.ConversationWrapper> conversationWrapperList = new List<FacebookWrappers.ConversationWrapper>();
        List<Case> caseList = new List<Case>();
        //Case caseObject = null;
        Map<String,FacebookWrappers.ConversationObject> conversationMap = new Map<String,FacebookWrappers.ConversationObject>();
        Map<String,FacebookPage__c> conversationPageMap = new Map<String,FacebookPage__c>();
        List<Contact> contactToUpsertList = new List<Contact>();
        for(FacebookPage__c fb : scope){
            paramMap = new Map<String,String>();
            paramMap.put('access_token',fb.Page_Token__c);
            paramMap.put('fields','unread_count,updated_time,senders');
            paramMap.put('limit','5000');
            List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
            String response = FaceBookMessagesAPI.callAPI('/'+fb.Page_Id__c+'/conversations','GET',paramList);
            System.debug(response);
            FacebookWrappers.ConversationWrapper conversationWrapper = (FacebookWrappers.ConversationWrapper)JSON.deserialize(response,FacebookWrappers.ConversationWrapper.class);
            //conversationWrapperList.add(conversationWrapper);
            for(FacebookWrappers.ConversationObject conversation : conversationWrapper.data ){
                if(fb.Last_Fetch_Time__c.getTime() < DateTime.valueOf(conversation.updated_time.replace('T',' ').substring(0,conversation.updated_time.indexOf('+'))).getTime()){
                    conversationMap.put(conversation.id,conversation);
                    conversationPageMap.put(conversation.id,fb);
                    Contact contactObject = new Contact();
                    contactObject.LastName = conversation.senders.data[0].name;
                    contactObject.Fb_User_Id__c = conversation.senders.data[0].id;
                    contactToUpsertList.add(contactObject);    
                } 
            }
        }  
        List<Case> caseFromDBList = [SELECT id,ClosedDate,Conversation_Id__c,status,Conversation_UpdateTime__c FROM CASE WHERE Conversation_Id__c IN :conversationMap.keySet()];
        Map<String,List<Case>> caseFromDbMap = new Map<String,List<Case>>();
        Map<Id,Case> idCaseMap = new Map<Id,Case>();
        for(Case caseObject : caseFromDBList){
            if(!caseFromDbMap.containsKey(caseObject.Conversation_Id__c)){
                caseFromDbMap.put(caseObject.Conversation_Id__c,new List<Case>());
            }
            caseFromDbMap.get(caseObject.Conversation_Id__c).add(caseObject);
            idCaseMap.put(caseObject.id,caseObject);
        }
        Datetime currentDateTime = Datetime.now().addDays(-7);
        Map<String,Case> newConversationIdCaseMap = new Map<String,Case>();
        Map<String,Case> conversationsWithCasemap = new Map<String,Case>();
        List<Case> caseToUpdate = new List<Case>();
        for(String conversationId : conversationMap.keySet()){
            //if(conversationMap.get(conversationId).unread_count > 0){
                Boolean isOpen = false;
                Boolean isReOpen = false;
                Case openId = null;
                Case reOpenId = null;
                if(caseFromDbMap.containsKey(conversationId)){
                    List<Case> saperateCaseList = caseFromDbMap.get(conversationId);
                    for(Case caseObject : saperateCaseList){
                        if(caseObject.status != 'closed'){
                            isOpen = true;
                            openId = caseObject;
                        }else if(caseObject.ClosedDate>=currentDateTime){
                            reOpenId = caseObject;
                            isReOpen = true;
                        }
                    }
                }

                if(!isOpen && !isReOpen){
                    Case caseObj = new Case();
                    caseObj.Conversation_Id__c = conversationId;
                    caseObj.Conversation_UpdateTime__c = DateTime.valueOf(conversationMap.get(conversationId).updated_time.replace('T',' ').substring(0,conversationMap.get(conversationId).updated_time.indexOf('+')));
                    caseObj.status = 'open';
                    String fromStr = '';
                    for(FacebookWrappers.ReciepientDetails recepient : conversationMap.get(conversationId).senders.data){
                        if(recepient.id != conversationPageMap.get(conversationId).Page_Id__c){
                            fromStr+=recepient.name+',';
                        }
                    }
                    if(fromStr.length()>1){
                        fromStr = fromStr.substring(0,fromStr.length()-1);
                    }
                    caseObj.subject = 'Inbox Message from: '+fromStr+' to: '+conversationPageMap.get(conversationId).Page_Name__c;
                    caseObj.origin = 'facebook';
                    caseObj.contact = new Contact(Fb_User_Id__c=conversationMap.get(conversationId).senders.data.get(0).id);
                    caseObj.FB_Page__c = conversationPageMap.get(conversationId).id;
                    newConversationIdCaseMap.put(conversationId,caseObj);
                }else if(isOpen){
                    conversationsWithCasemap.put(conversationId,openid);
                }else if(isReOpen){
                    conversationsWithCasemap.put(conversationId,reOpenid);
                    idCaseMap.get(reOpenid.id).status = 'reopened';
                    caseToUpdate.add(idCaseMap.get(reOpenid.id));
                }
            //}
        }
        Map<String,FacebookWrappers.MessageWrapper> newCaseMessageListMap = getMessages(newConversationIdCaseMap,conversationPageMap);
        Map<String,FacebookWrappers.MessageWrapper> oldCaseMessageListMap = getMessages(conversationsWithCasemap,conversationPageMap);
        
        System.debug('cases : '+conversationsWithCasemap);
        if(caseToUpdate.size()>0){
            System.debug('update');
            update caseToUpdate;
        }
        
        if(contactToUpsertList.size()>0){
            upsert contactToUpsertList Fb_User_Id__c;
        }

        if(newConversationIdCaseMap.values().size()>0){
            System.debug('insert');
            insert newConversationIdCaseMap.values();
        }
        List<ConversationAndMessage__c> convMessageList = [SELECT id,Conversation_Id__c,Message_Id__c FROM ConversationAndMessage__c WHERE Conversation_Id__c IN :conversationMap.keySet()];
        Set<String> messageIdList = new Set<String>();
        for(ConversationAndMessage__c convMessage : convMessageList){
            messageIdList.add(convMessage.Message_Id__c);
        }
        List<ConversationAndMessage__c> convMessageToInsertList = new List<ConversationAndMessage__c>();
        List<CaseComment> caseCommentToInsertList = new List<CaseComment>();
        for(String conversationId : newCaseMessageListMap.keyset()){
            List<FacebookWrappers.MessageObject> messageData = newCaseMessageListMap.get(conversationId).data;
            for(FacebookWrappers.MessageObject message : messageData){
                if(!messageIdList.contains(message.id)){
                    CaseComment caseCommentObject = new CaseComment();
                    caseCommentObject.ParentId = newConversationIdCaseMap.get(conversationId).id;
                    caseCommentObject.CommentBody = message.fromData.name +' ('+message.created_time.replace('T',' ').substring(0,message.created_time.indexOf('+')) +') : '+ message.message;
                    if(message.attachments!=null && message.attachments.data.size()>0){
                        caseCommentObject.CommentBody+=' '+message.attachments.data.get(0).image_data.url;
                    }
                    caseCommentToInsertList.add(caseCommentObject);
                    ConversationAndMessage__c conv = new ConversationAndMessage__c();
                    conv.Conversation_Id__c = conversationId;
                    conv.Message_Id__c = message.id;
                    convMessageToInsertList.add(conv);
                }
            }
        }

        for(String conversationId : oldCaseMessageListMap.keyset()){
            List<FacebookWrappers.MessageObject> messageData = oldCaseMessageListMap.get(conversationId).data;
            for(FacebookWrappers.MessageObject message : messageData){
                if(!messageIdList.contains(message.id)){
                    CaseComment caseCommentObject = new CaseComment();
                    caseCommentObject.ParentId = conversationsWithCasemap.get(conversationId).id;
                    caseCommentObject.CommentBody = message.fromData.name +' ('+message.created_time.replace('T',' ').substring(0,message.created_time.indexOf('+')) +') : '+ message.message;
                    if(message.attachments!=null && message.attachments.data.size()>0){
                        caseCommentObject.CommentBody+=' '+message.attachments.data.get(0).image_data.url;
                    }
                    caseCommentToInsertList.add(caseCommentObject);
                    ConversationAndMessage__c conv = new ConversationAndMessage__c();
                    conv.Conversation_Id__c = conversationId;
                    conv.Message_Id__c = message.id;
                    convMessageToInsertList.add(conv);
                }
            }
        }

        if(caseCommentToInsertList.size()>0){
            CaseCommmentStaticFlagClass.flag = false;
            insert convMessageToInsertList;
            insert caseCommentToInsertList;
        }

        for(FacebookPage__c fb : scope){
            fb.Last_Fetch_Time__c  = Datetime.now();
        }

        update scope;
        //upsert caseList Conversation_Id__c;

    }

    public Map<String,FacebookWrappers.MessageWrapper> getMessages(Map<String,Case> newConversationIdCaseMap,Map<String,FacebookPage__c> conversationPageMap){
        Map<String,FacebookWrappers.MessageWrapper> messageListMap = new Map<String,FacebookWrappers.MessageWrapper>();
        Map<String,String> paramMap = null;
        for(String conversationId : newConversationIdCaseMap.keySet()){
            paramMap = new Map<String,String>();
            paramMap.put('access_token',conversationPageMap.get(conversationId).Page_Token__c);
            paramMap.put('fields','message,from,to,created_time,attachments');
            paramMap.put('limit','5000');
            paramMap.put('order','chronological');
            List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
            String response = FaceBookMessagesAPI.callAPI('/'+conversationId+'/messages','GET',paramList);
            System.debug(response.replaceAll('"from"','"fromData"'));
            FacebookWrappers.MessageWrapper messageWrapper = (FacebookWrappers.MessageWrapper)JSON.deserialize(response.replaceAll('"from"','"fromData"'),FacebookWrappers.MessageWrapper.class);           
            messageListMap.put(conversationid,messageWrapper);
        }
        return messageListMap;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}