public class PapCommissionTriggerHandler {
    public static void papTrigHandler(List< Pap_Commission__c > lstPap){
        try{
            Set<Id> cntIds = new Set<Id>();
            Set<Id> papIdSet = new Set<Id>();
            Set<String> papOrderIdSet = new Set<String>();
            Set<String> papProductIdSet = new Set<String>();
            Map<id,String> papOrdIDMap = new map<Id,String>();
            Map<id,String> papProductIDMap = new map<Id,String>();
            Map<String,Decimal> refIdComAMap = new Map<String,Decimal>();
            for(Pap_Commission__c pc : lstPap){
                
                if(pc.Contact__c!=null){
                    papIdSet.add(pc.id);
                    cntIds.add(pc.Contact__c);
                    papOrderIdSet.add(pc.data5__c);
                    papProductIdSet.add(pc.product_id__c);
                    papOrdIDMap.put(pc.id,pc.data5__c);
                    papProductIDMap.put(pc.id,pc.product_id__c);
                }
            }
            
            List<Opportunity> oppList = new List<Opportunity>();
            if(papOrderIdSet != null && papOrderIdSet.size() > 0 && papProductIdSet != null && papProductIdSet.size() > 0){
                oppList = [Select id,Name,WC_Product_Id__c,WC_Order_Number__c from Opportunity where (WC_Order_Number__c != null AND WC_Order_Number__c != '' AND  WC_Order_Number__c In : papOrderIdSet  ) OR ( WC_Product_Id__c != null AND WC_Product_Id__c != '' ANd WC_Product_Id__c In : papProductIdSet)];
                System.debug('----oppList--'+oppList);
            }
            List<Pap_Commission__c> papList = new List<Pap_Commission__c>();
            if(papIdSet != null && !papIdSet.isEmpty()){
                papList = [Select id,order_id__c,product_id__c from Pap_Commission__c where Id =: papIdSet];
                System.debug('----papList----'+papList);
            }
            if(papList != null && !papList.isEmpty() && oppList != null && !oppList.isEmpty()) {
                for(Pap_Commission__c papCom : papList) {
                    for(Opportunity op : oppList) {
                        if(papOrdIDMap.get(papCom.id) != null && papOrdIDMap.get(papCom.id) != '' && op.WC_Order_Number__c != null && op.WC_Order_Number__c != '' && papProductIDMap.get(papCom.Id) != null && papProductIDMap.get(papCom.Id) != '' && op.WC_Product_Id__c != null && op.WC_Product_Id__c != ''){
                            if(papOrdIDMap.get(papCom.id) == op.WC_Order_Number__c && papProductIDMap.get(papCom.Id) == op.WC_Product_Id__c){
                                papCom.Opportunity__c = op.Id;
                            }
                        }
                    }
                }
                update papList;
                System.debug('---papList---'+papList);
            }
            
            Set<String> emailList = new Set<String>();
            Map<String,String> refIdEmailMap = new Map<String,String>();
            List<Contact> cntList = new List<Contact>();
            if(cntIds != null && cntIds.size() >0){
                
                cntList = [SELECT id,email,PAP_refid__c FROM Contact WHERE id in : cntIds];
            }
            if(cntList != null && !cntList.isEmpty()){
                for(Contact ct : cntList){
                    emailList.add(ct.email);
                    refIdEmailMap.put(ct.PAP_refid__c,ct.Email);
                }
            }
            if(emailList.size()>0){
                Map<String,String> emailCustMap = new Map<String,String>();
                List<OpportunityLineItem> oliList = [SELECT id,Opportunity.CustomerID__c,Status__c,Opportunity.Contact__r.email FROM OpportunityLineItem WHERE Opportunity.Contact__r.email in : emailList AND Subscription_Id__c!=null and Status__c='active' and Opportunity.CustomerID__c!=null ORDER by End__c asc];
                for(OpportunityLineItem ol : oliList){
                    if(emailCustMap.get(ol.Opportunity.Contact__r.email)==null){
                        emailCustMap.put(ol.Opportunity.Contact__r.email,ol.Opportunity.CustomerID__c);
                    }
                }
                for(Pap_Commission__c pc : lstPap){
                    if(pc.Contact__c!=null){
                        String custId = emailCustMap.get(refIdEmailMap.get(pc.affiliate_ref_id__c));
                        Integer com_amt = Integer.valueOf(pc.commission__c*(-100));
                        updateCustomerBalance(pc.Id,custId,com_amt);
                    }
                }
            }
        }
        catch (CalloutException e) { 
            ApexDebugLog apex=new ApexDebugLog(); 
            apex.createLog(new ApexDebugLog.Error('papTrigHandler','papTrigHandler','',e));
        }
    }
    @future (callout=true)
    public static void updateCustomerBalance(Id papComId,String custId,Integer com_amt){
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/customers/'+custId+'/balance_transactions');
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        String body = 'amount='+com_amt+'&currency=usd';
        http.setBody(body);
        System.debug('body : '+body);
        Http con = new Http();
        HttpResponse hs ;
        
        if(!Test.isRunningTest()){
            try {
                hs= con.send(http);
            } catch (CalloutException e) { 
                ApexDebugLog apex=new ApexDebugLog(); 
                apex.createLog(new ApexDebugLog.Error('updateCustomerBalance','calloutresponse','',e));
            }
            
        }else{
            hs= new HttpResponse();
            hs.setHeader('Content-Type', 'application/json');
            hs.setBody('{"status":"ok","{"id": "cbtxn_1GKo3kFzCf73siP0XVJdB1oE","object": "customer_balance_transaction","amount": -500,"created": 1583769748,"credit_note": null,"currency": "usd","customer": "cus_GsBRLv0RyvXmjg","description": null,"ending_balance": -500,"invoice": null,"livemode": false,"metadata": null,"type": "adjustment"}}');
            hs.setStatusCode(200);
            hs.setStatus('OK');
        }
        System.debug('--------response------- '+hs.getBody());
        if(hs.getStatusCode() == 200){
            Pap_Commission__c pc = [SELECT id,paid__c FROM Pap_Commission__c WHERE id=:papComId LIMIT 1];
            pc.paid__c = true;
            update pc;
        }
    }
    
    
}