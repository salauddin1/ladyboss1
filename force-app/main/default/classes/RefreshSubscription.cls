public class RefreshSubscription{
    //To get all the subscriptions of the customer
    @AuraEnabled
    public static string getContactId(String conId) {
       // List<Stripe_Profile__c > lstProfAddOne =  new List<Stripe_Profile__c > ();
        String jsonStr;
        List<Stripe_Profile__c > lstProf =  [select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where Customer__c =:conId];
        System.debug('-----------'+lstProf.size());
        String recTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Map<String,string>  mapProfileJson = new Map<String,string> ();
        for(Stripe_Profile__c sp:lstProf  ) {
             List<Stripe_Profile__c> lstStrp  = new List<Stripe_Profile__c>();
            lstStrp.add(sp); 
            jsonStr = StripeBatchHandler.getAllSubscriptions(lstStrp,recTypeID );
            mapProfileJson.put(sp.Stripe_Customer_Id__c,jsonStr);
        }
        
        for(Stripe_Profile__c sp:lstProf  ) {
            List<Stripe_Profile__c > lstProfAddOne =  new List<Stripe_Profile__c > ();
            lstProfAddOne.add(sp);
            if(mapProfileJson.containsKey(sp.Stripe_Customer_Id__c)) {
            String jsonString = json.serialize(lstProfAddOne);
             getAllSubscriptions(jsonString ,mapProfileJson.get(sp.Stripe_Customer_Id__c));
            }
        }
        
            
        return 'Refresh completed';
    }
    
   // @future
    public static void getAllSubscriptions( string jsonString ,string jsonStr) {
        List<Stripe_Profile__c > stripeProfileList = (List<Stripe_Profile__c >)Json.deserialize(jsonString,List<Stripe_Profile__c >.class);
        //String jsonStr;
        String recTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        List< Opportunity > lstOpp        = new List< Opportunity >();
        // To add the line item list to map from salesforce
        List<Stripe_Product_Mapping__c> mcs = Stripe_Product_Mapping__c.getall().values();
        Map<string,string> productMap = new Map<string,string>();
        Set<String> prodNameSet  = new Set<String>();
        for(Stripe_Product_Mapping__c spm: mcs){
            if(spm.Stripe_Nick_Name__c!=null)
                productMap.put(spm.Stripe_Nick_Name__c,spm.Salesforce_Product__c);
            
        }
        
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];
        List<PricebookEntry> pbe1 = [SELECT Id, Name, Pricebook2Id, Product2Id,Product2.Name FROM PricebookEntry  where Pricebook2ID=:standardPb.id and isActive=true];
        Map<string,string> priceBookMap = new Map<string,string>();
        for(PricebookEntry pe : pbe1) {
            priceBookMap.put(pe.Name,pe.ID);
        }
        List<Stripe_Profile__c> updateList = new  List<Stripe_Profile__c> ();
        for(Stripe_Profile__c stripeProf:stripeProfileList){
            stripeProf.Subscription_Processed__c = true;
            updateList.add(stripeProf);
            List< Opportunity > OppOb     = new List< Opportunity >();
            List<Stripe_Profile__c> lstStrp  = new List<Stripe_Profile__c>();
            lstStrp.add(stripeProf); 
            //jsonStr = StripeBatchHandler.getAllSubscriptions(lstStrp,recTypeID );
            if(jsonStr!= null && jsonStr != ''){
                String customerId = lstStrp[0].Stripe_Customer_Id__c;
                // List<Stripe_Profile__c> stripeProfList = [select id ,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
                Map<string,string> stripeProfIdMap = new Map<string,string> ();
                Set<Id> setProfId = new Set<Id>();
                Set<Id> setContactId = new Set<Id>();
                Map<string,string> conStripeMap1 = new Map<string,string> ();
                Map<string,string> conStripeMap = new Map<string,string> ();
                for(Stripe_Profile__c sp : stripeProfileList) {
                    stripeProfIdMap.put(sp.Stripe_Customer_Id__c,sp.id);
                    setProfId.add(sp.id);
                    conStripeMap1.put(sp.Stripe_Customer_Id__c,sp.id );
                    conStripeMap.put(sp.Stripe_Customer_Id__c,sp.Customer__c );
                    setContactId.add(sp.Customer__c);
                    
                    
                }
                List<card__c> lstCard = [select id,CustomerID__c,Stripe_Card_Id__c from card__c where (CustomerID__c = : customerId) OR (Stripe_Profile__c IN:setProfId )] ;
                set<Id> cardIdSet = new set<Id>();
                Map<string,string> cardMap = new Map<string,string> ();
                Map<string,string> cardProfMap = new Map<string,string> ();
                for (card__c cd : lstCard){
                    cardIdSet.add(cd.id);
                    cardMap.put(cd.CustomerID__c,cd.id);
                    cardProfMap.put(cd.Stripe_Card_Id__c,cd.id);
                }
                
                List<Opportunity> oppList = [select id,Card__c,Stripe_Charge_Id__c,Subscription__c,Contact__c, (SELECT Id, Subscription_Id__c FROM OpportunityLineItems)  from opportunity where Card__c IN :cardIdSet or Contact__c in: setContactId];
                Map<string,string> oppExistingMap = new Map<string,string> ();
                Map<string,string> oppSubChargeMap = new Map<string,string> ();
                Map<string,Opportunity> oppExMap = new Map<string,Opportunity> ();
                Map<string,string> oliMap = new Map<string,string>();
                Map<string,List<OpportunityLineItem>> oppListMap = new Map<string,List<OpportunityLineItem>> ();
                for(Opportunity o : oppList){
                    if (o.OpportunityLineItems.size () > 0) {
                        for(OpportunityLineItem oli :o.OpportunityLineItems){
                            if(oli.Subscription_Id__c!=null) {
                                oliMap.put(oli.Subscription_Id__c,oli.Subscription_Id__c);
                                
                                if(!oppListMap.containskey(oli.Subscription_Id__c)){
                                    List<OpportunityLineItem> ops =  new List<OpportunityLineItem>();
                                    ops.add(oli);
                                    oppListMap.put(oli.Subscription_Id__c,ops);
                                }else{
                                    
                                    oppListMap.get(oli.Subscription_Id__c).add(oli);
                                }
                            }
                            
                        }
                    }
                    oppExistingMap.put(o.Subscription__c,o.Card__c);
                    oppSubChargeMap.put(o.Stripe_Charge_Id__c,o.Card__c);
                    oppExMap.put(o.Subscription__c,o);
                }
                
                
                System.debug('oppListMap-----'+oppListMap);
                StripeListSubscriptionClone.Charges varCharges;
                JSONParser parse;
                //StripeCard o = (StripeCard.parse(response));
                
                parse = JSON.createParser(jsonStr.replaceAll('currency','currency_data')); 
                
                varCharges = (StripeListSubscriptionClone.Charges)parse.readValueAs(StripeListSubscriptionClone.Charges.class);
                
                Set<String> setExternalOppID = new Set<String>();
                Set<String> setExternalAccID = new Set<String>();
                Map<String,Opportunity> mapOpp = new Map<String,Opportunity>();
                Map<String,Account> mapAcc = new Map<String,Account>();
                
                List<Opportunity > oppUpdateList =  new List<Opportunity >();
                List<Opportunity > oppInsertList =  new List<Opportunity >();
                Datetime createdDate;
                for(StripeListSubscriptionClone.Data charges : varCharges.data){  
                    if(charges.id!=null){
                        
                        if(!oppExistingMap.ContainsKey(charges.id) && !oppSubChargeMap.ContainsKey(charges.id) && !oliMap.ContainsKey(charges.id)){
                            // System.debug('==>'+charges.plan.id );
                            Opportunity opp = new Opportunity();
                            //check for the contact
                            if(conStripeMap.containsKey(charges.customer))
                                opp.Contact__c = conStripeMap.get(charges.customer);
                            //chcek for the profile
                            if(stripeProfIdMap.containsKey(charges.customer))
                                opp.Stripe_Profile__c = stripeProfIdMap.get(charges.customer);
                            //check for the card
                            if(cardMap.ContainsKey(charges.customer)!=null)
                                opp.Card__c = cardMap.get(charges.customer);
                            if (recTypeID !='' && recTypeID !=null)
                                opp.RecordTypeId = recTypeID ;
                            opp.Name= charges.id;
                            opp.stageName= 'Closed Won';
                            opp.CloseDate = Date.valueOf(System.today());
                            opp.Stripe_Charge_Id__c = charges.id;
                            opp.Subscription__c = charges.id;
                            //opp.Success_Failure_Message__c = charges.status; 
                            opp.Status__c    =      charges.status;     
                            opp.Statement_Descriptor__c = charges.statement_descriptor;
                            opp.Description= charges.description;
                            opp.Currency__c = charges.currency_data;
                            
                            Integer quant = 0;
                            if(charges.items!=null){
                                //System.debug('--------de-'+charges.items.data);
                                Integer amountSum = 0;
                                for (StripeListSubscriptionClone.Data_x dt : charges.items.data) {
                                    //System.debug('--------de-'+charges.items.data);
                                    if(dt.plan.nickname!=null || dt.plan.nickname !=''){
                                        opp.Name= dt.plan.nickname;
                                        
                                    }
                                    else if( charges.statement_descriptor!=null) {
                                        opp.Name= charges.statement_descriptor;
                                    } else {
                                        opp.Name= charges.id;
                                    }
                                    quant =quant + Integer.valueOF(dt.quantity);
                                    Integer decimalAmount = 0;
                                    if(dt.plan.amount!=null || dt.plan.amount !=''){
                                        decimalAmount = amountSum + Integer.valueOF(dt.plan.amount);
                                        System.debug('----opp.Name----'+Integer.valueOF(dt.plan.amount));
                                    }
                                    opp.Amount= Decimal.valueOf(decimalAmount)*0.01;
                                }
                                
                            }
                            else
                            {
                                opp.Name= charges.id;
                            }
                            if(charges.plan!=null){
                                
                                //opp.Amount = Decimal.valueOf(charges.plan.amount)*0.01;
                            }
                            opp.Quantity__c= quant ;
                            if(charges.status !='canceled' && opp.Name!=null )
                                oppInsertList.add(opp);
                        }
                        else{
                            
                            Opportunity opp = oppExMap.get(charges.id);
                            
                            if(opp!=null){
                                System.debug('====== charges.status======='+ charges.status);
                                opp.Status__c    =      charges.status; 
                                //opp.Success_Failure_Message__c = charges.status;  
                                if(charges.plan!=null){
                                    
                                    opp.Amount = Decimal.valueOf(charges.plan.amount)*0.01;
                                }                       
                                if(stripeProfIdMap.containsKey(charges.customer)){
                                    oppUpdateList.add(opp);
                                }
                                
                            }
                            
                            
                            
                        }
                    }
                }
                if(updateList.size() > 0)
                    update updateList;
                if(oppInsertList.size() > 0){
                    
                    insert oppInsertList;
                    
                    
                }   
                if(oppUpdateList.size() > 0){
                    
                    update oppUpdateList;
                    
                    
                }
                Map<string,Opportunity > oppSubIdMap = new Map<string,Opportunity >();
                Set<Id> oppIdSet = new Set<Id> ();
                
                //Map<string,string> oppSubIdMap = new Map<string,string>();
                for(Opportunity opp : oppInsertList) {
                    if(opp.Subscription__c != null)
                        oppSubIdMap.put(opp.Subscription__c,opp); 
                         oppIdSet.add(opp.id);
                }
                for(Opportunity opp : oppUpdateList) {
                    if(opp.Subscription__c != null){
                        oppSubIdMap.put(opp.Subscription__c,opp);
                        oppIdSet.add(opp.id);
                    }
                }
                List<OpportunityLineItem> exOppLine = [select id,Subscription_Id__c from OpportunityLineItem where OpportunityId IN : oppIdSet];
                Map<string,string> exsistingOppLineMap = new Map<string,string>();
                for(OpportunityLineItem oppLineItem :exOppLine) {
                    exsistingOppLineMap.put(oppLineItem.Subscription_Id__c,oppLineItem.Subscription_Id__c);
                }
                Set<OpportunityLineitem> lstopLiItemUpdate = new Set<OpportunityLineItem>();
                List<OpportunityLineItem> oppLinelist = new List<OpportunityLineItem>();
                for(StripeListSubscriptionClone.Data charges : varCharges.data){  
                    if(charges.id!=null){
                        if(charges.items!=null){
                            //System.debug('--------de-'+charges.items.data);
                            for (StripeListSubscriptionClone.Data_x dt : charges.items.data) {
                                
                                if(dt.plan.nickname!=null){
                                    if(oppSubIdMap.containsKey(String.valueOf(charges.id)) && !exsistingOppLineMap.containsKey(String.valueOf(charges.id))) {
                                        OpportunityLineItem oppli = new OpportunityLineItem(); 
                                        //oppli.OpportunityId = oppSubIdMap.get(String.valueOf(charges.id));
                                        oppli.OpportunityId = oppSubIdMap.get(String.valueOf(charges.id)).id;
                                        if(charges.status=='active'){
                                        oppli.Success_Failure_Message__c='customer.subscription.created' ;
                                        }
                                        
                                        System.debug('----opp.NickName----'+String.valueOf(dt.plan.nickname));
                                        if(priceBookMap.containsKey(productMap.get(String.valueOf(dt.plan.nickname))))  {
                                            System.debug('----opp.NickNameEnter----'+String.valueOf(dt.plan.nickname));
                                            oppli.PricebookEntryId= priceBookMap.get(productMap.get(dt.plan.nickname));
                                            oppli.Subscription_Id__c =String.valueOf(charges.id); 
                                            oppli.Quantity = Integer.valueOf(dt.quantity);
                                            String amt = String.ValueOf(dt.plan.amount);
                                            oppli.TotalPrice =  decimal.valueOf(amt )*0.01;
                                            oppLinelist.add( oppli);
                                        }
                                        
                                    }
                                    else
                                    {
                                        if(oppListMap.get(String.valueOf(charges.id)) !=null ) {
                                    List<OpportunityLineitem> lstopLiItem =oppListMap.get(String.valueOf(charges.id));
                                    for(OpportunityLineitem opl : lstopLiItem ) {
                                     opl.Success_Failure_Message__c = charges.status; 
                                     lstopLiItemUpdate.add(opl); 
                                    }
                                    }
                                    }
                                }
                                
                            }
                            
                        }
                        
                    }
                }
                if(oppLinelist.size() > 0) {
                    insert oppLinelist;
                }
                if(lstopLiItemUpdate.size() > 0) {
                List<opportunityLineItem> lineUpdate = new List<opportunityLineItem>();
                lineUpdate.addAll(lstopLiItemUpdate);
                update lineUpdate;
                }
                
            }
        }
        
        
        //System.debug('---lstOpp----'+lstOpp);
        
    }
}