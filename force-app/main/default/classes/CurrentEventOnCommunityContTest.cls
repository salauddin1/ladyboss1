@isTest
public class CurrentEventOnCommunityContTest {
    @isTest
    public static void test() {
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@gmail.com';
        insert con;
        
        Event eventRecord = new Event();
        eventRecord.IsForWebSiteUse__c = true;
        eventRecord.Description = 'Test Data';
        eventRecord.Contact__c = con.id ;
        eventRecord.StartDateTime = system.now();
        eventRecord.EndDateTime = system.now();
        insert eventRecord;
        
        String check = String.valueOf( eventRecord.StartDateTime);
        CurrentEventOnCommunityCont.getCurrentEvent();
        CurrentEventOnCommunityCont.getWeekDay(check);
        CurrentEventOnCommunityCont.getStEndTimeVal(eventRecord.StartDateTime, eventRecord.EndDateTime);
    }
    
}