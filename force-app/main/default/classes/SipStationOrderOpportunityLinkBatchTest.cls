@isTest
public class SipStationOrderOpportunityLinkBatchTest {
    @isTest
    public static void testforordlinktrigger() {
        List<ShipStation_Orders__c> orderDetail = new List<ShipStation_Orders__c> ();
        contact con = new contact();
        con.lastName = 'YTest';
        insert con;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderNumber__c = 'ch_1FHabFFzCf73siP0ZyFwpP0i';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        insert shipOrder;
        ShipStation_Orders__c updtrec = [select Id from ShipStation_Orders__c where Id =: shipOrder.Id];
        updtrec.country__c = 'US';
        ShipstationOrderStatusShippedBatch.flag = false;
        Update updtrec;
    }
    @isTest
    public static void testforordlink() {
        List<ShipStation_Orders__c> orderDetail = new List<ShipStation_Orders__c> ();
        contact con = new contact();
        con.lastName = 'YTest';
        insert con;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderNumber__c = 'ch_1FHabFFzCf73siP0ZyFwpP0i';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        insert shipOrder;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        orderDetail.add(shipOrder);
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.Stripe_Charge_Id__c = shipOrder.orderNumber__c;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        insert opp ;
        Test.startTest();
        Database.executeBatch(new SipStationOrderOpportunityLinkBatch());
        Test.stopTest();
    }
    @isTest
    public static void testforordlink2() {
        List<ShipStation_Orders__c> orderDetail = new List<ShipStation_Orders__c> ();
        contact con = new contact();
        con.lastName = 'YTest';
        insert con;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderNumber__c = 'ch_1FHabFFzCf73siP0ZyFwpP0i';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        insert shipOrder;
        
        shipOrder.country__c = 'US';
        Update shipOrder;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        orderDetail.add(shipOrder);
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.Stripe_Charge_Id__c = shipOrder.orderNumber__c;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        insert opp ;
        Test.startTest();
        AssociateShiporderToOpportunity.linkOpptoShipOrd(orderDetail);
        Test.stopTest();
    }
}