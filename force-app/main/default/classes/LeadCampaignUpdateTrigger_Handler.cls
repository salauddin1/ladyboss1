public class LeadCampaignUpdateTrigger_Handler {
    
    Public Static void doAfterInsert(List<Lead> leadList){
        Map<Id,Lead> leadMap = new Map<ID,Lead>();
        Map<Id,Lead> cLeadMap = new Map<ID,Lead>();
        for(Lead ld : leadList){
            if(ld.Products__c != null){
                leadMap.put(ld.Id, ld);
            }else if(ld.Products__c == null && ld.Potential_Buyer__c != null ){
                cLeadMap.put(ld.Id,ld);
            }
            if(!leadMap.isEmpty()){
                updateCampaignMembers(leadMap);
            }else if(leadMap.isEmpty() && !cLeadMap.isEmpty()){
                addPotentialBuyers(cLeadMap);
            }        
        }
    }        
    
    
    Public Static void doBeforeUpdate(List<Lead> leadList , Map<Id,Lead> leadIdMap){
        Map<Id,Lead> leadMap = new Map<ID,Lead>();
        Map<Id,Lead> cLeadMap = new Map<ID,Lead>();
        for(Lead ld : leadList){
            if(ld.Products__c != null){
                if(ld.Products__c != leadIdMap.get(ld.Id).Products__c)
                    leadMap.put(ld.Id, ld);
            }else if(ld.Products__c == null && ld.Potential_Buyer__c != null && ld.Potential_Buyer__c != leadIdMap.get(ld.Id).Potential_Buyer__c) {
                cLeadMap.put(ld.Id,ld);
            }           
        }
        if(!leadMap.isEmpty()){
            updateCampaignMembers(leadMap);
        } else if(leadMap.isEmpty() && !cLeadMap.isEmpty()){
            addPotentialBuyers(cLeadMap);
        }   
    }
    
    Public Static void updateCampaignMembers(  Map<Id,Lead> leadMap){
        
        
        Map<String,List<String>> campaignLeadMap = new Map<String,List<String>>();
        Set<CampaignMember> targetCampaignMembersSet = new Set<CampaignMember>();        
        Set<String> cMemberLeadIdSet = new Set<String>();
        Set<CampaignMember> cMemberDeletionSet = new Set<CampaignMember>();
        Map<String,List<Workflow_Configuration__c>> sourceConfigurationMap = new Map<String,List<Workflow_Configuration__c>>();
        
        
        System.debug('leadMap---'+leadMap);
        Set<String> campaignIdSet = new Set<String>();
        List<CampaignMember> cMemberList = [SELECT Id, CampaignId,Campaign.Name, LeadId FROM CampaignMember WHERE LeadId IN :leadMap.keySet()];
        if(!cMemberList.isEmpty()){
            for(CampaignMember cm: cMemberList){
                if(cm.Campaign.Name.Contains('Day')  && (cm.Campaign.Name.Contains('Buy') || cm.Campaign.Name.Contains('Lead')))
                    campaignIdSet.add(cm.CampaignId);
            }
            System.debug('campaignIdSet--->'+campaignIdSet);
            System.debug('cMemberList---'+cMemberList);
            System.debug('cMemberList---'+cMemberList.size());
            if(!campaignIdSet.isEmpty()){
                for(Workflow_Configuration__c wc : [SELECT Id, Source_Campaign__c, Target_Campaign__c, Products__c FROM Workflow_Configuration__c WHERE Source_Campaign__c IN :campaignIdSet]){
                    system.debug('wc--->'+wc);
                    if(sourceConfigurationMap.containsKey(wc.Source_Campaign__c))
                        sourceConfigurationMap.get(wc.Source_Campaign__c).add(wc);
                    else
                        sourceConfigurationMap.put(wc.Source_Campaign__c, new List<Workflow_Configuration__c>{wc});
                }
                System.debug('sourceConfigurationMap---'+sourceConfigurationMap);
                System.debug('sourceConfigurationMap---'+sourceConfigurationMap.size());
                
                
                for(CampaignMember cm : cMemberList){
                    if(leadMap.containsKey(cm.LeadId) && sourceConfigurationMap.containsKey(cm.CampaignId)){
                        
                        for(Workflow_Configuration__c wc : sourceConfigurationMap.get(cm.campaignId)){
                            Lead ld = leadMap.get(cm.LeadId);
                            String productName = ld.Products__c.Split(';').get(ld.Products__c.Split(';').size()-1);
                            if(wc.Source_Campaign__c == cm.campaignId && wc.Products__c == productName){
                                System.debug('wc.Source_Campaign__c-->'+wc.Source_Campaign__c);
                                System.debug('lcw.campaignIdc-->'+cm.campaignId);
                                cMemberLeadIdSet.add(cm.LeadId);
                                if(campaignLeadMap.containsKey(wc.Target_Campaign__c)){
                                    campaignLeadMap.get(wc.Target_Campaign__c).add(cm.LeadId);
                                }else
                                    campaignLeadMap.put(wc.Target_Campaign__c, new List<String>{cm.LeadId});
                            }
                        }
                    }
                }
                
                for(CampaignMember cm : cMemberList){
                    for(String leadId : cMemberLeadIdSet){
                        if(cm.LeadId == leadId && !cMemberDeletionSet.contains(cm) && sourceConfigurationMap.containsKey(cm.CampaignId)){
                            cMemberDeletionSet.add(cm);
                        }
                    }
                }
                
                System.debug('campaignLeadMap---'+campaignLeadMap);
                Map<String,String> existingCMMap = new Map<String,String>();
                for(String targetId : campaignLeadMap.keySet()){
                    for(String leadId : campaignLeadMap.get(targetId)){
                        CampaignMember cm = new CampaignMember();
                        cm.LeadId = leadId;
                        cm.CampaignId = targetId;
                        existingCMMap.put(targetId, leadId);
                        if(!targetCampaignMembersSet.contains(cm))
                            targetCampaignMembersSet.add(cm);
                    }
                }
                
                
                System.debug('targetCampaignMembersSet---'+targetCampaignMembersSet);
                System.debug('cMemberDeletionSet---'+cMemberDeletionSet);
                List<CampaignMember> cmExistingList = [SELECT Id, CampaignId, LeadId FROM CampaignMember WHERE LeadId IN :existingCMMap.values() AND CampaignId IN :existingCMMap.keySet()];
                List<CampaignMember> deletionList = new List<CampaignMember>(cMemberDeletionSet);
                if(!cmExistingList.isEmpty()){
                    deletionList.addAll(cmExistingList);
                }
                try{
                    Database.delete( deletionList);
                    try{
                        Database.upsert(new List<CampaignMember>(targetCampaignMembersSet));
                        
                    }Catch(Exception ex){
                        System.debug('Upsert Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                    }
                }Catch(Exception ex){
                    System.debug('XDelete Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                }            
            }
        }else{
            
            Map<String,String> productLeadMap = new Map<String,String>();
            Map<String,String> productCampaignMap = new Map<String,String>();
            List<String> productList = new List<String>();
            Set<CampaignMember> cmList = new Set<CampaignMember>();
            for(String lid : leadMap.keySet()){
                Lead ld = leadMap.get(lid);
                String productName = ld.Products__c.Split(';').get(ld.Products__c.Split(';').size()-1);
                productLeadMap.put(productName, lid);
                productList.add(productName);
            }
            system.debug('productLeadMap-->'+productLeadMap);
            system.debug('productList-->'+productList);
            if(!productList.isEmpty()){
                for(Workflow_Configuration__c wc : [SELECT Id, Source_Campaign__c, Target_Campaign__c, Products__c FROM Workflow_Configuration__c WHERE Products__c IN :productList]){
                    if(!productCampaignMap.containsKey(wc.Products__c))
                        productCampaignMap.put(wc.Products__c,wc.Target_Campaign__c);
                }
                if(!productCampaignMap.isEmpty()){
                    for(String p : productLeadMap.keySet()){
                        CampaignMember cm = new CampaignMember();
                        cm.LeadId = productLeadMap.get(p);
                        cm.CampaignId =productCampaignMap.get(p);
                        system.debug('cm--->'+cm);
                        cmList.add(cm);
                    }
                    
                    system.debug('cmList--->'+cmList);
                    insert new List<CampaignMember>(cmList);
                }
            }
        } 
    }
    
    public static void addPotentialBuyers(Map<Id,Lead> leadMap){
        Map<String,CampaignMember> delCMMap = new Map<String,CampaignMember>();
        Set<String> potentialCampaignSet = new Set<String>();
        Map<String,String> productLeadMap = new Map<String,String>();
        Map<String,Campaign> campaignNameMap = new Map<String,Campaign>(); 
        Set<CampaignMember> insertCMSet = new Set<CampaignMember >();
        
        for(CampaignMember cm : [SELECT Id, CampaignId,Campaign.Name, LeadId FROM CampaignMember WHERE LeadId IN :leadMap.keySet()]){
            String leadCampaignId = String.valueOf(cm.LeadId)+String.valueOf(cm.CampaignId);           
            delCMMap.put(cm.Id,cm);
        }
        system.debug('delCMMap--->'+delCMMap);
        
        for(String lid : leadMap.keySet()){
            Lead ld = leadMap.get(lid);
            String productName = ld.Potential_Buyer__c;
            system.debug('potential buyer --->'+productName);
            potentialCampaignSet.add(productName);
            productLeadMap.put(lid, productName);
        }
        system.debug('potentialCampaignSet--> '+potentialCampaignSet);
        system.debug('productLeadMap--> '+productLeadMap);
        
        for(Campaign c : [SELECT Id,Name From Campaign WHERE Name IN :potentialCampaignSet]){
            campaignNameMap.put(c.Name, c);
        }
        system.debug('campaignNameMap---> '+campaignNameMap);
        
        if(!productLeadMap.isEmpty()){
            for(String lid : productLeadMap.keySet()){
                string cname = productLeadMap.get(lid);
                if(cname != null){
                    Campaign c = campaignNameMap.get(cname);
                    CampaignMember cm = new CampaignMember();
                    cm.CampaignId = c.Id;
                    cm.LeadId = lid;
                    insertCMSet.add(cm);
                }
            }
            system.debug('insertCMSet---> '+insertCMSet);
        }
        
        List<CampaignMember> deleteList = delCMMap.values();
        system.debug('deleteList--->'+deleteList);
        if(!deleteList.isEmpty()){
            try{
                delete deleteList;
                if(!insertCMSet.isEmpty()){
                    try{
                        insert new List<CampaignMember>(insertCMSet);
                    }catch(Exception ex){ 
                        system.debug(ex.getMessage()+ex.getLineNumber());
                    }
                }
                
            }catch(Exception ex){
                system.debug(ex.getMessage()+ex.getLineNumber());
            }
        }else if(!insertCMSet.isEmpty()){
            try{
                insert new List<CampaignMember>(insertCMSet);
            }catch(Exception ex){ 
                system.debug(ex.getMessage()+ex.getLineNumber());
            }
        }
    }
}