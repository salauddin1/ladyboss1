@IsTest
public class StripeListCustomer_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'    \"object\": \"list\",'+
		'    \"data\": ['+
		'        {'+
		'            \"id\": \"cus_BU9YEPogMk0fA1\",'+
		'            \"object\": \"customer\",'+
		'            \"account_balance\": 0,'+
		'            \"created\": 1506640589,'+
		'            \"currency\": null,'+
		'            \"default_source\": null,'+
		'            \"delinquent\": false,'+
		'            \"description\": \"des3\",'+
		'            \"discount\": null,'+
		'            \"email\": \"wynche3@cloudcreations.com\",'+
		'            \"livemode\": false,'+
		'            \"metadata\": {},'+
		'            \"shipping\": null,'+
		'            \"sources\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9YEPogMk0fA1/sources\"'+
		'            },'+
		'            \"subscriptions\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9YEPogMk0fA1/subscriptions\"'+
		'            }'+
		'        },'+
		'        {'+
		'            \"id\": \"cus_BU9Xv9yA6CrHbv\",'+
		'            \"object\": \"customer\",'+
		'            \"account_balance\": 0,'+
		'            \"created\": 1506640578,'+
		'            \"currency\": null,'+
		'            \"default_source\": null,'+
		'            \"delinquent\": false,'+
		'            \"description\": \"des2\",'+
		'            \"discount\": null,'+
		'            \"email\": \"wynche2@cloudcreations.com\",'+
		'            \"livemode\": false,'+
		'            \"metadata\": {},'+
		'            \"shipping\": null,'+
		'            \"sources\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9Xv9yA6CrHbv/sources\"'+
		'            },'+
		'            \"subscriptions\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9Xv9yA6CrHbv/subscriptions\"'+
		'            }'+
		'        },'+
		'        {'+
		'            \"id\": \"cus_BU9XtJ6Muptggp\",'+
		'            \"object\": \"customer\",'+
		'            \"account_balance\": 0,'+
		'            \"created\": 1506640558,'+
		'            \"currency\": null,'+
		'            \"default_source\": null,'+
		'            \"delinquent\": false,'+
		'            \"description\": \"des1\",'+
		'            \"discount\": null,'+
		'            \"email\": \"wynche@cloudcreations.com\",'+
		'            \"livemode\": false,'+
		'            \"metadata\": {},'+
		'            \"shipping\": null,'+
		'            \"sources\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9XtJ6Muptggp/sources\"'+
		'            },'+
		'            \"subscriptions\": {'+
		'                \"object\": \"list\",'+
		'                \"data\": [],'+
		'                \"has_more\": false,'+
		'                \"total_count\": 0,'+
		'                \"url\": \"/v1/customers/cus_BU9XtJ6Muptggp/subscriptions\"'+
		'            }'+
		'        }'+
		'    ],'+
		'    \"has_more\": false,'+
		'    \"url\": \"/v1/customers\"'+
		'}';
		StripeListCustomer.Customers r = StripeListCustomer.Customers.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.Customers objJSON2Apex = new StripeListCustomer.Customers(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		//System.assert(objJSON2Apex.object_Z == null);
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.has_more == null);
		System.assert(objJSON2Apex.url == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.Metadata objMetadata = new StripeListCustomer.Metadata(System.JSON.createParser(json));
		System.assert(objMetadata != null);
        System.assert(objMetadata.name == null);
        System.assert(objMetadata.first_name == null);
        System.assert(objMetadata.last_name == null);
        System.assert(objMetadata.phone == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.Data objData = new StripeListCustomer.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_data == null);
		System.assert(objData.account_balance == null);
		System.assert(objData.created == null);
		System.assert(objData.currencyx == null);
		System.assert(objData.default_source == null);
		System.assert(objData.delinquent == null);
		System.assert(objData.description == null);
		System.assert(objData.discount == null);
		System.assert(objData.email == null);
		System.assert(objData.livemode == null);
		System.assert(objData.metadata == null);
		System.assert(objData.shipping == null);
		System.assert(objData.sources == null);
		//System.assert(objData.subscriptions == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.Sources objSources = new StripeListCustomer.Sources(System.JSON.createParser(json));
		System.assert(objSources != null);
		System.assert(objSources.object_Z == null);
		System.assert(objSources.data == null);
		System.assert(objSources.has_more == null);
		System.assert(objSources.total_count == null);
		System.assert(objSources.url == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.Address objAddress = new StripeListCustomer.Address(System.JSON.createParser(json));
		System.assert(objAddress != null);
		System.assert(objAddress.city == null);
        System.assert(objAddress.country == null);
        System.assert(objAddress.line1 == null);
        System.assert(objAddress.line2 == null);
        System.assert(objAddress.postal_code == null);
        System.assert(objAddress.state == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.CustAddress objCustAddress = new StripeListCustomer.CustAddress(System.JSON.createParser(json));
		System.assert(objCustAddress != null);
        System.assert(objCustAddress.address == null);
        System.assert(objCustAddress.name == null);
        System.assert(objCustAddress.phone == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCustomer.DataSource objDataSource = new StripeListCustomer.DataSource(System.JSON.createParser(json));
		System.assert(objDataSource != null);
        System.assert(objDataSource.id == null);
        System.assert(objDataSource.object_Z == null);
        System.assert(objDataSource.address_city == null);
        System.assert(objDataSource.address_country == null);
        System.assert(objDataSource.address_line1 == null);
        System.assert(objDataSource.address_line1_check == null);
        System.assert(objDataSource.address_line2 == null);
        System.assert(objDataSource.address_state == null);
        System.assert(objDataSource.address_zip == null);
        System.assert(objDataSource.address_zip_check == null);
        System.assert(objDataSource.brand == null);
        System.assert(objDataSource.country == null);
        System.assert(objDataSource.customer == null);
        System.assert(objDataSource.cvc_check == null);
        System.assert(objDataSource.dynamic_last4 == null);
        System.assert(objDataSource.exp_month == null);
        System.assert(objDataSource.exp_year == null);
        System.assert(objDataSource.fingerprint == null);
        System.assert(objDataSource.funding == null);
        System.assert(objDataSource.last4 == null);
        System.assert(objDataSource.name == null);
        System.assert(objDataSource.tokenization_method == null);
	}
}