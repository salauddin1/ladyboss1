global class BatchToSendInvoiceAndPaymentProcess implements Database.Batchable<sObject>,Schedulable{
    global void execute(SchedulableContext SC) {
          Database.executeBatch(new BatchToSendInvoiceAndPaymentProcess());
     }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT id,Async_limit_status__c,Contact_email__c,(SELECT id FROM OpportunityLineItems) FROM Opportunity WHERE Async_limit_status__c EXCLUDES (\'Mail Sent\') AND Async_limit_status__c INCLUDES (\'Async Limit Exceeded\') AND Async_limit_status__c EXCLUDES (\'No Error\') AND Stripe_Message__c=\'Success\' ORDER BY CreatedDate DESC LIMIT 1000');
    }   
    global void execute(Database.BatchableContext bc, List<Opportunity> oppList){
        Set<Id> oppIds = new Set<Id>();
        List<Id> opplineitemidlist = new List<Id>();
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        for (Opportunity opp : oppList) {
            oppIds.add(opp.id);
            for (OpportunityLineItem opli : opp.OpportunityLineItems) {
                opplineitemidlist.add(opli.id);
            }
            opp.Async_limit_status__c = 'Async Limit Exceeded;Mail Sent';
            CustomerCheck.checkShopifyCustomerNotFuture(opp.id);
            CustomerCheck.getContactsWithMetadataNotFuture(opp.contact_email__c );
            oppListToUpdate.add(opp);
        } 
        if (opplineitemidlist.size()>0) {
            PaymentProcessor.paymentProcess(opplineitemidlist);
            sendmailInvoiceToCustomer(oppIds);
        }     
        if (oppListToUpdate.size()>0) {
            update oppListToUpdate;
        }      
    } 
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }  
    public static void sendmailInvoiceToCustomer(Set<Id> oppIds){
        try{
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
            
            for(Opportunity opp : [select id,Contact__r.Email,(select id ,product2Id,product2.Email_Body__c,product2.Name from OpportunityLineItems) from Opportunity where id =:oppIds ]){
                
                for(OpportunityLineItem lineItem : opp.OpportunityLineItems){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] { opp.Contact__r.Email});
                    
                    mail.setSubject('LadyBoss Receipt - ' + lineItem.product2.Name);
                    mail.setHtmlBody((lineItem.product2.Email_Body__c !=null ? lineItem.product2.Email_Body__c: 'Thank you for your purchase. If you have any questions or concerns, please email us at support@ladyboss.com.'));
                    mail.setUseSignature(false);
                    
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    
                    PageReference pdf = Page.OpportunityLineItemInvoice;
                    pdf.getParameters().put('id',lineItem.Id);
                    Blob body;                
                    if(!test.isRunningTest())  {
                        try{
                            body = pdf.getContent();
                        }catch(VisualforceException e){
                            body=Blob.valueOf('Some text');
                        }
                    }else  {
                        body=Blob.valueOf('Some text');   
                    }    
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.setContentType('application/pdf');
                    attach.setFileName('LadyBoss Receipt.pdf');
                    attach.setInline(false);
                    attach.Body = body;
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
                    mails.add(mail);
                    
                }
            }
            if(mails.size() >0 && !test.isRunningTest())
                Messaging.sendEmail(mails);
        }catch(Exception e){
            
        }
        
        
    }
}