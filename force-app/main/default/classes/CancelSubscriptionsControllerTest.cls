@isTest
public class CancelSubscriptionsControllerTest {
    @IsTest
    static void methodName(){
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        contact con = new contact();
        con.lastName='lastName';
        con.firstName='firstName';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_Eug8juCETidS33';
        sp.Customer__c = con.id;
        insert sp;
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        card.Stripe_Card_Id__c = 'card_testtest';
        Date d = date.Today();
        insert card;
        
        ACH_Account__c ac = new ACH_Account__c(); 
        ac.ACH_Token__c = null;
        ac.Name = 'test';
        ac.Stripe_Profile__c = sp.id;
        ac.Stripe_ACH_Id__c='achiddskfdsf';
        insert ac;
        
        Product2 pro = new Product2();
        pro.Name='LEAN-3';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        pro.Available_For_Hub__c= true;
        insert pro; 
        Product2 prod = new Product2();
        prod.Name='LEAN-LEAN';
        prod.Price__c=12345;
        prod.Fuel__c = 3;
        prod.Burn__c = 0;
        prod.Lean__c = 1;
        prod.Recover__c = 0;
        prod.Rest__c = 1;
        prod.Family = 'Hardware';
        prod.Stripe_Plan_Id__c='1yrmembershipw';
        prod.IsActive=true;
        prod.Available_For_Hub__c= true;
        insert prod; 
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        opportunity op  = new opportunity();
        op.name=pro.Name+'CLUB';
        op.Contact__c =con.Id;
        op.StageName = 'Closed Won';
        op.RecordTypeId = devRecordTypeId;
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        OpportunityLineItem olpo = new OpportunityLineItem();        
        olpo.Product2Id =pro.id;
        olpo.OpportunityId = op.Id;
        olpo.Quantity = 3;
        olpo.UnitPrice = 2.00;
        olpo.Subscription_Id__c='sub_EypY8AdfVmqvVo';
        olpo.Start__c=System.today()-1;
        olpo.End__c=System.today();    
        olpo.Opportunity_Unique_Name__c = op.Id;
        olpo.PricebookEntryId = standardPrice.Id;
        olpo.Status__c='Active';
        insert olpo;
        List<CancelSubscriptionsController.productWrapper> pwList = new List<CancelSubscriptionsController.productWrapper>();
        CancelSubscriptionsController.productWrapper pw = new CancelSubscriptionsController.productWrapper();
        pw.id = olpo.id;
        pw.totalPrice = 2.00;
        pw.Name = 'Test';
        pwList.add(pw);
        List<CancelSubscriptionsController.productWrapper> pwList1 = new List<CancelSubscriptionsController.productWrapper>();
        CancelSubscriptionsController.productWrapper pw1 = new CancelSubscriptionsController.productWrapper();
        pw1.Id = pro.id;
        pw1.totalPrice = 2.00;
        pw1.Name = 'Test';
        pwList1.add(pw1);
        List<CancelSubscriptionsController.customWrapper> cwList = new List<CancelSubscriptionsController.customWrapper>();
        CancelSubscriptionsController.customWrapper cw = new CancelSubscriptionsController.customWrapper();
        cw.Name = 'burn';
        cw.quantity = 0;
        cwList.add(cw);
        CancelSubscriptionsController.customWrapper cw1 = new CancelSubscriptionsController.customWrapper();
        cw1.Name = 'recover';
        cw1.quantity = 0;
        cwList.add(cw1);
        CancelSubscriptionsController.customWrapper cw2= new CancelSubscriptionsController.customWrapper();
        cw2.Name = 'fuel';
        cw2.quantity = 3;
        cwList.add(cw2);
        CancelSubscriptionsController.customWrapper cw3 = new CancelSubscriptionsController.customWrapper();
        cw3.Name = 'lean';
        cw3.quantity = 1;
        cwList.add(cw3);
        CancelSubscriptionsController.customWrapper cw4 = new CancelSubscriptionsController.customWrapper();
        cw4.Name = 'rest';
        cw4.quantity = 1;
        cwList.add(cw4);
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        Test.startTest();
        CancelSubscriptionsController.getProducts(con.Id);
        CancelSubscriptionsController.CancelSubscriptions(JSON.serialize(pwList));
        CancelSubscriptionsController.EmailReminder(JSON.serialize(pwList));
        CancelSubscriptionsController.checkPayment(JSON.serialize(pwList), JSON.serialize(pwList1));
        CancelSubscriptionsController.createPayment(JSON.serialize(pwList), JSON.serialize(pwList1), card.Stripe_Card_Id__c);
        CancelSubscriptionsController.getAllProducts();
        CancelSubscriptionsController.getCard(con.Id);
        CancelSubscriptionsController.getCustomProduct(JSON.serialize(cwList));
        CancelSubscriptionsController.getTaxForProduct(JSON.serialize(pw), 'line1', 'city', 'region', 'country', 'postalCode');
        CancelSubscriptionsController.getAddress(con.ID);
        Test.stopTest();
        
    }
}