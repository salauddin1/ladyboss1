global class WooOrderMaaToProcessingBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable,Database.Stateful{
    global integer totalOrders=0;
    global integer updateStatus=200;
    global void execute(SchedulableContext SC) {
        WooOrderMaaToProcessingBatch btc = new WooOrderMaaToProcessingBatch();
        Database.executeBatch(btc,1);
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT consumer_key__c,consumer_secret__c,site_url__c FROM Woocommerce_Status_Update__c where consumer_key__c!= null AND consumer_secret__c!= null AND  site_url__c != null]);
    }
    global void execute(Database.BatchableContext bc, List<Woocommerce_Status_Update__c> siteList){
        try{
            System.debug('List size : '+siteList.size());
            String site_url = siteList.get(0).site_url__c;
            String consumer_key = siteList.get(0).consumer_key__c;
            String consumer_secret = siteList.get(0).consumer_secret__c;
            //denver time - 30 minutes
            String tm = (''+(System.now().addHours(-6).addMinutes(-30))).replace(' ','T');
            System.debug(tm);
            // Fetch all orders with status as main order accepted
            HttpResponse response=null;
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            String endpoint = site_url +'?consumer_key='+consumer_key
                +'&consumer_secret='+consumer_secret
                +'&status=wcf-main-order'
                +'&before='+tm
                +'&per_page=50';
            request.setEndpoint(endpoint);
            request.setMethod('GET');
            request.setHeader('Content-Type', 'application/json');
            if(Test.isRunningTest()){
                HttpResponse rep = new HttpResponse();
                rep.setBody('[{"id":4554,"status": "wcf-main-order"}]');
                rep.setStatusCode(200);
                response = rep;
            }else{
                response = http.send(request);
            }
            List<String> orderIdsToUpdate = new List<String>();
            if(response.getStatusCode()==200){
                List<Object> ordList =  (List<Object>)JSON.deserializeUntyped(response.getBody());
                for(Object obj : ordList){
                    Map<String,Object> ord = (Map<String,Object>)obj;
                    orderIdsToUpdate.add(String.valueOf(ord.get('id')));
                }
            }
            if(response.getStatusCode()!=200){
                updateStatus = response.getStatusCode();
            }
            System.debug('Total Order Count : '+orderIdsToUpdate.size());
            if(orderIdsToUpdate.size()==50){
                totalOrders = orderIdsToUpdate.size();
            }
            //Request to update order status to porcessing
            if(orderIdsToUpdate.size()>0){
                List<OrderWrapper> odList = new List<OrderWrapper>();
                for(String ord_id : orderIdsToUpdate){
                    OrderWrapper od1 = new OrderWrapper();
                    od1.id = ord_id;
                    //od1.status = 'processing';
                    od1.status = 'processing';
                    odList.add(od1);
                }
                String body =JSON.serialize(odList);
                body = '{"update" : '+body+'}';
                http = new Http();
                request = new HttpRequest();
                endpoint = site_url +'/batch'+'?consumer_key='+consumer_key
                    +'&consumer_secret='+consumer_secret;
                request.setEndpoint(endpoint);
                request.setMethod('POST');
                request.setTimeout(120000);
                request.setBody(body);
                request.setHeader('Content-Type', 'application/json');
                if(!Test.isRunningTest()){
                    response = http.send(request);
                }
                if(response.getStatusCode()!=200){
                    updateStatus = response.getStatusCode();
                }
                System.debug('response : '+response);
            }
        }catch(Exception e){
            System.debug('error is : '+e.getMessage());
            System.debug('error is : '+e.getMessage());
        }
    }
    global void finish(Database.BatchableContext bc){
        if(totalOrders == 50 && updateStatus == 200){
            WooOrderMaaToProcessingBatch btc = new WooOrderMaaToProcessingBatch();
            Database.executeBatch(btc,1);
        }
    }
    public class OrderListWrapper{
        List<OrderWrapper> orderList;
    }
    public class OrderWrapper{
        string id;
        string status;
    } 
}