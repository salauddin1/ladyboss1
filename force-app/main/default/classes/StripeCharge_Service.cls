@RestResource(urlMapping='/Stripe_ChargeService')
global class StripeCharge_Service {
    
    @HttpPost
    global static void createCustomer() {
        String CustomerIde;
        
        if(RestContext.request.requestBody!=null){
            try {
                String str                              =   RestContext.request.requestBody.toString();
                System.debug('-------res----'+str);
                Map<String, Object> results             =   (Map<String, Object>)JSON.deserializeUntyped(str);
                System.debug(results.get('id'));
                CustomerIde                             =   String.valueOf(results.get('id'));    
                List<Map<String, Object>> data          =   new List<Map<String, Object>>();
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('data');
                
                Map<String, Object> aryCustomers        =   (Map<String, Object>) lstCustomers.get('object');
                Map<String, Object>  metaDataMap        =   (Map<String, Object> )aryCustomers.get('metadata');
                String invoice                          =   (String)aryCustomers.get('invoice'); 
                String order                            =   (String)aryCustomers.get('order'); 
                System.debug('-------order---'+order);
                String customerId                       =   String.valueOf(aryCustomers .get('customer')); 
                
                Map<String, Object> dataObj             =   (Map<String, Object>) aryCustomers.get('plan');
                Map<String, Object> sourceMap           =   (Map<String, Object>) aryCustomers .get('source');
                String cardId                           =   '';
                if(sourceMap!=null && sourceMap.containsKey('id')){
                    cardId = String.valueOf(sourceMap.get('id')); 
                }
                
                String chargeId                         =  String.valueOf(aryCustomers .get('id'));
                String stateMentdesc                    =  String.valueOf(aryCustomers .get('statement_descriptor'));
                String ChargeDesc                       =  String.valueOf(aryCustomers.get('description'));
                System.debug('----charge desc ---'+ChargeDesc);
                Map<String, Object> requestMap =  new  Map<String, Object>();
                requestMap  = (Map<String, Object>) results.get('request');
                String reqId = String.valueOf(requestMap.get('id'));
                System.debug('invoice----'+invoice);
                System.debug('requestId----'+reqId);
                String matchDesc ;
                // Code to link card and opportunity linking
                List<MC_ProductMapping__c> mapList = new List<MC_ProductMapping__c>();
                mapList = [select id,name,type__c,keyword__c from MC_ProductMapping__c where keyword__c!=null];
                Set<String> prodSet = new Set<String>();
                for(MC_ProductMapping__c mp: mapList){
                    prodSet.add(mp.keyword__c);
                    System.debug('----prodSet---'+prodSet);
                }
                if((order == null || order =='') || (reqId==null || reqId=='')){
                    System.debug('---Line48----');
                    Set<String> excludedValue = new Set<String>();
                    List<Stripe_Product_Mapping__c> lstStripeProd = new  List<Stripe_Product_Mapping__c>();
                    List<Stripe_Product_Mapping__c>  custTempList = new List<Stripe_Product_Mapping__c>();
                    custTempList = [select id,Salesforce__c,Stripe__c,To_Be_Excluded__c from Stripe_Product_Mapping__c where Stripe__c != null];
                    System.debug('---custTempList---'+custTempList);
                    if(custTempList != null && !custTempList.isEmpty()) {
                        System.debug('--custTempList enter if---'+stateMentdesc);
                        for(Stripe_Product_Mapping__c var :custTempList){
                            System.debug('----var.Stripe__c---'+var.Stripe__c);
                            if(stateMentdesc != null  && var.Stripe__c != null && stateMentdesc.contains(var.Stripe__c) ){
                                System.debug('----statement match----');
                                lstStripeProd.add(var);
                                System.debug('--lstStripeProd--'+lstStripeProd);
                            }
                        }
                    }
                    if(lstStripeProd != null && stateMentdesc != null && !lstStripeProd.isEmpty()){
                        for(Stripe_Product_Mapping__c stripeProdCust : lstStripeProd ){
                            if(stripeProdCust.To_Be_Excluded__c != null && stateMentdesc.Contains(stripeProdCust.To_Be_Excluded__c)){
                                excludedValue.add(stripeProdCust.To_Be_Excluded__c);
                                System.debug('---excludedValue----'+excludedValue);
                            }
                        }
                    }
                    String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
                    List<Stripe_Profile__c> lstStripeProf   = [ select id,Customer__c,(select id from Cards__r where Stripe_Card_Id__c=:cardId  ) from Stripe_Profile__c  where Stripe_Customer_Id__c =:customerId AND Stripe_Customer_id__c != null limit 1];
                    System.debug('----lstStripeProf---'+lstStripeProf);
                    //Iterate over the account with.
                    List<Card__c> cardList= new List<Card__c> ();
                    for(Stripe_Profile__c a : lstStripeProf){
                        //For each account object, get the child records using tests__r and iterate over each child records.
                        for(Card__c cardOb : a.Cards__r){
                            cardList.add(cardOb);
                        }
                    }
                    List<opportunity> lstOpportunity = new List<opportunity>();
                    lstOpportunity  = [ SELECT id ,RequestId__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c,Stripe_Order_Id__c FROM opportunity where Stripe_Charge_Id__c  =: chargeId OR Secondary_Stripe_Charge_Id__c =: chargeId limit 1000];
                    System.debug('----line 86---');
                    List<OpportunityLineItem> lstOpportunityLineItems = [SELECT Id,Stripe_charge_id__c FROM OpportunityLineItem where Stripe_charge_id__c =: chargeId limit 1000];
                    system.debug(chargeId);
                    Boolean isSalesforceInitiated = false;
                    Boolean isShopifyInitiated = false;
                    String ProductId;
                    if(!(metaDataMap== null) || !(metaDataMap.isEmpty())){
                        system.debug('---metaDataMap.getIntegration Initiated Fro-------'+metaDataMap);
                        if(metaDataMap.get('Integration Initiated From') =='Salesforce'){
                            isSalesforceInitiated = true;
                        }
                        if (metaDataMap.containsKey('wc_product_id')) {
                            ProductId = String.valueOf(metaDataMap.get('wc_product_id'));
                        }
                        if (metaDataMap.containsKey('shop_name') && metaDataMap.get('shop_name') == 'LadyBoss Shop') {
                            isShopifyInitiated = true;
                        }
                    }
                    System.debug('stateMentdesc--------'+stateMentdesc);
                    if (  !isSalesforceInitiated && (ChargeDesc == null || !ChargeDesc.contains('Subscription creation'))){
                        System.debug('invoiceline99----'+invoice);
                        System.debug('--lstOpportunity--'+lstOpportunity+'----lstOpportunityLineItems----'+lstOpportunityLineItems);
                        if( lstOpportunity.size()==0 && lstOpportunityLineItems.size()==0) {
                            System.debug('----opp create----');
                            Opportunity pm  = new Opportunity();
                            
                            String amt = String.ValueOf(aryCustomers.get('amount'));
                            pm.Amount =decimal.valueOf(amt )*0.01;
                            pm.StageName    = 'Closed Won';
                            string unixDatetime = String.valueOf(aryCustomers.get('created'));
                            if (devRecordTypeId !='' && devRecordTypeId !=null)
                                pm.RecordTypeId = devRecordTypeId ;
                            datetime dt = datetime.newInstance(Long.ValueOf(unixDatetime )* Long.ValueOf('1000'));
                            pm.closeDate = dt.date();
                            if(stateMentdesc!=null ){
                                pm.Name         = stateMentdesc;
                            }else if(metaDataMap.containsKey('product_name')){
                                pm.Name         = String.valueOf(metaDataMap.get('product_name'));
                            }else if (ChargeDesc != null && !isShopifyInitiated) {
                                pm.Name = ChargeDesc.length()>120 ? ChargeDesc.substring(0,120) : ChargeDesc;
                            } else {
                                pm.Name         = String.valueOf(aryCustomers.get('id'));
                            }
                            if (metaDataMap.containsKey('shipping')) {
                                pm.Shipping_Cost__c = Decimal.valueOf(String.valueOf(metaDataMap.get('shipping')));
                            }
                            //if(mc_productMap.containsKey(stateMentdesc)){
                            //  pm.WP_Product_Type__c =mc_productMap.get(stateMentdesc);
                            //}
                            Map<String, opportunity> oppMap = new Map<String, opportunity>();
                            List<opportunity> lstOpportunityExist = new List<opportunity>();
                            if(lstStripeProf.size() > 0){
                                
                                lstOpportunityExist  = [ SELECT id , MC_Email_Sent__c ,Name,WP_Product_Type__c FROM opportunity where contact__c!=null and contact__c =:lstStripeProf[0].Customer__c and MC_Email_Sent__c=true limit 1000];
                            }
                            System.debug('----line128----');
                            for(MC_ProductMapping__c mp :mapList){
                                if(stateMentdesc!=null && stateMentdesc!=''){
                                    System.debug('----line131----');
                                    //if(mp.keyword__c!=null && mp.type__c!=null && stateMentdesc.tolowercase().contains(mp.keyword__c.tolowercase())){
                                    if(mp.keyword__c!=null && mp.type__c!=null ){
                                        System.debug('--------line133------');
                                        String prodKey  =mp.keyword__c.tolowercase();
                                        if(prodKey.contains(',')){
                                            List<string> lstStr =prodKey.split(',');
                                            for(String var : lstStr) {
                                                if(stateMentdesc.tolowercase().contains(var)) {
                                                    pm.WP_Product_Type__c =mp.type__c;
                                                    pm.MC_OpportunityName__c= mp.Name;
                                                    break;
                                                }
                                            }
                                        }
                                        else{
                                            if(stateMentdesc.tolowercase().contains(prodKey)) {
                                                pm.WP_Product_Type__c =mp.type__c;
                                                pm.MC_OpportunityName__c= mp.Name;
                                            }  
                                        }
                                        System.debug('----line147----');
                                        if(!lstOpportunityExist.isempty() && lstOpportunityExist != null){
                                            for(opportunity opp : lstOpportunityExist){
                                                string oppname = opp.name;
                                                if(prodKey.contains(',')){
                                                    List<string> lstStr =prodKey.split(',');
                                                    for(String var : lstStr) {
                                                        if(oppname.tolowercase().tolowercase().contains(var) && stateMentdesc.tolowercase().contains(var)) {
                                                            pm.MC_Email_Sent__c = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else if(oppname.tolowercase().contains(mp.keyword__c.tolowercase()) && stateMentdesc.tolowercase().contains(prodKey)){
                                                    pm.MC_Email_Sent__c = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            pm.CardId__c =  cardId;
                            system.debug( pm.CardId__c); 
                            pm.RequestId__c = String.valueOf(requestMap.get('id'));
                            pm.Charge_Description__c=String.valueOf(aryCustomers.get('description'));
                            if (ProductId != null) {
                                pm.WC_Product_Id__c = ProductId;
                            }
                            if (metaDataMap.containsKey('wc_order_number')) {
                                pm.WC_Order_Number__c = String.valueOf(metaDataMap.get('wc_order_number'));
                                pm.WC_Order_Id__c = String.valueOf(metaDataMap.get('wc_order_id'));
                                pm.Coupon_Code__c = string.valueOf(metaDataMap.get('couponcode'));
                            }
                            system.debug( pm.RequestId__c); 
                            pm.description =customerId ;
                            pm.CustomerID__c  = customerId ;
                            if(lstStripeProf.size () > 0) {
                                System.debug('----profile---');
                                if( lstStripeProf[0].Customer__c!=null){
                                    pm.Contact__c = lstStripeProf[0].Customer__c;
                                }
                                if( lstStripeProf.size() > 0 && lstStripeProf[0].id!=null ){
                                    pm.Stripe_Profile__c = lstStripeProf[0].id;
                                }
                            }
                            //pm.Subscription__c=String.valueOf(aryCustomers .get('customer'));
                            if(cardList.size () > 0){
                                pm.card__c =cardList[0].id;
                            }
                            pm.Success_Failure_Message__c= String.valueOf(results.get('type'));
                            pm.External_ID__c = chargeId;
                            pm.Stripe_Charge_Id__c = chargeId;
                            if(pm.Success_Failure_Message__c=='charge.succeeded') {
                                pm.Stripe_Message__c = 'Success';
                                pm.Paid__c =true;
                            }else if (pm.Success_Failure_Message__c=='charge.refunded') {
                                pm.Stripe_Message__c ='Success';
                            }
                            else
                            {
                                pm.Stripe_Message__c ='Failure';
                                
                            }
                            pm.Initiated_From__c = 'Stripe';
                            System.debug('--results.get---'+results.get('type'));
                            if (metaDataMap.containsKey('TaxPercentage') && metaDataMap.get('TaxPercentage') != null) {
                                pm.Tax_Percentage__c = Decimal.valueOf(String.valueOf(metaDataMap.get('TaxPercentage')));
                                pm.Core_Amount__c = Decimal.valueOf(String.valueOf(metaDataMap.get('Core Amount')));
                                pm.Tax__c = Decimal.valueOf(String.valueOf(metaDataMap.get('Tax')));
                            }                            
                            if(String.valueOf(results.get('type'))=='charge.succeeded') {
                                insert pm;
                                System.debug('-----opportunity inserted-----'+pm);
                                if(cardList.size () <= 0  && pm.card__c==null){
                                    Card_Opp_Temporary__c cd = new Card_Opp_Temporary__c();
                                    if(String.Valueof(sourceMap.get('id'))!=null && String.Valueof(sourceMap.get('id'))!='')
                                        cd.CardId__c = String.Valueof(sourceMap.get('id'));
                                    if(chargeId!=null && chargeId!='')
                                        cd.ChargeId__c = chargeId;
                                    if(invoice !=null && invoice !='')
                                        cd.Invoice_Id__c  = invoice ;
                                    insert cd;
                                }
                            }
                        }else {
                            if(lstOpportunity.size()>0){
                                if (lstOpportunity[0].Scheduled_Payment_Date__c==null ) {
                                    if(requestMap .get('id')!=null){
                                        //List<Opportunity> oppReqObj = [select id, ownerId,RequestId__c from opportunity where RequestId__c=:String.valueOf(requestMap .get('id'))];
                                        //if(oppReqObj.size() > 0){
                                        //lstOpportunity[0].OwnerId= oppReqObj[0].ownerId;
                                        //}
                                        lstOpportunity[0].RequestId__c = String.valueOf(requestMap .get('id'));
                                        
                                    }
                                    if(lstStripeProf.size () > 0 && lstStripeProf[0].id!=null) {
                                        lstOpportunity[0].Stripe_Profile__c = lstStripeProf[0].id;
                                    }
                                    if(cardList.size () > 0 && cardList[0].id!=null)
                                        lstOpportunity[0].card__c =cardList[0].id;
                                    if(lstOpportunity.size() > 0)
                                        update lstOpportunity; 
                                }
                            }
                        }
                    }
                    System.debug('----lstStripeProf----'+lstStripeProf);
                    if(lstStripeProf.size() > 0)  {
                        System.debug('---Line233---');
                        List<Contact> conList = [select id,Product__c,Website_Products__c from Contact where id =: lstStripeProf[0].Customer__c limit 1];
                        if(stateMentdesc!=null && conList.size() > 0  && lstStripeProd.size()>  0 ) {
                            System.debug('---Line236----');
                            List<Stripe_Description__c> stripeDesc = new List<Stripe_Description__c>();
                            stripeDesc  = [Select Name,Description__c,Invoice_Number__c from Stripe_Description__c];
                            System.debug('----Line239---');
                            if(stripeDesc!= null && !stripeDesc.isEmpty()){
                                System.debug('----Line241---');
                                for(Stripe_Description__c stDes : stripeDesc){
                                    if(ChargeDesc != null && stDes.Description__c != null && ((stDes.Invoice_Number__c == null && ChargeDesc.contains(stDes.Description__c)) || (stDes.Invoice_Number__c != null && ChargeDesc.contains(stDes.Description__c) &&  ChargeDesc.contains(stDes.Invoice_Number__c)))) {
                                        System.debug('----Line244---'+stDes.Description__c+'--ChargeDesc--'+ChargeDesc);
                                        System.debug('---lstStripeProd[0].Salesforce__c---'+lstStripeProd[0].Salesforce__c+'--empty---'+excludedValue.isEmpty()+'--size---'+excludedValue.size());
                                        if( conList[0].Website_Products__c!=null && excludedValue.isEmpty() && excludedValue.size() <= 0){
                                            System.debug('-----Line246----');
                                            if(lstStripeProd[0].Salesforce__c != null && !conList[0].Website_Products__c .contains(lstStripeProd[0].Salesforce__c)){
                                                system.debug('----Line248---');
                                                conList[0].Website_Products__c += ';'+lstStripeProd[0].Salesforce__c;
                                            }
                                        }
                                        
                                        else if(lstStripeProd[0].Salesforce__c != null && excludedValue.isEmpty() && excludedValue.size() == 0)
                                        {
                                            System.debug('-----Line254---');
                                            conList[0].Website_Products__c = lstStripeProd[0].Salesforce__c;
                                            System.debug('--conList[0].Website_Products__c---'+conList[0].Website_Products__c);
                                        }
                                    }
                                }
                                update conList;
                                System.debug('--listupdated---'+conList);
                            }
                            
                            
                        }
                        else if(conList.size()>0 && lstStripeProd.isEmpty()){
                            System.debug('---Line262-----');
                            String chargeDescription = String.valueOf(aryCustomers.get('description'));
                            boolean isExactKeywordmatch = false;
                            Integer i;
                            List<Stripe_Product_Mapping__c> StripeProd = new  List<Stripe_Product_Mapping__c>();
                            StripeProd = [select id,Salesforce__c,Stripe__c,Description__c,To_Be_Excluded__c,Exact_Keyword__c from Stripe_Product_Mapping__c];
                            System.debug('-----Line269---');
                            if(StripeProd != null && StripeProd.size() > 0 && chargeDescription != null && conList.size() > 0) {
                                System.debug('----Line271----');
                                Set<String> excludeKeyCharge = new Set <String>();
                                for(Stripe_Product_Mapping__c prodMap : StripeProd) {
                                    isExactKeywordmatch=false;
                                    if(prodMap.Exact_Keyword__c != null){
                                        System.debug('---Line276---');
                                        if(chargeDescription.contains(prodMap.Exact_Keyword__c)){
                                            System.debug('-----Line278----');
                                            if(stateMentdesc != null){
                                                System.debug('-----Line280---');
                                                if(prodMap.To_Be_Excluded__c != null && stateMentdesc.contains(prodMap.To_Be_Excluded__c) ) { 
                                                    
                                                    excludeKeyCharge.add(prodMap.To_Be_Excluded__c);
                                                    System.debug('---excludeKeyCharge----'+excludeKeyCharge);
                                                }
                                            }
                                            if(prodMap.Description__c != null){
                                                System.debug('-----Line288----');
                                                if(prodMap.Description__c.contains(',')){
                                                    System.debug('----Line290---');
                                                    List<String> descList= new List<String>();
                                                    descList= prodMap.Description__c.split(',');
                                                    for( i = 0 ; i < descList.size(); i++){
                                                        if(chargeDescription.contains(descList[i])){
                                                            matchDesc = descList[i];
                                                            isExactKeywordmatch = true;
                                                            break;
                                                            System.debug('--matchDesc--'+matchDesc+'--isExactKeywordmatch--'+isExactKeywordmatch);
                                                        }
                                                    }
                                                }
                                                else if(chargeDescription.contains(prodMap.Description__c)){
                                                    System.debug('----Line303---');
                                                    matchDesc=prodMap.Description__c;
                                                    isExactKeywordmatch = true;                                           
                                                }
                                            }
                                        }
                                        if(matchDesc != null){
                                            System.debug('----Line310---');
                                            if(chargeDescription.contains(matchDesc)) {
                                                System.debug('----Line312---'+chargeDescription+'---matchDesc---'+matchDesc);
                                                List<Stripe_Description__c> stripeDesc = new List<Stripe_Description__c>();
                                                stripeDesc  = [Select Name,Description__c,Invoice_Number__c from Stripe_Description__c];
                                                if(stripeDesc!= null && !stripeDesc.isEmpty()){
                                                    System.debug('----Line316---');
                                                    for(Stripe_Description__c stDes : stripeDesc){
                                                        if(chargeDescription != null && stDes.Description__c != null && ((stDes.Invoice_Number__c == null && chargeDescription.contains(stDes.Description__c)) ||(stDes.Invoice_Number__c != null && chargeDescription.contains(stDes.Description__c) &&  chargeDescription.contains(stDes.Invoice_Number__c)))) {
                                                            System.debug('----Line319---');
                                                            if(excludeKeyCharge.isEmpty() && excludeKeyCharge.size() == 0 ){ 
                                                                System.debug('----Line321---');
                                                                if( conList[0].Website_Products__c != null && isExactKeywordmatch == true){
                                                                    System.debug('----Line323---');
                                                                    if(!conList[0].Website_Products__c.contains(prodMap.Salesforce__c)){
                                                                        conList[0].Website_Products__c += ';'+prodMap.Salesforce__c;
                                                                        System.debug('---conLis----'+conList[0].Website_Products__c);
                                                                    }
                                                                }else if(isExactKeywordmatch == true){
                                                                    conList[0].Website_Products__c = prodMap.Salesforce__c;
                                                                    System.debug('---Line330 conlist---');
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                    } 
                                }
                                update conList;
                            }
                        }
                    }
                    
                    
                }else if(order != null && chargeid != null){
                    List<Opportunity> oppList = [ SELECT id,Scheduled_Payment_Date__c,Stripe_Charge_Id__c,Stripe_Order_Id__c, Sales_Person__c,Contact__r.firstname, Contact__r.lastname, 
                                                 Contact__r.email, Contact__r.phone,User_to_give_credit__c FROM opportunity where (Stripe_Charge_Id__c  =: chargeId 
                                                                                                                                   OR Secondary_Stripe_Charge_Id__c =: chargeId) AND Stripe_Order_Id__c !=null limit 1];
                    if(oppList.size()>0){
                        for(Opportunity opp:oppList){
                            OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate(opp);
                        }
                    }
                }
                //Failed Payment Cases Status Close
                if(aryCustomers.get('description') !=null && String.valueOf(aryCustomers.get('description')) != null && String.valueOf(aryCustomers.get('description')) != ''){
                    String descriptionVal = String.valueOf(aryCustomers.get('description'));
                    Integer index = descriptionVal.lastIndexOf('-');
                    if(index != -1){
                        descriptionVal = descriptionVal.substring(0, index);
                    }
                    List<Case> failedPaymentCases = [select id,Status,Subject from Case 
                                                     where Status = 'Open' 
                                                     AND Subject LIKE : '%'+descriptionVal+'%'
                                                     AND Subject LIKe : '%Failed payment%' AND ( Subject LIKE : '%Payment for invoice%' or Subject LIKE : '%Invoice%' )];
                    if(failedPaymentCases.size() >0) {
                        for(Case cs:failedPaymentCases){
                            cs.status = 'Closed';
                        }
                        update failedPaymentCases;
                    }
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripeCharge_Service',
                        'createCustomer',
                        CustomerIde
                    )
                );
                if(Test.isRunningTest())  
                    Integer intTest = 1/0;    
            }
            catch(Exception ex) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeCharge_Service',
                        'createCustomer',
                        CustomerIde,
                        ex
                    )
                );
            }
        }
    }
    
}