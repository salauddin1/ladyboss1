@isTest
public class RefundSubscriptionChargeApex_Test {
       
    public static testMethod void test3(){
        //system.Test.setMock(HttpCalloutMock.class, new RefundSubCharge_Mock());
        
    contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Amount = 28626;
        opp.Refund_Amount__c =28626;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 23700;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        insert ol;
        
        Test.StartTest();
        RefundSubscriptionChargeApex.getOpportunity(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund(co.Id);
       // RefundSubscriptionChargeApex.getUpdate(co.Id,opp.Id,ol.Id,2,'duplicate');
        //RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,2,'duplicate');
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,2,'fraudulent');
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,2,'Requested_by_customer');
        //RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,2,'other');
       // RefundSubscriptionChargeApex.subscriptionChargeRefund(co.Id,opp.Id,ol.Id,23700,'duplicate');
    Test.stopTest();
    }
    public static testMethod void test1(){
        //system.Test.setMock(HttpCalloutMock.class, new MockCharge());
        
    contact co = new contact();
        co.LastName ='vijay';
        //co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Amount = 7690;
        opp.Refund_Amount__c =7690;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='succeeded';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 7690;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.s ='ch_1DafZmBwLSk1v1ohUH1yKzvh';
        insert ol;
        
        Test.StartTest();
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,7690,'duplicate');
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,7690,'fraudulent');
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,7690,'Requested_by_customer');
       // RefundSubscriptionChargeApex.chargeRefund(co.Id,opp.Id,ol.Id,7690,'other');
       Test.stopTest();
    }
    public static testMethod void test2(){
        //system.Test.setMock(HttpCalloutMock.class, new MockCharge());
        
    contact co = new contact();
        co.LastName ='vijay';
        //co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Amount = 7690;
        opp.Refund_Amount__c =7690;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        //opp.Status__c ='succeeded';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 7690;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.s ='ch_1DafZmBwLSk1v1ohUH1yKzvh';
        insert ol;
        
        Test.StartTest();
        RefundSubscriptionChargeApex.getOpportunity11(co.Id);
        RefundSubscriptionChargeApex.getOpportunity1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund1(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity2(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem2(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund2(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity3(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem3(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund3(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity4(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem4(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund4(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity5(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem5(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund5(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity6(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem6(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund6(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund1(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity7(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem7(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund7(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity8(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem8(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund8(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund1(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity9(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem9(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund9(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity10(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem10(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund10(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity1(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem11(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund11(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity12(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem12(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund12(co.Id);
        
         RefundSubscriptionChargeApex.getOpportunity13(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem13(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund13(co.Id);
          RefundSubscriptionChargeApex.getOpportunity14(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem14(co.Id);
        RefundSubscriptionChargeApex.getOpportunityRefund14(co.Id);
        
        RefundSubscriptionChargeApex.getOpportunityLineItem16(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem17(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem18(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem19(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem20(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem21(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem22(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem23(co.Id);
        RefundSubscriptionChargeApex.getOpportunityLineItem24(co.Id);
        RefundSubscriptionChargeApex.dummy();
        
       Test.stopTest();
    }
}