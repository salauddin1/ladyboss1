global class UpdateSubscriptionOpportunityLineitems implements  Database.AllowsCallouts, Database.Batchable<sObject> {
	String condition;

	global UpdateSubscriptionOpportunityLineitems(String condition){
		this.condition = condition;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select id,OpportunityId,Start__c,End__c,Success_Failure_Message__c,Subscription_Id__c,Status__c,Number_Of_Payments__c FROM OpportunityLineItem WHERE '+condition);
	}

   	global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope) {
   		string opId;
		try {
			List<OpportunityLineItem> opliToUpdate = new List<OpportunityLineItem>();
			for(OpportunityLineItem opli:scope){
				opId = opli.Id;
				Subscriptions sub = Subscriptions.getSubscription(opli.Subscription_Id__c);
				if (sub!=null) {
					if(sub.status=='canceled'){
						opli.Status__c = 'InActive';
					}else if (sub.status=='active') {
						opli.Status__c = 'Active';
					}
					opli.Number_Of_Payments__c = Invoices.getInvoice(sub.id);
					Datetime dT = DateTime.newInstance(sub.current_period_start * 1000);
					opli.Start__c = date.newinstance(dT.year(), dT.month(), dT.day());		
					Datetime dT1 = DateTime.newInstance(sub.current_period_end * 1000);
					opli.End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
					
					opliToUpdate.add(opli);
				}				
			}
			if(opliToUpdate.size()>0){
				update opliToUpdate;
			}
			if (Test.isRunningTest()) {
				Integer i = 1/0;
			}
		} catch(Exception ex) {
			ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                new ApexDebugLog.Error(
                 'UpdateSubscriptionOpportunityLineitems',
                 'Batch',
                 opId,
                 ex
                 )
                 );
                
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}