global class FuturePaymentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select id, AccountId, Account.NMI_External_ID__c, Account.External_ID__c, Total__c, Billing__c, Days_Until_Due__c, currency__c, Amount, Payment_Type__c from Opportunity where Payment_Type__c = \'Nmi\' and Payment_Date__c = TODAY and Paid__c = false');
    }    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        Map<Id, List<OpportunityLineItem>> oppOlisMap = new Map<Id, List<OpportunityLineItem>>();
        for(Opportunity opportunity:scope) oppOlisMap.put(opportunity.id, new List<OpportunityLineItem>());
        for(OpportunityLineItem oli:[select id, opportunityId, Product2.ProductCode, Product2.External_ID__c, Product2.Name, Quantity, UnitPrice, PricebookEntry.Product2.Type__c From OpportunityLineItem WHERE OpportunityId=:oppOlisMap.keySet()]) oppOlisMap.get(oli.opportunityId).add(oli);
        for(Opportunity opportunity:scope){
            try{
                if(!Test.isRunningTest()) PaymentGatewayNMI.HttpPayment(opportunity, oppOlisMap.get(opportunity.id));
            }
            catch(CalloutException e){
                System.debug('Error Executing FuturePaymentBatch');
            }
        }
    }  
    global void finish(Database.BatchableContext BC){
    }
}