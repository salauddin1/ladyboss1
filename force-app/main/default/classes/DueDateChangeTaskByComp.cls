//Controller to changes the checkbox values from the "DueDateChangeTaskByComp" component.
public class DueDateChangeTaskByComp {
    
    @AuraEnabled
    public static List<Task> Changestatus(String recordId,String fieldName,String CheckedValue) {
        List<Task> taskList = new List<Task>();
        AddDaysToDueDate__c myCustomSetting;
        Decimal theFieldValue;
        //Get the task list to update the checkbox checked on the comp
        taskList = [SELECT Id, Owner.Name, Subject,Status,No_Answer__c,ActivityDate,Closed_Sale__c,Do_Not_Call__c,Reordered_Online__c,Not_Time_Yet__c,Not_interested__c from Task where Id =: recordId];
        //Iterates over the task list to update status,Activitydate and checkbox values
        for(Task ListTask : taskList) {
            if(fieldName == 'Closed_Sale__c' ) {
                if(CheckedValue == 'true') {
                    ListTask.Closed_Sale__c = true;
                    ListTask.Status = 'Completed';
                }
                else{
                    ListTask.Closed_Sale__c = false;
                }
            }
            else if(fieldName == 'Reordered_Online__c' ) {
                if(CheckedValue == 'true') {
                    ListTask.Reordered_Online__c = true;
                    ListTask.Status = 'Completed';
                }
                else{
                    ListTask.Reordered_Online__c = false;
                }
            }
            else if(fieldName == 'Do_Not_Call__c' ) {
                if(CheckedValue == 'true') {
                    ListTask.Do_Not_Call__c = true;
                    ListTask.Status = 'Completed';
                }
                else{
                    ListTask.Do_Not_Call__c = false;
                }
            }
            else if(fieldName == 'Not_interested__c' ) {
                if(CheckedValue == 'true') {
                    ListTask.Not_interested__c = true;
                    ListTask.Status = 'Completed';
                }
                else{
                    ListTask.Not_interested__c = false;
                }
            }
            else if(fieldName == 'Not_Time_Yet__c' ) {
                if(CheckedValue == 'true') {
                    ListTask.Not_Time_Yet__c = true;
                }
                else{
                    ListTask.Not_Time_Yet__c = false;
                }
            }
            
            else if(fieldName == 'No_Answer__c') {
                if(CheckedValue == 'true') {
                    ListTask.No_Answer__c = true;
                }
                else { 
                    ListTask.No_Answer__c = false;
                    
                }
                
            }
            else if(fieldName == 'Activitydate'){	
                if(CheckedValue != null && CheckedValue !=''){
                       ListTask.ActivityDate= date.valueOf(CheckedValue); 
                }
             }
        }
        
        update taskList ;
        System.debug('-----------taskList updated-----------'+taskList);
        return taskList;
    }
    // To show the values of the task checkboxes on the component. 
    @AuraEnabled
    public static List<Task> getAllTask(String recordId) {
        List<Task> taskList = new List<Task>();
        taskList = [SELECT Id, Owner.Name, ActivityDate,Subject,Status,No_Answer__c,Closed_Sale__c,Do_Not_Call__c,Reordered_Online__c,Not_Time_Yet__c,Not_interested__c from Task where Id =: recordId];
        System.debug('----------taskList-----------'+taskList);
        return taskList;
    }
    
}