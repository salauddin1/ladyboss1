public class AddNoteToStripe {
    @AuraEnabled
    public Static String findObjectAPIName( String recordId ){
        if(recordId == null){
            return null;
        }
        String objectAPIName = '';
        String keyPrefix = recordId.substring(0,3);
        for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
            String prefix = obj.getDescribe().getKeyPrefix();
            if(prefix == keyPrefix){
                objectAPIName = obj.getDescribe().getName();
                break;
            }
        }
        System.debug('=================='+objectAPIName);
        return objectAPIName;
        
    }
    @AuraEnabled
    public static Boolean setButtonTag(String recordId,String recordName){
        List<Customer_Note__c> custNoteList = new List<Customer_Note__c>();
        Boolean check = false ;
        if(recordId != null && recordId != '' && recordName != null && recordName != '' ){
            if(recordName == 'Contact'){
                custNoteList = [select id,Customer_Note__c,Customer_key__c,Contact__c from Customer_Note__c where Contact__c =:recordId AND Contact__c != null AND Stripe_Profile__c != null];
            }
            else if(recordName == 'Case'){
                custNoteList = [select id,Customer_Note__c,Customer_key__c,Case__c from Customer_Note__c where Case__c =:recordId AND Case__c != null AND Stripe_Profile__c != null];
            }
            if(custNoteList != null && !custNoteList.isEmpty()){
                for( Customer_Note__c   custNote : custNoteList){
                    if(custNote.Customer_Note__c != null && custNote.Customer_Key__c != null && custNote.Customer_Note__c != ''  && custNote.Customer_Key__c != ''){
                        if(custNote.Customer_Note__c == 'true' && custNote.Customer_Key__c == 'lifetime'){
                            check = true ;
                            break ;
                        }
                    }
                }
            }  
        }
        return check ;    
    }
    @AuraEnabled
    public static list<Opportunity> getStripeLineItem(String recordId,String recordName){
        list<Opportunity> opp = new list<Opportunity>();
        Case cs = new Case();
        if(recordId != null && recordId != '' && recordName != null && recordName != ''){
            if(recordName == 'Contact'){
                opp = [select id , name,(select id,Product2.name from OpportunityLineItems),Card__r.CustomerID__c from Opportunity where Contact__c =:recordId];
            }
            else if(recordName == 'Case'){
                cs = [select id,ContactId from Case where id =: recordId limit 1];
                opp = [select id , name,(select id,Product2.name from OpportunityLineItems),Card__r.CustomerID__c from Opportunity where Contact__c =:cs.ContactId];
            }
        }
        return opp;
    }
    
    
    @AuraEnabled
    public static list<Stripe_Profile__c> getStripeCustomer(String recordId,String recordName){
        list<Stripe_Profile__c> str = new list<Stripe_Profile__c>();
        Case cs = new Case();
        if(recordId != null && recordId != '' && recordName != null && recordName != '' ){
            if(recordName == 'Contact'){
                str = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:recordId];
            }
            else If(recordName == 'Case'){
                cs = [select id,ContactId from Case where id =: recordId limit 1];
                str = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:cs.ContactId];  
            }
        }
        return str;
    }
    
    @AuraEnabled
    public static Customer_Note__c updateObject(String metaVal,String recordId,String custNoteId,String StripeId,String recordName){
        Customer_Note__c cust = new Customer_Note__c();
        Stripe_Profile__c str = new Stripe_Profile__c();
        Case cs = new Case();
        if(custNoteId != null && custNoteId != '' && recordName != null && recordName != '' ){
            cust = [select id,Customer_Note__c,Customer_key__c,Stripe_Profile__c from Customer_Note__c where id =:custNoteId limit 1];
            if(recordName == 'Contact'){
                str = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:recordId and id =: cust.Stripe_Profile__c limit 1]; 
            }
            else if(recordName == 'Case'){
                cs = [select id,ContactId from Case where id =: recordId limit 1];
                str = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:cs.ContactId and id =: cust.Stripe_Profile__c limit 1];   
            }
        }
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.stripe.com/v1/customers/'+Str.Stripe_Customer_Id__c);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        Blob headerValue2 = Blob.valueOf(StripeAPI.ApiKey + ':');
        
        system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader2 = 'Basic ' +EncodingUtil.base64Encode(headerValue2);
        req.setHeader('Authorization', authorizationHeader2);
        String body = '&metadata['+cust.Customer_key__c+']='+metaVal+'&';
        
        
        req.setBody(body);
        Http conn = new Http();
        HttpResponse resp = new HttpResponse();
        resp = conn.send(req);
        if(resp.getBody() != null) {
            System.debug('----resp-----'+resp.getBody());
        }
        return cust;
    }
    
    @AuraEnabled
    public static list<object> updateCustomerMetdata(String metaVal,string custId,string recordId,String StripeId,String recordName) {
        if(custId != null && custId != ''){
            HttpRequest http2 = new HttpRequest();
            //System.debug('subscriptionId===='+subscriptionId);
            http2.setEndpoint('https://api.stripe.com/v1/customers/'+custId );
            http2.setMethod('POST');
            Blob headerValue2 = Blob.valueOf(StripeAPI.ApiKey + ':');
            system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
            String authorizationHeader2 = 'BASIC ' +
                EncodingUtil.base64Encode(headerValue2);
            http2.setHeader('Authorization', authorizationHeader2);
            String body = '&metadata['+metaVal+']='+metaVal+'&'; 
            
            http2.setBody(body );
            String response2;
            Integer statusCode;
            Http con2 = new Http();
            HttpResponse hs2 = new HttpResponse();
            if(!Test.isRunningTest()){
                try {
                    hs2 = con2.send(http2);
                    response2 = hs2.getBody();
                    statusCode = hs2.getStatusCode();
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
            }else{
                statusCode=200;
                response2='{"customer":"test"}';
            }
            
            
            System.debug('----'+response2);
            System.debug('--**--'+statusCode);
            if(statusCode == 200){
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response2);
                map<string,object> obj = (map<string,object>)results.get('metadata'); 
                list<object> listSt = new list<object>();
                if(obj != null){
                    for(String st : obj.keySet()){
                        listSt.add(obj.get(st));
                    }
                }
                
                return listSt;
            } 
        }
        return null;
    }   
    
    
    @AuraEnabled
    public static list<object> getAllCustomer(String conId,String recordName) {
        list<Customer_Note__c> custNote = new list<Customer_Note__c>();
        Case cs = new Case();
        if(conId != null && conId != '' && recordName != null && recordName != ''){
            if(recordName == 'Contact'){ 
                custNote = [select id,Contact__c,Customer_Note__c,Stripe_Profile__c from Customer_Note__c where Contact__c =: conId  AND Customer_key__c != 'lifetime'];
            }
            else If(recordName == 'Case'){
                cs = [select id,ContactId from Case where id =: conId limit 1];
                custNote = [select id,Contact__c,Customer_Note__c,Stripe_Profile__c from Customer_Note__c where Contact__c =: cs.ContactId  AND Customer_key__c != 'lifetime']; 
            }
        }
        return custNote;  
    }
    
    
    @AuraEnabled
    public static Boolean setLifeTimeCust(String recordId,String recordName){
        try{
            Boolean checkRes = false ;
            if(recordId != null && recordId != '' && recordName != null && recordName != ''){
                List<Stripe_Profile__c> strList = new List<Stripe_Profile__c>();
                list<Customer_Note__c> custNote = new list<Customer_Note__c>();
                
                if(recordName == 'Contact'){ 
                    strList = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:recordId and Customer__c != null and Stripe_Customer_Id__c != null]; 
                }
                else If(recordName == 'Case'){
                    strList = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Case__c =:recordId and Case__c != null and Stripe_Customer_Id__c != null]; 
                }
                Set<ID> getId = new Set<ID>();
                if(strList != null && !strList.isEmpty()) {
                    for(Stripe_Profile__c strPro : strList) {
                        getId.add(strPro.id);    	
                    }
                }
                else{
                    return false ;
                }
                if(getId != null && !getId.isEmpty()) {
                    custNote = [select id,Contact__c,Customer_Note__c,Stripe_Profile__c from Customer_Note__c where Stripe_Profile__c =: getId  AND Stripe_Profile__c != null  AND Customer_key__c =: 'lifetime' and Customer_Note__c =: 'true']; 
                    if(custNote != null && !custNote.isEmpty()) {
                        for(Customer_Note__c note : custNote){
                            if(note.Stripe_Profile__c != null && note.Customer_Note__c != null && note.Customer_Key__c != null && note.Customer_Note__c != ''  && note.Customer_Key__c != ''){
                                if(note.Customer_Note__c == 'true' && note.Customer_Key__c == 'lifetime'){
                                    if(getId.contains(note.Stripe_Profile__c)){
                                        getId.remove(note.Stripe_Profile__c);        
                                    }
                                }
                            }
                        }
                    }	
                }
                
                List<Stripe_Profile__c> strListCustId = new List<Stripe_Profile__c>();
                Set<String> getIdCust = new Set<String>();
                
                if(getId != null && !getId.isEmpty()) {
                    strListCustId = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where id =:getId and Stripe_Customer_Id__c != null]; 
                    if(strListCustId != null && !strListCustId.isEmpty()) {
                        for(Stripe_Profile__c strProcust : strListCustId) {
                            getIdCust.add(strProcust.Stripe_Customer_Id__c);    	
                        }
                    }
                    
                }
                System.debug('-------getIdCust------'+getIdCust);
                if(getIdCust != null && !getIdCust.isEmpty()) {
                    for(String setId : getIdCust) {
                        String Body = '&metadata[lifetime]=true&';
                        HttpResponse resp = new HttpResponse();
                        HttpRequest reqs = new HttpRequest();
                        reqs.setEndpoint('https://api.stripe.com/v1/customers/'+setId);
                        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                        String authorizationHeader = 'BASIC ' +
                            EncodingUtil.base64Encode(headerValue);
                        
                        reqs.setHeader('Authorization', authorizationHeader);
                        reqs.setHeader('Accept', 'application/json');
                        reqs.setMethod('POST');
                        reqs.setBody(Body);
                        Http ht = new Http();
                        resp = ht.send(reqs);
                        System.debug('-----resp.getBody()-------'+resp.getBody());
                        checkRes = true ;
                    }
                }
            }
            return checkRes ;
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('AddNoteToStripe','setLifeTimeCust : '+recordName, recordId,ex));
            return false;
        }
    }
    @AuraEnabled
    public static Boolean removeLifeTimeCust(String recordId,String recordName){
        try{
            Boolean checkRes = false ;
            if(recordId != null && recordId != '' && recordName != null && recordName != ''){
                List<Stripe_Profile__c> strList = new List<Stripe_Profile__c>();
                list<Customer_Note__c> custNote = new list<Customer_Note__c>();
                
                if(recordName == 'Contact'){ 
                    strList = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Customer__c =:recordId and Customer__c != null and Stripe_Customer_Id__c != null]; 
                }
                else If(recordName == 'Case'){
                    strList = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where Case__c =:recordId and Case__c != null and Stripe_Customer_Id__c != null]; 
                }
                Set<ID> getId = new Set<ID>();
                if(strList != null && !strList.isEmpty()) {
                    for(Stripe_Profile__c strPro : strList) {
                        getId.add(strPro.id);    	
                    }
                }
                else{
                    return false ;
                }
                if(getId != null && !getId.isEmpty()) {
                    custNote = [select id,Contact__c,Customer_Note__c,Stripe_Profile__c from Customer_Note__c where Stripe_Profile__c =: getId  AND Stripe_Profile__c != null  AND Customer_key__c =: 'lifetime' and Customer_Note__c =: 'false']; 
                    if(custNote != null && !custNote.isEmpty()) {
                        for(Customer_Note__c note : custNote){
                            if(note.Stripe_Profile__c != null && note.Customer_Note__c != null && note.Customer_Key__c != null && note.Customer_Note__c != ''  && note.Customer_Key__c != ''){
                                if(note.Customer_Note__c == 'false' && note.Customer_Key__c == 'lifetime'){
                                    if(getId.contains(note.Stripe_Profile__c)){
                                        getId.remove(note.Stripe_Profile__c);        
                                    }
                                }
                            }
                        }
                    }	
                }
                
                List<Stripe_Profile__c> strListCustId = new List<Stripe_Profile__c>();
                Set<String> getIdCust = new Set<String>();
                
                if(getId != null && !getId.isEmpty()) {
                    strListCustId = [select id,Name,Stripe_Customer_Id__c, Customer__c from Stripe_Profile__c where id =:getId and Stripe_Customer_Id__c != null]; 
                    if(strListCustId != null && !strListCustId.isEmpty()) {
                        for(Stripe_Profile__c strProcust : strListCustId) {
                            getIdCust.add(strProcust.Stripe_Customer_Id__c);    	
                        }
                    }
                    
                }
                System.debug('-------getIdCust------'+getIdCust);
                if(getIdCust != null && !getIdCust.isEmpty()) {
                    for(String setId : getIdCust) {
                        String Body = '&metadata[lifetime]=false&';
                        HttpResponse resp = new HttpResponse();
                        HttpRequest reqs = new HttpRequest();
                        reqs.setEndpoint('https://api.stripe.com/v1/customers/'+setId);
                        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                        String authorizationHeader = 'BASIC ' +
                            EncodingUtil.base64Encode(headerValue);
                        
                        reqs.setHeader('Authorization', authorizationHeader);
                        reqs.setHeader('Accept', 'application/json');
                        reqs.setMethod('POST');
                        reqs.setBody(Body);
                        Http ht = new Http();
                        resp = ht.send(reqs);
                        System.debug('-----resp.getBody()-------'+resp.getBody());
                        checkRes = true ;
                    }
                }
            }
            return checkRes ;
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('AddNoteToStripe','removeLifeTimeCust : '+recordName, recordId,ex));
            return false;
        }
    }
}