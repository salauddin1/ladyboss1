public  class WebhookTableHandler {
	public static void addressHandler(Address__c address){
       
        Webhook_Table__c wt=new Webhook_Table__c();
        wt.Salesforce_Record_Id__c = address.Id;
        wt.Salesforce_Record_Type__c = 'Address__c';
        wt.Customer_Id__c =  address.CustomerID__c;
        insert wt;   
	}

	public static void cardHandler(Card__c card){
       
        Webhook_Table__c wt1=new Webhook_Table__c();
        wt1.Salesforce_Record_Id__c = card.Id;
        wt1.Salesforce_Record_Type__c = 'Card__c';
        
        wt1.Customer_Id__c =  card.CustomerID__c;
        insert wt1;
	}
}