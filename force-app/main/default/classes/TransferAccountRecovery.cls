public class TransferAccountRecovery{
@AuraEnabled
    public static Case getCase(Id caseId){
       
        return([Select id,Status, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId]);
    }  
    @AuraEnabled
    public static Case transferOwnership(Id caseId){
        
        try{
           Group gr = [SELECT CreatedDate,DeveloperName FROM Group where DeveloperName =: 'Account_Recovery'];
        
           Case caseToUpdate = [Select id,IsFailed_Payment__c,Status,OwnerId, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId];
              if( gr!=NULL && caseToUpdate !=null ){
                 if(gr.id!=null && caseToUpdate.IsFailed_Payment__c){
                     caseToUpdate.OwnerId = gr.id;
                      update caseToUpdate;
                  }
                } 
        
        return([Select id,Status, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId]);    
        }catch(Exception e){
            throw new AuraHandledException('No queue  found '+ e.getMessage()); 
            //return null;
        }
        
    }   

}