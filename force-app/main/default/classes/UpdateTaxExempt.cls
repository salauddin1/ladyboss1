global class UpdateTaxExempt implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    private List<String> stateList = new List<String>{'AK','Alaska','DE','Delaware','OR','Oregon','M','MI','MICHIGAN','MT','Montana','New Hampshire','NH','Vermont','VT','Wyoming','WY','NY','New York','New York,NY','NJ','New Jersey','TEXAS','TX,TX','TX','Thx','Pennsylvania','PA'};
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT id, Tax_Percentage_s__c,Subscription_Id__c,Tax_Amount__c FROM OpportunityLineItem where Opportunity.Contact__c != null AND Status__c != \'Inactive\' AND Opportunity.RecordType.Name = \'Subscription\' AND Tax_Percentage_s__c = 7.875 AND Subscription_Id__c != null  AND Opportunity.Contact__r.mailingState IN :stateList');
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        String recordId = '';
        try{
            Map<String, OpportunityLineItem> opliMap = new Map<String, OpportunityLineItem>();
            for (OpportunityLineItem opli: scope) {
                Subscriptions.updateSubscriptionForBatch(opli.Subscription_Id__c,0);
                opli.Tax_Percentage_s__c = 0;
                opli.Tax_Amount__c = 0;
            }
            
            update scope;
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
        }catch (Exception e) {
            system.debug('e.getMessage()-->'+e.getMessage());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'UpdateTaxonSub',
                    'Batch',
                    recordId,
                    e
                )
            );
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}