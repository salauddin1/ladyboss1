global class OrderLineItems {
	global Decimal id;
	global String name;
	global Decimal product_id;
	global String variation_id;
	global String quantity;
	global String tax_class;
	global String subtotal;
	global String subtotal_tax;
	global String total;
	global String total_tax;
	//global List<Object> taxes;
	global List<OrderMetaData> meta_data;
	global String sku;
	global String price;
}