public without sharing class PAPBalanceViewHubController {
    @AuraEnabled
    public static customerWrapper getBalance(String conID){
        Contact con = [SELECT id,Available_Commission__c,PAP_refid__c  FROM Contact WHERE id = : conID];
        Decimal balance = 0;
        Integer codeCount = 0;
        customerWrapper cwWrapper = new customerWrapper();        
        if (con.Available_Commission__c != null) {
            balance = con.Available_Commission__c;
        }
        cwWrapper.papBalance = balance;
         cwWrapper.moreNeeded = 0;
        Set <String> PapOrderSet = new Set <String>();
        Map<String,String> commValueLabelMap = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Pap_Commission__c.Commission_Type__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry v : values) {
            commValueLabelMap.put(v.getValue(), v.getLabel());
        }
        List<Pap_Commission__c>  papList = new List<Pap_Commission__c>();
        List<PAPBalanceViewProductMapping__c>  DescProductMap = new List<PAPBalanceViewProductMapping__c>();
        DescProductMap = [select id,name,Product_Name__c,Statement_Descriptor__c from PAPBalanceViewProductMapping__c where Product_Name__c!=null and Statement_Descriptor__c!=null and isActive__c=true];
        papList = [SELECT id,order_id__c,product_id__c,Commission_Type__c,Opportunity__r.Name,Opportunity__r.contact_email__c, data2__c, data1__c, data4__c,Opportunity__r.Amount,commission__c,Contact__r.Name,Opportunity__r.CreatedDate FROM Pap_Commission__c WHERE Contact__c =:conID order by Opportunity__r.CreatedDate Desc];
        
        List<commissionWrapper> commissionListWrNew = new List<commissionWrapper>();
        for(Pap_Commission__c pap : papList){
            commissionWrapper commWr = new commissionWrapper();
            commWr.commission = pap;
            if(DescProductMap.size()>0){
                for(PAPBalanceViewProductMapping__c stripe : DescProductMap){
                    if(  stripe.Product_Name__c != '' && stripe.Statement_Descriptor__c !='' && stripe.Product_Name__c != null && stripe.Statement_Descriptor__c !=null){
                        if(pap.Opportunity__r.Name!=null && pap.Opportunity__r.Name.contains(stripe.Statement_Descriptor__c)){
                            
                            commWr.productName =  stripe.Product_Name__c;
                        } else {
                            commWr.productName =  pap.Opportunity__r.Name ;
                            
                        }
                    }
                    else{
                       
                        commWr.productName =  pap.Opportunity__r.Name ; 
                    }
                }
            }else {
                commWr.productName =  pap.Opportunity__r.Name ; 
            }
            commWr.isRefund = False ;
            if(pap.Opportunity__r.contact_email__c   != null ){
                String emailValue = pap.Opportunity__r.contact_email__c;
                if(emailValue.contains('@')){
                    List<String> emList = emailValue.split('@');
                    String em1 = emList[0];
                    String temp = '';
                    if(em1.length() < 4){
                        Integer tempLength1 = em1.length() / 2 ;
                        Integer tempLength = em1.length() -tempLength1 ;
                        for(integer i = 0 ; i < tempLength ; i++){
                            temp = temp + '*';
                        }
                        emailValue = em1.substring(0, tempLength1)+temp+'@'+emList[1];
                    }
                    else if(em1.length() > 3){
                        Integer tempLength1 =  em1.length() / 3 ;
                       // tempLength1 = tempLength1 +1 ;
                        Integer tempLength = em1.length() -tempLength1 ;
                        for(integer i = 0 ; i < tempLength ; i++){
                            temp = temp + '*';
                        }
                        
                        emailValue = em1.substring(0, tempLength1)+temp+'@'+emList[1];
                    }
                    
                }
                commWr.email = emailValue ;
            }
            if(pap.Commission_Type__c != null && pap.Commission_Type__c.contains('refund') ){
                commWr.isRefund = True ;
            }
            if(pap.Commission_Type__c != null && pap.Commission_Type__c != '' ){
                if(commValueLabelMap != null && commValueLabelMap.keySet().size() > 0 && commValueLabelMap.keySet().contains(pap.Commission_Type__c)){
                    commWr.CLUBCaseType = commValueLabelMap.get(pap.Commission_Type__c) ;
                }
            }
            if(pap.Opportunity__r.CreatedDate != null ){
                String dateValue = String.valueOf( pap.Opportunity__r.CreatedDate.date() );
                commWr.createdDateVal = dateValue ;
            }
            commissionListWrNew.add(commWr);
        }
        
        cwWrapper.commissionListWr = commissionListWrNew;
        for(Pap_Commission__c pap : papList){
            if(pap.Commission_Type__c != 'refund'){
                PapOrderSet.add(pap.order_id__c);
            }
        }
        cwWrapper.codeUsedCount = PapOrderSet.size();
        cwWrapper.qualifiedThreeForFree = true;
        for (OpportunityLineItem opli : [SELECT id,End__c,Product2.Price__c,Product2.Name FROM OpportunityLineItem WHERE Opportunity.Contact__c =: conID AND (Status__c = 'Active' OR Status__c = 'trialing') AND Subscription_id__c != null ORDER BY End__c ASC LIMIT 1]) {
            cwWrapper.nextorderDate = opli.End__c;
            cwWrapper.name = opli.Product2.Name;
            cwWrapper.productPrice = opli.Product2.Price__c;
            cwWrapper.qualifiedNextOrder = false;
            if (balance >= opli.Product2.Price__c) {
                cwWrapper.qualifiedNextOrder = true;
                cwWrapper.moreNeededForFree = 0;
            }else {
                cwWrapper.moreNeededForFree = opli.Product2.Price__c - balance;
            }
        }
        System.debug('==balance=='+balance);
        
        AggregateResult ar = [SELECT min(Product2.Price__c)p FROM OpportunityLineItem WHERE (Status__c = 'Active' OR Status__c = 'trialing') AND Opportunity.Contact__c =: conID AND Product2.Price__c <= :balance AND Subscription_id__c != null];
        if (ar.get('p')!=null) {
            cwWrapper.minSubAmount = Decimal.valueOf(String.valueOf(ar.get('p')));
            cwWrapper.moreNeeded = 0;
            if (balance < cwWrapper.minSubAmount) {
                cwWrapper.moreNeeded = cwWrapper.minSubAmount - balance;
            }
        }
        
        
        cwWrapper.CouponCode = con.PAP_refid__c;
        return cwWrapper;
    }
    public class customerWrapper{
        @AuraEnabled public String name;
        @AuraEnabled public String CouponCode;
        @AuraEnabled public Integer codeUsedCount;
        @AuraEnabled public Decimal papBalance;
        @AuraEnabled public Decimal moreNeeded;
        @AuraEnabled public Decimal moreNeededForFree;
        @AuraEnabled public Decimal productPrice;
        @AuraEnabled public Decimal minSubAmount;
        @AuraEnabled public Date nextorderDate;
        @AuraEnabled public Boolean qualifiedNextOrder;
        @AuraEnabled public Boolean qualifiedThreeForFree;
        @AuraEnabled public List<commissionWrapper> commissionListWr = new List<commissionWrapper>();
    }
    public class commissionWrapper{
        @AuraEnabled public String email;
        @AuraEnabled public String productName;
        @AuraEnabled public String createdDateVal;
        @AuraEnabled public String CLUBCaseType;
        @AuraEnabled public Boolean isRefund;
        @AuraEnabled public Pap_Commission__c commission = new Pap_Commission__c();
    }
}