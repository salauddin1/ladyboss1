public class BatchSendTaskEmail implements Database.Batchable<sObject>,Schedulable,Database.AllowsCallouts {
	
	public void execute(SchedulableContext SC) {
		BatchSendTaskEmail m = new BatchSendTaskEmail(); 
		Database.executeBatch(m);
	}
   
	public Database.QueryLocator start(Database.BatchableContext BC){
		DateTime dt = System.now();
		dt = dt.addHours(-1);
		dt = dt.addMinutes(0-dt.Minute());
		dt = dt.addSeconds(0-dt.Second());
		return Database.getQueryLocator('select Id,WhoId,Five9__Five9AgentExtension__c,ownerId,CallDurationInSeconds from task where WhoId!=null and Five9__Five9AgentExtension__c != null and ownerId != null and CallDurationInSeconds !=null and CallDurationInSeconds > 120 and createdDate >= : dt');
	}

	public void execute(Database.BatchableContext BC,List<sObject> scope){
		map<Id,ID> mapId2Contact = new map<Id,ID>();
        map<Id,Id> mapId2Lead = new map<Id,Id>();
        List<Id> userIdList = new List<Id>();
        Map<Id,Id> userTaskMap = new Map<Id,Id>();
        for(sObject s : scope){
        		Task tsk = (Task)s;
            if(tsk.WhoId != null && tsk.Five9__Five9AgentExtension__c != null && tsk.ownerId != null && tsk.CallDurationInSeconds !=null && tsk.CallDurationInSeconds > 120){
                userIdList.add(tsk.ownerId);
                userTaskMap.put(tsk.Id,tsk.ownerId);
                String whoID = ((String)tsk.WhoId).substring(0,3);
                if (whoID == '003') {
                    mapId2Contact.put(tsk.Id,tsk.WhoId);
                } else if (whoID == '00Q') {
                    mapId2Lead.put(tsk.Id,tsk.WhoId);    
                }
            }    
        }
        Map<Id,User> usersMap = new Map<Id,User>([SELECT id,username FROM USER WHERE Id IN :userIdList]);
        Set<String> userNameList = new Set<String>();
        for(String key : usersMap.keyset()){
            userNameList.add(usersMap.get(key).username);
        }
        List<Email_Templates_For_User__mdt> emailTemplateMetaList = [SELECT DeveloperName,Id,isActive__c,Label,Language,MasterLabel,NamespacePrefix,QualifiedApiName,Template_Name__c,Username__c FROM Email_Templates_For_User__mdt where isActive__c = true and Username__c IN :userNameList];
        Map<String,Email_Templates_For_User__mdt> userNameEmaillMap = new Map<String,Email_Templates_For_User__mdt>();
        Set<String> emailTemplateData = new Set<String>();
        for(Email_Templates_For_User__mdt emailTempData : emailTemplateMetaList){
            userNameEmaillMap.put(emailTempData.Username__c,emailTempData);
            emailTemplateData.add(emailTempData.Template_Name__c);
        }
        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        Map<Id,Contact> contacttoSendEmailMap = new Map<Id,Contact>([Select firstname,lastname,email,id,name,MobilePhone, owner.email from Contact where id in : mapId2Contact.values()]);
        Map<Id,Lead> leadtoSendEmailMap = new Map<Id,Lead>([Select firstname,lastname,email,id,name,MobilePhone, owner.email from Lead where id in : mapId2Lead.values()]);
        List<EmailTemplate> emailTemplateList = [Select Body,DeveloperName,HtmlValue ,Id,Name,Subject from EmailTemplate where DeveloperName IN :emailTemplateData];
        Map<String,EmailTemplate> emailMap = new Map<String,EmailTemplate>();
        for(EmailTemplate email : emailTemplateList){
            emailMap.put(email.DeveloperName,email);
        }
        
        for(Id key : userTaskMap.keySet()){
            if(userNameEmaillMap.get(usersMap.get(userTaskMap.get(key)).userName) != null){
                EmailTemplate et = emailMap.get(userNameEmaillMap.get(usersMap.get(userTaskMap.get(key)).userName).Template_Name__c);
                if(et!=null){
                    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    mail.setSaveAsActivity(false);
                    mail.setSubject(et.Subject);
                    String body = et.HtmlValue;
                    if(body!=null){
                        String email = null;
                        String fname = null;
                        String lname = null;
                        List<String> toeamils = new List<String>();
                        if(contacttoSendEmailMap.get(mapId2Contact.get(key)) != null){
                            Contact cn = contacttoSendEmailMap.get(mapId2Contact.get(key));
                            toeamils.add(cn.Email);
                            email = cn.Email;
                            fname = cn.firstname;
                            lname = cn.lastname;
                        }
                        if(leadtoSendEmailMap.get(mapId2Lead.get(key)) != null){
                            Lead ld = leadtoSendEmailMap.get(mapId2Lead.get(key));
                            toeamils.add(ld.Email);
                            email = ld.Email;
                            fname = ld.firstname;
                            lname = ld.lastname;
                        }
                        mail.setReplyTo('support@ladyboss.com'); 
                        body = body.replaceAll('<<EMAIL>>',email!=null?email:'');
                        body = body.replaceAll('<<FNAME>>',fname!=null?fname:'');
                        body = body.replaceAll('<<LNAME>>',lname!=null?lname:'');
                        mail.setHtmlBody(body);
                        allmsg.add(mail);
                        mail.setToAddresses(toeamils);
                    } 
                }
                         
            } 
        }
        if(allmsg.size() > 0)
            Messaging.sendEmail(allmsg,false);
 
    }

	public void finish(Database.BatchableContext BC){

	}
    
}