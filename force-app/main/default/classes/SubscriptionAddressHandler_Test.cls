@isTest
public class SubscriptionAddressHandler_Test {
    static testMethod void testMethodPostive4(){ 
        ContactTriggerFlag.isContactBatchRunning = true;
        contact con = new contact();
        con.lastName='test';
        
        con.firstName='test';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
       
        insert add;
        opportunity op  = new opportunity();
        op.name='opclub';
        op.Contact__c =con.id;
        op.CloseDate = System.today();
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.Clubbed__c = true;
        op.Stripe_Message__c = 'Success';
        op.Created_Using__c= 'V10';
        insert op;
        
        
        
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        prod.Stripe_SKU_Id__c = 'abc';
        prod.Stripe_Product_Id__c = 'test';
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
         Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
         PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Unique_Name__c = op.Id;
        ol.Subscription_Id__c='ascd';
        ol.Tax_Percentage_s__c=8;
        ol.Status__c = 'trialing';
        
        insert ol;
        add.Primary__c =true;
        test.StartTest();
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        SubscriptionAddressHandler.ratemethodnotfuture(ol.Subscription_Id__c,add.Shipping_Street__c,add.Shipping_City__c,add.Shipping_State_Province__c,add.Shipping_Country__c,add.Shipping_Zip_Postal_Code__c,1,1,null); 
        SubscriptionAddressHandler.setTaxPercentage(add);
        test.StopTest();
     
    }
}