@isTest
private class InvoiceToChargeUpdateBatch_Test {
	@testSetup 
    static void setup() {
        ContactTriggerFlag.isContactBatchRunning=true;
    	Invoice__c inv1 = new Invoice__c();
    	inv1.Charge_Id__c = 'ch_0001';
    	inv1.Subscription_Id__c = 'sub_0001';
    	inv1.Period_End__c = Date.newInstance(2016, 12, 9);
    	inv1.Period_Start__c = Date.newInstance(2016, 01, 9);
        insert inv1;
        Invoice__c inv2 = new Invoice__c();
    	inv2.Charge_Id__c = 'ch_0002';
    	inv2.Subscription_Id__c = 'sub_0002';
    	inv2.Period_End__c = Date.newInstance(2016, 12, 10);
    	inv2.Period_Start__c = Date.newInstance(2016, 01, 7);
        insert inv2;
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='cus_DYWAlCN3vsdH4k';
        con.firstName='test';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity op  = new opportunity();        
        op.Name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.Clubbed__c = true;
        op.CloseDate = System.today();
        op.Created_Using__c = 'V8';
        oppList.add(op);
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'USA';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        add.Shipping_State_Province__c = 'test';
        insert add;
        Opportunity op1  = new opportunity();        
        op1.Name='op1';
        op1.StageName = 'Customer Won';
        op1.Amount = 3000;
        op1.CloseDate = System.today();
        op1.Created_Using__c = 'V8';
        oppList.add(op1);
        insert oppList;
        List<OpportunityLineItem> oplineList = new List<OpportunityLineItem>();
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345',Price__c = 20);
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Stripe_charge_id__c = 'ch_0001';
        ol.Subscription_Id__c = 'sub_0001';
        ol.Tax_amount__c = 20;
        
        oplineList.add(ol);
        OpportunityLineItem ol1 = new OpportunityLineItem();
        
        ol1.Product2Id =prod.id;
        ol1.OpportunityId = op1.Id;
        ol1.Quantity = 1;
        ol1.UnitPrice = 2.00;
        ol1.PricebookEntryId = customPrice.Id;
        ol1.Subscription_Id__c = 'sub_0002';
        
        ol.Tax_amount__c = 20;
        
        
        oplineList.add(ol1);

        insert oplineList;
    }
	
	@isTest static void test_method_one() {
		
		Test.startTest();
        ContactTriggerFlag.isContactBatchRunning=true;
			InvoiceToChargeUpdateBatch uca = new InvoiceToChargeUpdateBatch();
        	Id batchId = Database.executeBatch(uca);
		Test.stopTest();
	}
	/*@isTest static void test_method_two() {
		Test.startTest();
			InvoiceToChargeUpdateBatch uca = new InvoiceToChargeUpdateBatch();
        	Id batchId = Database.executeBatch(uca);
		Test.stopTest();
	}*/
	
	
	
}