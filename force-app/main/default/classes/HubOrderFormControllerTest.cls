@isTest
public class HubOrderFormControllerTest {
    @IsTest
    static void methodName(){
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        contact con = new contact();
        con.lastName='lastName';
        con.firstName='firstName';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_Eug8juCETidS33';
        sp.Customer__c = con.id;
        insert sp;
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        card.Stripe_Card_Id__c = 'card_testtest';
        Date d = date.Today();
        insert card;
        
        ACH_Account__c ac = new ACH_Account__c(); 
        ac.ACH_Token__c = null;
        ac.Name = 'test';
        ac.Stripe_Profile__c = sp.id;
        ac.Stripe_ACH_Id__c='achiddskfdsf';
        insert ac;
        
        Product2 pro = new Product2();
        pro.Name='LEAN-3';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        pro.Available_For_Hub__c= true;
        insert pro; 
        Product2 prod = new Product2();
        prod.Name='LEAN-LEAN';
        prod.Price__c=12345;
        prod.Fuel__c = 3;
        prod.Burn__c = 0;
        prod.Lean__c = 1;
        prod.Recover__c = 0;
        prod.Rest__c = 1;
        prod.Family = 'Hardware';
        prod.Stripe_Plan_Id__c='1yrmembershipw';
        prod.IsActive=true;
        prod.Available_For_Hub__c= true;
        insert prod; 
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        opportunity op  = new opportunity();
        op.name=pro.Name+'CLUB';
        op.Contact__c =con.Id;
        op.StageName = 'Closed Won';
        op.RecordTypeId = devRecordTypeId;
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        OpportunityLineItem olpo = new OpportunityLineItem();        
        olpo.Product2Id =pro.id;
        olpo.OpportunityId = op.Id;
        olpo.Quantity = 3;
        olpo.UnitPrice = 2.00;
        olpo.Subscription_Id__c='sub_EypY8AdfVmqvVo';
        olpo.Start__c=System.today()-1;
        olpo.End__c=System.today();    
        olpo.Opportunity_Unique_Name__c = op.Id;
        olpo.PricebookEntryId = standardPrice.Id;
        olpo.Status__c='Active';
        insert olpo;
        List<HubOrderFormController.productWrapper> pwList = new List<HubOrderFormController.productWrapper>();
        HubOrderFormController.productWrapper pw = new HubOrderFormController.productWrapper();
        pw.id = olpo.id;
        pw.totalPrice = 2.00;
        pw.Name = 'Test';
        pwList.add(pw);
        List<HubOrderFormController.productWrapper> pwList1 = new List<HubOrderFormController.productWrapper>();
        HubOrderFormController.productWrapper pw1 = new HubOrderFormController.productWrapper();
        pw1.Id = pro.id;
        pw1.totalPrice = 2.00;
        pw1.Name = 'Test';
        pwList1.add(pw1);
        List<HubOrderFormController.customWrapper> cwList = new List<HubOrderFormController.customWrapper>();
        HubOrderFormController.customWrapper cw = new HubOrderFormController.customWrapper();
        cw.Name = 'burn';
        cw.quantity = 0;
        cwList.add(cw);
        HubOrderFormController.customWrapper cw1 = new HubOrderFormController.customWrapper();
        cw1.Name = 'recover';
        cw1.quantity = 0;
        cwList.add(cw1);
        HubOrderFormController.customWrapper cw2= new HubOrderFormController.customWrapper();
        cw2.Name = 'fuel';
        cw2.quantity = 3;
        cwList.add(cw2);
        HubOrderFormController.customWrapper cw3 = new HubOrderFormController.customWrapper();
        cw3.Name = 'lean';
        cw3.quantity = 1;
        cwList.add(cw3);
        HubOrderFormController.customWrapper cw4 = new HubOrderFormController.customWrapper();
        cw4.Name = 'rest';
        cw4.quantity = 1;
        cwList.add(cw4);
        Test.startTest();
        HubOrderFormController.getProducts(con.Id);
        HubOrderFormController.CancelSubscriptions(JSON.serialize(pwList));
        HubOrderFormController.EmailReminder(JSON.serialize(pwList));
        HubOrderFormController.checkPayment(JSON.serialize(pwList), JSON.serialize(pwList1));
        HubOrderFormController.createPayment(JSON.serialize(pwList), JSON.serialize(pwList1), card.Stripe_Card_Id__c);
        HubOrderFormController.getAllProducts();
        HubOrderFormController.getCard(con.Id);
        HubOrderFormController.getCustomProduct(JSON.serialize(cwList));
        HubOrderFOrmCOntroller.getTax(con.ID, JSON.serialize(pwList));
        Test.stopTest();
        
    }
}