@isTest
public class GetOrdersFromShopify_Test {
	@isTest
    public Static void Test1() {
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'testemailtest@gmail.com';
        con.Phone = '7067749234';
        insert con;
        Shopify_Parameters__c peram= new Shopify_Parameters__c();
        peram.Access_Token__c = 'jwwdggeukeufgr3d23edd33e3enfbd';
        peram.Domain_Name__c = 'https://test.com';
        peram.Is_Active__c = true;
        insert peram;
        Shopify_Orders__c ord= new Shopify_Orders__c();
        ord.Order_Id__c = '123456789';
        insert ord;
        shopifyOrdercount__c countiterable = new shopifyOrdercount__c();
        countiterable.End_page__c = 100;
        countiterable.Start_Page__c = 1;
        insert countiterable;
        test.startTest();
        test.setMock(HttpcalloutMock.class, new GetOrdersFromShopify_Mock());
        GetOrdersFromShopify getord = new GetOrdersFromShopify();
        Database.executeBatch(getord);
        test.stopTest();
    }
}