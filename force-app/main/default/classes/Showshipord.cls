public class Showshipord {
    @AuraEnabled
    public static List<ShipStation_Orders__c> getShip(String contactId){
        List<ShipStation_Orders__c> shipVal = new List<ShipStation_Orders__c>();
        shipVal =[select id,Name,orderId__c,customerUsername__c,modifyDate__c,createdDate,OwnerId,Contact__c from ShipStation_Orders__c where Contact__c =: contactId order by createdDate DESC]; 
        return shipVal;
    }
}