public without sharing class UpdateCardSite{
    public boolean showUpdateCard {get;set;}
    public String MyActionMethod { get; set; }
    
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    public  String  headerdata{get;set;}
    public string urlvalue{get;set;}
    public string url{get;set;}
    public string cardType{get;set;}
    public string cardLast4Didit{get;set;}
    public string cardInfo {get;set;}
    public string customerID {get;set;}
    public string uniqueId {get;set;}
    public string cardAction {get;set;}
    public Boolean isUpdate {get;set;}
    public Boolean isShowEntry {get;set;}
    public String selectedValue { get; set; }
    public void openUpdateForm(){
        system.debug('openUpdateForm is fired');
        showUpdateCard = true;
        system.debug('showUpdateCard ==>'+showUpdateCard);
        
        
    }
    public void MyActionMethod(){
        selectedValue = 'Update Card';
        cardAction = 'Update Card';
        isUpdate = true;
        isShowEntry = false;
        customerID = ApexPages.currentPage().getParameters().get('id');
        uniqueId = ApexPages.currentPage().getParameters().get('key');
        HttpRequest http = new HttpRequest();
        //check if profile exists or not 
        // List<Stripe_Profile__c> stripeProfList = [select id ,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
        
        Map<string,string> conStripeMap = new Map<string,string> ();
        Map<string,string> conStripeMap1 = new Map<string,string> ();
        
        
        
        
        http.setEndpoint(SERVICE_URL+''+customerId );
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        try {
            hs = con.send(http);
            response = hs.getBody();
            statusCode = hs.getStatusCode();
        } catch (CalloutException e) {
            
        }
        
        system.debug('#### '+ hs);
        
        system.debug('response-- '+response);
        system.debug('statusCode-- '+statusCode);
        
        if(statusCode == 200 ){
            System.debug(response);
            try {
                //StripeCard o = (StripeCard.parse(response));
                //System.debug('---------StripeCard-o-----------'+o);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('sources');
                system.debug('response-- '+lstCustomers       );
                List<Object> lstsourcess = (List<Object>)lstCustomers.get('data');
                Object carddata = (object)lstsourcess[0];
                system.debug('response-- '+carddata );
                Map<String,Object> data = (Map<String,Object>)carddata ;
                
                //Magic!
                system.debug('dfgdgdf'+data.get('brand'));
                system.debug('dfgdgdf'+data.get('last4'));
                // return null;
                cardLast4Didit = string.valueOf(data.get('last4'));
                cardType=string.valueOf(data.get('brand'));
                System.debug('--------------cardType------------'+cardType);
                System.debug('--------------cardLast4Didit------------'+cardLast4Didit);
                
            } catch (System.JSONException e) {
                
            }
            List<Custom_Tracker__c> cusTrack = [SELECT id,Is_Clicked__c,Link_Click_Count__c,Email_Link_Click_Date_Time__c FROM  Custom_Tracker__c WHERE UniqueId__c =: uniqueId];
            if (cusTrack.size()>0) {
                cusTrack[0].Is_Clicked__c = true;
                cusTrack[0].Email_Link_Click_Date_Time__c = Datetime.now();
                if (cusTrack[0].Link_Click_Count__c!=null) {
                    cusTrack[0].Link_Click_Count__c ++;
                }else {
                   cusTrack[0].Link_Click_Count__c = 1; 
                }
                update cusTrack;
            }
            
        }
    }
    public void checkSelectedValue() {
    isShowEntry =true;
          system.debug('Selected value is: ' + selectedValue); 
          if(selectedValue == 'update') {
              cardAction = 'Update Card';
             isUpdate = true;
          } else if(selectedValue == 'new')  {
             cardAction = 'New Card';
             isUpdate = false;
          }
    }
    @auraEnabled
    public static Boolean updateCard1(){
        system.debug('fired===>');
        return true;
    }
      @auraEnabled
    public static Boolean updateCustomerCard(string cardId, string customerId,String address_city,
                                             string address_country,String address_state,string address_zip,
                                             string exp_month,string exp_year,string name,string tokenId,string address_line1,boolean isUpate)
    {
        
        //update customer
        String updateCustomerURL = 'https://api.stripe.com/v1/customers/'+customerId;
        if(!isUpate) updateCustomerURL =  updateCustomerURL + '/sources';
         //updateCustomerURL =  updateCustomerURL + '/sources';
        
        HttpRequest httpUpdateCustomer = new HttpRequest();
        httpUpdateCustomer.setEndpoint(updateCustomerURL);
        httpUpdateCustomer.setMethod('POST');
        httpUpdateCustomer.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
        Map<String, String> updateCustomerPayload = new Map<string,string>{'source' => tokenId };
           
            httpUpdateCustomer.setBody(StripeUtil.urlify(updateCustomerPayload));
        try {
            Integer statusCode1;
            Http con1 = new Http();
            HttpResponse hs1 = new HttpResponse();
            hs1 = con1.send(httpUpdateCustomer);
            
            statusCode1 = hs1.getStatusCode();
            system.debug('statusCode1==>'+statusCode1);
             system.debug('update result-->'+JSON.deserializeUntyped(hs1.getBody()));
            if(statusCode1 ==200) {
                
                
                
                HttpRequest http = new HttpRequest();
                
                //String customerId =ApexPages.currentPage().getParameters().get('id');
                //String customerId = 'cus_E3ASsMaZCTBNrv';
                
                // String cardId = 'card_1Db73sBwLSk1v1oh9Rh1SCjB';
                string url =  'https://api.stripe.com/v1/customers/'+customerId +'/sources/'+cardId ;
                http.setEndpoint(url );
                http.setMethod('POST');
                system.debug('tokenId==>'+tokenId);
                // Blob headerValue = Blob.valueOf(tokenId + ':');
                //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
                // String authorizationHeader = 'BASIC ' +
                //   EncodingUtil.base64Encode(headerValue);
                //system.debug('####---auth--- '+authorizationHeader);
                http.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
               
                Map<String, String> payload = new Map<String, String>{'address_zip' =>address_zip ,'address_city' =>address_city ,
                    'address_country' => address_country ,
                    'address_state' => address_state,'name' => name,'address_zip' => String.valueOf(address_zip),
                    'exp_month' => String.valueOf(exp_month),'exp_year' => String.valueOf(exp_year),'address_line1' => address_line1
                    };
                  system.debug('StripeUtil.urlify(payload ) :'+StripeUtil.urlify(payload ));
                http.setBody(StripeUtil.urlify(payload ));
                
                String response;
                Integer statusCode;
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                Map<String, Object> results;
                if(!test.isrunningTest()){
                hs = con.send(http);
                response = hs.getBody();
                statusCode = hs.getStatusCode();
                }else
                {
                statusCode =200;
                response = '{"id":"card_1DhIKkBwLSk1v1ohtWCxQrTa","object":"card","address_city":"sdf","address_country":null,"address_line1":"sdf","address_line1_check":"pass","address_line2":null,"address_state":"sdf","address_zip":"66666","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_E1EYecfsRNu1yD","cvc_check":"pass","dynamic_last4":null,"exp_month":12,"exp_year":2034,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"dfgggdfg","tokenization_method":null}';
                }
                                
                if(statusCode ==200) {
                    if(!isUpate){
                    system.debug('Updating Default card to new card');
                    //set default calrd
                    updateCustomerURL = 'https://api.stripe.com/v1/customers/'+customerId;
                    HttpRequest httpsetDefaultCard = new HttpRequest();
                    httpsetDefaultCard.setEndpoint(updateCustomerURL);
                    httpsetDefaultCard.setMethod('POST');
                    httpsetDefaultCard.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
                    map<string,string> defaultCardMap = new Map<string,string>{'default_card' => cardId};
                    httpsetDefaultCard.setBody(StripeUtil.urlify(defaultCardMap));
                    Integer defaultCardStatusCode;
                    Http defaultCardHTTP = new Http();
                    HttpResponse defaultCard_HS = new HttpResponse();
                    defaultCard_HS = defaultCardHTTP.send(httpsetDefaultCard);                    
                    defaultCardStatusCode = defaultCard_HS.getStatusCode();
                        system.debug('defaultCard_HS==>'+JSON.deserializeUntyped(defaultCard_HS.getBody()));
                }
                    
                    System.debug('======='+response );
                    if(!test.isrunningTest()){
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    }else{
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(response );
                    }
                    List<Stripe_Profile__c> lstStripeProfile = [SELECT Id, Name,Customer__c, Stripe_Customer_Id__c FROM Stripe_Profile__c where Stripe_Customer_Id__c =:customerId ];
                    
                    if(lstStripeProfile .size() > 0) {
                     updateShippingAddress(customerId , address_line1, address_city,address_country, address_state,address_zip);
                        
                         List<Address__c> aList = new List<Address__c> ();
                        if((address_city !=null && address_city!= '' ) || (address_state!=null && address_state!= '') ||  (address_line1!=null && address_line1!= '') || (address_zip!=null && address_zip!= '' ) || (address_country!=null && address_country!= ''))
                        {
                            Address__c addr = new Address__c();
                            addr.Contact__c =lstStripeProfile[0].Customer__c;
                            addr.Shipping_City__c =address_city;
                            addr.Shipping_State_Province__c = address_state;
                            addr.Shipping_Street__c =address_line1;
                            addr.Shipping_Zip_Postal_Code__c = address_zip;
                            addr.Shipping_Country__c= address_country;
                        
                            if((address_city !=null && address_city!= '' ) && (address_state!=null && address_state!= '') &&  (address_line1!=null && address_line1!= '') && (address_zip!=null && address_zip!= '' ) && (address_country!=null && address_country!= ''))
                        {
                                addr.Primary__c =true;
                            }
                        
                            aList.add(addr);
                        }
                        
                        if(aList.size() > 0) {
                            insert aList;
                            string JSONString = JSON.serialize(aList);
                           // insertAdress(JSONString ,string.valueOf(lstStripeProfile[0].Customer__c));
                        }
                                                
                    } 
                }
                else
                {
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    List<Map<String, Object>> error          =   new List<Map<String, Object>>();
                    Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                    String returnSucess= String.valueOf(errorMap.get('message'));
                    System.debug('sdfsfd'+returnSucess);
                }
            }
            
            
            
        } catch (CalloutException e) {
            
        }
        return true;
    }
    @auraEnabled
    public static Map<String,Object> getExpireDateValues(){
        Map<String,Object> returnValue = new Map<String,Object>();
        Set<Integer> months = new Set<Integer> ();
        Set<Integer> years =  new Set<Integer>();
        for(Integer i=1 ;i<13;i++){
            months.add(i);
        }
        Integer currentYear = Date.today().Year();
        for(Integer i=0 ;i<20;i++){
            years.add(currentYear+i);
        }
        returnValue.put('months',months);
        returnValue.put('years',years);
        return returnValue;
    }
    
    public static integer updateShippingAddress(String customerId , string address_line1,String address_city,
                                             string address_country,String address_state,string address_zip) {
        HttpRequest http = new HttpRequest();
        string url =  'https://api.stripe.com/v1/customers/'+customerId  ;
        http.setEndpoint(url );
        http.setMethod('POST');
        http.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
        String reqBody = '&metadata[Shipping Address]='+address_line1+' '+address_city +' '+address_state +' '+address_country+' '+ address_zip+'&';
        http.setBody(reqBody);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        try{
        hs = con.send(http);
        response = hs.getBody();
        System.debug('0000000'+response );
        return statusCode = hs.getStatusCode();
        }catch(exception e){}
        return null;
    }
    @future
    public static void insertAdress(string AddressString,string conId){
    //Address__c updateAddress = (Address__c) JSON.deserialize(AddressString, Address__c.class);
    //updateAddress.Contact__c = conId;
    //update updateAddress;
   // list<Address__c> li_TurnoverTargetstoUpdate = (list<Address__c>) JSON.deserialize(AddressString, list<Address__c>.class);
    //insert li_TurnoverTargetstoUpdate ;
    //System.debug('========='+li_TurnoverTargetstoUpdate );
    }
}