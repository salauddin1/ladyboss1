public class AddTaskNoteController {
    @AuraEnabled
    public static void getTask(String caseId, String noteData){
        if(caseId != null && noteData != ''){
            List<Subsequent_Task__c> subtaskList = new List<Subsequent_Task__c>();
            Subsequent_Task__c subtask = new Subsequent_Task__c();
            subtask.Case_id__c = caseId;
            subtask.Note__c= noteData;
            subtask.IsNote__c = true ;
            Insert subtask;
            
        }
    }  
}