global class RpvDNCBatchforOtherPhoneField implements Database.Batchable<sobject>,Schedulable, Database.AllowsCallouts {
    global list<String> Contactlist1;  
    //=======================Cunstroctor for pass Contact Id in batch=========================
     global RpvDNCBatchforOtherPhoneField(){
        
    }
    
    global RpvDNCBatchforOtherPhoneField(list<String> Contactlist2){
            Contactlist1=Contactlist2;
    }
    global database.querylocator start(database.batchablecontext bc) {
        if(!Contactlist1.isEmpty())  {
            string query='select id,Other_Phone_National_DNC__c,Other_Phone_State_DNC__c,OtherPhone from contact where Id IN:Contactlist1';
            return database.getquerylocator( query ); 
        }
        else {
            string query='select id,Other_Phone_National_DNC__c,Other_Phone_State_DNC__c,OtherPhone,Other_Phone_Scrub_Processed__c from contact where OtherPhone!=null and Other_Phone_Scrub_Processed__c=false order by OtherPhone desc Limit 50000';
            return database.getquerylocator( query );
        } 
    }
    global void execute(database.BatchableContext bc,list<Contact> contactList) { 
        Boolean flag=false;
        list<String> ContactID =new list<String> ();
        list<Contact> UpdateContactDNC=new list<Contact> ();
        List<Five9Credentials__c> Apikey=[select RealPhoneValidationKey__c from Five9Credentials__c where isLive__c=:true];
        for(Contact con : contactList) {
            ContactID.add(con.Id);
            String OtherphoneNumber= con.OtherPhone.replaceAll('\\D','');
            if(!Apikey.isEmpty() && Apikey!=null){
                Five9Credentials__c Tokens=Apikey[0];
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint('https://api.realvalidation.com/rpvWebService/DNCLookup.php?phone='+OtherphoneNumber+'&token='+Tokens.RealPhoneValidationKey__c);
                req.setMethod('GET');
                HttpResponse res = h.send(req);
                Dom.Document doc = res.getBodyDocument();           
                //==================parse the response xml file and update contact ========================
                Dom.XMLNode DncNode = doc.getRootElement();
                String NationalDnc = DncNode.getChildElement('national_dnc', null).getText();
                String stateDnc = DncNode.getChildElement('state_dnc', null).getText();
                if(NationalDnc.equalsIgnoreCase('Y')){
                    con.Other_Phone_National_DNC__c=true;
                    flag=con.Other_Phone_National_DNC__c;
                } 
                if(stateDnc.equalsIgnoreCase('Y')) { 
                    con.Other_Phone_State_DNC__c=true;
                    }
                if(NationalDnc.equalsIgnoreCase('N')) {
                    con.Other_Phone_National_DNC__c=false;
                    }
                if(stateDnc.equalsIgnoreCase('N')) {
                    con.Other_Phone_State_DNC__c=false;
                    }
                con.Other_Phone_Scrub_Processed__c=true;
                UpdateContactDNC.add(con);
            }
        }
        update UpdateContactDNC;
        if(flag==true){
            //System.enqueueJob(new Five9OtherPhoneValidationHelperQuable(ContactID));
        }
    }
    global void finish(database.batchablecontext bc){}
      global void execute(SchedulableContext sc) {
        //RpvDNCBatchforOtherPhoneField b = new RpvDNCBatchforOtherPhoneField();
        //database.executebatch(b);
    }

}