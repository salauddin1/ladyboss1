public class testpaymentCommission {
    public static void previousWeekFSRefundHCnagesNSTier(Id currentCredituser,userHierarchy__c userhie,Comission_configuration__c comBefore,Payment__c paymnt,Comission_configuration__c com,List<Comission__c> comList,Set<Id> prodSet,Map<id,User> userSpecialComMap){
        List<Integer> weekList = new List<Integer>();
        if(Math.mod(Integer.valueOf(paymnt.week__c),2)==1){
            weekList.add(Integer.valueOf(paymnt.week__c));
            weekList.add(Integer.valueOf(paymnt.week__c)+1);
        }else{
            weekList.add(Integer.valueOf(paymnt.week__c));
            weekList.add(Integer.valueOf(paymnt.week__c)-1);
        }
        Decimal twoWeekTotal=0;
        Map<id,Decimal> creditUserAndAmount = new Map<id,Decimal>();
        List<OpportunityLineitem> oliList = [SELECT id,Total_Commissionable_amount__c,current_commissionable_amount__c,is_credit_user_available__c FROM OpportunityLineItem WHERE Opportunity.Is_First_Sell__c=false AND Opportunity.week__c in : weekList AND Opportunity.Sales_Person_Id__c =: paymnt.Payment_Created_By__c AND Product2Id not in : prodSet];
        System.debug('oliList : '+oliList);
        Decimal cred_user_total = 0;
        for(OpportunityLineitem oli : oliList ){ 
            if(oli.is_credit_user_available__c){
                Decimal amt = (oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c)/2;
                if(creditUserAndAmount.get(oli.Opportunity.User_to_give_credit__c)==null){
                	creditUserAndAmount.put(oli.Opportunity.User_to_give_credit__c,amt);
                }else{
                    amt = amt + creditUserAndAmount.get(oli.Opportunity.User_to_give_credit__c);
                    creditUserAndAmount.put(oli.Opportunity.User_to_give_credit__c,amt);
                }
               cred_user_total = cred_user_total + (oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c)/2;
            }
            twoWeekTotal = twoWeekTotal + oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c;
        }	
        Decimal agent_amount = twoWeekTotal - cred_user_total;
        Decimal dir_amount = twoWeekTotal;
        Decimal sa_amount = twoWeekTotal - 2 * cred_user_total;
        Decimal vp_amount = twoWeekTotal;
        Decimal saTOvp_amount = 2 * cred_user_total;
        
        Decimal agentCOm = agent_amount*(com.Percentage_Paid_to_Agent_Making_Sell__c - comBefore.Percentage_Paid_to_Agent_Making_Sell__c);
        Decimal dirCom = dir_amount*(com.Percentage_Paid_to_Director__c - comBefore.Percentage_Paid_to_Director__c);
        Decimal saCom=0;
        if(userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == null || userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == 0){
        	saCom = sa_amount*(com.Percentage_Paid_to_Senior_Agent__c - comBefore.Percentage_Paid_to_Senior_Agent__c);
        }
        Decimal vpCom = vp_amount*(com.Percentage_Paid_to_Vice_President__c - comBefore.Percentage_Paid_to_Vice_President__c);
        Decimal saToVpCom=0;
        if(userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == null || userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == 0){
        	saToVpCom = saTOvp_amount*(com.Percentage_Paid_to_Senior_Agent__c - comBefore.Percentage_Paid_to_Senior_Agent__c);
        }
        if(userhie.Agent__c!=null){
            insertComission(userhie.Agent__r.id,paymnt.id,agentCOm,0,comList,com,'Agent',0);
        }
        if(userhie.Director__c!=null){
            insertComission(userhie.Director__r.id,paymnt.id,dirCom,0,comList,com,'Director',0);
        }
        if(userhie.Vice_President__c!=null){
            insertComission(userhie.Vice_President__r.id,paymnt.id,vpCom,0,comList,com,'Vice President',0);
            insertComission(userhie.Vice_President__r.id,paymnt.id,saToVpCom,0,comList,com,'Senior Agent To VP',0);
        }
        if(userhie.Senior_Agent__c!=null){
            insertComission(userhie.Senior_Agent__r.id,paymnt.id,saCom,0,comList,com,'Senior Agent',0);
        }
        for(id userId : creditUserAndAmount.keyset()){
            	insertComission(userId,paymnt.id,creditUserAndAmount.get(userId)*(com.Percentage_Paid_to_Agent_Making_Sell__c/100) - creditUserAndAmount.get(userId)*(comBefore.Percentage_Paid_to_Agent_Making_Sell__c/100),0,comList,com,'Credit User',0);
        }
    } 
    public static void previousWeekNSRefundHCnagesFSTier(Id currentCredituser,userHierarchy__c userhie,Comission_configuration__c comBefore,Payment__c paymnt,Comission_configuration__c com,List<Comission__c> comList,Set<Id> prodSet,Map<id,User> userSpecialComMap){
        List<Integer> weekList = new List<Integer>();
        if(Math.mod(Integer.valueOf(paymnt.week__c),2)==1){
            weekList.add(Integer.valueOf(paymnt.week__c));
            weekList.add(Integer.valueOf(paymnt.week__c)+1);
        }else{
            weekList.add(Integer.valueOf(paymnt.week__c));
            weekList.add(Integer.valueOf(paymnt.week__c)-1);
        }
        Decimal twoWeekTotal=0;
        Map<id,Decimal> creditUserAndAmount = new Map<id,Decimal>();
        List<OpportunityLineitem> oliList = [SELECT id,Total_Commissionable_amount__c,current_commissionable_amount__c,is_credit_user_available__c FROM OpportunityLineItem WHERE Opportunity.Is_First_Sell__c=true AND Opportunity.week__c in : weekList AND Opportunity.Sales_Person_Id__c =: paymnt.Payment_Created_By__c AND Product2Id not in : prodSet];
        System.debug('oliList : '+oliList);
        Decimal cred_user_total = 0;
        for(OpportunityLineitem oli : oliList ){ 
            if(oli.is_credit_user_available__c){
                Decimal amt = (oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c)/2;
                if(creditUserAndAmount.get(oli.Opportunity.User_to_give_credit__c)==null){
                	creditUserAndAmount.put(oli.Opportunity.User_to_give_credit__c,amt);
                }else{
                    amt = amt + creditUserAndAmount.get(oli.Opportunity.User_to_give_credit__c);
                    creditUserAndAmount.put(oli.Opportunity.User_to_give_credit__c,amt);
                }
               cred_user_total = cred_user_total + (oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c)/2;
            }
            twoWeekTotal = twoWeekTotal + oli.Total_Commissionable_amount__c-oli.current_commissionable_amount__c;
        }	
        Decimal agent_amount = twoWeekTotal - cred_user_total;
        Decimal dir_amount = twoWeekTotal;
        Decimal sa_amount = twoWeekTotal - 2 * cred_user_total;
        Decimal vp_amount = twoWeekTotal;
        Decimal saTOvp_amount = 2 * cred_user_total;
        
        Decimal agentCOm = agent_amount*(com.Percentage_Paid_to_Agent_Making_Sell__c - comBefore.Percentage_Paid_to_Agent_Making_Sell__c);
        Decimal dirCom = dir_amount*(com.Percentage_Paid_to_Director__c - comBefore.Percentage_Paid_to_Director__c);
        Decimal saCom=0;
        if(userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == null || userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == 0){
        	saCom = sa_amount*(com.Percentage_Paid_to_Senior_Agent__c - comBefore.Percentage_Paid_to_Senior_Agent__c);
        }
        Decimal vpCom = vp_amount*(com.Percentage_Paid_to_Vice_President__c - comBefore.Percentage_Paid_to_Vice_President__c);
        Decimal saToVpCom=0;
        if(userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == null || userSpecialComMap.get(userhie.Director__c).special_com_as_director__c == 0){
        	saToVpCom = saTOvp_amount*(com.Percentage_Paid_to_Senior_Agent__c - comBefore.Percentage_Paid_to_Senior_Agent__c);
        }
        if(userhie.Agent__c!=null){
            insertComission(userhie.Agent__r.id,paymnt.id,agentCOm,0,comList,com,'Agent',0);
        }
        if(userhie.Director__c!=null){
            insertComission(userhie.Director__r.id,paymnt.id,dirCom,0,comList,com,'Director',0);
        }
        if(userhie.Vice_President__c!=null){
            insertComission(userhie.Vice_President__r.id,paymnt.id,vpCom,0,comList,com,'Vice President',0);
            insertComission(userhie.Vice_President__r.id,paymnt.id,saToVpCom,0,comList,com,'Senior Agent To VP',0);
        }
        if(userhie.Senior_Agent__c!=null){
            insertComission(userhie.Senior_Agent__r.id,paymnt.id,saCom,0,comList,com,'Senior Agent',0);
        }
        for(id userId : creditUserAndAmount.keyset()){
            	insertComission(userId,paymnt.id,creditUserAndAmount.get(userId)*(com.Percentage_Paid_to_Agent_Making_Sell__c/100) - creditUserAndAmount.get(userId)*(comBefore.Percentage_Paid_to_Agent_Making_Sell__c/100),0,comList,com,'Credit User',0);
        }
    } 
    public static void insertComission(id userId, id paymentId, Decimal amount,Decimal add_days,List<Comission__c> comList,Comission_Configuration__c comcfg,String hierarchy,Decimal com_percent){
        Integer weeks = PayToCommTriggerHelper.getweek(add_days);
        Comission__c comAgent = new Comission__c();
        comAgent.User__c = userId;
        comAgent.Amount__c = amount;
        comAgent.Payment__c = paymentId;
        comAgent.weeks__c = weeks;
        comAgent.Comission_Configuration__c = comcfg.id;
        comAgent.hierarchy__c = hierarchy;
        comAgent.commission_percent__c = com_percent;
        comList.add(comAgent);
    }
}