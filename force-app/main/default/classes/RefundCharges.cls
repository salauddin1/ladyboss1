public class RefundCharges {
    @AuraEnabled
    public Static List<Shopify_Orders__c> getshopOrder(String conId) {
        List<Shopify_Orders__c> order = [select id,Charge_Id__c,Order_Id__c,First_Name__c,last_Name__c,Refundable_Amount__c,Refunded_Amount__c,Subtotal_Price__c,Total_Price__c,Transaction_Id__c,Contact__c from Shopify_Orders__c where Contact__c =: conId AND  Contact__c <> null];
        return order;
    }
    @AuraEnabled
    public Static List<ShopifyLineItem__c> getshopItems(String ordId) {
        System.debug('============'+ordId);
        List<ShopifyLineItem__c> order = [select id,Price__c,Name from ShopifyLineItem__c where Shopify_Orders__c =: ordId AND  Shopify_Orders__c <> null];
        return order;
    }
    @AuraEnabled
    public static String refunds(String conId , String amount, String reason) {
        System.debug(conId+'====='+amount+'====='+reason);
        Integer amt = Integer.valueOf(Decimal.valueOf(amount)*100);
        String errorMsg = '';
        Shopify_Orders__c shopcharge = [select Charge_Id__c from Shopify_Orders__c where Id =:conId limit 1];
        if(shopcharge != null) {
            HttpRequest chrgreq = new HttpRequest();
            chrgreq.setEndpoint('https://api.stripe.com/v1/refunds');
            chrgreq.setMethod('POST');
            chrgreq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            System.debug('==========='+StripeAPI.ApiKey);
            chrgreq.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
            chrgreq.setBody('charge='+shopcharge.Charge_Id__c+'&amount='+amt+'&reason='+reason);
            Http chrghtp =new Http();
            HttpResponse chrgres= chrghtp.send(chrgreq);
            System.debug('======STripe charge====='+chrgres.getBody());
            if(chrgres.getStatus() == 'OK') {
                Shopify_Orders__c OrdList=[select Order_Id__c,phone__c,province__c,zip__c, Refundable_Amount__c, Email__c,city__c,address1__c,fulfillment_Status__c,First_Name__c,Last_Name__c,country__c from Shopify_Orders__c where Id =: conId limit 1];
                if(OrdList != null) {
                    String accessToken;
                    Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
                    if(Tokens != null){
                        accessToken = Tokens.Access_Token__c; 
                    }
                    try{
                        HttpRequest req = new HttpRequest();
                        Http htp =new Http();
                        HttpResponse res= new HttpResponse();
                        req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'/refunds/calculate.json');
                        req.setMethod('POST');
                        req.setBody('{"refund": {"currency": "USD","shipping": {"full_refund": true}}}');
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('X-Shopify-Access-Token', accessToken);
                        res= htp.send(req);
                        System.debug('=================res.body====='+res.getBody());
                        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                        Map<String, Object> refnd = (Map<String, Object>)result.get('refund');
                        System.debug('=========ffsddssds====='+refnd);
                        List<object> trx = (List<Object>)refnd.get('transactions');
                        Map<String, Object> trxHistry =new Map<String, Object> ();
                        String pId;
                        for(Object o : trx) {
                            trxHistry = (Map<String, Object>)o;
                            System.debug('===trxHistry==='+trxHistry);
                            Pid = String.valueOf(trxHistry.get('parent_id'));
                        }
                        System.debug('===pid====='+pid);
                        HttpRequest req1 = new HttpRequest();
                        
                        Http htp1 =new Http();
                        HttpResponse res1= new HttpResponse();
                        req1.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'/refunds.json');
                        req1.setMethod('POST');
                        req1.setHeader('Content-Type', 'application/json');
                        req1.setHeader('X-Shopify-Access-Token', accessToken);
                        req1.setBody('{"refund": {"currency": "USD","shipping": {"full_refund": true},"transactions": [{"parent_id": '+pid+',"amount": '+Decimal.valueOf(amount)+',"kind": "refund","gateway": "bogus"}]}}');
                        res1= htp1.send(req1);
                        System.debug('=================res.body====='+res1.getBody());
                        if(res1.getStatusCode() == 201) {
                            if(amount == String.valueOf(OrdList.Refundable_Amount__c)) {
                                Ordlist.Status__c = 'Refunded';
                                Ordlist.Financial_Status__c = 'Refunded';
                            }
                            else {
                                Ordlist.Status__c = 'Partially Refunded';
                                Ordlist.Financial_Status__c = 'Partially Refunded';
                            }
                        }
                        ShopifyOrderCreation.isrecursive = true;
                        Update Ordlist;
                        
                    }
                    
                    catch(Exception e) {}
                }
            }
            
            else {
                Map<String, Object> refunderror = (Map<String, Object>)JSON.deserializeUntyped(chrgres.getBody());
                Map<String, Object> refunderrorMsg = (Map<String, Object>)refunderror.get('error');
                errorMsg =   String.valueOf(refunderrorMsg.get('message'));
            }
        }
        return errorMsg;
    }
}