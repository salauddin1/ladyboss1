@isTest(seeAllData = false)
global class MockHttpResponseGenerator implements HttpCalloutMock {
    String tst='';
    public MockHttpResponseGenerator(String tst){
        this.tst = tst;
    }
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
      //  System.assertEquals('https://api.twilio.com/2010-04-01/Accounts/AC44099911ff7ec99c40bc97943ad6c0ca/Messages.json', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        //System.assertEquals('Data',req.getBody());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(tst!='tst'){
            res.setBody('{"sid" : null, "message" : "test message"}');
        }else{
        	res.setBody('{"sid" : "test", "message" : "test message"}');
        }
        res.setStatusCode(200);
        return res;
    }
}