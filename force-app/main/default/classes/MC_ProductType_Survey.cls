//This class is to update the Opportunity for updated card survey
public class MC_ProductType_Survey{
    public string recID;
    public string res;
    public string resServer;
    public string NoRecordIF;
    public string oppname{get;set;}
    public string customerComment{get;set;}
    public string username{get;set;}
    public boolean showServerMessage{get;set;}
    public boolean isOther{get;set;}
    public boolean textField{get;set;}
    public string selectObject{get;set;}
    public string selectCoachingObject{get;set;}
    public string productName{get;set;}
    public string productType{get;set;}
    //Get the value of the picklist from the Opportunity
    public List<SelectOption> regions
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Opportunity.OneClick_Customer_Chioce__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public List<SelectOption> goodregions
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Opportunity.OneClick_Customer_Choice_product__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public List<SelectOption> coachingOpt
    {
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Opportunity.OneClick_Customer_Chioce__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    public MC_ProductType_Survey(){
        if(!test.isRunningTest()){
            recID = ApexPages.currentPage().getParameters().get('id');
            List<Opportunity> opRec = [select id,Name,One_Click_Survey__c,MC_EmailLinkClicked__c from Opportunity where ID=: recID];
            system.debug('-----recID----'+recID);
            system.debug('--opRec-'+opRec);
            List<MC_ProductMapping__c> mcs = MC_ProductMapping__c.getall().values();
            if(opRec.size() > 0) {
                 oppname = opRec[0].Name;
                for(MC_ProductMapping__c mc : mcs){
                    if (oppname.contains(mc.Name)) {
                        textField = true;
                        system.debug('---------mc.Name-------------'+mc.Name);
                    }
                }
            }
        }
        showServerMessage =false;
        isOther =false;
    }
    //This Method redirects to the respective success or error page
    public PageReference successRedirect() {
        try{
            //Get the record ID and the value
            recID = ApexPages.currentPage().getParameters().get('id');
            res= ApexPages.currentPage().getParameters().get('res');
            Schema.DescribeFieldResult field = Opportunity.One_Click_Survey__c.getDescribe();
            Set<String> myPicklist = new Set<String>();
            for (Schema.PicklistEntry f : field.getPicklistValues()){
                myPicklist.add(f.getLabel());
                
            }
            //Get the cse record
            List<Opportunity> opRec = [select id,Name,Is_Coaching_Failed__c,One_Click_Survey__c,MC_EmailLinkClicked__c from Opportunity where ID=: recID];
            System.debug('===opRec ==='+opRec );
            //If Opportunity exists checks the survey already updated or not
            if(opRec.size() > 0) {
                oppname = opRec[0].Name;
                if(opRec[0].One_Click_Survey__c!=null && myPicklist.Contains(res)){
                    System.debug('===opRec ==='+opRec );
                    PageReference myVFPage = new PageReference('/apex/MC_ClosedSurvey');
                    myVFPage.setRedirect(true);
                    // myVFPage.getParameters().put('myId', recID );
                    return myVFPage;
                }else{
                    System.debug('===opRec ==='+opRec );
                    //For email link clikced
                    opRec[0].One_Click_Survey__c =res;
                    opRec[0].MC_EmailLinkClicked__c = true;
                    system.debug('----------opRec[0].MC_EmailLinkClicked__c----------'+opRec[0].MC_EmailLinkClicked__c);
                    system.debug('----------opRec[0].One_Click_Survey__c----------'+opRec[0].One_Click_Survey__c);
                    update opRec;
                    if(res=='Excellent'){
                        PageReference myVFPage = new PageReference('/apex/MC_ProductType_Survey_Excellent');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('id', recID );
                        myVFPage.getParameters().put('res', res );
                        return myVFPage;
                    }else if(res=='Great'){
                        PageReference myVFPage = new PageReference('/apex/MC_ProductType_Survey_good');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('id', recID );
                        myVFPage.getParameters().put('res', res );
                        return myVFPage;
                    }
                    else if(res=='Ok'){
                        PageReference myVFPage = new PageReference('/apex/MC_ProductType_Survey_ok');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('id', recID );
                        myVFPage.getParameters().put('res', res );
                        return myVFPage;
                        
                        
                    }else if(res=='Poor' && opRec[0].Is_Coaching_Failed__c==false){
                        PageReference myVFPage = new PageReference('/apex/MC_ProductType_Survey_poor');
                        myVFPage.setRedirect(true);
                        myVFPage.getParameters().put('id', recID );
                        myVFPage.getParameters().put('res', res );
                        return myVFPage;
                    }
                }
            }else{
                //To redirect to the survey already completed
                PageReference myVFPage = new PageReference('/apex/MC_ClosedSurvey');
                myVFPage.setRedirect(true);
                myVFPage.getParameters().put('NoRecordIF', 'True');
                return myVFPage;
                
            }
            if(test.isRunningTest()){
                integer a = 10/0;  
            }
        }
        catch (Exception e) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_OneClickSurvey',
                    'successRedirect',
                    'recID'+recID,
                    e
                )
            );
            
            
        }
        return null;   
        
    }
    //To update comment for the poor survey
    public void save() {
        try{
            
            recID = ApexPages.currentPage().getParameters().get('id');
            system.debug('----------recID-----------'+recID);
            res= ApexPages.currentPage().getParameters().get('res');
            system.debug('-----------res-----------------'+res);
            showServerMessage=true;
            List<Opportunity> opRec = [select id,One_Click_Survey__c,OneClick_Customer_Choice_product__c,OneClick_Customer_Chioce__c, Is_Coaching_Failed__c ,OneClick_Customer_Comment__c from Opportunity where ID=: recID];
            system.debug('--------------opRec---------------'+opRec);
            if(opRec.size() > 0) {
                
                if(selectObject!='Other'){
                    if(res=='great'){
                        opRec[0].OneClick_Customer_Choice_product__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = '';
                        system.debug('--------------------------');
                    }
                    else{
                        opRec[0].OneClick_Customer_Chioce__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = '';
                    }
                }else{
                    if(res=='great'){
                        system.debug('--------------------------');
                        opRec[0].OneClick_Customer_Choice_product__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = username;
                    }
                    else{
                        opRec[0].OneClick_Customer_Chioce__c= selectObject;
                        opRec[0].OneClick_Customer_Comment__c = username;
                    }
                }
                showServerMessage = true;
                system.debug('--------opRec---------'+opRec);
                update opRec;
            }
            
            if(test.isRunningTest()){
                integer a = 10/0;  
            }
        }catch (Exception e) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_OneClickSurvey',
                    'save',
                    'recID'+recID,
                    e
                )
            );
        }
        
    }
    //To hide and show the section
    public void changeEvent() {
        showServerMessage = false;
        System.debug('---'+selectObject);
        if(selectObject=='Other'){
            isOther =true;
        }else{
            isOther =false;
        }
        
    }
    
    
    
}