@isTest
public class ChargeStripeWebHook_Test {
    @isTest static void test(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BcOxelyfXsr0Ki');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test opp',CloseDate=system.today(),StageName='Qualification',External_ID__c='inv_123456789');
        insert opp;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/ChargeStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1BGb19DiFnu7hVq7eWM8NykH\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508884623, \"data\": { \"object\": { \"id\": \"ch_1BGb19DiFnu7hVq7Vs7GihRu\", \"object\": \"charge\", \"amount\": 2000, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1BGb19DiFnu7hVq7vq3Tm0oX\", \"captured\": true, \"created\": 1508884623, \"currency\": \"usd\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"description\": \"null\", \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": { }, \"invoice\": \"inv_123456789\", \"livemode\": false, \"metadata\": { }, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1BGb19DiFnu7hVq7Vs7GihRu/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BGE5EDiFnu7hVq7MMj5y28q\", \"object\": \"card\", \"address_city\": null, \"address_country\": null, \"address_line1\": null, \"address_line1_check\": null, \"address_line2\": null, \"address_state\": null, \"address_zip\": null, \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 8, \"exp_year\": 2019, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": { }, \"name\": null, \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_i4OseGgOylA97N\", \"idempotency_key\": null }, \"type\": \"charge.succeeded\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	ChargeStripeWebHook.ChargeStripe();
        Test.stopTest();
    }
    
    @isTest static void test2(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BcOxelyfXsr0Ki');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test opp',CloseDate=system.today(),StageName='Qualification',External_ID__c='inv_123456789',DML_From_Salesforce__c=true,AccountId=acc.Id);
        insert opp;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/ChargeStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1BGb19DiFnu7hVq7eWM8NykH\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508884623, \"data\": { \"object\": { \"id\": \"ch_1BGb19DiFnu7hVq7Vs7GihRu\", \"object\": \"charge\", \"amount\": 2000, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1BGb19DiFnu7hVq7vq3Tm0oX\", \"captured\": true, \"created\": 1508884623, \"currency\": \"usd\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"description\": \"null\", \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": { }, \"invoice\": \"inv_123456780\", \"livemode\": false, \"metadata\": { }, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1BGb19DiFnu7hVq7Vs7GihRu/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BGE5EDiFnu7hVq7MMj5y28q\", \"object\": \"card\", \"address_city\": null, \"address_country\": null, \"address_line1\": null, \"address_line1_check\": null, \"address_line2\": null, \"address_state\": null, \"address_zip\": null, \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 8, \"exp_year\": 2019, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": { }, \"name\": null, \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_i4OseGgOylA97N\", \"idempotency_key\": null }, \"type\": \"charge.succeeded\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	ChargeStripeWebHook.ChargeStripe();
        Test.stopTest();
    }
}