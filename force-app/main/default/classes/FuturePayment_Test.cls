@IsTest
public class FuturePayment_Test {
    public static String CRON_EXP = '0 0 0 15 3 ? 2030';
    
	static testMethod void testFuturePaymentBatch() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,Payment_Type__c='Nmi',Paid__c=false,Payment_Date__c=system.today(),Amount=20);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        List<OpportunityLineItem> lstOlis = new List<OpportunityLineItem>();
        lstOlis.add(oli);
        
        Test.startTest();
        Database.executeBatch(new FuturePaymentBatch(),10);
        Test.stopTest();
    }
    
    static testMethod void testFuturePaymentScheduler(){
        Test.startTest();
            System.schedule('FuturePaymentScheduler',CRON_EXP,new FuturePaymentScheduler());
        Test.stopTest();
    }
}