@IsTest
public class ChangeCaseDueDateControllerV4_Test {
    static testMethod void test() {
        AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
        //addDa.C1PM__c =1;
        //addDa.C2PM__c =1;
        //addDa.C3PM__c =1;
       //addDa.C4PM__c =1;
      //  addDa.C5PM__c =1;
        addDa.name='AddDaysToDueDateOfCase';
        insert addDa;
         MoveCaseDueDate__c mv =  new MoveCaseDueDate__c();
            mv.name='C1PM__c';
            mv.Field_Api_Name__c = 'C1PM__c';
            mv.Index__c =1;
            mv.Number_Of_Days_To_Move__c =1;
            mv.isDueDateChange__c =true;
            mv.isCaseClosed__c =true;
            
            insert mv;
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        insert tsk;
        Case cs = new Case();
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        //insert askd;
        Test.startTest();
        ChangeCaseDueDateControllerV4.getCaseRecord(cs.id);
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Facebook_Message__c');
        Test.stopTest();
    }
    static testMethod void test1() {
        AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
       //addDa.C1PM__c =1;
       // addDa.C2PM__c =1;
       // addDa.C3PM__c =1;
       //addDa.C4PM__c =1;
       // addDa.C5PM__c =1;
        addDa.name='AddDaysToDueDateOfCase';
        insert addDa;
         MoveCaseDueDate__c mv =  new MoveCaseDueDate__c();
            mv.name='C1PM__c';
            mv.Field_Api_Name__c = 'C1PM__c';
            mv.Index__c =1;
            mv.Number_Of_Days_To_Move__c =1;
             mv.isCaseClosed__c =true;
            mv.isDueDateChange__c =true;
            insert mv;
        Case cs = new Case();
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.Contact_Phone__c = '9903294994';
        cs.C1PM__c=true;
        cs.C2PM__c=true;
        cs.C3PM__c=true;
        cs.C4PM__c=true;
        cs.C5PM__c=true;
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        //insert askd;
        Test.startTest();
        ChangeCaseDueDateControllerV4.getCaseRecord(cs.id);
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Recovered_Payment__c');
        
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Call_1_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Call_2_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Call_3_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Call_4_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Call_5_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Do_Not_Contact__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Never_Answered__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'C1PM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Text_Message__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Never_Answered__c');
        // ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Stop_Correspondence__c');
        //ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Do_Not_Contact__c');
       // ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Not_recovered__c');
       // ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Text_Message__c');
        
        Test.stopTest();
    }
    static testMethod void test2() {
        AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
        //addDa.C1PM__c =1;
       // addDa.C2PM__c =1;
       // addDa.C3PM__c =1;
       // addDa.C4PM__c =1;
       // addDa.C5PM__c =1;
        addDa.name='AddDaysToDueDateOfCase';
        insert addDa;
         MoveCaseDueDate__c mv =  new MoveCaseDueDate__c();
            mv.name='C1PM__c';
            mv.Field_Api_Name__c = 'C1PM__c';
            mv.Index__c =1;
             mv.isCaseClosed__c =true;
            mv.Number_Of_Days_To_Move__c =1;
            mv.isDueDateChange__c =true;
            insert mv;
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Case cs = new Case();
        cs.C1PM__c=false;
        cs.C2PM__c=false;
        cs.C3PM__c=false;
        cs.C4PM__c=false;
        cs.C5PM__c=false;
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = ct.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        //insert askd;
        Test.startTest();
        ChangeCaseDueDateControllerV4.getCaseRecord(cs.id);
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'C1PM__c');
        Test.stopTest();
    }
    static testMethod void test3() {
        AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
        //addDa.C1PM__c =1;
        //addDa.C2PM__c =1;
        //addDa.C3PM__c =1;
        //addDa.C4PM__c =1;
        //addDa.C5PM__c =1;
        addDa.name='AddDaysToDueDateOfCase';
        insert addDa;
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Case cs = new Case();
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = ct.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        //insert askd;
        Test.startTest();
        ChangeCaseDueDateControllerV4.getCaseRecord(cs.id);
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'true', 'Saved_Payment__c');
         ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Recovered_Payment__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'C1PM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Call_2_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Call_3_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Call_4_AM__c');
        ChangeCaseDueDateControllerV4.saveCase(cs.id, 'false', 'Call1PM__c');
        ChangeCaseDueDateControllerV4.updateCaseStatusClosed(cs, 'true', 'Text_Message__c');
         ChangeCaseDueDateControllerV4.updateCaseStatusClosed(cs, 'true', 'Saved_Payment__c');
         ChangeCaseDueDateControllerV4.updateCaseStatusClosed(cs, 'true', 'Recovered_Payment__c');
        ChangeCaseDueDateControllerV4.updateCaseStatus(cs,'true');
        ChangeCaseDueDateControllerV4.recoveredPayment(cs,'true');
        ChangeCaseDueDateControllerV4.getCase(cs.id);
        ChangeCaseDueDateControllerV4.getCallPMApiNames();
        ChangeCaseDueDateControllerV4.getfields();
        Test.stopTest();
    }
}