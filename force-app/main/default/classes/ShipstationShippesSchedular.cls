global class ShipstationShippesSchedular implements Schedulable {
   global void execute(SchedulableContext sc) {
      ShipstationOrderStatusShippedBatch b = new ShipstationOrderStatusShippedBatch();
      database.executebatch(b);
   }
}