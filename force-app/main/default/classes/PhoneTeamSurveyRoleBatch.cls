global class PhoneTeamSurveyRoleBatch implements Database.Batchable<sobject>,Schedulable {
    
    global database.querylocator start(database.batchablecontext bc){
        return Database.getQueryLocator([Select id, Call_Duration__c,Salesforce_ID__c FROM Phone_2_minute_Survey_agents__c]);
    }
    global void execute(SchedulableContext ctx) {
        PhoneTeamSurveyRoleBatch ptbatch = new PhoneTeamSurveyRoleBatch();
        database.executebatch(ptbatch,200);
    } 
    
    
    global void execute(database.BatchableContext bc,list<Phone_2_minute_Survey_agents__c > scope){
        Map<String,Decimal> idDurationMap = new Map<String,Decimal>();
        for(Phone_2_minute_Survey_agents__c p: scope){
            idDurationMap.put(p.Salesforce_ID__c,p.Call_Duration__c);
        }
        List<User> userlst = [SELECT id,username,UserRole.Name,Name FROM USER where UserRole.Name in ('Phone Team Agent','Team Leader-Phone','Coaching Facilitator','Coaching Liaison','Results Facilitator','Results Liaison','PT Admin Agent')];
        List<Phone_2_minute_Survey_agents__c>  Phonelst = new List<Phone_2_minute_Survey_agents__c>();
        for(User us:userlst){
            if(!idDurationMap.containsKey(us.id)){
                
                Phone_2_minute_Survey_agents__c p = new Phone_2_minute_Survey_agents__c();
                p.Call_Duration__c = 120;
                p.Salesforce_ID__c = us.Id;
                p.name = us.name;
                Phonelst.add(p);
            }
        }
        if(Phonelst.size()>0){
        insert Phonelst;
        }
        
    }
    
    global void finish(database.batchablecontext bc){
        
    }
    
}