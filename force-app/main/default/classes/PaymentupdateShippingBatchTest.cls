@isTest
public class PaymentupdateShippingBatchTest {
    @isTest 
    public static void test1(){
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        
        Dynamic_Item_Count_Cost__c dicc = new Dynamic_Item_Count_Cost__c();
        dicc.Cost_Per_Item__c = 0.1;
        dicc.Fix_cost__c = 1.25;
        dicc.max_count__c  = 4;
        dicc.min_count__c = 1;
        insert dicc;
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        prod.DNC_Shipping_Packing__c = false;
        prod.Dynamic_Fulfillment_Item_Count__c =1 ;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'ch_dkjfnfjnj';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        insert oL1;
        PaymentupdateShippingBatch p = new PaymentupdateShippingBatch();
        Database.executeBatch(p);
    }
}