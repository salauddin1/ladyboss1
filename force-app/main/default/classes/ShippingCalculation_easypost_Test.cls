@isTest
public class ShippingCalculation_easypost_Test {
    @isTest
    public static void insertTest1(){
        
        Account acc = new Account();
        acc.Name = 'Laptop X200';
        acc.ShippingStreet = 'testing';
        acc.ShippingCity = 'asdf';
        acc.ShippingState = 'asdfgg';
        acc.ShippingPostalCode = '1425';
        acc.ShippingCountry = 'iiinnnd';
        acc.Phone = '123456789';
        acc.Email__c = 'test@test.com';
        insert acc;
       
        List<Id> prodlst = new List<Id>();
        
        Product2 prod = new Product2();
        prod.Name = 'Laptop X200';
        prod.Family = 'Hardware';
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        prod.Height__c = 25;
        prod.Weight__c = 50;
        prod.Length__c = 30;
        prod.Width__c = 15;
        prod.IsBundledProduct__c = false;
        insert prod;
        prod.Stripe_Product_Id__c= 'abc';
        prodlst.add(prod.Id);
        
        
        test.startTest();
        ShippingCalculation_easypost se = new ShippingCalculation_easypost();
        ShippingCalculation_easypost.getShipping('Laptop X200', 'testing', 'asdf', 'asdfgg', '1425', 'iiinnnd', '123456789', 'test@test.com', prodlst);
        test.stopTest();
    }
    
    @isTest
    public static void insertTest2(){
        
        Account acc = new Account();
        acc.Name = 'Laptop X200';
        acc.ShippingStreet = 'testing';
        acc.ShippingCity = 'asdf';
        acc.ShippingState = 'asdfgg';
        acc.ShippingPostalCode = '1425';
        acc.ShippingCountry = 'iiinnnd';
        acc.Phone = '123456789';
        acc.Email__c = 'test@test.com';
        insert acc;
       
        List<Id> prodlst = new List<Id>();
        
        Product2 prod = new Product2();
        prod.Name = 'Laptop X200';
        prod.Family = 'Hardware';
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        prod.Height__c = 25;
        prod.Weight__c = 50;
        prod.Length__c = 30;
        prod.Width__c = 15;
        prod.IsBundledProduct__c = true;
        insert prod;
        prod.Stripe_Product_Id__c= 'abc';
        prodlst.add(prod.Id);
        
        
        test.startTest();
        ShippingCalculation_easypost se = new ShippingCalculation_easypost();
        ShippingCalculation_easypost.getShipping('Laptop X200', 'testing', 'asdf', 'asdfgg', '1425', 'iiinnnd', '123456789', 'test@test.com', prodlst);
        test.stopTest();
    }

}