/*
 * Developer Name : Tirth Patel
 * Description : This class for charge id and make consider for commission as true on oli
 */
global class PaymentProcessor {
    public static void paymentProcess(List<id> oliToUpdateIDs){
        List<OpportunityLineItem> oliToUpdate = [SELECT id,DNC_Ship_Pack__c,Opportunity.recordType.name,Subscription_Id__c,opportunity.stripe_charge_id__c,Consider_For_Commission__c,Month_Count__c,current_commissionable_amount__c,Dynamic_Item_Count_Cost__c,Product_Number_Count__c,Quantity,refund__c,stripe_charge_id__c,TotalPrice,Total_Commissionable_amount__c,Product2.Shipping_cost_1__c,Product2.Shipping_cost_2__c,Product2.Shipping_cost_3__c,Product2.Shipping_cost_4__c,Product2.Shipping_cost_5__c,Product2.Dynamic_Fulfillment_Item_Count__c,Product2Id FROM OpportunityLineItem where id IN : oliToUpdateIDs] ;
        Decimal oliItemCount = 0;
        Decimal oliDynamicCountCost = 0;
        Set<id> Product_withmonth = new Set<id>();
        for(OpportunityLineItem ol : oliToUpdate){
            if(ol.Month_Count__c>=4 && ol.DNC_Ship_Pack__c== false ){
                Product_withmonth.add(ol.Product2Id);
            }
        }
        Map<id,List<Decimal>> prod_shipMap = new Map<id,List<Decimal>>();
        Map<id,Decimal> prod_dyItemcnt = new Map<id,Decimal>();
        for(OpportunityLineItem ol : oliToUpdate){
            if(ol.DNC_Ship_Pack__c == false){
                List<Decimal> prList = new List<Decimal>();
                if(ol.Product2.Shipping_cost_1__c!=null){
                    prList.add(ol.Product2.Shipping_cost_1__c);
                }else{
                    prList.add(0);
                }
                if(ol.Product2.Shipping_cost_2__c!=null){
                    prList.add(ol.Product2.Shipping_cost_2__c);
                }else{
                    prList.add(0);
                }
                if(ol.Product2.Shipping_cost_3__c!=null){
                    prList.add(ol.Product2.Shipping_cost_3__c);
                }else{
                    prList.add(0);
                }
                if(ol.Product2.Shipping_cost_4__c!=null){
                    prList.add(ol.Product2.Shipping_cost_4__c);
                }else{
                    prList.add(0);
                }
                if(ol.Product2.Shipping_cost_5__c!=null){
                    prList.add(ol.Product2.Shipping_cost_5__c);
                }else{
                    prList.add(0);
                }
                prod_shipMap.put(ol.Product2Id,prList);
                if(ol.Product2.Dynamic_Fulfillment_Item_Count__c == null){
                    prod_dyItemcnt.put(ol.Product2Id,0);
                }else{
                    prod_dyItemcnt.put(ol.Product2Id, ol.Product2.Dynamic_Fulfillment_Item_Count__c);
                }
            }
        }
        for(OpportunityLineItem ol : oliToUpdate){
            if(ol.DNC_Ship_Pack__c==false){
                oliItemCount = oliItemCount + ol.Quantity*prod_dyItemcnt.get(ol.Product2Id);
            }
        }
        Dynamic_Item_Count_Cost__c dic = [SELECT Cost_Per_Item__c,Fix_cost__c,max_count__c,min_count__c FROM Dynamic_Item_Count_Cost__c];
        if(oliItemCount>dic.max_count__c){
            oliDynamicCountCost = dic.Fix_cost__c + (oliItemCount - dic.max_count__c)*dic.Cost_Per_Item__c;
        }else if (oliItemCount>=dic.min_count__c){
            oliDynamicCountCost = dic.Fix_cost__c;
        }
        if(prod_dyItemcnt.size()==0){
            oliDynamicCountCost = oliDynamicCountCost/1;
        }else{
            oliDynamicCountCost = oliDynamicCountCost/prod_dyItemcnt.size();
        }
        Map<id,Integer> prod_shipindex = new Map<id,Integer>();
        for(id pm : Product_withmonth){
            prod_shipindex.put(pm,0); 
            prod_shipMap.remove(pm);
        }
        integer st_index=0,index_start=0;
        Integer  loopCount = prod_shipMap.size();
        if(Product_withmonth.size()>0){
            st_index=1;
            index_start = 1;
            loopCount++;
        }
        System.debug('prod_shipMap : '+prod_shipMap);
        if(prod_shipMap.size()>0){
            for(integer i=index_start; i<loopCount;i++){
                Decimal max=0;
                String maxPrId = '';
                for(id ps : prod_shipMap.keySet()){
                    if(prod_shipMap.get(ps).get(i) >= max){
                        max = prod_shipMap.get(ps).get(i);
                        maxPrId = ps;
                    }
                }
                prod_shipindex.put(maxPrId,st_index);
                prod_shipMap.remove(maxPrId);
                st_index++;
            }
        }
        System.debug('prod_shipindex : '+prod_shipindex);
        System.debug('oliToUpdate : '+oliToUpdate);
        System.debug('oliDynamicCountCost : '+oliDynamicCountCost);
        for(OpportunityLineItem oppLineItem : oliToUpdate){
            System.debug('consider for commission');
            if((oppLineItem.Opportunity.stripe_charge_id__c!=null && oppLineItem.Opportunity.recordType.name=='charge')||(oppLineItem.Opportunity.recordType.name=='Subscription' && (oppLineItem.Stripe_charge_id__c!=null||oppLineItem.Subscription_Id__c!=null ))){ //||(oppLineItem.Opportunity.recordType.name=='Subscription' && oppLineItem.Stripe_charge_id__c!=null)
               System.debug('consider for commission true');
                oppLineItem.Consider_For_Commission__c = true;
            }
            if(oppLineItem.DNC_Ship_Pack__c == false){
                oppLineItem.Product_Number_Count__c = prod_shipindex.get(oppLineItem.Product2Id)+1;
                oppLineItem.Dynamic_Item_Count_Cost__c = oliDynamicCountCost;
            }else{
                oppLineItem.Product_Number_Count__c = 0;
                oppLineItem.Dynamic_Item_Count_Cost__c = 0;
            }
        }
        if(oliToUpdate.size()>0){
            try{
                PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
                upsert oliToUpdate;
                System.debug(oliToUpdate);
            }catch(Exception e){
                ApexDebugLog apex=new ApexDebugLog();
            	apex.createLog(new ApexDebugLog.Error('PaymentProcessor','paymentProcess',oliToUpdate[0].id,e));
            }
        }
    }
    
    @future(callout=true)
    public static void paymentProcess_future(List<id> oliToUpdateIDs){
        System.debug('oliToUpdateIDs : '+oliToUpdateIDs);
        paymentProcess(oliToUpdateIDs);
    }
}