public class ShipstationOrderUpdateBatchIterable implements iterable<Integer>{
   public Iterator<Integer> Iterator(){
      return new ShipstationOrderUpdateBatchIterator();
   }
}