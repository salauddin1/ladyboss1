global class GetOrdersFromShopify implements Database.batchable<Integer>,Database.AllowsCallouts,Database.Stateful{
    global Static Boolean flag = false;
    global Integer pagenumber=0;
    global Iterable<Integer> start(Database.batchableContext info){
       return new orderIterable(); 
   }      
    global void execute(Database.BatchableContext info,List<Integer> scope){
        pagenumber=scope[0];
        String FaieldOrderId;
        String accessToken;
        String conId;
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        try{
            HttpRequest req = new HttpRequest();
            Http htp =new Http();
            HttpResponse res= new HttpResponse();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders.json?status=any&limit=1&page='+pagenumber);
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            res= htp.send(req);
            System.debug('=================res.body====='+res.getBody());
            Map<String, object> result = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            List<Object> orders = (List<object>)result.get('orders');
           System.debug('=============orders==========='+orders);
            Map<String, object> orderMap = new Map<String, object> ();
            Map<String, object> Customer = new Map<String, object> ();
            Set<String> emailset = new Set<String>();
            Set<String> orderId = new Set<String>();
            Set<String> conemail = new set<String>();
            set<Contact> insCustomerSet = new set<Contact>();
            List<Contact> insCustomer = new List<Contact>();
            Integer i=0;
            if(!orders.isEmpty()){
                for(Object ord: orders){
                    orderMap=(Map<String, object>)ord;
                    orderId.add(String.valueOf(orderMap.get('id')));
                    Customer =(Map<String, object>)orderMap.get('customer');
                    emailset.add(String.valueOf(customer.get('email')));
                }
                System.debug('=========emailset========'+emailset);
            }
            List<Contact> conlist = [select id,email from Contact where email IN: emailset];
            for(contact con : conlist){
                conemail.add(String.valueOf(con.email).toLowerCase());
            }   
            for(Object ord: orders){
                orderMap=(Map<String, object>)ord;
                Customer =(Map<String, object>)orderMap.get('customer');
                Map<String, Object> addressArray =  (Map<String, Object>)orderMap.get('shipping_address');
                sYSTEM.debug('================'+conemail+'========'+String.valueOf(customer.get('email')));
                if(!conemail.contains(String.valueOf(customer.get('email')).toLowerCase())) {
                    String lastname ='';
                    lastname = String.valueOf(Customer.get('last_name'));
                    Contact shopifycontact = new Contact();
                    if( lastname == ''){
                        shopifycontact.LastName ='missing';
                    }
                    else {
                        shopifycontact.LastName = lastname;
                    }
                    shopifycontact.FirstName = String.valueOf(Customer.get('first_name'));
                    shopifycontact.Processed_from_Shopify_Batch__c = true;
                    shopifycontact.Email = String.valueOf(customer.get('email'));
                    shopifycontact.shopify_customer__c = true;
                    if(orderMap.containsKey('shipping_address')) {
                        shopifycontact.Phone = String.valueOf(addressArray.get('phone'));
                        shopifycontact.MailingStreet = String.valueOf(addressArray.get('address1'));
                        shopifycontact.MailingCity = String.valueOf(addressArray.get('city'));
                        shopifycontact.MailingCountry = String.valueOf(addressArray.get('country'));
                        shopifycontact.MailingState = String.valueOf(addressArray.get('province'));
                        shopifycontact.MailingPostalCode = String.valueOf(addressArray.get('zip'));
                        insCustomerSet.add(shopifycontact);
                        System.debug('=====insCustomerSet=='+insCustomerSet);
                    }
                    conemail.add(String.valueOf(customer.get('email')));
                }
            }
            for(contact con : insCustomerSet){
                insCustomer.add(con);
            }
            flag = true;
            if(!insCustomer.isEmpty()) {
                insert insCustomer;
            }
            System.debug('=========insCustomer=============='+insCustomer);
            List <Shopify_Orders__c> orderList = new List<Shopify_Orders__c>();
            List<ShopifyLineItem__c> productItems   = new List<ShopifyLineItem__c>();
            List<Shopify_Orders__c> existingOrders  = [select id, Order_Id__c from Shopify_Orders__c where Order_Id__c IN : orderId];
            Set<String> existingOrderId = new Set<String>();
            for(Shopify_Orders__c ordId : existingOrders) {
                existingOrderId.add(ordId.Order_Id__c);
            }
             System.debug('=====orderMap======='+orderMap);
            if(!orders.isEmpty()){
                for(Object ordre: orders){
                    orderMap=(Map<String, object>)ordre;
                    FaieldOrderId = String.valueOf(orderMap.get('id'));
                    Customer =(Map<String, object>)orderMap.get('customer');
                    Map<String, Object> addressArray =  (Map<String, Object>)orderMap.get('shipping_address');
                    String emails = String.valueOf(customer.get('email'));
                    System.debug('================'+existingOrderId+'======='+String.valueOf(orderMap.get('id')));
                    if(!existingOrderId.contains(String.valueOf(orderMap.get('id')))) {
                        System.debug('================'+existingOrderId+'======='+String.valueOf(orderMap.get('id')));
                        Shopify_Orders__c ord = new Shopify_Orders__c();
                        for(Contact con : insCustomer) {
                            if(con.Email == emails && !conlist.contains(con)) {
                                ord.Contact__c = con.Id;
                            }
                        }
                        for(Contact con : conlist){
                            if(con.Email == emails && !insCustomer.contains(con)) {
                                ord.Contact__c = con.Id;
                            }      
                        }
                        conId = String.valueOf(ord.Contact__c);
                        ord.Processed_from_Shopify_Batch__c = true;
                        ord.Order_Id__c                 = String.valueOf(orderMap.get('id'));
                        ord.Order_Number__c             = String.valueOf(orderMap.get('order_number'));
                        ord.Financial_Status__c         = String.valueOf(orderMap.get('financial_status'));
                        ord.Total_Line_Items_Price__c   = Double.valueOf(orderMap.get('total_line_items_price'));
                        ord.Total_Price__c              = Double.valueOf(orderMap.get('total_price'));
                        ord.Total_Tax__c                = Double.valueOf(orderMap.get('total_tax'));
                        ord.Subtotal_Price__c           = Double.valueOf(orderMap.get('subtotal_price'));
                        if(String.valueOf(orderMap.get('cancelled_at')) == null) {
                        	ord.Status__c                   = 'Confirmed';
                        }
                        else {
                            ord.Status__c                   = 'canceled';
                        }
                        ord.Email__c                    = String.valueOf(orderMap.get('email'));
                        ord.created_at__c               = String.valueOf(orderMap.get('created_at'));
                        ord.fulfillment_Status__c       = String.valueOf(orderMap.get('fulfillment_status'));
                        if(orderMap.containsKey('shipping_address')) {
                            ord.First_Name__c               = String.valueOf(addressArray.get('first_name'));
                            ord.Last_Name__c                = String.valueOf(addressArray.get('last_name'));
                            ord.address1__c                 = String.valueOf(addressArray.get('address1'));
                            ord.city__c                     = String.valueOf(addressArray.get('city'));
                            ord.province__c                 = String.valueOf(addressArray.get('province'));
                            ord.country__c                  = String.valueOf(addressArray.get('country'));
                            ord.phone__c                    = String.valueOf(addressArray.get('phone'));
                            ord.zip__c                      = String.valueOf(addressArray.get('zip'));
                        }
                        orderList.add(ord);
                    }
                ShopifyOrderCreation.isrecursive = true;
                if(!orderList.isEmpty()) {
                    insert orderList;
                }
                Set<String> itemId = new Set<String> ();
                Set<String> shopOrdId = new Set<String> ();
                for(Shopify_Orders__c Shopiorder:existingOrders){
                    shopOrdId.add(Shopiorder.id);
                }
                List<ShopifyLineItem__c> exestingItems = [select id,Item_Id__c from ShopifyLineItem__c where Shopify_Orders__c IN : shopOrdId];
                for(ShopifyLineItem__c orderitem:exestingItems) {
                        itemId.add(orderitem.Item_Id__c);
                }
                Map<String, Shopify_Orders__c> shopOrders = new Map<String, Shopify_Orders__c>();
                for(Shopify_Orders__c ord : orderList) {
                    shopOrders.put(ord.Order_Id__c, ord);
                }
                Map<String, Object> itemMap             = new Map<String, Object>();
                Map<String, Object> fulfillmentMap             = new Map<String, Object>();
                if(!orders.isEmpty()){
                    for(Object ordr: orders){
                        String fulfillmentStatus;
                        orderMap=(Map<String, object>)ordr;
                        Shopify_Orders__c ordid = shopOrders.get(String.valueOf(orderMap.get('id')));
                        List<Object> fulfillments = (List<Object>)orderMap.get('fulfillments');
                        if(!fulfillments.isEmpty()){
                            for(Object fulfilmnt : fulfillments) {
                                fulfillmentMap = (Map<String, Object>)fulfilmnt;
                                fulfillmentStatus = String.valueOf(fulfillmentMap.get('status'));
                                System.debug('===fulfillmentStatus======='+fulfillmentStatus);
                            }
                        }
                        List<Object> LineItems = (List<Object>)orderMap.get('line_items');
                        if(!itemId.contains(String.valueOf(itemMap.get('id')))) {
                            for(Object obj : LineItems) {
                                itemMap = (Map <String, Object>)obj;
                                ShopifyLineItem__c shopfyItems = new ShopifyLineItem__c();
                                if(ordid != null) {
                                    shopfyItems.Shopify_Orders__c       = ordid.Id;
                                }
                                shopfyItems.Item_Id__c              = String.valueOf(itemMap.get('id'));
                                shopfyItems.Price__c                = Double.valueOf(itemMap.get('price'));
                                shopfyItems.Quantity__c             = Double.valueOf(itemMap.get('quantity'));
                                shopfyItems.sku__c                  = String.valueOf(itemMap.get('sku'));
                                shopfyItems.Title__c                = String.valueOf(itemMap.get('title')); 
                                shopfyItems.Name                    = String.valueOf(itemMap.get('title')); 
                                System.debug('======conId====='+conId);
                                shopfyItems.Contact__c              = conId;
                                shopfyItems.Fulfillable_Quantity__c = Double.valueOf(itemMap.get('fulfillable_quantity'));
                                if(fulfillmentStatus != null && fulfillmentStatus != ''){
                                   shopfyItems.Status__c =fulfillmentStatus;
                                }
                                else{
                                    shopfyItems.Status__c = 'Unfulfilled';
                                }
                                productItems.add(shopfyItems);
                            }
                        }
                    }
                    if(!orderList.isEmpty()) {
                        insert productItems;
                    }
                }
              }
           }
        }
        catch(Exception e){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'patidarsurendra69@gmail.com' };
            message.optOutPolicy = 'FILTER';
            message.subject = 'Please pheck your field order';
            message.plainTextBody = 'Hi Surendra please Check your failed orders Causes of This:- '+ e +'\n See page Number======:'+pagenumber+'\n OrderId : ' +FaieldOrderId+ '\n Please Process again this orders by batch';
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        }
    }
    global void finish(Database.batchableContext info){  
        
   }
}