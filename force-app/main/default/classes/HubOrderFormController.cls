public without sharing class HubOrderFormController {
    @AuraEnabled
    public static List<Productwrapper> getProducts(String conID){
        List<productWrapper> pwWrapper = new List<productWrapper>();
        List<OpportunityLineItem> prodList = [SELECT id, Product2.Name,Product2.Tax_Code__c,Status__c,End__c,TotalPrice,Tax_Amount__c,Shipping_Cost__c,Product2.ImageUrl__c,Product2.Stripe_Plan_Id__c,Product2.Custom_Transformation_System__c,Product2.Description,Opportunity.Card_Customer_ID__c,opportunity.Card__r.Last4__c FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND (Status__c = 'Active' OR Status__c = 'trialing' OR Status__c = 'Past Due')];
        for (OpportunityLineItem prod : prodList) {
            productWrapper pw = new productWrapper();
            pw.Id = prod.Id;
            pw.Name = prod.Product2.Name;
            pw.checkValue=false;
            pw.totalPrice = prod.TotalPrice;
            pw.nextChargeDate = prod.End__c;
            pw.TaxCode =prod.Product2.Tax_Code__c;
            pw.subStatus = prod.Status__c;
            pw.Last4Digit = prod.opportunity.Card__r.Last4__c;
            pw.CustomerID = prod.opportunity.Card_Customer_ID__c;
            if (prod.Tax_Amount__c != null) {
                pw.taxAmount = prod.Tax_Amount__c;
            }else {
                pw.taxAmount = 0.00;
            }
            if (prod.Shipping_Cost__c != null) {
                pw.shippingCost = prod.Shipping_Cost__c;
            }else {
                pw.shippingCost = 0.00;
            }
            if (prod.Product2.ImageUrl__c != null) {
                pw.image = prod.Product2.ImageUrl__c;
            }else {
                pw.image = 'LB_Memberships.png';
            }
            pw.planId = prod.Product2.Stripe_Plan_Id__c;
            
            if (prod.Product2.Name.contains('BURN')) {
                pw.title = 'BURN CLUB';
                pw.originalCost = 47;
            }else if (prod.Product2.Name.contains('REST')) {
                pw.title = 'REST CLUB';
                pw.originalCost = 47;
            } else if (prod.Product2.Name.contains('RECOVER')) {
                pw.title = 'RECOVER CLUB';
                pw.originalCost = 47;
            }else if (prod.Product2.Name.contains('FUEL')) {
                pw.title = 'FUEL CLUB';
                pw.originalCost = 47;
            }else if (prod.Product2.Name.contains('LEAN')) {
                pw.title = 'LEAN CLUB';
            }else if (prod.Product2.Name.contains('Transformation System')) {
                pw.title = 'Transformation System CLUB';
                pw.originalCost = 237;
            }else if (prod.Product2.Name.contains('GREENS')) {
                pw.title = 'LADYBOSS GREENS';
            }
            
            if (prod.Product2.Custom_Transformation_System__c) {
                pw.transformationProd = true;
                pw.title = 'Custom Transformation System CLUB';
                pw.originalCost = 237;
            }
            if (prod.Product2.Description != null) {
                pw.description = prod.Product2.Description;
            }
            pwWrapper.add(pw);
            
        }
        return pwWrapper;
    }
    @AuraEnabled
    public static String CancelSubscriptions(String prod){
        List<productWrapper> pwWrapper = (List<productWrapper>)System.JSON.deserialize(prod, List<productWrapper>.class);
        List<OpportunityLineItem> olitoUpdatelist = new List<OpportunityLineItem>();
        String returnSuccess='';
        for (productWrapper p : pwWrapper) {
            List<OpportunityLineItem> oli = [select id,Subscription_Id__c,opportunityId,Cancelled_from_Hub_on__c from OPPORTUNITYLINEITEM WHERE id=:p.Id limit 1];
            Opportunity opp = [select id,Success_Failure_Message__c from Opportunity where Id =:oli[0].opportunityId];
            HttpRequest http = new HttpRequest();  
            http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli[0].Subscription_Id__c);
            http.setMethod('DELETE');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            try{
                if(!Test.isRunningTest()){
                    hs = con.send(http);
                }else{
                    hs.setStatusCode(200);
                    hs.setBody('{ "id": "sub_6alcFSxUKC6fK2", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1437272451, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": 1570198596, "collection_method": "charge_automatically", "created": 1436667651, "current_period_end": 1437272451, "current_period_start": 1436667651, "customer": "cus_Fvj2BXjyhiNikV", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": 1436668036, "items": { "object": "list", "data": [ { "id": "si_18SmzgFzCf73siP0qW4m5WEC", "object": "subscription_item", "billing_thresholds": null, "created": 1436667652, "metadata": {}, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_6alcFSxUKC6fK2", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_6alcFSxUKC6fK2" }, "latest_invoice": null, "livemode": false, "metadata": {}, "pending_setup_intent": null, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1436667651, "start_date": 1436667651, "status": "canceled", "tax_percent": null, "trial_end": 1437272451, "trial_start": 1436667651 }');
                }
            } 
            catch(Exception e) {
                System.debug('------------ Exception---------------'+e);   
            }
            Integer statusCode = hs.getStatusCode();
            
            if (hs.getstatusCode() == 200){
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                if(String.valueOf(results.get('status')).Contains('canceled')) {
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    oli[0].Status__c = 'InActive';
                }else 
                {
                    oli[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
                }
                oli[0].Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));
                oli[0].Cancelled_from_Hub_on__c = DateTime.now();
                olitoUpdatelist.add(oli[0]);
                
                returnSuccess= 'success';
                
            }else{
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                returnSuccess= String.valueOf(errorMap.get('message'));
                if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    olitoUpdatelist.add(oli[0]);
                }
                
            }
        }
        if (olitoUpdatelist.size()>0) {
            update olitoUpdatelist;
        }
        return returnSuccess;
        
    }
    
    public static void EmailReminder(String conID){
        List<OpportunityLineItem> prodList = [SELECT id, Product2.Name,Status__c,TotalPrice,Remind_Me_Later__c,Tax_Amount__c,Shipping_Cost__c,Product2.ImageUrl__c,Product2.Stripe_Plan_Id__c FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND Status__c = 'Active' ORDER BY End__c];
        
        for(OpportunityLineItem oli:prodList) {
            oli.Remind_Me_Later__c = true;
        }
        if (prodList.size()>0) {
            update prodList;
        }
    }
    public class productWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String Id;
        @AuraEnabled public Boolean checkValue;
        @AuraEnabled public Boolean transformationProd = false;
        @AuraEnabled public Decimal totalPrice;
        @AuraEnabled public Decimal taxAmount;
        @AuraEnabled public Decimal taxpercentage;
        @AuraEnabled public Decimal shippingCost;
        @AuraEnabled public Decimal originalCost;
        @AuraEnabled public String image;
        @AuraEnabled public String planId;
        @AuraEnabled public String taxCode;
        @AuraEnabled public String title;
        @AuraEnabled public String description;
        @AuraEnabled public Boolean switchProduct;
        @AuraEnabled public Boolean showProduct = true;
        @AuraEnabled public Boolean customTansformationProduct = false;
        @AuraEnabled public List<productWrapper> pwList = new List<productWrapper>();
        @AuraEnabled public String numOfBags;
        @AuraEnabled public String listStatus;
        @AuraEnabled public Date nextChargeDate;
        @AuraEnabled public String subStatus;
        @AuraEnabled public String Last4Digit;
        @AuraEnabled public String CustomerID;
    }
    public class customWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public Integer quantity;
    }
    public class cardWrapper{
        @AuraEnabled public List<Card__c> cardList = new List<Card__c>();
        @AuraEnabled public string Address;
        @AuraEnabled public Address__c AddressValue;
    }
    @AuraEnabled
    public static List<productWrapper> getAllProducts(){
        List<productWrapper> pwWrapperList = new List<productWrapper>();
        List<productWrapper> productList = new List<productWrapper>();
        List<productWrapper> leanWrapperList = new List<productWrapper>();
        List<productWrapper> greenWrapperList = new List<productWrapper>();
        List<productWrapper> transWrapperList = new List<productWrapper>();
        
        productWrapper leanwrapper = new productWrapper();
        leanwrapper.Name = 'LEAN Membership';
        leanwrapper.image = 'LEAN1.png';
        leanwrapper.Id = 'leanbag';
        leanwrapper.description = 'A Ridiculously Delicious Protein Shake For Women That Supports Fat Loss & Immune System Health While Also Providing Numerous Essential Vitamins And Minerals! This creamy, delicious, premium nutritional shake mix will have your taste buds exploding with joy!';
        leanwrapper.title = 'LEAN CLUB';
        leanwrapper.originalCost = 57.95;  
        leanwrapper.totalPrice = 49.26;  
        leanwrapper.TaxCode = '40020';
        
        productWrapper Greenwrapper = new productWrapper();
        Greenwrapper.Name = 'LadyBoss GREENS Membership';
        Greenwrapper.image = 'GREENS1.png';
        Greenwrapper.Id = 'Greenbags';
        Greenwrapper.description = 'Taste the pie! A Delicious Green Juice SuperDrink Powder That Can Be Enjoyed With Your Morning Shake, By Itself, Or Mixed With Your Favorite Beverage. This light, delicious, premium SuperDrink packed with nature’s best superfoods will have your taste buds skipping into the sunset!';
        Greenwrapper.title = 'GREENS CLUB';
        Greenwrapper.originalCost = 57.95;  
        Greenwrapper.totalPrice = 49.26; 
        Greenwrapper.TaxCode = '40020';
        for (Product2 prod : [SELECT id,Price__c,ImageUrl__c,Stripe_Plan_Id__c,Tax_Code__c,Name,Description From Product2 WHERE Available_For_Hub__c = true Order BY Name ASC]) {
            productWrapper pw = new productWrapper ();
            pw.Id = prod.Id;
            pw.planId = prod.Stripe_Plan_Id__c;
            pw.totalPrice = prod.Price__c;
            pw.Name = prod.Name;
            if (prod.ImageUrl__c != null) {
                pw.image = prod.ImageUrl__c;
            }else {
                pw.image = 'LB_Memberships.png';
            }
            if (prod.Description != null) {
                pw.description = prod.Description;
            }
            pw.switchProduct = false;
            pw.TaxCode = prod.Tax_Code__c;
            if (prod.Name.contains('LEAN-1') || prod.Name.contains('LEAN-2') || prod.Name.contains('LEAN-3')) {
                if (prod.Name.contains('LEAN-1')) {
                    pw.numOfBags = '1';
                }else if (prod.Name.contains('LEAN-2')) {
                    pw.numOfBags = '2';
                }else if (prod.Name.contains('LEAN-3')) {
                    pw.numOfBags = '3';
                }            
                leanwrapper.switchProduct = true;
                leanWrapperList.add(pw);
            } else if (prod.Name.contains('GREENS-1') || prod.Name.contains('GREENS-2') || prod.Name.contains('GREENS-3')) {
                if (prod.Name.contains('GREENS-1')) {
                    pw.numOfBags = '1';
                }else if (prod.Name.contains('GREENS-2')) {
                    pw.numOfBags = '2';
                }else if (prod.Name.contains('GREENS-3')) {
                    pw.numOfBags = '3';
                }              
                Greenwrapper.switchProduct = true;
                greenWrapperList.add(pw);
            } else {
                if (prod.Name.contains('BURN')) {
                    pw.title = 'BURN CLUB';
                    pw.originalCost = 47;
                }else if (prod.Name.contains('REST')) {
                    pw.title = 'REST CLUB';
                    pw.originalCost = 47;
                } else if (prod.Name.contains('RECOVER')) {
                    pw.title = 'RECOVER CLUB';
                    pw.originalCost = 47;
                }else if (prod.Name.contains('FUEL')) {
                    pw.title = 'FUEL CLUB';
                    pw.originalCost = 47;
                }else if (prod.Name.contains('Transformation')) {
                    pw.title = 'Transformation System CLUB';
                    pw.originalCost = 237;
                }else if (prod.Name.contains('Experience')) {
                    pw.title = prod.Name;
                    pw.originalCost = 27;
                }
                productList.add(pw);
            }
            
        }
        leanwrapper.pwList = leanWrapperList;
        greenwrapper.pwList = greenWrapperList;
        pwWrapperList.add(leanwrapper);
        pwWrapperList.add(greenwrapper);
        pwWrapperList.addAll(productList);          
        
        return pwWrapperList;
    }
    @AuraEnabled
    public static List<productWrapper> checkPayment(String prod,String alreadySubscribed){
        List<productWrapper> hubProducts = getAllProducts();
        List<String> idListHub = new List<String>();
        for (productWrapper p : hubProducts) {
            idListHub.add(p.planId);
        }
        List<productWrapper> cancelList = new List<productWrapper>();
        List<productWrapper> addList = new List<productWrapper>();
        List<productWrapper> prodList = new List<productWrapper>();
        List<productWrapper> pwWrapperChange = (List<productWrapper>)System.JSON.deserialize(prod, List<productWrapper>.class);
        List<productWrapper> pwWrapperAlready = (List<productWrapper>)System.JSON.deserialize(alreadySubscribed, List<productWrapper>.class);
        for (productWrapper pwa : pwWrapperAlready) {
            pwa.listStatus = 'cancel'; 
            cancelList.add(pwa);
            if (!idListHub.contains(pwa.planId) || pwa.Name.contains('LEAN')) {
                for (productWrapper pwc : pwWrapperChange) {
                    if (pwc.Id == pwa.Id) {
                        if (cancelList.contains(pwa)) {
                            system.debug('inside remove');
                            cancelList.remove(cancelList.indexOf(pwa));
                        } 
                    }                            
                }
            }else {
                for (productWrapper pwc : pwWrapperChange) {
                    if (pwc.planId == pwa.planId) {
                        if (cancelList.contains(pwa)) {
                            cancelList.remove(cancelList.indexOf(pwa));
                        } 
                    }                            
                }
            }
        }
        
        for (productWrapper pwc : pwWrapperChange) {
            pwc.listStatus = 'add';
            addList.add(pwc);
            for (productWrapper pwa : pwWrapperAlready) {
                if (pwa.planId == pwc.planId) {
                    if (addList.contains(pwc)) {
                        addList.remove(addList.indexOf(pwc));
                    }
                }
            }
        }
        prodList.addAll(cancelList);
        prodList.addAll(addList);
        if (cancelList.size()>0) {
            System.debug('Cancel List '+cancelList);
        }
        return prodList;
    }
    @AuraEnabled
    public static string createPayment(String canceledProds,String addedProds, String cardId){
        List<productWrapper> pwWrapper = (List<productWrapper>)System.JSON.deserialize(canceledProds, List<productWrapper>.class);
        String returnSuccess = '';
        String msg = '';
        Integer i = 0;
        Map<String, String> productPricebookMap = new Map<String, String>();
        List<productWrapper> pwWrapper1 = (List<productWrapper>)System.JSON.deserialize(addedProds, List<productWrapper>.class);
        for(PricebookEntry p : [SELECT Id, Name, Pricebook2Id, Product2Id FROM PricebookEntry]){
            productPricebookMap.put(p.Product2Id, p.Id);
        }
        
        List<OpportunityLineItem> opLineItemList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> olitoupsert = new List<OpportunityLineItem>();
        
        for (productWrapper p : pwWrapper) {
            List<OpportunityLineItem> oli = [select id,Subscription_Id__c,opportunityId,Cancelled_from_Hub_on__c from OPPORTUNITYLINEITEM WHERE id=:p.Id limit 1];
            HttpRequest http = new HttpRequest();  
            http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli[0].Subscription_Id__c);
            http.setMethod('DELETE');
            http.setTimeout(120000);
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            try{
                if(!Test.isRunningTest()){
                    hs = con.send(http);
                }else{
                    hs.setStatusCode(200);
                    hs.setBody('{ "id": "sub_6alcFSxUKC6fK2", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1437272451, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": 1570198596, "collection_method": "charge_automatically", "created": 1436667651, "current_period_end": 1437272451, "current_period_start": 1436667651, "customer": "cus_Fvj2BXjyhiNikV", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": 1436668036, "items": { "object": "list", "data": [ { "id": "si_18SmzgFzCf73siP0qW4m5WEC", "object": "subscription_item", "billing_thresholds": null, "created": 1436667652, "metadata": {}, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_6alcFSxUKC6fK2", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_6alcFSxUKC6fK2" }, "latest_invoice": null, "livemode": false, "metadata": {}, "pending_setup_intent": null, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1436667651, "start_date": 1436667651, "status": "canceled", "tax_percent": null, "trial_end": 1437272451, "trial_start": 1436667651 }');
                }
            } 
            catch(Exception e) {
                System.debug('------------ Exception---------------'+e);   
            }
            Integer statusCode = hs.getStatusCode();
            
            if (hs.getstatusCode() == 200){
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                System.debug('Cancel Subscription '+results.get('status'));
                if(results.get('status') == 'canceled') {
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    returnSuccess += 'Successfully Canceled '+p.Name;
                    System.debug('Cancel Subscription '+results.get('status'));
                    oli[0].Status__c = 'InActive';
                    oli[0].Cancelled_from_Hub_on__c = DateTime.now();
                }else 
                {
                    oli[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
                }
                oli[0].Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));
                olitoupsert.add(oli[0]);
                
            }else{
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                returnSuccess += String.valueOf(errorMap.get('message'));
                if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                    oli[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                    opLineItemList.add(oli[0]);
                }
                
            }
        }        
        Opportunity opp =  new Opportunity();
        List<Card__c> card = [select id,Stripe_Profile__r.Stripe_Customer_Id__c,Last4__c,Credit_Card_Number__c,contact__c,Stripe_Card_Id__c from card__c where Stripe_Card_Id__c =:cardId];
        if (card.size()>0) {
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Subscription').getRecordTypeId();
            opp.CloseDate = Date.today();
            opp.StageName = 'Closed Won';
            opp.Clubbed__c = true;
            opp.Card__c = card[0].Id;
            opp.Contact__c = card[0].contact__c;
            String customerId = card[0].Stripe_Profile__r.Stripe_Customer_Id__c;
            //StripeCustomer.setDefaultCard(customerId, cardId);
            for (productWrapper pw : pwWrapper1) {
                HttpRequest http = new HttpRequest();
                http.setEndpoint('https://api.stripe.com/v1/subscriptions');
                http.setMethod('POST');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                String authorizationHeader = 'BASIC ' +
                    EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                String body = 'items[0][plan]='+pw.planId+'&customer='+customerId+'&tax_percent='+pw.taxpercentage+'&metadata[Integration Initiated From]=Salesforce'+'&default_payment_method='+card[0].Stripe_Card_Id__c;
                
                http.setBody(body);   
                System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());
                
                String response;
                Integer statusCode;
                Http con = new Http();
                http.setTimeout(120000);
                HttpResponse hs = new HttpResponse();
                
                if (!Test.isRunningTest()) {
                    try {
                        hs = con.send(http);
                    } catch (CalloutException e) {
                        return null;
                    }
                } else {
                    //hs.setBody(StripeCustomerTests.testData_updateSubscription);
                    hs.setBody('{ "id": "sub_6alcFSxUKC6fK2", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1437272451, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": 1570198596, "collection_method": "charge_automatically", "created": 1436667651, "current_period_end": 1437272451, "current_period_start": 1436667651, "customer": "cus_Fvj2BXjyhiNikV", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": 1436668036, "items": { "object": "list", "data": [ { "id": "si_18SmzgFzCf73siP0qW4m5WEC", "object": "subscription_item", "billing_thresholds": null, "created": 1436667652, "metadata": {}, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_6alcFSxUKC6fK2", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_6alcFSxUKC6fK2" }, "latest_invoice": null, "livemode": false, "metadata": {}, "pending_setup_intent": null, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "amount_decimal": "2700", "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1436667651, "start_date": 1436667651, "status": "active", "tax_percent": null, "trial_end": 1437272451, "trial_start": 1436667651 }');
                    hs.setStatusCode(200);
                }
                
                system.debug('#### '+ hs.getBody());
                
                response = hs.getBody();
                statusCode = hs.getStatusCode();
                if (statusCode == 200) {
                    opp.Stripe_Message__c='Success';
                    OpportunityLineItem oli = new OpportunityLineItem();
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                    oli.Subscription_Id__c = String.valueOf(results.get('id'));
                    oli.Status__c = String.valueOf(results.get('status'));
                    oli.Product2Id = pw.Id;
                    oli.Quantity = 1;
                    oli.UnitPrice = pw.totalPrice;
                    oli.PricebookEntryId = productPricebookMap.get(pw.Id);
                    oli.Created_From_Hub_on__c = DateTime.now();
                    oli.Tax_Amount__c = pw.taxAmount;
                    oli.Tax_Percentage_s__c = pw.taxpercentage;
                    if (results.get('status') == 'trialing') {
                        Datetime trialStart = datetime.newInstance(Long.ValueOf(String.valueOf(results.get('trial_start')) )* Long.ValueOf('1000'));
                        oli.TrialPeriodStart__c = date.newinstance(trialStart.year(), trialStart.month(), trialStart.day());
                        Datetime trialEnd =  datetime.newInstance(Long.ValueOf(String.valueOf(results.get('trial_end')) )* Long.ValueOf('1000')); 
                        oli.TrialPeriodEnd__c = date.newinstance(trialEnd.year(), trialEnd.month(), trialEnd.day());
                    } else if (results.get('status') == 'active') {
                        Datetime periodStart = datetime.newInstance(Long.ValueOf(String.valueOf(results.get('current_period_start')) )* Long.ValueOf('1000'));
                        oli.Start__c = date.newinstance(periodStart.year(), periodStart.month(), periodStart.day());  
                        Datetime periodEnd = datetime.newInstance(Long.ValueOf(String.valueOf(results.get('current_period_end')) )* Long.ValueOf('1000'));
                        oli.End__c = date.newinstance(periodEnd.year(), periodEnd.month(), periodEnd.day());
                    }
                    opLineItemList.add(oli);
                    olitoupsert.add(oli);
                    if (i == 0) {
                        opp.Name = pw.Name;
                    }else {
                        opp.Name += ', ' + pw.Name;
                    }
                    i++;
                    if (msg == '') {
                        msg = 'Subscribed to ' + pw.Name;
                    }else {
                        msg += ', Subscribed to ' + pw.Name;
                    }
                    
                }else {
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                    Map<String, Object> error =  (Map<String, Object>)results.get('error');
                    if (msg == '') {
                        msg += 'Failed to subscribe to ' + pw.Name +' Error: '+ String.valueOf(error.get('message'));
                    }else {
                        msg += ', Failed to subscribe to ' + pw.Name +' Error: '+ String.valueOf(error.get('message'));
                    }
                }
            }
        }
        
        if (olitoupsert.size()>0) {
            if (opLineItemList.size()>0) {
                insert opp;
                for (OpportunityLineItem ol : opLineItemList) {
                    ol.OpportunityId = opp.Id;
                }
            }
            upsert olitoupsert;
        }
        if (msg.contains('Failed')) {
            if (returnSuccess != '') {
                returnSuccess += ', '+msg;
            }else {
                returnSuccess = msg;
            }
        }else {
            returnSuccess = 'success';
        }
        System.debug('message '+ returnSuccess);
        if(opLineItemList.size()>0){
            sendmailInvoiceToCustomer(new Set<Id>{opp.id});
        }
        return returnSuccess;
    }
    @AuraEnabled
    public static cardWrapper getCard(String conID){
        cardWrapper conWrapper = new cardWrapper();
        List<card__c> listCard = new List<card__c>();
        string Addressvalue;
        List <Address__c> addList = new List <Address__c>();
        if(conId!=null && conId!=''){
            listCard = [select id,Contact__r.Name,Stripe_Profile__r.Stripe_Customer_Id__c,Last4__c,Credit_Card_Number__c,Stripe_Card_Id__c from card__c where contact__c =:conId Order by createdDate Desc];
            system.debug('------listCard------'+listCard);
            addList = [SELECT Id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c =: conId AND Primary__c = true];
        }
        if(listCard!=null){
            conWrapper.cardList = listCard;
        }
        if(addList.size() > 0){
            Addressvalue = addList[0].Shipping_Street__c+','+addList[0].Shipping_City__c+','+addList[0].Shipping_State_Province__c+' '+addList[0].Shipping_Zip_Postal_Code__c;
            system.debug('=======Addressvalue========'+Addressvalue);
            conWrapper.Address = Addressvalue;
            conWrapper.AddressValue = addList[0];
        }
        return conWrapper;
    }
    @AuraEnabled
    public static productWrapper getCustomProduct(String mapProd){
        system.debug('getCustomProducts '+ mapProd);
        List<customWrapper> pwWrapper = (List<customWrapper>)System.JSON.deserialize(mapProd, List<customWrapper>.class);
        Map<String, Integer> mapProds = new Map<String, Integer>();
        for (customWrapper cw : pwWrapper) {
            mapProds.put(cw.Name, cw.quantity);
        }
        Integer Burn = mapProds.get('burn');
        Integer Lean = mapProds.get('lean');
        Integer Rest = mapProds.get('rest');
        Integer Recover = mapProds.get('recover');
        Integer Fuel = mapProds.get('fuel');
        productWrapper pw =new productWrapper();
        for (Product2 prod : [SELECT id,Price__c,ImageUrl__c,Stripe_Plan_Id__c,Name,Description From Product2 WHERE Fuel__c =: Fuel AND Burn__c =: Burn AND Rest__c =: Rest AND Recover__c =: Recover AND Lean__c =: Lean]) {
            pw.Name = prod.Name;
            pw.totalPrice = prod.Price__c;
            pw.planId = prod.Stripe_Plan_Id__c;
            pw.description = prod.Description;
            pw.image = 'CustomT-System.png';
            pw.Id = prod.Id;
            pw.transformationProd = true;
        }    
        return pw;    
    }
    @Future(callout=true)
    public static void sendmailInvoiceToCustomer(Set<Id> oppIds){
        try{
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
            
            for(Opportunity opp : [select id,Contact__r.Email,(select id ,product2Id,product2.Email_Body__c,product2.Name from OpportunityLineItems) from Opportunity where id =:oppIds and Stripe_Message__c='Success']){
                
                for(OpportunityLineItem lineItem : opp.OpportunityLineItems){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] { opp.Contact__r.Email});
                    
                    mail.setSubject('LadyBoss Receipt - ' + lineItem.product2.Name);
                    mail.setHtmlBody((lineItem.product2.Email_Body__c !=null ? lineItem.product2.Email_Body__c: 'Thank you for your purchase. If you have any questions or concerns, please email us at support@ladyboss.com.'));
                    mail.setUseSignature(false);
                    
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    
                    PageReference pdf = new Pagereference('https://ladyboss-support.secure.force.com/LadyBossHub/HubInvoice');
                    pdf.getParameters().put('id',lineItem.Id);
                    Blob body;                
                    if(!test.isRunningTest())  {
                        try{
                            body = pdf.getContent();
                        }catch(VisualforceException e){
                            body=Blob.valueOf('Some text');
                        }
                    }else  {
                        body=Blob.valueOf('Some text');   
                    }    
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.setContentType('application/pdf');
                    attach.setFileName('LadyBoss Receipt.pdf');
                    attach.setInline(false);
                    attach.Body = body;
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
                    mails.add(mail);
                    
                }
            }
            if(mails.size() >0 && !test.isRunningTest())
                Messaging.sendEmail(mails);
        }catch(Exception e){
            
        }
        
        
    }
    
    
    @AuraEnabled
    public static List<productWrapper> getTax(String conID, String Product){
        TaxjarTaxCalculate tx = new TaxjarTaxCalculate();
        Decimal tax =0.00;
        Decimal taxPercentageSum = 0.00;
        Integer count = 0;
        List<productWrapper> pwWrapper = (List<productWrapper>)System.JSON.deserialize(Product, List<productWrapper>.class);
        List<Address__c> addressList = [SELECT Id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c =: conId AND Primary__c = true];
        if(addressList.size()>0){
            for(productWrapper pw : pwWrapper){
                tx = TaxjarTaxCalculate.ratemethodHub(addressList[0].Shipping_Street__c, addressList[0].Shipping_City__c, addressList[0].Shipping_State_Province__c, 'US', addressList[0].Shipping_Zip_Postal_Code__c, pw.totalPrice, 1, pw.TaxCode);
                if(tx.error == null || tx.error == ''){
                    pw.taxAmount = tx.tax.amount_to_collect;
                    pw.taxpercentage = tx.tax.rate;
                    count++;
                }
            }
        }
        
        return pwWrapper;
    }
}