@isTest
public class RainBowDetoxBuyerCampaignMemberJobTest {
	@istest
    public static void doAddRbCmp() {
        List<Contact> conlist = new List<Contact>();
        contact con = new contact ();
        con.LastName = 'BlogTest';
        con.FirstName = 'firstname';
        con.Marketing_Cloud_Tags__c = 'Lead;Rainbow Detox Buyer;Coaching Buyer';
        insert con;
        con.Marketing_Cloud_Tags__c = 'Rainbow Detox Buyer;Coaching Buyer';
        update con;
        conList.add(con);
        campaign cmp = new campaign ();
        cmp.Name = 'Rainbow Detox Fulfillment from Shopify';
        cmp.Type = 'Marketing Cloud-Data Triggers';
        insert cmp;
        Test.startTest();
        RainBowDetoxBuyerCampaignMemberJob.addToRainbowCampaign(conList);
        Test.stopTest();
    }
}