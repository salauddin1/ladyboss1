//Class which updates the duedates of case using the components
public class ChangeCaseDueDateController {
    //Get the case record
    @AuraEnabled
    public static case getCaseRecord(Id caseId){
        List<case> subsequentList = [select id,Status,Facebook_Message__c,Called__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c from Case Where id=:caseId];
        return(subsequentList[0]);
    }  
    //This method will update the case after checking the field on the comp
    @AuraEnabled public static case saveCase(case cas,String AddRemoveDays,String fieldNameForDays){
        system.debug('===fieldNameForDays===='+fieldNameForDays);
        //Get the custom setting value
        List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,Index__c,Number_Of_Days_To_Move__c from MoveCaseDueDate__c];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        // AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfCase' );
       //Based on the condition updates the record
       if(fieldNameForDays == 'Recovered_Payment__c'){
            recoveredPayment(cas, AddRemoveDays);
            }else {
             List<MoveCaseDueDate__c> mapVal = new List<MoveCaseDueDate__c>();
        mapVal = indexMap.values();
        if(mcddList != null){
        //Loops the custom setting values
            for(MoveCaseDueDate__c customValues : mapVal) {
            //checks with the checked field on comp and the custom setting value
                if(fieldNameForDays == String.valueOf(customValues)){
                    for(Integer i=1; i <= indexMap.size();i++){
                        //If field checked and due date not null then enter into the if condition to update the due date
                        if(AddRemoveDays == 'true' && cas.DueDate__c!=null){
                            //cas.DueDate__c = cas.DueDate__c.addDays(integer.valueof(theFieldValue));
                            if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && indexMap.get(i).Field_Api_Name__c == fieldNameForDays ){
                                if(indexMap.get(i).isDueDateChange__c ==true){
                                cas.DueDate__c = cas.DueDate__c + (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                                }
                                cas.put(fieldNameForDays , true);
                            }
                            
                        }else if(AddRemoveDays == 'false' && cas.DueDate__c!=null){
                            if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && String.valueOf(customValues) == fieldNameForDays ){
                                if(indexMap.get(i).isDueDateChange__c ==true){
                                cas.DueDate__c = cas.DueDate__c - (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                                }
                                cas.put(fieldNameForDays ,false);
                            }
                            
                        }
                    }
                }
            }
             
            if(fieldNameForDays == 'Stop_Correspondence__c'){
                updateContact(cas, AddRemoveDays);
            }
            if(fieldNameForDays == 'Saved_Payment__c'){
                updateCaseStatus(cas, AddRemoveDays);
            }
        
       
        }
            
            }
       
        
        
        upsert cas;
        
        return cas;
    }  
    //To update the contact's DNC 
    public static void updateContact(Case cas,String flag){
        System.debug('contactId is : '+cas.ContactId);
        System.debug('isTrue : '+flag);
        Boolean value = false;
        if(flag=='true'){
            value = true;
            
        }
        List<Contact> ctList = [SELECT Id,DoNotCall,Other_Phone_National_DNC__c,Phone_National_DNC__c FROM Contact WHERE Id = : cas.ContactId];
        ctList.get(0).DoNotCall = value; 
        ctList.get(0).Other_Phone_National_DNC__c = value; 
        ctList.get(0).Phone_National_DNC__c = value; 
        update ctList;
    }
    //To update the case status completed or open if Saved_Payment__c clicked
    public static void updateCaseStatus(Case cas,String flag){
        System.debug('task : '+cas);
        System.debug('flag : '+flag);
        if(flag == 'true'){
            cas.Status = 'Completed';
        }else{
            cas.Status = 'Open';
        }
        update cas;
    }
    //If recovreed payment clicked
    public static void recoveredPayment(Case cas,String flag){
        if(flag == 'true'){
            cas.DueDate__c = System.today();
        }
        update cas;
    }
    //To refrsh the component
    @AuraEnabled
    public static case getCase(Id caseId){
        Case caseOb = [select id,Status,Called__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Facebook_Message__c,Text_Message__c from Case Where id=:caseId];
        return(caseOb );
    }  
    
}