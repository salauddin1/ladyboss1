//batch class to sync refunded all the charges from stripe to salesforce
global class RefundedChargeBatchService implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
    
    global database.querylocator start(database.batchablecontext bc){
        string query ='';
        if(!test.isRunningTest()) {
        Batch_Soql_Limit__c limitSize  = [Select Size__c From Batch_Soql_Limit__c limit 1];
        integer sizeLim = Integer.valueOf(limitSize.Size__c)  ;
        query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where   Charge_Processed__c=false and Stripe_Customer_Id__c!=null order by CreatedDate desc limit :sizeLim ';
        
        }
        if(test.isRunningTest()) {
         query = 'select id,Stripe_Customer_Id__c ,Customer__c from Stripe_Profile__c where   Charge_Processed__c=false limit 1';
        
        }
        System.debug(query);
        return database.getquerylocator( query );   
    }
    global void execute(database.BatchableContext bc,list<Stripe_Profile__c> stripeProfileList){
        //if(stripeProfileList[0].Stripe_Customer_Id__c!=null) {
            stripeProfileList[0].Charge_Processed__c = true;
            String jsonString = chargeHandler.getAllPurchases(stripeProfileList[0].Stripe_Customer_Id__c  );
            System.debug('===='+jsonString);
            if(!jsonString.contains('No such customer')){
                
                
                List<Opportunity> oppList = [select id,Card__c,Clubbed__c,Stripe_Charge_Id__c,RecordType.Name,Subscription__c,Contact__c, (SELECT Id, Subscription_Id__c,Opportunity.RecordType.Name,Stripe_charge_id__c,unitPrice,refund__c,OpportunityId FROM OpportunityLineItems)  from opportunity where  Contact__c =: stripeProfileList[0].Customer__c];
                List<OpportunityLineItem> oliUpdateList =  new List<OpportunityLineItem>();
                List<Opportunity> opUpdateList =  new List<Opportunity>();
                List<Opportunity> opInsertList =  new List<Opportunity>();
                
                Map<string,string> oppExistingMap = new Map<string,string> ();
                Map<string,Opportunity> oppSubChargeMap = new Map<string,Opportunity> ();
                Map<string,Opportunity> oppExMap = new Map<string,Opportunity> ();
                Map<string,string> oliMap = new Map<string,string>();
                Map<string,List<OpportunityLineItem>> oppListMap = new Map<string,List<OpportunityLineItem>> ();
                Map<string,List<OpportunityLineItem>> subLineItemMap = new Map<string,List<OpportunityLineItem>>();
                List<OpportunityLineItem> oppLineItemList =  new List<OpportunityLineItem>();
                
                for(Opportunity o : oppList){
                    /*if( o.recordtype.name== 'Subscription') {
                        if (o.OpportunityLineItems.size () > 0) {
                            for(OpportunityLineItem oli :o.OpportunityLineItems){
                                if(oli.OpportunityId!=null && oli.Stripe_charge_id__c!=null) {
                                    oppLineItemList.add(oli);
                                        subLineItemMap.put(oli.Stripe_charge_id__c,oppLineItemList);
                                    
                                }
                            }
                        }
                    }*/
                    
                    if(o.Stripe_Charge_Id__c!=null  ) {
                        if (o.OpportunityLineItems.size () > 0) {
                            for(OpportunityLineItem oli :o.OpportunityLineItems){
                                if(oli.OpportunityId!=null) {
                                    if(!oppListMap.containskey(oli.OpportunityId)){
                                        oppListMap.put(oli.OpportunityId, new List<OpportunityLineItem> { oli});
                                    }else{
                                        
                                        List<OpportunityLineItem> opli = oppListMap.get(oli.OpportunityId);
                                        opli.add(oli);
                                        oppListMap.put(oli.OpportunityId, opli);
                                    }
                                }
                            }
                        }
                        
                        oppExistingMap.put(o.Subscription__c,o.Card__c);
                        oppSubChargeMap.put(o.Stripe_Charge_Id__c,o);
                        oppExMap.put(o.Subscription__c,o);
                    }
                    
                }
                //Parse Json
                StripeListCharge.Charges varCharges;
                JSONParser parse;
                parse = JSON.createParser(jsonString.replaceAll('currency','currency_data')); 
                varCharges = (StripeListCharge.Charges)parse.readValueAs(StripeListCharge.Charges.class);
                
                for(StripeListCharge.Data charges : varCharges.data){ 
                    //For charge product
                    if(oppSubChargeMap.containsKey(charges.id) && charges.amount_refunded!=0) {
                        
                        Opportunity Op = oppSubChargeMap.get(charges.id);
                        if(!Op.Clubbed__c){
                            Op.Refund_Amount__c = charges.amount_refunded*0.01;
                            opUpdateList.add(Op);
                        }
                        
                        //This will not 
                        if( oppListMap.get(Op.id) !=null &&  Op.Clubbed__c){
                            List<OpportunityLineItem> oliList =  oppListMap.get(Op.id);
                            if(charges.amount_refunded == charges.amount) {
                                for(OpportunityLineItem oliLine : oliList) {
                                    
                                    oliLine.refund__c = oliLine.unitPrice;
                                    
                                    oliUpdateList.add(oliLine);
                                }
                            }
                            else if (charges.amount_refunded != charges.amount) {
                                if(oliList.size() > 0){
                                    decimal refundedinSF = 0;
                                    decimal refundedInStripe = 0;
                                    refundedInStripe = charges.amount_refunded * 0.01;
                                    for(OpportunityLineItem oliLine : oliList) {
                                        if(oliLine.refund__c!=null){
                                            refundedinSF = refundedinSF+ oliLine.refund__c;
                                        }
                                        
                                    }
                                    
                                    decimal refundedAmount = 0;
                                    refundedAmount = refundedInStripe  -  refundedinSF  ;
                                    
                                    for(OpportunityLineItem opli : oliList) {
                                        //opli.Refunded_date__c = Date.Today();
                                        if(opli.unitPrice!=opli.refund__c && refundedAmount > 0) {
                                            decimal reqAmtToFil = opli.unitPrice  - opli.refund__c;
                                            if(refundedinSF > reqAmtToFil) {
                                                opli.Refund__c = opli.Refund__c + reqAmtToFil;
                                                refundedAmount = refundedAmount - reqAmtToFil;
                                            }
                                            else {
                                                opli.Refund__c = opli.Refund__c + refundedAmount;
                                            }
                                            
                                            oliUpdateList.add(opli );
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                
               
                if(opUpdateList.size() > 0 ) {
                    update opUpdateList;
                }
                if(oliUpdateList.size() > 0 ) {
                    update oliUpdateList;
                }
                
            }
            if(stripeProfileList.size() > 0) {
                update stripeProfileList;
            }
        //}
        
    }
    global void finish(database.batchablecontext bc){
        
    }
}