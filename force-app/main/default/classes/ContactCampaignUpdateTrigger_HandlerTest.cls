@isTest
private class ContactCampaignUpdateTrigger_HandlerTest {

    public static void init(){
        Campaign sc = new Campaign();
        sc.Name = 'BookLead-1 Day';
        insert sc;    
    }
    public testmethod Static void doAfterInsertTest(){
        init();
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc;
        
        Campaign sc = new Campaign();
        sc.Name = 'source campaign Lead Days 2-5';
         sc.Type ='Phone Team';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
         dc.Type ='Phone Team';
        insert dc;
        
        Workflow_Configuration__c wc = new Workflow_Configuration__c();
        wc.Source_Campaign__c = sc.Id;
        wc.Products__c = 'UTA';
        wc.Target_Campaign__c = dc.Id;
        wc.Active__c =true;
        insert wc;
        
        Contact con1 = new Contact();
        con1.LastName = 'test lName';
        con1.Product__c = 'UTA';
        con1.AccountId = acc.Id;
        insert con1;
        
        Contact con2 = new Contact();
        con2.LastName = 'test lName';
        con2.Potential_Buyer__c= 'BookLead-1 Day';
        con2.AccountId = acc.Id;
        insert con2;
        
        Test.startTest();
        ContactCampaignUpdateTrigger_Handler.doAfterInsert(new List<Contact>{con1});
        ContactCampaignUpdateTrigger_Handler.doAfterInsert(new List<Contact>{con2});
        Test.stopTest();
    }
    
    public testmethod Static void doBeforeUpdateTest(){
        init();
        Account acc = new Account();
        acc.Name = 'testAcc1';
        insert acc; 
        
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
          Map<Id,Contact> contactMap1 = new Map<Id,Contact>();
        
        Campaign sc = new Campaign();
        sc.Name = 'source campaign Lead Days 2-5';
         sc.Type ='Phone Team';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
         dc.Type ='Phone Team';
        insert dc;
         Campaign dc2 = new Campaign();
        dc2.Name = 'destination campaign Buy Day 1';
         dc2.Type ='PipeLine';
        insert dc2;
        
        Workflow_Configuration__c wc = new Workflow_Configuration__c();
        wc.Source_Campaign__c = sc.Id;
        wc.Products__c = 'UTA';
        wc.Target_Campaign__c = dc.Id;
        wc.Active__c = true;
        insert wc;
        
        Workflow_Configuration__c wc2 = new Workflow_Configuration__c();
        wc2.Source_Campaign__c = sc.Id;
        wc2.Products__c = 'UTA';
        wc2.Target_Campaign__c = dc2.Id;
        wc2.Active__c = true;
        insert wc2;
        
        Contact con2 = new Contact();
        con2.LastName = 'test conlName';
        con2.Product__c = 'UTA';
        con2.Website_Products__c = 'UTA';
        con2.AccountId = acc.Id;
        insert con2;
        contactMap.put(con2.Id, con2);
        Workflow_Configuration__c wc1 = new Workflow_Configuration__c();
        wc1.Source_Campaign__c = dc.Id;
        wc1.Products__c = 'Book';
        wc1.Target_Campaign__c = sc.Id;
        insert wc1;
        
        con2.Product__c = 'Book';
        update con2;
        
        Contact con3 = new Contact();
        con3.LastName = 'test lName';
        con3.Potential_Buyer__c= 'BookLead-1 Day';
        con3.AccountId = acc.Id;
        insert con3;
         contactMap1.put(con3.Id, con3);
        
        Test.startTest();
        ContactCampaignUpdateTrigger_Handler.doBeforeUpdate(new List<Contact>{con2}, contactMap);
        ContactCampaignUpdateTrigger_Handler.doBeforeUpdate(new List<Contact>{con3}, contactMap);
        Test.stopTest();
    }
    
    public testmethod Static void updateCampaignMembersTest(){
        init();
        Account acc = new Account();
        acc.Name = 'testAcccon';
        insert acc;
        
        Campaign sc = new Campaign();
        sc.Name = 'source campaign Lead Days 2-5';
        sc.Type ='Phone Team';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
        dc.Type ='Phone Team';
        insert dc;
        
         Campaign dc2 = new Campaign();
        dc2.Name = 'destination campaign Buy Day 1';
         dc2.Type ='PipeLine';
        dc2.Product__c = 'Supplement';
        insert dc2;
         Campaign dc3 = new Campaign();
        dc3.Name = 'destination campaign Buy Day 1';
         dc3.Type ='PipeLine';
        dc3.Product__c = 'UTA';
        insert dc3;
        
        Workflow_Configuration__c wc = new Workflow_Configuration__c();
        wc.Source_Campaign__c = sc.Id;
        wc.Products__c = 'UTA';
        wc.Target_Campaign__c = dc.Id;
        wc.Active__c = true;
        insert wc;
        
        Workflow_Configuration__c wc2 = new Workflow_Configuration__c();
        wc2.Source_Campaign__c = sc.Id;
        wc2.Products__c = 'UTA';
        wc2.Target_Campaign__c = dc2.Id;
        wc2.Active__c = true;
        insert wc2;
        
        Contact con = new Contact();
        con.LastName = 'test lName';
        con.Product__c = 'UTA';
        con.Website_Products__c = 'UTA';
        con.AccountId = acc.Id;
        insert con;
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        contactMap.put(con.Id, con);
        
         Contact cont = new Contact();
        cont.LastName = 'test lName';
        cont.Product__c = 'UTA';
       	cont.Website_Products__c = 'UTA';
        cont.AccountId = acc.Id;
        insert cont;
         Contact contList1 = [Select id,Product__c,Highest_Product__c from Contact where id =: cont.id];
        System.debug('----contList---'+contList1);
        Map<Id,Contact> contactMapOld = new Map<Id,Contact>();
        contactMapOld.put(cont.Id, contList1);
        
        cont.Product__c = 'Supplement';
        cont.Website_Products__c = 'Supplement';
        update cont;
       
        Contact contList = [Select id,Product__c,Highest_Product__c from Contact where id =: cont.id];
        System.debug('----contList---'+contList);
        Map<Id,Contact> contactMapNew = new Map<Id,Contact>();
        contactMapNew.put(cont.Id, contList);
        
        
        Contact con3 = new Contact();
        con3.LastName = 'test lName';
        con3.Potential_Buyer__c= 'BookLead-1 Day';
        con3.AccountId = acc.Id;
        insert con3;
       
       
        CampaignMember cm = new CampaignMember();
        cm.ContactId = con.Id;
        cm.CampaignId = sc.Id;
        insert cm;

		/*CampaignMember cm2 = new CampaignMember();
        cm2.ContactId = contList1.Id;
        cm2.CampaignId = dc2.Id;
        insert cm2;*/   
        CampaignMember cm3 = new CampaignMember();
        cm3.ContactId = contList1.Id;
        cm3.CampaignId = dc3.Id;
        insert cm3;
        
        Test.startTest();
         ContactCampaignUpdateTrigger_Handler.movetoPipelineCampaigns(contactMapNew,contactMapOld);
        ContactCampaignUpdateTrigger_Handler.updateCampaignMembers(contactMap);
       
        Test.stopTest();
    }
    
    public testmethod Static void addPotentialBuyersTest(){
        init();
        Account acc = new Account();
        acc.Name = 'testAcccon';
        insert acc; 
        
      Contact con = new Contact();
        con.LastName = 'test lName';
        con.AccountId = acc.Id;
       // con.Potential_Buyer__c = 'UTA';
        insert con;
        Map<Id,Contact> conMap = new Map<Id,Contact>();
        conMap.put(con.Id, con);
        
    
        Campaign sc = new Campaign();
        sc.Name = 'source campaign Lead Days 2-5';
        sc.Type ='Phone Team';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
        dc.Type ='Phone Team';
        insert dc;
        
        CampaignMember cm = new CampaignMember();
        cm.ContactId = con.Id;
        cm.CampaignId = sc.Id;
        insert cm;
        
        Contact con3 = new Contact();
        con3.LastName = 'test lName';
        con3.Potential_Buyer__c= 'BookLead-1 Day';
        con3.AccountId = acc.Id;
        insert con3;
        
        Test.startTest();
        ContactCampaignUpdateTrigger_Handler.addPotentialBuyers(conMap);
        Test.stopTest();
         
     }
}