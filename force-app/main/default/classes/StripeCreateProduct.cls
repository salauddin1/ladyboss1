global class StripeCreateProduct implements Queueable, Database.AllowsCallouts {
    private static final String SERVICE_URL='https://api.stripe.com/v1/products';
    global String id;
    global String name;
    global Map<String, String> metadata = new Map<String,String>();
    global static String ProdID;
    private List<Product2> prods = new List<Product2>();
    public StripeCreateProduct(List<Product2> pr){
        this.prods = pr;
    }
    public void execute(QueueableContext context) {
        
        for(Product2 p: prods){
            if(p.Stripe_Plan_Id__c == null && p.Stripe_SKU_Id__c ==null && p.Stripe_Product_Id__c ==null){
                StripeCreateProduct.createProduct(p.Name, 'good', p.id);
            }
            if(p.Stripe_Plan_Id__c == null && p.Stripe_SKU_Id__c !=null && p.Stripe_Product_Id__c !=null&&p.Tax_Code__c!=null){
                StripeCreateProduct.setTaxProduct(p.Stripe_Product_Id__c , p.id );
            }
        }
        
        
    }
    @future(callout=true)
    global static void setTaxProduct(String prodID, String pid){
        //prodList = (List<Product2>)Json.deserialize(jsonString,List<Product2>.class);
        Product2 pr = [SELECT id, Name, Currency__c, Price__c, IsActive,Stripe_Plan_Id__c,Stripe_Product_Id__c,Stripe_SKU_Id__c,Length__c,Height__c,Width__c,Weight__c,Tax_Code__c  FROM Product2 WHERE id =: pid ];
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'/'+prodID);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setBody('metadata[tax_code]='+pr.Tax_Code__c);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                
            }
        }
        else {
            //hs.setBody(StripeChargeTests.testData_createCharge);
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        try {
            StripeCreateProduct o = StripeCreateProduct.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateProduct object: '+o); 
            //return o.id;
            
            system.debug('metadata'+o.metadata);
            /*if(pr.Height__c!=null&&pr.Length__c!=null&&pr.Width__c!=null&&pr.Weight__c!=null){
List<Product2> prodList = new List<Product2>();
prodList.add(pr);
System.enqueuejob(new  StripeUpdateSKU(prodList));
}*/
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
        
        
        
        
    }
    
    
    //@future(callout=true)
    global static void createProduct(String prodName, String prodType, String pid){
        //prodList = (List<Product2>)Json.deserialize(jsonString,List<Product2>.class);
        Product2 pr = [SELECT id,Stripe_Product_Id__c, IsActive, Currency__c, Price__c,Length__c,Height__c,Width__c,Weight__c,Tax_Code__c FROM Product2 WHERE id =: pid ];
        
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        if(pr.Tax_Code__c!=null&&pr.Tax_Code__c!=''){
            http.setBody('name='+prodName+'&type='+prodType+'&attributes[]=sfid&metadata[tax_code]='+pr.Tax_Code__c);
        }else{
            http.setBody('name='+prodName+'&type='+prodType+'&attributes[]=sfid');
        }
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                
            }
        }
        else {
            //hs.setBody(StripeChargeTests.testData_createCharge);
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        try {
            StripeCreateProduct o = StripeCreateProduct.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateProduct object: '+o); 
            //return o.id;
            
            // Product2 pr = [SELECT id,Stripe_Product_Id__c, IsActive, Currency__c, Price__c FROM Product2 WHERE id =: pid ];
            
            StripeCreateSKU.createsku('usd', 'infinite', pr.Price__c, o.id, pr.IsActive, pr.id, pr.Length__c,pr.Height__c,pr.Width__c, pr.Weight__c);
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    global static StripeCreateProduct getProduct(String pid){
        
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'/'+pid);
        http.setMethod('GET');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                return null;
                
            }
        }
        else {
            hs.setBody('{ "id": "prod_Ev5l7pdje6bEii", "object": "product", "active": true, "attributes": [], "caption": null, "created": 1555752859, "deactivate_on": [], "description": null, "images": [], "livemode": false, "metadata": {}, "name": "trstrdg prod", "package_dimensions": null, "shippable": null, "skus": { "object": "list", "data": [], "has_more": false, "total_count": 0, "url": "/v1/skus?product=prod_Ev5l7pdje6bEii&active=true" }, "statement_descriptor": null, "type": "service", "unit_label": null, "updated": 1557298749, "url": null }');
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        try {
            if(statusCode == 200){
                StripeCreateProduct o = StripeCreateProduct.parse(response);
                System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateProduct object: '+o); 
                return o;
            }
            else{
                return null;
            }
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    public static StripeCreateProduct parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        return (StripeCreateProduct) System.JSON.deserialize(json, StripeCreateProduct.class);
    }
}