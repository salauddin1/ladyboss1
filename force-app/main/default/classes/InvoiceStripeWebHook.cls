@RestResource(urlMapping='/InvoiceStripeWebHook/*')
global without sharing class InvoiceStripeWebHook {
	@HttpPost
	global static void InvoiceStripe() {
		if(RestContext.request.requestBody!=null){
            String server = 'Production';
			system.debug('###body: '+RestContext.request.requestBody);
            try{
                String strr = RestContext.request.requestBody.toString().replaceAll('end','end_data').replaceAll('object','object_data').replaceAll('currency','currency_data');
                StripeInvoice.StripeInvoiceObject varInvoice = (StripeInvoice.StripeInvoiceObject) json.deserialize(strr,StripeInvoice.StripeInvoiceObject.class);
                Opportunity opp = new Opportunity();      
                Datetime dateConvert;
            	List<OpportunityLineItem> lstOlis;
                OpportunityLineItem oli;
                
                opp.Paid__c = varInvoice.data.object_data.paid;
                opp.Closed__c = varInvoice.data.object_data.closed;
                if(varInvoice.data.object_data.paid==true){
                    opp.StageName='Approved';
                }else if(varInvoice.data.object_data.closed==true){
                    opp.StageName='Declined';
                }else{
                    opp.StageName='Pending';
                }
                
                //Created Date Unix to Date
                if(varInvoice.data.object_data.due_date!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(varInvoice.data.object_data.due_date);
                    opp.CloseDate = Date.valueOf(dateConvert);
                }else{
                    opp.CloseDate = system.today();
                }
                
                List<Account> oppSelect;
                if(varInvoice.data.object_data.customer != null){
                   oppSelect = [select Id, Name, External_ID__c from Account where External_ID__c =:varInvoice.data.object_data.customer];      
                }

                if(oppSelect!=null){
                    if(oppSelect.size()>0){
                        opp.AccountId = oppSelect.get(0).Id;
                    	opp.Name = oppSelect.get(0).Name + ' ' + String.valueOf(opp.CloseDate);
                    }else{
                        opp.Name= varInvoice.data.object_data.id;
                    }
                }else{
                     opp.Name= varInvoice.data.object_data.id;
                }
                
                if(varInvoice.data.object_data.tax!=null){
                    opp.Tax__c = varInvoice.data.object_data.tax*0.01;
                }
                
                if(varInvoice.data.object_data.period_start!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(varInvoice.data.object_data.period_start);
                    opp.Period_Start__c = Date.valueOf(dateConvert);
                }
                
                if(varInvoice.data.object_data.period_end_data!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(varInvoice.data.object_data.period_end_data);
                    opp.Period_End__c = Date.valueOf(dateConvert);
                }
                
                opp.Statement_Descriptor__c = varInvoice.data.object_data.statement_descriptor;
                opp.Amount = varInvoice.data.object_data.subtotal*0.01;
                opp.Description = varInvoice.data.object_data.description;
                opp.currency__c = varInvoice.data.object_data.currency_data;
                
                if(varInvoice.data.object_data.billing=='send_data_invoice'){
                	opp.Billing__c = 'send_invoice';
                }else if(varInvoice.data.object_data.billing=='send_invoice'){
                    opp.Billing__c = 'send_invoice';
                }else{
                    opp.Billing__c = 'charge_automatically';
                }
                
                if(varInvoice.data.object_data.discount!=null){
                    if(varInvoice.data.object_data.discount.coupon!=null){
                        if(varInvoice.data.object_data.discount.coupon.amount_off!=null){
                            opp.Discount__c = varInvoice.data.object_data.discount.coupon.amount_off*0.01;
                        }else if(varInvoice.data.object_data.discount.coupon.percent_off!=null){
                            opp.Discount__c = opp.Amount*(varInvoice.data.object_data.discount.coupon.percent_off*0.01);
                        }
                    }
                }
                
                String subscriptionID = varInvoice.data.object_data.subscription;
                opp.Subscription__c = subscriptionID;
                //if(varInvoice.data.object_data.lines!=null){
                //    for(StripeInvoice.Data line : varInvoice.data.object_data.lines.data){
                //        subscriptionID = line.id;
                //    }
                //}
                
                List<Opportunity> listOpp = [Select Id From Opportunity WHERE Subscription__c =: subscriptionID];
                if(listOpp.size()>0){
                    opp.External_ID__c = varInvoice.data.object_data.id;
                    opp.Id = listOpp.get(0).Id;
                	if(opp.External_ID__c!=null) update opp;
                }else{
                    opp.External_ID__c = varInvoice.data.object_data.id;
                    if(opp.External_ID__c!=null) upsert opp External_ID__c;
                }

                if(varInvoice.data.object_data.lines!=null){
                    lstOlis = new List<OpportunityLineItem>();
                    for(StripeInvoice.Data line : varInvoice.data.object_data.lines.data){
                        if(opp.External_ID__c!=null){
                            if(line.plan!=null && line.quantity!=null && line.amount!=null){
                                oli = new OpportunityLineItem();
                                oli.External_ID__c = line.id + '-' + line.plan.id;
                                oli.Invoice_ID__c = opp.External_ID__c;
                                oli.Plan_ID__c = line.plan.id;
                                oli.UnitPrice = (line.amount*0.01)/line.quantity;
                                oli.Quantity = line.quantity;
        
                                if(line.period.start!=null){
                                    dateConvert = datetime.newInstance(0);
                                    dateConvert = dateConvert.addSeconds(line.period.start);
                                    oli.Start__c = Date.valueOf(dateConvert);
                                }
                                
                                if(line.period.end_data!=null){
                                    dateConvert = datetime.newInstance(0);
                                    dateConvert = dateConvert.addSeconds(line.period.end_data);
                                    oli.End__c = Date.valueOf(dateConvert);
                                }
                                lstOlis.add(oli);
                            }
                        }
                    }
                }
                
                if(lstOlis.size()>0){
                    Set<String> setProductExternalId = new Set<String>();
                    for(OpportunityLineItem olix : lstOlis){
                        setProductExternalId.add(olix.Plan_ID__c);
                    }
                    
                    Map<String,String> mapPbe = new Map<String,String>();
                    for(PricebookEntry pbe : [Select Id, Product2.Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c IN: setProductExternalId and isActive=true and Product2.isActive=true]){
                        mapPbe.put(pbe.Product2.External_ID__c.toLowerCase(),pbe.Id);
                    }
                    
                    List<OpportunityLineItem> lstAux = new List<OpportunityLineItem>();
                    for(OpportunityLineItem olix : lstOlis){
                        if(mapPbe.get(olix.Plan_ID__c.toLowerCase())!=null){
                            olix.OpportunityId = opp.Id;
                            olix.PricebookEntryId = mapPbe.get(olix.Plan_ID__c.toLowerCase());
                            lstAux.add(olix);
                        }
                    }
                    
                    if(lstAux.size()>0){
                        delete [Select Id From OpportunityLineItem WHERE OpportunityId=:opp.Id];
                        insert lstAux;
                    }
                }
            }catch(Exception e){
                system.debug('##e:'+e);
                if(!e.getMessage().contains('duplicate value found: External_ID__c')){ Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); mail1.setSubject(server+': InvoiceStripeWebHook Request'); mail1.setHtmlBody(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + RestContext.request.requestBody.ToString()); Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); }
            }
		}
		else{
    		RestResponse res = RestContext.response;  res.statusCode = 400;		    
		}
	}
}