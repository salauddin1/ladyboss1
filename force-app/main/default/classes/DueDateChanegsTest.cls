@isTest
public class DueDateChanegsTest {
@isTest
    public static void test1 () {
        // RecordTypeIdopportunity = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
      AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
     // addDa.C1PM__c =1;
     // addDa.C2PM__c =1;
     // addDa.C3PM__c =1;
     // addDa.C4PM__c =1;
     // addDa.C5PM__c =1;
      addDa.name='AddDaysToDueDateOfCase';
      
            insert addDa;
            MoveCaseDueDate__c mv =  new MoveCaseDueDate__c();
            mv.name='C1PM__c';
            mv.Field_Api_Name__c = 'C1PM__c';
            mv.Index__c =1;
            mv.Number_Of_Days_To_Move__c =1;
            mv.isDueDateChange__c =true;
            insert mv;
        Case cs = new Case();
        cs.DueDate__c=  date.Today();
        cs.Status = 'Open';
        cs.Origin='Phone';
        cs.Priority='Medium';
        cs.Text_Message__c = true;
       cs.C1PM__c=true;
         cs.C2PM__c=true;
        cs.C3PM__c=true;
        cs.C4PM__c=true;
        cs.C5PM__c=true;
        cs.type='Account Recovery';
        insert cs;
         cs.C1PM__c=false;
       
         cs.C2PM__c=false;
        cs.C3PM__c=false;
        cs.C4PM__c=false;
        cs.C5PM__c=false;
        update cs;
    }
    @isTest
    public static void test2 () {
        // RecordTypeIdopportunity = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
      AddDaysToDueDate__c addDa = new AddDaysToDueDate__c();
      //addDa.C1PM__c =1;
     // addDa.C2PM__c =1;
      //addDa.C3PM__c =1;
      //addDa.C4PM__c =1;
      //addDa.C5PM__c =1;
      
      addDa.name='AddDaysToDueDateOfCase';
            insert addDa;
            MoveCaseDueDate__c mv =  new MoveCaseDueDate__c();
            mv.name='C2PM__c';
            mv.Field_Api_Name__c = 'C2PM__c';
            mv.Index__c =1;
            mv.Number_Of_Days_To_Move__c =1;
            mv.isDueDateChange__c =true;
            insert mv;
        Case cs = new Case();
        cs.DueDate__c=  date.Today();
        cs.Status = 'Open';
        cs.Origin='Phone';
        cs.Priority='Medium';
        cs.Text_Message__c = true;
       cs.C1PM__c=false;
         cs.C2PM__c=false;
        cs.C3PM__c=false;
        cs.C4PM__c=false;
        cs.C5PM__c=false;
        cs.type='Account Recovery';
        insert cs;
         cs.C1PM__c=true;
         cs.C2PM__c=true;
        cs.C3PM__c=true;
        cs.C4PM__c=true;
        cs.C5PM__c=true;
        cs.type='Account Recovery';
        update cs;
    }
}