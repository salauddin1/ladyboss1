public without sharing class CaseCommunityPagecontroller {
    @AuraEnabled
    public static WrapperClass getcaserec (String conId) {
        List<case> cseList = new List<case>();
        cseList = [select id,Contact.Name, CaseNumber, subject, Description, contactEmail,CreatedDate, Status, Contact_Email__c, Customer_Name__c from case where Is_Site_User_Case__c = true and ( Status = 'Open' or Status = 'New') and contactId =: conId Order by CreatedDate Desc];
        WrapperClass WrCObj = new WrapperClass(cseList);
        return WrCObj;
    }
    @AuraEnabled
    public static WrapperClass setCsObj() {
        Case cs = new Case();
        cs.Subject = '--None--';
        WrapperClass WrCObj = new WrapperClass(cs);
        return WrCObj;
    }
    @AuraEnabled
    public static WrapperClass InsertNewCase (Map<object,object> caseDetail, String conId) {
        try{
            System.debug('=======caseDetail========='+caseDetail);
            Case cs = new Case();
            Map<object,object> getallValuse = (Map<object,object>) caseDetail.get('wrCase');
            cs.Subject = (String) getallValuse.get('Subject');
         //   cs.Customer_Name__c =(String) getallValuse.get('Customer_Name__c');
            cs.Description = (String) getallValuse.get('Description');
            cs.Contact_Email__c = cs.ContactEmail;
            cs.ContactId = conId;
            cs.Origin= 'Web';
            cs.Is_Site_User_Case__c = true ;
            insert cs;
            
            CaseComment   tComment = new CaseComment();
            tComment.ParentId = cs.id;
            tComment.CommentBody = cs.Description;
            tComment.IsPublished = TRUE;
            
            Insert tComment;
            
            System.debug('============-===='+caseDetail);
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('InsertNewCase','CaseCommunityPagecontroller','Contact Id => '+conId+' : Case Record => '+caseDetail ,ex));
            System.debug('***Get Exception***'+ex);        
        }
        WrapperClass WrCObj = new WrapperClass();
        return WrCObj;
    }
    @AuraEnabled
    public static Void setCaseComment (String csId, String csComment) {
        try{
            System.debug('=======csId========='+csId +'=======csComment========='+csComment);
            CaseComment   tComment = new CaseComment();
            tComment.ParentId = csId;
            tComment.CommentBody = csComment;
            tComment.IsPublished = TRUE;
            
            Insert tComment;
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('setCaseComment','CaseCommunityPagecontroller','Case Id => '+csId+' : Comment => '+csComment ,ex));
            System.debug('***Get Exception***'+ex);        
        }
        
    }
    @AuraEnabled
    public static List<WrapperClass> getCaseComentValues (String caseId , String contactName) {
        List<CaseComment> commList =new List<CaseComment>();
        commList = [select Id, ParentId, IsPublished, CommentBody, CreatedBy.Name, CreatedBy.FirstName, CreatedBy.LastName, CreatedById, CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById, IsDeleted from CaseComment where ParentId != null and ParentId =: caseId order by CreatedDate desc];
        System.debug('==========commList========='+commList);
        List<WrapperClass> WrCaseComList = new List<WrapperClass>();
        if(contactName == null || contactName =='' || contactName == 'undefined' ){
            contactName = 'Community User' ;
        }
        if(commList != null && !commList.isEmpty()){
            for(CaseComment csCom : commList){
                System.debug('==========commList========='+csCom);
                if(csCom.CommentBody != null && csCom.CommentBody != '' && csCom.CreatedBy.Name != null ){
                    if(csCom.CreatedBy.Name == 'LadyBossHub Site Guest User'){
                        //if(csCom.CreatedBy.Name == 'TestCommunity Site Guest User'){
                        Date dt = csCom.CreatedDate.date();
                        WrapperClass WrCObj = new WrapperClass(csCom, true, dt, contactName);
                        WrCaseComList.add(WrCObj);
                    }
                    else{
                        Date dt = csCom.CreatedDate.date();
                        WrapperClass WrCObj = new WrapperClass(csCom, false, dt, contactName);
                        WrCaseComList.add(WrCObj);
                    }
                }
            }
        }
        return WrCaseComList;
    }
    
    // wrapper or Inner class with @AuraEnabled {get;set;} properties for CaseComment Record on Hub*    
    public without sharing class wrapperClass{
        @AuraEnabled public CaseComment wrCaseCom {get;set;}
        @AuraEnabled public Boolean isFromUser {get;set;}
        @AuraEnabled public Date wrCaseComDate {get;set;}
        @AuraEnabled public String wrConName {get;set;}
        @AuraEnabled public List<Case> wrCasevalue {get;set;}
        @AuraEnabled public Case wrCase {get;set;}
        
        public wrapperClass (CaseComment wrCaseCom, Boolean isFromUser, Date wrCaseComDate, String wrConName){
            this.wrCaseCom = wrCaseCom ;
            this.isFromUser = isFromUser ;
            this.wrCaseComDate = wrCaseComDate;
            this.wrConName = wrConName;
        }
        public wrapperClass (List<Case> wrCasevalue){
            this.wrCasevalue = wrCasevalue ;
        }
        public wrapperClass (Case wrCase){
            this.wrCase = wrCase ;
        }
        public wrapperClass (){}
    }
}