@isTest
global class NMIMockImplementation implements HttpCalloutMock{

   	public static HTTPResponse respond(HTTPRequest request) {
   		HttpResponse response = new HttpResponse();
		if(request.getEndpoint().contains('customer_vault')){
			response.setHeader('Content-Type', 'application/xml');
	        response.setBody('<?xml version="1.0" encoding="UTF-8"?>' +
                                '<nm_response>' +
                                    '<customer_vault>' +
                                        '<customer id="1066341912">' +
                                            '<first_name>1</first_name>' +
                                            '<last_name></last_name>' +
                                            '<address_1>3</address_1>' +
                                            '<address_2></address_2>' +
                                            '<company></company>' +
                                            '<city>palm</city>' +
                                            '<state>CA</state>' +
                                            '<postal_code>92260</postal_code>' +
                                            '<country>United States</country>' +
                                            '<email>eugene+test3@dgwizard.com</email>' +
                                            '<phone>2</phone>' +
                                            '<fax></fax>' +
                                            '<cell_phone></cell_phone>' +
                                            '<customertaxid></customertaxid>' +
                                            '<website></website>' +
                                            '<shipping_first_name>1</shipping_first_name>' +
                                            '<shipping_last_name></shipping_last_name>' +
                                            '<shipping_address_1>3</shipping_address_1>' +
                                            '<shipping_address_2></shipping_address_2>' +
                                            '<shipping_company></shipping_company>' +
                                            '<shipping_city>palm</shipping_city>' +
                                            '<shipping_state>CA</shipping_state>' +
                                            '<shipping_postal_code>92260</shipping_postal_code>' +
                                            '<shipping_country>United States</shipping_country>' +
                                            '<shipping_email>eugene+test3@dgwizard.com</shipping_email>' +
                                            '<shipping_carrier></shipping_carrier>' +
                                            '<tracking_number></tracking_number>' +
                                            '<shipping_date></shipping_date>' +
                                            '<shipping></shipping>' +
                                            '<cc_number>4xxxxxxxxxxx1881</cc_number>' +
                                            '<cc_hash>8627cfbcbe1a070d4d9e43734ae48798</cc_hash>' +
                                            '<cc_exp>0131</cc_exp>' +
                                            '<cc_start_date></cc_start_date>' +
                                            '<cc_issue_number></cc_issue_number>' +
                                            '<check_account></check_account>' +
                                            '<check_hash></check_hash>' +
                                            '<check_aba></check_aba>' +
                                            '<check_name></check_name>' +
                                            '<account_holder_type></account_holder_type>' +
                                            '<account_type></account_type>' +
                                            '<sec_code></sec_code>' +
                                            '<processor_id></processor_id>' +
                                            '<cc_bin>401288</cc_bin>' +
                                            '<created>20171104234850</created>' +
                                            '<updated>20171104234851</updated>' +
                                            '<account_updated></account_updated>' +
                                            '<customer_vault_id>1066341912</customer_vault_id>' +
                                        '</customer>' +
                                    ' </customer_vault>' +
                                '</nm_response>');
	        response.setStatusCode(200);
	        response.setStatus('OK');
        }else if(request.getEndpoint().contains('transact.php')){
			//response.setHeader('Content-Type', 'application/xml');
	        response.setBody('response=1&responsetext=Customer Added&authcode=&transactionid=&avsresponse=&cvvresponse=&orderid=&type=&response_code=100&customer_vault_id=1770269911');
	        response.setStatusCode(200);
	        response.setStatus('OK');
        }else{
            response.setHeader('Content-Type', 'application/xml');
	        response.setBody('<?xml version="1.0" encoding="UTF-8"?>' +
                                 '<nm_response>' +
                                 '<transaction>' +
                                 '<transaction_id>3806901434</transaction_id>' +
                                 '<partial_payment_id></partial_payment_id>' +
                                 '<partial_payment_balance></partial_payment_balance>' +
                                 '<platform_id></platform_id>' +
                                 '<transaction_type>cc</transaction_type>' +
                                 '<condition>pendingsettlement</condition>' +
                                 '<order_id></order_id>' +
                                 '<authorization_code>123456</authorization_code>' +
                                 '<ponumber></ponumber>' +
                                 '<order_description></order_description>' +
                                 '<first_name>walter</first_name>' +
                                 '<last_name>ynche</last_name>' +
                                 '<address_1>test</address_1>' +
                                 '<address_2></address_2>' +
                                 '<company></company>' +
                                 '<city>test</city>' +
                                 '<state>test</state>' +
                                 '<postal_code>1235</postal_code>' +
                                 '<country></country>' +
                                 '<email></email>' +
                                 '<phone></phone>' +
                                 '<fax></fax>' +
                                 '<cell_phone></cell_phone>' +
                                 '<customertaxid></customertaxid>' +
                                 '<customerid></customerid>' +
                                 '<website></website>' +
                                 '<shipping_first_name></shipping_first_name>' +
                                 '<shipping_last_name></shipping_last_name>' +
                                 '<shipping_address_1></shipping_address_1>' +
                                 '<shipping_address_2></shipping_address_2>' +
                                 '<shipping_company></shipping_company>' +
                                 '<shipping_city></shipping_city>' +
                                 '<shipping_state></shipping_state>' +
                                 '<shipping_postal_code></shipping_postal_code>' +
                                 '<shipping_country></shipping_country>' +
                                 '<shipping_email></shipping_email>' +
                                 '<shipping_carrier></shipping_carrier>' +
                                 '<tracking_number></tracking_number>' +
                                 '<shipping_date></shipping_date>' +
                                 '<shipping>0.00</shipping>' +
                                 '<shipping_phone></shipping_phone>' +
                                 '<cc_number>4xxxxxxxxxxx1111</cc_number>' +
                                 '<cc_hash>f6c609e195d9d4c185dcc8ca662f0180</cc_hash>' +
                                 '<cc_exp>1015</cc_exp>' +
                                 '<cavv></cavv>' +
                                 '<cavv_result></cavv_result>' +
                                 '<xid></xid>' +
                                 '<eci></eci>' +
                                 '<avs_response>N</avs_response>' +
                                 '<csc_response>N</csc_response>' +
                                 '<cardholder_auth></cardholder_auth>' +
                                 '<cc_start_date></cc_start_date>' +
                                 '<cc_issue_number></cc_issue_number>' +
                                 '<check_account></check_account>' +
                                 '<check_hash></check_hash>' +
                                 '<check_aba></check_aba>' +
                                 '<check_name></check_name>' +
                                 '<account_holder_type></account_holder_type>' +
                                 '<account_type></account_type>' +
                                 '<sec_code></sec_code>' +
                                 '<drivers_license_number></drivers_license_number>' +
                                 '<drivers_license_state></drivers_license_state>' +
                                 '<drivers_license_dob></drivers_license_dob>' +
                                 '<social_security_number></social_security_number>' +
                                 '<processor_id>ccprocessora</processor_id>' +
                                 '<tax>0.00</tax>' +
                                 '<currency>USD</currency>' +
                                 '<surcharge></surcharge>' +
                                 '<tip></tip>' +
                                 '<card_balance></card_balance>' +
                                 '<card_available_balance></card_available_balance>' +
                                 '<entry_mode>Keyed</entry_mode>' +
                                 '<cc_bin>411111</cc_bin>' +
                                 '<cc_type>Visa</cc_type>' +
                             	 '<product>' +
                                    '<sku>test3</sku>' +
                                    '<quantity>1.0000</quantity>' +
                                    '<description>Test Product 3</description>' +
                                    '<amount>105.0000</amount>' +
                                '</product>' +
                                '<product>' +
                                    '<sku>testproductnmi</sku>' +
                                    '<quantity>1.0000</quantity>' +
                                    '<description>Test Product NMI</description>' +
                                    '<amount>10.0000</amount>' +
                                '</product>' +
                                 '<action>' +
                                     '<amount>1.00</amount>' +
                                     '<action_type>sale</action_type>' +
                                     '<date>20171001033948</date>' +
                                     '<success>1</success>' +
                                     '<ip_address>136.147.62.8</ip_address>' +
                                     '<source>api</source>' +
                                     '<username>cloudcreations</username>' +
                                     '<response_text>SUCCESS</response_text>' +
                                     '<batch_id>0</batch_id>' +
                                     '<processor_batch_id></processor_batch_id>' +
                                     '<response_code>100</response_code>' +
                                     '<processor_response_text></processor_response_text>' +
                                     '<processor_response_code></processor_response_code>' +
                                     '<device_license_number></device_license_number>' +
                                     '<device_nickname></device_nickname>' +
                                 '</action>' +
                                 '</transaction>' +
                             '</nm_response>');
	        response.setStatusCode(200);
	        response.setStatus('OK');
        }
		return response;
   	}

}