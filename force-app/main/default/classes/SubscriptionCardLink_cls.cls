public class SubscriptionCardLink_cls{
    public static List<card__c> getCard(String chargeID,Opportunity op){
    try{
    if(chargeID!=null && op.Contact__c!=null){
            
           List<card__c> cardList = [select id,Contact__c,Stripe_Card_Id__c,Card_ID__c from card__c where Contact__c !=null and Contact__c =:op.Contact__c];
            Map<string,card__c > cardMap = new Map<string,card__c >();
            for(card__c cd: cardList){
                cardMap.put(cd.Stripe_Card_Id__c,cd);
            }
            String SERVICE_URL = ' https://api.stripe.com/v1/charges/';
            String customerIDs ='';
            HttpRequest http = new HttpRequest();

            http.setEndpoint(SERVICE_URL+''+chargeID);
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            //Blob headerValue = Blob.valueOf('sk_test_QihKQOy179xAArc6hBQeZoUJ:');

            String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);

            System.debug('-----');
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();

           
        IF(Test.isRunningTest()){
            statusCode =200;
            	response ='{"id":"ch_1ENGurFzCf73siP02cQfiW7d","object":"charge","amount":5555500,"customer":"cus_Eje0N3q8TtsoWz","source":{"id":"card_1EGAjBFzCf73siP0k1lOdNgd","object":"card","address_city":null,"address_country":null,"address_line1":null,"address_line1_check":null,"address_line2":null,"address_state":null,"address_zip":null,"address_zip_check":null,"brand":"Visa","country":"US","customer":"cus_Eje0N3q8TtsoWz","cvc_check":null,"dynamic_last4":null,"exp_month":12,"exp_year":2034,"fingerprint":"vsalrcOqPbeQp4Jc","funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null},"status":"succeeded"}';
        }else{
             hs = con.send(http);
             response = hs.getBody();
            statusCode = hs.getStatusCode();
        }
           
            if(statusCode == 200 && response!=null && response!=''){
                System.debug(response);


                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                System.debug('results---'+results);
                Map<String, Object> resultsSource =  (Map<String, Object> )results.get('source');
                card__c card;
                List<card__c> cardListRetu = new  List<card__c>();
               if(cardMap.containsKey(String.valueOf(resultsSource.get('id')))){
                     card = cardMap.get(String.valueOf(resultsSource.get('id')));
                    
                     cardListRetu.add(card);
                    return cardListRetu;
                }else{
                    return null;
                }
                
            }
            
                   }
        //return null;
        }catch(Exception e){
        ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error('StripeSubscriptioin_Service','getCard','',e) );
        }
         return null;
    }
}