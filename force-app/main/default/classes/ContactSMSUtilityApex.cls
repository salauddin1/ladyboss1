public class ContactSMSUtilityApex {
    
    @AuraEnabled
    public static List<User> getAllUser(){
        MasterViewSetting__c profiles = [SELECT Id,Name,UserProfiles__c FROM MasterViewSetting__c limit 1];
        List<String> profileList = profiles.UserProfiles__c.split(',');
        List<User> userlist = [select id,Twillio_From_phone__c,IsActive,Name from User where IsActive = true AND Profile.name in : profileList order by FirstName ASC];
        return userlist;
	}
    
    @AuraEnabled 
    public static void updateNum(String UserName,String Phone) {
        List<User> lstU = [select id,Twillio_From_phone__c,Name from User where IsActive =: true And Name !=: UserName and Twillio_From_phone__c =:Phone limit 1];
        if(lstU.size() > 0 && Phone != null && Phone != '' && Phone != '--None--')  {
            throw new AuraHandledException('This number is already assigned to other user, '+lstU.get(0).name);
        }
        
        User userData;
        if(UserName != null && Phone != null) {
            userData = [select id,Twillio_From_phone__c,Name from User where IsActive =: true And Name =: UserName limit 1];
        }
        
        if(userData != null && Phone != null) {
            if(phone == '--None--')  {
                userData.Twillio_From_phone__c = null;
            }else  {
            	userData.Twillio_From_phone__c = Phone;
            }
            update userData;
        }
    }
    
    @AuraEnabled
    public static List<String> getTwilioNumbers(){
        List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain'];
        String accountSID = lstTwilliConfigs.get(0).AccountSID__c;  //'AC44099911ff7ec99c40bc97943ad6c0ca';
        String authToken = lstTwilliConfigs.get(0).AuthToken__c; //'6a386f07923a55ec0729eff283d747a3';
        String fromNumber = lstTwilliConfigs.get(0).From_Number__c; //'15053226116';
        String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/IncomingPhoneNumbers.json';
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
       
        Http http = new Http();
        List<String> numList = new List<String>();
        if(!test.isRunningTest())  {
        HTTPResponse res = http.send(req);
        Map<String,Object> tl = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        List<Object> tl1 = (List<Object>) tl.get('incoming_phone_numbers');

        for(Object i : tl1){
            Map<String,Object> dt = (Map<String,Object>)i;
            numList.add((String)dt.get('phone_number'));
        }
       }
        System.debug(numList);
        return numList;
    }
     
    @AuraEnabled 
     public static User getSelectedUser(String Id) {
         System.debug('--------UserId--------'+Id);
         User userData = new User();
        if(Id != null) {  
            userData = [select id,Twillio_From_phone__c,Name from User where IsActive =: true And  id =: Id  limit 1];
        }
         return userData;
     }
}