global class InvoiceToChargeUpdateHandler {
    global static void OliUpdater(List<OpportunityLineItem> oliList) {
        try{
            Map<String,id> oppLineItemMap = new Map<String,id>();
            Map<id,String> oppIdChargeMap = new Map<id,String>();
            Map<String,List<OpportunityLineItem>> mapolI = new Map<String,List<OpportunityLineItem>>();
            Map<id, String> oppIdInvoiceNumberMap = new Map<id, String>();
            For(OpportunityLineItem oppLItem : oliList){
                if(oppLItem.Subscription_Id__c != null && oppLItem.Stripe_charge_id__c==null){
                    oppLineItemMap.put(oppLItem.Subscription_Id__c, oppLItem.OpportunityId);
                    if(!mapolI.containsKey(oppLItem.Subscription_Id__c))  {
                        mapolI.put(oppLItem.Subscription_Id__c, new List<OpportunityLineItem>());
                    }
                    mapolI.get(oppLItem.Subscription_Id__c).add(oppLItem);
                }
                System.debug('oppLItem '+oppLItem);
            }
            System.debug('oppLineItemMap '+oppLineItemMap.keyset());
            System.debug('mapolI '+mapolI);
            List<Invoice__c> invList = [Select id,Subscription_Id__c,Charge_Id__c,Invoice_Id__c,Period_Start__c,Period_End__c,Invoice_Number__c from Invoice__c where Subscription_Id__c in : oppLineItemMap.keyset() and Charge_Id__c!=null];
            List<OpportunityLineItem> oppLineItemToUpdate = new List<OpportunityLineItem>();
            List<Invoice__c> invoiceToBeDeleted = new List<Invoice__c>();
            Set<Id> setIDs = new Set<Id>();
            for(Invoice__c inv : invList){
                if(oppLineItemMap.containsKey(inv.Subscription_Id__c)){
                    oppIdChargeMap.put(oppLineItemMap.get(inv.Subscription_Id__c),inv.Charge_Id__c);
                    oppIdInvoiceNumberMap.put(oppLineItemMap.get(inv.Subscription_Id__c), inv.Invoice_Number__c);
                }
                
                if(mapolI.containsKey(inv.Subscription_Id__c))  {
                    for(OpportunityLineItem oli : mapolI.get(inv.Subscription_Id__c))  {
                        // oli.Stripe_charge_id__c = inv.Charge_Id__c;
                        if(setIDs.add(oli.Id))  {
                            decimal tempval = oli.Number_Of_Payments__c+1;
                            
                            system.debug('---temp---'+tempval);
                            oppLineItemToUpdate.add(new OpportunityLineItem(Id=oli.Id,Stripe_charge_id__c = inv.Charge_Id__c,Last_Charge_Id__c = inv.Charge_Id__c,Number_Of_Payments__c=tempval,Start__c=inv.Period_Start__c,End__c=inv.Period_End__c,Status__c = 'Active')); 
                        }
                    }
                }
                invoiceToBeDeleted.add(inv);
            }
            
            List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Owner.name,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c,User_to_give_credit__c,(select id,OpportunityId,Stripe_Charge_Id__c from OpportunityLineItems where Stripe_Charge_Id__c!=null) from Opportunity where id in : oppIdChargeMap.keySet() And Clubbed__c = true];
            
            List<Opportunity> oppListToBeUpdated = new List<Opportunity>();
            for(Opportunity opp : oppList){
                opp.Stripe_Charge_Id__c = oppIdChargeMap.get(opp.id);
                opp.Charge_Description__c = 'Invoice '+oppIdInvoiceNumberMap.get(opp.Id);
                //opp.Number_Of_Payments__c=opp.Number_Of_Payments__c+1;
                
                
                oppListToBeUpdated.add(opp);
            }
            
            // List<OpportunityLineItem> oppLineItemList = [SELECT id,OpportunityId,Subscription_Id__c FROM OpportunityLineItem where Subscription_Id__c in : subId];
            
            if(oppListToBeUpdated.size() > 0){
                system.debug('--->oppListToBeUpdated-->'+oppListToBeUpdated);
                for(Opportunity opp : oppListToBeUpdated){
                    OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate(opp);
                    String salesPerson;
                    if(opp.Sales_Person__c != null && opp.Sales_Person__c !=''){
                        salesPerson = opp.Sales_Person__c;
                    }else{
                        salesPerson = opp.Owner.name;
                    }
                    system.debug('--->oppLineItemToUpdate-->'+opp.OpportunityLineItems);
                    system.debug('--->oppLineItemToUpdate-->'+oppLineItemToUpdate);
                    if(oppLineItemToUpdate.size()  > 0){
                        system.debug('--->oppLineItemToUpdate-->'+opp.OpportunityLineItems);
                        Set<String> setof = new Set<String>();
                        for(OpportunityLineItem opli :oppLineItemToUpdate ){
                            if(opli.Stripe_charge_id__c != opp.Stripe_Charge_Id__c){
                                if(setof.add(opli.stripe_charge_id__c)){
                                    system.debug('---before call out ---'+opli.stripe_charge_id__c+'-----sales---'+opp.sales_person__c);
                                    if(salesPerson!=null && salesPerson!=''){
                                        system.debug('---after call out ---'+opli.stripe_charge_id__c);
                                        OpportunityWithStipeChargeIdHandler.createMetadataAndUpdateOLI2(opli,opp.sales_person__c,opp.Id);
                                    }
                                }
                            }
                            
                        }
                    }
                    
                }
            }
            if (invoiceToBeDeleted.size()>0) {
                delete invoiceToBeDeleted;
            }
            if (oppListToBeUpdated.size()>0) {
                update oppListToBeUpdated;
            }            
            if(oppLineItemToUpdate.size() > 0)  {                
                update oppLineItemToUpdate;
            }
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('InvoiceToChargeUpdateHandler','OliUpdater','',ex) );
        }
    }
    
    global static void updateStartDate(List<OpportunityLineItem> oliList){
        try{
            List<Id> oliIds = new List<Id>();
            List<String> subIdList = new List<String>();
            for(OpportunityLineItem ol : oliList){
                if(ol.Subscription_Id__c!=null && ol.Started_On__c==null){
                    oliIds.add(ol.Id);
                    subIdList.add(ol.Subscription_Id__c);
                }
            }
            List<OpportunityLineItem> oliToUpdate = [SELECT id,Subscription_Id__c,Started_On__c FROM OpportunityLineItem WHERE id In : oliIds];
            List<Invoice__c> invList = [SELECT id,Started_On__c,Subscription_Id__c FROM Invoice__c WHERE Subscription_Id__c in : subIdList and Started_On__c!=null];
            Map<String,Invoice__c> subIdDateMap = new Map<String,Invoice__c>();
            List<Invoice__c> delInv = new List<Invoice__c>();
            for(Invoice__c inv : invList){
                subIdDateMap.put(inv.Subscription_Id__c,inv);
            }
            for(OpportunityLineItem ol : oliToUpdate){
                if(subIdDateMap.get(ol.Subscription_Id__c)!=null && subIdDateMap.get(ol.Subscription_Id__c).Started_On__c!=null){
                    ol.Started_On__c = subIdDateMap.get(ol.Subscription_Id__c).Started_On__c;
                    delInv.add(subIdDateMap.get(ol.Subscription_Id__c));
                }
            }
            update oliToUpdate;
            delete delInv;
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('InvoiceToChargeUpdateHandler','updateStartDate','',ex) );
        }
    }
}