public class UpdateBalanceController {
    public static Contact ct{get;set;}
	@AuraEnabled
    public static Contact getContact(String conId){
        ct = [SELECT id, Available_Commission__c FROM Contact WHERE id=:conId];
        return ct;
    }
    @AuraEnabled
    public static Contact updateContact(String reasonText,String bal,String conId){
        System.debug('reason:-'+reasonText);
        System.debug('newBal:-'+bal);
        ct = [SELECT id, Available_Commission__c FROM Contact WHERE id=:conId];
        ct.Update_Balance_Reason__c = 'SF Agent:'+reasonText;
        ct.Available_Commission__c = Decimal.valueOf(bal);
        update ct;
        return ct;
    }
}