public class CampaignMemberBatchInsQueue implements Queueable{
 List<CampaignMember> records;
    public CampaignMemberBatchInsQueue(List<CampaignMember> allRecords) {
        this.records = allRecords;
    }
    public void execute(QueueableContext context) {
        system.debug('** queue insert list '+ records);
        Database.insert(records,false);
    }

}