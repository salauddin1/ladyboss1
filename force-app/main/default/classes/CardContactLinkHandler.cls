public class CardContactLinkHandler {
   public static void linkcardContact(List<Contact> lstContact) {
     
        Set<Id> conIdSet = new Set<Id>();
        for(Contact con : lstContact) {
            conIdSet.add(con.id); 
        }
        List<Stripe_Profile__c> lstStripeProfile =[SELECT Id,Stripe_Customer_Id__c, Customer__c FROM Stripe_Profile__c where Customer__c  In: conIdSet] ;
        Set<Id> profIdSet = new Set<Id>();
        Map<string, string> conProfileMap = new Map<string, string>();
         Map<string, string> cardProfileMap = new Map<string, string>();
        Set<string> custIdSet = new Set<string>();
        for(Stripe_Profile__c prof : lstStripeProfile ) {
            profIdSet.add(prof.id); 
            custIdSet.add(prof.Stripe_Customer_Id__c);
            conProfileMap.put(prof.id,prof.Customer__c );
            cardProfileMap.put(prof.Customer__c,prof.id );
        }
        List<card__c> cardListUpdate =  new List<card__c>();
        List<card__c> cardList =  [select id,Stripe_Profile__c,CustomerID__c,Contact__c from card__c where Stripe_Profile__c IN:profIdSet or CustomerID__c IN:custIdSet];
        for (card__c cd : cardList ) {
            if(conProfileMap.containsKey(cd.Stripe_Profile__c)) {
                if(conProfileMap.get(cd.Stripe_Profile__c)!=null){
                    cd.Contact__c  = conProfileMap.get(cd.Stripe_Profile__c);
                    cd.Stripe_Profile__c = cardProfileMap.get(cd.CustomerID__c);
                    cardListUpdate.add(cd);
                }
            }
        }
        if(cardListUpdate.size() > 0) {
            update cardListUpdate;
        }
    }

}