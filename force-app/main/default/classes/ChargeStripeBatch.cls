global class ChargeStripeBatch implements Database.Batchable<Payment__c>, Database.AllowsCallouts, Database.Stateful{
    
    public String startingAfter {get;set;}
    public Boolean hasMore {get;set;}
    
    public ChargeStripeBatch()
    {
        system.debug('###construct ChargeStripeBatch');
        startingAfter = '';
        hasMore = false;
    }
    
    public ChargeStripeBatch(String stAfter)
    {
        system.debug('###construct ChargeStripeBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        startingAfter = stAfter;
        hasMore = false;
    }
    
	public List<Payment__c> start(Database.BatchableContext BC ){
        system.debug('###start ChargeStripeBatch');
		List<Payment__c> lstPayment = new List<Payment__c>();
        HttpResponse res = new HttpResponse();
        if(startingAfter!=null && startingAfter!=''){
        	res = StripeConnection.HttpRequest('charges','?limit=50&starting_after='+startingAfter);    
        }else{
            res = StripeConnection.HttpRequest('charges','?limit=50');
        }
        if(res.getStatusCode()==200){
            StripeListCharge.Charges varCharges;
            JSONParser parse;
            parse = JSON.createParser(res.getBody().replaceAll('currency','currency_data')); 
            varCharges = (StripeListCharge.Charges)parse.readValueAs(StripeListCharge.Charges.class);
            Payment__c pay;
            system.debug('###varCharges: '+varCharges);
            
            Set<String> setExternalOppID = new Set<String>();
            Set<String> setExternalAccID = new Set<String>();
            Map<String,Opportunity> mapOpp = new Map<String,Opportunity>();
            Map<String,Account> mapAcc = new Map<String,Account>();
            for(StripeListCharge.Data charges : varCharges.data){     
                if(charges.invoice!=null){
                    setExternalOppID.add(charges.invoice);
                }
                if(charges.customer!=null){
                    setExternalAccID.add(charges.customer);
                }
            }
            for(Opportunity opp : [select Id, Name, External_ID__c from Opportunity where External_ID__c IN:setExternalOppID]){
                mapOpp.put(opp.External_ID__c, opp);
            }
            for(Account acc : [select Id, Name, External_ID__c from Account where External_ID__c IN:setExternalAccID]){
                mapAcc.put(acc.External_ID__c, acc);
            }
            
            Datetime createdDate;
            for(StripeListCharge.Data charges : varCharges.data){     
                system.debug('###Charges: '+charges);
                pay = new Payment__c();
                pay.External_ID__c = charges.id;
                pay.Status__c = charges.status;                
                pay.Statement_Descriptor__c = charges.statement_descriptor;
                pay.Description__c = charges.description;
                pay.Currency__c = charges.currency_data;
                pay.Paid__c = charges.paid;
                pay.Amount__c = charges.amount*0.01;
                pay.Invoice_ID__c = charges.invoice;
                
                //Created Date Unix to Date
                if(charges.created!=null){
                    createdDate = datetime.newInstance(0);
                    createdDate = createdDate.addSeconds(charges.created);
                    pay.Date__c = Date.valueOf(createdDate);
                }else{
                    pay.Date__c = system.today();
                }

                if(mapOpp.get(charges.invoice) != null){
                    pay.Opportunity__c = mapOpp.get(charges.invoice).Id;                     
                }
                
                if(mapAcc.get(charges.customer) != null){
                    pay.Account__c = mapAcc.get(charges.customer).Id;                     
                }

                if(charges.source!=null){
                    pay.Source_ID__c = charges.source.id;
                    pay.Address_City__c = charges.source.address_city;
                    pay.Address_Country__c = charges.source.address_country;
                    pay.Address_Street__c = charges.source.address_line1;
                    pay.Address_State__c = charges.source.address_state;
                    pay.Address_Zip__c = charges.source.address_zip;
                    pay.Brand__c = charges.source.brand;
                    pay.Country__c = charges.source.country;
                    pay.Exp_Month__c = charges.source.exp_month;
                    pay.Exp_Year__c = charges.source.exp_year;
                    pay.Last4__c = charges.source.last4;
                }
                
                startingAfter = pay.External_ID__c;
                lstPayment.add(pay);
            }
            system.debug('###has_more: '+ varCharges.has_more);
            hasMore = varCharges.has_more;
        }
        
        return lstPayment;
	}
	
	public void execute(Database.BatchableContext BC, List<Payment__c> scope) {
        system.debug('###execute ChargeStripeBatch');
        if(scope.size()>0){
            ApexUtil.isTriggerInvoked = true;
            upsert scope External_ID__c;
            ApexUtil.isTriggerInvoked = false;
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish ChargeStripeBatch');
        system.debug('###finish hasMore: '+hasMore);
        system.debug('###finish startingAfter: '+startingAfter);
        if(!Test.isRunningTest()){
            if(hasMore) Database.executeBatch(new ChargeStripeBatch(startingAfter));
        }
    }

}