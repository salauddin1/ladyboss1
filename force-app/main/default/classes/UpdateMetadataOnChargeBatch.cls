global class UpdateMetadataOnChargeBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    
    List<Opportunity> oppList = new List<Opportunity> ();
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select id,Sales_Person__c,Owner.name,contact__c,Stripe_Charge_Id__c,(select id,OpportunityId,Stripe_Charge_Id__c from OpportunityLineItems where Stripe_Charge_Id__c!=null) from Opportunity where CreatedDate > 2018-12-30T00:00:00.000Z and RecordTypeId=\'012f4000000WOPCAA4\' and ownerid!=\'005f4000000TFxHAAW\' and Sales_Person__c!=null and Stripe_Charge_Id__c!=null');
        //return Database.getQueryLocator('SELECT  id,Stripe_Charge_Id__c,Sales_Person__c from opportunity where clubbed__c = false and CreatedDate = LAST_N_DAYS:30 and id=\'0061F0000041cSFQAY\'  ');
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        oppList = (List<Opportunity>)scope;
        
        Set<String> setof = new Set<String>();
        
        for(Opportunity op : oppList){
            String salesPerson;
            if(op.Sales_Person__c != null && op.Sales_Person__c !=''){
                salesPerson = op.Sales_Person__c;
            }else{
                salesPerson = op.Owner.name;
            }
            if(op.OpportunityLineItems.size()  > 1){
                for(OpportunityLineItem opli :op.OpportunityLineItems ){
                    if(opli.Stripe_charge_id__c != op.Stripe_Charge_Id__c){
                        if(setof.add(opli.stripe_charge_id__c)){
                            //StripeCharge stripechrgli = StripeCharge.getCharge(opli.Stripe_Charge_Id__c, false);
                            if(salesPerson!=null && salesPerson!=''){
                                OpportunityWithStipeChargeIdHandler.createMetadataAndUpdateOLI(opli,salesPerson);
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){}
    
    global void dummy(){
        integer i =0 ;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
    
    
}