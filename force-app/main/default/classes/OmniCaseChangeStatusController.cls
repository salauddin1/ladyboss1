public with sharing class OmniCaseChangeStatusController {
    
    //Reassigning case to queue if user has not closed the case
    @AuraEnabled
    public static void onAgentLogout(){
    	List<Case> CaseToUpdate = new List<Case>();
    	DateTime dt = Datetime.now().addHours(-24);
    	List<Id> caseIdList = new List<Id>();
    	User u = new User();
    	u.id = UserInfo.getUserId();
    	for(AgentWork aw :[SELECT Id,OriginalQueueId,WorkItemId FROM AgentWork WHERE CreatedDate >=: dt AND UserId =: u.id]){
    		caseIdList.add(aw.WorkItemId);
    	}
    	for(Case cs:[SELECT Id,Status,OwnerId,Routed_From__c FROM Case WHERE Status IN ('Open','Reopened') AND Routed_From__c!=null AND Routed_From__c!='' AND ID IN :caseIdList]){
    		cs.OwnerId=cs.Routed_From__c;
    		if(cs.Status == 'Open'){
    			cs.Status = 'Received';
    		}else if(cs.Status == 'Reopened'){
    			cs.Status = 'Re-Received';
    		}
    		caseToUpdate.add(cs);
    	}
    	if(caseToUpdate.size()>0){
    		update caseToUpdate;
    	}
    }
    //Setting a field on case for queueid from which queue case is routed to the user
    @AuraEnabled
    public static void onAgentWorkAccept(String workId,String workItemId){
    	List<AgentWork> aWorkList = [SELECT Id,OriginalQueueId,WorkItemId FROM AgentWork WHERE ID =:workId];
 		List<Case> caseList = [SELECT Id,Status,OwnerId,Routed_From__c FROM Case WHERE ID = :workItemId];
    	if(aWorkList.size()>0 && caseList.size()>0){
    		if(caseList[0].Status == 'Received' ){
    			caseList[0].Status = 'Open';
    		}else if(caseList[0].Status == 'Re-Received'){
    			caseList[0].Status = 'Reopened';
    		}
    		caseList[0].Routed_From__c = aWorkList[0].OriginalQueueId;
    		update caseList;	    	
		}
    }							
}