global class StripeUpdateSKU implements Queueable  {
	private static final String SERVICE_URL = 'https://api.stripe.com/v1/skus ';
    
    global String stripeCurrency;
    global String id;
    private List<Product2> prods = new List<Product2>();
    public StripeUpdateSKU(List<Product2> pr){
        this.prods = pr;
    }
    public void execute(QueueableContext context) {
		        
        for(Product2 p: prods){
        	StripeUpdateSKU.updateSKU(p.Stripe_SKU_Id__c, 'usd', 'infinite', p.Price__c, p.IsActive,p.Length__c,p.Height__c,p.Width__c,p.Weight__c);
        }
        

    }
    
    @future(callout=true)
    global static void updateSKU(String skuId, String stripeCurrency, String inventoryType, Decimal prices,  Boolean active, Decimal length,Decimal height,Decimal width, Decimal weight){
        Integer price= (prices*100).intValue();
        String s;
        if(length!=null&&height!=null&&width!=null&&weight!=null){
            s='currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&active='+active+'&package_dimensions[height]='+height+'&package_dimensions[weight]='+weight+'&package_dimensions[length]='+length+'&package_dimensions[width]='+width;
        }else{
            s='currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&active='+active;
        }
        HttpRequest http1 = new HttpRequest();
        //String S ='?currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&active='+active;
        String message='';
        http1.setEndpoint('https://api.stripe.com/v1/skus/'+skuId);
        http1.setMethod('POST');
        http1.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http1.setHeader('Authorization', authorizationHeader);
        http1.setBody(s);
        String response1;
        Integer statusCode1;
        Http con1 = new Http();
        HttpResponse hs1 = new HttpResponse();
       	if(!Test.isRunningTest()){
            try {
                    hs1 = con1.send(http1);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    //return null;
                }
        }else{
            hs1.setStatusCode(200);
        }
         system.debug('#### '+ hs1.getBody());
        
        response1 = hs1.getBody();
        
        statusCode1 = hs1.getStatusCode();
        system.debug('$$statusCode = '+hs1.getStatusCode());
        if(hs1.getStatusCode() == 200 || hs1.getStatusCode() == 201){
            message = 'Success';
        }else{
           	Map<String,Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response1);
            Map<String,Object> error = (Map<String,Object>)responseMap.get('error');
			message = (String)error.get('message');
            system.debug(message);
            }
            
        
         try {
            StripeUpdateSKU op = StripeUpdateSKU.parse(response1);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeUpdateSKU object: '+op); 
            //return message;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    
    public static StripeUpdateSKU parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (StripeUpdateSKU) System.JSON.deserialize(json, StripeUpdateSKU.class);
    }
}