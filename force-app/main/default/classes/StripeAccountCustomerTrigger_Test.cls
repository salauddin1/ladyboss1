@IsTest
public class StripeAccountCustomerTrigger_Test {   
	static testMethod void test() {
        Account acc = new Account(Name='Test');
        
        Test.startTest();
        insert acc;
        Test.stopTest();
    }
    
    static testMethod void test2() {
        Account acc = new Account(Name='Test');
        insert acc;
        Test.startTest();
        	update acc;
        Test.stopTest();
    }
    
    static testMethod void test3() {
        Account acc = new Account(Name='Test');
        insert acc;
        
        Test.startTest();
        	acc.Name = 'Test 1';
        	update acc;
        Test.stopTest();
    }
}