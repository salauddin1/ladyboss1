global class InvoiceStripeBatch implements Database.Batchable<Opportunity>, Database.AllowsCallouts, Database.Stateful{
    
    public String startingAfter {get;set;}
    public Boolean hasMore {get;set;}
    public Map<String,List<OpportunityLineItem>> mapOlis {get;set;}
    
    public InvoiceStripeBatch()
    {
        system.debug('###construct InvoiceStripeBatch');
        mapOlis = new Map<String,List<OpportunityLineItem>>();
        startingAfter = '';
        hasMore = false;
    }
    
    public InvoiceStripeBatch(String stAfter)
    {
        system.debug('###construct InvoiceStripeBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        mapOlis = new Map<String,List<OpportunityLineItem>>();
        startingAfter = stAfter;
        hasMore = false;
    }
    
	public List<Opportunity> start(Database.BatchableContext BC ){
        system.debug('###start InvoiceStripeBatch');
		List<Opportunity> lstOpportunity = new List<Opportunity>();
        HttpResponse res = new HttpResponse();
        if(startingAfter!=null && startingAfter!=''){
        	res = StripeConnection.HttpRequest('invoices','?limit=25&starting_after='+startingAfter);    
        }else{
            res = StripeConnection.HttpRequest('invoices','?limit=25');
        }

        if(res.getStatusCode()==200){
            StripeListInvoice.Invoices varInvoices;
            JSONParser parse;
            parse = JSON.createParser(res.getBody().replaceAll('end','end_data').replaceAll('object','object_data').replaceAll('currency','currency_data'));
            varInvoices = (StripeListInvoice.Invoices)parse.readValueAs(StripeListInvoice.Invoices.class);
            
            //Cusomer
            Set<string> setExternalID = new Set<string>();
            Map<String,Account> mapAccount = new Map<String,Account>();
            for(StripeListInvoice.Data_Z invoice : varInvoices.data){  
                if(invoice.customer != null){
                    setExternalID.add(invoice.customer);
                }
            }   
            for(Account acc : [select Id, Name, External_ID__c from Account where External_ID__c IN:setExternalID]){
                mapAccount.put(acc.External_ID__c, acc);
            }

			Opportunity opp;            
            Datetime dateConvert;
            List<OpportunityLineItem> lstOlis;
            OpportunityLineItem oli;
            String subscriptionID;
            for(StripeListInvoice.Data_Z invoice : varInvoices.data){     
                system.debug('###invoice: '+invoice);
                subscriptionID = '';
                opp = new Opportunity();
                opp.External_ID__c = invoice.id;
                
                opp.Paid__c = invoice.paid;
                opp.Closed__c = invoice.closed;
                if(invoice.paid==true){
                    opp.StageName='Approved';
                }else if(invoice.closed==true){
                    opp.StageName='Declined';
                }else{
                    opp.StageName='Pending';
                }
                
                //Created Date Unix to Date
                if(invoice.due_date!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(invoice.due_date);
                    opp.CloseDate = Date.valueOf(dateConvert);
                }else{
                    opp.CloseDate = system.today();
                }
                
                if(mapAccount.get(invoice.customer)!=null){
                    opp.Name= mapAccount.get(invoice.customer).Name + ' ' + String.valueOf(opp.CloseDate);
                }else{
                    opp.Name= invoice.id;
                }
                
                if(mapAccount.get(invoice.customer) != null){
                    opp.AccountId = mapAccount.get(invoice.customer).Id;                     
                }
                
                if(invoice.tax!=null){
                    opp.Tax__c = invoice.tax*0.01;
                }
                
                if(invoice.period_start!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(invoice.period_start);
                    opp.Period_Start__c = Date.valueOf(dateConvert);
                }
                
                if(invoice.period_end!=null){
                    dateConvert = datetime.newInstance(0);
                    dateConvert = dateConvert.addSeconds(invoice.period_end);
                    opp.Period_End__c = Date.valueOf(dateConvert);
                }
                
                opp.Statement_Descriptor__c = invoice.statement_descriptor;
                opp.Amount = invoice.subtotal*0.01;
                opp.Description = invoice.description;
                opp.currency__c = invoice.currency_data;
                //opp.Billing__c = invoice.billing; 
                
                if(invoice.billing=='send_data_invoice'){
                	opp.Billing__c = 'send_invoice';
                }else if(invoice.billing=='send_invoice'){
                    opp.Billing__c = 'send_invoice';
                }else{
                    opp.Billing__c = 'charge_automatically';
                }

                if(invoice.discount!=null){
                    if(invoice.discount.coupon!=null){
                        if(invoice.discount.coupon.amount_off!=null){
                            opp.Discount__c = invoice.discount.coupon.amount_off*0.01;
                        }else if(invoice.discount.coupon.percent_off!=null){
                            opp.Discount__c = opp.Amount*(invoice.discount.coupon.percent_off*0.01);
                        }
                    }
                }
                
                if(invoice.lines!=null){
                    lstOlis = new List<OpportunityLineItem>();
                    for(StripeListInvoice.Data line : invoice.lines.data){
                        oli = new OpportunityLineItem();
                        oli.External_ID__c = line.id + '-' + line.plan.id;
                        oli.Invoice_ID__c = opp.External_ID__c;
                        oli.Plan_ID__c = line.plan.id;
                        oli.UnitPrice = (line.amount*0.01)/line.quantity;
                        oli.Quantity = line.quantity;

                        if(line.period.start!=null){
                            dateConvert = datetime.newInstance(0);
                            dateConvert = dateConvert.addSeconds(line.period.start);
                            oli.Start__c = Date.valueOf(dateConvert);
                        }
                        
                        if(line.period.end_data!=null){
                            dateConvert = datetime.newInstance(0);
                            dateConvert = dateConvert.addSeconds(line.period.end_data);
                            oli.End__c = Date.valueOf(dateConvert);
                        }
                        lstOlis.add(oli);
                    }
                    
                    if(lstOlis.size()>0){
                        mapOlis.put(opp.External_ID__c,lstOlis.clone());
                    }
                }
                
                opp.Subscription__c = invoice.subscription;
                
                startingAfter = opp.External_ID__c;
                lstOpportunity.add(opp);
            }
            system.debug('###has_more: '+ varInvoices.has_more);
            hasMore = varInvoices.has_more;
        }
        
        return lstOpportunity;
	}
	
	public void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        system.debug('###execute InvoiceStripeBatch');
        if(scope.size()>0){
            ApexUtil.isTriggerInvoked = true;
            upsert scope External_ID__c;
            ApexUtil.isTriggerInvoked = false;
            
            Set<String> setProductExternalId = new Set<String>();
            for(String keyOli : mapOlis.keySet()){ 
                for(OpportunityLineItem oli : mapOlis.get(keyOli)){
                    setProductExternalId.add(oli.Plan_ID__c);
                }
            }
            
            Map<String,String> mapPbe = new Map<String,String>();
            for(PricebookEntry pbe : [Select Id, Product2.Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c IN: setProductExternalId]){
                mapPbe.put(pbe.Product2.External_ID__c.toLowerCase(),pbe.Id);
            }
            
            List<String> lstOppId = new List<String>();
            List<OpportunityLineItem> lstAux = new List<OpportunityLineItem>();
            for(Opportunity opp : scope){
                lstOppId.add(opp.Id);
                if(mapOlis.get(opp.External_ID__c)!=null){
                    for(OpportunityLineItem oli : mapOlis.get(opp.External_ID__c)){
                        if(mapPbe.get(oli.Plan_ID__c)!=null){
                            oli.OpportunityId = opp.Id;
                            oli.PricebookEntryId = mapPbe.get(oli.Plan_ID__c.toLowerCase());
                            lstAux.add(oli);
                        }
                    }
                }
            }

            if(lstAux.size()>0){
                delete [Select Id From OpportunityLineItem WHERE OpportunityId IN: lstOppId];
                insert lstAux;
            }
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish InvoiceStripeBatch');
        system.debug('###finish hasMore: '+hasMore);
        system.debug('###finish startingAfter: '+startingAfter);
        if(!Test.isRunningTest()){
            if(hasMore) Database.executeBatch(new InvoiceStripeBatch(startingAfter));
        }
    }

}