public class UpdateOrderItemsBatch implements Database.Batchable<sobject>, Database.AllowsCallouts {
	public database.querylocator start(database.batchablecontext bc) {   
            string query='select id, Shopify_Orders__c, contact__c from ShopifyLineItem__c where createddate=today';
            return database.getquerylocator( query );
        }
    public void execute(database.BatchableContext bc,list<ShopifyLineItem__c> scope) {
        Set<String> idset = new sET<String> ();
        for(ShopifyLineItem__c itm : scope) {
            idset.add(itm.Shopify_Orders__c);
        }
        List<Shopify_Orders__c> ord= [select id,contact__c from Shopify_Orders__c where id IN: idset];
		for(Shopify_Orders__c so: ord) {
    		for(ShopifyLineItem__c item : scope) {
        		if(so.id == item.Shopify_Orders__c) {
            		item.contact__c = so.contact__c;
        		}
    		}
		}
	update scope;
    }
    public void finish(database.batchablecontext bc){}
}