@isTest
public class RefundChargesTest {
	@isTest
    public static void refundtestMethod(){
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'test@gmail.com';
        insert con;
        
        Shopify_Orders__c order = new Shopify_Orders__c();
        order.Contact__c = con.id;
        order.First_Name__c = 'test';
        order.Order_Id__c = '123455654322';
        order.Last_Name__c = 'CristopherTest';
        order.city__c = 'Indore';
        order.province__c = 'Madhya Pradesh';
        order.country__c = 'India';
        order.Charge_Id__c = 'ch__gyadhceu';
        order.Refunded_Amount__c = 1.1;
        order.Subtotal_Price__c = 2.0;
        order.Total_Price__c = 5.0;
        order.Transaction_Id__c = 'tr_dgu3ehbwgyue221';
        insert order;
        
        ShopifyLineItem__c itm = new ShopifyLineItem__c();
        itm.Contact__c = con.Id;
        itm.Price__c = 10;
        itm.Refunded_Amount__c = 10;
        itm.Refunded_Tax_Amount__c = 10;
        itm.Name = 'burn by 1 day test';
        itm.Shopify_Orders__c = order.Id;
        insert itm;
        
       Shopify_Parameters__c token = new Shopify_Parameters__c();
        token.Access_Token__c = 'uygeyudsjgwurg378uaixs';
        token.Domain_Name__c = 'https://demo.com';
        token.Is_Active__c = true;
        insert token;
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new OrderCalloutTest()) ;
        RefundCharges.getshopOrder(con.Id);
        RefundCharges.getshopItems(order.Id);
        RefundCharges.refunds(order.Id, '30', 'duplicate');
        test.stopTest();
    }
}