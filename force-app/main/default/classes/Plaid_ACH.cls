public class Plaid_ACH {
    public  String public_token { get; set; }
    public String account_id { get; set; }
    public String bank_name { get; set; }
    public String externalId { get; set; }
    public boolean isSafe {get; set;}
    public boolean isError {get; set;}
    public String messageError { get; set; }
    public String public_key {get; set;}
    ACH_Account__c newACH;
    
    public Plaid_ACH(){
        Plaid__c pl = Plaid__c.getInstance('PlaidAPI');
        system.debug('pl.public_key__c'+pl.public_key__c);
        public_key = pl.public_key__c;
        externalId = ApexPages.currentPage().getParameters().get('externalId');
        system.debug('-----'+externalId);
        List<Contact> cntList = [select id from Contact where id =: externalId];
        if(cntList.size() > 0 && public_key!= null){
            isError= false;
            
        }else{
            if(cntList.size() == 0){
                messageError ='Seems like your Contact doesn\'t exist in salesforce!';
            }else if(public_key == null){
                messageError += ' Public Key Error.';
            }
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
            isError = true;
        }
    }
    public String callOutNow(String webServiceVal,String access_token) {
        Plaid__c pl = Plaid__c.getInstance('PlaidAPI');
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://development.plaid.com'+webServiceVal);
        http.setMethod('POST');
        
        http.setHeader('Content-Type','application/json');
        Map<String,String> metadata = new Map<String,String>();
        Map<String, String> payload = new Map<String, String>();
        if(access_token!=null && access_token!=''){
            payload = new Map<String, String>{
                'client_id' => pl.client_id__c,
                    'secret'=> pl.Secret_key__c,
                    'access_token'=>access_token,
                    'account_id'=>account_id
                    };
                        }else{
                            payload = new Map<String, String>{
                                'client_id' => pl.client_id__c,
                                    'secret'=> pl.Secret_key__c,
                                    'public_token'=>public_token
                                    };
                                        
                                        }
        
        
        http.setBody(json.serialize(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                system.debug('#### '+ hs.getBody());
                
                return hs.getBody();
            } catch (CalloutException e) {
                isError = true;
                messageError = String.valueOf(e);
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
                return null;
            }
        } else {
            return '{"access_token": "access-sandbox-5cd6e1b1-1b5b-459d-9284-366e2da89755", "stripe_bank_account_token": "btok_5oEetfLzPklE1fwJZ7SG", "request_id": "[Unique request ID]" }';
        }
    }
    public PageReference InsertRecord() {
        
        
        try {
            //StripeACH o = StripeACH.parse(response);
            String response = callOutNow('item/public_token/exchange',null);
            if(response != null){
                Map<String, Object> results =  (Map<String, Object>)JSON.deserializeUntyped(response);
                InsertRecordAgain(String.valueof(results.get('access_token')));
                System.debug(System.LoggingLevel.INFO, '\n**** StripeACH object: '+response); 
               
            }
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
        }
        
        
        return null;
    }
    
    public void InsertRecordAgain(String access_token) {
        
        //String response = '{"stripe_bank_account_token": "btok_98FUxH3j2SpjYCw1b6hO","request_id": "req_dff4r4fd"}';
        
        try {
            String response = callOutNow('processor/stripe/bank_account_token/create',access_token);
            if(response != null){
                
                Map<String, Object> results =  (Map<String, Object>)JSON.deserializeUntyped(response);
                if(String.valueof(results.get('stripe_bank_account_token')) != null && String.valueof(results.get('request_id')) != null){
                    String stripe_bank_account_token= String.valueof(results.get('stripe_bank_account_token'));
                    String request_id= String.valueof(results.get('request_id'));
                    if(stripe_bank_account_token!=null && request_id!=null){
                        
                        
                        
                        ACH_Account__c newACH = new ACH_Account__c();
                        
                        List<Stripe_Profile__c> existingStripeProfiles = new List<Stripe_Profile__c>();
                        existingStripeProfiles =[select id,Customer__c,Customer__r.firstname from Stripe_Profile__c where Customer__c=:externalId order by createdDate limit 1];
                        if(!existingStripeProfiles.isEmpty()){
                            newACH.Stripe_Profile__c = existingStripeProfiles[0].id;        
                        }else{
                            Stripe_Profile__c newStriprProfile = new Stripe_Profile__c();
                            newStriprProfile.Customer__c = externalId;
                            insert newStriprProfile;
                            newACH.Stripe_Profile__c = newStriprProfile.id;
                        }
                        
                        newACH.ACH_Token__c = stripe_bank_account_token;
                        newACH.Contact__c = externalId;
                        newACH.Bank_Name__c = bank_name;
                        
                        /*newACH.Last4__c = String.valueOf(ACHAccount.get('last4'));
newACH.Account_Holder_Type__c = String.valueOf(ACHAccount.get('account_holder_type'));
newACH.Account_Holder_Name__c = String.valueOf(ACHAccount.get('account_holder_name'));*/
                        
                        insert newACH;
                        this.newACH = newACH;
                        isSafe =true;
                    }
                }
                
                System.debug(System.LoggingLevel.INFO, '\n**** StripeACH object: '+response); 
            }
        } catch (System.JSONException e) {
            isSafe = false;
            messageError = 'Salesforce Error - '+e;
            this.isError = true;
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
        
    }
    
    public PageReference CallWebService() {
        String message;
        //this.isError = false;
        System.debug('------before callout-----');
        try{
            if(isSafe != null && isSafe){
                System.debug('newCard-----'+newACH.id);
                
                   message = StripeIntegrationHandlerChanges.ACHWithTokenStripeRealTimeHandler(newACH.Id);
                
                if(message != null && message !=''){
                    delete newACH;
                    messageError = message+' Please contact Ladyboss Agent';
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
                    this.isError = true;
                }else{
                    newACH.isVerified__c = true;
                    update newACH;
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Please close this Window, you have successfully verified your account!'));
                    this.isError = false;
                }
            }
            
        }catch(exception ex){
            delete newACH;
            messageError = 'Error--'+ex;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messageError));
            this.isError = true;
        }
        return null;
    }
    
}