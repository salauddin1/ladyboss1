@isTest
private class FacebookFetchCaseBatchTest
{
	@isTest
	static void itShould()
	{
		// Given
		Test.setMock(HttpCalloutMock.class, new FacebookAPIMock(200,'Ok','{"data": [{"unread_count": 13,"updated_time": "2099-07-30T14:08:54+0000"","senders": {"data": [{"name": "Test test","email": "1956@facebook.com","id": "199"},{"name": "Shrine Software Services Pvt. Ltd.","email": "7508@facebook.com","id": "testPageId"}]},"id": "t_16"}],"paging": {"cursors": {"before": "before","after": "after"}}}',new  Map<String, String>()));		
		FacebookPage__c fb = new FacebookPage__c();
		fb.isActive__c = true;
		fb.Last_Fetch_Time__c = Datetime.now();
		fb.Page_Id__c = 'testPageId';
		fb.Page_Token__c = 'token';
		fb.Page_Name__c = 'test name';
        fb.Last_Fetch_Time__c = DateTime.now().addDays(-1);
		insert fb;

		Case caseObject = new Case();
		caseObject.subject = 'Inbox Message from: ';
		caseObject.origin = 'facebook';
		caseObject.status = 'new';
		caseObject.Conversation_Id__c = 'ets';
		insert caseObject;

		Contact cnt = new Contact();
		cnt.Fb_User_Id__c = 'testPageId';
		cnt.lastName = 'test';
		insert cnt;

		FacebookWrappers.ImageData img = new FacebookWrappers.ImageData();
		img.url = 'testurl';
		img.preview_url = 'testurl';

		FacebookWrappers.AttachmentData att = new FacebookWrappers.AttachmentData();		
		att.image_data = img;
		att.id='ds';
		att.name='dss';

		FacebookWrappers.Category category = new FacebookWrappers.Category();		
		category.id = 'dsds';
		category.name = 'sds';

		FacebookWrappers.AccessToken accessToken = new FacebookWrappers.AccessToken();
		accessToken.token_type = 'asd';
		accessToken.expires_in = 'asd';

		FacebookWrappers fw = new FacebookWrappers ();

		FacebookWrappers.Cursor cur = new FacebookWrappers.Cursor();
		cur.after = 'dd';
		cur.before = 'ggdd';

		FacebookWrappers.Paging paging = new FacebookWrappers.Paging();
		paging.cursors = cur;
		paging.next = 'next';
		paging.next = 'previous';
 		
		List<FacebookWrappers.AttachmentData> attList = new List<FacebookWrappers.AttachmentData>();

		FacebookWrappers.MultipleAttachments multi = new FacebookWrappers.MultipleAttachments();		
		multi.data = attList;
		
		// When

		// Then
		FacebookFetchCaseBatch facebookFetchBatch = new FacebookFetchCaseBatch();
        database.executebatch(facebookFetchBatch);

	}
}