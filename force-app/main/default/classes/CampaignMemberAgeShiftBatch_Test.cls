@isTest
public class CampaignMemberAgeShiftBatch_Test {
    @isTest
    public static void testMtd() {
        
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new  Map<String,String>();
        
        
        Lead ld1 = new Lead(Status = 'New', LastName = 'test1Name',Company = 'testing1company');
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        List<Lead> ldList = new List<Lead>{ld1,ld2};
            insert ldList;
        
        List<Campaign> cList = new List<Campaign>();
        
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1-4';
        c1.Product__c = 'Book'; 
        c1.Max_Age__c = 4;
        c1.Min_Age__c = 0;
        //c1.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c1);
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Buy Days 5-7';
        c2.Product__c = 'Book'; 
        c2.Max_Age__c = 10;
        c2.Min_Age__c = 5;
        c2.Type='Phone Team';
        //c2.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c2);
        
        Campaign c3 =new  Campaign();
        c3.Name = 'Campaign Lead Days 8-10';
        c3.Product__c = 'Book'; 
        c3.Max_Age__c = 10;
        c3.Min_Age__c = 8;
        c3.Type='Phone Team';
        //c3.Marketing_Cloud_Tags__c ='Book Buyer';
        cList.add(c3);
        Campaign c4 =new  Campaign();
        c4.Name = 'Campaign Lead Days 11-30';
        c4.Product__c = 'Book'; 
        c4.Type='Phone Team';
        //c4.Marketing_Cloud_Tags__c ='Book Buyer';
        //c4.Max_Age__c = ;
        
        c4.Min_Age__c = 11;
        cList.add(c4);        
        insert cList;
        
        
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c1.Id,LeadId = ld1.Id , Created_Date__c = d1);
        insert cm1;   
        
        
        Date d2 = dt.addDays(-12);
        CampaignMember cm2 = new CampaignMember(CampaignId = c3.Id,LeadId = ld2.Id , Created_Date__c = d2);
        insert cm2;    
        
        List<CampaignMember> cmList =[SELECT Id,Name,Created_Date_Time__c , Created_Date__c,CampaignId,Product__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Name, Age__c,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = False ];
        
        //System.debug('cmList size --> '+cmList.size());
        List<Campaign> campaignList = [SELECT Id, Name, Product__c, Max_Age__c, Min_Age__c  FROM Campaign ];
        
        Campaign_Products__c setting = new Campaign_Products__c();
        setting.Name = 'Book';
        insert setting;        
        
        
        Test.startTest();
        CampaignMemberAgeShiftBatch campMemberAge = new CampaignMemberAgeShiftBatch();
        Id batchId = Database.executeBatch(campMemberAge);
        Test.StopTest();
    }
}