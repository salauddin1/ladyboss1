@isTest
public class RecoveredPaymentChartControllerTest {
    @isTest
    static void TestPageCon(){
        PageReference pageRef = Page.Recovered_Dollars_report;
        Test.setCurrentPage(pageRef);
        
        RecoveredPaymentChartController recPayCon = new RecoveredPaymentChartController();
        
        RecoveredPaymentChartController.userAmount usAm = new RecoveredPaymentChartController.userAmount('Test',12.00,11.00);
        
        List<SelectOption> options = recPayCon.getStudOption();
        
        Stripe_Product_Mapping__c strMap = new Stripe_Product_Mapping__c();
        strMap.Salesforce__c = 'Supplement' ;
        strMap.Stripe__c = 'LadyBoss LEAN 2 CLUB';
        strMap.Name = 'test';
        
        insert strMap ; 
            Card__c crd = new Card__c ();
			
			crd.Card_ID__c = 'card_1E8MojBwLSk1v1ohTTbxFfxT';
			crd.Cvc_Check__c = 'pass';
			crd.Expiry_Year__c = '2034';
			crd.Last4__c = '4242' ;
			crd.Expiry_Month__c = '04';
			crd.Name_On_Card__c = 'test';
			crd.Brand__c= 'Visa';
            crd.isCreatedNew__c=true;
			insert crd;

        
        Opportunity opp = new Opportunity (
            Name='Opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,Amount=23,
        	card__c =crd.id	);
		insert opp;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(
            Name = 'LadyBoss LEAN 2 CLUB',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 1 * pbEntry.UnitPrice
        );
        insert oli;
        
        recPayCon.endDate = Date.today().addDays(3);
        recPayCon.startDate = Date.today().addDays(-1);
        recPayCon.getData();
    }
    @isTest
    static void TestPageCon2(){
        PageReference pageRef = Page.Recovered_Dollars_report;
        Test.setCurrentPage(pageRef);
        
        RecoveredPaymentChartController recPayCon = new RecoveredPaymentChartController();
        
        RecoveredPaymentChartController.userAmount usAm = new RecoveredPaymentChartController.userAmount('Test',12.00,11.00);
        
        List<SelectOption> options = recPayCon.getStudOption();
    
        Stripe_Product_Mapping__c strMap = new Stripe_Product_Mapping__c();
        strMap.Salesforce__c = 'Supplement' ;
        strMap.Stripe__c = 'LadyBoss LEAN 2 CLUB';
        strMap.Name = 'test';
        
        insert strMap ; 
            Card__c crd = new Card__c ();
			
			crd.Card_ID__c = 'card_1E8MojBwLSk1v1ohTTbxFfxT';
			crd.Cvc_Check__c = 'pass';
			crd.Expiry_Year__c = '2034';
			crd.Last4__c = '4242' ;
			crd.Expiry_Month__c = '04';
			crd.Name_On_Card__c = 'test';
			crd.Brand__c= 'Visa';
            crd.isCreatedNew__c=true;
			insert crd;

        
        Opportunity opp = new Opportunity (
            Name='Opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,Amount=23,
        	card__c =crd.id	);
		insert opp;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(
            Name = 'LadyBoss LEAN 2 CLUB',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 1 * pbEntry.UnitPrice
        );
        insert oli;
        
        Opportunity opp2 = new Opportunity (
            Name='Opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,Amount=23,
        	card__c =crd.id	);
		insert opp2;
        
        Id pricebookId2 = Test.getStandardPricebookId();
        
        Product2 prod2 = new Product2(
            Name = 'LadyBoss LEAN 2 CLUB',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod2;
        
        PricebookEntry pbEntry2 = new PricebookEntry(
            Pricebook2Id = pricebookId2,
            Product2Id = prod2.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry2;
        
        OpportunityLineItem oli2 = new OpportunityLineItem(
            OpportunityId = opp2.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry2.Id,
            TotalPrice = 1 * pbEntry2.UnitPrice
        );
        insert oli2;
        
        Case cs = new case();
        cs.Subject = 'Failed payment - $79.88 | null Test Badole | badole.salesforce@gmail.com | null | Your card was declined. | Payment for invoice 75488BA-0001';
        cs.Status = 'New' ;
        insert cs ;
        recPayCon.endDate = Date.today().addDays(3);
        recPayCon.startDate = Date.today().addDays(-1);
        recPayCon.getData();
    }
    
    @isTest
    static void TestPageCon3(){
        PageReference pageRef = Page.Recovered_Dollars_report;
        Test.setCurrentPage(pageRef);
        
        RecoveredPaymentChartController recPayCon = new RecoveredPaymentChartController();
        
        RecoveredPaymentChartController.userAmount usAm = new RecoveredPaymentChartController.userAmount('Test',12.00,11.00);
        
        List<SelectOption> options = recPayCon.getStudOption();
        
        Stripe_Product_Mapping__c strMap = new Stripe_Product_Mapping__c();
        strMap.Salesforce__c = 'Supplement' ;
        strMap.Stripe__c = 'LadyBoss LEAN 2 CLUB';
        strMap.Name = 'test';
        strMap.Stripe_Nick_Name__c ='opp1';
        
        insert strMap ; 
            Card__c crd = new Card__c ();
			
			crd.Card_ID__c = 'card_1E8MojBwLSk1v1ohTTbxFfxT';
			crd.Cvc_Check__c = 'pass';
			crd.Expiry_Year__c = '2034';
			crd.Last4__c = '4242' ;
			crd.Expiry_Month__c = '04';
			crd.Name_On_Card__c = 'test';
			crd.Brand__c= 'Visa';
            crd.isCreatedNew__c=true;
			insert crd;

        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,Amount=23,
        	card__c =crd.id	);
		insert opp;
        
        recPayCon.endDate = Date.today().addDays(3);
        recPayCon.startDate = Date.today().addDays(-1);
        recPayCon.getData();
    }
    
}