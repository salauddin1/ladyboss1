@isTest
global class five9calloutforRemovecontactMock  implements WebServiceMock{
 global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) {
        
        String[] resplist1=new String[]{'(http://service.admin.ws.five9.com/, false, false)'};
        String[] resplist2=new String[]{'(return_x)'};
        String[] resplist3=new String[]{'(return, http://service.admin.ws.five9.com/, null, 1, 1, false)'};

       five9calloutforcontactVCC.removeNumbersFromDncResponse respElement =new five9calloutforcontactVCC.removeNumbersFromDncResponse();
       respElement.return_x_type_info=resplist1;
       respElement.return_x=0;
       respElement.field_order_type_info= resplist2;
       respElement.apex_schema_type_info= resplist3;   
       response.put('response_x', respElement);
   }
}