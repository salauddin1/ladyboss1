@isTest(SeeAllData=true)
private class OmniCaseChangeStatusControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        List<String> caseList= new List<String>();
        List<String> agList= new List<String>();
        contact con = new contact();
        con.lastName='test';
        insert con;
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        User user1 = new User();
        user1.id = UserInfo.getUserId();
        Case caseRec = new Case();
        caseRec.Subject = 'testCase';
        caseRec.Description = 'TTB0333333';
        caseRec.OwnerId =  UserInfo.getUserId(); 
        caseRec.Status = 'Received';
        caseRec.Opportunity__c= op.ID;
        caseRec.Routed_From__c= user1.id;
        insert caseRec;
        
        caseList.add(caseRec.id);
        
        AgentWork ag = [Select id,UserId FROM AgentWork WHERE UserId =:user1.id LIMIT 1];
        Test.startTest();
        OmniCaseChangeStatusController.onAgentLogout();
        OmniCaseChangeStatusController.onAgentWorkAccept(ag.id,caseRec.id);
        Test.stopTest();



    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        List<String> caseList= new List<String>();
        List<String> agList= new List<String>();
        contact con = new contact();
        con.lastName='test';
        insert con;
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        User user1 = new User();
        user1.id = UserInfo.getUserId();
        Case caseRec = new Case();
        caseRec.Subject = 'testCase';
        caseRec.Description = 'TTB0333333';
        caseRec.OwnerId =  UserInfo.getUserId(); 
        caseRec.Status = 'Re-Received';
        caseRec.Opportunity__c= op.ID;
        caseRec.Routed_From__c= user1.id;
        insert caseRec;
        
        caseList.add(caseRec.id);
        
        AgentWork ag = [Select id,UserId FROM AgentWork WHERE UserId =:user1.id LIMIT 1];
        Test.startTest();
        OmniCaseChangeStatusController.onAgentLogout();
        OmniCaseChangeStatusController.onAgentWorkAccept(ag.id,caseRec.id);
        Test.stopTest();



    }
}