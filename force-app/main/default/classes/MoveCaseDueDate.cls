/*
 * Developer Name : Tirth Patel
 * Description : This Batch processes all the Cases with Status value as open and due date as today. Then it checks for the corresponding am field and set pm field to true if am field value is true.There is a trigger which moves due date if pm field is marked true.
 */
global class MoveCaseDueDate implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful ,Schedulable {
    // Fetches all the case records where status is open and due date is today
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set<String> accFieldList = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap().keySet();
        List<String> strList = new List<String>();
        strList.addAll(accFieldList);
        String fields = string.join(strList,',');
        String dynamicquery = 'select '+fields+' FROM Case  where DueDate__c=today and Status=\'Open\'';
        return Database.getQueryLocator(dynamicquery);
    }
    
    global void execute(SchedulableContext ctx) {
        MoveCaseDueDate sdbatch = new MoveCaseDueDate();
        database.executebatch(sdbatch,200);
    }   
    // This method checks the value of am field on the case and set corresponding pm field to true if am field value is true.
    global void execute(Database.BatchableContext bc, List<Case> records){
        List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,Index__c,Number_Of_Days_To_Move__c,related_am_field_api_name__c from MoveCaseDueDate__c where use_pm_check__c=true];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        for(Case cs : records){
            for(Integer i=1; i <= indexMap.size();i++){
                if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && cs.get(indexMap.get(i).Field_Api_Name__c) == false && indexMap.get(i).related_am_field_api_name__c != '' && cs.get(indexMap.get(i).related_am_field_api_name__c) == true){
					cs.put(indexMap.get(i).Field_Api_Name__c,true);
                    break;
                }
            }
        }
        update records;
    }    
    
    global void finish(Database.BatchableContext bc){
        
    }    
}