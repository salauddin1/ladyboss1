// Developer Name :- Surendra Patidar
// class contains code about add contact to the Blog Option campaign 
// =====================================================================================
public class BlogCampaignMovement {
    public static void doinsert (List<Contact> con) {
        // veriable initialization
        try{
            Set<String> conSet = new Set<String> ();
            List<CampaignMember> cmember = new List<CampaignMember>();
            Map<String, CampaignMember> cmMap = new Map<String, CampaignMember>();
            Set<String> conIdMap = new Set<String> ();
            List<CampaignMember> cmrList = new  List<CampaignMember>() ;
            
            // filter all blog orogine contact
            if(con != null && !con.isEmpty()){
                for(contact c : con){
                    if(c.sf4twitter__Origin__c != null && c.sf4twitter__Origin__c == 'Blog' ){
                        conset.add(c.Id);
                    }
                }
                // Query Blog Campaignmember and filter contact if it is not in blog option campaign
                if(conset != null && !conset.isEmpty()) {
                    cmember = [SELECT Id, CampaignId,Campaign.Name, ContactId, Status, Name FROM CampaignMember where ContactId!=null and ContactId IN : conset and Campaign.Name = 'Blog Optins'];
                    if(cmember != null && !cmember.isEmpty()) {
                        for(CampaignMember cmp : cmember) {
                            cmMap.put(cmp.ContactId,cmp);
                        }
                        
                    }
                    for(String conId : conset) {
                        if(!cmMap.containsKey(conId)) {
                            conIdMap.add(conId);
                        }
                    }
                }
            }
            // Query Blog Option campaign
            List<campaign> cmp = [select id,name,type from campaign where type = 'Blog' and name = 'Blog Optins'];
            
            // add campaign member to the blog option campaign
            if(cmp != null && !cmp.isEmpty()){
                if(conIdMap != null && !conIdMap.isEmpty()) {
                    for(String str : conIdMap) {
                        CampaignMember newCM = new CampaignMember(CampaignId = cmp[0].Id,ContactId = str,status = 'Sent' );
                        cmrList.add(newCM);
                    }
                }
            }
            System.debug('====cmrList====='+cmrList);
            if(cmrList.size() > 0  ) {
                //Insert the campaign member
                database.insert (cmrList, false);
                System.debug('====cmrList====='+cmrList);
            }
        }
        catch(Exception e){
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('MC_CampaignGenearator ','On Insert and update','Trigger for campaign', e));
        }
    }
}