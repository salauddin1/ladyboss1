global class UpdatePaymentBatch  implements  Database.Batchable<sObject>, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT id,createdNow__c FROM Payment__c where createdNow__c=true]);
    }
    global void execute(Database.BatchableContext bc, List<Payment__c> PayList){
        for(Payment__c pay : PayList){
            pay.createdNow__c=false;
        }
        update PayList;
    }
    global void finish(Database.BatchableContext bc){
        
    }
}