/*
 * Developer Name : Tirth Patel
 * Description :  This class is a handler to a campaignMember trigger and it send message to number.
*/
public class CampaignMemberTriggerHandler {
    public static void sendSMSwithoutFuture(String cmbId,String messageData,String defaultFromNumber)  {
    	sendSMSwithoutFuture(cmbId,null,messageData,defaultFromNumber);    
    }
    
    public static void sendSMSwithoutFuture(String cmbId,String cntId, String messageData,String defaultFromNumber)  {  
    	CampaignMember cmb = null;
        List<CampaignMember> lst1 = [select id,ContactId from CampaignMember where id =:cmbId];
        String toNumber = null;
        String contactId = null;
        if(lst1.size() > 0)  {
            cmb  = lst1.get(0);
            contactId= lst1.get(0).ContactId;
        }
        if(contactId == null)  {
            contactId = cntId; 
        }
        if(contactId == null)  {
            return;
        }
        
        List<Contact> cntList = [select id,firstname,lastname,MobilePhone,Phone,OtherPhone,AssistantPhone,DoNotCall,Other_Phone_National_DNC__c,Phone_National_DNC__c,Unsubscribe_Me__c  from Contact where id =:contactId];
        if(cntList.size()==0){
            System.debug('DoNotCall');
            return;
        }
        Contact cnt = cntList.get(0);
        if(cnt.Unsubscribe_Me__c=='Yes'){
            toNumber = null;
        }else if(cnt.MobilePhone != null && !cnt.DoNotCall)  {
            toNumber = cnt.MobilePhone; 
        }else if(cnt.Phone != null && !cnt.Phone_National_DNC__c)  {
            toNumber = cnt.Phone;
        }else if(cnt.OtherPhone != null && !cnt.Other_Phone_National_DNC__c)  {
            toNumber = cnt.OtherPhone;
        }else if(cnt.AssistantPhone != null)  {
            toNumber = cnt.AssistantPhone;
        }
        if(toNumber == null)  {
            return;
        }
        if(toNumber.indexof('+') != 0)  {
            toNumber = toNumber.replaceAll('\\(','').replaceAll('\\)','').replaceAll('\\s','').replaceAll('\\-','').replaceAll(' ',''); 
            toNumber = '+1'+toNumber;
        }
        System.debug(toNumber);
        System.debug(messageData);
        Map<String,Object> mapData = sendSMSNew(toNumber,messageData,cnt,defaultFromNumber);
        System.debug(mapData);
        if(cmb != null)  {
        	cmb.Twilio_Response__c = (String)mapData.get('message');
        	update cmb;	
        }
    }
    
    @future(callOut=true)
    public static void sendSMS(String cmbId,String messageData,String defaultFromNumber)  {
        sendSMSwithoutFuture(cmbId,messageData,defaultFromNumber);
    }
    
    @future(callOut=true)
    public static void sendSMSList(String jsonData)  {
        system.debug('---list--'+jsonData);
        List<campaignMessageWrapper> cmwList = (List<campaignMessageWrapper>)System.JSON.deserialize(jsonData, List<campaignMessageWrapper>.class);
        for(campaignMessageWrapper cmw : cmwList){
            sendSMSwithoutFuture(cmw.campaignId,cmw.messageData,cmw.defaultFromNumber);
        }
        //sendSMSwithoutFuture(cmbId,messageData,defaultFromNumber);
    }
    public class campaignMessageWrapper{
        public id campaignId; 
        public String messageData;
        public String defaultFromNumber;
    }
    public static List<Object> slice(List<Object> input, Integer ge, Integer l)
{
    List<Object> output = input.clone();
    for (Integer i = 0; i < ge; i++) output.remove(0);
    Integer elements = l - ge;
    while (output.size() > elements) output.remove(elements);
    return output;
}
    
    public static Map<String,Object> sendSMSNew(String toNumber, String messageData,Contact cnt,String defaultFromNumber)  {  
        Map<String,Object> mapOutput = new  Map<String,Object>();
        List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain'];
        String accountSID = lstTwilliConfigs.get(0).AccountSID__c;  
        String authToken = lstTwilliConfigs.get(0).AuthToken__c; 
        String fromNumber;
        if(defaultFromNumber!=null){
             fromNumber = defaultFromNumber;
        }else {
             fromNumber = lstTwilliConfigs.get(0).From_Number__c; 
        }
        
        String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        if(test.isRunningTest()){
            messageData = 'tests';
        }
        req.setBody('To='+ EncodingUtil.urlEncode(toNumber,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(messageData,'UTF-8'));
        Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
        String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        Http http = new Http();
        //if(!Test.isRunningTest()){
        HTTPResponse res = http.send(req);
        TwillioResponse tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
        if(tl.sid != null)  {
            mapOutput.put('success',TRUE);
            mapOutput.put('message','SMS send succsufully');
            
            Task t = new Task();
            t.WhoId = cnt.id;
            t.Description = messageData;
            t.status = 'Completed';
            t.Subject = 'SMS Sent to ' + toNumber;
            insert t;
            
        }else  {
            mapOutput.put('success',FALSE);
            mapOutput.put('message','SMS sending fail,Error from Twillio is : '+tl.message);
        }
        //}
        return mapOutput;
            
    }
    class TwillioResponse  {
        public String sid;	
        public String message;
    }
}