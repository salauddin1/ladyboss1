/*
 * Developer Name : Tirth Patel
 * Description    : This class checks for Twilio Number field on the case and using that creates or updates case.
*/
public class TwilioMessagePageController {
    public TwilioMessagePageController() {
        
    } 
    
    public void createOrUpdateCase(){
        
        String twilioNumber = ApexPages.currentPage().getParameters().get('To');
        String fromPhone = ApexPages.currentPage().getParameters().get('From');
        String body = ApexPages.currentPage().getParameters().get('Body');
        System.debug(fromPhone);
        System.debug(twilioNumber);
        if( fromPhone!= null && twilioNumber != null ){
            List<Case> caseList = new List<Case>();
            caseList = [SELECT id,status,ClosedDate,ContactId FROM Case WHERE Twilio_Number__c =: fromPhone];
            Datetime currentDateTime = Datetime.now().addDays(-7);
            Boolean isOpen = false;
            Boolean isReOpen = false;
            Case openId = null;
            Case reOpenId = null;
            System.debug(caseList);
            for(Case caseObject : caseList){
                if(caseObject.status != 'closed'){
                    isOpen = true;
                    openId = caseObject;
                }else if(caseObject.ClosedDate>=currentDateTime){
                    reOpenId = caseObject;
                    isReOpen = true;
                }
            }
            Case caseObj;
            if(!isOpen && !isReOpen){
                caseObj = new Case();
                caseObj.Twilio_Number__c = fromPhone;
                caseObj.status = 'Open';
                caseObj.subject = 'Inbox Message To : '+twilioNumber;
                caseObj.toTwilioNumber__c = twilioNumber;
                caseObj.Description = body;
                caseObj.origin = 'Phone';
                if(!Test.isRunningTest()){
                    //QueueSobject qobj = [SELECT Id,Queueid,Queue.name,SobjectType FROM QueueSobject where Queue.name='Support Team' and SobjectType= 'Case' limit 1];
                    caseObj.ownerId = [SELECT Id,Queueid,Queue.name,SobjectType FROM QueueSobject where Queue.name='Support Team' and SobjectType= 'Case' limit 1].Queueid;
                	//caseObj.ownerId = [SELECT Id FROM USER WHERE username = 'grant@ladybossweightloss.com' limit 1].Id;
                }
            }else if(isOpen){
                caseObj = openId;
            }else if(isReOpen){
                caseObj = reOpenid;
                caseObj.status = 'reopened';
            }
            
            upsert caseObj;
            CaseComment caseCommentObject = new CaseComment();
            caseCommentObject.ParentId =caseObj.id;
            caseCommentObject.CommentBody = body;
            insert caseCommentObject;
        }
    }
    
}