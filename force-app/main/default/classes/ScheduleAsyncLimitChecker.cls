global class ScheduleAsyncLimitChecker implements Schedulable, Database.AllowsCallouts {
     global void execute(SchedulableContext SC) {
          getAsyncLimit();
     }
     @future(callout=true)
     public static void getAsyncLimit(){
          Async_Apex_Limit__c  asyncLimit = [SELECT Remaining__c, Name From Async_Apex_Limit__c WHERE Name ='Async Limit Left'];
          RESTLimitResource rlr = RESTLimitResource.getLimitsResp();
          System.debug('RESTLimitResource '+rlr);
          if (rlr != null) {
               asyncLimit.Remaining__c = Decimal.valueOf(String.valueOf(rlr.dailyAsyncApexExecutions.Remaining));
               if (asyncLimit.Remaining__c<10000) {
                    sendEmail(asyncLimit.Remaining__c);
               }
          }
          update asyncLimit;
     }
     public static void sendEmail(Decimal asyncLimit){
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          string [] toaddress= New string[]{'grant@ladyboss.com','jay@ladyboss.com','tirth@ladyboss.com'};
          email.setSubject('AsyncApex Limits Alert! Remaining Limit :'+asyncLimit);
          email.setPlainTextBody('This is a system alert to inform about remaining AsyncApex Limit which is '+asyncLimit);
          email.setToAddresses(toaddress);
          Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
     }
}