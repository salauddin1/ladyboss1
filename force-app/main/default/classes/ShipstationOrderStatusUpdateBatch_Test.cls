@isTest
public class ShipstationOrderStatusUpdateBatch_Test {
    @isTest
    public Static void unittest1(){
        ShipStation_Orders__c ordr = new ShipStation_Orders__c();
        ordr.orderStatus__c= 'awaiting_shipment';
        ordr.orderId__c='20291736';
        ordr.orderKey__c='manual-768d148d7dd54452bde6109bc9b3a85a';
        insert ordr;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SHipStationRoutingWebhookClassMock());
        ShipstationOrderStatusUpdateBatch srs=new ShipstationOrderStatusUpdateBatch();
        Database.executeBatch(srs);
        System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new ShipstationordStatusSchedular());
        Test.stopTest();
    }
}