public class AssociateShiporderToOpportunity {
    public static void linkOpptoShipOrd (List<ShipStation_Orders__c> orderDetail){
        System.debug('======='+orderDetail);
        Set<String> OrdNumberSet = new Set<String>();
        Map<String, ShipStation_Orders__c> OrdNumberMap = new Map<String, ShipStation_Orders__c>();
        for(ShipStation_Orders__c ord : orderDetail) {
            if(ord.orderNumber__c.contains('ch_')) {
                OrdNumberSet.add(ord.orderNumber__c);
                OrdNumberMap.put(ord.orderNumber__c, ord);
            }
        }
        try {
            Set<String> oppIdSet = new Set<string>();
            List<opportunity> oppList = [select Id, Stripe_Charge_Id__c from opportunity where Stripe_Charge_Id__c != null AND Stripe_Charge_Id__c IN : OrdNumberSet];
            if(oppList.size()>0 && !OrdNumberMap.isEmpty()) {
                for(Opportunity opt : oppList) {
                    if(OrdNumberMap.containsKey(opt.Stripe_Charge_Id__c)) {
                        ShipStation_Orders__c curntOrd = OrdNumberMap.get(opt.Stripe_Charge_Id__c);
                        if(curntOrd != null) {
                            opt.ShipStation_Orders__c = curntOrd.Id;
                            opt.Linked_To_ShipStation_Order__c = true;
                            oppIdSet.add(opt.Stripe_Charge_Id__c);
                        }
                    }
                }
            }
            List<ShipStation_Orders__c> orderDetailUpdate =[select Id, orderNumber__c,Linked_To_Opportunity__c from ShipStation_Orders__c where orderNumber__c IN : oppIdSet FOR UPDATE];
            for(ShipStation_Orders__c shpOrd : orderDetailUpdate) {
                shpOrd.Linked_To_Opportunity__c = true;
            }
            ShipstationOrderStatusShippedBatch.flag = true;
            Update oppList;
            Update orderDetailUpdate;
        }
        catch (exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(orderDetail.size()>0) {   for(ShipStation_Orders__c c : orderDetail) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('linkOpptoShipOrd','AssociateShiporderToOpportunity',Ids, e));
        }    }
}