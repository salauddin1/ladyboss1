@RestResource(urlMapping='/ChargeStripeWebHook/*')
global without sharing class ChargeStripeWebHook {
	@HttpPost
	global static void ChargeStripe() {
		if(RestContext.request.requestBody!=null){
            String server = 'Production';
			system.debug('###body: '+RestContext.request.requestBody);
            try{
                //String strr = RestContext.request.requestBody.toString().replace('object','object_data').replace('currency','currency_data');
                //StripeCharge varCharge = StripeCharge.parse(strr);
                StripeCharge.StripeChargeObject varCharge = (StripeCharge.StripeChargeObject) json.deserialize(RestContext.request.requestBody.toString().replace('object','object_data').replace('currency','currency_data'),StripeCharge.StripeChargeObject.class);
                Payment__c pay = new Payment__c();            
                
                pay.External_ID__c = varCharge.data.object_data.id;
                pay.Status__c = varCharge.data.object_data.status;                
                pay.Statement_Descriptor__c = varCharge.data.object_data.statement_descriptor;
                pay.Description__c = varCharge.data.object_data.description;
                pay.Currency__c = varCharge.data.object_data.currency_data;
                pay.Paid__c = varCharge.data.object_data.paid;
                pay.Amount__c = varCharge.data.object_data.amount*0.01;
  				pay.Invoice_ID__c = varCharge.data.object_data.invoice;
                
                //Created Date Unix to Date
                Datetime createdDate;
                if(varCharge.data.object_data.created!=null){
                    createdDate = datetime.newInstance(0);
                    createdDate = createdDate.addSeconds(varCharge.data.object_data.created);
                    pay.Date__c = Date.valueOf(createdDate);
                }else{
                    pay.Date__c = system.today();
                }
                
                if(varCharge.data.object_data.customer != null){
                   	List<Account> oppSelectAcc = [select Id, Name, External_ID__c from Account where External_ID__c =:varCharge.data.object_data.customer];  
					if(oppSelectAcc.size()>0){
                        pay.Account__c = oppSelectAcc.get(0).Id; 
                    }
                }
                
                List<Opportunity> oppSelectOpp;
                if(varCharge.data.object_data.invoice != null){
                   	oppSelectOpp = [select Id, Name, External_ID__c from Opportunity where External_ID__c =:varCharge.data.object_data.invoice];  
					if(oppSelectOpp.size()>0){
                        pay.Opportunity__c = oppSelectOpp.get(0).Id; 
                    }else{
                        oppSelectOpp = [select Id, Name, External_ID__c from Opportunity where DML_From_Salesforce__c=true and AccountId=:pay.Account__c];  
                        if(oppSelectOpp.size()>0){
                            pay.Opportunity__c = oppSelectOpp.get(0).Id; 
                            oppSelectOpp.get(0).DML_From_Salesforce__c = false;
                        }
                    }
                }
                if(varCharge.data.object_data.metadata != null){
                   	oppSelectOpp = [select Id, Name, External_ID__c from Opportunity where id =:varCharge.data.object_data.metadata.order_id];  
					if(!oppSelectOpp.isEmpty()){
                        pay.Opportunity__c = oppSelectOpp.get(0).Id; 
                        oppSelectOpp.get(0).DML_From_Salesforce__c = false;
                    }
                }
                
                if(pay.Opportunity__c==null && pay.External_ID__c != null){
                   	oppSelectOpp = [select Id, Name, External_ID__c from Opportunity where External_ID__c =:pay.External_ID__c];  
					if(!oppSelectOpp.isEmpty()){
                        oppSelectOpp.get(0).StageName = 'Approved'; oppSelectOpp.get(0).Paid__c = true; oppSelectOpp.get(0).Closed__c = true; pay.Opportunity__c = oppSelectOpp.get(0).Id; oppSelectOpp.get(0).DML_From_Salesforce__c = false;
                    }
                }
                
                if(varCharge.data.object_data.source!=null){
                    pay.Source_ID__c = varCharge.data.object_data.source.id;
                    pay.Address_City__c = varCharge.data.object_data.source.address_city;
                    pay.Address_Country__c = varCharge.data.object_data.source.address_country;
                    pay.Address_Street__c = varCharge.data.object_data.source.address_line1;
                    pay.Address_State__c = varCharge.data.object_data.source.address_state;
                    pay.Address_Zip__c = varCharge.data.object_data.source.address_zip;
                    pay.Brand__c = varCharge.data.object_data.source.brand;
                    pay.Country__c = varCharge.data.object_data.source.country;
                    pay.Exp_Month__c = varCharge.data.object_data.source.exp_month;
                    pay.Exp_Year__c = varCharge.data.object_data.source.exp_year;
                    pay.Last4__c = varCharge.data.object_data.source.last4;
                }
                
                
                ApexUtil.isTriggerInvoked = true;
				upsert pay External_ID__c;
                if(oppSelectOpp.size()>0){
                    update oppSelectOpp;
                }
                ApexUtil.isTriggerInvoked = false;
                
                /*Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'});
                mail1.setSubject('Sandbox : ChargeStripeWebHook Request333');
                mail1.setHtmlBody(strr);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); */
            }catch(Exception e){
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); mail1.setSubject(server+': ChargeStripeWebHook Request'); mail1.setHtmlBody(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + RestContext.request.requestBody.ToString()); Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); 
            }
		}
		else{
    		RestResponse res = RestContext.response; 
            res.statusCode = 400;		    
		}
	}
}