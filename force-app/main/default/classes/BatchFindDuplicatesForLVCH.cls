/*
* Developer Name : Mehul Rathod
* Description : this batch finds duplicates based on email ids, contact with maximum dups (same email id) will be send first 
*/
public class BatchFindDuplicatesForLVCH implements  Database.Batchable<AggregateResult>,Schedulable  {
    public Iterable<AggregateResult> start(Database.BatchableContext bc) {
        List<Workflow_Configuration__c> oldcamplist = [SELECT Products__c,Source_Campaign__c,Target_Campaign__c FROM Workflow_Configuration__c where Products__c != null and Source_Campaign__c != null and Consider_For_CM_Movement__c = true];
        set<id> oldcampaignset = new set<id>();
        for(Workflow_Configuration__c ncm : oldcamplist){
            oldcampaignset.add(ncm.Source_Campaign__c);
        }
        if(oldcampaignset.size() > 0){
            String s ='';
            List<id> oldcampaignsetList = new List<id>();
            oldcampaignsetList.addall(oldcampaignset);
            for(id idd : oldcampaignset){
                s = s + '\''+idd+'\''+',';
            }
            s=s.removeEnd(',');
            s= '('+s+')';
            System.debug('s : '+s);
            if(Test.isRunningTest()){
                return new AggregateResultIterable('SELECT count(id), email contactEmail from contact where email != null and email!=\'notavail@gmail.com\' group by email having count(id) > 1 order by count(id) desc limit 100');
            }else{
                return new AggregateResultIterable('SELECT count(id), email contactEmail from contact where email != null and email!=\'notavail@gmail.com\' and id IN (Select contactid From CampaignMember WHERE campaignid in  '+s+') group by email having count(id) > 1 order by count(id) desc limit 100');
            }
            }else{
            return null;
        }
    }
    
    public void execute(SchedulableContext SC) {
        BatchFindDuplicatesForLVCH btc = new BatchFindDuplicatesForLVCH(); 
        Database.executeBatch(btc);
    } 
    
    public void execute(Database.BatchableContext bc, List<Sobject> lstSo){
        
        List<String> lstEmail = new List<String>();
        for(sObject sObj : lstSo) {
            AggregateResult ar = (AggregateResult)sObj;
            lstEmail.add((String)ar.get('contactEmail'));
        }
        List<Contact> lstContacts = [select id ,email from Contact where Email in :lstEmail order by createdDate asc ];
        Set<String> setEmails = new Set<String>();
        LIst<Contact> lstCntUpdate = new List<Contact>();
        for(Contact cnt : lstContacts)  {
            if(setEmails.add(cnt.Email.toLowerCase()))  {
                cnt.check_for_LVCH_dup__c = true;
                lstCntUpdate.add(cnt);   
            }  
        } 
        update lstCntUpdate;
        
    }
    
    public void finish(Database.BatchableContext bc){ 
        List<Contact> contList = [Select id from Contact Where check_for_LVCH_dup__c=true];
        // execute any post-processing operations
        if(!test.isRunningTest() )   {
            if(contList.size()>0){
                ContactMergeForLVCHBatch cmb = new ContactMergeForLVCHBatch(UserInfo.getSessionId());
                Database.executeBatch(cmb,1);
            }else{
                DeleteMemberFromCampainBatch db = new DeleteMemberFromCampainBatch();
                Database.executeBatch(db);
            }
            
        }
        
    }
    
    
}