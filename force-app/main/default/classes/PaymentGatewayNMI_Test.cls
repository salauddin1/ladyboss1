@IsTest
public class PaymentGatewayNMI_Test {
    static{
        NMI_Parameters__c np = new NMI_Parameters__c(User__c='user',Password__c='pwd',Endpoint__c='https://secure.nmi.com/api/');
        insert np;
    }
    
	static testMethod void testHttpRequest() {
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Map<String,String> requestMap = new Map<String,String>();
        requestMap.put('report_type','customer_vault');
        Test.startTest();
        PaymentGatewayNMI.HttpRequest(requestMap);
        Test.stopTest();
	}
    
    static testMethod void testHttpDirectPostTrue() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today());
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Map<String,String> requestMap = new Map<String,String>();
        requestMap.put('customer_vault','add_customer');
        requestMap.put('username','user');
        requestMap.put('password','pwd');
        requestMap.put('ccnumber','123456789');
        requestMap.put('ccexp','2022');
        requestMap.put('first_name','');
        requestMap.put('last_name','');
        Test.startTest();
        PaymentGatewayNMI.HttpDirectPost('customer', acc.Id, requestMap);
        Test.stopTest();
	}
    
    static testMethod void testXmlParser() {
        String xml = '<?xml version="1.0" encoding="UTF-8"?>' +
                        '<nm_response>' +
                        '<customer_vault>' +
                        '<customer id="1066341912">' +
                        '<first_name>1</first_name>' +
                        '<last_name></last_name>' +
                        '<address_1>3</address_1>' +
                        '<address_2></address_2>' +
                        '<company></company>' +
                        '<city>palm</city>' +
                        '<state>CA</state>' +
                        '<postal_code>92260</postal_code>' +
                        '<country>United States</country>' +
                        '<email>eugene+test3@dgwizard.com</email>' +
                        '<phone>2</phone>' +
                        '<fax></fax>' +
                        '<cell_phone></cell_phone>' +
                        '<customertaxid></customertaxid>' +
                        '<website></website>' +
                        '<shipping_first_name>1</shipping_first_name>' +
                        '<shipping_last_name></shipping_last_name>' +
                        '<shipping_address_1>3</shipping_address_1>' +
                        '<shipping_address_2></shipping_address_2>' +
                        '<shipping_company></shipping_company>' +
                        '<shipping_city>palm</shipping_city>' +
                        '<shipping_state>CA</shipping_state>' +
                        '<shipping_postal_code>92260</shipping_postal_code>' +
                        '<shipping_country>United States</shipping_country>' +
                        '<shipping_email>eugene+test3@dgwizard.com</shipping_email>' +
                        '<shipping_carrier></shipping_carrier>' +
                        '<tracking_number></tracking_number>' +
                        '<shipping_date></shipping_date>' +
                        '<shipping></shipping>' +
                        '<cc_number>4xxxxxxxxxxx1881</cc_number>' +
                        '<cc_hash>8627cfbcbe1a070d4d9e43734ae48798</cc_hash>' +
                        '<cc_exp>0131</cc_exp>' +
                        '<cc_start_date></cc_start_date>' +
                        '<cc_issue_number></cc_issue_number>' +
                        '<check_account></check_account>' +
                        '<check_hash></check_hash>' +
                        '<check_aba></check_aba>' +
                        '<check_name></check_name>' +
                        '<account_holder_type></account_holder_type>' +
                        '<account_type></account_type>' +
                        '<sec_code></sec_code>' +
                        '<processor_id></processor_id>' +
                        '<cc_bin>401288</cc_bin>' +
                        '<created>20171104234850</created>' +
                        '<updated>20171104234851</updated>' +
                        '<account_updated></account_updated>' +
                        '<customer_vault_id>1066341912</customer_vault_id>' +
                        '</customer>' +
                        ' </customer_vault>' +
                        '</nm_response>';
        Test.startTest();
        PaymentGatewayNMI.XmlParser(xml);
        Test.stopTest();
	}
    
    static testMethod void testpay() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 

        Test.startTest();
        PaymentGatewayNMI.pay(opp.Id);
        Test.stopTest();
	}
    
    static testMethod void testpayLightning() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 

        Test.startTest();
        PaymentGatewayNMI.payLightning(opp.Id);
        Test.stopTest();
	}
    
    static testMethod void testHttpPayment() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
        insert oli;
        
        List<OpportunityLineItem> lstOlis = new List<OpportunityLineItem>();
        lstOlis.add(oli);
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 

        Test.startTest();
        PaymentGatewayNMI.HttpPayment(opp,lstOlis);
        Test.stopTest();
	}
}