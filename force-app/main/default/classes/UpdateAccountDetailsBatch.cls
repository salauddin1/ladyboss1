global class UpdateAccountDetailsBatch implements Database.Batchable<SObject>{

	global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('select id,External_ID__c,Name,Credit_Card_Number__c,Email__c,LastModifiedDate,Last_Stripe_Modified_Date__c,Last_Email_Modified_Date__c,Last_CC_Modified_Date__c,BillingCountry,BillingState,BillingStreet,BillingCity,BillingPostalCode,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalCode from Account');
    }

	global void execute(Database.BatchableContext context, List<SObject> records) {
		List<Account_Old_Email__c> oldEmailList = new List<Account_Old_Email__c>();
		List<Account_Old_CC__c> oldCCList = new List<Account_Old_CC__c>();
		List<Account_Old_Stripe_Id__c> oldStripeList = new List<Account_Old_Stripe_Id__c>();
		List<Mail_Address__c> lstMailingAddresses = new List<Mail_Address__c>();
		Account_Old_CC__c oldCC;
		Account_Old_Stripe_Id__c oldStripe;
		Account_Old_Email__c oldEmail;

		for(Sobject sobj : records){
			Account act = (Account)sobj;
			if(act.Email__c!=null){
				oldEmail = new Account_Old_Email__c();  
				oldEmail.Last_Email__c = act.Email__c;
				oldEmail.Account__c = act.id;
				oldEmail.Name = act.Name;
				oldEmail.Last_Email_Modified_Date__c = Datetime.now();
				oldEmailList.add(oldEmail);
			}

			if(act.External_ID__c!=null){
				oldStripe = new Account_Old_Stripe_Id__c();
				oldStripe.Account__c = act.id;
				oldStripe.Last_Stripe_Id__c = act.External_ID__c;
				oldStripe.Last_Stripe_Modified_Date__c = Datetime.now();
				oldStripe.Name = act.Name;
				oldStripeList.add(oldStripe);
			}

			if(act.Credit_Card_Number__c!=null){
				oldCC = new Account_Old_CC__c();
				oldCC.Account__c = act.id;
				oldCC.Last_CC_Num__c = act.Credit_Card_Number__c;
				oldCC.Last_CC_Modified_Date__c = Datetime.now();
				oldCC.Name = act.Name;
				oldCCList.add(oldCC);
			}
			
			if(act.BillingCountry != null || act.BillingState != null || act.BillingStreet != null || act.BillingCity != null || act.BillingPostalCode != null )  {
				Mail_Address__c oldMailA = new Mail_Address__c();  
            		oldMailA.Last_Mail__c = String.valueOf(act.BillingStreet) + ', '+ String.valueOf(act.BillingCity) + ', '+String.valueOf(act.BillingState) + ', '+ String.valueOf(act.BillingCountry)+ ' - ' +String.valueOf(act.BillingPostalCode);
            		oldMailA.Account__c = act.id;
            		oldMailA.Name = act.Name;
            		oldMailA.Last_Mail_Modified_Date__c = Datetime.now();
        			oldMailA.Address_type__c = 'Billing Address';
        			lstMailingAddresses.add(oldMailA);
			}
			
			if(act.ShippingStreet != null || act.ShippingCity != null || act.ShippingState != null || act.ShippingCountry != null || act.ShippingPostalCode != null )  {
				Mail_Address__c oldMailA = new Mail_Address__c();  
            		oldMailA.Last_Mail__c = String.valueOf(act.ShippingStreet) + ', '+ String.valueOf(act.ShippingCity) + ', '+String.valueOf(act.ShippingState) + ', '+ String.valueOf(act.ShippingCountry)+ ' - ' +String.valueOf(act.ShippingPostalCode);
            		oldMailA.Account__c = act.id;
            		oldMailA.Name = act.Name;
            		oldMailA.Last_Mail_Modified_Date__c = Datetime.now();
        			oldMailA.Address_type__c = 'Shipping Address';
        			lstMailingAddresses.add(oldMailA);
			}
			
		
		}
		
		
		
		insert oldCCList;
    		insert oldStripeList;
    		insert oldEmailList;
    		insert lstMailingAddresses;
	}

	global void finish(Database.BatchableContext context) {
    
	}

}