global class FuturePaymentScheduler implements Schedulable{
	global void execute(SchedulableContext sc) {
        database.executebatch(new FuturePaymentBatch(), 10);
    }
}