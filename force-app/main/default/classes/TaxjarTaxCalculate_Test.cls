@isTest
public class TaxjarTaxCalculate_Test {
    @isTest
    public static void insertMethod1(){
    	Test.startTest();
    	TaxjarTaxCalculate.ratemethod('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 'taxCode');
        TaxjarTaxCalculate.ratemethodForOppUpdate('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 'taxCode','sub');
        TaxjarTaxCalculate.createTransaction('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 1.50, 'planId', 'chargeId', 'prodName', 12.50, 'createdUsing');
        TaxjarTaxCalculate.createTransactionnotfuture('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 1.50, 'planId', 'chargeId', 'prodName', 12.50, 'createdUsing');
        TaxjarTaxCalculate.refundTransaction('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 1.50, 'planId', 'chargeId', 'prodName', 12.50, 'createdUsing');
        TaxjarTaxCalculate.ratemethodHub('line1', 'city', 'region', 'country', 'postalCode', 12.50, 1, 'taxCode');
        Test.stopTest();
    }
}