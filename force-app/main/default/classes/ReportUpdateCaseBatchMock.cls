// Created By : Sourabh Badole 
// Date       : 15-3-2019
@isTest
global class ReportUpdateCaseBatchMock implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {       
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id": "ch_1E8SgWBwLSk1v1oh6AGORpxc", "object": "charge", "amount": 3994, "amount_refunded": 0, "application": null, "application_fee": null, "application_fee_amount": null, "balance_transaction": null, "captured": false, "created": 1551274976, "currency": "usd", "customer": "cus_EQm8e1lPsw0oIG", "description": "Payment for invoice 13D90D1-0002", "destination": null, "dispute": null, "failure_code": "card_declined", "failure_message": "Your card was declined.", "fraud_details": { }, "invoice": null, "livemode": false, "metadata": { }, "on_behalf_of": null, "order": null, "outcome": { "network_status": "declined_by_network", "reason": "generic_decline", "risk_level": "normal", "risk_score": 45, "seller_message": "The bank did not return any further details with this decline.", "type": "issuer_declined" }, "paid": false, "payment_intent": "pi_1E8SgWBwLSk1v1ohJEihOQR7", "receipt_email": null, "receipt_number": null, "receipt_url": "https://pay.stripe.com/receipts/acct_1BxpDGBwLSk1v1oh/ch_1E8SgWBwLSk1v1oh6AGORpxc/rcpt_Ebg2EUaLC4SSsXORuEB4UQqaK55EhXI", "refunded": false, "refunds": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/charges/ch_1E8SgWBwLSk1v1oh6AGORpxc/refunds" }, "review": null, "shipping": null, "source": { "id": "card_1E8STJBwLSk1v1ohCgxqac7V", "object": "card", "address_city": null, "address_country": null, "address_line1": null, "address_line1_check": null, "address_line2": null, "address_state": null, "address_zip": null, "address_zip_check": null, "brand": "Visa", "country": "US", "customer": "cus_EQm8e1lPsw0oIG", "cvc_check": null, "dynamic_last4": null, "exp_month": 12, "exp_year": 2034, "fingerprint": "DdrxpLlX0sSb0HOn", "funding": "credit", "last4": "0341", "metadata": { }, "name": null, "tokenization_method": null }, "source_transfer": null, "statement_descriptor": null, "status": "failed", "transfer_data": null, "transfer_group": null }');
        res.setStatusCode(200);
        return res;
    }

}