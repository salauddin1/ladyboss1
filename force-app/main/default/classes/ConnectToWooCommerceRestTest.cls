@isTest
public class ConnectToWooCommerceRestTest {
    public static testMethod void ConnectToWooCommerce1(){
        Test.setMock(HttpCalloutMock.class, new CouponHttpCalloutMock2());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        cc.Product_Exclude_Categories__c = '22,32';
        insert cc;
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Store';
        cs.Site_Url__c = 'www.test.com';
        insert cs;        
        Contact ct = new Contact();
        ct.LastName = 'test';
        insert ct;
        ct.PAP_refid__c = 'testCoupon';
        update ct;
        ct.PAP_refid__c = 'testCoupon1';
        ct.Coupon_Code_Id__c = 'couponTESt';
        update ct;
        List<Contact> ctList = new List<Contact>();
        ctList.add(ct);
        
        test.startTest();
        ConnectToWooCommerceRest.createPostRequest(ctList);
        test.stopTest();
        
    }
    public static testMethod void updateConnectToWooCommerce(){
        Test.setMock(HttpCalloutMock.class, new CouponHttpCalloutMock2());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        insert cc;
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Store';
        cs.Site_Url__c = 'www.test.com';
        insert cs; 
        Contact ct = new Contact();
        ct.LastName = 'test';
        insert ct;
        ct.PAP_refid__c = 'testCoupon';
        update ct;
        ct.PAP_refid__c = 'test2SourCoupon';
        update ct;
        
        List<Contact> ctList = new List<Contact>();
        ctList.add(ct);
        
        
        test.startTest();
        ConnectToWooCommerceRest.updatePostRequest(ctList);
        test.stopTest();
        
        
    }
    
}