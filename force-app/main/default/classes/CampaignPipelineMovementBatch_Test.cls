@isTest
public class CampaignPipelineMovementBatch_Test {
    @isTest
    public static void test1() {
        Triggers_activation__c  cusSetting = new Triggers_activation__c();
        cusSetting.SuccessPaymentWithCaseLink__c = true;
        insert cusSetting;
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'test@gmail.com';
        con.Product__c = 'Supplement';      
        insert con;
        
        Campaign camp = new Campaign();
        camp.Name = 'Burn Supplement';
        camp.Product__c = 'Supplement';
        camp.Type = 'Pipeline';
        insert camp;
        
        
        Campaign camp1 = new Campaign();
        camp1.Name = 'Burn Supplement';
        camp1.Product__c = 'Supplement';
        camp1.Type = 'Phone Team';
        insert camp1;
        
        CampaignMember campMem = new CampaignMember();
        campMem.Status = 'Sent';
        campMem.ContactId = con.Id;
        campMem.CampaignId = camp.Id;
        insert campMem;
        
        Opportunity opp = new Opportunity ();
        opp.Name = 'BurnSupplement';
        opp.Contact__c = con.Id;
        opp.CloseDate  = Date.today();
        opp.StageName = 'Closed Won';
        insert opp;
        Test.setCreatedDate(opp.Id, DateTime.newInstance(2012,12,12));

        
        
        
        
        Test.startTest();
        CampaignPipelineMovementBatch campa = new CampaignPipelineMovementBatch();
        Id batchId = Database.executeBatch(campa);
        Test.StopTest();
    }
    
    @isTest
    public static void test2() {
        Triggers_activation__c  cusSetting = new Triggers_activation__c();
        cusSetting.SuccessPaymentWithCaseLink__c = true;
        insert cusSetting;
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'test@gmail.com';
        con.Product__c = 'Supplement';      
        insert con;
        
        Campaign camp = new Campaign();
        camp.Name = 'Burn Supplement';
        camp.Product__c = 'Supplement';
        camp.Type = 'Pipeline';
        insert camp;
        
        List<CampaignMember> campList = new List<CampaignMember>();
        Campaign camp1 = new Campaign();
        camp1.Name = 'Burn Supplement';
        camp1.Product__c = 'Supplement';
        camp1.Type = 'Phone Team';
        insert camp1;
        
        CampaignMember campMem = new CampaignMember();
        campMem.Status = 'Sent';
        campMem.ContactId = con.Id;
        campMem.CampaignId = camp.Id;
        campList.add(campMem);
        
        insert campList;
        
        Opportunity opp = new Opportunity ();
        opp.Name = 'BurnSupplement';
        opp.Contact__c = con.Id;
        opp.CloseDate  = Date.today();
        opp.StageName = 'Closed Won';
        insert opp;
        Test.setCreatedDate(opp.Id, DateTime.newInstance(2012,12,12));

        
        
        
        
        Test.startTest();
        System.enqueueJob(new CampaignMemberPipelineBatchDelQueue(new List<CampaignMember>(campList)));    
        Test.StopTest();
    }
}