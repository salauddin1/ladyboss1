public class updateCard {
    public static void meth() {
        List<opportunity> oppList = [select id,CustomerID__c,Stripe_Profile__r.Stripe_Customer_Id__c,Stripe_Profile__c, Stripe_Charge_Id__c,Contact__c from opportunity where  stripe_Charge_Id__c!=null and Card__c=null   and RecordType.Name= 'Charge' and CardId__c!=null and CardId__c!='' and RequestId__c='' and Success_Failure_Message__c!='charge.refunded'   order by lastmodifieddate limit 1];
Set<string> idset = new Set<string>();
for(opportunity op : oppList) {
    idset.add(op.CustomerID__c);
    System.debug('==OppOb =>'+op );
    op.RequestId__c='Na';
    
}
update oppList;
List< Stripe_Profile__c > stripeProfileList = [select id,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c in: idset];
List< Card__c > lstOpp        = new List< Card__c >();
   System.debug('==OppOb =>'+stripeProfileList );
        for(Stripe_Profile__c stripeProf:stripeProfileList){
          List< Stripe_Profile__c > param = new List< Stripe_Profile__c >();
           param.add(stripeProf);
            //List< Card__c > OppOb = StripeBatchHandler.getAllCard(param,'card');
            
                System.debug('==OppOb =>' );
        //check if profile exists or not 
        // List<Stripe_Profile__c> stripeProfList = [select id ,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
        String customerId = stripeProfileList[0].Stripe_Customer_Id__c;
        Map<string,string> conStripeMap = new Map<string,string> ();
        Map<string,string> conStripeMap1 = new Map<string,string> ();
        for(Stripe_Profile__c con : stripeProfileList ){
            conStripeMap.put(con.Stripe_Customer_Id__c,con.id );
            conStripeMap1.put(con.Stripe_Customer_Id__c,con.Customer__c);
        }
        
        
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/customers/'+customerId+'/sources?object=card');
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                response = hs.getBody();
                statusCode = hs.getStatusCode();
            } catch (CalloutException e) {
                //return null;
            }
        }
        if(Test.isRunningTest()){
         response = '{ "object": "list", "data": [ { "id": "card_1EGAqnFzCf73siP0e9xVOl8s", "object": "card", "address_city": "Albuquerque", "address_country": "United States of America", "address_line1": "20200 Indian School", "address_line1_check": "pass", "address_line2": null, "address_state": "NM", "address_zip": "87110", "address_zip_check": "pass", "brand": "Visa", "country": "CA", "customer": "cus_Eje9LCKHjiRRn3", "cvc_check": "pass", "dynamic_last4": null, "exp_month": 1, "exp_year": 2024, "fingerprint": "89Hw4xtIbs92mApd", "funding": "credit", "last4": "1881", "metadata": { }, "name": "Jose Test", "tokenization_method": null } ], "has_more": false, "url": "/v1/customers/cus_Eje9LCKHjiRRn3/sources" }';
                statusCode = 200;
        }
        List<Card__c> cardList =  new List<Card__c>();
      
        if(statusCode == 200 ){
            System.debug(response);
         
                //StripeCard o = (StripeCard.parse(response));
                //System.debug('---------StripeCard-o-----------'+o);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                List<Map<String, Object>> data = new List<Map<String, Object>>();
                for (Object instance : (List<Object>)results.get('data')){
                    data.add((Map<String, Object>)instance);
                }
                 
                List<Object> lstCustomers = (List<Object>)results.get('data');
                Object[] aryCustomers = (Object[]) results.get('Customers');
               
                String customerIdExst = '';
                String stripeId = '';
                List<card__c> crdListUpdate = new List<card__c>();
                Boolean isUpdate =true;
                for (Object customer : lstCustomers) {
                    isUpdate =true;
                    // now get attributes for this card.
                    Map<String, Object> customerAttributes = (Map<String, Object>)customer;
                    // now loop through our card attributes.
                    
                    customerId =  String.Valueof(customerAttributes.get('customer'));
                    List<card__c> lstUpdateCard = [select id,Stripe_Profile__c,Card_ID__c,CustomerID__c from card__c where (CustomerID__c =: customerId and CustomerID__c !=null) OR Stripe_Card_Id__c=: String.Valueof(customerAttributes.get('id'))];
                     map<string,string> cardMap = new map<string,string>();
                    for(card__c cd : lstUpdateCard ) {
                       cardMap.put(cd.Card_ID__c ,cd.id);
                    }
                        card__c crd = new card__c();
                        crd.Stripe_Profile__c = conStripeMap.get(String.Valueof(customerAttributes.get('customer')));
                        stripeId = conStripeMap.get(String.Valueof(customerAttributes.get('customer')));
                        crd.contact__c = conStripeMap1.get(String.Valueof(customerAttributes.get('customer')));
                        crd.CustomerID__c = String.Valueof(customerAttributes.get('customer'));
                        
                        
                        
                        
                        crd.Billing_Zip_Postal_Code__c = String.Valueof(customerAttributes.get('address_zip'));
                        
                        crd.Card_ID__c = String.Valueof(customerAttributes.get('id'));
                        
                        crd.Cvc_Check__c = String.Valueof(customerAttributes.get('cvc_check'));
                        
                        crd.Billing_Street__c = String.Valueof(customerAttributes.get('address_state'));
                        
                        crd.Billing_Country__c = String.Valueof(customerAttributes.get('address_country'));
                        
                        crd.Billing_City__c = String.Valueof(customerAttributes.get('address_city'));
                        
                        crd.Expiry_Year__c = String.Valueof(customerAttributes.get('exp_year'));
                        
                        crd.Billing_State_Province__c= String.Valueof(customerAttributes.get('address_state'));
                        
                        crd.Stripe_Card_Id__c = String.Valueof(customerAttributes.get('id'));
                        
                        crd.Last4__c = String.Valueof(customerAttributes.get('last4'));
                        
                        
                        if((Integer.Valueof(customerAttributes.get('exp_month'))==11 ) || Integer.Valueof(customerAttributes.get('exp_month'))==12){
                            crd.Expiry_Month__c = String.Valueof(customerAttributes.get('exp_month'));
                        }else
                        {
                            crd.Expiry_Month__c = '0'+String.Valueof(customerAttributes.get('exp_month'));
                            
                        }
                        
                        crd.Name_On_Card__c = String.Valueof(customerAttributes.get('name'));
                        
                        crd.Brand__c= String.Valueof(customerAttributes.get('brand'));
                        
                        
                        if((String.Valueof(customerAttributes.get('brand')).contains('Visa')) || (String.Valueof(customerAttributes.get('brand')).contains('MasterCard')) ||(String.Valueof(customerAttributes.get('brand')).contains('Discover')) || (String.Valueof(customerAttributes.get('brand')).contains('JCB'))){
                            crd.Credit_Card_Number__c = '000000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        else if((String.Valueof(customerAttributes.get('brand')).contains('American Express'))){
                            crd.Credit_Card_Number__c = '00000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        else if((String.Valueof(customerAttributes.get('brand')).contains('Diners Club'))){
                            crd.Credit_Card_Number__c = '0000000000'+String.Valueof(customerAttributes.get('last4'));
                        }
                        
                        crd.isBatchCard__c=true;
                        crd.Error_Message__c = '';
                        if(!cardMap.containsKey(String.Valueof(customerAttributes.get('id'))))
                         cardList.add(crd);
                    
                    
                    
                }
                
               
        }
            
            
            if(cardList != null  && cardList.size() > 0 && !cardList.isEmpty()){
            for (Card__c op1 :cardList ) {
                lstOpp.add(op1);
            }
        }   
        //System.debug('===>'+lstOpp);
        if(lstOpp.size() > 0 && !lstOpp.isEmpty() ) {
            insert lstOpp;
             system.debug(stripeProfileList.size());
        }
    }
}
}