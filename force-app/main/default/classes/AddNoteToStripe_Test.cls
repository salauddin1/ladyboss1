@isTest
public class AddNoteToStripe_Test {
    @isTest
    public static  void test(){
        
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'ashish.sharma12@gmail.com';
        insert con;
        
        Stripe_Profile__c st = new Stripe_Profile__c();
        st.Customer__c = con.Id;
        st.Stripe_Customer_Id__c = 'cus_D73euJKZ80dsRI';
        insert st;
        Opportunity op = new Opportunity();
        op.name = 'test opp';
        op.CloseDate = system.today();
        op.StageName = 'Approved';
        insert op;
        Customer_Note__c cust = new Customer_Note__c();
        cust.Customer_Note__c = 'test Stripe';
        cust.Customer_key__c = 'Note : 556';
        cust.Contact__c = con.Id;
        cust.Stripe_Profile__c = st.Id;
        insert cust;
        Case cs = new Case();
        cs.ContactId = con.Id;
        insert cs;
        
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem opLineItem = new OpportunityLineItem();
        opLineItem.Product2Id = prod.Id;
        opLineItem.Opportunity__c = op.Id;
        opLineItem.OpportunityId = op.Id;
        opLineItem.Quantity = 1;
        opLineItem.UnitPrice = 2.00;
        opLineItem.PricebookEntryId = customPrice.Id;
        opLineItem.Opportunity_Name__c='CLUB';
        insert opLineItem;
        test.startTest();
        AddNoteToStripe.getStripeCustomer(con.Id,'Contact');
        
        AddNoteToStripe.updateCustomerMetdata('test Stripe',st.Stripe_Customer_Id__c,con.Id,st.Id,'Contact');
        
        Test.setMock(HttpCalloutMock.class, new AddNoteToStripe_Mock());
        
        AddNoteToStripe.getAllCustomer(con.Id,'Contact');
        AddNoteToStripe.getStripeLineItem(con.Id,'Contact');
        AddNoteToStripe.getStripeLineItem(cs.Id,'Case');
        AddNoteToStripe.findObjectAPIName(con.Id);
        AddNoteToStripe.updateObject(cust.Customer_Note__c,con.Id,cust.Id,st.Id,'Contact');
        AddNoteToStripe.setButtonTag(con.Id, 'Contact');
        AddNoteToStripe.setLifeTimeCust(con.Id, 'Contact');
        AddNoteToStripe.removeLifeTimeCust(con.Id, 'Contact');
        test.stopTest();
    }
}