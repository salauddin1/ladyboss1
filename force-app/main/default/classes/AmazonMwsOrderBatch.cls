/*
* Developer name : Tirth Patel
* Description : This batch fetches orders from Amaozon Seller and creates contact records using buyermail,buyername and address of an order.
*/
global class AmazonMwsOrderBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable {
    String NextToken='';
    global void execute(SchedulableContext SC) {
        AmazonMwsOrderBatch btc = new AmazonMwsOrderBatch();
        Database.executeBatch(btc);
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Name,LastUpdatedAfter__c,NextToken__c FROM AmazonOrder__c where Name='Amazon Order Keys' LIMIT 1]);
    }
    global void execute(Database.BatchableContext bc, List<AmazonOrder__c> orderData){
        
        NextToken = orderData.get(0).NextToken__c;
        DateTime LastUpdatedDate = orderData.get(0).LastUpdatedAfter__c;
        
        //Current time in GMT ISO 8601
        String timestamp = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        timestamp = EncodingUtil.urlEncode(timestamp,'UTF-8');
        
        //LastUpdatedAfter in GMT ISO 8601
        String timestamp2;
        if(LastUpdatedDate!=null){
            timestamp2 = LastUpdatedDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            timestamp2 = EncodingUtil.urlEncode(timestamp2,'UTF-8');
        }
        
        //Amazon Variables
        String action = 'ListOrders';
        if(NextToken==null || NextToken==''){
            action = 'ListOrders';
        }else{
            action = 'ListOrdersByNextToken';
            NextToken = EncodingUtil.urlEncode(NextToken,'UTF-8');
        }
        AmazonMwsKeys__c mwsKeys = [SELECT AWSAccessKeyId__c,marketplaceidUs__c,MWSAuthToken__c,Name,secretKey__c,SellerId__c,SignatureMethod__c,SignatureVersion__c,url__c,Version__c FROM AmazonMwsKeys__c where name='amazonKeys'];
        String version = mwsKeys.Version__c;
        String signatureVersion = mwsKeys.SignatureVersion__c;
        String signatureMethod = mwsKeys.SignatureMethod__c;
        String marketplaceId= mwsKeys.marketplaceidUs__c;
        String sellerId = mwsKeys.SellerId__c;
        String endpoint = mwsKeys.url__c;
        String accessKey = mwsKeys.AWSAccessKeyId__c;
        String amazonSecretKey = mwsKeys.secretKey__c;
        String authToken = mwsKeys.MWSAuthToken__c;
        
        //Construct a query string with the query information
        String queryString = 'AWSAccessKeyId=' + accessKey + 
            '&Action=' + action;
        endpoint = endpoint + 'AWSAccessKeyId=' + accessKey + 
            '&Action=' + action;    
        
        if(NextToken==null || NextToken==''){
            queryString = queryString +
                '&LastUpdatedAfter='+timestamp2;
            endpoint = endpoint +
                '&LastUpdatedAfter='+timestamp2;
        }
        queryString = queryString + 
            '&MWSAuthToken='+authToken;
        
        if(NextToken==null || NextToken==''){
            queryString = queryString +
                '&MarketplaceId.Id.1=' + MarketplaceId;
            endpoint = endpoint +
                '&MarketplaceId.Id.1=' + MarketplaceId;
        }else{
            queryString = queryString +
                '&NextToken=' + NextToken;
            endpoint = endpoint +
                '&NextToken=' + NextToken;
        }
        
        queryString = queryString +
            '&SellerId=' + SellerId +
            '&SignatureMethod=' + SignatureMethod  +
            '&SignatureVersion=' + SignatureVersion  +
            '&Timestamp=' + timestamp  +
            '&Version=' + version;
        
        String stringtoSign = 'POST' + '\n' +
            'mws.amazonservices.com' + '\n' +
            '/Orders/2013-09-01' + '\n' +
            queryString;
        
        //Covert query string to signature using Amazon secret key as key
        Blob mac = Crypto.generateMac('HMacSHA256', blob.valueof(stringtoSign),blob.valueof(amazonSecretKey));
        String signature = EncodingUtil.base64Encode(mac);
        signature = EncodingUtil.urlEncode(signature,'UTF-8');
        HttpRequest req = new HttpRequest();    
        req.setEndpoint(endpoint +
                        '&MWSAuthToken='+authToken + 
                        '&SignatureVersion='+SignatureVersion +
                        '&Timestamp=' + timestamp +
                        '&Version=' + version +
                        '&Signature=' + signature +
                        '&SignatureMethod='+ signatureMethod +
                        '&SellerId=' + sellerId );
        req.setMethod('POST');
        Http http = new Http();
        HttpResponse res = http.send(req);
        if(res.getStatusCode() == 200){
            AmazonOrderHelper.insertOrders(res.getBody()); 
        }
    } 
    // If NextToken is not null then batch will be scheduled to run after one minute. One minute break is kept to avoid api limit issue.
    global void finish(Database.BatchableContext bc){
        List<AmazonOrder__c> orderData = [SELECT LastUpdatedAfter__c,NextToken__c FROM AmazonOrder__c where Name='Amazon Order Keys' LIMIT 1];
        NextToken = orderData.get(0).NextToken__c;
        if(NextToken!='' && NextToken!=null){
            DateTime dt = System.now().addMinutes(1);
            Integer minute = dt.minute();
            Integer second = dt.second();
            Integer hour = dt.hour();
            Integer year = dt.year();
            Integer month = dt.month();
            Integer day = dt.day();
            AmazonMwsOrderBatch aw = new AmazonMwsOrderBatch();
            String sch = second+' '+minute+' '+hour+' '+day+' '+month+' ? '+year;
            String jobID = system.schedule('amazon order batch '+System.now(), sch, aw);
        }
    }
}