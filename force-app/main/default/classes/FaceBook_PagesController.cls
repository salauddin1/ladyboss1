public class FaceBook_PagesController {
	public String token{get;set;}
	public String pageId{get;set;}
	public FacebookWrappers.PageWrapper pageWrapper{get;set;}
	public List<FacebookPage__c> facebookPageList{get;set;}
    public webhook_Id_Secret__mdt id_secret{get; set;}
	public FacebookWrappers.AccessToken longLiveToken{get;set;}
	public String checkedListStr{get;set;}
	public FaceBook_PagesController() {
		 id_secret = [SELECT client_id__c,client_secret__c,DeveloperName,redirect_uri__c FROM webhook_Id_Secret__mdt where DeveloperName = 'webhookAppmdt'];
		 System.debug(id_secret.client_id__c);
         System.debug(id_secret.client_secret__c);
         System.debug(id_secret.redirect_uri__c);
	}

	public void onInit(){
        System.debug('inDoinit');
		String code = ApexPages.currentPage().getParameters().get('code');	
		if(code!=null){
			//code = code.substring(0,code.indexOf('&state'));
			
			System.debug(code);
			Map<String,String> paramMap = new Map<String,String>();
			paramMap.put('client_id',id_secret.client_id__c);
			paramMap.put('client_secret',id_secret.client_secret__c);
			paramMap.put('redirect_uri',id_secret.redirect_uri__c);
			paramMap.put('code',code);
			List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
			String response = FaceBookMessagesAPI.callAPI('/oauth/access_token','GET',paramList);
            System.debug('30 : '+response);
            //response = null;
			FacebookWrappers.AccessToken shortLiveToken = (FacebookWrappers.AccessToken)JSON.deserialize(response,FacebookWrappers.AccessToken.class);
			System.debug('shortLived : '+shortLiveToken.access_token);
			paramMap = new Map<String,String>();
            System.debug('client_id : '+id_secret.client_id__c);
			paramMap.put('client_id',id_secret.client_id__c);
			paramMap.put('client_secret',id_secret.client_secret__c);
			paramMap.put('fb_exchange_token',shortLiveToken.access_token);
			paramMap.put('grant_type','fb_exchange_token'); 
			paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
			response = FaceBookMessagesAPI.callAPI('/oauth/access_token','GET',paramList);
			longLiveToken = (FacebookWrappers.AccessToken)JSON.deserialize(response,FacebookWrappers.AccessToken.class);
			token = longLiveToken.access_token;
			System.debug('longLived : '+longLiveToken.access_token);
			paramMap = new Map<String,String>();
			paramMap.put('access_token',longLiveToken.access_token);
			//paramMap.put('limit','25');
			paramMap.put('fields','about,emails,name,username,category,access_token');
			paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
			response = FaceBookMessagesAPI.callAPI('/me/accounts','GET',paramList);
			pageWrapper = (FacebookWrappers.PageWrapper)JSON.deserialize(response,FacebookWrappers.PageWrapper.class);
			facebookPageList = updatePages();
            System.debug('facebookPageList : '+facebookPageList);
		}
	}

	public List<FacebookPage__c> updatePages(){
		List<FacebookPage__c> facebookPageList = new List<FacebookPage__c>();
		List<Contact> contactToUpsertList = new List<Contact>();
        System.debug('pageWrapper.data : '+pageWrapper.data);
		for(FacebookWrappers.PageObject pageData : pageWrapper.data){
			FacebookPage__c fb = new FacebookPage__c();
			fb.Page_Id__c = pageData.Id;
			fb.Page_Name__c = pageData.name;
			fb.username__c = pageData.username;
			fb.about__c = pageData.about;
			fb.category__c = pageData.category;
			fb.Page_Token__c = pageData.access_token;
			fb.LongLive_User_Token__c = longLiveToken.access_token;
			Contact contactObject = new Contact();
			contactObject.LastName = pageData.name;
			contactObject.Fb_User_Id__c = pageData.Id;
			contactToUpsertList.add(contactObject);
			facebookPageList.add(fb);
		}
		if(facebookPageList.size()>0){
			upsert facebookPageList Page_Id__c;
			upsert contactToUpsertList Fb_User_Id__c;
		}

		return [SELECT id,Page_Id__c,Last_Fetch_Time__c,about__c,category__c,username__c,isActive__c,Page_Name__c,Page_Token__c FROM FacebookPage__c WHERE id IN :facebookPageList];
	}

	public PageReference savePage(){
	/*	System.debug(checkedListStr);
		List<String> checkedList = (List<String>)JSON.deserialize(checkedListStr,List<String>.class);
		for(FacebookPage__c fb : facebookPageList){
			if(checkedList.contains(fb.id)){
				//Map<String,String> paramMap = new Map<String,String>();
				//paramMap.put('access_token',fb.Page_Token__c);
				//List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
				//String response = FaceBookMessagesAPI.callAPI('/'+fb.Page_Id__c+'/subscribed_apps','POST',paramList);
				//System.debug(response);
				fb.isActive__c = true;
			}else{
				fb.isActive__c = false;
			}
		}     */
		if(facebookPageList.size()>0){
			update facebookPageList;
		}
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved successfully!'));
		  BatchKeepUpdatingFacebook br = new BatchKeepUpdatingFacebook();
        Database.executeBatch(br,10);

		return null;
	}
}