@isTest
public class StripeFailedCharge_Service_Test {
    
    public static testMethod void test1(){
        Profile pf= [Select Id from profile where Name='System Administrator'];
        User u = new User(
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = pf.Id
            
        );
        insert u;
        
        Case_User__c cu = new Case_User__c();
        cu.UserId__c = u.Id;
        insert cu;
        
        
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'Unlimited';
        con.Email = 'test@gmail.com';
        insert Con;
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_E0w3qq3i41QeYa';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        /*
        Card__c Card =new Card__c();
        Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
        Card.Contact__c =Con.Id;
        insert Card ;
        
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        op.Description = 'Payment for invoice 993D93F-0026';
        op.CustomerID__c = 'cus_E0w3qq3i41QeYa';
        op.Stripe_Charge_Id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        op.Contact__c = Con.Id;
        op.Stripe_Profile__c = StripePro.id;
        insert op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Stripe_charge_id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        ol.Subscription_Id__c = 'sub_EtMQI0bKEYfNCn';
        insert ol;
    */    
        WebHookSecrets__c wb = new WebHookSecrets__c();
        wb.name = 'Stripe_FailedChargeService';
        wb.Webhook_Name__c = 'Stripe_FailedChargeService';
        wb.Webhook_Secret__c = 'whsec_cIdDwVu0vpFqtbk13jL35lUQ8RhG2zHS';
        insert wb;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{"id":"evt_1Dl9xoFzCf73siP0QrWpTnaL","object":"event","api_version":"2018-02-28","created":1545721468,"data":{"object":{"id":"ch_1Dl9xnFzCf73siP0LyKD4qMk","object":"charge","billing_details": {"address": {"city": "Fort Morgan ","country": "United States of America","line1": "14979 Country Rd 17","line2": null,"postal_code": "80701","state": "Colorado "},"email": null,"name": "Annette Rusch ","phone": null},"amount":100,"amount_refunded":0,"application":null,"application_fee":null,"balance_transaction":null,"captured":false,"created":1545721467,"currency":"usd","customer":"cus_E0w3qq3i41QeYa","description":"Payment for invoice 993D93F-0026","destination":null,"dispute":null,"failure_code":"card_declined","failure_message":"Your card was declined.","fraud_details":{},"invoice":"in_1EPZhYFzCf73siP0F9K6XAL1","livemode":true,"metadata":{"Shipping Address":"Albuquerque 10010 Indian School Rd. NE New Mexico United States 87112","Full Name":"GrantRefundfunction GrantRefundfunction2","Email":"grant+grantrefundfunction@ladyboss.com","Phone":"234-234-2342","Sales Person":"Tirth Patel","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss.my.salesforce.com/003f400000kse8rAAA","Fulfillment Product Name":"TEST $1 ProductLABS|","Street":"10010 Indian School Rd. NE","City":"Albuquerque","State":"New Mexico","Postal Code":"87112","Country":"United States"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"declined_by_network","reason":"do_not_honor","risk_level":"normal","risk_score":1,"seller_message":"The bank returned the decline code `do_not_honor`.","type":"issuer_declined"},"paid":false,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/charges/ch_1Dl9xnFzCf73siP0LyKD4qMk/refunds"},"review":null,"shipping":null,"source":{"id":"card_1DXT4SBwLSk1v1ohBcsQwM2T","object":"card","address_city":"Albuquerque","address_country":"United States","address_line1":"10010 Indian School Rd. NE","address_line1_check":"pass","address_line2":null,"address_state":"New Mexico","address_zip":"87112","address_zip_check":"pass","brand":"American Express","country":"US","customer":"cus_E0w3qq3i41QeYa","cvc_check":null,"dynamic_last4":null,"exp_month":7,"exp_year":2022,"fingerprint":"KfQdByKUuKuf6iEL","funding":"credit","last4":"1014","metadata":{},"name":"Grant test","tokenization_method":null},"source_transfer":null,"statement_descriptor":"TEST $1 ProductLABS","status":"failed","transfer_group":null}},"livemode":true,"pending_webhooks":6,"request":{"id":"req_d4rTiHNBHtbVW9","idempotency_key":null},"type":"charge.failed"}';
        req.requestBody = Blob.valueOf(body);
        req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
            test.startTest();
    
        StripeFailedCharge_Service.upsertCase();
        
        test.stopTest();         
    }
    public static testMethod void test2(){
        
        Profile pf= [Select Id from profile where Name='System Administrator'];
        
        Contact cnt = new Contact();
        cnt.Email = 'grant+grantrefundfunction@ladyboss.com';
        cnt.lastname = 'patel';
        insert cnt;
        
        Case cs = new Case();
        cs.Failed_Amount__c = 1;
        cs.contactId = cnt.id;
        cs.Subject = 'Payment for invoice';
        cs.IsFailed_Payment__c = true;
        insert cs;
        
        User u = new User(
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = pf.Id
            
        );
        insert u;
        
        Case_User__c cu = new Case_User__c();
        cu.UserId__c = u.Id;
        insert cu;
        
        
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        Con.Product__c = 'Unlimited';
        con.email ='test@gmail.com';
        insert Con;
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_E0w3qq3i41QeYa';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        
        Card__c Card =new Card__c();
        Card.Stripe_Card_Id__c = 'card_1DJgJfBwLSk1v1ohUY0sHXij';
        Card.Contact__c =Con.Id;
        insert Card ;
  /*      
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.Description = 'Payment for invoice 993D93F-0026';
        op.CustomerID__c = 'cus_E0w3qq3i41QeYa';
        op.Stripe_Charge_Id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        op.Contact__c = Con.Id;
        op.Stripe_Profile__c = StripePro.id;
        insert op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Stripe_charge_id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        ol.Subscription_Id__c = 'sub_EtMQI0bKEYfNCn';
        insert ol;
    */    
        WebHookSecrets__c wb = new WebHookSecrets__c();
        wb.name = 'Stripe_FailedChargeService';
        wb.Webhook_Name__c = 'Stripe_FailedChargeService';
        wb.Webhook_Secret__c = 'whsec_cIdDwVu0vpFqtbk13jL35lUQ8RhG2zHS';
        insert wb;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{"id":"evt_1Dl9xoFzCf73siP0QrWpTnaL","object":"event","api_version":"2018-02-28","created":1545721468,"data":{"object":{"id":"ch_1Dl9xnFzCf73siP0LyKD4qMk","object":"charge","billing_details": {"address": {"city": "Fort Morgan ","country": "United States of America","line1": "14979 Country Rd 17","line2": null,"postal_code": "80701","state": "Colorado "},"email": null,"name": "Annette Rusch ","phone": null},"amount":100,"amount_refunded":0,"application":null,"application_fee":null,"balance_transaction":null,"captured":false,"created":1545721467,"currency":"usd","customer":"cus_E0w3qq3i41QeYa","description":"Payment for invoice 993D93F-0026","destination":null,"dispute":null,"failure_code":"card_declined","failure_message":"Your card was declined.","fraud_details":{},"invoice":"in_1EPZhYFzCf73siP0F9K6XAL1","livemode":true,"metadata":{"Shipping Address":"Albuquerque 10010 Indian School Rd. NE New Mexico United States 87112","Full Name":"GrantRefundfunction GrantRefundfunction2","Email":"grant+grantrefundfunction@ladyboss.com","Phone":"234-234-2342","Sales Person":"Tirth Patel","Integration Initiated From":"Salesforce","One Time Products Charge":"YES","Salesforce Contact Link":"https://ladyboss.my.salesforce.com/003f400000kse8rAAA","Fulfillment Product Name":"TEST $1 ProductLABS|","Street":"10010 Indian School Rd. NE","City":"Albuquerque","State":"New Mexico","Postal Code":"87112","Country":"United States"},"on_behalf_of":null,"order":null,"outcome":{"network_status":"declined_by_network","reason":"do_not_honor","risk_level":"normal","risk_score":1,"seller_message":"The bank returned the decline code `do_not_honor`.","type":"issuer_declined"},"paid":false,"payment_intent":null,"receipt_email":null,"receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/charges/ch_1Dl9xnFzCf73siP0LyKD4qMk/refunds"},"review":null,"shipping":null,"source":{"id":"card_1DXT4SBwLSk1v1ohBcsQwM2T","object":"card","address_city":"Albuquerque","address_country":"United States","address_line1":"10010 Indian School Rd. NE","address_line1_check":"pass","address_line2":null,"address_state":"New Mexico","address_zip":"87112","address_zip_check":"pass","brand":"American Express","country":"US","customer":"cus_E0w3qq3i41QeYa","cvc_check":null,"dynamic_last4":null,"exp_month":7,"exp_year":2022,"fingerprint":"KfQdByKUuKuf6iEL","funding":"credit","last4":"1014","metadata":{},"name":"Grant test","tokenization_method":null},"source_transfer":null,"statement_descriptor":"TEST $1 ProductLABS","status":"failed","transfer_group":null}},"livemode":true,"pending_webhooks":6,"request":{"id":"req_d4rTiHNBHtbVW9","idempotency_key":null},"type":"charge.failed"}';
        req.requestBody = Blob.valueOf(body);
        req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        test.startTest();
        StripeFailedCharge_Service.upsertCase();
        
        test.stopTest();         
    }   
}