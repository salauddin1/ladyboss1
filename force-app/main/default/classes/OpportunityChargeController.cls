public without sharing class OpportunityChargeController {
    
    @AuraEnabled
    public static List<productWrapper> getOpportunity(String conID){
        List<productWrapper> pwList = new List<productWrapper>();
        List <Opportunity> OppList = new List <Opportunity>();
        List <Opportunity> newOppList = new List <Opportunity>();
        Set<Id> lineItemIdSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();
        Set<String> customSetProductNames = new Set<String>();
        List <Opportunity> subsOppList = new List <Opportunity>();
        List<Contact> conList = new List<Contact>();
        conList  = [Select id,Name,Enable_Product_Access_In_Hub__c from Contact where Id =: conID];
        List<OpportunityLineItem> newOppLineItemList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> oppSubsitemList = new List<OpportunityLineItem>();//OpportunityLineItem for first subscription Charge record
    List<OpportunityLineItem> oppitemList = new List<OpportunityLineItem>();//OpportunityLineItem for single opportunity
        List<Hub_Order_Page_Products__c> custHubList = new List<Hub_Order_Page_Products__c>();
        Map<String, Hub_Order_Page_Products__c> prodlinkMap = new Map<String,Hub_Order_Page_Products__c>();
        List <OpportunityLineItem> OppLineItemList = new List <OpportunityLineItem>();//OpportunityLineItem for All opportunity
        custHubList = [Select id,Name,Product_Name__c,Description_name__c,Url_Link__c from Hub_Order_Page_Products__c];
        for(Hub_Order_Page_Products__c hubVar : custHubList){
            prodlinkMap.put(hubVar.Name, hubVar);
            customSetProductNames.add(hubVar.Name);
        }
        
        //Subscription Opportunity Line Item List 
         oppSubsitemList = [SELECT id, Product2.Name,opportunityID,refund__c,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c != null AND Stripe_Charge_Id__c != null AND Stripe_Charge_Id__c != ''  Order BY CreatedDate DESC];
    //Map Used for opportunityLineItem with opportunityId
        Map<Id,List<OpportunityLineItem>> OpportunityLineItemMap = new Map<Id,List<OpportunityLineItem>>();
        oppLineItemList = [SELECT id, Product2.Name,opportunityID,refund__c,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Charge' AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null Order BY CreatedDate DESC];
        
        if(oppSubsitemList != null && !oppSubsitemList.isEmpty()) {
            for(OpportunityLineItem suboppList : oppSubsitemList) {
               oppLineItemList.add(suboppList); 
            }
        }
        for(OpportunityLineItem oppli : oppLineItemList) {
            lineItemIdSet.add(oppli.Id);
        }
        if(lineItemIdSet != null && !lineItemIdSet.isEmpty()){
        newOppLineItemList = [SELECT id,Product2.Name,opportunityID,refund__c,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE id =:lineItemIdSet AND Opportunity.Contact__c=:conID AND  Opportunity.Contact__c != null  Order BY CreatedDate DESC];
            }
        if(newOppLineItemList.size()>0){
            for (OpportunityLineItem oli: newOppLineItemList) {
                if(OpportunityLineItemMap.containskey(oli.opportunityID)){
                    OpportunityLineItemMap.get(oli.opportunityID).add(oli); 
                }
                else{
                    OpportunityLineItemMap.put(oli.opportunityID,new List<OpportunityLineItem>{oli});
                }
            }
        }
        
        //Susbscription Opportunity List
        subsOppList = [SELECT id,Name,Amount,Refund_Amount__c,CreatedDate,ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE RecordType.Name = 'Subscription' AND Contact__c=:conID AND Contact__c!=null Order BY CreatedDate DESC];
         
        OppList = [SELECT id, Name,Amount,CreatedDate,Refund_Amount__c,ShipStation_Orders__r.Tracking_Number__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE RecordType.Name = 'Charge' AND Contact__c=:conID AND Contact__c!=null Order BY CreatedDate DESC];
        
        if(subsOppList != null && !subsOppList.isEmpty()) {
            for(Opportunity subsOpp : subsOppList) {
                OppList.add(subsOpp);
            }
        }
        for(Opportunity oppVar : OppList) {
            oppIdSet.add(oppVar.id);
        }
        
        newOppList = [SELECT id, Name,Amount,Refund_Amount__c,CreatedDate,ShipStation_Orders__r.Tracking_Number__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE id =:oppIdSet  AND Contact__c=:conID AND Contact__c!=null Order BY CreatedDate DESC];
        if(newOppList.size()>0){
            for(Opportunity opp : newOppList){
                if(OpportunityLineItemMap.containskey(opp.id)){
                    system.debug('==============contains====================');
                    oppitemList = OpportunityLineItemMap.get(opp.id);
                    if(oppitemList.size()>0){
                        for (OpportunityLineItem oli: oppitemList) {
                            productWrapper pw = new productWrapper();
                            pw.Id = oli.id;
                            pw.totalPrice = oli.TotalPrice;
                            if (oli.refund__c != 0) {
                                pw.purchaseStatus = 'Refunded';
                                pw.refunded = oli.refund__c;
                            }else {
                                pw.purchaseStatus = '';
                                pw.refunded = oli.refund__c; 
                            }
                           if(customSetProductNames != null && !customSetProductNames.isEmpty() && customSetProductNames.contains(opp.Name)){
                                pw.ProductLinkName = opp.Name;
                                pw.Name = opp.Name;
                                pw.ProductLink = prodlinkMap.get(opp.Name).Url_Link__c;
                            }
                          /*  else if((opp.Name.contains('Monthly') && !opp.Name.contains('Coaching'))  || opp.Name.contains('Trainer') || opp.Name.contains('Unlimited')) {
                                pw.ProductLinkName = opp.Name;
                                pw.Name = opp.Name;
                                pw.purchaseDate = oli.CreatedDate.date();
                                String Monthlyvar = 'Monthly';
                                pw.ProductLink = prodlinkMap.get(Monthlyvar).Url_Link__c;
                            }*/
                            else if(customSetProductNames != null && !customSetProductNames.isEmpty() && !customSetProductNames.contains(opp.Name)){
                                pw.purchaseDate = oli.CreatedDate.date();
                                pw.Name = oli.Product2.Name;
                                pw.TrackingId = oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c;
                                if(custHubList != null && !custHubList.isEmpty()){
                                    for(Hub_Order_Page_Products__c hubVar : custHubList){
                                        if(oli.Product2.Name != null && hubVar.Url_Link__c != null && hubVar.Product_Name__c != null && ((oli.Product2.Category__c != null && hubVar.Product_Name__c == oli.Product2.Category__c) || (oli.Product2.Name != null && oli.Product2.Name.contains(hubVar.Product_Name__c)))){
                                            pw.ProductLinkName = oli.Product2.Name;
                                            pw.ProductLink = hubVar.Url_Link__c;
                                        }
                                        else if (hubVar.Url_Link__c != null&& oli.Product2.Name != null && hubVar.Description_name__c != null && oli.Opportunity.Description != null && hubVar.Description_name__c == oli.Opportunity.Description) {
                                            pw.ProductLink  = hubVar.Url_Link__c;
                                            pw.ProductLinkName = oli.Product2.Name;
                                        }
                                    }
                                }
                            }
                            
                            if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c != null && opp.Name != '14 Day Rainbow Detox' && opp.Name != '30 Day Metabolic Reset' && opp.Name != 'Booty Bootcamp' && opp.Name != 'LadyBoss Trainer' && opp.Name != 'LadyBoss UTA' && opp.Name != 'Unlimited Access') {
                                if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('ups')) {
                                    pw.TrackingURL = 'https://www.ups.com/track?loc=en_US&tracknum='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c+'&requester=NES&agreeTerms=yes/trackdetails';
                                }
                                else if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('USPS')) {
                                    pw.TrackingURL = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c;
                                }
                                else if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('FedEx')) {
                                    pw.TrackingURL = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c+'&cntry_code=us&locale=en_US';
                                }
                            }
                            pwList.add(pw);
                         }
                    }
                }
                else{
                    productWrapper pw = new productWrapper();
                    pw.Id = opp.id;
                    pw.Name = opp.Name;
                    pw.totalPrice = opp.Amount;
                    if (opp.Refund_Amount__c != 0) {
                        pw.purchaseStatus = 'Refunded';
                        pw.refunded = opp.Refund_Amount__c;
                    }else {
                        pw.purchaseStatus = '';
                        pw.refunded = opp.Refund_Amount__c; 
                    }
                    pw.TrackingId = opp.ShipStation_Orders__r.Tracking_Number__c;
                    if(opp.ShipStation_Orders__r.Carrier_Code__c != null && opp.Name != '14 Day Rainbow Detox' && opp.Name != '30 Day Metabolic Reset' && opp.Name != 'Booty Bootcamp' && opp.Name != 'LadyBoss Trainer' && opp.Name != 'LadyBoss UTA' && opp.Name != 'Unlimited Access') {
                        if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('ups')) {
                            pw.TrackingURL = 'https://www.ups.com/track?loc=en_US&tracknum='+opp.ShipStation_Orders__r.Tracking_Number__c+'&requester=NES&agreeTerms=yes/trackdetails';
                        }
                        else if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('USPS')) {
                            pw.TrackingURL = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels='+opp.ShipStation_Orders__r.Tracking_Number__c;
                        }
                        else if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('FedEx')) {
                            pw.TrackingURL = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='+opp.ShipStation_Orders__r.Tracking_Number__c+'&cntry_code=us&locale=en_US';
                        }
                    }
                    if(customSetProductNames != null && !customSetProductNames.isEmpty() && customSetProductNames.contains(opp.Name)){
                        pw.ProductLinkName = opp.Name;
                        pw.ProductLink = prodlinkMap.get(opp.Name).Url_Link__c;
                    }
                    else if((opp.Name.contains('Monthly') && !opp.Name.contains('Coaching')) || opp.Name.contains('Unlimited')) {
                                pw.ProductLinkName = opp.Name;
                                String Monthlyvar = 'Monthly';
                                pw.ProductLink = prodlinkMap.get(Monthlyvar).Url_Link__c;
                            }
                    if(customSetProductNames != null && !customSetProductNames.isEmpty() && !customSetProductNames.contains(opp.Name)){
                        pw.purchaseDate = opp.CreatedDate.date();
                    }
                    
                    pwList.add(pw);
                    
                }
            }
        }
        return pwList;
    }
    
    @AuraEnabled
    public static List<productWrapper> searchOpportunity(String conID, String searchText){
        searchText = '%'+searchText+'%';
        Set<productWrapper> pwList = new Set<productWrapper>();
        List <Opportunity> newOppList = new List <Opportunity>();
        Set<Id> lineItemIdSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();
        Set<String> customSetProductNames = new Set<String>();
        List<OpportunityLineItem> newOppLineItemList = new List<OpportunityLineItem>();
        List <Opportunity> OppList = new List <Opportunity>();
        List <Opportunity> subsOppList = new List <Opportunity>();
        Map<String, Hub_Order_Page_Products__c> prodlinkMap = new Map<String,Hub_Order_Page_Products__c>();
        List<Hub_Order_Page_Products__c> custHubList = new List<Hub_Order_Page_Products__c>();
        List<OpportunityLineItem> oppSubsitemList = new List<OpportunityLineItem>();//OpportunityLineItem for first subscription Charge record
        List<OpportunityLineItem> oppitemList = new List<OpportunityLineItem>();//OpportunityLineItem for single opportunity
        List <OpportunityLineItem> OppLineItemList = new List <OpportunityLineItem>();//OpportunityLineItem for All opportunity
        
        //Subscription Opportunity Line Item List 
         oppSubsitemList = [SELECT id, Product2.Name,opportunityID,refund__c,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c != null AND Stripe_Charge_Id__c != null AND Stripe_Charge_Id__c != ''  Order BY CreatedDate DESC];
    
        
        //Map Used for opportunityLineItem with opportunityId
        Map<Id,List<OpportunityLineItem>> OpportunityLineItemMap = new Map<Id,List<OpportunityLineItem>>();
        oppLineItemList = [SELECT id, Product2.Name,opportunityID,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Charge' AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND Product2.Name LIKE :searchText Order BY CreatedDate DESC];
        System.debug(oppLineItemList.size()+'========='+oppLineItemList);
        
        if(oppSubsitemList != null && !oppSubsitemList.isEmpty()) {
            for(OpportunityLineItem suboppList : oppSubsitemList) {
               oppLineItemList.add(suboppList); 
            }
        }
        for(OpportunityLineItem oppli : oppLineItemList) {
            lineItemIdSet.add(oppli.Id);
        }
        if(lineItemIdSet != null && !lineItemIdSet.isEmpty()){
        newOppLineItemList = [SELECT id,Product2.Name,opportunityID,refund__c,Product2.Category__c,Opportunity.ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,Opportunity.Description,Opportunity.ShipStation_Orders__r.Carrier_Code__c, TotalPrice,CreatedDate FROM OpportunityLineItem WHERE id =:lineItemIdSet AND Opportunity.Contact__c=:conID AND  Opportunity.Contact__c != null  Order BY CreatedDate DESC];
            }
        
        if(newOppLineItemList.size()>0){
            for (OpportunityLineItem oli: newOppLineItemList) {
                if(OpportunityLineItemMap.containskey(oli.opportunityID)){
                    OpportunityLineItemMap.get(oli.opportunityID).add(oli); 
                }else{
                    OpportunityLineItemMap.put(oli.opportunityID,new List<OpportunityLineItem>{oli});
                }
            }
        }
        custHubList = [Select id,Name,Product_Name__c,Description_name__c,Url_Link__c from Hub_Order_Page_Products__c];
        for(Hub_Order_Page_Products__c hubVar : custHubList){
            prodlinkMap.put(hubVar.Name, hubVar);
            customSetProductNames.add(hubVar.Name);
        }
        
        //Susbscription Opportunity List
        subsOppList = [SELECT id,Name,Amount,Refund_Amount__c,CreatedDate,ShipStation_Orders__r.Tracking_Number__c,Stripe_Charge_Id__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE RecordType.Name = 'Subscription' AND Contact__c=:conID AND Contact__c!=null Order BY CreatedDate DESC];
         
        OppList = [SELECT id, Name,Amount,CreatedDate,Refund_Amount__c,ShipStation_Orders__r.Tracking_Number__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE RecordType.Name = 'Charge' AND Contact__c=:conID AND Contact__c!=null AND Name LIKE : searchText Order BY CreatedDate DESC];        
        System.debug('===============OppList=========='+OppList.size()+'======='+OppList);
        
        if(subsOppList != null && !subsOppList.isEmpty()) {
            for(Opportunity subsOpp : subsOppList) {
                OppList.add(subsOpp);
            }
        }
        for(Opportunity oppVar : OppList) {
            oppIdSet.add(oppVar.id);
        }
        
        newOppList = [SELECT id, Name,Amount,Refund_Amount__c,CreatedDate,ShipStation_Orders__r.Tracking_Number__c,ShipStation_Orders__r.Carrier_Code__c FROM Opportunity WHERE id =:oppIdSet  AND Contact__c=:conID AND Contact__c!=null Order BY CreatedDate DESC];
        System.debug('---newOppList--'+newOppList+'----newOppList size--'+newOppList.size());
        if(newOppList.size()>0){
            for(Opportunity opp : OppList){
                if(OpportunityLineItemMap.containskey(opp.id)){
                    system.debug('==============contains======================');
                    oppitemList = OpportunityLineItemMap.get(opp.id);
                    if(oppitemList.size()>0){
                        for (OpportunityLineItem oli: oppitemList) {
                            productWrapper pw = new productWrapper();
                            pw.Id = oli.id;
                            pw.Name = oli.Product2.Name;
                            pw.totalPrice = oli.TotalPrice;
                            if (oli.refund__c != 0) {
                                pw.purchaseStatus = 'Refunded';
                                pw.refunded = oli.refund__c;
                            }else {
                                pw.purchaseStatus = '';
                                pw.refunded = oli.refund__c; 
                            }
                            if(customSetProductNames != null && !customSetProductNames.isEmpty() && customSetProductNames.contains(opp.Name)){
                                pw.ProductLinkName = opp.Name;
                                pw.ProductLink = prodlinkMap.get(opp.Name).Url_Link__c;
                            }
                            
                            if(customSetProductNames != null && !customSetProductNames.isEmpty() && !customSetProductNames.contains(opp.Name)){
                                pw.purchaseDate = oli.CreatedDate.date();
                                pw.TrackingId = oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c;
                                if(custHubList != null && !custHubList.isEmpty()){
                                    for(Hub_Order_Page_Products__c hubVar : custHubList){
                                        if(oli.Product2.Name != null && hubVar.Url_Link__c != null && hubVar.Product_Name__c != null && ((oli.Product2.Category__c != null && hubVar.Product_Name__c == oli.Product2.Category__c) || (oli.Product2.Name != null && oli.Product2.Name.contains(hubVar.Product_Name__c)))){
                                            pw.ProductLinkName = oli.Product2.Name;
                                            pw.ProductLink = hubVar.Url_Link__c;
                                        }
                                        else if (hubVar.Url_Link__c != null&& oli.Product2.Name != null && hubVar.Description_name__c != null && oli.Opportunity.Description != null && hubVar.Description_name__c == oli.Opportunity.Description) {
                                            pw.ProductLink  = hubVar.Url_Link__c;
                                            pw.ProductLinkName = oli.Product2.Name;
                                        }
                                    }
                                }
                            }
                            else if((opp.Name.contains('Monthly') && !opp.Name.contains('Coaching'))  || opp.Name.contains('Unlimited')) {
                                pw.ProductLinkName = opp.Name;
                                String Monthlyvar = 'Monthly';
                                pw.ProductLink = prodlinkMap.get(Monthlyvar).Url_Link__c;
                            }
                            if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c != null && opp.Name != '14 Day Rainbow Detox' && opp.Name != '30 Day Metabolic Reset' && opp.Name != 'Booty Bootcamp' && opp.Name != 'LadyBoss Trainer' && opp.Name != 'LadyBoss UTA' && opp.Name != 'Unlimited Access') {
                                if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('ups')) {
                                    pw.TrackingURL = 'https://www.ups.com/track?loc=en_US&tracknum='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c+'&requester=NES&agreeTerms=yes/trackdetails';
                                }
                                else if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('USPS')) {
                                    pw.TrackingURL = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c;
                                }
                                else if(oli.Opportunity.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('FedEx')) {
                                    pw.TrackingURL = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='+oli.Opportunity.ShipStation_Orders__r.Tracking_Number__c+'&cntry_code=us&locale=en_US';
                                }
                            }
                            pwList.add(pw);
                        }
                    }
                }
                else{
                    productWrapper pw = new productWrapper();
                    pw.Id = opp.id;
                    pw.Name = opp.Name;
                    pw.totalPrice = opp.Amount;
                    if (opp.Refund_Amount__c != 0) {
                        pw.purchaseStatus = 'Refunded';
                        pw.refunded = opp.Refund_Amount__c;
                    }else {
                        pw.purchaseStatus = '';
                        pw.refunded = opp.Refund_Amount__c; 
                    }
                    pw.TrackingId = opp.ShipStation_Orders__r.Tracking_Number__c;
                    if(opp.ShipStation_Orders__r.Carrier_Code__c != null && opp.Name != '14 Day Rainbow Detox' && opp.Name != '30 Day Metabolic Reset' && opp.Name != 'Booty Bootcamp' && opp.Name != 'LadyBoss Trainer' && opp.Name != 'LadyBoss UTA' && opp.Name != 'Unlimited Access') {
                        if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('ups')) {
                            pw.TrackingURL = 'https://www.ups.com/track?loc=en_US&tracknum='+opp.ShipStation_Orders__r.Tracking_Number__c+'&requester=NES&agreeTerms=yes/trackdetails';
                        }
                        else if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('USPS')) {
                            pw.TrackingURL = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels='+opp.ShipStation_Orders__r.Tracking_Number__c;
                        }
                        else if(opp.ShipStation_Orders__r.Carrier_Code__c.equalsIgnoreCase('FedEx')) {
                            pw.TrackingURL = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='+opp.ShipStation_Orders__r.Tracking_Number__c+'&cntry_code=us&locale=en_US';
                        }
                    }
                    if(customSetProductNames != null && !customSetProductNames.isEmpty() && !customSetProductNames.contains(opp.Name)){
                        pw.purchaseDate = opp.CreatedDate.date();
                    }
                     if(customSetProductNames != null && !customSetProductNames.isEmpty() && customSetProductNames.contains(opp.Name)){
                        pw.ProductLinkName = opp.Name;
                        pw.ProductLink = prodlinkMap.get(opp.Name).Url_Link__c;
                    }
                    else if((opp.Name.contains('Monthly') && !opp.Name.contains('Coaching')) || opp.Name.contains('Unlimited')) {
                                pw.ProductLinkName = opp.Name;
                                String Monthlyvar = 'Monthly';
                                pw.ProductLink = prodlinkMap.get(Monthlyvar).Url_Link__c;
                            }

                    pwList.add(pw);
                }
            }
        }
         System.debug('============================='+pwList.size()+'==='+pwList);   
        return new List<productWrapper>(pwList);
    }
    
    public class productWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String Id;
        @AuraEnabled public Decimal totalPrice;
        @AuraEnabled public Decimal refunded;
        @AuraEnabled public Date purchaseDate;
        @AuraEnabled public String TrackingId;
        @AuraEnabled public String TrackingURL;
        @AuraEnabled public String ProductLink;
        @AuraEnabled public String ProductLinkName;
        @AuraEnabled public String purchaseStatus;
    }
}