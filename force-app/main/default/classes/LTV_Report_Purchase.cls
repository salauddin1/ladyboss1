public class LTV_Report_Purchase {
 
    public Date Todat {get;set;}
    public String acctId {get;set;}
    public Decimal totAmount {get;set;}
    public Integer totCount {get;set;}
    List<opportunity> ar;
    
    public LTV_Report_Purchase (ApexPages.StandardController controller){
        
        //if((String)controller.getRecord().id!=null)
        // acctId =  (String)controller.getRecord().id;
        acctId   = apexpages.currentpage().getparameters().get('id'); 
        totAmount = 0;
        totCount = 0;
        Todat = Date.Today();
        
         ar = [select createddate,amount  from Opportunity
              where  Stripe_Charge_Id__c!=null  and Contact__c = : acctId ];
          
        Integer countOpp=0;
        for(Opportunity opp: ar ) {
            if(opp.amount!=null) {
            totAmount =totAmount +opp.amount;
            countOpp =countOpp+1;
            }
        }
        totCount =countOpp ;
        
        
    }
    
    
    
}