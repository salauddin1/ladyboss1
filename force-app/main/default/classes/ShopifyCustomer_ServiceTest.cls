@isTest
public class ShopifyCustomer_ServiceTest {
    @isTest
    public static void main(){
        Contact con = new Contact();
        con.LastName='TestLast';
        con.FirstName='TestFirst';
        con.Email='bob@biller.com';
        con.Phone='7067749234';
        insert con;
        Shopify_Orders__c ord = new Shopify_Orders__c();
        ord.Order_Id__c='450789469';
        ord.fulfillment_Status__c='fulfilled';
        ord.Refunded_Amount__c=123;
        ord.Total_Price__c=12;
        ord.Status__c='confirmed';
        ord.Contact__c=con.id;
        insert ord;
        ShopifyLineItem__c itm  = new ShopifyLineItem__c();
        itm.Contact__c=con.id;
        itm.Refunded_Amount__c=43;
        itm.Refunded_Tax_Amount__c=23;
        itm.Item_Id__c='518995019';
        itm.Shopify_Orders__c=ord.Id;
        insert itm;
        Shopify_Parameters__c tokens = new Shopify_Parameters__c ();
        tokens.Access_Token__c='1234redcre3edsx23wq3wed32w32wes32w';
        insert tokens;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new orderCalloutTest());
        String JSONBody = '{"id":706405506930370084,"email":"bob@biller.com","accepts_marketing":true,"created_at":null,"updated_at":null,"first_name":"Bob","last_name":"Biller","orders_count":0,"state":"disabled","total_spent":"0.00","last_order_id":null,"note":"This customer loves ice cream","verified_email":true,"multipass_identifier":null,"tax_exempt":false,"phone":null,"tags":"","last_order_name":null,"currency":"INR","addresses":[],"refunds": [{"id": 509562969,"order_id": 450789469,"refund_line_items": [{"id": 1058498307,"quantity": 1,"line_item_id": 518995019,"restock_type": "no_restock","subtotal": 195.67,"total_tax": 3.98,"line_item": {"id": 518995019}}],"transactions": [{"id": 1072844663,"order_id": 450789469,"kind": "refund","amount": "41.94"}]}]}';
        RestRequest request = new RestRequest();
        request.requestUri ='https://ashish-ladyboss-support.cs53.force.com/ShipStation/services/apexrest/ShopifyCustomerCreation';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONBody);
        RestContext.request = request;
        ShopifyCustomer_Service.orderCancel();
        Shopify_JSON2Apex.coverage();
        Test.stopTest();
        
        
    }
}