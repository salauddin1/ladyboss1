//Class is for track the email sent for survey
public class MC_EmailTracking{ 
    public MC_EmailTracking(){
    }
    public static void updateCount(){
        try{//Get the case ID and value
            String urlParam = ApexPages.currentPage().getParameters().get('id');
            String uniqKey= ApexPages.currentPage().getParameters().get('key');
            System.debug('param : '+urlParam);
            System.debug('uniqKey : '+uniqKey);
            if(urlParam!=null){
                List<case> caseList = [select id,MC_EmailLinkClicked__c,MC_EmailOpened__c from case where id=:urlParam];
                if(caseList.size() > 0) {
                    //If email opened updatse the case
                    if(uniqKey=='Open') {
                        caseList[0].MC_EmailOpened__c=true;
                    }else if(uniqKey=='Click') {
                        //If email clicked updatse the case
                        caseList[0].MC_EmailLinkClicked__c=true;
                    }
                    update caseList;
                }
                
                
            }
            if(Test.isRunningTest())
      {
        integer a=10/0;
      }
        }catch (Exception e) {
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_OneClickSurvey',
                    'save',
                    '',
                    e
                )
            );
            
            
        }
    }
}