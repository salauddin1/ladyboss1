@isTest
public class CaseCommentControllerTest {
	@isTest static void itShould(){
		Test.setMock(HttpCalloutMock.class, new FacebookAPIMock(200,'Ok','{"id": "dfdggdgdg"}',new  Map<String, String>()));
		FacebookPage__c fb = new FacebookPage__c();
		fb.isActive__c = true;
		fb.Last_Fetch_Time__c = Datetime.now();
		fb.Page_Id__c = 'testPageId';
		fb.Page_Token__c = 'token';
		fb.Page_Name__c = 'test name';
		insert fb;

		Case caseObject = new Case();
		caseObject.subject = 'Inbox Message from: ';
		caseObject.origin = 'facebook';
		caseObject.status = 'new';
		caseObject.FB_Page__c = fb.id;
		caseObject.Conversation_Id__c = 'ets';
		insert caseObject;

		ApexPages.StandardController sc = new ApexPages.StandardController(caseObject);
		CaseCommentController CaseCommentControllerObject = new CaseCommentController(sc);
		CaseCommentControllerObject.CommentBody = 'test body';

		PageReference pageRef = Page.CaseComment;
        pageRef.getParameters().put('id', String.valueOf(caseObject.Id));
        Test.setCurrentPage(pageRef);

		caseCommentControllerObject.getCaseCommmentsList();
		caseCommentControllerObject.saveComment();
	}

}