/** 

Developer Name:- Jay Patel
Email service class to sync inbound and outbound emails from gmail accounts of salesforce users with contacts in salesforce.
Emails are synced to contacts and are shown on activity feed of the contact in activity timeline.

*/
global class GmailToSalesforceHandler implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
	Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    system.debug(email);
    if (email.fromAddress.contains('noreply@google.com')) {
		//verifucation email sent from google when email is added in forwarding
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {'grant@ladyboss.com'};
		mail.setToAddresses(toAddresses);
		mail.setSenderDisplayName('Google Support');
		mail.setSubject('Verification code from google');
		mail.setPlainTextBody(email.plainTextBody);
		mail.setHtmlBody(email.htmlBody);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail });
    } else {
      List<User> usr = [SELECT id FROM User WHERE Email =:email.fromAddress];
      
      if (usr.size()>0) {
        //for outbound emails
        List<Contact> conList = [SELECT id FROM Contact WHERE Email IN:email.toAddresses Order by CreatedDate ASC];
        
        if (conList.size()>0) {
			List<EmailMessageRelation> emailmsgrelList = new List<EmailMessageRelation>();
			EmailMessage em = new EmailMessage();
			em.TextBody = email.plainTextBody;
			em.Subject = email.subject;
			em.Status = '3';
			insert em;
			EmailMessageRelation emr = new EmailMessageRelation();
			emr.EmailMessageId = em.id;
			emr.RelationId = conList[0].id;
			emr.RelationType = 'ToAddress';
			emailmsgrelList.add(emr);
			EmailMessageRelation emr1 = new EmailMessageRelation();
			emr1.EmailMessageId = em.id;
			emr1.RelationId = usr[0].id;
			emr1.RelationType = 'FromAddress';
			emailmsgrelList.add(emr1);
          insert emailmsgrelList;
        }
      }else { 
        //for inbound emails
        List<String> lstStrin = email.toAddresses;
        List<User> usrList = [SELECT id FROM User WHERE Email IN :email.toAddresses];
        List<Contact> conList = [SELECT id FROM Contact WHERE Email =:email.fromAddress Order by CreatedDate ASC];
        if (conList.size()>0 && usrList.size()>0) {
			List<EmailMessageRelation> emailmsgrelList = new List<EmailMessageRelation>();
			EmailMessage em = new EmailMessage();
			em.TextBody = email.plainTextBody;
			em.Subject = email.subject;
			em.Status = '2';
			insert em;
			EmailMessageRelation emr = new EmailMessageRelation();
			emr.EmailMessageId = em.id;
			emr.RelationId = conList[0].id;
			emr.RelationType = 'FromAddress';
			emailmsgrelList.add(emr);
			EmailMessageRelation emr1 = new EmailMessageRelation();
			emr1.EmailMessageId = em.id;
			emr1.RelationId = usrList[0].id;
			emr1.RelationType = 'ToAddress';
			emailmsgrelList.add(emr1);
			insert emailmsgrelList;
        }
      }
    }    
    result.success=true;
    return result;
  }
}