@isTest
public class UpdateTaskTest 
{
static testMethod void testStandardUserwithTask() 
{
    Profile standardProf = [select id from profile where name='Standard User'];
    User su = new User(alias = 'standt',         
    email='standarduser@azugatask.com',emailencodingkey='UTF-8',FirstName='task',lastname='testing',languagelocalekey='en_US',localesidkey='en_US',profileid = standardProf.Id,timezonesidkey='America/Los_Angeles',username='standarduser@azugatask.com');
    System.runAs(su) 
    {
        Account a = new Account(name='Testtask');
        insert a;
        contact con = new Contact();
        con.lastName = 'last';
        con.email = 'test@test.com';
        con.phone= '990990990';
        insert con;
        contact con1 = new Contact();
        con1.lastName = 'last';
        con1.email = 'test@test.com';
        con1.phone= '990990990';
        insert con1;
        contact con2 = new Contact();
        con2.lastName = 'last';
        con2.phone= '990990990';
        insert con2;
        List<Task> tasks = new List<Task>{};
            for(Integer i = 0; i < 200; i++) 
        {
            Task t = new Task(Subject='Donni'+i,Status='New',Priority='Normal',CallType='Outbound',WhoId=con2.id);
            tasks.add(t);
        }
        test.startTest();
        insert tasks;
        batchTaskUpdate b = new batchTaskUpdate ();
        Database.executeBatch(b);
        test.stopTest();
        
    }
}

static testMethod void testStandardUserwithTask1() 
{
    Profile standardProf = [select id from profile where name='Standard User'];
    User su = new User(alias = 'standt',         
    email='standarduser@azugatask.com',emailencodingkey='UTF-8',FirstName='task',lastname='testing',languagelocalekey='en_US',localesidkey='en_US',profileid = standardProf.Id,timezonesidkey='America/Los_Angeles',username='standarduser@azugatask.com');
    System.runAs(su) 
    {
        Account a = new Account(name='Testtask');
        insert a;
        contact con = new Contact();
        con.lastName = 'last';
        con.email = 'test@test.com';
        con.phone= '990990990';
        insert con;
        contact con1 = new Contact();
        con1.lastName = 'last';
        con1.email = 'test@test.com';
        con1.phone= '990990990';
        insert con1;
        List<Task> tasks = new List<Task>{};
            for(Integer i = 0; i < 200; i++) 
        {
            Task t = new Task(Subject='Donni'+i,Status='New',Priority='Normal',CallType='Outbound');
            tasks.add(t);
        }
        test.startTest();
        insert tasks;
        batchTaskUpdate b = new batchTaskUpdate ();
        Database.executeBatch(b);
        test.stopTest();
        
    }
}
     static testMethod void testTask() {
        
        
        Account a = new Account(name='Testtask');
        insert a;
        contact con = new Contact();
        con.lastName = 'last';
        con.email = 'test@test.com';
        con.phone= '990990990';
        insert con;
        contact con1 = new Contact();
        con1.lastName = 'last';
        con1.email = 'test@test.com';
        con1.phone= '990990990';
        insert con1;
        Task t = new Task(Subject='Donni',Status='New',Priority='Normal',CallType='Outbound',WhoId=con.id,Stop_Correspondence__c=true);
        insert t;
        t.Stop_Correspondence__c=false;
        update t;
    }
    static testMethod void testTask1() {
        
        
        Account a = new Account(name='Testtask');
        insert a;
        contact con = new Contact();
        con.lastName = 'last';
        con.email = 'test@test.com';
        con.phone= '990990990';
        insert con;
        contact con1 = new Contact();
        con1.lastName = 'last';
        con1.email = 'test@test.com';
        con1.phone= '990990990';
        insert con1;
        Task t = new Task(Subject='Donni',Status='New',Priority='Normal',CallType='Outbound',WhoId=con.id,Stop_Correspondence__c=false);
        insert t;
        t.Stop_Correspondence__c=true;
        update t;
    }

}