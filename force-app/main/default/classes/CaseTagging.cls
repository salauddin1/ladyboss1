//This class used with the component to tag the case
public class CaseTagging {
//Ths method gets the case record which is passed from the comp
 @AuraEnabled
    public static case getCaseRecord(Id caseId){
        List<case> subsequentList = [select id,Status,Cancel__c,Recovered__c,Not_recovered__c,Transfer__c,ContactId,Upsold__c,No_Response__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c from Case Where id=:caseId];
        return(subsequentList[0]);
    }  
    //Method to update the case status based on the check box clicked on the component
    @AuraEnabled public static case saveCase(case cas,String AddRemoveDays,String fieldNameForDays){
        
        if(fieldNameForDays == 'Transfer__c' && AddRemoveDays == 'true'){
           cas.status='Open';
        }
         if(fieldNameForDays == 'Cancel__c' && AddRemoveDays == 'true'){
           cas.status='Closed';
        }
         if(fieldNameForDays == 'Recovered__c' && AddRemoveDays == 'true'){
            cas.status='Closed';           
        }
         if(fieldNameForDays == 'Not_recovered__c' && AddRemoveDays == 'true'){
           cas.status='Closed';
        }
         if(fieldNameForDays == 'Upsold__c' && AddRemoveDays == 'true'){
           cas.status='Closed';
        }
         if(fieldNameForDays == 'No_Response__c' && AddRemoveDays == 'true'){
           cas.status='Closed';
        }
        //Update the case object
        update cas;
        
        return cas;
    }  
}