public class ContactSMSUtility{
    public string toNumber{get;set;}
    public string fromNumber{get;set;}
    public String lastReply {get;set;}
    public String phone {get;set;}
    public String contName {get;set;}
    public string senderName{get;set;}
    public string ContactNumber{get;set;}
    public string messageData{get;set;}
    public List<Event> smsUtil {get;set;}
    public List<Event> showSMSList{get;set;}
    public String smsId{get;set;} //capture value from assignTo
    Public Integer newSMSCount{get;set;}
    Public Boolean newSMSBool{get;set;}
    Public Boolean isListSMS{get;set;}
    Public Boolean isFullCon{get;set;}
    Public Boolean showTable{get;set;}
    Public Boolean actionstatusrendered{get;set;}
    public List<Event> Util = new List<Event>();
   
    public ContactSMSUtility(){
        back();
    }
    public void sendSMS() {
        //=====================Show Up Success/Failure message on vf page
        Map<String,Object> mapData = sendSMSNew();
        if(mapData.get('success') != null && (Boolean)mapData.get('success')) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,(String)mapData.get('message')));
        }else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,(String)mapData.get('message')));
        }
        toNumber = ContactNumber;
    }
    public void showNumber() {
        actionstatusrendered = true;
        isFullCon =true;
        isListSMS =false;
        String Phform1 = '';
        String Phform2 = '';
        String Phform3 = '';
        List<Event> showSMSListV1 =new List<Event>();
        showSMSList =new List<Event>();
        System.debug('======numberToShow=========='+smsId);
        Phform3 = smsId;
        smsId = smsId.trim();
        //============To show selected number and its last reply in Text Box===================================
        if(smsId.length() == 11)
        {
            Phform1 = smsId.removeStart('1');
            Phform2 = '+1'+Phform1;
        }
        if(smsId.length() == 10) {
            Phform1 = smsId;
            Phform2 = '+1'+Phform1;
        }
        if(smsId.length() == 12 && smsId.contains('+1')) {
            Phform1 = smsId.replace('+1', '');
            Phform2 = smsId;
        }
        if(smsId.length() == 12 && smsId.contains('(') && smsId.contains(')')) {
            smsId = smsId.replace('(', '');
            smsId = smsId.replace(')', '');
            smsId = smsId.replace('-', '');
            Phform1 = smsId;
            Phform2 = '+1'+Phform1;
        }
          list<event> showSMSListV2 = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c,Phone__c,senddate__c,Sent_By__r.name,Contact__r.Name, Sent_By__c, Description,CreatedDate , Id FROM Event where (Phone__c=:Phform1 OR Phone__c=:Phform2 OR Phone__c=:Phform3) AND Sent_By__c =:userinfo.getuserId() Limit 100 FOR UPDATE];
        for(Event num1 : showSMSListV2 ) {
            if(num1.isNew__c==true){
                num1.isNew__c = false;
            }
        }
        update showSMSListV2;
        System.debug('=====Phform1======'+Phform1+'==========Phform2========='+Phform2+'==========Phform3========='+Phform3);
        showSMSListV1 = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c,Phone__c,senddate__c,Sent_By__r.name,Contact__r.Name, Sent_By__c, Description,CreatedDate , Id FROM Event where (Phone__c=:Phform1 OR Phone__c=:Phform2 OR Phone__c=:Phform3) AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate ASC Limit 101];
        ContactNumber = Phform2;
        for(Event num : showSMSListV1 ) {
            toNumber = num.Phone__c;
            //ContactNumber = toNumber;
            lastReply = num.Description;
            if(num.isNew__c==true){
                num.isNew__c = false;
            }
            showSMSList.add(num);
            if(num.Contact__r.Name != null) {
                senderName = num.Contact__r.Name;
            }
            
        } 
        Map<String,Event> lstMap = new Map<String,Event> ();
        smsUtil = new List<Event>();
        newSMSCount = 0;
        //========Count new message for show notification to incoming message======================
        List<Event> lst = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c,Phone__c,Sent_By__r.name, Contact__r.Name,Sent_By__c, Description,CreatedDate , Id FROM Event where Phone__c!=null AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate Desc Limit 101];
        for(Event s : lst){
            if(s.isNew__c==true){
                newSMSCount=newSMSCount+1;
            }
            if(newSMSCount == 0) {
                newSMSBool =false;
            } 
            if(!lstMap.containsKey(s.Phone__c)){
                lstMap.put(s.Phone__c,s);
            }
            
        }
        for(string str : lstMap.keySet()){
            smsUtil.add(lstMap.get(str));
        }
        
        System.debug('--------toNumber--------'+toNumber);
        smsId = '';
        
        
    }
    
    public Map<String,Object> sendSMSNew(){
        Map<String,Object> mapOutput = new Map<String,Object>();
        try{
            USER currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Id=:userinfo.getuserId() limit 1];
            System.debug('===='+currentuser.Twillio_From_phone__c);
            if(currentuser != null && currentuser.Twillio_From_phone__c != null) {
                fromNumber = String.valueOf(currentuser.Twillio_From_phone__c);
            }
            String str1 =messageData ; 
            
            //=========Send SMS to the customer from twilio webservice=====================
            
            List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain' ];
            String accountSID = lstTwilliConfigs.get(0).AccountSID__c; 
            String authToken = lstTwilliConfigs.get(0).AuthToken__c;
            if(fromNumber ==null){
                mapOutput.put('success',FALSE);
                mapOutput.put('message','SMS send unsuccsuful');
                return mapOutput;
            }
            
            if(toNumber.contains('+1')){
                toNumber = toNumber.trim();
                toNumber = toNumber.replaceAll('\\D','');
            }
            else {
                toNumber = toNumber.trim();
                toNumber = toNumber.replaceAll('\\D','');
            }
            ContactNumber = toNumber;
            String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
            System.debug(str1 );
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            // =============appending +1 with the number if it is not already append============
            if(toNumber.contains('+1')){
                req.setBody('To='+ EncodingUtil.urlEncode(toNumber ,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8'));
            }
            else {
                req.setBody('To='+ EncodingUtil.urlEncode('+1'+toNumber ,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8')); 
            }
            Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
            String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            Http http = new Http();
            TwillioResponse tl;
            System.debug('tl==' );
            if(!test.isrunningTest() && toNumber !=''){
                HTTPResponse res = http.send(req);
                tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
                if(tl.sid != null) {
                    mapOutput.put('success',TRUE);
                    mapOutput.put('message','SMS send succsufully');
                    // return 'SMS Send Successful';
                    String Phform1 = '';
                    String Phform2 = '';
                    toNumber = toNumber.trim();
                    if(toNumber.length() == 11)
                    {
                        Phform1 = toNumber.removeStart('1');
                        Phform2 = '+1'+Phform1;
                    }
                    if(toNumber.length() == 10) {
                        Phform1 = toNumber;
                        Phform2 = '+1'+Phform1;
                    }
                    if(toNumber.length() == 12 && toNumber.contains('+1')) {
                        Phform1 = toNumber.replace('+1', '');
                        Phform2 = toNumber;
                    }
                    if(toNumber.length() == 12 && toNumber.contains('(') && toNumber.contains(')')) {
                        toNumber = toNumber.replace('(', '');
                        toNumber = toNumber.replace(')', '');
                        toNumber = toNumber.replace('-', '');
                        Phform1 = toNumber;
                        Phform2 = '+1'+Phform1;
                    }
                    List<Contact> cont = new List <Contact>();
                    if(Phform1 != null) {
                      cont = [Select id,Phone,MobilePhone,OtherPhone from Contact where Phone =: Phform1 or MobilePhone =: Phform1  or OtherPhone =: Phform1 Limit 1];
                    }
                    if(Phform1 != null && cont.isEmpty()) {
                        cont = [Select id,Phone,MobilePhone,OtherPhone from Contact where Phone =: Phform2 or MobilePhone =: Phform2  or OtherPhone =: Phform2 Limit 1];    
                    }
                    System.debug('========contactrecord==========='+cont);
                    Event smsu = new Event();
                    if(cont != null && !cont.isEmpty()) {
                        for(Contact con : cont) {
                            smsu  = new Event(DurationInMinutes = 1, ActivityDateTime = System.now(), Sent_By__c =UserInfo.getUserId(),Description = messageData,phone__c = Phform2,senddate__c = Date.Today(),From_Phone_Number__c=fromNumber,SMS_Initiated_By__c='Agent',Contact__c = con.Id );
                        }
                    }
                    else {
                        smsu  = new Event(DurationInMinutes = 1, ActivityDateTime = System.now(), Sent_By__c =UserInfo.getUserId(),Description = messageData,phone__c = Phform2,senddate__c = Date.Today(),From_Phone_Number__c=fromNumber,SMS_Initiated_By__c='Agent');
                    }
                    insert smsu;
                    System.debug('========smsusmsusmsu=========='+smsu);
                    ContactNumber = Phform2;
                    toNumber = '';
                    messageData = '';
                    
                }else {
                    mapOutput.put('success',FALSE);
                    mapOutput.put('message','SMS sending fail,Error from Twillio is : '+tl.message);
                }
            }
            if(test.isRunningTest()){
                mapOutput.put('success',true);
                mapOutput.put('message','SMS send succsufully');
                Integer i=10/0;
            }
            return mapOutput;
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'ContactSMSUtility',
                    'sendSMS',
                    '',
                    e
                )
            ); 
            if(test.isRunningTest()){
                mapOutput.put('success',FALSE);
                mapOutput.put('message','SMS send succsufully');
                
            }
            return mapOutput;
        }
    } 
    
    //return 'SMS Not sent!!';
    public class TwillioResponse {
        public String sid; 
        public String message;
    }
    //==================Reload partially for show new SMS on Vf page=========
    public void reload(){
        if(isFullCon == true) {
            isFullCon =true;
        }
        else if(isFullCon == false) {
            isFullCon =false;
        }
        else if(isListSMS == true) {
            isListSMS =true;
        }
        else {
            isListSMS =false;
        }
        List<Event> lst = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c, CreatedDate, Phone__c, Sent_By__c, Contact__r.Name,Description, Id FROM Event where Phone__c!=null AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate DESC Limit 100];
        USER currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Id=:userinfo.getuserId() limit 1];
        System.debug('===='+currentuser.Twillio_From_phone__c);
        if(currentuser != null && currentuser.Twillio_From_phone__c != null) {
            fromNumber = String.valueOf(currentuser.Twillio_From_phone__c);
        }
        newSMSCount = 0;
        Map<String,Event> lstMap = new Map<String,Event> ();
        smsUtil = new List<Event>();
        system.debug(lst);
        
        for(Event s : lst){
            if(s.isNew__c==true){
                newSMSCount=newSMSCount+1;
                displayPopup = true;
            }
            System.debug('==========newSMSCount============='+newSMSCount);
            String str1 = s.Phone__c;
            contName = s.Contact__r.Name;
            if(str1.contains('+1')){
                str1 = str1.replace('+1', '');
            }
            str1 = str1.trim();
            str1 = str1.replace('(', '');
            str1 = str1.replace(')', '');
            str1 = str1.replace('-', '');
            if(!lstMap.containsKey(str1)){
                lstMap.put(str1,s);
            }
        }
        for(string str : lstMap.keySet()){
            smsUtil.add(lstMap.get(str));
        }
        system.debug(smsUtil);
        if(newSMSCount>0) {
            newSMSBool =true;
            displayPopup = true;
        }
        String secondform = '';
        List<Event> showSMSListV1 =new List<Event>();
        showSMSList =new List<Event>();
        System.debug('======ContactNumberContactNumber=========='+ContactNumber);
        if(ContactNumber != null && ContactNumber != '') {
            if(ContactNumber.contains('+1')) {
                secondform = ContactNumber.replace('+1', '');
            }
            if(!ContactNumber.contains('+1')) {
                secondform = '+1'+ContactNumber;
            }
        }
        showSMSListV1 = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c,Phone__c,senddate__c,Sent_By__r.name,Contact__r.Name, Sent_By__c, Description,CreatedDate , Id FROM Event where Phone__c!= null AND (Phone__c=:ContactNumber OR Phone__c =: secondform) AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate ASC Limit 100];
        
        System.debug('--------show number--------'+showSMSList);
        for(Event num : showSMSListV1 ) {
            toNumber = num.Phone__c;
            ContactNumber = toNumber;
            lastReply = num.Description;
            if(num.isNew__c==true){
                displayPopup = true;
                num.isNew__c = false;
            }
            showSMSList.add(num);
            if(num.Contact__r.Name != null) {
                senderName = num.Contact__r.Name;
                System.debug('--------senderName------------'+senderName);
            }
            System.debug('--------showSMSList------------'+showSMSList);
            //update showSMSList;
        }
    }
    // ======================= this method is for back button ================================
    public void back(){
        isFullCon =false;
        isListSMS =true;
        senderName= '';
        List<Event> lst = [SELECT SMS_Initiated_By__c, isNew__c,From_Phone_Number__c, CreatedDate, Phone__c, Sent_By__c, Contact__r.Name,Description, Id FROM Event where Phone__c!=null AND Sent_By__c =:userinfo.getuserId() Order By CreatedDate DESC Limit 100];
        USER currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Id=:userinfo.getuserId() limit 1];
        System.debug('===='+currentuser.Twillio_From_phone__c);
        if(currentuser != null && currentuser.Twillio_From_phone__c != null) {
            fromNumber = String.valueOf(currentuser.Twillio_From_phone__c);
        }
        newSMSCount = 0;
        Map<String,Event> lstMap = new Map<String,Event> ();
        smsUtil = new List<Event>();
        for(Event s : lst){
            if(s.isNew__c==true){
                newSMSCount=newSMSCount+1;
                displayPopup = true;
            }
            String str1 = s.Phone__c;
            contName = s.Contact__r.Name;
            if(str1.contains('+1')){
                str1 = str1.replace('+1', '');
            }
            str1 = str1.trim();
            str1 = str1.replace('(', '');
            str1 = str1.replace(')', '');
            str1 = str1.replace('-', '');
            if(!lstMap.containsKey(str1)){
                lstMap.put(str1,s);
            }
            
        }
        for(string str : lstMap.keySet()){
            smsUtil.add(lstMap.get(str));
        }
        system.debug(smsUtil);
        if(newSMSCount>0) {
            displayPopup = true;
            newSMSBool =true;
        }
        toNumber ='';
    }
    public boolean displayPopup {get; set;}
    
    public void closePopup() {
        displayPopup = false;
    }
    
    public void showPopup() {
        displayPopup = true;
    }
}