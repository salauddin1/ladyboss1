global class getAllPlansFromStripe {
	private static  List<Object> datas =new List<Object>() ;
	global static void getplans(){
		List<Object> objectList = getPlans('FirstTime');
		List<Product2> productToUpdate = new List<Product2>();
            List<PricebookEntry> pricebookToInsert = new List<PricebookEntry>();
		Map<String, Object> obmap1=new Map<String, Object>();
		List<String> planIds = new List<String>();
		Map<String, Map<String, Object>> planMap=new Map<String, Map<String, Object>>();
		for (Object ob: objectList) {
			obmap1=(Map<String, Object>)ob;
			String planID = String.valueOf(obmap1.get('id'));
			planMap.put(planID,obmap1);            
		}
		planIds.addAll(planMap.keySet());
		List<product2> prods= [SELECT id,Name,Price__c,Stripe_plan_Id__c FROM Product2 WHERE Stripe_plan_Id__c IN :planIDs];          
        List<PriceBookEntry> priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_plan_Id__c FROM PriceBookEntry WHERE Product2Id in :prods AND PriceBook2.isStandard=true];
        Map<String,PriceBookEntry> priceBookMap = new Map<String,PriceBookEntry>();
        Map<String,product2> prodMap = new Map<String,product2>();
        for(PriceBookEntry pbookEntry:priceBookList){
            priceBookMap.put(pbookEntry.Product2.Stripe_plan_Id__c,pbookEntry);
        }
        for(product2 prod:prods){
            prodMap.put(prod.Stripe_plan_Id__c,prod);
        }
        Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];
        for (String planID: planIds) {
            if (!prodMap.containsKey(planID)) {
                Product2 product = new Product2();
                if (planMap.get(planID).get('nickname')!=null) {
                	product.Name = String.valueOf(planMap.get(planID).get('nickname'));
                }else {
                	StripeCreateProduct scp = StripeCreateProduct.getProduct(String.valueOf(planMap.get(planID).get('product')));
                	product.Name = scp.name;
                }									
                product.Price__c = Decimal.valueOf(String.valueOf(planMap.get(planID).get('amount')))/100;
                product.Stripe_Plan_Id__c = planID;
                if (!product.Name.contains('Test')) {
                	productToUpdate.add(product);
                }
                
                system.debug('Anydatatype_msg'+productToUpdate);
            }
        }
        if (productToUpdate.size()>0) {
            insert productToUpdate;
        }
        for (Product2 pr: productToUpdate) {
            PricebookEntry pbe = new PricebookEntry (Pricebook2Id=pb.id, Product2Id=pr.id, IsActive=true, UnitPrice=pr.Price__c);
            pricebookToInsert.add(pbe);				
        }
        if (pricebookToInsert.size()>0) {
            insert pricebookToInsert;
        }
          
		
	}
	global static List<Object> getPlans(String startAfter) {
        HttpRequest http = new HttpRequest();
        if (startAfter == 'FirstTime') {
            http.setEndpoint('https://api.stripe.com/v1/plans?limit=100');
        } else {
            http.setEndpoint('https://api.stripe.com/v1/plans?limit=100&starting_after=' + startAfter);
        }
        
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{ "object": "list", "data": [ { "id": "plan_DFphhy99UBrdvs", "object": "plan", "active": true, "aggregate_usage": null, "amount": 12700, "billing_scheme": "per_unit", "created": 1531937149, "currency": "usd", "interval": "week", "interval_count": 2, "livemode": true, "metadata": {}, "nickname": "UTA 3 Pay-Salesforce", "product": "prod_DFpgbRwyyH7yA5", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, { "id": "SF-COACHING12PaymentsSubscription", "object": "plan", "active": true, "aggregate_usage": null, "amount": 29700, "billing_scheme": "per_unit", "created": 1531249898, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Coaching 12 Monthly Payments-Salesforce Order", "product": "prod_DCqwWnuTgIcWe1", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, { "id": "FFFRsRs", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19700, "billing_scheme": "per_unit", "created": 1531175544, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "FUEL-FUEL-FUEL-REST-REST", "product": "prod_DCWvNSeSfuS83m", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" } ], "has_more": false, "url": "/v1/plans" }');
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response );
        system.debug('#### '+ results);
		List<Object> data = (List<Object>)results.get('data');
		system.debug('1111 '+ data);
        datas.addAll(data);
        Boolean hasMore = Boolean.valueOf(results.get('has_more'));
        if (hasMore == true) {
        	Map<String, Object> sObjectMap = (Map<String,Object>)data[data.size()-1];
        	getplans(String.valueOf(sObjectMap.get('id')));
        }
        	return datas;
        
        
        
    }
}