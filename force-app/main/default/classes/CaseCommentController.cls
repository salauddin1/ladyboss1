public class CaseCommentController {

	public String commentBody{get;set;}
    ApexPages.StandardController stdController{get;set;}
    public List<CaseComment> caseCommentList{get;set;}

    public CaseCommentController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        //getCaseCommmentsList();
    }

    public void getCaseCommmentsList(){
        caseCommentList = [SELECT id,CommentBody FROM CaseComment Where ParentId =:stdController.getId()];
    }

    public PageReference saveComment(){
        System.debug('stdController.getId() : '+stdController.getId());
        List<CaseComment> commentList = new List<CaseComment>();
        Integer bodyLength = commentBody.length();
        for(Integer i=0 ; i<= bodyLength/4000 ; i++){
        	CaseComment caseComment =  new CaseComment(ParentId = stdController.getId());
            if(bodyLength/4000==i){
                caseComment.CommentBody = commentBody.substring(4000*i,4000*i+math.mod(bodyLength, 4000));
            }else{
        		caseComment.CommentBody = commentBody.substring(4000*i,4000*(i+1));
            }
            commentList.add(caseComment);
            if(math.mod(bodyLength, 4000) == 0 &&  ( bodyLength/4000 - 1) == i ){
                break;
            }
        }
        insert commentList;
        return null;
    }
}