@IsTest
public class StripePlanProductTrigger_Test {   
	static testMethod void test() {
        Product2 pro = new Product2(Name='Test Product',Currency__c='usd',Interval__c='month',Price__c=20,Type__c='Stripe');
        
        Test.startTest();
        insert pro;
        Test.stopTest();
    }
    
    static testMethod void test2() {
        Product2 pro = new Product2(Name='Test Product',Currency__c='usd',Interval__c='month',Price__c=20,Type__c='Stripe');
        insert pro;
        Test.startTest();
        	update pro;
        Test.stopTest();
    }
    
    static testMethod void test3() {
        Product2 pro = new Product2(Name='Test Product',Currency__c='usd',Interval__c='month',Price__c=20,Type__c='Stripe',External_ID__c='123456');
        insert pro;
        
        Test.startTest();
        	pro.Name = 'Test Product 1';
        	update pro;
        Test.stopTest();
    }
}