public class RefundAffiliateCommissionHandler {
    @future(callout=true)
    public static void updateAffiliateCommission(Set<String> papCommIdSet){
        try{
            Set<Id> conIdSet = new Set<Id>();
            List<Pap_Commission__c> commissionList = new List<Pap_Commission__c>();
            commissionList = [select id, Contact__c, Total_Cost__c, data1__c, data2__c, data4__c, product_id__c, order_id__c, Affiliate_ref_id__c, Opportunity__c, Commission__c, Commission_Type__c, Campaign_Id__c, Campaign_name__c from Pap_Commission__c where id =: papCommIdSet and Contact__c != null and Contact__r.PAPUserId__c != null and (Commission_type__c =: 'refund' or Commission_type__c =: 'repeated')];
            if(commissionList.size() > 0){    
                for(Pap_Commission__c comm : commissionList){
                    if(comm.Contact__c != null){
                        conIdSet.add(comm.Contact__c);
                    }
                }
            }
            List<Contact> conList = new List<Contact>();
            Map<String, Contact> conMap = new Map<String, Contact>();
            
            if(conIdSet.size() > 0){
                conMap = new Map<String, Contact>([SELECT id,Email,FirstName,LastName,PAP_refid__c,PAPRPassword__c,PAPUserId__c FROM Contact WHERE id=:conIdSet ]);
            }
            if(commissionList.size() > 0 && conMap != null && conMap.keySet().size() > 0){    
                pap_credentials__c pap_url = [select id, name, pap_url__c, username__c, password__c, X3FF_Commission_Type_Id__c, X3FF_Renewals_Campaign_Id__c from pap_credentials__c where Name='pap url' limit 1];
                if(pap_url != null && pap_url.username__c != null && pap_url.username__c != '' && pap_url.password__c != null && pap_url.password__c != '' && pap_url.pap_url__c != null && pap_url.pap_url__c != ''  ){
                    Http http = new Http();
                    HttpRequest ht = new HttpRequest();
                    ht.setEndpoint(pap_url.pap_url__c);
                    ht.setMethod('POST');
                    ht.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                    String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]}';
                    Boolean sendReq = false ;
                    for(Pap_Commission__c commission : commissionList){
                        if(commission.Contact__c != null &&  commission.Commission__c != null){
                            if(commission != null && commission.Contact__c != null){   
                                if(conMap.keySet().contains(commission.Contact__c)){
                                    Contact ct = conMap.get(commission.Contact__c);
                                    if(commission.Total_Cost__c == null){
                                        commission.Total_Cost__c = 0;
                                    }
                                    if(commission.Commission_type__c == 'refund' ){
                                        body =  body + ',{"C":"Pap_Merchants_Transaction_TransactionsForm", "M":"add", "fields":[["name","value"],["Id",""],["transid",""],["rstatus","A"],["dateinserted","'+System.now()+'"],["totalcost","'+commission.Total_Cost__c+'"],["channel",""],["fixedcost",""],["multiTier","N"],["sendNotification","N"],["commtypeid","R"],["bannerid",""],["payoutstatus","U"],["countrycode",""],["userid","'+ct.PAPUserId__c+'"],["campaignid","'+commission.Campaign_Id__c+'"],["parenttransid",""],["commission","'+commission.Commission__c+'"],["tier","1"],["split","1"],["commissionTag","Commissions are computed automatically"],["orderid","'+commission.order_id__c+'"],["productid","'+commission.product_id__c+'"],["data1","'+commission.Data1__c+'"],["data2","'+commission.Data2__c+'"],["data3",""],["data4","'+commission.Data4__c+'"],["data5",""],["trackmethod","M"],["refererurl",""],["ip",""],["browser",""],["firstclicktime",""],["firstclickreferer",""],["firstclickip",""],["firstclickdata1",""],["firstclickdata2",""],["lastclicktime",""],["lastclickreferer",""],["lastclickip",""],["lastclickdata1",""],["lastclickdata2",""],["systemnote",""],["merchantnote",""]]}';
                                        sendReq = true ;
                                    }
                                    else if(commission.Commission_type__c == 'repeated' && pap_url.X3FF_Commission_Type_Id__c != null && pap_url.X3FF_Renewals_Campaign_Id__c != null){
                                        body =  body + ',{"C":"Pap_Merchants_Transaction_TransactionsForm", "M":"add", "fields":[["name","value"],["Id",""],["transid",""],["rstatus","A"],["dateinserted","'+System.now()+'"],["totalcost","'+commission.Total_Cost__c+'"],["channel",""],["fixedcost",""],["multiTier","N"],["sendNotification","N"],["commtypeid","'+pap_url.X3FF_Commission_Type_Id__c+'"],["bannerid",""],["payoutstatus","U"],["countrycode",""],["userid","'+ct.PAPUserId__c+'"],["campaignid","'+pap_url.X3FF_Renewals_Campaign_Id__c+'"],["parenttransid",""],["commission","'+commission.Commission__c+'"],["tier","1"],["split","1"],["commissionTag","Commissions are computed automatically"],["orderid","'+commission.order_id__c+'"],["productid","'+commission.product_id__c+'"],["data1","'+commission.Data1__c+'"],["data2","'+commission.Data2__c+'"],["data3",""],["data4","'+commission.Data4__c+'"],["data5",""],["trackmethod","M"],["refererurl",""],["ip",""],["browser",""],["firstclicktime",""],["firstclickreferer",""],["firstclickip",""],["firstclickdata1",""],["firstclickdata2",""],["lastclicktime",""],["lastclickreferer",""],["lastclickip",""],["lastclickdata1",""],["lastclickdata2",""],["systemnote",""],["merchantnote",""]]}';
                                    	sendReq = true ;
                                    }
                                    
                                }
                            }
                        }
                    }
                    body= body + ']}';
                    System.debug('====body=='+body);
                    ht.setBody(body);
                    if(sendReq){
                        HttpResponse res = http.send(ht);
                        System.debug('res : '+res.getBody());
                    }
                }            
            }
        }
        catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog(); 
            apex.createLog(new ApexDebugLog.Error('RefundAffiliateCommissionHandler','updateAffiliateCommission',String.valueOf(papCommIdSet),ex));
            
        }
    }
}