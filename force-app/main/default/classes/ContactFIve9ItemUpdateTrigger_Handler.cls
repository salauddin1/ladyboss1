public class ContactFIve9ItemUpdateTrigger_Handler {
    Public Static void doAfterInsert(List<Contact> contactList){
            Map<Id,Contact> contactMap = new Map<Id,Contact>();
            Map<Id,Contact> cContactMap = new Map<Id,Contact>();
            for(Contact con : contactList){
                if(con.Product__c != null){
                    contactMap.put(con.Id, con);
                }else if(con.Product__c == null && con.Potential_Buyer__c != null ){
                    cContactMap.put(con.Id,con);
                }
            }
            if(!contactMap.isEmpty()){
                updatefive9ListItem(contactMap);
            }else if(contactMap.isEmpty() && !cContactMap.isEmpty()){
                addPotentialBuyers(cContactMap);
            }          
        }
        
        
        
        Public Static void doBeforeUpdate(List<Contact> contactList , Map<Id,Contact> contactIdMap){
            Map<Id,Contact> contactMap = new Map<Id,Contact>();
            Map<Id,Contact> cContactMap = new Map<Id,Contact>();
            for(Contact con : contactList){
                if(con.Product__c != null){
                    if(con.Product__c != contactIdMap.get(con.Id).Product__c)
                        contactMap.put(con.Id, con);
                }else if(con.Product__c == null && con.Potential_Buyer__c != null ){
                    cContactMap.put(con.Id,con);
                }
            }
            system.debug(cContactMap);
            if(!contactMap.isEmpty()){
                updatefive9ListItem(contactMap);
            }else if(contactMap.isEmpty() && !cContactMap.isEmpty()){
                addPotentialBuyers(cContactMap);
            }  
        }
        
        Public Static void updatefive9ListItem(  Map<Id,Contact> contactMap){
            
            
            Map<String,List<String>> five9ContactMap = new Map<String,List<String>>();
            Set<Five9LSP__Five9_List_Item__c> targetfive9MembersSet = new Set<Five9LSP__Five9_List_Item__c>();        
            Set<String> cMemberContactIdSet = new Set<String>();
            Set<Five9LSP__Five9_List_Item__c> cMemberDeletionSet = new Set<Five9LSP__Five9_List_Item__c>();
            Map<String,List<Workflow_Configuration__c>> sourceConfigurationMap = new Map<String,List<Workflow_Configuration__c>>();
            
            System.debug('contactMap---'+contactMap);
            Set<String> five9IdSet = new Set<String>();
            List<Five9LSP__Five9_List_Item__c> five9List = [SELECT Id, Five9LSP__Five9_List__c,Five9LSP__Five9_List__r.Name, Five9LSP__Contact__c FROM Five9LSP__Five9_List_Item__c WHERE Five9LSP__Contact__c IN :contactMap.keySet()];
            if(!five9List.isEmpty()){
                for(Five9LSP__Five9_List_Item__c cm: five9List){
                    if(cm.Five9LSP__Five9_List__r.Name.Contains('Day')  && (cm.Five9LSP__Five9_List__r.Name.Contains('Buy') || cm.Five9LSP__Five9_List__r.Name.Contains('Lead')))
                        five9IdSet.add(cm.Five9LSP__Five9_List__c);
                }
                
                System.debug('five9List---'+five9List);
                System.debug('five9List---'+five9List.size());
                if(!five9IdSet.isEmpty()){
                    for(Workflow_Configuration__c wc : [SELECT Id, Source_Five9_List__c, Target_Five9_List__c, Products__c FROM Workflow_Configuration__c WHERE Source_Five9_List__c IN :five9IdSet AND ListMovementActive__c=true]){
                        if(sourceConfigurationMap.containsKey(wc.Source_Five9_List__c))
                            sourceConfigurationMap.get(wc.Source_Five9_List__c).add(wc);
                        else
                            sourceConfigurationMap.put(wc.Source_Five9_List__c, new List<Workflow_Configuration__c>{wc});
                    }
                    System.debug('sourceConfigurationMap---'+sourceConfigurationMap);
                    System.debug('sourceConfigurationMap---'+sourceConfigurationMap.size());
                    
                    
                    for(Five9LSP__Five9_List_Item__c cm : five9List){
                        if(contactMap.containsKey(cm.Five9LSP__Contact__c) && sourceConfigurationMap.containsKey(cm.Five9LSP__Five9_List__c)){
                            
                            for(Workflow_Configuration__c wc : sourceConfigurationMap.get(cm.Five9LSP__Five9_List__c)){
                                Contact con = contactMap.get(cm.Five9LSP__Contact__c);
                                String productName = con.Product__c.Split(';').get(con.Product__c.Split(';').size()-1);
                                if(wc.Source_Five9_List__c == cm.Five9LSP__Five9_List__c && wc.Products__c == productName){
                                    System.debug('wc.Source_Five9_List__c-->'+wc.Source_Five9_List__c);
                                    System.debug('lcw.Five9LSP__Five9_List__c-->'+cm.Five9LSP__Five9_List__c);
                                    cMemberContactIdSet.add(cm.Five9LSP__Contact__c);
                                    if(five9ContactMap.containsKey(wc.Target_Five9_List__c)){
                                        five9ContactMap.get(wc.Target_Five9_List__c).add(cm.Five9LSP__Contact__c);
                                    }else
                                        five9ContactMap.put(wc.Target_Five9_List__c, new List<String>{cm.Five9LSP__Contact__c});
                                }
                            }
                        }
                    }
                    
                    for(Five9LSP__Five9_List_Item__c cm : five9List){
                        for(String contactId : cMemberContactIdSet){
                            if(cm.Five9LSP__Contact__c == contactId && !cMemberDeletionSet.contains(cm)){
                                cMemberDeletionSet.add(cm);
                            }
                        }
                    }
                    
                    System.debug('five9ContactMap---'+five9ContactMap);
                    Map<String,String> existingCMMap = new Map<String,String>();
                    for(String targetId : five9ContactMap.keySet()){
                        for(String contactId : five9ContactMap.get(targetId)){
                            Five9LSP__Five9_List_Item__c cm = new Five9LSP__Five9_List_Item__c();
                            cm.Five9LSP__Contact__c = contactId;
                            cm.Five9LSP__Five9_List__c = targetId;
                            cm.Moved_Date__c=Date.today();
                            existingCMMap.put(targetId, contactId);
                            if(!targetfive9MembersSet.contains(cm))
                                targetfive9MembersSet.add(cm);
                        }
                    }
                    
                    System.debug('targetfive9MembersSet---'+targetfive9MembersSet);
                    System.debug('cMemberDeletionSet---'+cMemberDeletionSet);
                    
                    List<Five9LSP__Five9_List_Item__c> cmExistingList = [SELECT Id, Five9LSP__Five9_List__c,Five9LSP__Contact__c FROM Five9LSP__Five9_List_Item__c WHERE Five9LSP__Contact__c IN :existingCMMap.values() AND Five9LSP__Five9_List__c IN :existingCMMap.keySet()];
                    List<Five9LSP__Five9_List_Item__c> deletionList = new List<Five9LSP__Five9_List_Item__c>(cMemberDeletionSet);
                    if(!cmExistingList.isEmpty()){
                        deletionList.addAll(cmExistingList);
                    }
                    try{
                        Database.delete( deletionList);
                        try{
                            Database.upsert(new List<Five9LSP__Five9_List_Item__c>(targetfive9MembersSet));
                            
                        }Catch(Exception ex){
                            System.debug('Upsert Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                        }
                    }Catch(Exception ex){
                        System.debug('XDelete Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                    }                        
                }
            }else{
                
                Map<String,String> productContactMap = new Map<String,String>();
                Map<String,String> productFive9ItemMap = new Map<String,String>();
                List<String> productList = new List<String>();
                Set<Five9LSP__Five9_List_Item__c> five9Listitem = new Set<Five9LSP__Five9_List_Item__c>();
                for(String lid : contactMap.keySet()){
                    Contact ld = contactMap.get(lid);
                    String productName = ld.Product__c.Split(';').get(ld.Product__c.Split(';').size()-1);
                    productContactMap.put(productName, lid);
                    productList.add(productName);
                }
                system.debug('productContactMap-->'+productContactMap);
                system.debug('productList-->'+productList);
                if(!productList.isEmpty()){
                    for(Workflow_Configuration__c wc : [SELECT Id, Source_Five9_List__c, Target_Five9_List__c, Products__c FROM Workflow_Configuration__c WHERE Products__c IN :productList AND ListMovementActive__c=true AND Target_Five9_List__c!=null]){
                        System.debug('=================================workflowsData======================>'+wc);
                        if(!productFive9ItemMap.containsKey(wc.Products__c))
                            productFive9ItemMap.put(wc.Products__c,wc.Target_Five9_List__c);
                    }
                    if(!productFive9ItemMap.isEmpty()){
                        for(String p : productContactMap.keySet()){
                            Five9LSP__Five9_List_Item__c f9item = new Five9LSP__Five9_List_Item__c();
                            f9item.Five9LSP__Contact__c = productContactMap.get(p);
                            f9item.Five9LSP__Five9_List__c =productFive9ItemMap.get(p);
                            f9item.Moved_Date__c=Date.today();
                            system.debug('f9item--->'+f9item);
                            five9Listitem.add(f9item);
                        }
                        
                        system.debug('five9Listitem--->'+five9Listitem);
                        insert new List<Five9LSP__Five9_List_Item__c>(five9Listitem);
                    }
                }
            }
        }
        
        public static void addPotentialBuyers(Map<Id,Contact> contactMap){
            Map<String,Five9LSP__Five9_List_Item__c> delf9itemMap = new Map<String,Five9LSP__Five9_List_Item__c>();
            Set<String> potentialf9itemSet = new Set<String>();
            Map<String,String> productContactMap = new Map<String,String>();
            Map<String,Five9LSP__Five9_List__c> FIve9itemNameMap = new Map<String,Five9LSP__Five9_List__c>(); 
            Set<Five9LSP__Five9_List_Item__c> insertfive9Set = new Set<Five9LSP__Five9_List_Item__c >();
            
            for(Five9LSP__Five9_List_Item__c cm : [SELECT Id, Five9LSP__Five9_List__c,Five9LSP__Five9_List__r.Name, Five9LSP__Contact__c FROM Five9LSP__Five9_List_Item__c WHERE Five9LSP__Contact__c IN :contactMap.keySet()]){  
                delf9itemMap.put(cm.Id,cm);
            }
            system.debug('delf9itemMap--->'+delf9itemMap);
            
            for(String cid : contactMap.keySet()){
                Contact con = contactMap.get(cid);
                String productName = con.Potential_Buyer__c;
                system.debug('potential buyer --->'+productName);
                potentialf9itemSet.add(productName);
                productContactMap.put(cid, productName);
            }
            system.debug('potentialf9itemSet--> '+potentialf9itemSet);
            system.debug('productContactMap--> '+productContactMap);
            
            for(Five9LSP__Five9_List__c c : [SELECT Id,Name From Five9LSP__Five9_List__c WHERE Name IN :potentialf9itemSet]){
                FIve9itemNameMap.put(c.Name, c);
            }
            system.debug('FIve9itemNameMap---> '+FIve9itemNameMap);
            if(!FIve9itemNameMap.isEmpty()){
                if(!productContactMap.isEmpty()){
                    for(String cid : productContactMap.keySet()){
                        string cname = productContactMap.get(cid);
                        if(cname != null){
                            Five9LSP__Five9_List__c c = FIve9itemNameMap.get(cname);
                            System.debug('=============================Five9LSP__Five9_List__c=========>'+c);
                            Five9LSP__Five9_List_Item__c cm = new Five9LSP__Five9_List_Item__c();
                            cm.Five9LSP__Five9_List__c = c.Id;
                            cm.Five9LSP__Contact__c = cid;
                            cm.Moved_Date__c=Date.today();
                            insertfive9Set.add(cm);
                        }
                    }
                    system.debug('insertfive9Set---> '+insertfive9Set);
                }
            }
            List<Five9LSP__Five9_List_Item__c> deleteList = delf9itemMap.values();
            system.debug('deleteList--->'+deleteList);
            if(!deleteList.isEmpty()){
                try{
                    delete deleteList;
                    if(!insertfive9Set.isEmpty()){
                        try{
                            insert new List<Five9LSP__Five9_List_Item__c>(insertfive9Set);
                        }catch(Exception ex){ 
                            system.debug(ex.getMessage()+ex.getLineNumber());
                        }
                    }
                }catch(Exception ex){
                    system.debug(ex.getMessage()+ex.getLineNumber());
                }
            }else if(!insertfive9Set.isEmpty()){
                try{
                    insert new List<Five9LSP__Five9_List_Item__c>(insertfive9Set);
                }catch(Exception ex){ 
                    system.debug(ex.getMessage()+ex.getLineNumber());
                }
            }
    }
     public Static void Coverage(){
        Integer i=1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
    }    
}