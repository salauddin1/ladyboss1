@RestResource(urlMapping='/pap_commission')
global class PAP_API_commission {
    @HttpGet
    global static void createEvent() {
        RestRequest req = RestContext.request;
		System.debug(req.params);
        Map<String,String> paramValueMap = req.params;
        System.debug('paramValueMap : '+paramValueMap);
        String actionname = paramValueMap.get('actionname');
        String com = paramValueMap.get('com');
        String ord_id = paramValueMap.get('ordid');
        ord_id = ord_id.replaceAll('\\(.*\\)','');
        String prod_id = paramValueMap.get('prodId');
        String refid = paramValueMap.get('refid');
        String status = paramValueMap.get('status');
        String types = paramValueMap.get('type');
        String campId = paramValueMap.get('campaignId');
        String campName = paramValueMap.get('campaignName');
        String data1 = paramValueMap.get('data1');
        String data2 = paramValueMap.get('data2');
        String data3 = paramValueMap.get('data3');
        String data4 = paramValueMap.get('data4');
        String data5 = paramValueMap.get('data5');
        String TotalCost = paramValueMap.get('totalcost');

        data5 = data5.removeStart('#');
        
        System.debug(actionname+' '+com+' '+ord_id+' '+prod_id+' '+refid+' '+status+' '+types+' '+TotalCost);
        if(refid!=null && status=='approved' && ord_id!=null && prod_id!=null && types=='Sales'){
            List<Pap_Commission__c> papList = [select id,Contact__c,affiliate_ref_id__c,order_id__c,product_id__c,Campaign_Id__c,Campaign_name__c,data1__c,data2__c,data3__c,data4__c,data5__c,Total_Cost__c,Commission_Type__c from Pap_Commission__c where order_id__c =:ord_id and product_id__c=:prod_id and Commission_Type__c ='initial' ];
            List<Contact> cnt = [SELECT id FROM Contact WHERE PAP_refid__c=:refid AND Is_Master__c = true LIMIT 1];
            List<Opportunity> oppList = [SELECT id,Coupon_Code__c,Contact__c FROM Opportunity WHERE WC_Order_Number__c =:data5 AND WC_Product_Id__c =:prod_id];
            Pap_Commission__c pc = new Pap_Commission__c();
            if(papList.size()>0 && oppList.size()>0 && papList[0].Contact__c == oppList[0].Contact__c){
                pc = papList[0];
                pc.affiliate_ref_id__c = refid;
                pc.order_id__c = ord_id;
                pc.product_id__c = prod_id;
                pc.Campaign_Id__c = campId;
                pc.Campaign_name__c = campName;
                pc.data1__c = data1;
                pc.data2__c = data2;
                pc.data3__c = data3;
                pc.data4__c = data4;
                pc.data5__c = data5;
                pc.Total_Cost__c = Decimal.valueof(TotalCost);
                pc.Commission_Type__c = 'initial';
                update pc;
            }else{
                pc.affiliate_ref_id__c = refid;
                pc.commission__c = Decimal.valueOf(com);
                pc.order_id__c = ord_id;
                pc.product_id__c = prod_id;
                pc.Campaign_Id__c = campId;
                pc.Campaign_name__c = campName;
                pc.data1__c = data1;
                pc.data2__c = data2;
                pc.data3__c = data3;
                pc.data4__c = data4;
                pc.data5__c = data5;
                pc.Total_Cost__c = Decimal.valueof(TotalCost);
                pc.Commission_Type__c = 'initial';
                
                if(cnt.size()==1){
                    pc.Contact__c = cnt.get(0).Id;
                }
                if (oppList.size()>0) {
                    pc.Opportunity__c = oppList[0].Id;
                    //oppList[0].Coupon_Code__c = pc.affiliate_ref_id__c;
                    //update oppList;
                }
                insert pc;
            }
            
        }
    }
}