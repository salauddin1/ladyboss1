global class SipStationOrderOpportunityLinkBatch implements Database.Batchable<Sobject>,Database.AllowsCallouts {
    global database.querylocator start(database.batchableContext bc){
        String query = 'select Id, orderNumber__c,Linked_To_Opportunity__c from ShipStation_Orders__c where orderNumber__c != null AND Linked_To_Opportunity__c = false AND CreatedDate >= Last_N_Days:14 AND orderNumber__c Like \'ch_%\'';//
        return database.getquerylocator(query);
    }
    global void execute(database.batchablecontext bd, list<ShipStation_Orders__c> scope){
        System.debug('========'+scope);
        Set<String> OrdNumberSet = new Set<String>();
        Map<String, ShipStation_Orders__c> OrdNumberMap = new Map<String, ShipStation_Orders__c>();
        for(ShipStation_Orders__c ord : scope) {
            if(ord.orderNumber__c.contains('ch_')) {
                OrdNumberSet.add(ord.orderNumber__c);
                OrdNumberMap.put(ord.orderNumber__c, ord);
            }
        }
        try {
            List<ShipStation_Orders__c> OrdUpdate = new List<ShipStation_Orders__c>();
            List<opportunity> oppList = [select Id, Stripe_Charge_Id__c from opportunity where Stripe_Charge_Id__c != null AND Stripe_Charge_Id__c IN : OrdNumberSet];
            if(oppList.size()>0 && !OrdNumberMap.isEmpty()) {
                for(Opportunity opt : oppList) {
                    if(OrdNumberMap.containsKey(opt.Stripe_Charge_Id__c)) {
                        ShipStation_Orders__c curntOrd = OrdNumberMap.get(opt.Stripe_Charge_Id__c);
                        if(curntOrd != null) {
                            opt.ShipStation_Orders__c = curntOrd.Id;
                            opt.Linked_To_ShipStation_Order__c = true;
                            curntOrd.Linked_To_Opportunity__c = true;
                            OrdUpdate.add(curntOrd);
                        }
                    }
                }
            }
            ShipstationOrderStatusShippedBatch.flag = true;
            Update oppList;
            Update OrdUpdate;
        }
        catch (exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(scope.size()>0) {   for(ShipStation_Orders__c c : scope) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('ececutemethod','SipStationOrderOpportunityLinkBatch',Ids, e));
        }
    }
    global void finish(database.batchablecontext bd){
    }
}