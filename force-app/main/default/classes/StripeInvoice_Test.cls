@IsTest
public class StripeInvoice_Test {

	static testMethod void testParse() {
		String json = '{ \"id\": \"evt_1BGxKyDiFnu7hVq7Fa0Ui798\", \"object_data\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508970420, \"data\": { \"object_data\": { \"id\": \"in_1BGvc2DiFnu7hVq7DNLFCBWE\", \"object_data\": \"invoice\", \"amount_due\": 2750, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": false, \"billing\": \"send_data_invoice\", \"charge\": null, \"closed\": false, \"currency_data\": \"usd\", \"customer\": \"cus_BUPh8oOgx5CXG2\", \"date\": 1508963789, \"description\": \"TEST H\", \"discount\": null, \"due_date\": 1511555789, \"end_dataing_balance\": 0, \"forgiven\": false, \"lines\": { \"object_data\": \"list\", \"data\": [ { \"id\": \"sub_BeE40s8qr26QUX\", \"object_data\": \"line_item\", \"amount\": 2500, \"currency_data\": \"usd\", \"description\": null, \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1508963789, \"end_data\": 1511642189 }, \"plan\": { \"id\": \"test1\", \"object_data\": \"plan\", \"amount\": 2500, \"created\": 1508947075, \"currency_data\": \"usd\", \"interval\": \"month\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"name\": \"Test Product 1\", \"statement_descriptor\": null, \"trial_period_days\": null }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_1BGvc1DiFnu7hVq7RbuZfvdv\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1BGvc2DiFnu7hVq7DNLFCBWE/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"c097b8d110-0001\", \"paid\": false, \"period_end_data\": 1508963789, \"period_start\": 1508963789, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_BeE40s8qr26QUX\", \"subtotal\": 2500, \"tax\": 250, \"tax_percent\": 10.0, \"total\": 2750, \"webhooks_delivered_at\": 1508963790 }, \"previous_attributes\": { \"description\": null } }, \"livemode\": false, \"pend_dataing_webhooks\": 1, \"request\": { \"id\": \"req_5a0jhAQhqcE4gh\", \"idempotency_key\": null }, \"type\": \"invoice.updated\" }';
		StripeInvoice.StripeInvoiceObject r = StripeInvoice.StripeInvoiceObject.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Data_Z objData_Z = new StripeInvoice.Data_Z(System.JSON.createParser(json));
		System.assert(objData_Z != null);
		System.assert(objData_Z.object_data == null);
		System.assert(objData_Z.previous_attributes == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.StripeInvoiceObject objJSON2Apex = new StripeInvoice.StripeInvoiceObject(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.id == null);
		System.assert(objJSON2Apex.object_data == null);
		System.assert(objJSON2Apex.api_version == null);
		System.assert(objJSON2Apex.created == null); 
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.livemode == null);
		System.assert(objJSON2Apex.pend_dataing_webhooks == null);
		System.assert(objJSON2Apex.request == null);
		System.assert(objJSON2Apex.type_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Previous_attributes objPrevious_attributes = new StripeInvoice.Previous_attributes(System.JSON.createParser(json));
		System.assert(objPrevious_attributes != null);
		System.assert(objPrevious_attributes.description == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Metadata objMetadata = new StripeInvoice.Metadata(System.JSON.createParser(json));
		System.assert(objMetadata != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Request objRequest = new StripeInvoice.Request(System.JSON.createParser(json));
		System.assert(objRequest != null);
		System.assert(objRequest.id == null);
		//System.assert(objRequest.idempotency_key == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Period objPeriod = new StripeInvoice.Period(System.JSON.createParser(json));
		System.assert(objPeriod != null);
		System.assert(objPeriod.start == null);
		System.assert(objPeriod.end_data == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Data objData = new StripeInvoice.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_data == null);
		System.assert(objData.amount == null);
		System.assert(objData.currency_data == null);
		System.assert(objData.description == null);
		System.assert(objData.discountable == null);
		System.assert(objData.livemode == null);
		System.assert(objData.metadata == null);
		System.assert(objData.period == null);
		System.assert(objData.plan == null);
		System.assert(objData.proration == null);
		System.assert(objData.quantity == null);
		System.assert(objData.subscription == null);
		System.assert(objData.subscription_item == null);
		System.assert(objData.type_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Object_data objObject_data = new StripeInvoice.Object_data(System.JSON.createParser(json));
		System.assert(objObject_data != null);
		System.assert(objObject_data.id == null);
		System.assert(objObject_data.object_data == null);
		System.assert(objObject_data.amount_due == null);
		System.assert(objObject_data.application_fee == null);
		System.assert(objObject_data.attempt_count == null);
		System.assert(objObject_data.attempted == null);
		System.assert(objObject_data.billing == null);
		System.assert(objObject_data.charge == null);
		System.assert(objObject_data.closed == null);
		System.assert(objObject_data.currency_data == null);
		System.assert(objObject_data.customer == null);
		System.assert(objObject_data.date_data == null);
		System.assert(objObject_data.description == null);
		System.assert(objObject_data.discount == null);
		System.assert(objObject_data.due_date == null);
		System.assert(objObject_data.end_dataing_balance == null);
		System.assert(objObject_data.forgiven == null);
		System.assert(objObject_data.lines == null);
		System.assert(objObject_data.livemode == null);
		System.assert(objObject_data.metadata == null);
		System.assert(objObject_data.next_payment_attempt == null);
		System.assert(objObject_data.number_Z == null);
		System.assert(objObject_data.paid == null);
		System.assert(objObject_data.period_end_data == null);
		System.assert(objObject_data.period_start == null);
		System.assert(objObject_data.receipt_number == null);
		System.assert(objObject_data.starting_balance == null);
		System.assert(objObject_data.statement_descriptor == null);
		System.assert(objObject_data.subscription == null);
		System.assert(objObject_data.subtotal == null);
		System.assert(objObject_data.tax == null);
		System.assert(objObject_data.tax_percent == null);
		System.assert(objObject_data.total == null);
		System.assert(objObject_data.webhooks_delivered_at == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Plan objPlan = new StripeInvoice.Plan(System.JSON.createParser(json));
		System.assert(objPlan != null);
		System.assert(objPlan.id == null);
		System.assert(objPlan.object_data == null);
		System.assert(objPlan.amount == null);
		System.assert(objPlan.created == null);
		System.assert(objPlan.currency_data == null);
		System.assert(objPlan.interval == null);
		System.assert(objPlan.interval_count == null);
		System.assert(objPlan.livemode == null);
		System.assert(objPlan.metadata == null);
		System.assert(objPlan.name == null);
		System.assert(objPlan.statement_descriptor == null);
		//System.assert(objPlan.trial_period_days == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Lines objLines = new StripeInvoice.Lines(System.JSON.createParser(json));
		System.assert(objLines != null);
		System.assert(objLines.object_data == null);
		System.assert(objLines.data == null);
		System.assert(objLines.has_more == null);
		System.assert(objLines.total_count == null);
		System.assert(objLines.url == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Coupon objCoupon = new StripeInvoice.Coupon(System.JSON.createParser(json));
		System.assert(objCoupon != null);
		System.assert(objCoupon.id == null);
		System.assert(objCoupon.object_data == null);
		System.assert(objCoupon.amount_off == null);
		System.assert(objCoupon.created == null);
		System.assert(objCoupon.currency_data == null);
        System.assert(objCoupon.duration == null);
        System.assert(objCoupon.duration_in_months == null);
        System.assert(objCoupon.livemode == null);
        System.assert(objCoupon.metadata == null);
        System.assert(objCoupon.percent_off == null);
        System.assert(objCoupon.times_redeemed == null);
        System.assert(objCoupon.valid == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeInvoice.Discount objDiscount = new StripeInvoice.Discount(System.JSON.createParser(json));
		System.assert(objDiscount != null);
		System.assert(objDiscount.object_data == null);
		System.assert(objDiscount.coupon == null);
		System.assert(objDiscount.customer == null);
		System.assert(objDiscount.end_data == null);
		System.assert(objDiscount.start == null);
        System.assert(objDiscount.subscription == null);
	}
}