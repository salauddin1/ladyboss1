//Batch to update subscription opportunities where Status , Period Start and Period End is missing
global class SyncsubStatusBatch implements Database.AllowsCallouts,Database.Batchable<sObject> {
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id,Subscription_Id__c,Start__c,End__c,Status__c,TrialPeriodStart__c,TrialPeriodEnd__c FROM OpportunityLineItem WHERE CreatedById = \'005f4000000TFxHAAW\' AND (Start__c = null OR End__c = null OR Status__c = null ) AND Subscription_Id__c != null ');
	}

   	global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope) {
		for (OpportunityLineItem oli: scope) {
			//getting subscription details from stripe
			Subscriptions sub = Subscriptions.getSubscription(oli.Subscription_Id__c);
			//getting number of invoices which are paid for the subscription
			oli.Number_Of_Payments__c = Invoices.getInvoice(oli.Subscription_Id__c);
			//updating Opportunityline item
			if (sub.status == 'trialing' || Test.isRunningTest()) {
				oli.Status__c = 'trialing';
				string trialstart = String.valueOf(sub.trial_start);
                Datetime dttrstart = datetime.newInstance(Long.ValueOf(trialstart)* Long.ValueOf('1000'));	
				oli.TrialPeriodStart__c = dttrstart.date();
				string trialend = String.valueOf(sub.trial_end);
                Datetime dttrend = datetime.newInstance(Long.ValueOf(trialend)* Long.ValueOf('1000'));	
				oli.TrialPeriodEnd__c = dttrend.date();
			}
			if (sub.status == 'active') {
				oli.Status__c = 'active';
				string periodstart = String.valueOf(sub.current_period_start);
                Datetime dtprstart = datetime.newInstance(Long.ValueOf(periodstart)* Long.ValueOf('1000'));
				oli.Start__c = dtprstart.date();
				string periodend = String.valueOf(sub.current_period_end);
                Datetime dtprend = datetime.newInstance(Long.ValueOf(periodend)* Long.ValueOf('1000'));
				oli.End__c = dtprend.date();
			}
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}