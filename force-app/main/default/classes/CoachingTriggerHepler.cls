public class CoachingTriggerHepler {
    
    public static List<Coaching_Commissions__c>  getCommission(Contact con , Map<id ,Map<String, List<String>>>	oldCommissionMap ){
        List<Coaching_Commissions__c > coachDataList = new List <Coaching_Commissions__c>();
        if(con.Commission_Year__c != null ){
            if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c == 'Full Pay' ){
                String commiCheck = 'M1C1;M1C2;M2C1;M2C2;M3C1;M3C2;M4C1;M4C2;M5C1;M5C2;M6C1;M6C2;M7C1;M7C2;M8C1;M8C2;M9C1;M9C2;M10C1;M10C2;M11C1;M11C2;M12C1;M12C2';
                
                if(commiCheck != null && commiCheck != ''){
                    List<String> commiMonth  =  new List<String>();
                    commiMonth = commiCheck.split(';') ;    
                    
                    if(commiMonth != null && !commiMonth.isEmpty()){
                        List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                        String parentPickVal = con.Full_Pay__c+' Full Pay';
                        if(con.Full_Pay__c == 'Launch Conference'){
                            List<String> commiMonth1  =  new List<String>();
                            commiMonth1.add('Launch Conference');
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        	returnCommission = createCommission(con, commiMonth1, oldCommissionMap, parentPickVal,  CommisPay.Launch_Conference__c);
                        }
                        else if(con.Full_Pay__c == '3 Month Retention'){
                            List<String> commiMonth2  =  new List<String>();
                            commiMonth2.add('3 Month Retention');
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        	returnCommission = createCommission(con, commiMonth2, oldCommissionMap, parentPickVal,  CommisPay.X3_Month_Retention__c);
                        }
                        else if(con.Full_Pay__c == '6 Month Retention'){
                            List<String> commiMonth3  =  new List<String>();
                            commiMonth3.add('6 Month Retention');
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        	returnCommission = createCommission(con, commiMonth3, oldCommissionMap, parentPickVal,  CommisPay.X6_Month_Retention__c);
                        }
                        else if(con.Full_Pay__c == '9 Month Retention'){
                            List<String> commiMonth4  =  new List<String>();
                            commiMonth4.add('9 Month Retention');
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        	returnCommission = createCommission(con, commiMonth4, oldCommissionMap, parentPickVal,  CommisPay.X9_Month_Retention__c);
                        }
                        else if(con.Full_Pay__c == '1 Year Ascension'){
                            List<String> commiMonth5  =  new List<String>();
                            commiMonth5.add('1 Year Ascension');
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        	returnCommission = createCommission(con, commiMonth5, oldCommissionMap, parentPickVal,  CommisPay.X1_Year_Ascension__c);
                        }
                        else if(con.Full_Pay__c == '$2997'){
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_2997_PicValue__c);
                        }
                        else if(con.Full_Pay__c == '$3997'){
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_3997_PicValue__c);
                        }
                        else if(con.Full_Pay__c == '$4997'){
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_4997_PicValue__c);
                        }
                        else if(con.Full_Pay__c == '$4497'){
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_4497_PicValue__c);
                        }
                        
                        if(returnCommission != null && !returnCommission.isEmpty()){
                            coachDataList.addAll(returnCommission);
                        }
                    }
                }
            }
            else if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c == 'Monthly Subscription'){
                if(con.X297_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X297_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X297_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$297 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_297_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.X397_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X397_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X397_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$397 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_397_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.X497_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X497_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X497_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$497 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_497_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.X447_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X447_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X447_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$447 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_447_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
            }
            else if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c == 'Subscription with Final Payment'){
                if(con.X297_Subscription_with_Final_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X297_Subscription_with_Final_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X297_Subscription_with_Final_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$297 Subscription with Final Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_297_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.X397_Subscription_with_Final_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X397_Subscription_with_Final_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X397_Subscription_with_Final_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$397 Subscription with Final Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_397_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.X497_Subscription_with_Final_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X497_Subscription_with_Final_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X497_Subscription_with_Final_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$497 Subscription with Final Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_497_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }    
                }
                if(con.X447_Subscription_with_Final_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.X447_Subscription_with_Final_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.X447_Subscription_with_Final_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = '$447 Subscription with Final Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_447_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
            }
            else if(con.Coaching_Payment_Option__c != null && con.Coaching_Payment_Option__c == 'Down Payment with Subscription'){
                System.debug('  test ' +con.Coaching_Payment_Option__c);
                if(con.Down_Payment_and_297_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.Down_Payment_and_297_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.Down_Payment_and_297_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = 'Down Payment and $297 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_297_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.Down_Payment_and_397_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.Down_Payment_and_397_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.Down_Payment_and_397_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = 'Down Payment and $397 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_397_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.Down_Payment_and_497_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.Down_Payment_and_497_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.Down_Payment_and_497_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = 'Down Payment and $497 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_497_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
                if(con.Down_Payment_and_447_Monthly_Payment__c != null ){
                    String commiCheck = '';
                    commiCheck = con.Down_Payment_and_447_Monthly_Payment__c ;
                    if(commiCheck != null && commiCheck != ''){
                        List<String> commiMonth  =  new List<String>();
                        if(commiCheck.contains(';')){
                            commiMonth = con.Down_Payment_and_447_Monthly_Payment__c.split(';') ;    
                        }
                        else{
                            commiMonth.add(commiCheck);
                        }
                        if(commiMonth != null && !commiMonth.isEmpty()){
                            List<Coaching_Commissions__c > returnCommission = new List <Coaching_Commissions__c>();
                            String parentPickVal = 'Down Payment and $447 Monthly Payment';
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            returnCommission = createCommission(con, commiMonth, oldCommissionMap, parentPickVal ,CommisPay.Amount_447_PicValue__c);
                            if(returnCommission != null && !returnCommission.isEmpty()){
                                coachDataList.addAll(returnCommission);
                            }
                        }
                    }
                }
            }
        }
        return coachDataList ;
    }
    
    private static List<Coaching_Commissions__c>  createCommission(Contact con , List<String> commiMonth ,Map<id ,Map<String, List<String>>>	oldCommissionMap , String parentPickVal, Decimal amount){
        List<Coaching_Commissions__c > coachDataList = new List <Coaching_Commissions__c>();
        
        
        if(commiMonth != null && !commiMonth.isEmpty()){
            for(String str : commiMonth){
                if(str != null && str !=''){
                    if(oldCommissionMap != null && oldCommissionMap.keySet().contains(con.id) ){
                        Map<String, List<String>>	childCommMap = new Map<String, List<String>> ();
                        childCommMap = oldCommissionMap.get(con.id);
                        if(childCommMap != null && childCommMap.keySet().contains(parentPickVal) ){
                            String strYear = str+';'+con.Commission_Year__c;
                            if( childCommMap.get(parentPickVal).contains(strYear) ){
                                System.debug('**str 1 **'+str);
                            }
                            else{
                                Coaching_Commissions__c coCom = new Coaching_Commissions__c();
                                coCom.Contact__c = con.Id ;
                                coCom.Commission_Amount__c = amount ;
                                coCom.Commission_Type__c = str ;
                                coCom.Commission_Parent_Type__c = parentPickVal;
                                coCom.Commission_Year__c = con.Commission_Year__c;
                                coCom.Coaching_Payment_Option__c = con.Coaching_Payment_Option__c ;
                                
                                Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                                if(str == 'Launch Conference'){
                                    coCom.Commission_Amount__c = CommisPay.Launch_Conference__c ;
                                }
                                else if(str == '3 Month Retention'){
                                    coCom.Commission_Amount__c = CommisPay.X3_Month_Retention__c ;
                                }
                                else if(str == '6 Month Retention'){
                                    coCom.Commission_Amount__c = CommisPay.X6_Month_Retention__c ;
                                }
                                else if(str == '9 Month Retention'){
                                    coCom.Commission_Amount__c = CommisPay.X9_Month_Retention__c ;
                                }
                                else if(str == '1 Year Ascension'){
                                    coCom.Commission_Amount__c = CommisPay.X1_Year_Ascension__c ;
                                }
                                
                                System.debug('**str 2 **'+str);
                                coachDataList.add(coCom);
                            }
                        }
                        else{
                            Coaching_Commissions__c coCom = new Coaching_Commissions__c();
                            coCom.Contact__c = con.Id ;
                            coCom.Commission_Amount__c = amount ;
                            coCom.Commission_Type__c = str ;
                            coCom.Commission_Parent_Type__c = parentPickVal;
                            coCom.Commission_Year__c = con.Commission_Year__c;
                            coCom.Coaching_Payment_Option__c = con.Coaching_Payment_Option__c ;
                            
                            Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                            if(str == 'Launch Conference'){
                                coCom.Commission_Amount__c = CommisPay.Launch_Conference__c ;
                            }
                            else if(str == '3 Month Retention'){
                                coCom.Commission_Amount__c = CommisPay.X3_Month_Retention__c ;
                            }
                            else if(str == '6 Month Retention'){
                                coCom.Commission_Amount__c = CommisPay.X6_Month_Retention__c ;
                            }
                            else if(str == '9 Month Retention'){
                                coCom.Commission_Amount__c = CommisPay.X9_Month_Retention__c ;
                            }
                            else if(str == '1 Year Ascension'){
                                coCom.Commission_Amount__c = CommisPay.X1_Year_Ascension__c ;
                            }
                            
                            System.debug('**str 2 **'+str);
                            coachDataList.add(coCom);
                        }
                    }
                    else{
                        Coaching_Commissions__c coCom = new Coaching_Commissions__c();
                        coCom.Contact__c = con.Id ;
                        coCom.Commission_Amount__c = amount ;
                        coCom.Commission_Type__c = str ;
                        coCom.Commission_Parent_Type__c = parentPickVal;
                        coCom.Commission_Year__c = con.Commission_Year__c;
                        coCom.Coaching_Payment_Option__c = con.Coaching_Payment_Option__c ;
                        
                        Coaching_Commission_Pay__c  CommisPay = Coaching_Commission_Pay__c.getInstance();
                        if(str == 'Launch Conference'){
                            coCom.Commission_Amount__c = CommisPay.Launch_Conference__c ;
                        }
                        else if(str == '3 Month Retention'){
                            coCom.Commission_Amount__c = CommisPay.X3_Month_Retention__c ;
                        }
                        else if(str == '6 Month Retention'){
                            coCom.Commission_Amount__c = CommisPay.X6_Month_Retention__c ;
                        }
                        else if(str == '9 Month Retention'){
                            coCom.Commission_Amount__c = CommisPay.X9_Month_Retention__c ;
                        }
                        else if(str == '1 Year Ascension'){
                            coCom.Commission_Amount__c = CommisPay.X1_Year_Ascension__c ;
                        }
                        
                        
                        System.debug('**str 3 **'+str);      
                        coachDataList.add(coCom);
                    }
                }
            }
        }
        
        return coachDataList;
    }
}