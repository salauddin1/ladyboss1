global class InvoiceToOliChargeHelper {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/charges';
    private static final String SERVICE_URL_1 = 'https://api.stripe.com/v1/subscriptions';
	global static void updateChargeCallout(OpportunityLineItem oli,String salesPerson,String chargeId) {
        List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c from Opportunity where id =: oli.OpportunityId ];
        Opportunity opp = oppList[0];
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
        }
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',BillingAddress);
        metadata.put('Full Name',opp.Contact__r.firstname+' '+opp.Contact__r.lastname);
        metadata.put('Email',opp.Contact__r.email);
        metadata.put('Phone',opp.Contact__r.phone);
        metadata.put('Sales Person',salesPerson);
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.Contact__r.id);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',addressList[0].Shipping_Street__c);
        metadata.put('City',addressList[0].Shipping_City__c);
        metadata.put('State',addressList[0].Shipping_State_Province__c);
        metadata.put('Postal Code',addressList[0].Shipping_Zip_Postal_Code__c);
        metadata.put('Country',addressList[0].Shipping_Country__c);
        
        system.debug('metadata'+metadata);
        if(System.isBatch()){
        	updateChargeNotFuture(chargeId,metadata);
        }else{
            System.debug('Not In Batch');
            updateChargeFuture(chargeId,metadata);
        }
        if(Test.isRunningTest()){
            updateChargeNotFuture(chargeId,metadata);
        }
    }
    
    global static void updateSubCallout(OpportunityLineItem oli,String salesPerson,String subId) {
        List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c from Opportunity where id =: oli.OpportunityId ];
        Opportunity opp = oppList[0];
        List<Address__c> addressList = new List<Address__c>();
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:opp.Contact__c and primary__c = true]; 
        String BillingAddress;
        if(!addressList.isEmpty()){
            BillingAddress = addressList[0].Shipping_City__c+' '+addressList[0].Shipping_Street__c +' '+addressList[0].Shipping_State_Province__c +' '+addressList[0].Shipping_Country__c+' '+ addressList[0].Shipping_Zip_Postal_Code__c;
        }
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',BillingAddress);
        metadata.put('Full Name',opp.Contact__r.firstname+' '+opp.Contact__r.lastname);
        metadata.put('Email',opp.Contact__r.email);
        metadata.put('Phone',opp.Contact__r.phone);
        metadata.put('Sales Person',salesPerson);
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp.Contact__r.id);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',addressList[0].Shipping_Street__c);
        metadata.put('City',addressList[0].Shipping_City__c);
        metadata.put('State',addressList[0].Shipping_State_Province__c);
        metadata.put('Postal Code',addressList[0].Shipping_Zip_Postal_Code__c);
        metadata.put('Country',addressList[0].Shipping_Country__c);
        
        system.debug('metadata'+metadata);
        if(System.isBatch()){
        	updateSubNotFuture(subId,metadata);
        }else{
            System.debug('Not In Batch');
            updateSubFuture(subId,metadata);
        }
        if(Test.isRunningTest()){
            updateSubNotFuture(subId,metadata);
        }
    }
    
    @Future(callout=true)
    global static void updateChargeFuture(String chargeId, Map<String, String> metadata) {
        
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL+'/'+chargeId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
        
    }
    
    @Future(callout=true)
    global static void updateSubFuture(String subId, Map<String, String> metadata) {
        System.debug('subId : '+subId);
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL_1+'/'+subId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    
    global static void updateChargeNotFuture(String chargeId, Map<String, String> metadata) {
        
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL+'/'+chargeId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    
    global static void updateSubNotFuture(String subId, Map<String, String> metadata) {
        
        system.debug('metadata-->'+metadata);
        String endPoint = SERVICE_URL_1+'/'+subId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();         
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                //return null;
            }
        } else {
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            //return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
    
}