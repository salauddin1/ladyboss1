@isTest
public class CreateAffiliateAndCouponBatchTest {
    @isTest
    public static void testMethod1(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        pa.X3FF_Campaign_Id__c = 'test';
        insert pa;
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        cc.Product_Exclude_Categories__c = '22,32';
        insert cc;
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Store';
        cs.Site_Url__c = 'www.test.com';
        cs.Create_Coupon_Site_URL__c = 'www.test.com';
        insert cs;     
        
        Contact ct = new Contact();
        ct.Email = 'tirth@test.com';
        ct.LastName = 'test';
        ct.FirstName = 'test';
        ct.X3FF_Batch_Description__c = '3FF Beta Member Group';
        ct.X3FF_AffiliateCreatedByBatch__c = false;
        insert ct;
        
        Contact ct1 = new Contact();
        ct1.Email = 'tirth1@test.com';
        ct1.LastName = 'test';
        ct1.FirstName = 'test';
        ct1.PAP_refid__c ='test10';
        ct1.X3FF_Batch_Description__c = '3FF Beta Member Group';
        ct.X3FF_AffiliateCreatedByBatch__c = false;
        insert ct1;
        Test.startTest();
        	CreateAffiliateAndCouponBatch obj = new CreateAffiliateAndCouponBatch('3FF Beta Member Group');
            DataBase.executeBatch(obj);

        Test.stopTest();
    }
}