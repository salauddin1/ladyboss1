@isTest
public class StripeCreateSKU_Test {
    static testMethod void test() {
        List<Product2> prodList= new List<Product2>();
        Product2 p = new Product2();
        p.Name='test';
        p.Stripe_Product_Id__c= 'abc';
        p.Price__c= 100;
        p.IsActive=true;
        p.Length__c = 10;
        p.Height__c = 15;
        p.Width__c = 20;
        p.Weight__c = 25;
        insert p;
        prodList.add(p);
        test.startTest();
        System.enqueuejob(new  StripeCreateSKU(p));
        StripeCreateSKU.createsku('usd', 'infinite', 100, 'abc', true,  p.Id, 10, 15, 20, 25);
     //   StripeCreateSKU.createsku('usd', 'infinite', 100, 'abc', true, p.Id);
        StripeCreateSKU.createskunotfuture('usd', 'infinite', p.Price__c, p.Stripe_Product_Id__c, p.IsActive, p.Id);
        StripeCreateSKU.getSKU(p.Stripe_Product_Id__c,p.Id);
        test.stopTest();
        
    }
}