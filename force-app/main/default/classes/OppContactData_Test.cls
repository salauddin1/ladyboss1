@isTest
public class OppContactData_Test {

    public static TestMethod void testGetCallout(){
        
        
        
        Test.startTest(); 
        
        
        
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        
        
        Id SubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity oppSub = new Opportunity();
        oppSub.Name = 'opp0' ;
        oppSub.RecordTypeId = SubRecordTypeId;
        oppSub.CloseDate = Date.Today();
        oppSub.StageName = 'Pending';
        oppSub.Subscription__c = 'card_1DMEcrBwLSk1v1ohY3YX0vGo';
        //       opp.AccountId = Acc.id;
        oppSub.Contact__c = Co.Id;
        insert oppSub;
       
         
        Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.Name = 'opp0' ;
        opp.RecordTypeId = ChargeRecordTypeId;
        opp.CloseDate = Date.Today();
        opp.StageName = 'Pending';
        opp.Subscription__c = 'ch_1DMElfBwLSk1v1ohKFlpJg4i';
        //       opp.AccountId = Acc.id;
        opp.Contact__c = Co.Id;
        insert opp;
       OppContactData.dummy();
        
        String ConId = String.valueOf(co.id) ;
        String OppId1 = String.valueOf(opp.id) ;
        String OppSubId1 = String.valueOf(oppSub.id) ;
        OppContactData.getOpportunityRefund(ConId);
        OppContactData.getOpportunity(ConId);
        OppContactData.getOpportunity1(ConId);
        OppContactData.getAmount(opp , 'duplicate' , OppId1 , 'test' , 1);
        OppContactData.getAmount(oppSub , 'fraudulent' , OppSubId1 , 'test' , 1);
        OppContactData.getAmount(oppSub , 'Requested_by_customer' , OppSubId1 , 'test' , 1);
        OppContactData.getAmount(oppSub , 'other' , OppSubId1 , 'test' , 1);
        OppContactData.getAmount(oppSub , 'test' , OppSubId1 , 'test' , 1);
        OppContactData.getOpportunityRefund1(ConId);
        OppContactData.getOpportunityRefund2(ConId);
        OppContactData.getOpportunityRefund3(ConId);
         OppContactData.getOpportunityRefund4(ConId);
         OppContactData.getOpportunityRefund5(ConId);
        ActiveSubscriptionApex.getOpportunity(ConId);
        Test.stopTest(); 
        
    }
}