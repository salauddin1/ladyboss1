@isTest
public  class PaypalSubscriptionBatchTest {
    @IsTest
    static void methodName(){
        Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 0;
        insert apex;
        
        ContactTriggerFlag.isContactBatchRunning=true;
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Id opportunityClubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Subscription').getRecordTypeId();
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;
        
        
        Map<String,Opportunity> oomap = new Map<String,Opportunity>();
        Map<String,List<OpportunityLineItem>> olmap = new Map<String,List<OpportunityLineItem>>();
        List<OpportunityLineItem> oplinelist = new List<OpportunityLineItem>();
        List<Opportunity> oplist = new List<Opportunity>();
        
        Paypal_Billing_Agreement__c pba = new Paypal_Billing_Agreement__c();
        pba.Billing_Agreement__c = 'billingAgreement';
        pba.Contact__c = con.Id;
        insert pba;
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        op.RecordTypeId = opportunityClubRecordTypeId;
        op.Is_PayPal__c = true;
        op.Paypal_Billing_Agreement__c = pba.id;
        insert op;
        
        oplist.add(op);
        
        oomap.put('nonClub', op);
        //insert oomap;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345',Price__c = 20.0);
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.TotalPrice = 2;
        ol.PricebookEntryId = customPrice.Id;
        ol.End__c = System.today();
        ol.Status__c = 'Active';
        ol.Tax_Amount__c = 1.0;
        insert ol;
        
        oplinelist.add(ol);
        
        olmap.put('nonClub', oplinelist);
        
        set<id> idset = new set<id>();
        idset.add(op.id);
        
        PaypalAPICreds__c paypalApi = new PaypalAPICreds__c();
        paypalApi.Client_ID__c = 'test';
        paypalApi.Secret__c = 'test';
        paypalApi.Name = 'Test';
        insert paypalApi;
        
        test.StartTest();
        PaypalSubscriptionBatch pb = new PaypalSubscriptionBatch();
        Database.executeBatch(pb,1);
        test.stopTest();
        
    }
}