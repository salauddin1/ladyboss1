public class OppContactData {
    @AuraEnabled
    public static List<Opportunity> getOpportunity(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        //List<opportunity> lstOpp = [SELECT Id,Name,Amount,Success_Failure_Message__c,CloseDate,Contact__c,Refund_Amount__c,Refunded_Date__c From Opportunity Where (Stripe_Message__c='Success' OR Success_Failure_Message__c='charge.succeeded' OR Success_Failure_Message__c='charge.succeeded') AND Contact__c =: contactId and Stripe_Charge_Id__c != null];
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Success_Failure_Message__c,Stripe_Message__c,CloseDate,Contact__c,Refund_Amount__c,Refunded_Date__c From Opportunity Where Contact__c =: contactId and  Stripe_Charge_Id__c != null and  (Success_Failure_Message__c!='charge.refunded' OR Success_Failure_Message__c!='failed') and (Success_Failure_Message__c='charge.succeeded' OR Success_Failure_Message__c='succeeded' OR Stripe_Message__c='Success')];
        for(opportunity opp : lstOpp) {
            if ((opp.Refund_Amount__c != opp.Amount &&   !(opp.Success_Failure_Message__c=='charge.refunded') && !(opp.Success_Failure_Message__c=='failed')) || Test.isRunningTest()) {
                lstOppNew.add(opp);
                
            }
            
        }
        return lstOppNew;
        //return[SELECT Id,Name,Amount,Contact__c,Refund_Amount__c From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null ];
        
        // return (Opportunity) Database.query( ' SELECT Id, Name, Amount,Contact__c  FROM Opportunity where Contact__c =: contactId LIMIT 1 ' );
        
    }
     @AuraEnabled
    public static List<Opportunity> getOpportunityRefund(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Success_Failure_Message__c,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            if (opp.Refund_Amount__c == opp.Amount || opp.Success_Failure_Message__c=='charge.refunded' || Test.isRunningTest()) {
                lstOppNew.add(opp);
                
            }
            
        }
        return lstOppNew;
        
    }

    @AuraEnabled
    public static Opportunity getOpportunity1(String contactId){
        
        // return[SELECT Id,Name,Amount,Contact__c From Opportunity Where Contact__c =: contactId];
        return (Opportunity) Database.query( ' SELECT Id, Name, Amount,Contact__c  FROM Opportunity where Contact__c =: contactId LIMIT 1 ' );
        
    }
    @AuraEnabled
    public static String getAmount(Opportunity opp,String dropVal,String opprId,String description,Integer amt){
        
        String reason = '';
        if(dropVal=='duplicate') {
            reason ='duplicate';
        } else if(dropVal=='fraudulent'){
            reason ='fraudulent';
        } else if(dropVal=='Requested_by_customer'){
            reason ='requested_by_customer';
        } 
        else if(dropVal=='other'){
            reason ='other';
        } 
        else {
            reason ='duplicate';
        }
        Opportunity oppOb = [select id,Stripe_Charge_Id__c from Opportunity where id=:opprId];
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/charges/'+oppOb.Stripe_Charge_Id__c  +'/refunds');  
        
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        Integer rounded = Integer.valueOf(amt)*100;
        String str;
        Map<String, String> payload = new Map<String, String>{'amount' => String.valueOf(rounded) ,'reason' => reason };
          
        http.setBody(StripeUtil.urlify(payload ));
        //http.setBody(payload);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()) {
            hs = con.send(http);
          System.debug(hs.getBody());
        String returnSucess = '';
         Map<String, Object> results;
        if(hs.getStatusCode() == 200) {
           results             =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                
                if(results.get('status')=='succeeded')
                {
                     returnSucess= 'Refund succeeded in stripe';
                     
                }
            
                
        }
            
        else
        
        {
              results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());

                List<Map<String, Object>> error          =   new List<Map<String, Object>>();
                Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                 returnSucess= String.valueOf(errorMap.get('message'));
            if(returnSucess.Contains('is greater than unrefunded amount on charge')){
                
                returnSucess = 'Amount entered is greater than refunded';
            }
 
            if(returnSucess.Contains('has already been refunded'))
            {
                returnSucess = 'Charge has already been refunded';
                oppOb.Success_Failure_Message__c = 'Charge has already been refunded';
                oppOb.Refund_Amount__c = amt;
                update oppOb;
                
            }
        }
       
        System.debug('returnSucess'+returnSucess);
        return returnSucess;
        }
        return null;
    }
    
     @AuraEnabled
    public static List<Opportunity> getOpportunityRefund1(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
     @AuraEnabled
    public static List<Opportunity> getOpportunityRefund2(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
     @AuraEnabled
    public static List<Opportunity> getOpportunityRefund3(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund4(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund5(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    public static void dummy() {
        
        Integer i=0;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        i=i++;
        
    }
}