//batch class to insert all the cards from stripe to salesforce
global class CardStripeBatchService implements Database.Batchable<sobject>, Database.AllowsCallouts, Database.Stateful {
    
    global database.querylocator start(database.batchablecontext bc){
        //string query='select id,Customer__c,Stripe_Customer_Id__c from Stripe_Profile__c where Card_Processed__c=false limit 100000';

        string query='select id,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Card_Processed__c=false and Stripe_Customer_Id__c!=null and LastModifiedDate = today limit 100000';
        return database.getquerylocator( query );   
    }
    global void execute(database.BatchableContext bc,list<Stripe_Profile__c> stripeProfileList){
       //System.debug('===>'+stripeProfileList);
        List< Card__c > lstOpp        = new List< Card__c >();
        for(Stripe_Profile__c stripeProf:stripeProfileList){
          List< Stripe_Profile__c > param = new List< Stripe_Profile__c >();
           param.add(stripeProf);
            stripeProf.Card_Processed__c= true; 
            List< Card__c > OppOb     = new List< Card__c >();
             OppOb = StripeBatchHandler.getAllCard(param,'card');
            
             //System.debug('==OppOb =>'+OppOb );
       if(OppOb != null  && OppOb.size() > 0 && !OppOb.isEmpty())
            for (Card__c op :OppOb ) {
                lstOpp.add(op);
                
            }
        }   
        //System.debug('===>'+lstOpp);
        if(lstOpp.size() > 0 && !lstOpp.isEmpty() ) {
            insert lstOpp;
            update stripeProfileList;
        }
         
        
        
    }
    global void finish(database.batchablecontext bc){
        
    }
}