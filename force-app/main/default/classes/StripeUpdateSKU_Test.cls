@isTest
public class StripeUpdateSKU_Test {
	 static testMethod void test() {
        List<Product2> prodList= new List<Product2>();
        Product2 p = new Product2();
        p.Name='test';
        p.Stripe_Product_Id__c= 'abc';
        p.Price__c= 100;
        p.IsActive=true;
        p.Length__c=20;
        p.Height__c=30;
        p.Weight__c =50;
        p.Width__c= 15;
        insert p;
         p.Stripe_SKU_Id__c='abd';
        prodList.add(p);
         update prodList;
        System.enqueuejob(new  StripeUpdateSKU(prodList));
        StripeUpdateSKU.updateSKU(p.Stripe_SKU_Id__c, 'usd','infinite', 100, true, 20, 30, 15, 50);
        //StripeUpdateSKU.updateSKU(p.Stripe_SKU_Id__c,'usd', 'infinite', p.Price__c, p.IsActive);
        StripeUpdateSKU.parse('{"object":"test","date":"test","end":"test","data":"test","currency":"test","type":"test","customer":"test","invoice":"test"}');
        
    }
}