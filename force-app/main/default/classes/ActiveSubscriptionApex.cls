//This class used wiht the component to show the active subscriptions
public class ActiveSubscriptionApex {
 @AuraEnabled
    public static List<Opportunity> getOpportunity(String contactId ){
        //Fetch alll the opportunities
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,CloseDate,Contact__c,Refund_Amount__c,Status__c    From Opportunity  where Contact__c=: contactId and Status__c!='canceled' and recordType.Name='Subscription' ];
       
        return lstOpp;
        
   }
    @AuraEnabled
    public static OpportunityLineItem getQuantity1(string oppIds){
        //Get all the Opp line items for the opportunity
        OpportunityLineItem oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: oppIds LIMIT 1 ];
        return oppData;
    }
    //Method to update the quantity on the subscription
     @AuraEnabled
    public static void getQuantityUpdate(Integer qty,String OppId){
        //String dropValue = String.ValueOf(dropVal);
        String OpprId = OppId;
        //String CurrentOppId = currOppId;
         //Get all the Opp line items for the opportunity
        OpportunityLineItem oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Opportunityid=: OpprId  LIMIT 1 ];
        HttpRequest http = new HttpRequest();  
        //SET THE end point URL
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppData.Subscription_Id__c);        
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        //Map of quantity
        Map<String, String> payload = new Map<String, String>{'quantity' =>String.ValueOf(qty)  };
        //set to the request bosy
        http.setBody(StripeUtil.urlify(payload));
       
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        //Send the request
        hs = con.send(http);
        //System.debug('-----'+statusCode );
        if (hs.getstatusCode() == 200){
            StripeListSubscriptionClone.Charges varCharges;
            JSONParser parse;
            system.debug('#### '+ 1);
            
            //Deserialize the JSO
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            oppData.Success_Failure_Message__c = String.valueOf(results.get('status'));
            
            oppData.Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity')));    
            //Upddates the quanityt and status  
            update oppData; 
            
            
        }
        
        system.debug('#### '+ hs.getBody());
        
    }
    
   
    //Mehtod to delete the subscription
    @AuraEnabled
    public static void  getOppData(String OppId){
    //get the oli's
        OpportunityLineItem oli = [select id,Subscription_Id__c from OPPORTUNITYLINEITEM WHERE Opportunityid =:OppId limit 1];
        
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli.Subscription_Id__c);
        //Delete method
        http.setMethod('DELETE');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        //Send the request
        hs = con.send(http);
        Integer statusCode = hs.getStatusCode();
        
        if (hs.getstatusCode() == 200){
            
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            
            oli.Success_Failure_Message__c = String.valueOf(results.get('status'));
            
            oli.Quantity     = Integer.ValueOf(String.valueOf(results.get('quantity'))); 
            //Update the oli with status     
            update oli; 
            
            
        }
    }
    
    
}