public class RPVDNCFurtherHandler {
    @future(callout = true)
    public Static void RpvDNCforMobile(list<String> Contactlist1,String nameofField) {
        if(!test.isRunningTest()){
        Boolean flag = false;
         Boolean flagM = false;
        list<Contact> contactList = new list<Contact> ();
        if(!Contactlist1.isEmpty()) {    
            contactList = [select id,DoNotCall,Mobile_State_DNC__c,MobilePhone,Phone_National_DNC__c,Phone_State_DNC__c,Phone,Other_Phone_National_DNC__c,Other_Phone_State_DNC__c,OtherPhone from contact where Id In:Contactlist1 ];
            
        }
        list<Contact> UpdateContactDNC = new list<Contact> ();
        List<Five9Credentials__c> Apikey=[select UserName__c,Password__c,RealPhoneValidationKey__c from Five9Credentials__c where isLive__c=:true];
        for(Contact con : contactList) {
            String MobilePhonenumber = ''; 
            String phoneNumber = '';
            String OtherphoneNumber = '';
            if(!Apikey.isEmpty() && Apikey != null) {
                Five9Credentials__c Tokens = Apikey[0];
                if(con.MobilePhone != null  && nameofField =='MobilePhone' ){
                    MobilePhonenumber =  con.MobilePhone.replaceAll('\\D','');
                    Http h = new Http();
                    HttpRequest req = new HttpRequest();
                    
                    req.setEndpoint('https://api.realvalidation.com/rpvWebService/DNCLookup.php?phone='+MobilePhonenumber+'&token='+Tokens.RealPhoneValidationKey__c);
                    req.setMethod('GET');
                    HttpResponse res = h.send(req);
                    Dom.Document doc = res.getBodyDocument(); 
                    System.debug('=================MobilePhone==============Response'+res.getBody());
                    //==================parse the response xml file and update contact ========================
                    Dom.XMLNode DncNode = doc.getRootElement();
                    String NationalDnc = DncNode.getChildElement('national_dnc', null).getText();
                    String stateDnc = DncNode.getChildElement('state_dnc', null).getText();
                    if(NationalDnc.equalsIgnoreCase('Y')){
                        con.DoNotCall = true;
                        flagM = con.DoNotCall;
                    }
                    if(stateDnc.equalsIgnoreCase('Y')){
                        con.Mobile_State_DNC__c=true;
                    }
                    
                    if(NationalDnc.equalsIgnoreCase('N')) {
                        con.DoNotCall=false;
                    }
                    if(stateDnc.equalsIgnoreCase('N')){
                        con.Mobile_State_DNC__c=false;
                    }
                    
                }
                if(con.Phone != null  && nameofField == 'Phone' ){
                    phoneNumber = con.Phone.replaceAll('\\D','');
                    Http h = new Http();
                    HttpRequest req1 = new HttpRequest();
                    
                    req1.setEndpoint('https://api.realvalidation.com/rpvWebService/DNCLookup.php?phone=8624323737&token='+Tokens.RealPhoneValidationKey__c);
                    req1.setMethod('GET');
                    HttpResponse res = h.send(req1);
                    Dom.Document doc = res.getBodyDocument(); 
                    System.debug('================Phone===============Response'+res.getBody());
                    //==================parse the response xml file and update contact ========================
                    Dom.XMLNode DncNode = doc.getRootElement();
                    String NationalDnc = DncNode.getChildElement('national_dnc', null).getText();
                    String stateDnc = DncNode.getChildElement('state_dnc', null).getText();               
                    if(NationalDnc.equalsIgnoreCase('Y')){
                        con.Phone_National_DNC__c=true;
                        flag = con.Phone_National_DNC__c;
                    } 
                    if(stateDnc.equalsIgnoreCase('Y')) { 
                        con.Phone_State_DNC__c = true;
                    }
                    
                    if(NationalDnc.equalsIgnoreCase('N')) {
                        con.Phone_National_DNC__c = false;
                    }
                    if(stateDnc.equalsIgnoreCase('N')) {
                        con.Phone_State_DNC__c = false;
                    }
                }
                if(con.OtherPhone != null && nameofField == 'OtherPhone' ){
                    OtherphoneNumber = con.OtherPhone.replaceAll('\\D','');
                    Http h = new Http();
                    HttpRequest req2 = new HttpRequest();
                    
                    req2.setEndpoint('https://api.realvalidation.com/rpvWebService/DNCLookup.php?phone='+OtherphoneNumber+'&token='+Tokens.RealPhoneValidationKey__c);
                    req2.setMethod('GET');
                    HttpResponse res = h.send(req2);
                    Dom.Document doc = res.getBodyDocument(); 
                    System.debug('===============OtherPhone================Response'+res.getBody());
                    //==================parse the response xml file and update contact ========================
                    Dom.XMLNode DncNode = doc.getRootElement();
                    String NationalDnc = DncNode.getChildElement('national_dnc', null).getText();
                    String stateDnc = DncNode.getChildElement('state_dnc', null).getText();
                    if(NationalDnc.equalsIgnoreCase('Y')){
                        con.Other_Phone_National_DNC__c=true;
                        flag=con.Other_Phone_National_DNC__c;
                    } 
                    if(stateDnc.equalsIgnoreCase('Y')) { 
                        con.Other_Phone_State_DNC__c=true;
                    }
                    
                    if(NationalDnc.equalsIgnoreCase('N')) {
                        con.Other_Phone_National_DNC__c=false;
                    }
                    if(stateDnc.equalsIgnoreCase('N')) {
                        con.Other_Phone_State_DNC__c=false;
                    }
                }
                
                UpdateContactDNC.add(con);
            }
        }
       update UpdateContactDNC; 
        System.debug('=================UpdateContactDNC=============='+UpdateContactDNC);
        if(flagM == true) {
          // System.enqueueJob(new Five9MobileValidationHelper(Contactlist1));
        }
       if(flag == true && Limits.getQueueableJobs() > 2 && Limits.getQueueableJobs()<4){
          //  System.enqueueJob(new Five9PhoneValidationHelper(Contactlist1));   
        } 
    }
    }
    public static void dummy(){
    integer i = 0;   
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
              
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
            }
 
}