@isTest
public class SetProductIdForStripePlanTriggerTest {
 	@IsTest
    static void methodName(){
        Test.startTest();
        Product2 pr = new Product2();
        pr.Name = 'test';
        pr.Stripe_Plan_Id__c = '1234';
        insert pr;
        
        Test.stopTest();
        
    }
    @IsTest
    static void methodName1(){
        Test.startTest();
        Product2 pr = new Product2();
        pr.Name = 'test';
        pr.Price__c = 1.00;
        pr.Interval__c = 'week';
        pr.Interval_Count__c = 2;
        pr.Subscription__c = true;
        insert pr;
        
        Test.stopTest();
        
    }
}