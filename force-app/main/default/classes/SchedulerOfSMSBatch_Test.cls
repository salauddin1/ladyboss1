@isTest
public class SchedulerOfSMSBatch_Test {
    @isTest
    public static void testschedule() {
        Campaign_Sms_Time__c cst = new Campaign_Sms_Time__c();
        cst.Name = 'cst Time';
        cst.Start_Hour__c = 0;
        cst.End_Hour__c = 23;
        insert cst;
        Campaign cam = new Campaign();
        cam.Name = 'sfsdd';
        insert cam;
        
        Contact c = new Contact();
        c.FirstName = 'asd';
        c.LastName = 'asdf';
        c.Phone = '3324234';
        c.MobilePhone = '3423453';
        c.AssistantPhone = '4453443';
        c.OtherPhone ='2353453';
        insert c;
        
        CampaignMember cm = new CampaignMember();
        cm.ContactId = c.id;
        cm.CampaignId = cam.id;
        insert cm;
        for(Integer i=0;i<40;i++){
            ScheduleSMS__c sds = new ScheduleSMS__c();
            sds.CampaignMemberId__c = cm.id;
            sds.MessageData__c = 'sdfsd';
            sds.Schedule_Time__c = Datetime.now();
            sds.IsSchedule__c = true;
            insert sds;
        }
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        SchedulerOfSMSBatch sh1 = new SchedulerOfSMSBatch();
        
        String sch = '0 0 23 * * ?'; 
        database.executebatch(sh1);
        sh1.execute(null);
        //system.schedule('Test Territory Check', sch, sh1); 
        
        Test.stopTest(); 
    }
    
}