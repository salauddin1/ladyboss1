@isTest
public class totalShippingCost_Test {
    
    public static testMethod void test(){
        Contact ct = new Contact();
        ct.LastName = 'test';
        insert ct;
        Product2 prod = new Product2();
        prod.Name='test';
        prod.Shipping_cost_1__c = 2;
        prod.Available_For_Profiles_For_Search__c = 'System Admin';
        insert prod;
        Product2 prod1 = new Product2();
        prod1.Name='test';
        prod1.Shipping_cost_1__c = 2;
        prod1.Switch_To_Product_four_month__c=prod.id;
        insert prod1;
        List<Id> prodList = new List<Id>();
        prodList.add(prod.id);
        prodList.add(prod1.id);
        Shipping_Cost_Table__c spingcst = new Shipping_Cost_Table__c();
        spingcst.Product_Id__c=prod1.Id;
        insert spingcst;
       
        Map<ID,String> oomap = new Map<ID,String>();
        oomap.put(prod1.Id, String.valueOf(prod1));
        test.startTest();
        totalShippingCost tsc = new totalShippingCost();
        totalShippingCost.getCost(prodList);
        totalShippingCost.createShipping(prodList, ct.id);
        totalShippingCost.getShippingCost(prodList);
        
        List<Product2> prodListForshippingTable = new List<Product2>();
        prodListForshippingTable.add(prod);
        prodListForshippingTable.add(prod1);
        
        List<Shipping_Cost_Table__c> shipingcst  = new List<Shipping_Cost_Table__c>();
        Shipping_Cost_Table__c spingcst2 = new Shipping_Cost_Table__c();
        spingcst2.Product_Id__c=prod1.Id;
        spingcst2.Customer_Id__c = '1avzssd';
        shipingcst.add(spingcst2);
        
        totalShippingCost.addShippingTable(9995.00,prodListForshippingTable,oomap,shipingcst,ct.id);
        test.stopTest();
    }
    
    public static testMethod void test2(){
        Contact ct = new Contact();
        ct.LastName = 'test';
        insert ct;
        Product2 prod = new Product2();
        prod.Name='test';
        prod.Shipping_cost_1__c = 2;
        prod.Available_For_Profiles_For_Search__c = 'System Admin';
        insert prod;
        Product2 prod1 = new Product2();
        prod1.Name='test';
        prod1.Shipping_cost_1__c = 2;
        prod1.Switch_To_Product_four_month__c=prod.id;
        prod1.Available_For_Profiles_For_Search__c = 'System Admin';
        insert prod1;
        List<Id> prodList = new List<Id>();
        prodList.add(prod.id);
        prodList.add(prod1.id);
        Shipping_Cost_Table__c spingcst = new Shipping_Cost_Table__c();
        spingcst.Product_Id__c=prod1.Id;
        insert spingcst;
       
        Map<ID,String> oomap = new Map<ID,String>();
        oomap.put(prod1.Id, String.valueOf(prod1));
        test.startTest();
        totalShippingCost tsc = new totalShippingCost();
        totalShippingCost.getCost(prodList);
        totalShippingCost.createShipping(prodList, ct.id);
        totalShippingCost.getShippingCost(prodList);
        
        List<Product2> prodListForshippingTable = new List<Product2>();
        prodListForshippingTable.add(prod);
        prodListForshippingTable.add(prod1);
        
        List<Shipping_Cost_Table__c> shipingcst  = new List<Shipping_Cost_Table__c>();
        Shipping_Cost_Table__c spingcst2 = new Shipping_Cost_Table__c();
        spingcst2.Product_Id__c=prod1.Id;
        spingcst2.Customer_Id__c = '1avzssd';
        shipingcst.add(spingcst2);
        
        totalShippingCost.addShippingTable(9995.00,prodListForshippingTable,oomap,shipingcst,ct.id);
        test.stopTest();
    }
}