public class RefundInternalReasonReport {
  @AuraEnabled
    public static List<Opportunity> getOpportunity(String contactId,String reasonFilter ){
        
         List<Opportunity> lstOppNew ;
        List<opportunity> oppList = new List<opportunity>();
        if(reasonFilter == 'All'){
               lstOppNew = [select id,Name,Card__c,createddate,Secondary_Refund_Amount__c ,Secondary_Stripe_Charge_Id__c,amount,Refund_Amount__c,Stripe_Charge_Id__c,Subscription__c,Contact__c,Refund_Internal_Reason__c, (SELECT Id, Subscription_Id__c FROM OpportunityLineItems)  from opportunity where  Contact__c =:contactId  and (Refund_Amount__c!= 0 and  Refund_Amount__c!= null ) and Refund_Internal_Reason__c!=null and Stripe_Charge_Id__c != null];
        }else {
            lstOppNew = [select id,Name,Card__c,createddate,Secondary_Refund_Amount__c ,Secondary_Stripe_Charge_Id__c,amount,Refund_Amount__c,Stripe_Charge_Id__c,Subscription__c,Contact__c,Refund_Internal_Reason__c, (SELECT Id, Subscription_Id__c FROM OpportunityLineItems)  from opportunity where  Contact__c =:contactId  and (Refund_Amount__c!= 0 and  Refund_Amount__c!= null ) and Refund_Internal_Reason__c!=null and Stripe_Charge_Id__c != null and Refund_Internal_Reason__c=:reasonFilter];
        }
        
        return lstOppNew ;
    }
    @AuraEnabled
    public static List<OpportunityLineItem > getOpportunityLineItem(String contactId ,String reasonFilter){
        
       List<OpportunityLineItem> oppLineItem= new List<OpportunityLineItem>();
        List<OpportunityLineItem> oppLine1 ; 
        if(reasonFilter == 'All'){
       
            oppLine1 = [SELECT Id,Name,Refund_Internal_Reason__c,Opportunity.Clubbed__c,createddate,Refunded_Formula__c,Refunded_date__c,UnitPrice,refund__c, Created_Date__c, Product2.name,OpportunityId, Quantity,Success_Failure_Message__c, Subscription_Id__c FROM OpportunityLineItem  where Opportunity.contact__c =:contactId and Refund_Internal_Reason__c!=null  order by Refunded_Formula__c desc];
        }else {
        
            oppLine1 = [SELECT Id,Name,Refund_Internal_Reason__c,Opportunity.Clubbed__c,createddate,Refunded_Formula__c,Refunded_date__c,UnitPrice,refund__c, Created_Date__c, Product2.name,OpportunityId, Quantity,Success_Failure_Message__c, Subscription_Id__c FROM OpportunityLineItem  where Opportunity.contact__c =:contactId and Refund_Internal_Reason__c!=null  and Refund_Internal_Reason__c=:reasonFilter order by Refunded_Formula__c desc];
        
        }
        for(OpportunityLineItem op : oppLine1 ) {
            if(op.refund__c > 0 || op.Success_Failure_Message__c=='Charge has already been refunded' || op.Success_Failure_Message__c=='charge.refunded') {
                oppLineItem.add(op);
            }
        }
        return oppLineItem;
    }
     
     
}