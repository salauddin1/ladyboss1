public class CardDeleteController {
    @AuraEnabled
    public static List<Card__c> getCards(String contactId ){
        return OrderFormControllerChanges.cardListServer(contactId);
        //return [SELECT Brand__c,Card_ID__c,Card_Type__c,Contact__c,Credit_Card_Number__c,CustomerID__c,Cvc_Check__c,Cvc__c,Error_Message__c,isBatchCard__c,Last4__c,Stripe_Card_Id__c,Stripe_Profile__c FROM Card__c where Contact__c =: contactId and Stripe_Profile__c != null and CustomerID__c != null and Stripe_Card_Id__c != null];
    }
    
    @AuraEnabled
    public static List<OpportunityLineItem > getCardsOpportunity(String contactId ){
        List<card__c> cardList1 = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: contactId and Error_Message__c != 'Your card number is incorrect.'];
        Set<Id> cardSet = new Set<ID>();
        for(card__c c : cardList1 ) {
            cardSet.add(c.id);
        }
        List<OpportunityLineItem > lstOpp = [SELECT Id, Product2.name,Opportunity.Card__c  ,OpportunityId, Name FROM OpportunityLineItem where Opportunity.Card__c IN : cardSet  and  Opportunity.recordType.developername = 'Subscription'  and   (Opportunity.Success_Failure_Message__c !='customer.subscription.deleted' and Opportunity.Success_Failure_Message__c !='canceled' and OpportunityLineItem.Success_Failure_Message__c !='customer.subscription.deleted' and OpportunityLineItem.Success_Failure_Message__c !='canceled')];
        return lstOpp ;
    }
    
    @AuraEnabled
    public static String deleteCardFromSaleforce(Card__c singleCard  ){
        try{
            delete singleCard;
            return 'Card Deleted';
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
    
    @AuraEnabled
    public static String deleteCard(Card__c singleCard ){
        try{
            System.debug('-------> '+singleCard);
            String returnMessage = '';
            if(singleCard.Stripe_Card_Id__c != null ){
                system.debug('--->'+singleCard.Stripe_Profile__r.Stripe_customer_Id__c);
                if(singleCard.Stripe_Profile__r.Stripe_customer_Id__c != null){
                    StripeCustomer scustomer = StripeCustomer.getCustomer(singleCard.Stripe_Profile__r.Stripe_customer_Id__c);
                    if(scustomer.sources.total_count !=  null && scustomer.sources.total_count > 0){
                        if(scustomer.subscriptions.total_count >  0){
                            if(scustomer.sources.total_count > 1){
                                Integer cardCount = 0;
                                for(StripeCard sc : scustomer.sources.StripeData){
                                    if(sc.stripeObject == 'card'){
                                        cardCount++;
                                    }
                                }
                                if(cardCount > 1){
                                    StripeCard scard = StripeCard.deleteCard(singleCard.Stripe_Profile__r.Stripe_customer_Id__c, singleCard.Stripe_Card_Id__c); 
                                    
                                    if(scard.Id==null && scard.error.message!=null ){
                                        returnMessage = scard.error.message;
                                        if(scard.error.code != null && scard.error.code != ''){
                                            returnMessage += '('+scard.error.code+')';
                                        }
                                    }else if(scard.deleted != null && scard.deleted == true){
                                        returnMessage = 'Card Deleted';
                                        CardDeleteController.deleteCardFromSaleforce(singleCard);
                                    }else{
                                        returnMessage = 'Something went wrong.';
                                    }
                                }else{
                                    //singleCard.addError('Record cannot be deleted');
                                    returnMessage = 'Record cannot be deleted';
                                }
                            }
                        }else{
                            StripeCard scard = StripeCard.deleteCard(singleCard.Stripe_Profile__r.Stripe_customer_Id__c, singleCard.Stripe_Card_Id__c); 
                            
                            if(scard.Id==null && scard.error.message!=null ){
                                returnMessage = scard.error.message;
                                if(scard.error.code != null && scard.error.code != ''){
                                    returnMessage += '('+scard.error.code+')';
                                }
                            }else if(scard.deleted != null && scard.deleted == true){
                                returnMessage = 'Card Deleted';
                                CardDeleteController.deleteCardFromSaleforce(singleCard);
                            }else{
                                returnMessage = 'Something went wrong.';
                            }
                        }
                    }else{
                        returnMessage = 'No such source : '+singleCard.Stripe_Card_Id__c;
                    }
                }else{
                    returnMessage = 'No Stripe Card Id Found.';
                }
            }else{
                returnMessage = 'No Stripe Card Id Found.';
            }
            
            return returnMessage;
            
        }catch(Exception ex){
            return ex.getMessage();
        }
        
    }
    public static void dummy(){
        integer i=0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
}