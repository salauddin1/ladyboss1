@isTest 
public class InvoiceToChargeUpdateHandler_Test {
    
    static testMethod void testinvoice() 
    { 
        ContactTriggerFlag.isContactBatchRunning=true;
        contact con = new contact();
        con.lastName='test';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='123455';
        sp.Customer__c =  con.id;
        insert sp;
        
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        
        Map<String,Opportunity> oomap = new Map<String,Opportunity>();
        Map<String,List<OpportunityLineItem>> olmap = new Map<String,List<OpportunityLineItem>>();
        List<OpportunityLineItem> oplinelist = new List<OpportunityLineItem>();
        List<Opportunity> oplist = new List<Opportunity>();
        
        opportunity op  = new opportunity();
        
        op.name='opcl';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.Clubbed__c = true;
        
        op.CloseDate = System.today();
        insert op;
        op.Opportunity_Unique_Name__c = op.id;
        update op;
        
        oplist.add(op);
        
        oomap.put('club', op);
        //insert oomap;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Unique_Name__c = op.Id;
        ol.Subscription_Id__c='sub_sdsdd23';
        ol.Number_Of_Payments__c =0;
        insert ol;
        
        Invoice__c inv = new Invoice__c();
        inv.Charge_Id__c ='dsdsd';
        inv.Subscription_Id__c ='sub_sdsdd23';
        inv.Invoice_Id__c = 'sdsds';
        insert inv;
        
        oplinelist.add(ol);
        
        olmap.put('club', oplinelist);
        
        set<id> idset = new set<id>();
        idset.add(op.id);
        test.StartTest();
        Test.setMock(HttpCalloutMock.class, new StripeGetCard_MockTest());
        InvoiceToChargeUpdateHandler.OliUpdater(oplinelist);
        InvoiceToChargeUpdateHandler.updateStartDate(oplineList);
        
        OliUpdateTriggerFlag.stopTrigger=true;
        test.stopTest();

}
}