@IsTest
public class StripeCustomer_Test {
    
    static testMethod void testParse() {
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        StripeCustomer sc = new StripeCustomer();
        StripeCustomer.create('test','test','test','test@test.com','90900099','1234');
        StripeCustomer.create('test','test','test','test@test.com','test','90900099','1234');
        StripeCustomer.getCustomer('test');
        StripeCustomer.cancelSubscription('test');
        Map<String, String> metadata = new Map<String, String>();
        metadata.put('test', 'test');
        StripeCustomer.updateSubscription('test', 'test', Date.valueOf(Date.Today()), metadata, null);
        StripeCustomer.updateSubscription('customerId', 'plan', 'Name', 'billingAddress', 'test@test.com','0099008','');
        StripeCustomer.updateSubscription('customerId', 'plan', 'Name', 'billingAddress', 'test@test.com','0099008','','','','','','','');
        StripeCustomer.updateSubscription('customerId', 'String plan','String Name','String billingAddress','String email', 'String phone', 'String contactId','String MailingStreet','String MailingCity','String MailingState','String MailingPostalCode','String MailingCountry','String salesPerson',0);
        StripeCustomer.updateSubscription('customerId','test', 'plan', 'Name', 'billingAddress', 'test@test.com','0099008','','','','','','','',null);
        StripeCustomer.updateSubscription('customerId', prod, 'Name', 'billingAddress', 'test@test.com','0099008','','','','','','','',null);
        StripeCustomer.getCustomers(12, 1);
        StripeCustomer.getCustomer('test');
        StripeCustomer.createCardWithTokenCUS('token', 'FullName', 'email', 'billingAddress', 'phone', 'conId');  
    }
}