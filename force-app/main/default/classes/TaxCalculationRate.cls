global class TaxCalculationRate  {
    global Double totalTax;
    global String code;
    global lines[] lines;

   
    
    public static TaxCalculationRate parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (TaxCalculationRate) System.JSON.deserialize(json, TaxCalculationRate.class);
    }
    global class lines {
        global String lineNumber;   //1
        global Double tax;
    }
    public static TaxCalculationRate ratemethod(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode){
       String lines='';
        //system.debug('shippingcountry'+shippingCountry);
        Avalara__c av = Avalara__c.getInstance();
        HttpRequest http = new HttpRequest();
        http.setEndpoint( 'https://rest.avatax.com/api/v2/transactions/create');
        http.setMethod('POST');
        http.setHeader('content-type', 'application/json');
        http.setHeader('Authorization', 'Basic '+av.API_Key__c);
        /*(Integer i=0; i<quantity.size();i++){
            if(i==0){
                lines='{"number": "'+i+1+'","quantity": "'+quantity[i]+'","amount": "'+amount[i]*100+'", "taxcode": "'+taxCode[i]+'"}';
            }else{
                lines+=',{"number": "'+i+1+'","quantity": "'+quantity[i]+'","amount": "'+amount[i]*100+'", "taxcode": "'+taxCode[i]+'"}';
            }
        }*/
        system.debug(lines);
        string body='{"lines": [{"number": "1","quantity": "1","amount": "'+amount*quantity+'", "taxcode": "'+taxCode+'"}],"type": "SalesInvoice","companyCode": "'+av.Company_Code__c+'","date": "'+date.today().format()+'","customerCode": "ABC","addresses": {"singleLocation": {' +
            + '"line1": "'+line1+'",'+
            '"city": "'+city+'",'+
            '"region": "'+region+'",'+
            '"country": "'+country+'",'+
            '"postalCode": "'+postalCode+'"}},'+
            '"commit": false,"currencyCode": "USD"}';
            system.debug('Anydatatype_msg'+body);
        http.setBody(body);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!test.isRunningTest()){
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
               system.debug('e.getMessage()-->'+e.getMessage());
            }
        }else{
            hs.setStatusCode(200);
        }
        if(!test.isRunningTest()){
            response = hs.getBody();
            system.debug(response);
        }else{
            response='{"id":8630151704,"code":"853529cd-a320-4548-ab86-3dbf5945293f","companyId":218115,"date":"2019-02-04","status":"Saved","type":"SalesInvoice","batchCode":"","currencyCode":"USD","customerUsageType":"","entityUseCode":"","customerVendorCode":"ABC","customerCode":"ABC","exemptNo":"","reconciled":false,"locationCode":"","reportingLocationCode":"","purchaseOrderNo":"","referenceCode":"","salespersonCode":"","taxOverrideType":"None","taxOverrideAmount":0,"taxOverrideReason":"","totalAmount":100,"totalExempt":0,"totalDiscount":0,"totalTax":7.75,"totalTaxable":100,"totalTaxCalculated":7.75,"adjustmentReason":"NotAdjusted","adjustmentDescription":"","locked":false,"region":"CA","country":"US","version":1,"softwareVersion":"19.2.0.39","originAddressId":13892590310,"destinationAddressId":13892590310,"exchangeRateEffectiveDate":"2019-02-04","exchangeRate":1,"isSellerImporterOfRecord":false,"description":"","email":"","businessIdentificationNo":"","modifiedDate":"2019-02-28T12:29:45.273","modifiedUserId":247336,"taxDate":"2019-02-04T00:00:00"}';   
        }
        statusCode = hs.getStatusCode();
       
        try {
            TaxCalculationRate o = TaxCalculationRate.parse(response);
            TaxCalculationRate.voidTransaction(o.code);
            
            return o;
           
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
         
    }
    @Future(callout=true)
    public static void setTransaction(String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode, String planId,String chargeId, String prodName, Decimal shippingCost,String createdUsing){
       String lines='';
        system.debug('shippingCost'+shippingCost);
        Avalara__c av = Avalara__c.getInstance();
        HttpRequest http = new HttpRequest();
        http.setEndpoint( 'https://rest.avatax.com/api/v2/transactions/create');
        http.setMethod('POST');
        http.setHeader('content-type', 'application/json');
        http.setHeader('Authorization', 'Basic '+av.API_Key__c);
        if(createdUsing=='V10'){
            lines ='{"number": "1","quantity": "1","amount": "'+amount*quantity+'", "taxcode": "'+taxCode+'","itemCode":"'+planId+'","description":"'+prodName+'"},{"number": "2","quantity": "1","amount": "'+shippingCost+'","itemCode":"Dynamic_shipping","description":"Shipping costs"}';
        }else{
            lines ='{"number": "1","quantity": "1","amount": "'+amount*quantity+'", "taxcode": "'+taxCode+'","itemCode":"'+planId+'","description":"'+prodName+'"}';
        }
        system.debug(lines);
        string body='{"lines": ['+lines+'],"type": "SalesInvoice","code":"'+chargeId+'","companyCode": "'+av.Company_Code__c+'","date": "'+date.today().format()+'","customerCode": "'+chargeId+'","purchaseOrderNo":"'+chargeId+'","addresses": {"shipTo": {' +
            + '"line1": "'+line1+'",'+
            '"city": "'+city+'",'+
            '"region": "'+region+'",'+
            '"country": "'+country+'",'+
            '"postalCode": "'+postalCode+'"},"shipFrom": { "line1": "10010 Indian School Rd. NE", "city": "Albuquerque", "region": "NM", "country": "US", "postalCode": "87112"  }},'+
            '"commit": true,"currencyCode": "USD"}';
            system.debug('Anydatatype_msg'+body);
        http.setBody(body);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!test.isRunningTest()){
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
               system.debug('e.getMessage()-->'+e.getMessage());
            }
        }else{
            hs.setStatusCode(200);
        }
        if(!test.isRunningTest()){
            response = hs.getBody();
            system.debug(response);
        }else{
            response='{"id":8630151704,"code":"853529cd-a320-4548-ab86-3dbf5945293f","companyId":218115,"date":"2019-02-04","status":"Saved","type":"SalesInvoice","batchCode":"","currencyCode":"USD","customerUsageType":"","entityUseCode":"","customerVendorCode":"ABC","customerCode":"ABC","exemptNo":"","reconciled":false,"locationCode":"","reportingLocationCode":"","purchaseOrderNo":"","referenceCode":"","salespersonCode":"","taxOverrideType":"None","taxOverrideAmount":0,"taxOverrideReason":"","totalAmount":100,"totalExempt":0,"totalDiscount":0,"totalTax":7.75,"totalTaxable":100,"totalTaxCalculated":7.75,"adjustmentReason":"NotAdjusted","adjustmentDescription":"","locked":false,"region":"CA","country":"US","version":1,"softwareVersion":"19.2.0.39","originAddressId":13892590310,"destinationAddressId":13892590310,"exchangeRateEffectiveDate":"2019-02-04","exchangeRate":1,"isSellerImporterOfRecord":false,"description":"","email":"","businessIdentificationNo":"","modifiedDate":"2019-02-28T12:29:45.273","modifiedUserId":247336,"taxDate":"2019-02-04T00:00:00"}';   
        }
        statusCode = hs.getStatusCode();
       
        try {
            TaxCalculationRate o = TaxCalculationRate.parse(response);
            
            
            
           
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
         
    }
    
    public static void voidTransaction(String transactionCode){
       String lines='';
        
        Avalara__c av = Avalara__c.getInstance();
        HttpRequest http = new HttpRequest();
        http.setEndpoint( 'https://rest.avatax.com/api/v2/companies/'+av.Company_Code__c+'/transactions/'+transactionCode+'/void');
        http.setMethod('POST');
        http.setHeader('content-type', 'application/json');
        http.setHeader('Authorization', 'Basic '+av.API_Key__c);
        
        system.debug(lines);
        string body='{  "code": "DocVoided" }';
            system.debug('Anydatatype_msg'+body);
        http.setBody(body);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!test.isRunningTest()){
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
               system.debug('e.getMessage()-->'+e.getMessage());
            }
        }else{
            hs.setStatusCode(200);
        }
        if(!test.isRunningTest()){
            response = hs.getBody();
            system.debug(response);
        }else{
            response='{"id":8630151704,"code":"853529cd-a320-4548-ab86-3dbf5945293f","companyId":218115,"date":"2019-02-04","status":"Saved","type":"SalesInvoice","batchCode":"","currencyCode":"USD","customerUsageType":"","entityUseCode":"","customerVendorCode":"ABC","customerCode":"ABC","exemptNo":"","reconciled":false,"locationCode":"","reportingLocationCode":"","purchaseOrderNo":"","referenceCode":"","salespersonCode":"","taxOverrideType":"None","taxOverrideAmount":0,"taxOverrideReason":"","totalAmount":100,"totalExempt":0,"totalDiscount":0,"totalTax":7.75,"totalTaxable":100,"totalTaxCalculated":7.75,"adjustmentReason":"NotAdjusted","adjustmentDescription":"","locked":false,"region":"CA","country":"US","version":1,"softwareVersion":"19.2.0.39","originAddressId":13892590310,"destinationAddressId":13892590310,"exchangeRateEffectiveDate":"2019-02-04","exchangeRate":1,"isSellerImporterOfRecord":false,"description":"","email":"","businessIdentificationNo":"","modifiedDate":"2019-02-28T12:29:45.273","modifiedUserId":247336,"taxDate":"2019-02-04T00:00:00"}';   
        }
        statusCode = hs.getStatusCode();
       
        try {
            TaxCalculationRate ol = TaxCalculationRate.parse(response);
            
            
            
           
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
         
    }

}