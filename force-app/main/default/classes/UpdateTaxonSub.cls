global class UpdateTaxonSub implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    private List<String> subList = new List<String>();
    private List<String> prodIDList = new List<String>();
    global String query;
    global UpdateTaxonSub(){

    }
    global UpdateTaxonSub(List<String> IdList,String type){
        if (type == 'SubscriptionID') {
            this.subList = IdList;
        }else {
            this.prodIDList = IdList;
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        if (subList.size()>0) {
            query = 'SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c,totalPrice,Add_Tax_to_subscription__c FROM OpportunityLineItem where Subscription_Id__c in : subList AND Opportunity.Contact__c != null AND Status__c != \'Inactive\' AND Opportunity.RecordType.Name = \'Subscription\' AND Tax_Percentage_s__c = null AND Subscription_Id__c != null  AND Product2.Tax_code__c != null AND Product2.Tax_code__c != \'99999\'';
        }else if(prodIDList.size()>0){
            query = 'SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c,totalPrice,Add_Tax_to_subscription__c FROM OpportunityLineItem where Product2ID in : prodIDList AND Opportunity.Contact__c != null AND Status__c != \'Inactive\' AND Opportunity.RecordType.Name = \'Subscription\' AND Tax_Percentage_s__c = null AND Subscription_Id__c != null  AND Product2.Tax_code__c != null AND Product2.Tax_code__c != \'99999\'';
        }else {
            query ='SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c,totalPrice,Add_Tax_to_subscription__c FROM OpportunityLineItem where Opportunity.Contact__c != null AND Status__c != \'Inactive\' AND Opportunity.RecordType.Name = \'Subscription\' AND Tax_Percentage_s__c = null AND Subscription_Id__c != null  AND Product2.Tax_code__c != null AND Product2.Tax_code__c != \'99999\'';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        String recordId = '';
        try{
            Map<String, Id> subConMap = new Map<String, Id>();
            Map<String, Id> subProdMap = new Map<String, Id>();
            Map<Id, Address__c> addMap = new Map<Id, Address__c>();
            Map<Id, Card__c> cardMap = new Map<Id, Card__c>();
            Map<String, OpportunityLineItem> opliMap = new Map<String, OpportunityLineItem>();
            Map<Id, String> taxCode = new Map<Id, String>();
            for (OpportunityLineItem opli: scope) {
                subConMap.put(opli.Subscription_Id__c,opli.Opportunity.Contact__c);
                subProdMap.put(opli.Subscription_Id__c,opli.Product2Id);
                opliMap.put(opli.Subscription_Id__c, opli);
            }
            if (subConMap.size()>0) {
                for (Address__c add: [SELECT id, Shipping_City__c, Shipping_Street__c, Shipping_Country__c, Shipping_State_Province__c, Contact__c, Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c IN :subConMap.values()]) {
                    addMap.put(add.Contact__c, add);
                } 
                for (Card__c add: [SELECT id, Billing_City__c, Billing_Street__c, Billing_Country__c, Billing_State_Province__c, Contact__c, Billing_Zip_Postal_Code__c FROM Card__c WHERE Contact__c IN :subConMap.values()]) {
                    cardMap.put(add.Contact__c, add);
                } 
                for (Product2 prod: [SELECT id,Tax_Code__c FROM Product2 WHERE id IN : subProdMap.values() AND Tax_Code__c!=null]) {
                    taxCode.put(prod.Id, prod.Tax_Code__c);
                }
                for (String sub: subConMap.keySet()) {
                    String street = 'LadyBoss Weight Loss 10010 Indian School Rd. NE';
                    String state = 'NM';
                    String city = 'Albuquerque';
                    String country = 'US';
                    String zipcode = '87112';
                    if (addMap.containsKey(subConMap.get(sub))) {
                        street = addMap.get(subConMap.get(sub)).Shipping_Street__c;
                        state = addMap.get(subConMap.get(sub)).Shipping_State_Province__c;
                        city = addMap.get(subConMap.get(sub)).Shipping_City__c;
                        country = addMap.get(subConMap.get(sub)).Shipping_Country__c;
                        zipcode = addMap.get(subConMap.get(sub)).Shipping_Zip_Postal_Code__c;
                    }else if (cardMap.containsKey(subConMap.get(sub)) && cardMap.get(subConMap.get(sub)).Billing_Street__c != null && cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c != null && cardMap.get(subConMap.get(sub)).Billing_City__c != null && cardMap.get(subConMap.get(sub)).Billing_State_Province__c != null && cardMap.get(subConMap.get(sub)).Billing_Country__c != null) {
                        street =  cardMap.get(subConMap.get(sub)).Billing_Street__c;
                        state = cardMap.get(subConMap.get(sub)).Billing_State_Province__c;
                        city = cardMap.get(subConMap.get(sub)).Billing_City__c;
                        country = cardMap.get(subConMap.get(sub)).Billing_Country__c;
                        zipcode = cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c;
                    }
                    String codetax;
                    if (taxCode.get(subProdMap.get(sub)) == null) {
                        codetax = '99999';
                    }else {
                        codetax = taxCode.get(subProdMap.get(sub));
                    }
                    Decimal amount = opliMap.get(sub).TotalPrice; 
                    TaxjarTaxCalculate tx = TaxjarTaxCalculate.ratemethod(street, city, state, country, zipcode, amount, 1, codetax); 
                    Decimal taxPercentage = tx.tax.rate*100;
                    Subscriptions.updateSubscriptionForBatch(sub,taxPercentage);
                    opliMap.get(sub).Tax_Percentage_s__c = taxPercentage;
                    opliMap.get(sub).Tax_Amount__c = tx.tax.amount_to_collect;

                    
                }
            }
            
            update opliMap.values();
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
        }catch (Exception e) {
            system.debug('e.getMessage()-->'+e.getMessage());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'UpdateTaxonSub',
                    'Batch',
                    recordId,
                    e
                )
            );
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}