@isTest
public class StripeListSubscriptionClone_Test {
    public static testMethod void test(){
        StripeListSubscriptionClone st = new StripeListSubscriptionClone();
        String json = '{'+
		'    \"object\": \"list\",'+
		'    \"data\": ['+
		'        {'+
		'            \"id\": \"test2\",'+
		'            \"object\": \"plan\",'+
		'            \"amount\": 2655,'+
		'            \"created\": 1508947122,'+
		'            \"currency\": \"usd\",'+
		'            \"interval\": \"week\",'+
		'            \"interval_count\": 2,'+
		'            \"livemode\": false,'+
        '            \"has_more\": true,'+
        '            \"nickname\":\"test\",'+
		'            \"metadata\": {},'+
		'            \"name\": \"Test Product 2\",'+
		'            \"statement_descriptor\": null,'+
		'            \"trial_period_days\": null'+
		'        }'+
		'    ],'+
		'    \"has_more\": true,'+
		'    \"url\": \"/v1/plans\"'+
		'}';
         StripeListSubscriptionClone.Plan objPlan = new StripeListSubscriptionClone.Plan(System.JSON.createParser(json));
        System.assert(objPlan != null);
		//System.assert(objPlan.id == null);
		//System.assert(objPlan.amount == null);
		//System.assert(objPlan.nickname == null);
	
        json = '{ \"object\": \"list\", \"data\": [ { \"id\": \"ch_1Bnu6XDiFnu7hVq7FZC78Hpr\", \"object\": \"charge\", \"amount\": 2655, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1Bnu6XDiFnu7hVq7vBzOqKyf\", \"captured\": true, \"created\": 1516822817, \"currency\": \"usd\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"description\": null, \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": {}, \"invoice\": \"in_1BntAHDiFnu7hVq7qGHrvQKO\", \"livemode\": false, \"metadata\": {}, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1Bnu6XDiFnu7hVq7FZC78Hpr/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BYf1vDiFnu7hVq7kXdTknjq\", \"object\": \"card\", \"address_city\": \"\", \"address_country\": \"United States\", \"address_line1\": \"\", \"address_line1_check\": null, \"address_line2\": null, \"address_state\": \"\", \"address_zip\": \"\", \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 2, \"exp_year\": 2022, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": {}, \"name\": \"Grant Stripe\", \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null }], \"has_more\": true, \"url\": \"/v1/charges\" }';
		StripeListCharge.Charges r = StripeListCharge.Charges.parse(json);
          StripeListSubscriptionClone.Charges.parse(json);

		System.assert(r != null);
        //System.assert(ch != null);
		//System.assert(ch.object_Z == null);
		//System.assert(ch.data == null);
		//System.assert(ch.has_more == null);
		//System.assert(ch.url == null);
		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		
        StripeListSubscriptionClone.Data objData = new StripeListSubscriptionClone.Data(System.JSON.createParser(json));
        System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_Z == null);
		System.assert(objData.amount == null);
		System.assert(objData.amount_refunded == null);
		System.assert(objData.balance_transaction == null);
		System.assert(objData.captured == null);
		System.assert(objData.created == null);
		System.assert(objData.currency_data == null);
		System.assert(objData.customer == null);
		System.assert(objData.description == null);
		System.assert(objData.quantity == null);
		System.assert(objData.invoice == null);
        System.assert(objData.livemode == null);
        System.assert(objData.plan == null);
        System.assert(objData.paid == null);
        System.assert(objData.items == null);
         System.assert(objData.statement_descriptor == null);
         System.assert(objData.status == null);
      	json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		
        StripeListSubscriptionClone.Item objItem = new StripeListSubscriptionClone.Item(System.JSON.createParser(json));
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		
        StripeListSubscriptionClone.Data_x objDatax = new StripeListSubscriptionClone.Data_x(System.JSON.createParser(json));
        System.assert(objDatax != null);
		System.assert(objDatax.id == null);
		System.assert(objDatax.object_Z == null);
		System.assert(objDatax.created == null);
		System.assert(objDatax.plan == null);
		System.assert(objDatax.quantity == null);
		System.assert(objDatax.subscription == null);
		//json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		
      	
        
        
    }
}