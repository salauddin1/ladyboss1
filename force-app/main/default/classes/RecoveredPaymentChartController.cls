// Created By   : Sourabh Badole
// Created Date : 13-03-2019
public class RecoveredPaymentChartController {   
    public Decimal amount{get;set;}
    public Decimal dollarsRecovered{get;set;}
    
    public Decimal revenueFailed{get;set;}
    public String strSelectStudentOption    {get;set;}
    public List<SelectOption> Status{get;set;}
    
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public List<userAmount> user_amount{get;set;}
    public Boolean showOpp{get;set;}
    public RecoveredPaymentChartController (){
        startDate = System.today().toStartOfWeek().addDays(1);
        endDate = System.Today();
        
    }    
    public list<SelectOption> getStudOption(){
        List<User> userList = [SELECT Id,Name, Username, IsActive FROM User where IsActive =true];
        list<SelectOption>  lstOptions = new list<SelectOption>();
        lstOptions.add(new SelectOption('All', 'All'));
        for(User  getEm : userList ){
            lstOptions.add(new SelectOption(getEm.UserName, getEm.Name));
        }
        return lstOptions;
    }
    public void getData() {
        Integer intAmount =0;
        revenueFailed=0;
        String dynamic_query;
        system.debug('strSelectStudentOption '+strSelectStudentOption );
        
        if(strSelectStudentOption =='All' || strSelectStudentOption ==Null){
            dynamic_query = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
        }else{
            dynamic_query = 'select owner.Name,SUM(Failed_Amount__c)aver  from case where owner.userName=:strSelectStudentOption  and createdDate>= : startDate AND createdDate<=: endDate and (Product_Category__c IN (\'Supplement\',\'UTA\',\'Unlimited\') and isProcessedForReport__c=true and IsFailed_Payment__c=true and Charge_Id__c!=null and Subject LIKE \'Failed Payment -%\'and Failed_Amount__c!=null) GROUP BY owner.Name';
        }
        
        revenueFailed=0;
        dollarsRecovered=0;
        
        List<Stripe_Product_Mapping__c> msppingList =[select id,Salesforce__c,Salesforce_Product__c,Stripe__c,Stripe_Nick_Name__c from Stripe_Product_Mapping__c where salesforce__c IN ('Supplement','UTA','Unlimited') ];
        Map<String,String> sfDescMap = new Map<String,String>();
        Map<String,String> sfDescNickMap= new Map<String,String>();
        Map<String,String> sfDescNickMap1= new Map<String,String>();
        
        Set<String> sfSet = new  Set<String>();
        Set<String> sfNickSet = new  Set<String>();
        Set<String> failedCaseChargeId = new Set<String> ();
        Map<String,Case > failedCaseChargeMap = new Map<String,Case > ();
        
        for (Stripe_Product_Mapping__c mp : msppingList ) {
            sfDescMap.put(mp.Salesforce_Product__c,mp.Salesforce__c);
            sfSet.add(mp.Stripe__c);
            sfNickSet.add(mp.Stripe_Nick_Name__c);
                    System.debug('==Stripe_Product_Mapping__c ==='+mp);
            System.debug('==Stripe_Nick_Name__c ==='+mp.Stripe_Nick_Name__c);
                }
        List<Opportunity> oppList ;
        if(strSelectStudentOption =='All' || strSelectStudentOption ==Null){
            oppList = [select id,Card__c,Owner.userName,Owner.Name,name,amount,Card__r.isUpdated__c,Card__r.isCreatedNew__c,(select id,UnitPrice,opportunityId,opportunity.Owner.userName,opportunity.Owner.Name,product2.name from opportunityLineitems)  from Opportunity where createdDate>= : startDate AND createdDate<=: endDate and Card__c!=null and (Card__r.isUpdated__c=true OR Card__r.isCreatedNew__c=true)];
        }
        else{
            oppList = [select id,Card__c,name,Owner.userName,Owner.Name,amount,Card__r.isUpdated__c,Card__r.isCreatedNew__c,(select id,UnitPrice,opportunityId,product2.name,opportunity.Owner.userName ,opportunity.Owner.Name from opportunityLineitems)  from Opportunity where owner.userName=:strSelectStudentOption  and createdDate>= : startDate AND createdDate<=: endDate and Card__c!=null and (Card__r.isUpdated__c=true OR Card__r.isCreatedNew__c=true)];
        }
        System.debug('==oppList ==='+oppList.size() );
        Map<string,Integer> userMap = new Map<string,Integer>();
        
        for (Opportunity opp : oppList ) {
            if(opp.opportunityLineitems.size()>0) {
                For(opportunityLineitem b : opp.opportunityLineitems){
                    
                    System.debug('==oppList ==='+sfSet.contains(b.product2.name) );
                    if(sfSet.contains(b.product2.name) || sfNickSet.contains(b.product2.name)) {
                        dollarsRecovered = dollarsRecovered+Integer.valueOf(b.UnitPrice) ;
                        
                        if(userMap.containsKey(opp.owner.Name)) {
                            intAmount = 0;
                            intAmount = Integer.valueOf(userMap.get(b.opportunity.owner.Name));
                            userMap.remove(b.opportunity.owner.Name);
                            intAmount = intAmount + Integer.valueOf(opp.Amount);
                            userMap.put(b.opportunity.owner.Name,intAmount);
                            
                        }else{
                            intAmount = 0;
                            intAmount = Integer.valueOf(opp.Amount);
                            userMap.put(b.opportunity.owner.Name,intAmount);
                            
                        }
                    }
                }
            }else{
                System.debug('==oppList ==='+sfNickSet );
                System.debug('==oppList ==='+opp.name);
                System.debug('==oppList ==='+sfNickSet.contains(opp.name) );
                if(sfNickSet.contains(opp.name) || sfSet.contains(opp.name)) {
                    dollarsRecovered = dollarsRecovered+Integer.valueOf(opp.Amount) ;
                    
                    if(userMap.containsKey(opp.owner.Name)) {
                        intAmount = userMap.get(opp.owner.Name);
                        
                        userMap.remove(opp.owner.userName);
                        intAmount = intAmount + Integer.valueOf(opp.Amount);
                        userMap.put(opp.owner.Name,intAmount);
                        
                    }else{
                        intAmount = 0;
                        intAmount =Integer.valueOf(opp.Amount);
                        userMap.put(opp.owner.Name,intAmount);
                        
                    }
                }
            }
        }
        amount =0;
        AggregateResult[] groupedResults = Database.query(dynamic_query); 
        this.user_amount = new List<userAmount>();
        if(groupedResults.size()==0){
            showOpp = false;
        }
        else{
            showOpp = true;
        }
        System.debug('==userMap==='+userMap);
        
        for(AggregateResult res : groupedResults){
            userAmount ua;
            if(res.get('aver')!=null && res.get('Name')!=null){
                amount = amount + Decimal.valueOf(String.valueOf(res.get('aver')));
                Decimal checkvalue = 0;
                if(Decimal.valueOf(userMap.get(String.valueOf(res.get('Name')))) != null ){
                    checkvalue = Decimal.valueOf(userMap.get(String.valueOf(res.get('Name')))) ;
                }
                ua = new userAmount(String.valueOf(res.get('Name')),Decimal.valueOf(String.valueOf(res.get('aver'))),checkvalue);
            }else{
                amount = amount +0;
                if(res.get('Name') != null){
                    if(userMap.containsKey(String.valueOf(res.get('Name')))) {
                        ua = new userAmount(String.valueOf(res.get('Name')),0,Decimal.valueOf(userMap.get(String.valueOf(res.get('Name')))));
                    }else {
                        ua = new userAmount(String.valueOf(res.get('Name')),0,0);
                    }
                }
            } 
            
            user_amount.add(ua);
        }
        //To get the failed all payments from the Stripe
    }   
    
    public class userAmount{
        public string userName{get;set;}
        public decimal totalAmount{get;set;}
        public decimal recoverdAmount  {get;set;}
        
        public userAmount(string userName,decimal totalAmount,decimal  recoverdAmount  ){
            this.userName = userName;
            this.totalAmount = totalAmount;
            this.recoverdAmount  =recoverdAmount  ;
        }
        
    }
    
}