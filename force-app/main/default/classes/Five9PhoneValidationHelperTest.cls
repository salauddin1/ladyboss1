@isTest
public class Five9PhoneValidationHelperTest {
    @isTest
    public Static void MytestUnit1(){
         Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        List<String> Conlist=new List<String>();
        Contact con =new Contact();
        con.FirstName='AshishTest';
        con.LastName='sharma';
        con.MobilePhone='1234566777';
        con.Phone='7473467363';
        Insert con;
        Conlist.add(con.Id);
        Five9Credentials__c credentials=new Five9Credentials__c();
        credentials.UserName__c='Ashish@Username';
        credentials.Password__c='Ashish@1234';
        credentials.RealPhoneValidationKey__c='sdgye322egdt3rt32121372e';
        credentials.IsLive__c=true;
        insert credentials;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        System.enqueueJob(new Five9MobileValidationHelper(Conlist));
        Test.stopTest();
    }
    @isTest
    public Static void MytestUnit2(){
         Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        List<String> Conlist2=new List<String>();
        Contact con =new Contact();
        con.FirstName='AshishTest';
        con.LastName='sharma';
        con.MobilePhone='1234566777';
        con.Phone='7473467363';
        Insert con;
        Conlist2.add(con.Id);
        Five9Credentials__c credentials=new Five9Credentials__c();
        credentials.UserName__c='Ashish@Username';
        credentials.Password__c='Ashish@1234';
        credentials.RealPhoneValidationKey__c='sdgye322egdt3rt32121372e';
        credentials.IsLive__c=true;
        insert credentials;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        System.enqueueJob(new Five9PhoneValidationHelper(Conlist2));
        Test.stopTest();
    }
}