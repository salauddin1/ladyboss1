/*
 * Developer Name : Hiranmay Joshi
 * Description : This batch moves campaign members to "Live Challenge Prospect Campaign" from the campaigns whose name is defined in the custom setting "Campaign Name List".There are Purchase window dates and calling window dates are defined on "live challenge campaign itself". 
 * This batch only moves those campaignmembers whose last purchased date is between the purchase window dates defined on "Live Challange". And it moves campaignmembers only on specific dates which false between Calling window dates on "Live Challenge Campaign"
 */
global class LiveChallangeCasmpaignMoveBatch implements Database.Batchable<sObject>,Schedulable {
    global void execute(SchedulableContext ctx) {
        LiveChallangeCasmpaignMoveBatch cb = new LiveChallangeCasmpaignMoveBatch();
        Database.executeBatch(cb);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date CurrDate = Date.today();
        return Database.getQueryLocator([SELECT Calling_End_Date__c,Calling_Start_Date__c,Live_Challenge_Campaign__c,Purchase_End_Date__c,Purchase_Start_Date__c FROM Campaign WHERE Live_Challenge_Campaign__c=true AND Calling_Start_Date__c <=: CurrDate  AND Calling_End_Date__c>=:CurrDate LIMIT 1]);
    }
    global void execute(Database.BatchableContext bc, List<Campaign> records){
        try{
            Campaign Live_camp = records.get(0);
            Date Purchase_Start_Date = Live_camp.Purchase_Start_Date__c;
            Date Purchase_End_Date = Live_camp.Purchase_End_Date__c;
            Date Calling_Start_Date = Live_camp.Calling_Start_Date__c;
            Date Calling_End_Date = Live_camp.Calling_End_Date__c;
            List<CampaignMember> existMembersList = [SELECT Id,ContactId FROM CampaignMember WHERE CampaignId =: Live_camp.Id AND ContactId!=null];
            List<Id> existContIdList = new List<Id>();
            for(CampaignMember cmp : existMembersList){
                existContIdList.add(cmp.ContactId);
            } 
            List<Campaign_Name_List__c> cmpList = [SELECT CamapignNames__c FROM Campaign_Name_List__c LIMIT 1];
            String campaigns = cmpList.get(0).CamapignNames__c;
            List<String> cmpNamelist = campaigns.split(',');
            List<CampaignMember> cmpMemList = [SELECT Id,ContactId,Created_Date__c FROM CampaignMember WHERE Campaign.Name in : cmpNamelist AND Contact.First_website_sell__C=true AND Created_Date__c>=: Purchase_Start_Date AND Created_Date__c <=: Purchase_End_Date AND ContactId!=null];
            Set<Id> cntIdList = new Set<Id>();
            for(CampaignMember cmp : cmpMemList){
                if(!existContIdList.contains(cmp.ContactId)){
                    cntIdList.add(cmp.ContactId);
                }
            }
            List<CampaignMember> memberToAddList = new List<CampaignMember>();
            for(Id cntId : cntIdList){
                CampaignMember cm = new CampaignMember();
                cm.ContactId = cntId;
                cm.CampaignId = Live_camp.Id;
                memberToAddList.add(cm);
            }
            insert memberToAddList;
            delete cmpMemList;
            if(Test.isRunningTest()){
            System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('LiveChallangeCasmpaignMoveBatch','Batch',String.valueOf(records.get(0).id),e) );
        }
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}