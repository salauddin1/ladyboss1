/*
 * Description : This Serivce creates cases and subsequent tasks for failed stripe payments. If Case already exists than it creates subsequent tasks for failed charges otherwise it will create case and subsequent task both.
 */
@RestResource(urlMapping='/Stripe_FailedChargeService')
global class StripeFailedCharge_Service {
    public StripeFailedCharge_Service() {
    }
    @HttpPost
    global static void upsertCase() {
        string chargeIde;
        if(RestContext.request.requestBody!=null){
            try {
                String str                              =   RestContext.request.requestBody.toString();
                System.debug('-------raw res----'+RestContext.response);
                System.debug('-------res----'+str);
                Map<String, Object> results             =   (Map<String, Object>)JSON.deserializeUntyped(str);
                List<Map<String, Object>> data          =   new List<Map<String, Object>>();
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('data');
                Map<String, Object> aryCustomers        =   (Map<String, Object>) lstCustomers.get('object');
                Map<String, Object>  metaDataMap        =   (Map<String, Object> )aryCustomers.get('metadata');
                String invoice                          =   (String)aryCustomers.get('invoice');
                String customerId                       =   String.valueOf(aryCustomers .get('customer')); 
                Map<String, Object> dataObj             =   (Map<String, Object>) aryCustomers.get('plan');
                Map<String, Object> sourceMap           =   (Map<String, Object>) aryCustomers .get('source');
                String cardId                    = '';
                if(sourceMap != null && sourceMap.keySet() != null && sourceMap.keySet().contains('id') && String.valueOf(sourceMap.get('id')) != null &&  String.valueOf(sourceMap.get('id')) != ''){
                    cardId       =   String.valueOf(sourceMap.get('id')) ;
                }
                System.debug('cardId   ======'+cardId );
                String chargeId                         =   String.valueOf(aryCustomers .get('id'));
                String invoiceID                        =   String.valueOf(aryCustomers .get('invoice'));
                chargeIde=String.valueOf(aryCustomers .get('id'));
                String stateMentdesc                    =   String.valueOf(aryCustomers .get('statement_descriptor'));
                String descriptiondesc        = '';
                if(aryCustomers != null && aryCustomers.keySet() != null && aryCustomers.keySet().contains('description') && String.valueOf(aryCustomers .get('description')) != null &&  String.valueOf(aryCustomers .get('description')) != ''){
                	descriptiondesc        =   String.valueOf(aryCustomers .get('description'));
                }
                String failure_message                  =   String.valueOf(aryCustomers .get('failure_message'));
                String amt = String.ValueOf(aryCustomers.get('amount'));
                Map<String,Object> bill_details = (Map<String,Object>)aryCustomers.get('billing_details');
                Map<String,Object> addr = (Map<String,Object>)bill_details.get('address');
                String city = (String)addr.get('city');
                String country = (String)addr.get('country');
                String street='';
                if((String)addr.get('line1')!=null){
                    street = (String)addr.get('line1');
                }
                if((String)addr.get('line2')!=null){
                    street = street +  (String)addr.get('line2');
                }
                String line1 = (String)addr.get('line1');
                String line2 = (String)addr.get('line2');
                String postal_code = (String)addr.get('postal_code');
                String state = (String)addr.get('state');
                String email = (String)bill_details.get('email');
                String name = (String)bill_details.get('name');
                String phone = (String)bill_details.get('phone');
                System.debug(email+''+name+' '+city+' '+state);
                List<Stripe_Product_Mapping__c> lstStripeProd = [select id,Salesforce__c,Stripe__c from Stripe_Product_Mapping__c where Stripe__c  =: stateMentdesc];
                System.debug('------lstStripeProd-----'+lstStripeProd);
                Case_User__c cUser = Case_User__c.getInstance();
                String cUserId = cUser.UserId__c;
                String subID = Invoices.getSubID(invoiceID);
                List<Stripe_Profile__c> lstStripeProf   = [ select id,Customer__c,(select id from Cards__r where Stripe_Card_Id__c=:cardId  ) from Stripe_Profile__c  where Stripe_Customer_Id__c =:customerId  limit 1];
                System.debug('------lstStripeProf-----'+lstStripeProf);
                List<opportunity> lstOpportunity = new List<Opportunity>();
                List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
                if(subID != null && subID != '' && subID != 'null'){
                    lstOpportunityLineItems = [SELECT Id,Stripe_charge_id__c,opportunityId FROM OpportunityLineItem where Subscription_Id__c =: subID limit 1];
                    System.debug('------lstOpportunityLineItems-----'+lstOpportunityLineItems);
                }
                if(lstOpportunityLineItems.size() > 0){
                    lstOpportunity  = [ SELECT id ,RequestId__c,Scheduled_Payment_Date__c,Stripe_Charge_Id__c,card__c FROM opportunity where Subscription__c  =: subID OR id =: lstOpportunityLineItems[0].opportunityId];
                    System.debug('----lstOpportunity---'+lstOpportunity);
                }
                
                String phoneNumber = '';
                String emailAddress= '';
                String fullNameVal= '';

                List<User> uName =  [select username,id from User where id =: cUserId];
                System.debug('------uName------'+uName);
                
                if(results.get('type') == 'charge.failed'){
                System.debug('----result is chsrge failed------'+results.get('type'));
                    if(lstStripeProf.size() > 0 && lstStripeProf[0].Customer__c != null){
                    System.debug('----lstStripeProf----lstStripeProf.customer != null-----');
                        List<Case> caseList = new List<Case>();
                        if(metaDataMap != null && !(metaDataMap.isEmpty()) ){
                        System.debug('-----metaDataMap--------'+metaDataMap);
                            phoneNumber = (metaDataMap.get('Phone') != null && metaDataMap.get('Phone') != 'null') ? ((String)metaDataMap.get('Phone')) : '';
                            emailAddress = (metaDataMap.get('Email') != null && metaDataMap.get('Email') != 'null') ? ((String)metaDataMap.get('Email')) : '';
                            fullNameVal = (metaDataMap.get('Full Name') != null && metaDataMap.get('Full Name') != 'null') ? ((String)metaDataMap.get('Full Name')) : '';
                        }else{
                            List<Contact> conlist = [SELECT Id,Email,FirstName,LastName,Phone from Contact where id =: lstStripeProf[0].Customer__c];
                            System.debug('----conlist else ----'+conlist);
                            if(conlist.size() > 0){
                            System.debug('------conlist.size()-------'+conlist.size());
                                phoneNumber = conlist[0].Phone; 
                                emailAddress = conlist[0].Email; 
                                fullNameVal = conlist[0].firstname +' '+conlist[0].lastname;
                            }
                        }
                        
                        decimal checkAmount = decimal.valueOf(amt)*0.01;
                        Datetime checkDate = system.today().addDays(-16);
                        Boolean checkfirst = false ;
                        if(descriptiondesc   != null && descriptiondesc  != '' && descriptiondesc!='null'){
                        System.debug('--------descriptiondesc-------'+descriptiondesc);
                            if(checkAmount != null && checkAmount != 0 && emailAddress != null && emailAddress != ''  ){
                            System.debug('-----checkAmount-----'+checkAmount);
                                if( ( descriptiondesc.contains('Payment for invoice') || descriptiondesc.contains('Subscription creation') || descriptiondesc.contains('Invoice') ) && !descriptiondesc.contains('-0001')){
                                System.debug('----descriptiondesc----');
                                    checkfirst =true; 
                                    caseList = [select id,Charge_Id__c,Statement_Descriptor__c,Subject,Opportunity__c,Invoice_Id__c,Failed_Amount__c,ContactEmail from Case where Failed_Amount__c =: checkAmount and Failed_Amount__c != null  and ContactEmail =: emailAddress and ContactEmail != null and CreatedDate >: checkDate and ( Subject  like : '%Payment for invoice%' or Subject  like : '%Invoice%' or Subject  like : '%Subscription creation%' ) ];
                                }
                            }
                        }
                        if(checkfirst == true){
                        System.debug('------checkfirst------'+checkfirst);
                            if(caseList.size() <= 0){   
                            System.debug('------caseList------'+caseList);
                                Case cs = new Case();
                                cs.OwnerId = uName[0].id;
                                cs.IsFailed_Payment__c = true;
                                cs.DueDate__c = system.today().addDays(2);
                                cs.Charge_Id__c = chargeId;
                                cs.Invoice_Id__c = invoice;
                                cs.Subject = 'Failed payment - $'+decimal.valueOf(amt)*0.01+' | '+fullNameVal+' | '+emailAddress+' | '+phoneNumber+' | '+failure_message + ((descriptiondesc!=null && descriptiondesc!='null' && descriptiondesc!='') ? (' | ' + descriptiondesc) : '');
                                cs.ContactId = lstStripeProf[0].Customer__c;
                                cs.Stripe_Customer_Id__c = customerId;
                                cs.Statement_Descriptor__c  = stateMentdesc ;
                                cs.Failed_Amount__c = decimal.valueOf(amt)*0.01;
                                cs.UpdateCardStatus__c= 'Payment Failed';
                                cs.Stripe_Card_Id__c = cardId ;
                                cs.Last_4_Digit__c = string.valueOf(sourceMap.get('last4'));
                                cs.Failure_Reason__c = string.valueOf(aryCustomers.get('failure_code'));
                                System.debug('cardId   ======'+cardId );
                                if(cardId  != null && cardId != '' ){
                                System.debug('---------cardId--------'+cardId);
                                    List<Card__c> cardList = new List<Card__c>();
                                    cardList  = [select id from Card__c where  Stripe_Card_Id__c =: cardId and Stripe_Card_Id__c != null order by LastModifieddate desc];
                                    System.debug('-------cardList  ======'+cardList  );
                                    if(cardList    != null && !cardList.isEmpty() ){
                                    System.debug('------cardList if------');
                                        cs.Card__c = cardList[0].id;
                                    }
                                }
                                
                                if(lstOpportunity.size() > 0 ){
                                System.debug('----lstOpportunity---'+lstOpportunity);
                                    cs.Opportunity__c = lstOpportunity[0].id;
                                }
                                Boolean checkAccRec = false ;
                                if(stateMentdesc != null && stateMentdesc != ''){
                                System.debug('-----stateMentdesc-----'+stateMentdesc);
                                    List<User> userId = new List<User>();
                                    userId =  [select id, Email,  profile.name, Username , isActive FROM User where ( profile.name = 'LadyBoss Coaching Admin' AND isActive = true ) limit 1];
                                    System.debug('----------userId--------'+userId);
                                    List<Stripe_Coaching_Product__c> prodLIst  = new List<Stripe_Coaching_Product__c>();
                                    prodLIst = Stripe_Coaching_Product__c.getall().values();
                                    Set<String> CoachprodName = new Set<String>();
                                    if(prodLIst != null && !prodLIst.isEmpty()){
                                    System.debug('------prodLIst-----'+prodLIst);
                                        for(Stripe_Coaching_Product__c stCoach : prodLIst){
                                            if(stCoach.name != null){
                                            System.debug('-------stCoach.name-----'+stCoach.name);
                                                CoachprodName.add(stCoach.name);    
                                            }
                                        }
                                    }
                                    if(CoachprodName != null && !CoachprodName.isEmpty() ){
                                    System.debug('-------CoachprodName-------'+CoachprodName);
                                        if(CoachprodName.contains(stateMentdesc)){
                                        System.debug('------CoachprodName contain stateement descriptor-------');
                                            if(userId != null && !userId.isEmpty() && userId.size() > 0){
                                            System.debug('------userId------'+userId);
                                                cs.OwnerId = userId[0].id;   
                                                cs.Is_Coaching_Failed__c = true ;
                                                checkAccRec = true ;     
                                            }
                                        }
                                    }
                                }
                                if( checkAccRec != true ){
                                System.debug('------checkAccRec-----'+checkAccRec);
                                    cs.type = 'Account Recovery';  
                                }
                                insert cs;
                                System.debug('------Case cs-----'+cs);
                                if(lstStripeProf.size() > 0 && lstStripeProf[0].Customer__c != null)  {
                                System.debug('------lstStripeProf-----'+lstStripeProf);
                                    Contact ct = new Contact();
                                    ct.id = lstStripeProf[0].Customer__c;
                                    ct.Account_recovery_link__c  = 'https://ladyboss-support.secure.force.com/UpdateCardCloneCase?id='+customerId+'&source=email&CaseId='+cs.id;
                                    update ct;
                                }
                                
                                
                                Subsequent_Task__c subtask = new Subsequent_Task__c();
                                subtask.Charge_Id__c = chargeId;
                                subtask.Case_id__c = cs.id;
                                subtask.DueDate__c = system.today().addDays(7);
                                subtask.Invoice_Id__c = invoice;
                                subtask.failure_message__c = failure_message;
                                subtask.Amount__c=decimal.valueOf(amt)*0.01;
                                subtask.name__c = name;
                                subtask.email__c = email;
                                subtask.phone__c = phone;
                                subtask.country__c = country;
                                subtask.state__c = state;
                                subtask.postal_code__c = postal_code;
                                subtask.street__c = street;
                                subtask.city__c = city;
                                subtask.state__c = state;
                                subtask.postal_code__c = postal_code;
                                subtask.type__c = (String)results.get('type');
                                insert subtask;
                                System.debug('------subtask-----'+subtask);
                                
                            }else{
                            System.debug('-------checkAccRec else-------');
                                Subsequent_Task__c subtask = new Subsequent_Task__c();
                                subtask.Charge_Id__c = chargeId;
                                subtask.Case_id__c = caseList[0].id;
                                subtask.DueDate__c = system.today().addDays(7);
                                subtask.failure_message__c = failure_message;
                                subtask.Invoice_Id__c = invoice;
                                subtask.Amount__c=decimal.valueOf(amt)*0.01;
                                subtask.name__c = name;
                                subtask.email__c = email;
                                subtask.phone__c = phone;
                                subtask.country__c = country;
                                subtask.state__c = state;
                                subtask.postal_code__c = postal_code;
                                subtask.street__c = street;
                                subtask.city__c = city;
                                subtask.state__c = state;
                                subtask.postal_code__c = postal_code;
                                subtask.type__c = (String)results.get('type');
                                insert subtask;
                                System.debug('------subtask-----'+subtask);
                                
                            }
                        }
                    }
                }
                
            }
            catch(Exception ex) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                System.debug('----e---'+ex.getStackTraceString());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeFailedCharge_Service',
                        'upsertTask',
                        chargeIde,
                        ex
                    )
                );
            }
        }
    }
}