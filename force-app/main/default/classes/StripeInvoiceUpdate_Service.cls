@RestResource(urlMapping='/StripeInvoiceUpdate_Service')
global class StripeInvoiceUpdate_Service {
    @HttpPost
    global static void updateInvoice() {
        string ChargeId;
        if(RestContext.request.requestBody!=null){
            try {
                String str = RestContext.request.requestBody.toString();
                System.debug('-------firsttttt twooo----StripeInvoice_ServiceDemo'+str);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                ChargeId                         = String.valueOf(results.get('id')); 
                Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('data');
                Map<String, Object>  lstCustomers2 = (Map<String, Object> )lstCustomers.get('object');
                Map<String, Object>  lstCustomers3 = (Map<String, Object> )lstCustomers2.get('lines');
                List<Object> data = (List<Object>)lstCustomers3.get('data');
                List<Map<String,Object>> dataList = new List<Map<String, Object>>();
                String customer = (String)lstCustomers2.get('customer');
                Set<String> subId = new Set<String>();
                for(Object ob : data){
                    Map<String, Object> obmap = (Map<String, Object>)ob;
                    subId.add(String.valueof(obmap.get('id')));
                    System.debug('-------res----data--'+obmap.get('id'));
                    dataList.add(obmap);
                }
                Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
                Map<Id,String> prodSubscription = new Map<Id,String>();
                System.debug('-------res----data--'+subId);
                List<id> productIds = new List<Id>();
                List<id> oppId  = new List<id>();
                List<OpportunityLineItem> oppLineItemList = [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Created_Using__c FROM OpportunityLineItem where Subscription_Id__c in : subId AND Opportunity.Created_Using__c = 'V10'];
                system.debug('oppLineItemList  '+oppLineItemList);
                List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
                if(oppLineItemList.size()>0){
                	for(OpportunityLineItem oli:oppLineItemList){
                        system.debug(oli.Opportunity.Created_Using__c);
                        
	                	List<OpportunityLineItem> oppLineItemList1 = [SELECT id,Product2Id,Subscription_Id__c,OpportunityId FROM OpportunityLineItem where OpportunityId = : oli.OpportunityId];
	                	system.debug('oppLineItemList1  '+oppLineItemList1);
	                	if(oppLineItemList1.size()>0 ){
		                	for(OpportunityLineItem opli:oppLineItemList1){
		                		productIds.add(opli.Product2Id);
		                		prodSubscription.put(opli.Product2Id, opli.Subscription_Id__c);
		                	}	
		                	prodShip = totalShippingCost.getShippingCost(productIds);
		                	for(Id prodId:prodShip.keySet()){
		                		if(oli.Product2Id == prodId){
		                			StripeCreateInvoiceItem.createInvoiceItem('Shipping Cost',customer,'usd',prodShip.get(prodId),prodSubscription.get(prodId));//Adding shipping cost to the subscription
		                			oli.Shipping_Costs__c = prodShip.get(prodId);
                                    oliToUpdate.add(oli);
		                			if(oli.Tax_Percentage_s__c!=0 && prodShip.get(prodId)!=0){
		                				Decimal a = -((prodShip.get(prodId)*oli.Tax_Percentage_s__c)/100);
		                				StripeCreateInvoiceItem.createInvoiceItem('Tax on Shipping',customer,'usd',a,prodSubscription.get(prodId));//Subtracting the tax on shipping from the overall amount
		                			}
		                			
		                		}
		                	}
		                } 
		            }
                }
                if(oliToUpdate.size()>0){
                    update oliToUpdate;
                }          
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeInvoiceUpdate_Service',
                 'updateInvoice',
                 ChargeId
                 )
                 );               
                if(Test.isRunningTest())  
                    integer intTest =1/0;
            }
            catch(Exception e) {
                RestResponse res = RestContext.response; 
            res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeInvoiceUpdate_Service',
                        'updateInvoice',
                        ChargeId,
                        e
                    )
                );
            }
        }
        
    }
    
}