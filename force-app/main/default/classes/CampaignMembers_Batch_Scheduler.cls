global class CampaignMembers_Batch_Scheduler implements Schedulable {
     global void execute(SchedulableContext ctx){
      CampaignMembers_Batch batch = new CampaignMembers_Batch();
      Database.executebatch(batch,20);
    }

}