@isTest
public class CustomerSentimentScore_Test{
static testMethod void myUnitTest() {
     test.startTest();
        contact con = new contact();
    con.lastname = 'est';
    insert con;
    //Insert test case record
        Case c = new Case();
        c.Subject = 'Om Test';  
       c.One_Click_Survey__c= 'Excellent';
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
    c.contactid = con.id;
        insert c;
    Case c1 = new Case();
        c1.Subject = 'Om Test';  
       c1.One_Click_Survey__c= 'Great';
        c1.Status ='New';
        c1.Priority = 'Medium';
        c1.Origin = 'Email';
    c1.contactid = con.id;
        insert c1;
    Case c2 = new Case();
        c2.Subject = 'Om Test';  
       c2.One_Click_Survey__c= 'Poor';
        c2.Status ='New';
        c2.Priority = 'Medium';
        c2.Origin = 'Email';
    c2.contactid = con.id;
        insert c2;
    Case c3 = new Case();
        c3.Subject = 'Om Test';  
       c3.One_Click_Survey__c= 'Ok';
        c3.Status ='New';
        c3.Priority = 'Medium';
        c3.Origin = 'Email';
    c3.contactid = con.id;
        insert c3;
    
         ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.Customer_Sentiment_Score;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        CustomerSentimentScore recPayCon = new CustomerSentimentScore(sc);
    test.stopTest();
     }
     static testMethod void myUnitTest2() {
          test.startTest();
         contact con = new contact();
    con.lastname = 'est1';
    insert con;
        //Insert test case record
        Case c = new Case();
        c.Subject = 'Om Test';  
       c.One_Click_Survey__c= 'Ok';
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
         c.contactid = con.id;
        insert c;
         ApexPages.StandardController sc = new ApexPages.StandardController(c);
        PageReference pageRef = Page.Customer_Sentiment_Score;
        pageRef.getParameters().put('id', String.valueOf(c.Id));
        Test.setCurrentPage(pageRef);
        
        CustomerSentimentScore recPayCon = new CustomerSentimentScore(sc);
         test.stopTest();
     }
     static testMethod void myUnitTest3() {
         contact con = new contact();
    con.lastname = 'est3';
    insert con;
        //Insert test case record
        
         test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.Customer_Sentiment_Score;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        CustomerSentimentScore recPayCon = new CustomerSentimentScore(sc);
         test.stopTest();
     }
     static testMethod void myUnitTest4() {
         test.startTest();
         contact con = new contact();
    con.lastname = 'est5';
    insert con;
        //Insert test case record
        Case c = new Case();
        c.Subject = 'Om Test';  
       c.One_Click_Survey__c= 'Great';
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
         c.contactid = con.id;
        insert c;
         ApexPages.StandardController sc = new ApexPages.StandardController(c);
        PageReference pageRef = Page.Customer_Sentiment_Score;
        pageRef.getParameters().put('id', String.valueOf(c.Id));
        Test.setCurrentPage(pageRef);
        
        CustomerSentimentScore recPayCon = new CustomerSentimentScore(sc);
         test.stopTest();
     }
}