/*
 * Developer Name : Tirth Patel
 * Description : this batch finds duplicates based on email ids, contact with maximum dups (same email id) will be send first 
 */
public class BatchFindDuplicates implements  Database.Batchable<AggregateResult>,Schedulable  {
    List<Contact_Merge_Manage__c> runMergeJob = [SELECT run_contact_merge__c FROM Contact_Merge_Manage__c LIMIT 1];
    public Iterable<AggregateResult> start(Database.BatchableContext bc) {
        return new AggregateResultIterable('SELECT count(id), email contactEmail from contact where email != null  and email!=\'notavail@gmail.com\' group by email having count(id) > 1 order by count(id) desc limit 100');
    }
    
    public void execute(SchedulableContext SC) {
        List<AsyncApexJob> syncJobs = [SELECT ApexClass.name,Status FROM AsyncApexJob where ApexClass.name in ('BatchFindDuplicates','ContactMergeBatch') AND Status in ('Holding','Queued','Preparing','Processing') AND JobType!='ScheduledApex'];
            if(syncJobs.size()==0 && runMergeJob.size()==1 && runMergeJob.get(0).run_contact_merge__c){
                BatchFindDuplicates btc = new BatchFindDuplicates(); 
                Database.executeBatch(btc);
            }
    } 
    
    public void execute(Database.BatchableContext bc, List<Sobject> lstSo){
        
        List<String> lstEmail = new List<String>();
        for(sObject sObj : lstSo) {
            AggregateResult ar = (AggregateResult)sObj;
            lstEmail.add((String)ar.get('contactEmail'));
        }
        List<Contact> lstContacts = [select id,Is_Master__c,email from Contact where Email in :lstEmail order by createdDate asc ];
        Set<String> setEmails = new Set<String>();
        LIst<Contact> lstCntUpdate = new List<Contact>();
        for(Contact cnt : lstContacts)  {
            if(setEmails.add(cnt.Email.toLowerCase()))  {
                cnt.check_for_dup__c = true;
                if(!cnt.Is_Master__c){
                	cnt.Is_Master__c = true;
                }
                lstCntUpdate.add(cnt);   
            }  
        } 
        update lstCntUpdate;
        
    }
    
    public void finish(Database.BatchableContext bc){ 
        /*
        List<Contact> contList = [Select id from Contact Where check_for_dup__c=true];
        List<Business_Hours__c> businessHrs = [SELECT Start_Hour__c,End_Hour__c,is_main__c FROM Business_Hours__c];
        Boolean runBatch=false;
        for(Business_Hours__c bh : businessHrs){
            if(bh.is_main__c && (System.now().hour() >= bh.Start_Hour__c || System.now().hour() < bh.End_Hour__c)){
                runBatch = true;
            }
            if(!bh.is_main__c && System.now().hour()>= bh.Start_Hour__c &&  System.now().hour() < bh.End_Hour__c){
                runBatch = true;
            }
        }
		*/
        // execute any post-processing operations
        List<Contact> contList = [Select id from Contact Where check_for_dup__c=true];
        if(!test.isRunningTest() && contList.size()>0 && runMergeJob.size() == 1 && runMergeJob.get(0).run_contact_merge__c){
            ContactMergeBatch cmb = new ContactMergeBatch(UserInfo.getSessionId());
            Database.executeBatch(cmb,1);
        }	
    }  
}