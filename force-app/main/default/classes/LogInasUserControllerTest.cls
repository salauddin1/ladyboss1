@isTest
public  class LogInasUserControllerTest {
    @IsTest
    static void methodName(){
        ContactTriggerFlag.isContactBatchRunning=true;
        PageReference pageRef = Page.Redirecttohub;
        Test.setCurrentPage(pageRef);

        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Site';
        con.Email = 'test@site.com';
        con.Username__c = 'username1';
        Blob key =  crypto.generateAesKey(256);
        con.Site_Crypto_Key__c = EncodingUtil.base64Encode(key).substring(0, 32);
        con.Password__c = 'test12345';
        con.Site_Password_Reset__c = true;
        con.Phone = '78569854126';
        con.Hub_Customer_Age__c = system.today();
        insert con;
        pageRef.getParameters().put('conid',con.Id);
        pageRef.getParameters().put('conname','con.Name');
        Test.startTest();
        LogInasUserController.getLoginDetails(con.Id);
        LogInasUserController controller = new LogInasUserController();
        controller.createCookies();
        Test.stopTest();
        
    }
}