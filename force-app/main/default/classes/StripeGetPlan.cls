global class StripeGetPlan {
	global String id;
    global String nickname;
    global String product;
    global String statement_description;
    global String interval; // 'week', 'month' or 'year'
    global Integer created;
    global Integer amount;
    global String stripeCurrency;
    global Integer interval_count;
    global Integer trial_period_days;
    global Map<String, String> metadata;

    private static final String SERVICE_URL = 'https://api.stripe.com/v1/plans';

    public static StripeGetPlan getPlan(String planId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeGetPlan.SERVICE_URL+'/'+planId);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody(StripeGetPlan_Test.testData_getPlan);
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            if(statusCode == 200){
                StripeGetPlan o = StripeGetPlan.parse(response);
                System.debug(System.LoggingLevel.INFO, '\n**** Stripe Plan: '+o); 
                return o;
            }
            else{
                return null;
            }
            
        } catch (System.JSONException e) {
            return null;
        }
    }
    @Future(callout=true)
    public static void getPlan(List<Id> prodIdList) {
        List<Product2> prodList = [SELECT id,Stripe_Plan_Product_Id__c,Stripe_Plan_Id__c FROM Product2 WHERE ID IN :prodIdList];
        for (Product2 pr : prodList) {
            HttpRequest http = new HttpRequest();
            http.setEndpoint(StripeGetPlan.SERVICE_URL+'/'+pr.Stripe_Plan_Id__c);
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            Integer statusCode;
            
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            
            if (!Test.isRunningTest()) {
                try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                   
                }
            } else {
                hs.setBody(StripeGetPlan_Test.testData_getPlan);
                hs.setStatusCode(200);
            }
            system.debug('#### '+ hs.getBody());
            
            response = hs.getBody();
            statusCode = hs.getStatusCode();
            
            try {
                StripeGetPlan o = StripeGetPlan.parse(response);
                if (o.product!=null) {
                    pr.Stripe_Plan_Product_Id__c = o.product;
                }else {
                    pr.Stripe_Plan_Product_Id__c = 'No such plan';
                }
                System.debug(System.LoggingLevel.INFO, '\n**** Stripe Plan: '+o); 
               
            } catch (System.JSONException e) {
               
            }
        }
        update prodList;
        
    }
    @Future(callout=true)
    public static void createPlan(List<Id> prodIdList) {
        List<Product2> prodList = [SELECT id,Stripe_Plan_Product_Id__c,Stripe_Plan_Id__c,Name,Price__c,Interval__c,Interval_Count__c FROM Product2 WHERE ID IN :prodIdList];
        Map<String, Object> results = new Map<String, Object>();
        String productid;
        String pbId = '';
        if(!Test.isRunningTest()){
            pbId = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
        }else{
            pbId = Test.getStandardPricebookId();
        }
        List<PricebookEntry> lstPbe = new List<PricebookEntry>();
        PricebookEntry pbeNew;
        for (Product2 pr : prodList) {
            HttpRequest htrq = new HttpRequest();
            htrq.setEndpoint('https://api.stripe.com/v1/products');
            htrq.setMethod('POST');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
            htrq.setHeader('Authorization', authorizationHeader);
            String resp;
            Integer stsCode;
            htrq.setHeader('content-type', 'application/x-www-form-urlencoded');
            htrq.setBody('name='+pr.Name+'&type=service');
            Http con1 = new Http();
            HttpResponse htrs = new HttpResponse();
            if (!Test.isRunningTest()) {
               try {
                    htrs = con1.send(htrq);
                    resp = htrs.getBody();
                    stsCode = htrs.getStatusCode();
               } catch (Exception e) {
                   
               } 
            }else {
                stsCode = 200;
                resp= '{ "id": "prod_GQ4hCEwNncNJye", "object": "product", "active": true, "attributes": [ ], "caption": null, "created": 1577198418, "deactivate_on": [ ], "description": null, "images": [ ], "livemode": false, "metadata": { }, "name": "Testing once agin", "package_dimensions": null, "shippable": null, "skus": { "object": "list", "data": [ ], "has_more": false, "total_count": 0, "url": "/v1/skus?product=prod_GQ4hCEwNncNJye&active=true" }, "statement_descriptor": null, "type": "service", "unit_label": null, "updated": 1577198418, "url": null }';
            }
            if (stsCode == 200) {
                results = (Map<String, Object>)JSON.deserializeUntyped(resp); 
                productid = String.valueOf(results.get('id'));
            }
            if (productid != null) {
                HttpRequest http = new HttpRequest();
                http.setEndpoint(StripeGetPlan.SERVICE_URL);
                http.setMethod('POST');
                http.setHeader('content-type', 'application/x-www-form-urlencoded');
                http.setHeader('Authorization', authorizationHeader);
                http.setBody('currency=usd&interval='+pr.Interval__c+'&product='+productid+'&amount='+Integer.valueOf(String.valueOf(pr.Price__c*100))+'&billing_scheme=per_unit&interval_count='+pr.Interval_Count__c+'&nickname='+pr.Interval__c);
                
                String response;
                Integer statusCode;
                
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                
                if (!Test.isRunningTest()) {
                    try {
                        hs = con.send(http);
                    } catch (CalloutException e) {
                    
                    }
                } else {
                    hs.setBody('{ "id": "plan_GQ4hSEkJ2qp4Ay", "object": "plan", "active": true, "aggregate_usage": null, "amount": 3636, "amount_decimal": "3636", "billing_scheme": "per_unit", "created": 1577198418, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "month", "product": "prod_GQ4hCEwNncNJye", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }');
                    hs.setStatusCode(200);
                }
                system.debug('#### '+ hs.getBody());
                
                response = hs.getBody();
                statusCode = hs.getStatusCode();
                
                try {
                    StripeGetPlan o = StripeGetPlan.parse(response);
                    if (o.product!=null) {
                        pr.Stripe_Plan_Product_Id__c = o.product;
                        pr.Stripe_Plan_Id__c = o.id;
                    }
                    
                    pbeNew = new PricebookEntry();
                    pbeNew.IsActive = true;
                    pbeNew.Pricebook2Id = pbId;
                    pbeNew.Product2Id = pr.Id;
                    pbeNew.UnitPrice = pr.Price__c;
                    lstPbe.add(pbeNew);
                    System.debug(System.LoggingLevel.INFO, '\n**** Stripe Plan: '+o); 
                
                } catch (System.JSONException e) {
                
                } 
            }
            
        }
        update prodList;
        insert lstPbe;
        
    }
    public static StripeGetPlan parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        return (StripeGetPlan) System.JSON.deserialize(json, StripeGetPlan.class);
    }
}