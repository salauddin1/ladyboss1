@isTest
public class ReopenCaseFromCommunityTest {
    @isTest 
    public static void testmethos1() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'Site Guest User',
                            FirstName='LadyBossHub',
                            Alias = 'guest',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jaso@ggmail.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            isActive = true
                           );
        insert usr;
        List<CaseComment> IdSet = new List<CaseComment>();
        System.runAs(usr) {
            contact con = new contact();
        con.lastname = 'lastName';
        con.FirstName = 'firstName';
        con.Email = 'test@gmail.com';
        insert con;
        case caseDetail = new case ();
        caseDetail.ContactId = con.Id;
        caseDetail.Origin = 'Web';
        caseDetail.Is_Site_User_Case__c = true ;
        caseDetail.Description = 'Description Test' ;
         caseDetail.Status='Closed';   
        insert caseDetail;
        
        CaseComment   tComment = new CaseComment();
            tComment.ParentId = caseDetail.id;
            tComment.CommentBody = 'User : '+caseDetail.Description;
            tComment.IsPublished = TRUE;
            insert tComment;
            IdSet.add(tComment);
        }
        
            
        Test.startTest();
        ReopenCaseFromCommuntiy.reopenCase(IdSet);
        Test.stopTest();
    }
}