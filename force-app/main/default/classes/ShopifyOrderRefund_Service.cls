@RestResource(urlMapping='/ShopifyOrderRefund')
global class ShopifyOrderRefund_Service {
    @HttpPost
    global static void getcustomer() {
     if(RestContext.request.requestBody!=null  ){
            try {
                
                String str = RestContext.request.requestBody.toString();
                System.debug('-------'+str);
                ShopifyRefundedJSON2Apex  refundObj = ShopifyRefundedJSON2Apex.parse(str);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                Map<String, Object> customerMap = new Map<String, Object>();
                customerMap =  (Map<String, Object>)results.get('customer');
                String customerEmail = '';
                customerEmail = String.valueOf(results.get('email')); 
                List<Contact> conList = [select id from Contact where email =:customerEmail ];
                set<id> conIdSet = new set<id>();
                for(Contact con : conList ) {
                    conIdSet.add(con.id);
                }
                
                String OrderId = String.valueOf(results.get('order_id'));
                System.debug('-------'+OrderId );
                List<ShopifyLineItem__c> lstLineItem = [SELECT Contact__c,Refunded_Amount__c,Refunded_Tax_Amount__c , Item_Id__c,Shopify_Orders__r.Order_Id__c, Id FROM ShopifyLineItem__c where Shopify_Orders__r.Order_Id__c =:OrderId ];
                List<Shopify_Orders__c > lstOrders = [SELECT  Id,Total_Price__c,Contact__c,Order_Id__c,Refunded_Amount__c FROM Shopify_Orders__c where Order_Id__c =:OrderId ];
                Map<string,ShopifyLineItem__c> orderLineItemMap = new Map<string,ShopifyLineItem__c> ();
                for (ShopifyLineItem__c o: lstLineItem ) {
                    if(o.Item_Id__c!=null){
                        orderLineItemMap.put(o.Item_Id__c,o);
                      }
                }
                
                Map<string,Shopify_Orders__c> orderMap = new Map<string,Shopify_Orders__c> ();
                for (Shopify_Orders__c o: lstOrders ) {
                    if(o.Order_Id__c!=null){
                        orderMap.put(o.Order_Id__c,o);
                      }
                }
                List<Shopify_Orders__c > lstOrdersUpdate = new List<Shopify_Orders__c >();
                List<ShopifyLineItem__c> lstOrderLinesUpdate = new List<ShopifyLineItem__c>();
                if(lstOrders[0].Contact__c!=null ) {
                ShopifyRefundedJSON2Apex  lstObbj = refundObj;
                List<ShopifyRefundedJSON2Apex.Refund_line_items > listRefundedItem = lstObbj.refund_line_items;
                Decimal orderRefundedAmount = 0;
                for (ShopifyRefundedJSON2Apex.Refund_line_items obj : listRefundedItem ) {
                    System.debug('obj '+orderLineItemMap);
                     System.debug('obj '+string.valueOf(obj.line_item_id));
                    if(orderLineItemMap.containsKey(string.valueOf(obj.line_item_id))) {
                        ShopifyLineItem__c lineItem = orderLineItemMap.get(string.valueOf(obj.line_item_id));
                         System.debug('==lineItem ==='+lineItem );             
                                       if(lineItem != null) {
                                       
                                           lineItem.Fulfillable_Quantity__c = Integer.valueOf(obj.line_item.fulfillable_quantity);
                                           lineItem.Status__c = 'Refunded';
                                           if(obj.subtotal!=null){
                                           lineItem.Refunded_Amount__c= lineItem.Refunded_Amount__c+Decimal.valueOf(obj.subtotal);
                                           orderRefundedAmount = orderRefundedAmount +Decimal.valueOf(obj.subtotal);
                                           }
                                           if(obj.total_tax!=null){
                                           Decimal refundedTx = 0 ;
                                           if(lineItem.Refunded_Tax_Amount__c!=null)
                                            refundedTx =lineItem.Refunded_Tax_Amount__c;
                                           Decimal refundedTxVal =0 ;
                                            if(obj.total_tax!=null)
                                           refundedTxVal  = Decimal.valueOf(obj.total_tax);
                                           lineItem.Refunded_Tax_Amount__c = refundedTx  + refundedTxVal  ; 
                                           }
                                           lstOrderLinesUpdate.add(lineItem);
                                       }
                    }
                }
                if(lstOrders.size () > 0) {
                    lstOrders[0].Refunded_Amount__c = orderRefundedAmount + lstOrders[0].Refunded_Amount__c;
                    lstOrders[0].Status__c= 'Refunded';
                    if(lstOrders[0].Refunded_Amount__c==lstOrders[0].Total_Price__c){
                    lstOrders[0].Financial_Status__c = 'refunded';
                    }
                }
                }
                if(lstOrders.size() > 0) {
                    update lstOrders;
                }
                if(lstOrderLinesUpdate.size() > 0) {
                    update lstOrderLinesUpdate;
                }
                
                
                
            }
            catch(Exception e) {}    
            
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
 }