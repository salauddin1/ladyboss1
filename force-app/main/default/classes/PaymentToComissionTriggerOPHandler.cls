/*
 * Developer Name : Tirth Patel
 * Description : This is a helper class to CreditSubstractCommission
 */
public class PaymentToComissionTriggerOPHandler { 
    @AuraEnabled
    public static String comission(Payment__c payt){
        List<Comission__c> comList = new List<Comission__c>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Set<id> inactiveUsers = new Set<id>();
        Integer weekNum = Integer.valueOf(payt.week__c);
        String agendId = payt.Payment_Created_By__c;
        Decimal amount= payt.Amount__c;
        Map<id,Comission_Configuration__c> config_with_product_Map = new Map<id,Comission_Configuration__c>();
        for(Comission_Configuration__c comconfig : [SELECT id,Product__c,Amount_Paid_to_Agent_Finalizing_Sale__c,Amount_Paid_to_Agent_Initiating_Sale__c ,Amount_Paid_to_Director__c,Amount_Paid_to_Vice_President__c,Amount_Paid_to_Senior_Agent__c,Agent_paid_on__c,Director_paid_on__c,Vp_paid_on__c,Special_agent_paid_on__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Senior_Agent__c,Percentage_Paid_to_Vice_President__c,For_First_Sell__c FROM Comission_Configuration__c WHERE Product__c!=null]){
            config_with_product_Map.put(comconfig.Product__c,comconfig);
        }
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = PayToCommTriggerOPHelper.getTotalCommissionConfiguration('Max_Value__c','asc');
        List<Id> userIdList = new List<Id>();
        System.debug(agendId);
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c=:payt.Payment_Created_By__c] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Senior_Agent__c);
            userIdList.add(userhierarki.Vice_President__c); 
        }
        List<User> userHieList = [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE Id in : userIdList];
        Map<id,User> userSpecialComMap = new Map<id,User>();
        for(User ur : userHieList){
            userSpecialComMap.put(ur.id, ur);
            if(!ur.IsActive){
                inactiveUsers.add(ur.id);
            }
        }
        if(userHieMap.size()==0){
            System.debug('No User Hierarchies');
            return null;
        }
        Map<id,String> userValueMap= PayToCommTriggerOPHelper.getTotalCommission(agendId,config_with_product_Map.keyset(),weekNum,amount);
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap = PayToCommTriggerOPHelper.getOldPayments(new Set<Id>{payt.Payment_Created_By__c}, config_with_product_Map.keyset(),weekNum);
        Map<id,id> payAndcreditUser = PayToCommTriggerOPHelper.getOldPaymentCreditUser(new Set<Id>{agendId}, config_with_product_Map.keyset(),weekNum);
        UserHierarchy__c userhie = userHieMap.get(agendId);
        if(userhie==null){
            System.debug('UserHierarchy does not exist for this user');
            return null;
        }
        Decimal twoWeekTotal = Decimal.valueOf(userValueMap.get(agendId)); 
        System.debug(twoWeekTotal);
        Comission_Configuration__c com = PayToCommTriggerOPHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
        System.debug(com);
        PayToCommTriggerOPHelper.tierBasedCommission(null,userhie, payt, com, comList,userSpecialComMap,inactiveUsers);
        Decimal twoWeekTotalBefore = twoWeekTotal - amount;
        Comission_Configuration__c comBefore = PayToCommTriggerOPHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
        if(comBefore.id != com.id){ 
            System.debug('tier changed');
            if(payUserAndComMap.size()>0){
                PayToCommTriggerOPHelper.updatePrevCommissions(payUserAndComMap.get(agendId), com, userhie, comList, payAndcreditUser,userSpecialComMap,inactiveUsers);
            } 
        }
       
        upsert comList;
        
        List<Comission__c> comwithhierarchyList = [SELECT Hierarchies_type__c,hierarchy__c FROM Comission__c where id in : comList];
         for(Comission__c commm : comwithhierarchyList){
             if(commm.hierarchy__c.toLowerCase().contains('senior agent')){
                commm.hierarchy__c = 'Senior Agent with Manual Non Direct';
                 continue ;
            }else if(commm.hierarchy__c.toLowerCase().contains('agent')){
                commm.hierarchy__c = 'Agent with Manual Non Direct';
                 continue ;
            } else if(commm.hierarchy__c.toLowerCase().contains('vice president')){
                commm.hierarchy__c = 'Vice President with Manual Non Direct';
                 continue ;
            } else if(commm.hierarchy__c.toLowerCase().contains('director')){
                commm.hierarchy__c = 'Director with Manual Non Direct';
                 continue ;
            }
             /*if(commm.hierarchy__c.toLower().contains('credit user')){
                commm.hierarchy__c = 'Credit User with Manual Non Direct';
                 break ;
            }*/
            
        }
        upsert comwithhierarchyList;
        System.debug('Commissions : '+comList.size()+' '+comList);
        return 'Success';
    }
    @AuraEnabled
    public static String refund(Payment__c payt){
        List<Comission__c> comList = new List<Comission__c>();
        Set<id> inactiveUsers = new Set<id>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Integer weekNum = Integer.valueOf(payt.week__c);
        Comission_Configuration__c emptyConfig = new Comission_Configuration__c();
        Comission_Configuration__c configForFirstSell = new Comission_Configuration__c();
        Map<id,Comission_Configuration__c> comConfigProdMap = new Map<id,Comission_Configuration__c>();
        for(Comission_Configuration__c comconfig : [SELECT id,Product__c,Amount_Paid_to_Agent_Finalizing_Sale__c,Amount_Paid_to_Agent_Initiating_Sale__c ,Amount_Paid_to_Director__c,Amount_Paid_to_Vice_President__c,Amount_Paid_to_Senior_Agent__c,Agent_paid_on__c,Director_paid_on__c,Vp_paid_on__c,Special_agent_paid_on__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Senior_Agent__c,Percentage_Paid_to_Vice_President__c,For_First_Sell__c FROM Comission_Configuration__c WHERE Product__c!=null or For_First_Sell__c = true]){
            if(comconfig.Product__c!=null){
                comConfigProdMap.put(comconfig.Product__c,comconfig);
            }else if(comconfig.For_First_Sell__c == true){
                configForFirstSell = comconfig;
            }
        }
        List<Id> userIdList = new List<Id>();
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c =: payt.Payment_Created_By__c] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Senior_Agent__c);
            userIdList.add(userhierarki.Vice_President__c);
        }
        List<User> userHieList = [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE Id in : userIdList];
        Map<id,User> userSpecialComMap = new Map<id,User>();
        for(User ur : userHieList){
            userSpecialComMap.put(ur.id, ur);
            if(!ur.IsActive){
                inactiveUsers.add(ur.id);
            }
        } 
        if(userHieMap.size()==0||inactiveUsers.contains(payt.Payment_Created_By__r.id)){
            return null;
        } 
        Map<id,id> payAndcreditUser = PayToCommTriggerOPHelper.getOldPaymentCreditUser(new Set<Id>{payt.Payment_Created_By__c}, comConfigProdMap.keyset(),weekNum);
        Map<id,List<Map<id,List<Comission__c>>>> payUserAndComMap = PayToCommTriggerOPHelper.getOldPayments(new Set<Id>{payt.Payment_Created_By__c}, comConfigProdMap.keyset(),weekNum);
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = PayToCommTriggerOPHelper.getTotalCommissionConfiguration('Max_Value__c','asc');
        UserHierarchy__c userhie = userHieMap.get(payt.Payment_Created_By__c);
        if(userhie==null){
            System.debug('UserHierarchy does not exist for this user');
            return null;
        }
        Decimal week = payt.week__c;  
        Map<id,String> userValueMap = PayToCommTriggerOPHelper.getTotalCommissionRef(payt.week__c,comConfigProdMap.keyset(),payt.Payment_Created_By__c);
        if(userValueMap.size()==0){
            userValueMap.put(payt.Payment_Created_By__c,'0');
            //System.debug('No opportunity to refund');
            //return 'No';
        }  
        System.debug('userValueMap : '+userValueMap);
        Decimal twoWeekTotal = Decimal.valueOf(userValueMap.get(payt.Payment_Created_By__c)) + payt.Amount__c; 
        Comission_Configuration__c com = PayToCommTriggerOPHelper.getCommissionConfiguration(twoWeekTotal,maxValAndComMap);
        if(PayToCommTriggerOPHelper.isInTwoWeek(week)){
            System.debug('In Two Week');
            PayToCommTriggerOPHelper.tierBasedCommission(null, userhie, payt, com, comList,userSpecialComMap,inactiveUsers);
            Decimal twoWeekTotalBefore = twoWeekTotal - payt.Amount__c;
            Comission_Configuration__c comBefore = PayToCommTriggerOPHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
            System.debug(comBefore.Name);
            System.debug(com.Name);
            if(comBefore.id != com.id){ 
                if(payUserAndComMap.size()>0){
                    PayToCommTriggerOPHelper.updatePrevCommissions(payUserAndComMap.get(payt.Payment_Created_By__c), com, userhie, comList, payAndcreditUser,userSpecialComMap,inactiveUsers);
                }
            } 
        }else{
            System.debug('Not in two week');
            System.debug('Amount : '+payt.Amount__c);
            Decimal twoWeekTotalBefore = twoWeekTotal - payt.Amount__c;
            if(!PayToCommTriggerOPHelper.isWeekSpecific(week,comConfigProdMap.keyset())){
                Comission_Configuration__c comBefore = PayToCommTriggerOPHelper.getCommissionConfiguration(twoWeekTotalBefore,maxValAndComMap);
                if(com.id == comBefore.id){
                    PayToCommTriggerOPHelper.previoustwoWeekSameTier(null,userhie, payt, com, comList,userSpecialComMap,inactiveUsers);
                }else{ 
                    PayToCommTriggerOPHelper.previoustwoWeekTierChange(null,twoWeekTotalBefore,twoWeekTotal, userhie, comBefore, payt, com, comList,comConfigProdMap.keyset(),userSpecialComMap);
                }
            }else{ 
                System.debug('specific week');
                Commission_configuration_specific_week__c cm = PayToCommTriggerOPHelper.getCOmCgfWeekSpecific(twoWeekTotal, week);
                Commission_configuration_specific_week__c cmBefore = PayToCommTriggerOPHelper.getCOmCgfWeekSpecific(twoWeekTotalBefore, week);
                if(cm.id == cmBefore.id){
                    PayToCommTriggerOPHelper.previoustwoWeekSpecificSameTier(null,userhie, cm, payt, comList);
                }else{ 
                    PayToCommTriggerOPHelper.previoustwoWeekSpecificTierChange(null,twoWeekTotalBefore, userhie, twoWeekTotal, cmBefore, cm, payt, comList,comConfigProdMap.keyset());
                }
            }
        }
        upsert comList;
        
        List<Comission__c> comwithhierarchyList = [SELECT Hierarchies_type__c,hierarchy__c FROM Comission__c where id in : comList];
         for(Comission__c commm : comwithhierarchyList){
             if(commm.hierarchy__c.toLowerCase().contains('senior agent')){
                commm.hierarchy__c = 'Senior Agent with Manual Non Direct';
                 continue ;
            }else if(commm.hierarchy__c.toLowerCase().contains('agent')){
                commm.hierarchy__c = 'Agent with Manual Non Direct';
                 continue ;
            } else if(commm.hierarchy__c.toLowerCase().contains('vice president')){
                commm.hierarchy__c = 'Vice President with Manual Non Direct';
                 continue ;
            } else if(commm.hierarchy__c.toLowerCase().contains('director')){
                commm.hierarchy__c = 'Director with Manual Non Direct';
                 continue ;
            }
             /*if(commm.hierarchy__c.toLower().contains('credit user')){
                commm.hierarchy__c = 'Credit User with Manual Non Direct';
                 break ;
            }*/
            
        }
        upsert comwithhierarchyList;
        System.debug('Commissions : '+comList.size()+' '+comList);  
        return 'Success';
    }   
}