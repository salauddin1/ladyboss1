@isTest
global class StripeMockImplementation implements HttpCalloutMock{

   	public static HTTPResponse respond(HTTPRequest request) {
   		HttpResponse response = new HttpResponse();
		if(request.getEndpoint().contains('charges')){
			response.setHeader('Content-Type', 'application/json');
	        response.setBody('{ \"object\": \"list\", \"data\": [ { \"id\": \"ch_1Bnu6XDiFnu7hVq7FZC78Hpr\", \"object\": \"charge\", \"amount\": 2655, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1Bnu6XDiFnu7hVq7vBzOqKyf\", \"captured\": true, \"created\": 1516822817, \"currency\": \"usd\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"description\": null, \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": {}, \"invoice\": \"in_1BntAHDiFnu7hVq7qGHrvQKO\", \"livemode\": false, \"metadata\": {}, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1Bnu6XDiFnu7hVq7FZC78Hpr/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BYf1vDiFnu7hVq7kXdTknjq\", \"object\": \"card\", \"address_city\": \"\", \"address_country\": \"United States\", \"address_line1\": \"\", \"address_line1_check\": null, \"address_line2\": null, \"address_state\": \"\", \"address_zip\": \"\", \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 2, \"exp_year\": 2022, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": {}, \"name\": \"Grant Stripe\", \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null }], \"has_more\": true, \"url\": \"/v1/charges\" }');
	        response.setStatusCode(200);
	        response.setStatus('OK');
		}else if(request.getEndpoint().contains('customers')){
			response.setHeader('Content-Type', 'application/json');
            String json = '{'+
                            '    \"object\": \"list\",'+
                            '    \"data\": ['+
                            '        {'+
                            '            \"id\": \"cus_BUOWQlyttHhSXd\",'+
                            '            \"object\": \"customer\",'+
                            '            \"account_balance\": 0,'+
                            '            \"created\": 1506640589,'+
                            '            \"currency\": null,'+
                            '            \"default_source\": null,'+
                            '            \"delinquent\": false,'+
                            '            \"description\": \"des3\",'+
                            '            \"discount\": null,'+
                            '            \"email\": \"wynche3@cloudcreations.com\",'+
                            '            \"livemode\": false,'+
                            '            \"metadata\": {},'+
                			'            \"shipping\": {'+
                			'					\"address\":{\"city\":\"test\",\"country\":\"test\",\"postal_code\":\"123\",\"state\":\"test\",\"line1\":\"test\"}'+
                			'				},'+
                            '            \"sources\": {'+
                            '                \"object\": \"list\",'+
                			'                \"data\": [{\"id\":\"crd_123456789\"}],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9YEPogMk0fA1/sources\"'+
                            '            },'+
                            '            \"subscriptions\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": [],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9YEPogMk0fA1/subscriptions\"'+
                            '            }'+
                            '        },'+
                            '        {'+
                            '            \"id\": \"cus_BU9Xv9yA6CrHbv\",'+
                            '            \"object\": \"customer\",'+
                            '            \"account_balance\": 0,'+
                            '            \"created\": 1506640578,'+
                            '            \"currency\": null,'+
                            '            \"default_source\": null,'+
                            '            \"delinquent\": false,'+
                            '            \"description\": \"des2\",'+
                            '            \"discount\": null,'+
                            '            \"email\": \"wynche2@cloudcreations.com\",'+
                            '            \"livemode\": false,'+
                            '            \"metadata\": {},'+
                            '            \"shipping\": null,'+
                            '            \"sources\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": [],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9Xv9yA6CrHbv/sources\"'+
                            '            },'+
                            '            \"subscriptions\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": [],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9Xv9yA6CrHbv/subscriptions\"'+
                            '            }'+
                            '        },'+
                            '        {'+
                            '            \"id\": \"cus_BU9XtJ6Muptggp\",'+
                            '            \"object\": \"customer\",'+
                            '            \"account_balance\": 0,'+
                            '            \"created\": 1506640558,'+
                            '            \"currency\": null,'+
                            '            \"default_source\": null,'+
                            '            \"delinquent\": false,'+
                            '            \"description\": \"des1\",'+
                            '            \"discount\": null,'+
                            '            \"email\": \"wynche@cloudcreations.com\",'+
                            '            \"livemode\": false,'+
                            '            \"metadata\": {},'+
                            '            \"shipping\": null,'+
                            '            \"sources\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": [],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9XtJ6Muptggp/sources\"'+
                            '            },'+
                            '            \"subscriptions\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": [],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 0,'+
                            '                \"url\": \"/v1/customers/cus_BU9XtJ6Muptggp/subscriptions\"'+
                            '            }'+
                            '        }'+
                            '    ],'+
                            '    \"has_more\": false,'+
                            '    \"url\": \"/v1/customers\"'+
                            '}';
	        response.setBody(json);
	        response.setStatusCode(200);
	        response.setStatus('OK');
		}else if(request.getEndpoint().contains('plans')){
			response.setHeader('Content-Type', 'application/json');
            String json = '{'+
                            '    \"object\": \"list\",'+
                            '    \"data\": ['+
                            '        {'+
                            '            \"id\": \"test2\",'+
                            '            \"object\": \"plan\",'+
                            '            \"amount\": 2655,'+
                            '            \"created\": 1508947122,'+
                            '            \"currency\": \"usd\",'+
                            '            \"interval\": \"week\",'+
                            '            \"interval_count\": 2,'+
                            '            \"livemode\": false,'+
                            '            \"metadata\": {},'+
                            '            \"name\": \"Test Product 2\",'+
                            '            \"statement_descriptor\": null,'+
                            '            \"trial_period_days\": null'+
                            '        }'+
                            '    ],'+
                            '    \"has_more\": true,'+
                            '    \"url\": \"/v1/plans\"'+
                            '}';
	        response.setBody(json);
	        response.setStatusCode(200);
	        response.setStatus('OK');
		}else if(request.getEndpoint().contains('invoices')){
			response.setHeader('Content-Type', 'application/json');
            String json = '{'+
                            '    \"object\": \"list\",'+
                            '    \"data\": ['+
                            '        {'+
                            '            \"id\": \"in_1BGucSDiFnu7hVq7UjEJWYAF\",'+
                            '            \"object\": \"invoice\",'+
                            '            \"amount_due\": 14700,'+
                            '            \"application_fee\": null,'+
                            '            \"attempt_count\": 1,'+
                            '            \"attempted\": true,'+
                            '            \"billing\": \"send_invoice\",'+
                            '            \"charge\": \"ch_1BGupnDiFnu7hVq75QG3rA0X\",'+
                            '            \"closed\": true,'+
                            '            \"currency\": \"usd\",'+
                            '            \"customer\": \"cus_BUTZMTLfmUyQ9H\",'+
                            '            \"date\": 1508959972,'+
                            '            \"description\": null,'+
                            '            \"discount\": null,'+
                            '            \"due_date\": 1511551972,'+
                            '            \"ending_balance\": 0,'+
                            '            \"forgiven\": false,'+
                            '            \"lines\": {'+
                            '                \"object\": \"list\",'+
                            '                \"data\": ['+
                            '                    {'+
                            '                        \"id\": \"sub_BeD21gvhlwqB2q\",'+
                            '                        \"object\": \"line_item\",'+
                            '                        \"amount\": 7500,'+
                            '                        \"currency\": \"usd\",'+
                            '                        \"description\": null,'+
                            '                        \"discountable\": true,'+
                            '                        \"livemode\": false,'+
                            '                        \"metadata\": {},'+
                            '                        \"period\": {'+
                            '                            \"start\": 1508959972,'+
                            '                            \"end\": 1511638372'+
                            '                        },'+
                            '                        \"plan\": {'+
                            '                            \"id\": \"test1\",'+
                            '                            \"object\": \"plan\",'+
                            '                            \"amount\": 2500,'+
                            '                            \"created\": 1508947075,'+
                            '                            \"currency\": \"usd\",'+
                            '                            \"interval\": \"month\",'+
                            '                            \"interval_count\": 1,'+
                            '                            \"livemode\": false,'+
                            '                            \"metadata\": {},'+
                            '                            \"name\": \"Test Product 1\",'+
                            '                            \"statement_descriptor\": null,'+
                            '                            \"trial_period_days\": null'+
                            '                        },'+
                            '                        \"proration\": false,'+
                            '                        \"quantity\": 3,'+
                            '                        \"subscription\": null,'+
                            '                        \"subscription_item\": \"si_1BGucSDiFnu7hVq7nfW3kqaU\",'+
                            '                        \"type\": \"subscription\"'+
                            '                    },'+
                            '                    {'+
                            '                        \"id\": \"sub_BeD21gvhlwqB2q\",'+
                            '                        \"object\": \"line_item\",'+
                            '                        \"amount\": 6000,'+
                            '                        \"currency\": \"usd\",'+
                            '                        \"description\": null,'+
                            '                        \"discountable\": true,'+
                            '                        \"livemode\": false,'+
                            '                        \"metadata\": {},'+
                            '                        \"period\": {'+
                            '                            \"start\": 1508959972,'+
                            '                            \"end\": 1511638372'+
                            '                        },'+
                            '                        \"plan\": {'+
                            '                            \"id\": \"test4\",'+
                            '                            \"object\": \"plan\",'+
                            '                            \"amount\": 3000,'+
                            '                            \"created\": 1508959942,'+
                            '                            \"currency\": \"usd\",'+
                            '                            \"interval\": \"month\",'+
                            '                            \"interval_count\": 1,'+
                            '                            \"livemode\": false,'+
                            '                            \"metadata\": {},'+
                            '                            \"name\": \"Test 4\",'+
                            '                            \"statement_descriptor\": null,'+
                            '                            \"trial_period_days\": null'+
                            '                        },'+
                            '                        \"proration\": false,'+
                            '                        \"quantity\": 2,'+
                            '                        \"subscription\": null,'+
                            '                        \"subscription_item\": \"si_1BGucSDiFnu7hVq7gC0SikUf\",'+
                            '                        \"type\": \"subscription\"'+
                            '                    }'+
                            '                ],'+
                            '                \"has_more\": false,'+
                            '                \"total_count\": 2,'+
                            '                \"url\": \"/v1/invoices/in_1BGucSDiFnu7hVq7UjEJWYAF/lines\"'+
                            '            },'+
                            '            \"livemode\": false,'+
                            '            \"metadata\": {},'+
                            '            \"next_payment_attempt\": null,'+
                            '            \"number\": \"2945c0ac44-0001\",'+
                            '            \"paid\": true,'+
                            '            \"period_end\": 1508959972,'+
                            '            \"period_start\": 1508959972,'+
                            '            \"receipt_number\": null,'+
                            '            \"starting_balance\": 1200,'+
                            '            \"statement_descriptor\": null,'+
                            '            \"subscription\": \"sub_BeD21gvhlwqB2q\",'+
                            '            \"subtotal\": 13500,'+
                            '            \"tax\": null,'+
                            '            \"tax_percent\": null,'+
                            '            \"total\": 13500,'+
                            '            \"webhooks_delivered_at\": 1508959973'+
                            '        }'+
                            '    ],'+
                            '    \"has_more\": true,'+
                            '    \"url\": \"/v1/invoices\"'+
                            '}';
	        response.setBody(json);
	        response.setStatusCode(200);
	        response.setStatus('OK');
		}else if(request.getEndpoint().contains('subscriptions')){
			response.setHeader('Content-Type', 'application/json');
            String json = '{ \"id\": \"evt_1BGb19DiFnu7hVq7eWM8NykH\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508884623, \"data\": { \"object\": { \"id\": \"ch_1BGb19DiFnu7hVq7Vs7GihRu\", \"object\": \"charge\", \"amount\": 2000, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1BGb19DiFnu7hVq7vq3Tm0oX\", \"captured\": true, \"created\": 1508884623, \"currency\": \"usd\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"description\": \"null\", \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": { }, \"invoice\": \"inv_123456789\", \"livemode\": false, \"metadata\": { }, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1BGb19DiFnu7hVq7Vs7GihRu/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BGE5EDiFnu7hVq7MMj5y28q\", \"object\": \"card\", \"address_city\": null, \"address_country\": null, \"address_line1\": null, \"address_line1_check\": null, \"address_line2\": null, \"address_state\": null, \"address_zip\": null, \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BcOxelyfXsr0Ki\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 8, \"exp_year\": 2019, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": { }, \"name\": null, \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_i4OseGgOylA97N\", \"idempotency_key\": null }, \"type\": \"charge.succeeded\" }';
	        response.setBody(json);
	        response.setStatusCode(200);
	        response.setStatus('OK');
		}
		return response;
   	}

}