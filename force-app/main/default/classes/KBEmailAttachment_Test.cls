@istest
public class KBEmailAttachment_Test{
    static testMethod void test1() {
        Knowledge__kav kk = new Knowledge__kav();
        kk.Answer__c='sdsds';
        kk.Question__c ='dsd';
        kk.Title='ddsd';
        kk.UrlName='ddsd';
        //kk.PublishStatus = 'online'; 
        kk.Language = 'en_US';
        insert kk;
        knowledge__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM knowledge__kav WHERE id =: kk.Id];
        KbManagement.PublishingService.publishArticle(obj1.KnowledgeArticleId, true);
        DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
            new String[] { 'KnowledgeArticleVersion'}
        );
        
        
        Knowledge__DataCategorySelection kdcs = new Knowledge__DataCategorySelection();
        kdcs.DataCategoryName = 'FAQ';
        kdcs.ParentId = kk.id;
        kdcs.DataCategoryGroupName=results[0].getName();
        insert kdcs;
        
        test.starttest();
        KBEmailAttachment.getArticlesTitle(kk.Id);
        KBEmailAttachment.getArticles('ds');
        test.stopTest();
        
    }
    
    static testMethod void test2() {
        Knowledge__kav kk = new Knowledge__kav();
        kk.Answer__c='sdsds';
        kk.Question__c ='dsd';
        kk.Title='ddsd';
        kk.UrlName='ddsd';
        //kk.PublishStatus = 'online'; 
        kk.Language = 'en_US';
        insert kk;
        
        Knowledge__kav kk2 = new Knowledge__kav();
        kk2.Answer__c='sdsds2';
        kk2.Question__c ='dsd2';
        kk2.Title='ddsd2';
        kk2.UrlName='ddsd2';
        kk2.Language = 'en_US';
        insert kk2;
        
        knowledge__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM knowledge__kav WHERE id =: kk.Id];
        KbManagement.PublishingService.publishArticle(obj1.KnowledgeArticleId, true);
        knowledge__kav obj2 = [SELECT Id,Title,KnowledgeArticleId FROM knowledge__kav WHERE id =: kk2.Id];
        KbManagement.PublishingService.publishArticle(obj2.KnowledgeArticleId, true);
        DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
            new String[] { 'KnowledgeArticleVersion'}
        );
        
        
        Knowledge__DataCategorySelection kdcs = new Knowledge__DataCategorySelection();
        kdcs.DataCategoryName = 'FAQ';
        kdcs.ParentId = kk.id;
        kdcs.DataCategoryGroupName=results[0].getName();
        insert kdcs;
        
        test.starttest();
        KBEmailAttachment.getArticles('');
        test.stopTest();
        
    }
}