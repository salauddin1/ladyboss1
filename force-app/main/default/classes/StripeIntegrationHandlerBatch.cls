global class StripeIntegrationHandlerBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
   
    public boolean isSuccess = false;
    List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
    List<Opportunity> oppList = new List<Opportunity> ();
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('SELECT Id,Name, Contact__c,Card__c,Amount,Stripe_Message__c,(SELECT Id, OpportunityId,product2id,product2.Name,product2.Email_Body__c FROM OpportunityLineItems ) FROM Opportunity where Scheduled_Payment_Processed__c= false and Scheduled_Payment_Date__c =today');
   }

   global void execute(Database.BatchableContext BC, 
                       List<sObject> scope){
                       oppList = (List<Opportunity>)scope;
                       oliToUpdate =  oppList[0].OpportunityLineItems;
                       StripeIntegrationHandler.OpportunityStripeHandlerBatch(oppList,oliToUpdate);
     
      
   }

   global void finish(Database.BatchableContext BC){
        
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
            Set<Id> oppIds = new  Set<Id>();
            for(Opportunity opp : oppList){
                oppIds.add(opp.Id);
            } 
            for(Opportunity opp : [select id,Contact__r.Email,Stripe_Message__c,(select id ,product2Id,product2.Email_Body__c,product2.Name from OpportunityLineItems) from Opportunity where id =:oppIds and Stripe_Message__c='Success']){
            
                      
                for(OpportunityLineItem lineItem : opp.OpportunityLineItems){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] { opp.Contact__r.Email});
                    
                    mail.setSubject('LadyBoss Receipt - ' + lineItem.product2.Name);
                    mail.setHtmlBody((lineItem.product2.Email_Body__c !=null ? lineItem.product2.Email_Body__c: 'Thank you for your purchase. If you have any questions or concerns, please email us at support@ladyboss.com.'));
                    
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    
                    PageReference pdf = Page.OpportunityLineItemInvoice;
                    pdf.getParameters().put('id',lineItem.Id);
                    Blob body;                
                    try{
                        body = pdf.getContent();
                    }catch(VisualforceException e){
                        body=Blob.valueOf('Some text');
                    }            
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.setContentType('application/pdf');
                    attach.setFileName('LadyBoss Receipt.pdf');
                    attach.setInline(false);
                    attach.Body = body;
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
                    mails.add(mail);
                    
                }
            }
            if(mails.size() >0 && !test.isRunningTest())
                Messaging.sendEmail(mails);
                
       
        }
            
   

}