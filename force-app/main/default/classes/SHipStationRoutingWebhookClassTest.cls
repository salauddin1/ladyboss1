@isTest
public class SHipStationRoutingWebhookClassTest {
    @isTest
    public static void shipstationtestHookUnit1(){
        Contact con = new contact();
        con.LastName = 'Test';
        con.Email = 'Test@gmail.com';
        con.phone = '1234567890';
        insert con;
        ShipStation__c credential = new ShipStation__c();
        credential.userName__c = 'dghvhvshv2e3242eway';
        credential.password__c = 'sffefeeysyudbscbwqu';
        credential.IsLive__c=true;
        credential.Domain_Name__c='https://ssapi9.shipstation.com';
        insert credential;
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/SHipStationRoutingWebhookClass'; 
        req.requestBody = Blob.valueof('{"resource_url":"https://ssapi9.shipstation.com/orders?importBatch=f169abbe-8bc6-421a-9f1c-2ad2ea654be5","resource_type":"ORDER_NOTIFY"}');
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        Test.setMock(HttpCalloutMock.class, new SHipStationRoutingWebhookClassMock());
        SHipStationRoutingWebhookClass.shipStationRouted();
        Test.stopTest();
    }
    @isTest
    public static void shipstationtestHookUnit2(){
        ShipStation__c credential = new ShipStation__c();
        credential.userName__c = 'dghvhvshv2e3242eway';
        credential.password__c = 'sffefeeysyudbscbwqu';
        credential.IsLive__c=true;
        credential.Domain_Name__c='https://ssapi9.shipstation.com';
        insert credential;
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/SHipStationRoutingWebhookClass'; 
        req.requestBody = Blob.valueof('{"resource_url":"https://ssapi9.shipstation.com/orders?importBatch=f169abbe-8bc6-421a-9f1c-2ad2ea654be5","resource_type":"ORDER_NOTIFY"}');
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        Test.setMock(HttpCalloutMock.class, new SHipStationRoutingWebhookClassMock());
        SHipStationRoutingWebhookClass.shipStationRouted();
        Test.stopTest();
    }
}