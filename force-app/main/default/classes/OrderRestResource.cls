@RestResource(urlMapping='/Order/*')
global without sharing class OrderRestResource {    
    @HttpPost
    global static String doPost() {
        try {
            OrderWrapper odw = null;
            odw = (OrderWrapper)JSON.deserialize(RestContext.request.requestBody.toString(), OrderWrapper.class);
            
            Map<String,Object> mapActualJSON = (Map<String,Object>)JSON.deserializeUntyped (RestContext.request.requestBody.toString());
            OrderBilling lstOrderBilling = odw.billing;
            // Quick fixes added by Jay 31st March --> Start
         
            if(lstOrderBilling == null && mapActualJSON.get('order_number') != null )  {
                lstOrderBilling = new OrderBilling();
                lstOrderBilling.address_1 = (String)mapActualJSON.get('billing_address_1');    
                lstOrderBilling.address_2 = (String)mapActualJSON.get('billing_address_2');    
                lstOrderBilling.city= (String)mapActualJSON.get('billing_city');    
                lstOrderBilling.company   = (String)mapActualJSON.get('billing_company');    
                lstOrderBilling.country= (String)mapActualJSON.get('_billing_country');    
                lstOrderBilling.email= (String)mapActualJSON.get('billing_email');    
                lstOrderBilling.first_name = (String)mapActualJSON.get('billing_first_name');    
                lstOrderBilling.last_name = (String)mapActualJSON.get('billing_last_name');
                lstOrderBilling.phone= (String)mapActualJSON.get('billing_phone');    
                lstOrderBilling.postcode= (String)mapActualJSON.get('_billing_postcode');
                
                
               
                
                odw.billing = lstOrderBilling; 
                odw.order_date_temp = (String)mapActualJSON.get('order_date');
                OrderShipping os = new OrderShipping();
                
                os.address_1 = (String)mapActualJSON.get('billing_address_1');    
                os.address_2 = (String)mapActualJSON.get('billing_address_2');    
                os.city= (String)mapActualJSON.get('billing_city');    
                os.company   = (String)mapActualJSON.get('billing_company');    
                os.country= (String)mapActualJSON.get('_billing_country');    
                
                os.first_name = (String)mapActualJSON.get('billing_first_name');    
                os.last_name = (String)mapActualJSON.get('billing_last_name');
                
                os.postcode= (String)mapActualJSON.get('_billing_postcode');
                
                
                odw.shipping = os; 
                odw.id = (String)mapActualJSON.get('order_number');
                odw.status =(String) mapActualJSON.get('order_status');
                odw.total = Double.valueOf((String)mapActualJSON.get('order_total'));
                odw.customer_note = (String)mapActualJSON.get('customer_note');
                odw.total_tax = Double.valueOf(mapActualJSON.get('order_total_tax'));
                
                if(mapActualJSON.get('products') != null)  {
                    List<Object> lstO = (List<object>)mapActualJSON.get('products');
                    odw.line_items = new List<OrderLineItems>();
                    for(Object o : lstO)  {
                        OrderLineItems ol = new OrderLineItems();
                        odw.line_items.add(ol);
                        Map<String,Object> map123 = (Map<String,Object>)o;
                        ol.id = Double.valueOf(map123.get('item_id'));
                        ol.name = (String)map123.get('name');
                        ol.product_id = Double.valueOf(map123.get('product_id'));
                        ol.variation_id = (String)map123.get('_variation_id');
                        ol.quantity = (String)map123.get('qty');
                        ol.tax_class= (String)map123.get('_tax_class');
                        ol.subtotal= (String)map123.get('_line_subtotal');
                        ol.subtotal_tax= (String)map123.get('_line_subtotal_tax');
                        ol.total= (String)map123.get('_line_total');
                        ol.total_tax= (String)map123.get('_line_tax');
                        if(map123.get('sku') != null)  {
                        	String sku = map123.get('sku')+'';
                        	ol.sku= sku;
                        }    
                        if(map123.get('_price') != null)  {
                        	String price = map123.get('_price') + '';
                            ol.price = price;
                        }    
                       
                    }
                }
                
                List<Map<String,String>> metadata = new List<Map<String,String>>();                
                Map<String,String> m1 = new Map<String,String>(); 
                m1.put('key','wc_order_field_4141');
                m1.put('value',(String)mapActualJSON.get('wc_order_field_4141'));
                
                Map<String,String> m2 = new Map<String,String>(); 
                m2.put('key','wc_order_field_4501');
                m2.put('value',(String)mapActualJSON.get('wc_order_field_4501'));
                
                Map<String,String> m3 = new Map<String,String>(); 
                m3.put('key','wc_order_field_1274');
                m3.put('value',(String)mapActualJSON.get('wc_order_field_1274'));
                
                Map<String,String> m4 = new Map<String,String>(); 
                m4.put('key','wc_order_field_3355');
                m4.put('value',(String)mapActualJSON.get('wc_order_field_3355'));
                
                Map<String,String> m5 = new Map<String,String>(); 
                m5.put('key','wc_order_field_9653');
                m5.put('value',(String)mapActualJSON.get('wc_order_field_9653'));
                
                metadata.add(m1);
                metadata.add(m2);
                metadata.add(m3);
                metadata.add(m4);
                metadata.add(m5);
                mapActualJSON.put('meta_data',metadata);
            }

            // Quick fixes added by Jay 31st March --> Stop
            
            
            
            
            //Account accSobject =upsertAccount(mapActualJSON,lstOrderBilling,odw);
            
            
            //List<Order> lstO = [select id,EffectiveDate from Order where Order_ID__c =: odw.id ];
            String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
            Order order = new Order();
            Opportunity opp = new Opportunity();
            //opp.Name = OppName;
            opp.CloseDate = System.today();
            opp.StageName = 'closed won';
            opp.Status__c = odw.status;
            opp.Amount = odw.total;
            opp.RecordTypeId = devRecordTypeId ;
            
            insert opp;
            
            /*if(lstO.size() > 0)  {
                order = lstO.get(0);
            }*/
            
            //order.Order_ID__c = odw.id;
            order.Status  = odw.status;
            //order.AccountId = accSobject.Id;
            //order.webkul_wws__woo_Total_order__c = odw.total;
            //System.debug(lstO.size());
            /*if(lstO.size() == 0)  {  // only set date on create not update.
                System.debug(odw.order_date_temp);
            	if(odw.order_date_temp != null)  {
                    System.debug('setting custom dates');
                    String tempDate =  odw.order_date_temp;
                    tempDate = tempDate.subString(0,10);
                	order.EffectiveDate  = Date.valueOf(tempDate);
            	}else  {
					order.EffectiveDate  = System.today();    
            	}
            }    */
            System.debug(order.EffectiveDate);
            Id pricebook2Id = NULL;
            if(!Test.isRunningTest())  {
                pricebook2Id = [select id from Pricebook2 where isstandard = true limit 1].id;
            }else {
                pricebook2Id =  Test.getStandardPricebookId();
            }
            
            
            order.Pricebook2Id = pricebook2Id;
            
            //order.AccountId = accSobject.id;
            
           // order.EffectiveDate = system.today();
            //order.Budget_Confirmed__c = true;
            //order.Email__c = lstOrderBilling.email;
            List<Object> lstOrderMetaData = (List<Object>)mapActualJSON.get('meta_data');
            String licenseNumber = '';
            if(lstOrderMetaData != null && lstOrderMetaData.size() > 0)  {
                for(object om : lstOrderMetaData)  {
                    Map<String,Object> mapS = (Map<String,Object>)om;
                    if(mapS.get('key') != null && mapS.get('value') != null)  {
                        String key = mapS.get('key').toString();
                        String value = mapS.get('value').toString();
                        if(key == 'wc_order_field_4141'){
                            if(value.length() > 250)  {
                                value = value.subString(0,250);
                            }
                            //order.License_Number__c = value;
                            licenseNumber = value;
                        }
                        if(key == 'wc_order_field_4501'){
                            //order.Kit_User_Name__c = value;
                        }
                        if(key == 'wc_order_field_1274'){
                           // order.Medical_Designation_Above_RN__c = value;
                        }
                        if(key == 'wc_order_field_3355'){
                            //order.Supply_Kit_User_License_Number__c = value;
                        }
                        if(key == 'wc_order_field_9653'){
                            //04031985 
                            if(value.indexOf('/') == -1)  {
                                if(value.length() == 8)  {
                                    String newValue = value.subString(0,2)+'/'+value.subString(2,4)+'/'+value.subString(4,8);
                                    value = newValue;
                                }else  {
                                    value = '';
                                }
                            }
                            //order.Billing_Date_of_Birth__c = value;
                        }
                    }
                }
            }
            
            if(licenseNumber != null && licenseNumber != ''){
                //accSobject.License_Number__c = order.License_Number__c;
                //upsert accSobject;
            }else {
                //accSobject.License_Number__c = null;
            }
            if(odw.customer_note != null && odw.customer_note.length() > 240)  {
                odw.customer_note = odw.customer_note.subString(0,240);
            }
            //order.Order_Notes__c = odw.customer_note;
            //order.Shipping_Tax__c = odw.shipping_tax;
            order.Status = odw.status;
            //order.Tax_Total__c = odw.total_tax;
            if(odw.transaction_id != null && odw.transaction_id != ''){
                //order.Transaction_ID__c = Decimal.valueOf(odw.transaction_id);    
            }
            //upsert order;
            
            List<OrderLineItems> lstOrderLinteItems = odw.line_items;
            if(lstOrderLinteItems == null || lstOrderLinteItems.size() == 0)  {
                //No Product found, so we can't do anything.
                return order.id;
            }
            OrderLineItems orderLineItemObj = lstOrderLinteItems.get(0);
            
            if(orderLineItemObj.product_id == null)  {
                //No Product Id found so we can't do anything.
                return order.id;
            }
            
           // List<OrderItem> orderItems = [select id,OrderItem_ID__c, Product2.Product_ID__c from OrderItem where OrderId =: order.Id ];
            //List<Product2> pro = [select Product_ID__c,name,RecordType.DeveloperName from Product2 where Product_ID__c =: orderLineItemObj.product_id and Product_ID__c != null];
            Boolean isCreateSeminarAccount = false;
            //List<Seminar__c> lstSeminars = null;
            /*if(pro.size() > 0)  { //It should be Webinar or Seminar Prodcut.
                String productRecordTypeName = pro.get(0).RecordType.DeveloperName;
                if(productRecordTypeName == 'Webinar')  { //it's webinar just create orderItem
                    order.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Webinar').getRecordTypeId();
                }else  { //It's Seminar create orderitem and seminars
                    
                    if(productRecordTypeName == 'Model')  {
                        String probableProdName = orderLineItemObj.name+' seminar';
                		system.debug('probableProdName#####'+probableProdName);
                		String regex = ' ?- ?((1[0-2]|0?[1-9]):([0-5][0-9]) ?([AaPp][Mm]))';
                		Pattern regexPattern = Pattern.compile(regex);
                		Matcher regexMatcher = regexPattern.matcher(probableProdName);
                		if(regexMatcher.find()) {
                    		probableProdName = probableProdName.replaceAll(regex, '');
                		}
                		system.debug('probableProdName###'+probableProdName);
                        order.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Model').getRecordTypeId();
                        //lstSeminars = [select id,name,Product__c,Model_Seminar_Name__c from Seminar__c where Model_Seminar_Name__c =:probableProdName and Product__c != null];
                    }else  {
                        String probableProdName = orderLineItemObj.name+' seminar';
                    	order.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Participant').getRecordTypeId();
                        //lstSeminars = [select id,name,Product__c from Seminar__c where Seminar_Name__c =:probableProdName and Product__c != null];
                    }    
                    isCreateSeminarAccount = true;
                    
                    
                }
            }*/
            /*else  { //It could be Modal Product create seminar and orderitems.
                System.debug('HERE I AM WHICH SHOULD NEVER EVER HAVE HAPPEN');
                String probableProdName = orderLineItemObj.name+' seminar';
                system.debug('probableProdName#####'+probableProdName);
                String regex = ' ?- ?((1[0-2]|0?[1-9]):([0-5][0-9]) ?([AaPp][Mm]))';
                Pattern regexPattern = Pattern.compile(regex);
                Matcher regexMatcher = regexPattern.matcher(probableProdName);
                if(regexMatcher.find()) {
                    probableProdName = probableProdName.replaceAll(regex, '');
                }
                system.debug('probableProdName###'+probableProdName);
                // replace what ever match with above regex and after that use probableProdName
                //lstSeminars = [select id,name,Product__c,Model_Seminar_Name__c from Seminar__c where Model_Seminar_Name__c =:probableProdName and Product__c != null];
                //system.debug(lstSeminars);
                //if(lstSeminars.size() ==  0 )  {
                  //  return order.Id;
                //}
               // else  {
                 //   pro.add(new Product2(Id=lstSeminars.get(0).Product__c));
                   // order.recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Model').getRecordTypeId();
                    //isCreateSeminarAccount = true;
                //}
            }            */
            
            //upsertOrderItem(mapActualJSON,lstOrderBilling,odw,orderLineItemObj,order,pro.get(0),pricebook2Id);    
            //update Order;
            
            /*if(isCreateSeminarAccount && lstSeminars!=null && lstSeminars.size()>0)  {
                Seminar_Account__c  SeminarAccount = new Seminar_Account__c ();
                SeminarAccount.Seminar_Account_EXT__c  = order.id + '' + pro.get(0).Id;
                SeminarAccount.OrderItem_ID__c = orderLineItemObj.Id;
                SeminarAccount.account__c = accSobject.id;
                SeminarAccount.Account_Email__c = lstOrderBilling.email;
                SeminarAccount.Account_License_Number__c = accSobject.License_Number__c;
                if(order.Billing_Date_of_Birth__c!=null && order.Billing_Date_of_Birth__c!=''){
                    try  {
                    	SeminarAccount.Account_Date_of_Birth__c = date.parse(order.Billing_Date_of_Birth__c);    
                    }catch(Exception ex)  {
                        
                    }    
                }
                //SeminarAccount.Amount_Due__c = odw.total;
                if(order.recordTypeId==Schema.SObjectType.Order.getRecordTypeInfosByName().get('Participant').getRecordTypeId()){
                    SeminarAccount.participant_seminar__c  = lstSeminars.get(0).Id; 
                    SeminarAccount.recordTypeId = Schema.SObjectType.Seminar_Account__c.getRecordTypeInfosByName().get('Participant').getRecordTypeId();
                }else if(order.recordTypeId==Schema.SObjectType.Order.getRecordTypeInfosByName().get('Model').getRecordTypeId()){
                    SeminarAccount.model_Seminar__c  = lstSeminars.get(0).Id;  
                    SeminarAccount.recordTypeId = Schema.SObjectType.Seminar_Account__c.getRecordTypeInfosByName().get('Model').getRecordTypeId();
                    String regex = ' ?((1[0-2]|0?[1-9]):([0-5][0-9]) ?([AaPp][Mm]))';
                    Pattern regexPattern = Pattern.compile(regex);
                    Matcher regexMatcher = regexPattern.matcher(orderLineItemObj.name);
                    if(regexMatcher.find()) {
                        SeminarAccount.Model_Variation_time__c = regexMatcher.group(0);
                    }
                }
                SeminarAccount.seminar__c = lstSeminars.get(0).Id;
                SeminarAccount.Order__c = order.id;
                Database.upsert(SeminarAccount, Seminar_Account__c.Seminar_Account_EXT__c );
            }*/
            return order.ID;
        }catch(Exception e)  {
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            
            try {
                OrderWrapper odw = null;
                odw = (OrderWrapper)JSON.deserialize(RestContext.request.requestBody.toString(), OrderWrapper.class);
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'OrderRestResource',
                        'doPost()',
                        odw.Id,
                        e
                    )
                );
                return e.getMessage();
            }catch(Exception ex)  {
                System.debug(ex.getMessage());
                System.debug(ex.getLineNumber());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'OrderRestResource',
                        'doPost()',
                        '',
                        e
                    )
                );
                return e.getMessage();
            }    
        }
    }
    
    /*global static OrderItem upsertOrderItem(Map<String,Object> mapActualJSON,OrderBilling lstOrderBilling,OrderWrapper odw,OrderLineItems orderLineItemObj,Order order,Product2 sfProduct,Id pricebook2Id)  {
        List<OrderItem> orderItems = [select id,OrderItem_ID__c,Product2Id from OrderItem where  OrderId =: order.Id ];
        OrderItem orderitem = new OrderItem();
        Boolean isUpdate = false;
        if(orderItems.size() > 0)  {
            for(OrderItem oi : orderItems)  {
                if(oi.Product2Id == sfProduct.Id)  {
            		orderitem.id = orderItems.get(0).Id;   
                    isUpdate = true;
                }
            }
            
        } 
        
        if(!isUpdate){
            orderitem.OrderId = order.id;
            orderitem.Product2Id = sfProduct.Id; 
            List<PricebookEntry> lstPricebooks = [select id,Product2Id from PricebookEntry where Product2Id = :sfProduct.Id and PriceBook2Id =: pricebook2Id ];
            orderitem.PricebookEntryId = lstPricebooks.get(0).id; 
        }
        
        order.Product_purchased__c = sfProduct.Id;
        orderitem.OrderItem_ID__c = orderLineItemObj.id;
        
        if(orderLineItemObj.price != null && orderLineItemObj.price != '')  {
            orderitem.UnitPrice  = Decimal.valueOf(orderLineItemObj.price);
        }
        if(orderLineItemObj.quantity != null && orderLineItemObj.quantity != '')  {
            orderitem.Quantity = Decimal.valueOf(orderLineItemObj.quantity);
        }
        
        upsert orderitem;
        return orderitem;
    }
    global static Account upsertAccount(Map<String,Object> mapActualJSON,OrderBilling lstOrderBilling,OrderWrapper odw)  {
        List<Account> lstA = [select id from Account where Billing_Email__c =: lstOrderBilling.email and Billing_Email__c != null];
        Account accSobject = new Account();
        if(lstA.size() > 0)  {
            accSobject = lstA.get(0);
        }
        //OrderBilling lstOrderBilling = odw.billing;
        accSobject.name = lstOrderBilling.first_name+' '+lstOrderBilling.last_name;
        // return accSobject.name;
        accSobject.User_Email__c = lstOrderBilling.email;
        accSobject.User_Name__c = lstOrderBilling.first_name + odw.id;
        accSobject.Password__c = '123';
        accSobject.First_Name__c = lstOrderBilling.first_name;
        accSobject.Last_Name__c = lstOrderBilling.last_name;
        accSobject.Billing_First_Name__c = lstOrderBilling.first_name;
        accSobject.Billing_Last_Name__c = lstOrderBilling.last_name;
        accSobject.Billing_Company__c = lstOrderBilling.company;
        accSobject.BillingStreet = lstOrderBilling.address_1;
        accSobject.BillingCity = lstOrderBilling.city;
        accSobject.BillingState = lstOrderBilling.state;
        accSobject.BillingPostalCode = lstOrderBilling.postcode;
        accSobject.BillingCountry = lstOrderBilling.country;
        accSobject.Billing_Email__c = lstOrderBilling.email;
        accSobject.Phone = lstOrderBilling.phone;
        OrderShipping lstOrderShipping = odw.shipping;
        accSobject.Shipping_First_Name__c = lstOrderShipping.first_name;
        accSobject.Shipping_Last_Name__c = lstOrderShipping.last_name;
        accSobject.ShippingStreet = lstOrderShipping.address_1;
        accSobject.ShippingCity = lstOrderShipping.city;
        accSobject.Shipping_Company__c = lstOrderShipping.company;
        accSobject.ShippingState = lstOrderShipping.state;
        accSobject.ShippingPostalCode = lstOrderShipping.postcode;
        accSobject.ShippingCountry = lstOrderShipping.country;
        if(accSobject.name == null || accSobject.name.trim() == '')  {
            accSobject.name = 'Name Missing';
        }
        // set account record type as peron account and activated
        upsert accSobject;
        return accSobject;
    }*/
}