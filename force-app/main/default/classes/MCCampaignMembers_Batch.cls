//@Developer - Ashish Sharma
//@Description - This job moves marketing cloud  campaign members to their next campaign as per age in hours ,
                 //it doen not delete campaignmember from previous campaign.
public class MCCampaignMembers_Batch implements Database.Batchable<sObject>{
   public String query;
    
    public MCCampaignMembers_Batch(){   
        query =  'SELECT Id,Name,CampaignId,Product__c,Marketing_Cloud_Tag__c,Contact.Product__c,Created_Date__c,Created_Date_Time__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Marketing_Cloud_Tags__c,Campaign.Name, Age__c, Age_In_Hours__c ,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = false and Campaign.Marketing_Cloud_Tags__c != null and (NOT Campaign.Name Like \'%trigger%\')  and MC_Campaign_Movement__c = false and Age_In_Hours__c > 24 and Contact.Amazon_Customer__c=false and Campaign.Type=\'Marketing Cloud\'';
        System.debug('------query-----'+query);
       //query =  'SELECT Id,Name, CampaignId,Created_Date__c,Product__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c, Campaign.Name, Age__c , LeadId, ContactId FROM CampaignMember WHERE LeadId = \'00Q2F000002lNbS\'';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
      System.debug('**** Batch Query String ---->'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<CampaignMember> scope){
        System.debug('---------scope-------'+scope);
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new Map<String,String>();
        List<Campaign> campaignList = [SELECT Id, Name, Product__c, Marketing_Cloud_Tags__c, Max_Age__c, Min_Age__c  FROM Campaign FOR UPDATE];
        
        Map<Id,List<Workflow_Configuration__c>> mapWorkflowConfig = new Map<Id,List<Workflow_Configuration__c>>();
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign c:campaignList){
            campaignIds.add(c.Id);
        }
        
        for(Workflow_Configuration__c  wc:[select id,Source_Campaign__c ,Target_Campaign__c,Products__c,Special_Movement__c from Workflow_Configuration__c where Source_Campaign__c in:campaignIds]){
            if(mapWorkflowConfig.containskey(wc.Source_Campaign__c)){
                mapWorkflowConfig.get(wc.Source_Campaign__c).add(wc);
            }else{
                mapWorkflowConfig.put(wc.Source_Campaign__c,new List<Workflow_Configuration__c>{wc});
            }
        }
                
        MCCampaignMembers_Batch_Handler.updateCampaignMembers(campaignList, scope,mapWorkflowConfig);
        
        
    }
    
    public void finish(Database.BatchableContext BC){
        //database.executeBatch(new MCCampaignMemberDelBatch());

    }
    
}