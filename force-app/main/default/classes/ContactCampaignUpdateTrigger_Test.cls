@isTest
private class ContactCampaignUpdateTrigger_Test {
    
    public testmethod Static void onInsertTest(){
        // Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        Contact con = new Contact();
        con.LastName = 'test lName';
        con.Product__c = 'UTA';
        //con.AccountId = acc.Id;
        con.Phone = '9425063186';
        con.DoNotCall = false;
        con.Mobile_State_DNC__c = true;
        con.MobilePhone = '9452368745';
        con.OtherPhone = '9865745321';
        con.Potential_Buyer__c= 'BookLead-1 Day';
        insert con;
        Contact con3 = new Contact();
        con3.LastName = 'test lName';
        con3.Potential_Buyer__c= 'BookLead-1 Day';
        // con3.AccountId = acc.Id;
        insert con3;

        Campaign sc = new Campaign();
        sc.Name = 'CoachLead-1 Day';
        insert sc;
        Campaign dc = new Campaign();
        dc.Name = 'CoachLead-1 Buy Day';
        insert dc;
        
        Workflow_Configuration__c wc = new Workflow_Configuration__c();
        wc.Source_Campaign__c = sc.Id;
        wc.Products__c = 'UTA';
        wc.Target_Campaign__c = dc.Id;
        wc.Active__c = true;
        insert wc;
        
        Five9LSP__Five9_List__c five = new Five9LSP__Five9_List__c();
        five.Name ='Master List';
        five.Five9LSP__Five9_Domain__c='abc';
        five.Five9LSP__Five9_User_Name__c = 'test1';
        five.Five9LSP__Five9_User_Password__c = 'abcdfed';
        five.Five9LSP__API_URL__c = 'https://api.five9.com';
        insert five;
        
        
        Five9Credentials__c five_Cred = new Five9Credentials__c();
        five_Cred.UserName__c = 'Ashish@Username';
        five_Cred.Password__c = 'Ashish@1234';
        five_Cred.RealPhoneValidationKey__c ='PhoneKey';
        five_Cred.IsLive__c = true;
        insert five_Cred;
        
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        contactMap.put(con.Id, con);
        
        
        
        
        
        /*   List<Five9LSP__Five9_List_Item__c> Five_list = new List<Five9LSP__Five9_List_Item__c>();
Five9LSP__Five9_List_Item__c fiveLine = new Five9LSP__Five9_List_Item__c();
fiveLine.Five9LSP__Five9_List__c = five.Id;
fiveLine.Five9LSP__Contact__c = con.Id;
Five_list.add(fiveLine);
insert Five_list;
*/   
        test.startTest();
        con.LastName = 'lName';
        con.Product__c ='Coaching';
        con.Phone = '9425315668';
        Update con;
        
        five_Cred.IsLive__c = false;
        update five_Cred;
        
        con.DoNotCall = true;
        con.OtherPhone = '565655';
        con.Other_Phone_National_DNC__c = true;
        con.Phone_National_DNC__c = true;
        con.MobilePhone = '332323232';
        update con;
        
        con.DoNotCall = false;
        con.Phone_National_DNC__c = false;
        con.Other_Phone_National_DNC__c = false;
        update con;
        test.stopTest();
        
    }
}