global class ShipStationCancelIterable implements iterable<Integer>{
   global Iterator<Integer> Iterator(){
      return new ShipStationCancelIterator();
   }
}