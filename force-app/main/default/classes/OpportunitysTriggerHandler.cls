public class OpportunitysTriggerHandler { 
    public static void stripeChargeCreation(List<Opportunity> Triggernew){
        for(Opportunity opp : Triggernew){
            if(opp.Stripe_Charge_Id__c !=null){
                opp.StripeChargeCreationId__c = opp.stripe_charge_id__c;
            }
        }
    }
    
    public static void OpportunityWeekHandler(List<Opportunity> Triggernew){
        try{
            if(OpportunityWeekTriggerFlag.runOnce==true){
                List<Opportunity> oppList = [SELECT id,refund_week__c,Scheduled_Payment_Date__c,CreatedDate,Current_Week__c,Week__c,Refunded_Date__c,Current_Refund_Week__c FROM Opportunity WHERE Id in : Triggernew];
                for( Opportunity opp : oppList){
                    DateTime dt;
                    Time tm = Time.newInstance(0, 0, 0, 0);
                    if(opp.Week__c>0){
                        if(opp.Current_Week__c==null || opp.Current_Week__c==0){
                            if(opp.Scheduled_Payment_Date__c!=null){
                                dt = DateTime.newInstance(opp.Scheduled_Payment_Date__c, tm);
                            }else{
                                dt = opp.CreatedDate;
                            }
                            opp.Current_Week__c = Integer.valueOf(dt.addDays(-1).format('w'));
                        }
                    }
                    if(opp.refund_week__c>0){
                        dt = DateTime.newInstance(opp.Refunded_Date__c, tm);
                        if(opp.Current_Refund_Week__c==null || opp.Current_Refund_Week__c==0){
                            opp.Current_Refund_Week__c = Integer.valueOf(dt.addDays(-1).format('w'));
                        }
                    }
                }
                OpportunityWeekTriggerFlag.runOnce=false;
                update oppList;
                if(Test.isRunningTest()){
                    System.debug(1/0);
                }
            }
        }Catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OpportunityWeekHandler','OpportunityWeekHandler',Triggernew[0].id,ex) );
        }
    }
    
    public static void OpportunityTimeDiffHandler(List<Opportunity> Triggernew,Map<Id,Opportunity> triggernewMap){
        try{
            Set<ID> conIdSet = new Set<Id>();
            Set<Id> currentOppId = new Set<Id>();
            for(Opportunity opp : Triggernew) {
                conIdSet.add(opp.Contact__c);
                currentOppId.add(opp.id);
            }
            List<Additional_Opportunity_Names__c> custOppName = new List<Additional_Opportunity_Names__c>();
            custOppName = Additional_Opportunity_Names__c.getall().values();
            Set<String> oppNames = new Set<String>();
            if(custOppName != null && !custOppName.isEmpty()) {
                for(Additional_Opportunity_Names__c oppNameVar : custOppName) {
                    oppNames.add(oppNameVar.Opportunity_Name__c);
                }
            }
            List<Opportunity> oppList = new List<Opportunity>();
            boolean check = false;
            oppList = [Select id,Name,Opportunities_Time_Difference__c,CreatedDate,createdTime__c from Opportunity where id =: triggernewMap.keySet() and Name =: oppNames];
            if(oppList != null && !oppList.isEmpty()){
                for(Opportunity opp : [Select id,Name,Contact__c,Statement_Descriptor__c,createdTime__c,CreatedDate from Opportunity where Contact__C != null and  Contact__c =: conIdSet and id !=: currentOppId and (Name = 'LadyBoss-LVCH-$1-Dig' OR Name = 'LadyBoss-LVCH-$27-Phy')]) {
                    for(Opportunity op : oppList) {
                        Long milliseconds = op.CreatedDate.getTime() - opp.CreatedDate.getTime();
                        Long seconds = milliseconds / 1000;
                        Long minutes = seconds / 60;
                        if(minutes <= 10) {
                            op.Opportunities_Time_Difference__c = true;
                        }
                        check = true ;
                    }
                }
            }
            if(check == true){
                update oppList; 
                
            }
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
        }Catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('OpportunityTimeDiffHandler','OpportunityTimeDiffHandler',Triggernew[0].id,ex) );
        }
    }
    
    public static void SuccessPaymentWithCaseLink(List<Opportunity> Triggernew){
        List<Triggers_activation__c> op = [select id, SuccessPaymentWithCaseLink__c,isCampaignAssociate__c from Triggers_activation__c];
        try{
            
            //This is to stop trigger firing or to avoid updates or to go into the triger body execution
            if(op[0].SuccessPaymentWithCaseLink__c){
                Set<Id> caseIdSet = new set<id>();
                Set<Id> oppIdSet = new set<id>();
                Set<string> custId  = new set<String>();
                Set<String>  emailID = new Set<String>();
                Set<Decimal>  amountSet = new Set<Decimal>();
                Map<string,List<case>> mapcaseList = new Map<string,List<case>>();
                Map<string,List<case>> mapCoachingCaseList = new Map<string,List<case>>();
                for(Opportunity opp : Triggernew) {
                    oppIdSet.add(opp.id);
                }
                List<Opportunity> oppList = [select id,CustomerID__c, Amount, Stripe_Charge_Id__c,contact__r.email,Scheduled_Payment_Date__c from opportunity where ID IN:oppIdSet];
                for(Opportunity opp : oppList) {
                    if((opp.Contact__r.email!=null && opp.CustomerID__c!=null && opp.CustomerID__c!='') && ((opp.Stripe_Charge_Id__c!=null && opp.Stripe_Charge_Id__c!='') || opp.Scheduled_Payment_Date__c !=null)){
                        emailID.add(opp.Contact__r.email);
                        amountSet.add(opp.Amount);
                        System.debug(' test data emailID opp.Contact__r.email '+opp.Contact__r.email);
                    }
                    
                }
                
                //   System.debug(' test data emailID '+emailID);
                
                
                
                //Get all the cases belongs to the customer
                //List<Case> caseList = [select id ,UpdateCardStatus__c, CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c from case where contact.Name!=null and IsFailed_Payment__c=true and (Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c!='') and Stripe_Customer_Id__c IN:custId and type='Account Recovery' and UpdateCardStatus__c!='Payment Successful' limit 10000 ];
                
                List<Case> caseList = [select id ,UpdateCardStatus__c,Contact.email, Successful_Payment_DateTime__c,CreatedDate ,Failed_Amount__c,Stripe_Customer_Id__c from case where  IsFailed_Payment__c=true and (Stripe_Customer_Id__c!=null and Stripe_Customer_Id__c!='') and ( Contact.Email!=null and  Contact.Email IN :emailID) and type='Account Recovery' and UpdateCardStatus__c!='Payment Successful' limit 10000 ];
                List<Case> coachingCaseList = new List<Case>();
                coachingCaseList = [select id,UpdateCardStatus__c, Successful_Payment_DateTime__c, Contact.email, Charge_Id__c, CoachingChargeSucceeded__c, Statement_Descriptor__c, Subject, Opportunity__c, Invoice_Id__c, Failed_Amount__c, ContactEmail from Case where Statement_Descriptor__c != null and CoachingChargeSucceeded__c = false and Failed_Amount__c != null and Failed_Amount__c =: amountSet and opportunity__c = null  and Is_Coaching_Failed__c = true  and ContactEmail =: emailID and ContactEmail != null];
                System.debug(' test data coachingCaseList '+coachingCaseList);
                
                for(Case cs : caseList) {
                    if(!mapcaseList.containsKey(cs.Contact.email)) {
                        List<Case> csList = new List<Case> ();
                        csList.add(cs);
                        mapcaseList.put(cs.Contact.email,csList);
                    }else{
                        List<Case> csList = new List<Case> ();
                        csList = mapcaseList.get(cs.Contact.email);
                        csList.add(cs);
                        mapcaseList.put(cs.Contact.email,csList);
                    }
                }
                for(Case cs : coachingCaseList) {
                    if(!mapCoachingCaseList.containsKey(cs.Contact.email)) {
                        List<Case> csList = new List<Case> ();
                        csList.add(cs);
                        mapCoachingCaseList.put(cs.Contact.email,csList);
                    }else{
                        List<Case> csList = new List<Case> ();
                        csList = mapCoachingCaseList.get(cs.Contact.email);
                        csList.add(cs);
                        mapCoachingCaseList.put(cs.Contact.email,csList);
                    }
                }
                List<Case> caseListUpdate = new List<Case>();
                List<Case> coachingCaseListUpdate = new List<Case>();
                //Updates if successful payment
                for(Opportunity opp : oppList) {
                    
                    if(mapcaseList.containsKey(opp.Contact__r.email)) {
                        List<Case> caseList1  = new List<Case>();
                        caseList1 =mapcaseList.get(opp.Contact__r.email);
                        if(caseList1 != null && !caseList1.isEmpty()){
                            
                            for(Case c : caseList1) {
                                
                                c.UpdateCardStatus__c = 'Payment Successful';
                                c.Opportunity__c = opp.id;
                                c.Successful_Payment_DateTime__c = System.Now();
                                caseListUpdate.add(c);
                                
                            }
                        }
                    }
                    
                    if(mapCoachingCaseList != null && !mapCoachingCaseList.keySet().isEmpty() && mapCoachingCaseList.containsKey(opp.Contact__r.email)) {
                        List<Case> caseList1  = new List<Case>();
                        caseList1 =mapCoachingCaseList.get(opp.Contact__r.email);
                        System.debug(' test data caseList1 '+caseList1);
                        if(caseList1 != null && !caseList1.isEmpty()){
                            for(Case c : caseList1) {
                                
                                c.UpdateCardStatus__c = 'Payment Successful';
                                c.Opportunity__c = opp.id;
                                c.Successful_Payment_DateTime__c = System.Now();
                                c.CoachingChargeSucceeded__c = true;
                                coachingCaseListUpdate.add(c);
                                
                            }
                        }
                    }
                }
                
                if(caseListUpdate.size()>0){
                    update caseListUpdate;
                }
                if(coachingCaseListUpdate.size()>0){
                    update coachingCaseListUpdate;
                }
                
            }
            if(op[0].isCampaignAssociate__c){
                AssociateOpportunityCampaign.updateCampaignUpdate(Triggernew);
            }
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('SuccessPaymentWithCaseLink','SuccessPaymentWithCaseLink','',e));
        }
    }
    
    public static void NMIStripeBeforeInsert(List<Opportunity> Triggernew){
        try{
            Map<Id, Account> accountMap = new Map<Id, Account>();
            Map<Id, List<Opportunitylineitem>> opportunityOlisMap = new Map<Id, List<Opportunitylineitem>>();
            Integer Nmi = 0;
            Integer stripe = 0;
            for(Opportunity opportunity:Triggernew){
                if(opportunity.Payment_Date__c != null || test.isRunningTest()){
                    accountMap.put(opportunity.accountId, new Account());
                    opportunityOlisMap.put(opportunity.id, new List<Opportunitylineitem>());
                }
            }
            for(Account account:[select id, NMI_External_ID__c, External_ID__c, Card_ID__c from Account where id in:accountMap.keySet()]) accountMap.put(account.Id, account);
            for(Opportunitylineitem oli:[select id, OpportunityId, Product2.Type__c from Opportunitylineitem where OpportunityId in:opportunityOlisMap.keySet()]) opportunityOlisMap.get(oli.OpportunityId).add(oli);
            for(Opportunity opportunity:Triggernew){ 
                if(opportunity.Payment_Date__c != null){
                    if(opportunity.StageName == 'Approved' || (opportunity.Subscription__c != null && (opportunity.External_ID__c != null || opportunity.NMI_External_ID__c != null))) opportunity.addError('This opportunity already registers a payment.');
                    if(opportunity.Total__c <= 0) opportunity.addError('The amount must be greater than 0.');
                    for(Opportunitylineitem oli:opportunityOlisMap.get(Opportunity.id)){
                        if(oli.Product2.Type__c == 'NMI') nmi++;
                        if(oli.Product2.Type__c == 'Stripe') stripe++;
                    }
                    if(opportunityOlisMap.get(Opportunity.id).size() != nmi) opportunity.addError('Please the products must be of the Nmi type.');
                    else if(opportunityOlisMap.get(Opportunity.id).size() == nmi) opportunity.Payment_Type__c = 'Nmi';
                    else if(opportunityOlisMap.get(Opportunity.id).size() == stripe) opportunity.Payment_Type__c = 'Stripe';
                    if(opportunity.Payment_Type__c == 'Nmi' && accountMap.get(opportunity.accountId).NMI_External_ID__c == null) opportunity.addError('The account is not registered in NMI.');
                } 
            }
            list< Triggers_activation__c > activateTrigger = [select id, Opportunitytriggerhandler__c,isCampaignAssociate__c from Triggers_activation__c limit 1];
            system.debug('----------activateTrigger--------------'+activateTrigger);
            if(activateTrigger.size() >0 && activateTrigger[0].Opportunitytriggerhandler__c){
                Opportunitytriggerhandler.opportunityupdate(triggernew);
            }
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('NMIStripeBeforeInsert','NMIStripeBeforeInsert','',e));
        }
    }
    
    public static void NMIStripeBeforeUpdate(List<Opportunity> Triggernew,Map<Id,Opportunity> TriggeroldMap){
        try{
            Map<Id, Account> accountMap = new Map<Id, Account>();
            Map<Id, List<Opportunitylineitem>> opportunityOlisMap = new Map<Id, List<Opportunitylineitem>>();
            Integer Nmi = 0;
            Integer stripe = 0;
            for(Opportunity opportunity:Triggernew){
                if(opportunity.Payment_Date__c != TriggeroldMap.get(opportunity.id).Payment_Date__c && opportunity.Payment_Date__c != null || test.isRunningTest()){
                    accountMap.put(opportunity.accountId, new Account());
                    opportunityOlisMap.put(opportunity.id, new List<Opportunitylineitem>());
                } 
            }
            for(Account account:[select id, NMI_External_ID__c, External_ID__c, Card_ID__c from Account where id in:accountMap.keySet()]) accountMap.put(account.Id, account);
            for(Opportunitylineitem oli:[select id, OpportunityId, Product2.Type__c from Opportunitylineitem where OpportunityId in:opportunityOlisMap.keySet()]) opportunityOlisMap.get(oli.OpportunityId).add(oli);
            for(Opportunity opportunity:Triggernew){
                if(opportunity.Payment_Date__c != TriggeroldMap.get(opportunity.id).Payment_Date__c && opportunity.Payment_Date__c != null){
                    if(opportunity.StageName == 'Approved' || (opportunity.Subscription__c != null && (opportunity.External_ID__c != null || opportunity.NMI_External_ID__c != null))) opportunity.addError('This opportunity already registers a payment.');
                    if(opportunity.Total__c <= 0) opportunity.addError('The amount must be greater than 0.');
                    for(Opportunitylineitem oli:opportunityOlisMap.get(Opportunity.id)){
                        if(oli.Product2.Type__c == 'NMI') nmi++;
                        if(oli.Product2.Type__c == 'Stripe') stripe++;
                    }
                    if(opportunityOlisMap.get(Opportunity.id).size() != nmi) opportunity.addError('Please the products must be of the Nmi type.');
                    else if(opportunityOlisMap.get(Opportunity.id).size() == nmi) opportunity.Payment_Type__c = 'Nmi';
                    else if(opportunityOlisMap.get(Opportunity.id).size() == stripe) opportunity.Payment_Type__c = 'Stripe';
                    if(opportunity.Payment_Type__c == 'Nmi' && accountMap.get(opportunity.accountId).NMI_External_ID__c == null) opportunity.addError('The account is not registered in NMI.');
                } 
            } 
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('NMIStripeBeforeUpdate','NMIStripeBeforeUpdate','',e));
        }
    }
    
    public static void firstSellCheckHandler(List<Opportunity> Triggernew){
        try{
            for(Opportunity opp :Triggernew){
                system.debug('----------------'+opp.id+'-'+opp.contact_email__c+' '+opp.Contact__c);
                if(opp.id != null && opp.contact_email__c != null  && opp.Contact__c != null ){
                    //   future method for check New Sale 
                    if(!(System.isBatch() || System.isFuture())){
                        Async_Apex_Limit__c  asyncLimit = new Async_Apex_Limit__c();
                        asyncLimit = [SELECT Remaining__c, Name From Async_Apex_Limit__c WHERE Name ='Async Limit Left'];
                        if (asyncLimit != null && asyncLimit.Remaining__c != null && asyncLimit.Remaining__c>1000) {
                            CustomerCheck.checkShopifyCustomer(opp.id);
                            //Marks first web sell on contact and opportunity
                            CustomerCheck.getContactsWithMetadata(opp.contact_email__c );
                        }
                    }
                }    
            }  
            
            list< Triggers_activation__c > activateTrigger = [select id, isCampaignAssociate__c from Triggers_activation__c limit 1];
            system.debug('----------activateTrigger--------------'+activateTrigger);
            if(activateTrigger.size()>0 && activateTrigger[0].isCampaignAssociate__c){
                AssociateOpportunityCampaign.updateCampaignUpdate(Triggernew);
            }
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('firstSellCheckHandler','firstSellCheckHandler','',e));
        }
    }
    
    public static void rollupDelete(List<Opportunity> Triggerold){
        List<Opportunity>  oppList = new List<Opportunity>(); 
        for(Opportunity opp : Triggerold) {
            if(opp != null && opp.contact__c != null && opp.Amount != null) {
                oppList.add(opp);
            }
        }
        if(oppList != null && !oppList.isEmpty()){
            rollupHandler(oppList);
        }
    }
    
    public static void rollupAfterUpdate(List<Opportunity> Triggernew,Map<id,Opportunity> triggeroldMap){
        List<Opportunity>  oppList = new List<Opportunity>(); 
        List<Opportunity> oppListRefund = new List<Opportunity>();
        for(Opportunity opp : Triggernew ) {
            if(opp != null && opp.contact__c != null && opp.Amount != null) {
                if( triggeroldMap.get(opp.id).amount != opp.amount || triggeroldMap.get(opp.id).Refund_Amount__c != opp.Refund_Amount__c) {
                    system.debug('inside rollup');
                    oppList.add(opp);
                }
                if (triggeroldMap.get(opp.id).Refund_Amount__c != opp.Refund_Amount__c) {
                    oppListRefund.add(opp);
                }
            } 
        }
        if(oppList != null && !oppList.isEmpty()){
            rollupHandler(oppList);
        }
        if(oppListRefund != null && !oppListRefund.isEmpty()){
            refundPapCommission(oppListRefund,triggeroldMap);
        }
    }
    
    public static void rollupInsertUndelete(List<Opportunity> Triggernew){
        List<Opportunity>  oppList = new List<Opportunity>(); 
        for(Opportunity opp : Triggernew ) {
            if(opp != null && opp.contact__c != null && opp.Amount != null) {
                oppList.add(opp);
            }
        }
        if(oppList != null && !oppList.isEmpty()){
            rollupHandler(oppList);
        }
        
    }
    
    //----------------- Method to check Pap Commission match with the Ordr Id and Product Id ------------//
    public static void updatePapCommision(List<Opportunity> Triggernew) {
        try{
            Set<Id> oppIdSet = new Set<Id>();
            Set<String> oppOrderIdSet = new Set<String>();
            Set<String> oppProductIdSet = new Set<String>();
            Map<id,String> oppOrdIDMap = new map<Id,String>();
            Map<id,String> oppProductIDMap = new map<Id,String>();
            for(Opportunity opp : TriggerNew) {
                if (opp.WC_Order_Number__c != null && opp.WC_Product_Id__c != null && opp.WC_Product_Id__c != '' && opp.WC_Order_Number__c != '') {                
                    oppOrderIdSet.add(opp.WC_Order_Number__c);
                    oppProductIdSet.adD(opp.WC_Product_Id__c);
                    oppOrdIDMap.put(opp.Id,opp.WC_Order_Number__c);
                    oppProductIDMap.put(opp.Id,opp.WC_Product_Id__c);
                }
            }
            
            List<Pap_Commission__c> papList = new List<Pap_Commission__c>();
            papList = [Select id,order_id__c,product_id__c,affiliate_ref_id__c,data5__c from Pap_Commission__c where data5__c IN: oppOrderIdSet AND data5__c != null AND product_id__c != null];
            if(papList != null && papList.size() > 0) {
                for(Opportunity op : TriggerNew) {
                    for(Pap_Commission__c pa : papList) {
                        if((oppOrdIDMap.get(op.id) == pa.data5__c && oppProductIDMap.get(op.id) == pa.product_id__c) || (oppOrdIDMap.get(op.id).equals( pa.data5__c) && oppProductIDMap.get(op.id).equals( pa.product_id__c)) ) {
                            pa.Opportunity__c = op.id;
                            
                            
                            
                        }
                        
                    }
                }
                
                if(papList.size() > 0) {
                    update papList;
                }
                
            }
        }
        Catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('updatePapCommision','updatePapCommision','',ex) );
        }
    }
    public static void updatePapCommision2(List<Opportunity> Triggernew) {
        try{
            Set<Id> oppIdSet = new Set<Id>();
            Set<String> oppOrderIdSet = new Set<String>();
            Set<String> oppProductIdSet = new Set<String>();
            Map<id,String> oppOrdIDMap = new map<Id,String>();
            Map<id,String> oppProductIDMap = new map<Id,String>();
            for(Opportunity opp : TriggerNew) {
                if (opp.WC_Order_Number__c != null && opp.WC_Product_Id__c != null && opp.WC_Product_Id__c != '' && opp.WC_Order_Number__c != '') {                
                    oppOrderIdSet.add(opp.WC_Order_Number__c);
                    oppProductIdSet.adD(opp.WC_Product_Id__c);
                    oppOrdIDMap.put(opp.Id,opp.WC_Order_Number__c);
                    oppProductIDMap.put(opp.Id,opp.WC_Product_Id__c);
                }
            }
            
            List<Pap_Commission__c> papList = new List<Pap_Commission__c>();
            papList = [Select id,order_id__c,product_id__c,affiliate_ref_id__c,data5__c from Pap_Commission__c where data5__c IN: oppOrderIdSet AND data5__c != null AND product_id__c != null];
            if(papList != null && papList.size() > 0) {
                List<Id> updateTriggerNew = new List<Id>();
                Map<Id,String> couponCodeMap = new Map<Id,String>();
                for(Opportunity op : TriggerNew) {
                    for(Pap_Commission__c pa : papList) {
                        if((oppOrdIDMap.get(op.id) == pa.data5__c && oppProductIDMap.get(op.id) == pa.product_id__c) || (oppOrdIDMap.get(op.id).equals( pa.data5__c) && oppProductIDMap.get(op.id).equals( pa.product_id__c)) ) {
                            //pa.Opportunity__c = op.id;
                            couponCodeMap.put(op.id, pa.affiliate_ref_id__c);
                            //op.Coupon_Code__c = pa.affiliate_ref_id__c;
                            updateTriggerNew.add(op.Id);
                        }
                        
                    }
                }
                
                /*if(papList.size() > 0) {
update papList;
}*/
                if(updateTriggerNew.size()>0){
                    List<Opportunity> oppList = [select id,Coupon_Code__c from Opportunity where id in :updateTriggerNew];
                    for(Opportunity opp:oppList){
                        opp.Coupon_Code__c = couponCodeMap.get(opp.Id);
                    }
                    //update oppList;
                }
            }
        }
        Catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('updatePapCommision','updatePapCommision','',ex) );
        }
    }
    //-----------------... Method to check Pap Commission match with the Ordr Id and Product Id ------------//
    
    public static void rollupHandler(Opportunity[] objects){
        try{
            /*
First step is to create a context for LREngine, by specifying parent and child objects and
lookup relationship field name
*/
            LREngine.Context ctx = new LREngine.Context(Contact.SobjectType, // parent object
                                                        Opportunity.SobjectType,  // child object
                                                        Schema.SObjectType.Opportunity.fields.Contact__c // relationship field name
                                                       );     
            /*
Next, one can add multiple rollup fields on the above relationship. 
Here specify 
1. The field to aggregate in child object
2. The field to which aggregated value will be saved in master/parent object
3. The aggregate operation to be done i.e. SUM, AVG, COUNT, MIN/MAX
*/
            ctx.add(
                new LREngine.RollupSummaryField(
                    Schema.SObjectType.Contact.fields.Total_Spend_Amount__c,
                    Schema.SObjectType.Opportunity.fields.Amount,
                    LREngine.RollupOperation.Sum 
                )); 
            ctx.add(
                new LREngine.RollupSummaryField(
                    Schema.SObjectType.Contact.fields.Spend_Without_Refund__c,
                    Schema.SObjectType.Opportunity.fields.Spend_Amount__c,
                    LREngine.RollupOperation.Sum 
                )); 
            /* 
Calling rollup method returns in memory master objects with aggregated values in them. 
Please note these master records are not persisted back, so that client gets a chance 
to post process them after rollup
*/ 
            if(objects != null ){
                Sobject[] masters = LREngine.rollUp(ctx, objects);    
                
                // Persiste the changes in master
                update masters;
            }
            if(Test.isRunningTest()){
                System.debug(1/0);
            }
            
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('rollupHandler','rollupHandler','',e));
        }
    }
    public static void refundPapCommission(List<Opportunity> oppList,Map<id,Opportunity> triggeroldMap){
        List<Pap_Commission__c> papList = [SELECT Id,order_id__c,product_id__c,commission__c,data1__c,data2__c,data4__c,Opportunity__c,Contact__c,Campaign_Id__c,Campaign_name__c,affiliate_ref_id__c FROM Pap_Commission__c WHERE Opportunity__c IN:oppList and Commission_Type__c != 'refund'];
        List<Pap_Commission__c> papToInsert = new List<Pap_Commission__c>();
        for (Pap_Commission__c pc : papList) {
            for (Opportunity opp: oppList) {
                if (pc.commission__c > 0 && pc.Opportunity__c == opp.Id) {
                    Decimal Rate = 0;
                    Decimal totalAmount = 0;
                    List<Opportunitylineitem> opli = [SELECT id,Tax_Amount__c FROM Opportunitylineitem WHERE OpportunityId =:pc.Opportunity__c];
                    
                    Decimal rfndNew = opp.Refund_Amount__c - triggeroldMap.get(opp.Id).Refund_Amount__c;
                    
                    if(opp.Core_Amount__c!=null && opp.Core_Amount__c>0){
                        totalAmount = opp.Core_Amount__c;
                        // check if full refund
                        if(opp.Core_Amount__c<=rfndNew){
                            rfndNew = opp.Core_Amount__c;
                        }
                    }else{
                        if (opli.size() > 0) {
                            totalAmount = opp.Amount ;
                        }
                        else{
                            totalAmount = opp.Amount ;
                            if (opp.Tax__c != null && opp.Tax__c != 0) {
                                totalAmount = totalAmount - opp.Tax__c;
                            }
                            if (opp.Shipping_Cost__c != null && opp.Shipping_Cost__c != 0) {
                                totalAmount = totalAmount - opp.Shipping_Cost__c;
                            }
                        }
                    }
                    
                    Rate = pc.commission__c/totalAmount;
                    
                    system.debug('inside rollup');
                    Pap_Commission__c pap = new Pap_Commission__c();
                    pap.Contact__c = pc.Contact__c;
                    pap.Opportunity__c = opp.Id;
                    pap.commission__c = - rfndNew*Rate;
                    pap.Commission_Type__c = 'refund';
                    pap.Campaign_Id__c = pc.Campaign_Id__c;
                    pap.Campaign_name__c = pc.Campaign_name__c;
                    pap.affiliate_ref_id__c = pc.affiliate_ref_id__c;
                    pap.order_id__c = pc.order_id__c;
                    pap.product_id__c = pc.product_id__c;
                    pap.data1__c = pc.data1__c;
                    pap.data2__c = pc.data2__c;
                    pap.data4__c = pc.data4__c;
                    pap.data5__c = opp.WC_Order_Number__c;
                    papToInsert.add(pap);
                    
                }
            }
        }
        if (papToInsert.size()>0) {
            insert papToInsert;
        }
    }
}