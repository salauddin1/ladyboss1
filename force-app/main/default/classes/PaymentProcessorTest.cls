@isTest
public class PaymentProcessorTest {
    @isTest
    public static void testMethod1(){
        Dynamic_Item_Count_Cost__c dy = new Dynamic_Item_Count_Cost__c();
        dy.Cost_Per_Item__c =2;
        dy.Fix_cost__c =3;
        dy.max_count__c =2;
        dy.min_count__c =7;
        insert dy;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        oL1.Quantity = 2;
        insert oL1;
        List<Id> idOliList = new List<Id>();
        idOliList.add(oL1.id);
        PaymentProcessor.paymentProcess(idOliList);
    }
    @isTest
    public static void testMethod2(){
        Dynamic_Item_Count_Cost__c dy = new Dynamic_Item_Count_Cost__c();
        dy.Cost_Per_Item__c =2;
        dy.Fix_cost__c =3;
        dy.max_count__c =2;
        dy.min_count__c =7;
        insert dy;
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Shipping_cost_1__c = null;
        prod.Shipping_cost_2__c = null;
        prod.Shipping_cost_3__c = null;
        prod.Shipping_cost_4__c = null;
        prod.Shipping_cost_5__c = null;
        prod.Dynamic_Fulfillment_Item_Count__c = 5;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        oL1.Quantity = 2;
        insert oL1;
        List<Id> idOliList = new List<Id>();
        idOliList.add(oL1.id);
        PaymentProcessor.paymentProcess(idOliList);
        PaymentProcessor.paymentProcess_future(idOliList);
    }
}