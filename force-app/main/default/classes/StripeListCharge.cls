public class StripeListCharge {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class Charges {
		public String object_Z {get;set;} // in json: object
		public String url {get;set;} 
		public Boolean has_more {get;set;} 
		public List<Data> data {get;set;} 

		public Charges(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'url') {
							url = parser.getText();
						} else if (text == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text == 'data') {
							data = new List<Data>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data.add(new Data(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Refunds {
		public String object_Z {get;set;} // in json: object
		public List<Fraud_details> data {get;set;} 
		public Boolean has_more {get;set;} 
		public Integer total_count {get;set;} 
		public String url {get;set;} 

		public Refunds(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'data') {
							data = new List<Fraud_details>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data.add(new Fraud_details(parser));
							}
						} else if (text == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text == 'total_count') {
							total_count = parser.getIntegerValue();
						} else if (text == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Refunds consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public String id {get;set;} 
		public String object_Z {get;set;} // in json: object
		public Integer amount {get;set;} 
		public Integer amount_refunded {get;set;} 
		//public Object application {get;set;} 
		//public Object application_fee {get;set;} 
		public String balance_transaction {get;set;} 
		public Boolean captured {get;set;} 
		public Integer created {get;set;} 
		public String currency_data {get;set;} 
		public String customer {get;set;} 
		public String description {get;set;} 
	/*	public Object destination {get;set;} 
		public Object dispute {get;set;} 
		public Object failure_code {get;set;} 
		public Object failure_message {get;set;} 
	*/	public Fraud_details fraud_details {get;set;} 
		public String invoice {get;set;} 
		public Boolean livemode {get;set;} 
		public Fraud_details metadata {get;set;} 
	//	public Object on_behalf_of {get;set;} 
	//	public Object order {get;set;} 
	//	public Object outcome {get;set;} 
		public Boolean paid {get;set;} 
	//	public Object receipt_email {get;set;} 
	//	public Object receipt_number {get;set;} 
		public Boolean refunded {get;set;} 
		public Refunds refunds {get;set;} 
	//	public Object review {get;set;} 
	//	public Object shipping {get;set;} 
		public Source source {get;set;} 
	//	public Object source_transfer {get;set;} 
		public String statement_descriptor {get;set;} 
		public String status {get;set;} 
	//	public Object transfer_group {get;set;} 

		public Data(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'amount') {
							amount = parser.getIntegerValue();
						} else if (text == 'amount_refunded') {
							amount_refunded = parser.getIntegerValue();
						} else if (text == 'application') {
							//application = new Object(parser);
						} else if (text == 'application_fee') {
							//application_fee = new Object(parser);
						} else if (text == 'balance_transaction') {
							balance_transaction = parser.getText();
						} else if (text == 'captured') {
							captured = parser.getBooleanValue();
						} else if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'description') {
							description = parser.getText();
						} else if (text == 'destination') {
							//destination = new Object(parser);
						} else if (text == 'dispute') {
							//dispute = new Object(parser);
						} else if (text == 'failure_code') {
							//failure_code = new Object(parser);
						} else if (text == 'failure_message') {
							//failure_message = new Object(parser);
						} else if (text == 'fraud_details') {
							fraud_details = new Fraud_details(parser);
						} else if (text == 'invoice') {
							invoice = parser.getText();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'metadata') {
							metadata = new Fraud_details(parser);
						} else if (text == 'on_behalf_of') {
							//on_behalf_of = new Object(parser);
						} else if (text == 'order') {
							//order = new Object(parser);
						} else if (text == 'outcome') {
							//outcome = new Object(parser);
						} else if (text == 'paid') {
							paid = parser.getBooleanValue();
						} else if (text == 'receipt_email') {
							//receipt_email = new Object(parser);
						} else if (text == 'receipt_number') {
							//receipt_number = new Object(parser);
						} else if (text == 'refunded') {
							refunded = parser.getBooleanValue();
						} else if (text == 'refunds') {
							refunds = new Refunds(parser);
						} else if (text == 'review') {
							//review = new Object(parser);
						} else if (text == 'shipping') {
							//shipping = new Object(parser);
						} else if (text == 'source') {
							source = new Source(parser);
						} else if (text == 'source_transfer') {
							//source_transfer = new Object(parser);
						} else if (text == 'statement_descriptor') {
							statement_descriptor = parser.getText();
						} else if (text == 'status') {
							status = parser.getText();
						} else if (text == 'transfer_group') {
							//transfer_group = new Object(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Fraud_details {

		public Fraud_details(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Fraud_details consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Source {
		public String id {get;set;} 
		public String object_Z {get;set;} // in json: object
		public String address_city {get;set;} 
		public String address_country {get;set;} 
		public String address_line1 {get;set;} 
		public String address_line1_check {get;set;} 
		public String address_line2 {get;set;} 
		public String address_state {get;set;} 
		public String address_zip {get;set;} 
		public String address_zip_check {get;set;} 
		public String brand {get;set;} 
		public String country {get;set;} 
		public String customer {get;set;} 
	//	public Object cvc_check {get;set;} 
	//	public Object dynamic_last4 {get;set;} 
		public Integer exp_month {get;set;} 
		public Integer exp_year {get;set;} 
		public String fingerprint {get;set;} 
		public String funding {get;set;} 
		public String last4 {get;set;} 
		public Fraud_details metadata {get;set;} 
	//	public Object name {get;set;} 
	//	public Object tokenization_method {get;set;} 

		public Source(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'address_city') {
							address_city = parser.getText();
						} else if (text == 'address_country') {
							address_country = parser.getText();
						} else if (text == 'address_line1') {
							address_line1 = parser.getText();
						} else if (text == 'address_line1_check') {
							address_line1_check = parser.getText();
						} else if (text == 'address_line2') {
							address_line2 = parser.getText();
						} else if (text == 'address_state') {
							address_state =parser.getText();
						} else if (text == 'address_zip') {
							address_zip = parser.getText();
						} else if (text == 'address_zip_check') {
							address_zip_check = parser.getText();
						} else if (text == 'brand') {
							brand = parser.getText();
						} else if (text == 'country') {
							country = parser.getText();
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'cvc_check') {
							//cvc_check = new Object(parser);
						} else if (text == 'dynamic_last4') {
							//dynamic_last4 = new Object(parser);
						} else if (text == 'exp_month') {
							exp_month = parser.getIntegerValue();
						} else if (text == 'exp_year') {
							exp_year = parser.getIntegerValue();
						} else if (text == 'fingerprint') {
							fingerprint = parser.getText();
						} else if (text == 'funding') {
							funding = parser.getText();
						} else if (text == 'last4') {
							last4 = parser.getText();
						} else if (text == 'metadata') {
							metadata = new Fraud_details(parser);
						} else if (text == 'name') {
							//name = new Object(parser);
						} else if (text == 'tokenization_method') {
							//tokenization_method = new Object(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Source consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static Charges parse(String json) {
		return new Charges(System.JSON.createParser(json));
	}
}