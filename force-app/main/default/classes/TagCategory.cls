Public class TagCategory{

    @AuraEnabled
    public static List<Case> getCase(String caseId ){

        List<Case> ts = [select id,Tags_Support__c,LadyBoss_Tags__c,Other_Description__c   from case where id=: caseId];
        return ts;

    }
    @AuraEnabled 
    public static map<String,String> getTags() {
        map<String,String> options = new map<String,String>();

        Schema.DescribeFieldResult fieldResult = case.LadyBoss_Tags__c.getDescribe();
        List<Schema.PicklistEntry> ple;
        if(fieldResult!=null){
            ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.put(f.getLabel(), f.getValue());
        }       
        return options;
        }
        else{
            return null;
        }
        
    }
    @AuraEnabled 
    public static  List<String> getValues(String selectedVal) {
    
        Object obj = (Object)selectedVal;
        Schema.DescribeFieldResult F = case.Tags_Support__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        
        Map<Object,List<String>> mapVal = dependedPicklis.getDependentPicklistValues(T);
        List<String> multiVal = new  List<String> ();

        multiVal = mapVal.get(obj);
    
    return multiVal ;
    }

    @AuraEnabled
    public static case saveCase(String csId, String tagVal,String  MultiPicVal,String otherval){

        Case cs = [select id ,Tags_Support__c,LadyBoss_Tags__c,Other_Description__c  from case where id=:csId];
        cs.Other_Description__c = otherval;
        cs.Tags_Support__c= tagVal;
        cs.LadyBoss_Tags__c= MultiPicVal;

        update cs;
    return cs;

    }
}