@isTest
public class SubscriptionHandler_Test {
    
    public static TestMethod void testGetCallout(){
        Test.startTest();
        
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        //Test.setMock(HttpCalloutMock.class, new MockTest());
        
        
        
        Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.Name = 'opp0' ;
        opp.RecordTypeId = ChargeRecordTypeId;
        opp.CloseDate = Date.Today();
        opp.StageName = 'Pending';
        opp.Subscription__c = 'ch_1DMElfBwLSk1v1ohKFlpJg4i';
        //       opp.AccountId = Acc.id;
        opp.Contact__c = Co.Id;
        insert opp;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        //create your opportunity line item.  This assumes you already have an opportunity created, called opp
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = pbEntry.UnitPrice,
            Subscription_Id__c = 'sub_Dq0IvbH3NVgpVE'
        );
        
        insert oli;
        
        String OppId = String.valueOf(opp.id) ;
        String oliId = String.valueOf(oli.id) ;
       
        Test.stopTest(); 
        
    }
    public static TestMethod void testGetCalloutAgain(){
         Test.startTest();
        
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
       // Test.setMock(HttpCalloutMock.class, new MockTest());
        
        Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.Name = 'oppyy' ;
        opp.RecordTypeId = ChargeRecordTypeId;
        opp.CloseDate = Date.Today();
        opp.StageName = 'Pending';
        opp.Subscription__c = 'ch_1DMElfBwLSk1v1ohKFlpJg4i';
        opp.Contact__c = Co.Id;
        insert opp;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        //create your opportunity line item.  This assumes you already have an opportunity created, called opp
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = pbEntry.UnitPrice,
            Subscription_Id__c = 'sub_Dq0IvbH3NVgpVE'
        );
        
        insert oli;
        
        
        String OppId = String.valueOf(opp.id) ;
        String oliId = String.valueOf(oli.id) ;
        String dateval = String.valueOf(date.today());
       
        
        SubscriptionHandler.dummyCover();
        Test.stopTest(); 
        
    }
    
}