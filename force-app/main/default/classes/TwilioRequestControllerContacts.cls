public class TwilioRequestControllerContacts 
{  //get inbound perameter from Apex Page Url
    public String fromNumber      = ApexPages.currentPage().getParameters().get('From');
    public String toNumber        = ApexPages.currentPage().getParameters().get('To');
    public String body            = ApexPages.currentPage().getParameters().get('Body');
    public PageReference init()
    {
        String formatednumber = '';
        User currentuser = new User();
        try {
            String toNum='';       
            System.debug('STEP 0 FROM: ==========>' + fromNumber); 
            System.debug('STEP 1 TO: ===============>' + toNumber);
            System.debug('STEP 2 BODY: ==========>' + body);
            //formate String of phone numbers
            String format1 = '';
            String format2 = '';
            String format4 = '';
            if(fromNumber != null && !fromNumber.contains('+1'))
                fromNumber = '+1'+fromNumber;
            format1 = fromNumber;
            toNum= toNumber.replaceAll( '\\s+', '');
            if(toNum.contains('(') && toNum.contains(')')){
                toNum= toNum.replace('(','');
                toNum= toNum.replace(')','');
            }
            if(toNum.contains('-')){
                toNum= toNum.replace('-','');
            }
            String fromNum='';
            fromNum= fromNumber.replaceAll( '\\s+', '');
            if(fromNum.contains('(') && fromNum.contains(')')){
                fromNum= fromNum.replace('(','');
                fromNum= fromNum.replace(')','');
            }
            if(fromNum.contains('-')){
                fromNum= toNum.replace('-','');
            }
            if(format1 != null) {
                String format3 = format1.replace('+1', '');
                format2    ='(' + format3.substring(0, 3) + ') ' + format3.substring(3, 6) + '-' + format3.substring(6);
            }
            if(fromNum != null && fromNum.contains('+1')) {
                format4 = fromNum.replace('+1', '');
            }
            // Query to look for contact record by from phone number
            List<Contact> conlist = new List<Contact> ();
            if(format1 != null) {
                conlist = [select Id, Name,Phone from Contact where Phone =:format1 OR MobilePhone =: format1 OR OtherPhone =: format1 Order by createdDate ];
            } 
            if (format2 != null && conlist.isEmpty()) {
                System.debug('=========='+conlist);
                conlist = [select Id, Name from Contact where (Phone =:format2 OR MobilePhone =: format2 OR OtherPhone =: format2) Order by createdDate ];
            }
            if (format4 != null && conlist.isEmpty()) {
                System.debug('=========='+conlist);
                conlist = [select Id, Name from Contact where (Phone =:format4 OR MobilePhone =: format4 OR OtherPhone =: format4) Order by createdDate ];
            }
            //Query to look for User record by to phone number
            IF(fromNumber!=null && toNumber !=null){
                currentuser=[Select Id,Name,Email,Twillio_From_phone__c from User where Twillio_From_phone__c=:toNum limit 1];
                //save message into event Object
                if(currentuser != null) {
                    Event sm = new Event();
                    sm.SMS_Initiated_By__c = 'Customer';
                    sm.Sent_By__c = currentuser.Id;
                    sm.Phone__c = fromNum;
                    sm.From_Phone_Number__c = toNum;
                    sm.Description = body;
                    sm.isNew__c= true;
                    sm.DurationInMinutes = 1;
                    sm.ActivityDateTime = System.now();
                    if(!conlist.isEmpty()) {
                        sm.Contact__c = conlist[0].id;
                    }
                    insert sm;
                }
            }
        }
        catch(exception e){}
        RETURN null;
    }  
}