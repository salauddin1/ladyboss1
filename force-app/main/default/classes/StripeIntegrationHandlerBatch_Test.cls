@isTest 
public class StripeIntegrationHandlerBatch_Test {
    static testMethod void testMethodPostive1() 
    { 	
        ContactTriggerFlag.isContactBatchRunning = true;
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        insert add;
        
        Stripe_Profile__c stp = new Stripe_Profile__c();
        stp.Customer__c =con.id;
        stp.Stripe_Customer_Id__c ='1234';
        insert stp;
        
        
        Card__c  card = new Card__c ();
        card.Stripe_Profile__c = stp.id;
        card.Contact__c = con.id;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        opportunity op  = new opportunity();
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        op.Scheduled_Payment_Processed__c = false;
        op.Scheduled_Payment_Date__c =date.today();
        op.Stripe_Message__c='Success';
        insert op;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        
        insert ol;
        set<id> idset = new set<id>();
        idset.add(op.id);
       
        test.startTest();
        StripeIntegrationHandlerBatch b = new StripeIntegrationHandlerBatch(); 
        database.executebatch(b,1);
        test.stopTest();
    }
}