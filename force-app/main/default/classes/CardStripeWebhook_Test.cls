@isTest
public class CardStripeWebhook_Test {
    @isTest static void test(){
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CardStripeWebhook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1BHYS4DiFnu7hVq7CGn2S7hm\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1509113088, \"data\": { \"object\": { \"id\": \"card_1BHHFhDiFnu7hVq7pLsixdmW\", \"object\": \"card\", \"address_city\": null, \"address_country\": null, \"address_line1\": null, \"address_line1_check\": null, \"address_line2\": null, \"address_state\": null, \"address_zip\": null, \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BeaOaH8aCGpkn2\", \"cvc_check\": \"pass\", \"dynamic_last4\": null, \"exp_month\": 3, \"exp_year\": 2021, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": { }, \"name\": null, \"tokenization_method\": null }, \"previous_attributes\": { \"exp_month\": 2 } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_Es7y5qYUWSW4Nz\", \"idempotency_key\": null }, \"type\": \"customer.source.updated\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	CardStripeWebhook.CardStripe();
        Test.stopTest();
    }
}