public class FBMessageFromCaseCommentTriggerHandler {
	public static boolean hasExecuted = false;
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public FBMessageFromCaseCommentTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
    
    public void OnBeforeInsert(List<CaseComment> newCaseComments){
		
	}
	
	public void OnAfterInsert(List<CaseComment> newCaseComments){
		
	}
	
	@future(callout=true) public static void OnAfterInsertAsync(Set<ID> newCaseCommentsIDs){
		List<CaseComment> caseCommentList = [SELECT id,commentbody,Parent.id,Parent.Conversation_Id__c,Parent.Contact.Fb_User_Id__c,Parent.FB_Page__r.Page_Token__c,Parent.FB_Page__r.Page_Name__c  FROM CaseComment WHERE Parent.FB_Page__c!=null AND Parent.Origin = 'Facebook' AND Id IN :newCaseCommentsIDs];
		Map<String,String> paramMap = null;
		List<ConversationAndMessage__c> convMessageToInsertList = new List<ConversationAndMessage__c>();
		for(CaseComment caseCommentsObject : caseCommentList){
			paramMap = new Map<String,String>();
			system.debug(caseCommentsObject.Parent.FB_Page__r.Page_Token__c);
			paramMap.put('access_token',caseCommentsObject.Parent.FB_Page__r.Page_Token__c);
			// paramMap.put('message',EncodingUtil.URLENCODE(caseCommentsObject.commentbody,'UTF-8'));
			List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
            String messagecase = JSON.serialize(caseCommentsObject.commentbody);
			String body = '{"recipient":{"id":"'+caseCommentsObject.Parent.Contact.Fb_User_Id__c+'"},"message":{"text":'+messagecase+'}}';
			String response = FaceBookMessagesAPI.callAPI('/me/messages','POST',paramList,body);
			System.debug(response);
			caseCommentsObject.commentbody = caseCommentsObject.Parent.FB_Page__r.Page_Name__c +' ('+Datetime.now() +') : '+ caseCommentsObject.commentbody;
			FacebookWrappers.MessageObject message = (FacebookWrappers.MessageObject)JSON.deserialize(response,FacebookWrappers.MessageObject.class);
			ConversationAndMessage__c conv = new ConversationAndMessage__c();
			conv.Conversation_Id__c = caseCommentsObject.Parent.Conversation_Id__c;
			conv.Message_Id__c = message.message_id;
			convMessageToInsertList.add(conv);
			
		}
		system.debug(convMessageToInsertList);
		if(caseCommentList.size()>0){
			CaseCommmentStaticFlagClass.flag = false;
			update caseCommentList;
			insert convMessageToInsertList;
		}

    }
}