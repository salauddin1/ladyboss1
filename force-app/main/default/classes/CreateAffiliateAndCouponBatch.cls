global class CreateAffiliateAndCouponBatch implements Database.Batchable<sObject>, Database.allowsCallouts {
    global String descriptionValue = '3FF Beta Member Group';
    global CreateAffiliateAndCouponBatch(String descriptionValue){
        this.descriptionValue = descriptionValue;
    }
    global CreateAffiliateAndCouponBatch(){
    }
    
    global List<Contact> start(Database.BatchableContext bc) {
        List<Contact> conList = new List<Contact>();
        conList = [Select id, name, firstName, lastName, email, pap_refid__c, PAPUserId__c, PAPRPassword__c, Coupon_Code_Id__c, Coupon_Amount__c, Coupon_Discount_Type__c,X3FF_Batch_Description__c,X3FF_AffiliateCreatedByBatch__c,Description from contact where  PAPUserId__c = null and PAPRPassword__c = null and pap_refid__c = null and X3FF_AffiliateCreatedByBatch__c != true and X3FF_Batch_Description__c =: descriptionValue ];
        System.debug(' ****conList*** '+conList);
        return conList;
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> contactList) {
        if(contactList.size() > 0) {
            try{
                Map<String, String> refIdValueMap = new Map<String, String>();
                for(Contact conbt :contactList){
                    String refIdNew='';
                    if(refIdNew == null || refIdNew == ''){
                        if(conbt.FirstName != null ){ 
                            if( conbt.FirstName.contains(' ') ){
                                refIdNew = conbt.FirstName.remove(' ') +'10';
                            }
                            else{
                                refIdNew = conbt.FirstName +'10';
                            }
                        }
                        else{
                            refIdNew = '10';
                        }
                    }
                    if(refIdNew != null && refIdNew != '') {
                        refIdValueMap.put(conbt.id, refIdNew);
                    }
                }
                if(refIdValueMap != null && refIdValueMap.keySet().size() > 0 ){
                    List<Contact> ctList = new List<Contact>();
                    ctList = [SELECT id,Email,PAP_refid__c,firstname,lastname FROM Contact WHERE PAP_refid__c != null AND PAP_refid__c =: refIdValueMap.values()];
                    if(ctList.size()>0){
                        System.debug('ctList:-'+ctList);
                        for(String newRefIdMapKey : refIdValueMap.keySet()){
                            String newRefIdMapVal = refIdValueMap.get(newRefIdMapKey);
                            String str ='';
                            Boolean checkRefId = false ;
                            Set<Integer> numberSet = new Set<Integer>();
                            for(Contact cnt :ctList){
                                if(cnt.PAP_refid__c ==  newRefIdMapVal){
                                    String temp = '0';
                                    str = cnt.PAP_refid__c.toLowerCase();
                                    if(str != null && str != '' && str.substring((str.length()-2), str.length()) == '10'){
                                        str = str.remove('10');
                                    }
                                    for(integer i = 0; i < str.length(); i++){
                                        String st = str.substring(i,i+1);
                                        if(st.isNumeric()){
                                            temp = temp+st;
                                        } 
                                    } 
                                    System.debug('str:='+str);
                                    System.debug('temp:='+temp);
                                    numberSet.add(integer.valueOf(temp));
                                    checkRefId = true;
                                }
                            }
                            if(checkRefId){
                                List<Integer> sortedList = new List <Integer>();
                                sortedList.addAll(numberSet);
                                sortedList.sort(); 
                                integer maxNo = sortedList.get(sortedList.size()-1);
                                refIdValueMap.put(newRefIdMapKey, newRefIdMapVal.substringBefore('10') + (maxNo + 1)+'10');
                            }   
                        }
                    }
                }
                if(refIdValueMap != null && refIdValueMap.keySet().size() > 0 ){
                    pap_credentials__c pap_url = [select id, name, pap_url__c, username__c, password__c from pap_credentials__c where name = 'pap url' limit 1];
                    List<Create_Site_Coupon__c> siteCouponList = [select id,Name,Create_Coupon_Site_URL__c from Create_Site_Coupon__c];
                    Create_Coupon__c cc = [select id, Amount__c, Discount_Type__c, Product_Categories__c from Create_Coupon__c where name = 'default'];                                    
                    if(pap_url != null &&  cc != null && siteCouponList.size() > 0){
                        for(Contact conbt :contactList){
                            
                            String newBtRefId = refIdValueMap.get(conbt.id);
                            String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Merchants_User_AffiliateForm","M":"add","isFromApi":"Y","fields":[["name","value","values","error"],["username","'+conbt.Email+'",null,""],["firstname","'+conbt.FirstName+'",null,""],["status","A",null,""],["lastname","'+conbt.LastName+'",null,""],["refid","'+newBtRefId+'",null,""],["data25","3FF",null,""],["data24","'+newBtRefId+'",null,""],["agreeWithTerms","Y",null,""]]}]}';
                            Http http = new Http();
                            HttpRequest req = new HttpRequest();
                            if(pap_url.pap_url__c != null && pap_url.pap_url__c != '') {
                                req.setEndpoint(pap_url.pap_url__c);
                            }
                            req.setMethod('POST');
                            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                            if(body != null && body != '') {
                                req.setBody(body);
                            }
                            HttpResponse response = http.send(req);
                            if(response.getStatusCode() == 200 || response.getStatusCode() == 201) {
                                List<Object> result = (List<Object>)JSON.deserializeUntyped(response.getBody());
                                String papUserId;
                                String papUserPassword;
                                if(result.size() > 0) {
                                    String msg='';
                                    for(Object ob : result){
                                        Map<String,Object> ob1 = (Map<String,Object>)ob;
                                        if((String)ob1.get('message')!=null){
                                            msg = (String)ob1.get('message');
                                        }
                                    }
                                    if(msg=='Affiliate was successfully added'){
                                        for(Object ob : result){
                                            Map<String,Object> ob1 = (Map<String,Object>)ob;
                                            if(ob1.get('fields')!=null){
                                                List<Object> fieldList = (List<Object>)ob1.get('fields');
                                                for(Object field:fieldList){
                                                    List<Object> fields = (List<Object>)field;
                                                    if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('userid')){
                                                        conbt.PAPUserId__c = (String)fields.get(1);
                                                    }
                                                    if(fields!=null && fields.size()==4 && ((String)fields.get(0)).equals('rpassword')){
                                                        conbt.PAPRPassword__c = (String)fields.get(1);
                                                    }
                                                    if(fields != null && fields.get(0).equals('refid')) {
                                                        String papRefIdVal = String.valueOf(fields.get(1));
                                                        conbt.PAP_refid__c = papRefIdVal;
                                                        conbt.X3FF_AffiliateCreatedByBatch__c = true;
                                                    }
                                                }
                                            }    
                                        }
                                    }
                                    if( response !=  null && conbt.PAP_refid__c != null && conbt.PAP_refid__c != ''){
                                        String coupCode = conbt.PAP_refid__c;
                                        if( coupCode != null){
                                            if( coupCode.contains(' ') ){
                                                coupCode = coupCode.remove(' ') ;
                                            }
                                            if(conbt.Coupon_Code_Id__c == null){
                                                Boolean individual_use = true;
                                                Boolean exclude_sale_items = true;
                                                String jsonBody = '';
                                                
                                                String coupAmount = String.valueOf(cc.Amount__c);
                                                String coupDiscountType = cc.Discount_Type__c;
                                                String prodCatbody = '';
                                                if(cc.Product_Categories__c != null ){
                                                    if(cc.Product_Categories__c.contains(',') ){
                                                        List<Integer> pcIdList = new List<Integer>();
                                                        List<String> pcList = cc.Product_Categories__c.split(',');
                                                        for(String pcval : pcList){
                                                            pcIdList.add( Integer.valueOf(pcval) );
                                                        }
                                                        if(pcIdList.size() > 0){
                                                            prodCatbody =JSON.serialize(pcIdList);
                                                        }
                                                    }
                                                    else{
                                                        prodCatbody =JSON.serialize(new List<Integer>{Integer.valueOf( cc.Product_Categories__c)});
                                                    }
                                                }
                                                jsonBody = '{ "code": "'+coupCode+'", "amount": "'+coupAmount+'", "discount_type": "'+coupDiscountType+'", "individual_use": '+individual_use+', "exclude_sale_items": '+exclude_sale_items+',"product_categories":'+prodCatbody+' }';
                                                System.debug('jsonBody:-'+jsonBody);
                                                HttpResponse response1=null;
                                                for(Create_Site_Coupon__c site: siteCouponList){
                                                    System.debug('jsonBody:-'+jsonBody);
                                                    Http http1 = new Http();
                                                    HttpRequest request1 = new HttpRequest();
                                                    request1.setEndpoint(site.Create_Coupon_Site_URL__c);
                                                    request1.setMethod('POST');
                                                    request1.setBody(jsonBody);
                                                    request1.setHeader('Content-Type', 'application/json');
                                                    
                                                    if(site.Name == 'Store'){
                                                       // response1 = http.send(request1);
                                                    }
                                                    else{
                                                         response1 = http.send(request1);
                                                      //  HttpResponse response2 = http.send(request1);
                                                    }
                                                }
                                                if(response1 != null){
                                                    if (response1.getStatusCode() == 200 || response1.getStatusCode() == 201 ) {
                                                        if(response1.getBody() != null && response1.getBody() != ''){
                                                            String body1 = response1.getBody();
                                                            System.debug('Body1 =========test========== :-'+Body1);
                                                            Map<String,Object> getCodeMap = new Map<String,Object>();
                                                            if(!test.isRunningTest()){
                                                                getCodeMap = (Map<String,Object>)Json.deserializeUntyped(body1);
                                                                if(getCodeMap != null && getCodeMap.keySet().size() > 0){
                                                                    if(getCodeMap.keySet().contains('code') && getCodeMap.keySet().contains('id') && getCodeMap.get('code') != null && getCodeMap.get('id') != null){
                                                                        conbt.Coupon_Code_Id__c = String.valueOf(getCodeMap.get('id'));
                                                                        conbt.Coupon_Amount__c = Decimal.valueOf(coupAmount);
                                                                        conbt.Coupon_Discount_Type__c = coupDiscountType;
                                                                    }
                                                                } 
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }   
                                    }
                                }
                                
                            }
                        }
                        if(contactList.size() > 0) {
                            System.debug('=========contactList========== :-'+contactList);
                            update contactList;
                        }
                    }
                    
                }
            }
            catch(Exception ex) {
                ApexDebugLog apex=new ApexDebugLog(); 
                String conId='';
                for(Contact conbt :contactList){
                        conId = conId +', '+conbt.id;
                }    
                apex.createLog(new ApexDebugLog.Error('CreateAffiliateAndCouponBatch','execute',conId,ex));
            }
        }
    }
    global void finish(Database.BatchableContext bc) {
        System.debug('Batch processed successfully !!');
    }
}