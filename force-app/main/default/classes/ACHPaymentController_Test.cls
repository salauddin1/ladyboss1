@IsTest
public class ACHPaymentController_Test {
    
    static testMethod void testParse() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
        insert addr;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Customer__c = con.id;
        insert sp;
        
        test.startTest();
        
        PageReference pageRef = Page.GenerateCardToken;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        
        ACHPaymentController gcc = new ACHPaymentController();
        gcc.ACHJson  = '{"id":"ba_1DYA8wBwLSk1v1ohhOAIkJnK","object":"bank_account","account_holder_name":"","account_holder_type":"individual","bank_name":"JPMORGAN CHASE","country":"US","currency":"usd","last4":"6789","name":"","routing_number":"021000021","status":"new"}';
        gcc.token = '"'+'btok_1DYA8wBwLSk1v1ohy6k1e0OI'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
    
    static testMethod void testParse2() {
        
        contact con = new contact();
        con.FirstName = 'dsd';
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
        insert addr;
        
       
        
        test.startTest();
        
        PageReference pageRef = Page.ACHPayment;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(emp);
        ApexPages.currentPage().getParameters().put('stripeKey','pk_test_B2mkgKwtIVCEPFFKRpaYw7T8');
        ApexPages.currentPage().getParameters().put('cntId',con.id);
        
        ACHPaymentController gcc = new ACHPaymentController();
        gcc.ACHJson  = '{"id":"ba_1DYA8wBwLSk1v1ohhOAIkJnK","object":"bank_account","account_holder_name":"","account_holder_type":"individual","bank_name":"JPMORGAN CHASE","country":"US","currency":"usd","last4":"6789","name":"","routing_number":"021000021","status":"new"}';
        gcc.token = '"'+'btok_1DYA8wBwLSk1v1ohy6k1e0OI'+'"';
        gcc.contactId=con.id;
        gcc.stipeApiKey='pk_test_B2mkgKwtIVCEPFFKRpaYw7T8';
        
        gcc.InsertRecord();
        gcc.CallWebService();
        test.stopTest();
    }
}