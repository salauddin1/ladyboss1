@RestResource(urlMapping='/Opportunity/*')
global  class RestResource {
    
    @HttpPost
    global static String doPost() {
        OpportunityWrapper oppw = (OpportunityWrapper)JSON.deserialize(RestContext.request.requestBody.toString(), OpportunityWrapper.class);
        List<Opportunity> lstO = [select id from Opportunity where wc_order_id__c =: oppw.id ];
        if(lstO.size() > 0)  {
            Opportunity opp = lstO.get(0);
            opp.wc_order_id__c = oppw.id;
            opp.Name = 'test';
            opp.StageName = 'Closed Won';
            opp.CloseDate = date.today();
            update opp;
            return opp.Id;
        }else {
            Opportunity opp = new Opportunity();
            opp.wc_order_id__c = oppw.id;
            opp.Name = 'test';
            opp.StageName = 'Closed Won';
            opp.CloseDate = date.today();
            insert opp;
            return opp.ID;
        }
        
        
    }
}