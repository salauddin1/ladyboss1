global class UpdateNullAddressCharge implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    
    public boolean isSuccess = false;
    List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
    List<Opportunity> oppList = new List<Opportunity> ();
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        //SELECT  id, (select id from Opportunities__r WHERE CREATEDDATE >= LAST_N_DAYS:15),(select id from Addresses__r WHERE SHIPPING_CITY__C = NULL) from Contact where Id=‘003f400000V04PjAAJ’
        return Database.getQueryLocator('SELECT  id from Contact');
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        List<Contact> cntList = (List<Contact>)scope;
        List<Contact> cntList3 = [SELECT  id, (select id,Stripe_charge_id__c from Opportunities__r WHERE CREATEDDATE >= LAST_N_DAYS:15),(select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c,primary__c from Addresses__r) from Contact where id in :cntList ];
        List<Contact> cntList2 = new List<Contact>();
        for(Contact opp : cntList3){
            if(opp.Opportunities__r.size() >0 && opp.Addresses__r.size() > 0){
                boolean addFound = false;
                for(Address__c ac : opp.Addresses__r){
                    if(ac.primary__c){
                        addFound=true;
                    }
                }
                if(addfound){
                    for(Address__c ac : opp.Addresses__r){
                        if(ac.Shipping_Street__c == null){
                            cntList2.add(opp);
                            break;
                        }
                        
                    }
                }
            }
        }
        list<Opportunity> oppList2 =[select id,contact__c,Stripe_charge_id__c,(select id,Stripe_charge_id__c from OpportunityLineItems) from Opportunity where contact__c in : cntlist2 ];
        System.debug('size of oppList2-- '+oppList2.size());
        Set<String> setof = new Set<String>();
        
        for(Opportunity op : oppList2){
            if(op.stripe_charge_id__c != null){
                if(setof.add(op.stripe_charge_id__c)){
                    StripeCharge stripechrg = StripeCharge.getCharge(op.Stripe_Charge_Id__c, false);
                    String addressVal;
                    if(stripechrg !=null && stripechrg.metadata != null){
                        addressVal = stripechrg.metadata.get('Shipping Address');
                    }
                    if(stripechrg != null && addressVal!=null && addressVal.contains('null')){
                        OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate1(op);
                    }
                }
            }
            for(OpportunityLineItem opli :op.OpportunityLineItems ){
                if(opli.stripe_charge_id__c != null){
                    if(setof.add(opli.stripe_charge_id__c)){
                        StripeCharge stripechrgli = StripeCharge.getCharge(opli.Stripe_Charge_Id__c, false);
                        String addressVal2;
                        if(stripechrgli !=null && stripechrgli.metadata != null){
                            addressVal2 = stripechrgli.metadata.get('Shipping Address');
                        }
                        if(stripechrgli != null && addressVal2!=null && addressVal2.contains('null')){
                            OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate2(opli);
                        }
                    }
                }
                
            }
        }
        
        
        
    }
    
    global void finish(Database.BatchableContext BC){}
    
    
    
}