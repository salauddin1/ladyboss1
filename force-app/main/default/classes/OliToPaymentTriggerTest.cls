@isTest
public class OliToPaymentTriggerTest {
    @isTest
    public static void insertMethod1(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        oL1.Quantity = 2;
        ol1.Dynamic_Item_Count_Cost__c = 1;
        insert oL1;
        
        
        ol1.refund__c = 10;
        upsert oL1;
    }
    @isTest
    public static void insertMethod2(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        oL1.Quantity = 2;
        insert oL1;
        
        
        ol1.refund__c = 10;
        upsert oL1;
    }
    @isTest
    public static void insertMethod3(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Dynamic_Item_Count_Cost__c = 1;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        ol1.refund__c = 1;
        upsert oL1;
    }
    @isTest
    public static void insertMethod4(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
    @isTest
    public static void insertMethod5(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.isMonthProduct__c = true;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod1.Commissionable_amount__c = 5;
        prod1.Price__c = 100;
        prod1.isMonthProduct__c = true;
        prod1.Shipping_cost_1__c = 0;
        prod1.Shipping_cost_2__c = 0;
        prod1.Shipping_cost_3__c = 0;
        prod1.Shipping_cost_4__c = 0;
        prod1.Shipping_cost_5__c = 0;
        prod1.Dynamic_Fulfillment_Item_Count__c = 1;
        prod1.Switch_To_Product_two_month__c = prod.id;
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
    @isTest
    public static void insertMethod6(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.isMonthProduct__c = true;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod1.Commissionable_amount__c = 5;
        prod1.Price__c = 100;
        prod1.isMonthProduct__c = true;
        prod1.Shipping_cost_1__c = 0;
        prod1.Shipping_cost_2__c = 0;
        prod1.Shipping_cost_3__c = 0;
        prod1.Shipping_cost_4__c = 0;
        prod1.Shipping_cost_5__c = 0;
        prod1.Dynamic_Fulfillment_Item_Count__c = 1;
        prod1.Switch_To_Product_three_month__c = prod.id;
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
    @isTest
    public static void insertMethod7(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.isMonthProduct__c = true;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod1.Commissionable_amount__c = 5;
        prod1.Price__c = 100;
        prod1.isMonthProduct__c = true;
        prod1.Shipping_cost_1__c = 0;
        prod1.Shipping_cost_2__c = 0;
        prod1.Shipping_cost_3__c = 0;
        prod1.Shipping_cost_4__c = 0;
        prod1.Shipping_cost_5__c = 0;
        prod1.Dynamic_Fulfillment_Item_Count__c = 1;
        prod1.Switch_To_Product_four_month__c = prod.id;
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
    @isTest
    public static void insertMethod8(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.isMonthProduct__c = true;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod1.Commissionable_amount__c = 5;
        prod1.Price__c = 100;
        prod1.isMonthProduct__c = true;
        prod1.Shipping_cost_1__c = 0;
        prod1.Shipping_cost_2__c = 0;
        prod1.Shipping_cost_3__c = 0;
        prod1.Shipping_cost_4__c = 0;
        prod1.Shipping_cost_5__c = 0;
        prod1.Dynamic_Fulfillment_Item_Count__c = 1;
        prod1.Switch_To_Product_five_month__c = prod.id;
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
    @isTest
    public static void insertMethod9(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.isMonthProduct__c = true;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        
        Product2 prod1 = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod1.Commissionable_amount__c = 5;
        prod1.Price__c = 100;
        prod1.isMonthProduct__c = true;
        prod1.Shipping_cost_1__c = 0;
        prod1.Shipping_cost_2__c = 0;
        prod1.Shipping_cost_3__c = 0;
        prod1.Shipping_cost_4__c = 0;
        prod1.Shipping_cost_5__c = 0;
        prod1.Dynamic_Fulfillment_Item_Count__c = 1;
        prod1.Switch_To_Product_six_month__c = prod.id;
        insert prod1;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        oL1.Quantity = 2;
        ol1.Consider_For_Commission__c = false;
        insert oL1;
        PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
        ol1.Consider_For_Commission__c = true;
        upsert oL1;
    }
}