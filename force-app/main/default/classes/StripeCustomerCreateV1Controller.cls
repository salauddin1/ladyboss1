public class StripeCustomerCreateV1Controller {
    @AuraEnabled
    public static String getConDetails(String conID){
        List<Stripe_Profile__c> spList = [SELECT id FROM Stripe_Profile__c WHERE Customer__c =: conID];
        if (spList.size()>0) {
            return 'Already have Stripe Profile';
        }
        Contact con = [SELECT id,Email FROM Contact WHERE id =:conID];
        if (con.Email == null || con.Email == '') {
            return 'No email attached';
        }
        return 'Success';
    }
    @AuraEnabled
    public static string createCustomer(String conID){
        String billingAddress = '';
        Map<String,String> metadata = new Map<String,String>();
        Contact con = [SELECT id,Email,Name,Phone,MailingCity,MailingCountry,MailingState,MailingStreet,MailingPostalCode FROM Contact WHERE id =:conID];
        List<Address__c> addList = [SELECT id,Shipping_City__c,Shipping_country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c = :conID AND Primary__c = true];
        if (addList.size()>0) {
            billingAddress = addList[0].Shipping_City__c +' ' + addList[0].Shipping_Street__c + ' ' + addList[0].Shipping_State_Province__c + ' ' + addList[0].Shipping_Country__c + ' ' + addList[0].Shipping_Zip_Postal_Code__c;
        }else if (con.MailingCity != null || con.MailingState != null || con.MailingStreet != null) {
            billingAddress = con.MailingCity + ' ' + con.MailingStreet + ' ' + con.MailingState + ' ' + con.MailingCountry + ' ' + con.MailingPostalCode;
        }
        if (billingAddress != '') {
            metadata.put('Shipping Address',billingAddress);
        }
        metadata.put('Full Name',con.Name);
        metadata.put('Email',con.Email);
        metadata.put('Phone',con.Phone);
        metadata.put('Created By',UserInfo.getName());
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        Map<String, String> properties = new Map<String, String>{
            'description' => con.Name,
            'email' => con.Email
        };
        Map<String, String> payload = new Map<String, String>();
        if (properties != null) {
            for (String key : properties.keySet()) {
                if (properties.get(key) != null) {
                    payload.put(key, properties.get(key));
                }
            }
        }
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.stripe.com/v1/customers');
        req.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);        
        req.setBody(StripeUtil.urlify(payload));
        HttpResponse res = new HttpResponse();
        if (!Test.isRunningTest()) {
            res = http.send(req);
        }
        if(res.getStatusCode() == 200){
          return 'Success';  
        } 
        return 'Failure';
    }
}