/*
 * Developer Name : Tirth Patel
 * Description : This Batch update shipping payment
*/
global class PaymentupdateShippingBatch implements  Database.Batchable<sObject>, Database.Stateful{
    List<first_monday_of_year__c> fmList = [SELECT day__c,month__c,year__c FROM first_monday_of_year__c];
    datetime dt_time = datetime.newInstance(Integer.valueof(fmList.get(0).year__c), Integer.valueof(fmList.get(0).month__c), Integer.valueof(fmList.get(0).day__c),0,0,0);
    date dt = date.newInstance(Integer.valueof(fmList.get(0).year__c), Integer.valueof(fmList.get(0).month__c), Integer.valueof(fmList.get(0).day__c));
	global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT id,Scheduled_Payment_Date__c,Stripe_Charge_Id__c,(select id,Quantity,DNC_Ship_Pack__c,Product2.Dynamic_Fulfillment_Item_Count__c,Product2Id,Product_Number_Count__c,Product2.Shipping_cost_1__c,Product2.Shipping_cost_2__c,Product2.Shipping_cost_3__c,Product2.Shipping_cost_4__c,Product2.Shipping_cost_5__c from opportunityLineitems) FROM Opportunity  where Stripe_Charge_Id__c!=null AND (createdDate >= : dt_time OR Scheduled_Payment_Date__c >= : dt )');
    }
    global void execute(Database.BatchableContext bc, List<Opportunity> oppList){
        System.debug('oppList is : '+oppList);
        List<OpportunityLineItem> oliUpdateList = new List<OpportunityLineItem>();
        List<id> ProdIdList = new List<id>();
        Map<id,Decimal> prod_place = new Map<id,Decimal>();
        List<Product2> productList = [SELECT id,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 where Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c !=null];
        for(Opportunity op : oppList){
            for(OpportunityLineItem oli :  op.opportunityLineitems){
                ProdIdList.add(oli.Product2Id);
            }
        }
        for(Product2 prod1 : productList){
            for(Id prod2 : ProdIdList){
                if(prod2 == prod1.Switch_To_Product_four_month__c){
                    prod_place.put(prod2, 4);
                }else if(prod2 == prod1.Switch_To_Product_five_month__c){
                    prod_place.put(prod2, 5);
                }else if(prod2 == prod1.Switch_To_Product_six_month__c){
                    prod_place.put(prod2, 6);
                }
            }
        }
        System.debug('prod_place : '+prod_place);
        for(Opportunity op : oppList){
            List<opportunityLineItem> oliList = new List<opportunityLineItem>();
            for(opportunityLineItem oli : op.opportunityLineItems){
                oliList.add(oli);
            }
            if(oliList.size()>0){
            	updateShipping(oliList,prod_place,oliUpdateList);
            }
        }
        
        if(oliUpdateList.size()>0){
            update oliUpdateList;
        }
        
    }
    global void finish(Database.BatchableContext bc){ 
        // execute any post-processing operations
    }
    public static void updateShipping(List<opportunityLineItem> oliList,Map<id,Decimal> prod_place,List<OpportunityLineItem> oliUpdateList){
        System.debug('oliList : '+oliList);
        System.debug('prod_place : '+prod_place);
        Set<id> Product_withmonth = new Set<id>();
        Decimal oliItemCount = 0;
        Decimal oliDynamicCountCost = 0;
        for(OpportunityLineItem ol : oliList){
            if(prod_place.get(ol.Product2Id)>=4 && ol.DNC_Ship_Pack__c!=true){
            	Product_withmonth.add(ol.Product2Id);
            }
        }
        System.debug('Product_withmonth : '+Product_withmonth);
        Map<id,List<Decimal>> prod_shipMap = new Map<id,List<Decimal>>();
        Map<id,Decimal> prod_dyItemcntMap = new Map<id,Decimal>();
        for(opportunityLineItem ol : oliList){
            if(ol.DNC_Ship_Pack__c!=true){
            List<Decimal> prList = new List<Decimal>();
            if(ol.Product2.Shipping_cost_1__c!=null){
                prList.add(ol.Product2.Shipping_cost_1__c);
            }else{
                prList.add(0);
            }
            
            if(ol.Product2.Shipping_cost_2__c!=null){
                prList.add(ol.Product2.Shipping_cost_2__c);
            }else{
                prList.add(0);
            }
            
            if(ol.Product2.Shipping_cost_3__c!=null){
                prList.add(ol.Product2.Shipping_cost_3__c);
            }else{
                prList.add(0);
            }
            
            if(ol.Product2.Shipping_cost_4__c!=null){
                prList.add(ol.Product2.Shipping_cost_4__c);
            }else{
                prList.add(0);
            }
            
            if(ol.Product2.Shipping_cost_5__c!=null){
                prList.add(ol.Product2.Shipping_cost_5__c);
            }else{
                prList.add(0);
            }
            prod_shipMap.put(ol.Product2Id,prList);
                if(ol.Product2.Dynamic_Fulfillment_Item_Count__c == null){
                prod_dyItemcntMap.put(ol.Product2Id,0);
            }else{
                prod_dyItemcntMap.put(ol.Product2Id, ol.Product2.Dynamic_Fulfillment_Item_Count__c);
            }
            }
        }
        System.debug('prod_shipMap : '+prod_shipMap);
        System.debug('prod_dyItemcntMap : '+prod_dyItemcntMap);
        for(OpportunityLineItem ol : oliList){
            if(ol.DNC_Ship_Pack__c!=true){
            	oliItemCount = oliItemCount + ol.Quantity*prod_dyItemcntMap.get(ol.Product2Id);
            }
        }
        System.debug('oliItemCount : '+oliItemCount);
        Dynamic_Item_Count_Cost__c dic = [SELECT Cost_Per_Item__c,Fix_cost__c,max_count__c,min_count__c FROM Dynamic_Item_Count_Cost__c];
        if(oliItemCount>4){
            oliDynamicCountCost = dic.Fix_cost__c + (oliItemCount - dic.max_count__c)*dic.Cost_Per_Item__c;
        }else if (oliItemCount>=dic.min_count__c){
            oliDynamicCountCost = dic.Fix_cost__c;
        }
        if(prod_dyItemcntMap.size()==0){
            oliDynamicCountCost = oliDynamicCountCost/1;
        }else{
        	oliDynamicCountCost = oliDynamicCountCost/prod_dyItemcntMap.size();
        }
        System.debug('oliDynamicCountCost '+oliDynamicCountCost);
        Map<id,Integer> prod_shipindex = new Map<id,Integer>();
        for(id pm : Product_withmonth){
			prod_shipindex.put(pm,0); 
            prod_shipMap.remove(pm);
        }
        integer st_index=0,index_start=0;
        Integer  loopCount = prod_shipMap.size();
        if(Product_withmonth.size()>0){
            st_index=1;
            index_start = 1;
            loopCount++;
        }
        
        System.debug('prod_shipMap : '+prod_shipMap);

        if(prod_shipMap.size()>0){
            for(integer i=index_start; i<loopCount;i++){
                Decimal max=0;
                String maxPrId = '';
                    for(id ps : prod_shipMap.keySet()){
                        if(prod_shipMap.get(ps).get(i) >= max){
                            max = prod_shipMap.get(ps).get(i);
                            maxPrId = ps;
                        }
                    }
                    prod_shipindex.put(maxPrId,st_index);
                    prod_shipMap.remove(maxPrId);
                	st_index++;
            }
        }
        System.debug('prod_shipindex : '+prod_shipindex);
        for(OpportunityLineItem oppLineItem : oliList){
            if(oppLineItem.DNC_Ship_Pack__c!=true){
        		oppLineItem.Product_Number_Count__c = prod_shipindex.get(oppLineItem.Product2Id)+1;
                oppLineItem.Dynamic_Item_Count_Cost__c = oliDynamicCountCost;
            }else{
                oppLineItem.Product_Number_Count__c = 0; 
                oppLineItem.Dynamic_Item_Count_Cost__c = 0;
            }
        }
        System.debug('update oli '+oliList);
        if(oliList.size()>0){
            oliUpdateList.addAll(oliList);
        }
    }
}