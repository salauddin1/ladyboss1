@isTest
public class FacebookAPIMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public FacebookAPIMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        if(req.getEndpoint().contains('conversations')){
            res.setBody('{"data": [{"unread_count": 13,"updated_time": "2099-07-27T15:08:25+0000","senders": {"data": [{"name": "Test test","email": "1956@facebook.com","id": "199"},{"name": "Shrine Software Services Pvt. Ltd.","email": "7508@facebook.com","id": "798"}]},"id": "t_16"},{"unread_count": 13,"updated_time": "2099-07-27T15:08:25+0000","senders": {"data": [{"name": "Test test","email": "1956@facebook.com","id": "1929"},{"name": "Shrine Software Services Pvt. Ltd.","email": "7508@facebook.com","id": "791118"}]},"id": "ets"}],"paging": {"cursors": {"before": "before","after": "after"}}}');
        }else if(req.getEndpoint().contains('message')){
            res.setBody('{"data": [{"message": "Hello From Chat Window.","from": {"name": "Shrine Software Services Pvt. Ltd.","email": "7508@facebook.com","id": "testPageId"},"to": {"data": [{"name": "Test test","email": "1956@facebook.com","id": "199"}]},"created_time": "2018-07-27T15:08:25+0000","id": "ffddd"}],"paging": {"cursors": {"before": "before","after": "after"}}}');
        }else if(req.getEndpoint().contains('oauth/access_token')){
            res.setBody('{"access_token": "bdjsbdjs","token_type": "bearer","expires_in":"30"}');
        }else if(req.getEndpoint().contains('me/accounts')){
            res.setBody('{"data": [{"about": "fdfbksjfbjksfbkk","emails": ["info@test.com"],"name": "Shrine Software Services Pvt. Ltd.","username": "shrine","category": "Software Company","access_token": "sdbsjdbsdjs","id": "testPageId"}],"paging": {"cursors": {"before": "MTgzMDY0NjY4MDM1MzQwMwZDZD","after": "NzUwNDk2MjcxNzYxMzk4"}}}');
        }else{
            res.setBody(this.body);
        }
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}