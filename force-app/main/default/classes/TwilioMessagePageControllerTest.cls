@isTest
private class TwilioMessagePageControllerTest{
	@isTest
	static void itShould(){
		Account act = new Account();
        act.name='annnaa';
        act.BillingCity='hassg';
        act.BillingCountry='jfyuagha';
        act.ShippingCity='fdfsfd';
        act.ShippingCountry='dfs';
        act.Credit_Card_Number__c='5445';
        act.Email__c='hakshda@new.com';
        insert act;

		Contact cntct = new Contact();
        cntct.LastName='DO NOT DELETE';
		cntct.AccountId=act.id;
		insert cntct;

		Contact cntct1 = new Contact();
        cntct1.LastName='tets';
		cntct1.AccountId=act.id;
		insert cntct1;

		Test.StartTest(); 

		PageReference pageRef = Page.TwilioMessagePage; // Add your VF page Name here
		pageRef.getParameters().put('From', '234567');
		pageRef.getParameters().put('To', '33234567');
		pageRef.getParameters().put('Body', String.valueOf(act.Id));
		Test.setCurrentPage(pageRef);

		Case caseObj = new Case();
		caseObj.Twilio_Number__c = pageRef.getParameters().get('From');
		//caseObj.Conversation_UpdateTime__c = DateTime.valueOf(conversationMap.get(conversationId).updated_time.replace('T',' ').substring(0,conversationMap.get(conversationId).updated_time.indexOf('+')));
		caseObj.status = 'new';
		caseObj.subject = 'Inbox Message To : '+pageRef.getParameters().get('To');
		caseObj.origin = 'Phone';
		caseObj.contactId = cntct1.id;
		insert caseObj;

		TwilioMessagePageController testAccPlan = new TwilioMessagePageController();
		testAccPlan.createOrUpdateCase();	
		Test.StopTest();
	}

	@isTest
	static void itShouldAgain(){
		Account act = new Account();
        act.name='annnaa';
        act.BillingCity='hassg';
        act.BillingCountry='jfyuagha';
        act.ShippingCity='fdfsfd';
        act.ShippingCountry='dfs';
        act.Credit_Card_Number__c='5445';
        act.Email__c='hakshda@new.com';
        insert act;

		Contact cntct = new Contact();
        cntct.LastName='DO NOT DELETE';
		cntct.AccountId=act.id;
		insert cntct;

		Test.StartTest(); 

		PageReference pageRef = Page.TwilioMessagePage; // Add your VF page Name here
		pageRef.getParameters().put('From', '234567');
		pageRef.getParameters().put('To', '33234567');
		pageRef.getParameters().put('Body', String.valueOf(act.Id));
		Test.setCurrentPage(pageRef);

		TwilioMessagePageController testAccPlan = new TwilioMessagePageController();
		testAccPlan.createOrUpdateCase();	
		Test.StopTest();
	}
}