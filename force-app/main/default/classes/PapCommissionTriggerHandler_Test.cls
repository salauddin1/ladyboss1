@isTest
public class PapCommissionTriggerHandler_Test {
	@isTest
    public static void PapCommissionTest(){
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'test@gmail.com';
        con.PAP_refid__c = 'test';
        insert con;
        
         
        opportunity op  = new opportunity();
        
        op.name='opclub';
        op.Contact__c =con.id;
       	op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CustomerID__c = 'cus_GsBRLv0RyvXmjg';
        op.Clubbed__c = true;
        
        op.CloseDate = System.today();
        insert op;
        op.Opportunity_Unique_Name__c = op.id;
        update op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Unique_Name__c = op.Id;
        
        insert ol;
        
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.order_id__c = '1706';
        pap.product_id__c = '506';
        pap.commission__c = 1.90;
        pap.affiliate_ref_id__c = 'test';
        pap.Campaign_name__c = 'test';
        pap.Contact__c = con.id;
        insert pap;
    }
}