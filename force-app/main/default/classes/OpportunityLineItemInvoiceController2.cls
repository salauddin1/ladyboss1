public class OpportunityLineItemInvoiceController2 {
	Public Static String oppLineItemId;
    
    public OpportunityLineItemInvoiceController2(ApexPages.StandardController controller){
        // oppId = '0061F000002OFkv';
         oppLineItemId =  ApexPages.currentPage().getParameters().get('id');
        system.debug('---'+oppLineItemId);
      OpportunityLineItem oppLineItem = [Select id,Quantity, UnitPrice,Tax_Amount__c, Tax_Percentage_s__c,Shipping_Costs__c,TotalPrice, PricebookEntry.Name,PricebookEntry.Product2.name, PricebookEntry.Product2.Family,Opportunity.Account.Name,Opportunity.Address__c,Opportunity.CloseDate ,Opportunity.Scheduled_Payment_Date__c,Opportunity.Contact__r.MailingAddress,Opportunity.Contact__r.name,
                                            Opportunity.Contact__r.MailingStreet, Opportunity.Contact__r.MailingCity, Opportunity.Contact__r.MailingState, 
                                            Opportunity.Contact__r.MailingPostalCode, Opportunity.Contact__r.MailingCountry,
                                            Opportunity.Card__r.Billing_Street__c,Opportunity.Card__r.Billing_City__c,Opportunity.Card__r.Billing_State_Province__c,
                                            Opportunity.Card__r.Billing_Country__c,Opportunity.Card__r.Billing_Zip_Postal_Code__c from OpportunityLineItem where id =: oppLineItemId.trim()];
        
        system.debug('---'+oppLineItem);
        oppWrp = new oppWrapper();
        
        oppWrp.accname = oppLineItem.Opportunity.Account.Name;
            
        if(oppLineItem.Opportunity.Card__r.Billing_Street__c!= null){
           oppWrp.billingAddress = (oppLineItem.Opportunity.Card__r.Billing_Street__c!=null ? oppLineItem.Opportunity.Card__r.Billing_Street__c+ ',': '')
                                    + (oppLineItem.Opportunity.Card__r.Billing_City__c!= null ? oppLineItem.Opportunity.Card__r.Billing_City__c+',':'') 
               + (oppLineItem.Opportunity.Card__r.Billing_State_Province__c!= null ? oppLineItem.Opportunity.Card__r.Billing_State_Province__c+',':'') 
               + (oppLineItem.Opportunity.Card__r.Billing_Zip_Postal_Code__c != null ? oppLineItem.Opportunity.Card__r.Billing_Zip_Postal_Code__c +',':'') 
               + (oppLineItem.Opportunity.Card__r.Billing_Country__c!= null ? oppLineItem.Opportunity.Card__r.Billing_Country__c+',':'') ;
           oppWrp.billingAddress = oppWrp.billingAddress.removeEnd(',');
                                    
        }
            
        /*    
        if(oppLineItem.Opportunity.Contact__r.MailingAddress != null){
           oppWrp.billingAddress = (oppLineItem.Opportunity.Contact__r.MailingStreet !=null ? oppLineItem.Opportunity.Contact__r.MailingStreet + ',': '')
                                    + (oppLineItem.Opportunity.Contact__r.MailingCity != null ? oppLineItem.Opportunity.Contact__r.MailingCity +',':'') 
               + (oppLineItem.Opportunity.Contact__r.MailingState != null ? oppLineItem.Opportunity.Contact__r.MailingState +',':'') 
               + (oppLineItem.Opportunity.Contact__r.MailingPostalCode != null ? oppLineItem.Opportunity.Contact__r.MailingPostalCode +',':'') 
               + (oppLineItem.Opportunity.Contact__r.MailingCountry  != null ? oppLineItem.Opportunity.Contact__r.MailingCountry  +',':'') ;
           oppWrp.billingAddress = oppWrp.billingAddress.removeEnd(',');
                                    
        }
        */
            
            oppWrp.oppRec= oppLineItem;
            oppWrp.closeDate = oppLineItem.Opportunity.CloseDate;
            oppWrp.scheduleDate = oppLineItem.Opportunity.Scheduled_Payment_Date__c;
            oppWrp.total = (oppLineItem.TotalPrice + oppLineItem.Tax_Amount__c  + oppLineItem.Shipping_Costs__c).setScale(2);
        
        
    }
    
    public oppWrapper oppWrp {get;set;}
    public class oppWrapper{
        public Decimal total{get;set;}
        public Date closeDate{get;set;}
        public Date scheduleDate{get;set;}
        public string accname{get;set;}
        public String billingAddress{get;set;}
        public OpportunityLineItem oppRec{get;set;}
        
    }
}