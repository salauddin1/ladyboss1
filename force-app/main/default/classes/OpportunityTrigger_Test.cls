@IsTest
public class OpportunityTrigger_Test {
    
    static testMethod void testHttpRequest() {
        ApexUtil.isTriggerInvoked = true;
        
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        contact c = new contact();
        c.LastName = 'test';
        c.Email = 'test@gmail.com';
        insert c;
        
        Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 300151;
        insert apex;
        
        Triggers_activation__c activation = new Triggers_activation__c();
        activation.SuccessPaymentWithCaseLink__c = true;
        activation.CampaignMemberdeleteHelper__c = true;
        activation.isCampaignAssociate__c = true;
        activation.Opportunitytriggerhandler__c = true;
        insert activation;
        
        Case ca = new Case();
        ca.Status = 'New';
        ca.IsFailed_Payment__c = true;
        ca.UpdateCardStatus__c = 'Payment Successful';
        ca.ContactId = c.Id;       
        ca.Successful_Payment_DateTime__c = system.today();                                                   
        insert ca;
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(ca.Id, yesterday);
        
        
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.order_id__c = '1760(3)';
        pap.product_id__c = '506';
        pap.commission__c = 2;
        Additional_Opportunity_Names__c addnalOpp = new Additional_Opportunity_Names__c();
        addnalOpp.Opportunity_Name__c='LB-BURN Bonus';
        addnalOpp.Name='addopp';
        insert addnalOpp;
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp = new Opportunity(Name='LB-BURN Bonus',CloseDate=system.today().addDays(5), 
                                          StageName='Closed Won',Scheduled_Payment_Date__c = System.today().addDays(2),
                                          AccountId=acc.Id,Contact__c=c.Id, Payment_Date__c=system.today(),
                                          Amount=20,Initiated_From__c = 'Stripe',
                                          Current_Week__c=3,Opportunities_Time_Difference__c=true,Refunded_Date__c=system.today(),
                                          WC_Order_Id__c = '1760(3)',WC_Product_Id__c = '506',Refund_Amount__c = 2
                                         );
        insert opp;
        oppList.add(opp);
        pap.Opportunity__c = opp.id;
        insert pap;
        Map<Id,Opportunity> opMap = new Map<Id,Opportunity>();
        opMap.put(opp.id, opp);
        Opportunity opp1 = [select Id,contact_email__c ,Contact__r.Email from Opportunity limit 1];
        System.debug('==========='+opp1.contact_email__c+'--------'+opp1.Contact__r.Email);
        
        opp.Payment_Date__c = opp.Payment_Date__c.addDays(2);
        update opp; 
        
        Test.startTest();
        OpportunitysTriggerHandler.rollupDelete(oppList);
        OpportunitysTriggerHandler.refundPapCommission(oppList, opMap);
        Test.stopTest();
        
    }
    
    static testMethod void testHttpRequest1() {
        ApexUtil.isTriggerInvoked = true;
        
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        contact c = new contact();
        c.LastName = 'test';
        c.Email = 'test@gmail.com';
        insert c;
        
        Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 300151;
        insert apex;
        
        Triggers_activation__c activation = new Triggers_activation__c();
        activation.SuccessPaymentWithCaseLink__c = true;
        activation.CampaignMemberdeleteHelper__c = true;
        activation.isCampaignAssociate__c = true;
        activation.Opportunitytriggerhandler__c = true;
        insert activation;
        
        Case ca = new Case();
        ca.Status = 'New';
        ca.IsFailed_Payment__c = true;
        ca.UpdateCardStatus__c = 'Payment Successful';
        ca.ContactId = c.Id;
        insert ca;
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(ca.Id, yesterday);
        
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.order_id__c = '1760(3)';
        pap.product_id__c = '506';
        insert pap;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(),
                                          StageName='Closed Won',AccountId=acc.Id,Contact__c=c.Id,WC_Order_Id__c = '1760(3)',
                                          WC_Product_Id__c = '506', Payment_Date__c=system.today(),Amount=20,Initiated_From__c = 'Stripe');
        insert opp;
        Opportunity opp1 = [select Id,contact_email__c ,Contact__r.Email from Opportunity limit 1];
        System.debug('==========='+opp1.contact_email__c+'--------'+opp1.Contact__r.Email);
        
        opp.Payment_Date__c = opp.Payment_Date__c.addDays(2);
        update opp; 
        
        
    }
}