// Developer Name : Sourabh Badole 
// Date       : 25-07-2019
// Description : Test class for VF page Controler to show the remaining commission values LTVCoachingCommissionYear2Cont.
@isTest
public class LTVCoachingCommissionYear2ContTest {
    // Test Methods for code coverage LTVCoachingCommissionYear2Cont controller.
    @isTest
    public static void TestPageCon1(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Full Pay' ;
        con.Commission_Year__c = 'Year 2' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Full Pay', 
            Commission_Parent_Type__c='$2997 Full Pay', 
            Commission_Year__c='Year 2', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
        insert commi;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    @isTest
    public static void TestPageCon2(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    
    @isTest
    public static void TestPageCon3(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Commission_Year__c = 'Year 2' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$297 Monthly Payment', 
            Commission_Year__c='Year 2', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
         
        insert commi;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    @isTest
    public static void TestPageCon4(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Monthly Subscription';
        con.Commission_Year__c = 'Year 2' ;
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c='Monthly Subscription', 
            Commission_Parent_Type__c='$297 Monthly Payment', 
            Commission_Year__c='Year 2', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
         
        insert commi;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
	@isTest
    public static void TestPageCon5(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Down Payment with Subscription';
        con.Down_Payment_with_Subscription__c = 'Down Payment and $297 Monthly Payment';
        con.Down_Payment_and_297_Monthly_Payment__c    = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 2' ;
        con.Payment_Month_Remaining__c = 1 ;
        con.Down_Payment_Amount__c = 10  ;
        con.Total_Amount_Remaining__c = 10 ;
        
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Down Payment with Subscription', 
            Commission_Parent_Type__c='Down Payment and $297 Monthly Payment', 
            Commission_Year__c='Year 2', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
	@isTest
    public static void TestPageCon6(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$297 Subscription with Final Payment';
        con.X297_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 2' ;
        con.Payment_Month_Remaining__c = 1 ;
        con.Down_Payment_Amount__c = 10  ;
        con.Total_Amount_Remaining__c = 10 ;
        
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$297 Subscription with Final Payment', 
            Commission_Year__c='Year 2', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear2;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    @isTest
    public static void TestPageCon7(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$297 Subscription with Final Payment';
        con.X297_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 3' ;
        con.Payment_Month_Remaining__c = 1 ;
        con.Down_Payment_Amount__c = 10  ;
        con.Total_Amount_Remaining__c = 10 ;
        
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$297 Subscription with Final Payment', 
            Commission_Year__c='Year 3', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear3;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    @isTest
    public static void TestPageCon8(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$297 Subscription with Final Payment';
        con.X297_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 4' ;
        con.Payment_Month_Remaining__c = 1 ;
        con.Down_Payment_Amount__c = 10  ;
        con.Total_Amount_Remaining__c = 10 ;
        
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$297 Subscription with Final Payment', 
            Commission_Year__c='Year 4', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommissionYear4;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
    @isTest
    public static void TestPageCon9(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Coaching_Payment_Option__c = 'Subscription with Final Payment';
        con.Subscription_with_Final_Payment__c = '$297 Subscription with Final Payment';
        con.X297_Subscription_with_Final_Payment__c   = 'M1C1;M1C2;M2C1;M2C2;';
        con.Commission_Year__c = 'Year 1' ;
        con.Payment_Month_Remaining__c = 1 ;
        con.Down_Payment_Amount__c = 10  ;
        con.Total_Amount_Remaining__c = 10 ;
        
        insert con ;
        
        Coaching_Commissions__c commi = new Coaching_Commissions__c(
            Contact__c= con.id,
            Coaching_Payment_Option__c= 'Subscription with Final Payment', 
            Commission_Parent_Type__c='$297 Subscription with Final Payment', 
            Commission_Year__c='Year 1', 
            Commission_Amount__c=16.88, 
            Commission_Type__c='M1C1'
            );
		Insert commi ;
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        PageReference pageRef = Page.LTVCoachingCommission;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        LTVCoachingCommissionYear2Cont commiPayCon = new LTVCoachingCommissionYear2Cont(sc);
        commiPayCon.redirect1() ;
        commiPayCon.redirect2() ;
        commiPayCon.redirect3() ;
        commiPayCon.redirect4() ;
        
    }
}