@RestResource(urlMapping='/CardStripeWebhook/*')
global without sharing class CardStripeWebhook {
	@HttpPost
	global static void CardStripe() {
		if(RestContext.request.requestBody!=null){
            String server = 'Production';
			system.debug('###body: '+RestContext.request.requestBody);
            try{
                StripeCard.StripeCardObject varCard = (StripeCard.StripeCardObject) json.deserialize(RestContext.request.requestBody.toString().replace('object','object_data'),StripeCard.StripeCardObject.class);
                String customerId = varCard.data.object_data.customer;
                
                HttpResponse res = new HttpResponse();
                if(!Test.isRunningTest()){ res = StripeConnection.HttpRequest('customers','/'+customerId);
                }else{
                    String bodyTest = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"wynche5@cloudcreations.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": null, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, '+
                        '\"sources\": { \"object\": \"source\", \"data\":[{\"id\":\"crd_12345678\"}] }, \"type\": \"customer.created\" }';
                    res.setStatusCode(200);
                    res.setBody(bodyTest);
                }
                
                if(res.getStatusCode()==200){
                    StripeListCustomer.Data varCustomer = (StripeListCustomer.Data) json.deserialize(res.getBody(),StripeListCustomer.Data.class);
                    
                    List<Account> lstAccount = [Select Id, Name From Account WHERE External_ID__c=:customerId];
                    Account acc = new Account();
                    if(lstAccount.size()>0){
                        acc.Id = lstAccount.get(0).Id;
                        acc.Name = lstAccount.get(0).Name;
                    }else{
                        if(varCustomer.metadata!=null && varCustomer.metadata.name!=null){
                            acc.Name = varCustomer.metadata.name;
                            acc.First_Name__c = varCustomer.metadata.first_name;
                            acc.Last_Name__c = varCustomer.metadata.last_name;
                            acc.Phone = varCustomer.metadata.phone;
                        }else{
                            if(varCustomer.email!=null && varCustomer.email!=''){
                                acc.Name = varCustomer.email;    
                            }else{
                                acc.Name = varCustomer.id;    
                            }
                        }
                       	if(acc.Name==null) acc.Name = customerId;
                    }
                    
                    acc.External_ID__c = customerId;
                    
                    Boolean haveCard = false;
                    if(varCustomer.sources!=null){
                        for(StripeListCustomer.DataSource source : varCustomer.sources.data){
                            system.debug('###cust.sources: '+source.id);
                            if(source.id.substring(0,3)=='src'){ acc.Source_ID__c = source.id;
                            }else{
                                acc.Card_ID__c = source.id;
                                if(source.address_city!=null){ if(source.address_city.length()>40) acc.BillingCity = source.address_city.substring(0,40); else acc.BillingCity = source.address_city; }
                                if(source.address_country!=null) acc.BillingCountry = source.address_country;
                                if(source.address_line1!=null) acc.BillingStreet = source.address_line1;
                                if(source.address_state!=null) acc.BillingState = source.address_state;
                                if(source.address_zip!=null){ if(source.address_zip.length()>20) acc.BillingPostalCode = source.address_zip.substring(0,20); else acc.BillingPostalCode = source.address_zip; }
                                acc.Brand__c = source.brand;
                                acc.Country__c = source.country;
                                acc.Cvc_Check__c = source.cvc_check;
                                acc.Exp_Month__c = String.valueOf(source.exp_month);
                                acc.Exp_Year__c = source.exp_year;
                                acc.Last4__c = source.last4;
                                haveCard = true;
                                break;
                            }
                        }
                    }
                    
                    if(haveCard==false){
                        acc.Card_ID__c = null; acc.Brand__c = null; acc.Country__c = null; acc.Cvc_Check__c = null; acc.Exp_Month__c = null; acc.Exp_Year__c = null; acc.Last4__c = null;
                        /*acc.BillingCity = null;
                        acc.BillingCountry = null;
                        acc.BillingStreet = null;
                        acc.BillingState = null;
                        acc.BillingPostalCode = null;*/
                    }
                    
                    ApexUtil.isTriggerInvoked = true;
                    if(lstAccount.size()>0){
                        update acc;
                    }else{
                        upsert acc External_ID__c;
                    }
                    //ApexUtil.isTriggerInvoked = false;
                }else{
                    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); mail1.setSubject(server+': CardStripeWebhook Request'); mail1.setHtmlBody(res.getStatusCode() + ' - ' + res.getBody() + ' - ' + RestContext.request.requestBody.ToString()); Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); 
                }
                
                /*Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'});
                mail1.setSubject('Sandbox : CardStripeWebhook Request xxx');
                mail1.setHtmlBody(strr);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });*/
                
            }catch(Exception e){
                if(!e.getMessage().contains('duplicate value found: External_ID__c')){ Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); mail1.setSubject(server+': CardStripeWebhook Request'); mail1.setHtmlBody(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + RestContext.request.requestBody.ToString()); Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); }
            }
		}
		else{
    		RestResponse res = RestContext.response; res.statusCode = 400;		    
		}
	}
}