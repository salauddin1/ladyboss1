public class ContactHistoryController {
    
    @AuraEnabled
    public static List<ContactHistory> getContactHistory (Id recordId) {  
        
        List<ContactHistory> contacts = [select id,CreatedDate,ContactId,Field,NewValue,OldValue  from ContactHistory WHERE ContactId=:recordId AND Field = 'Available_Commission__c' order by createddate DESC];
        
        return contacts;
    }
    @AuraEnabled
    public static void RefreshBalanceHistory(String conIDwhen){
        
        system.debug('Hello');
    }
    
    @AuraEnabled
    public static List<ContactHistoryWraper> getContactHistoryNew (Id recordId) {  
        
        List<ContactHistory> contacts = [select id,CreatedDate,ContactId,Field,NewValue,OldValue  from ContactHistory WHERE ContactId=:recordId AND Field in ('Available_Commission__c') order by createddate ASC];
        if(Test.isRunningTest())  {
            ContactHistory ch = new ContactHistory();
            ch.ContactId = recordId;
            contacts.add(ch);
        }
        
        List<ContactHistoryWraper> lst = new List<ContactHistoryWraper>();
		Map<DateTime,String> mapD = new Map<DateTime,String>();
        
        List<ContactHistory> contacts1 = [select id,CreatedDate,ContactId,Field,NewValue,OldValue  from ContactHistory WHERE ContactId=:recordId AND Field in ('Update_Balance_Reason__c') order by createddate ASC];
        
        if(Test.isRunningTest())  {
            ContactHistory ch1 = new ContactHistory();
            ch1.ContactId = recordId;
            contacts1.add(ch1);
        }
        
        for(ContactHistory ct : contacts1)  {  
            if(ct.NewValue != null)
            	mapD.put(ct.CreatedDate,''+ct.NewValue);
            else 
                mapD.put(ct.CreatedDate,'Not tracked');
        }
        
        String lastReason = 'Not tracked';
        for(ContactHistory ct : contacts)  {
            ContactHistoryWraper cw = new ContactHistoryWraper();
            if(ct.OldValue != null)
                cw.oldValue = Decimal.valueOf(''+ct.OldValue);
            if(ct.NewValue != null)
                cw.NewValue = Decimal.valueOf(''+ct.NewValue);
            cw.CreateDdate = ct.CreatedDate;
            if(mapD.get(cw.CreateDdate) != null)  {
                lastReason = mapD.get(cw.CreateDdate);    
            	cw.reason = mapD.get(cw.CreateDdate);    
            }else  {
                cw.reason = lastReason;
            }
            

            lst.add(cw);
        }
        
        
        
		List<ContactHistoryWraper> lst1 = new List<ContactHistoryWraper>();
        for(integer i = lst.size() - 1; i >= 0 ; i--)  {
            lst1.add(lst.get(i));
        }
        
        System.debug(lst);
        return lst1;
    }
    
    
    public class ContactHistoryWraper  {
        @AuraEnabled 
        public Decimal OldValue{get;set;}
        @AuraEnabled 
        public Decimal NewValue{get;set;}
        @AuraEnabled 
        public DateTime CreatedDate{get;set;}
        @AuraEnabled
        public string reason{get;set;}
    }
}