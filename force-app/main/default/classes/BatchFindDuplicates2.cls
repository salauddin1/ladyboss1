/*
 * Developer Name : Tirth Patel
 * Description : This is duplicate of BatchFindDuplicates. It will be deleted as soon as task is done.
 */
public class BatchFindDuplicates2 implements  Database.Batchable<AggregateResult>,Schedulable  {
    public Iterable<AggregateResult> start(Database.BatchableContext bc) {
        if(Test.isRunningTest()){
        	return new AggregateResultIterable('SELECT count(id), email contactEmail from contact where email != null and email like \'%yahoo.com\'  and email!=\'notavail@gmail.com\' group by email having count(id) > 1 order by count(id) desc limit 100');
        }else{
            return new AggregateResultIterable('SELECT count(id), email contactEmail from contact where email != null and (email like \'n%gmail.com\') and email!=\'notavail@gmail.com\' group by email having count(id) = 2 order by count(id) desc limit 100');
        }     
    }
    
    public void execute(SchedulableContext SC) {
        BatchFindDuplicates2 btc = new BatchFindDuplicates2(); 
        Database.executeBatch(btc);
    } 
    
    public void execute(Database.BatchableContext bc, List<Sobject> lstSo){
        
        List<String> lstEmail = new List<String>();
        for(sObject sObj : lstSo) {
            AggregateResult ar = (AggregateResult)sObj;
            lstEmail.add((String)ar.get('contactEmail'));
        }
        List<Contact> lstContacts = [select id ,email from Contact where Email in :lstEmail order by createdDate asc ];
        Set<String> setEmails = new Set<String>();
        LIst<Contact> lstCntUpdate = new List<Contact>();
        for(Contact cnt : lstContacts)  {
            if(setEmails.add(cnt.Email.toLowerCase()))  {
                cnt.check_for_dup_2__c = true;
                lstCntUpdate.add(cnt);   
            }  
        } 
        update lstCntUpdate;
        
    }
    
    public void finish(Database.BatchableContext bc){ 
        List<Contact> contList = [Select id from Contact Where check_for_dup_2__c=true];
        // execute any post-processing operations
        if(!test.isRunningTest() && contList.size()>0 && !(System.now().hour() >= 9 && System.now().hour() < 17 ) )  {
            ContactMergeBatch2 cmb = new ContactMergeBatch2(UserInfo.getSessionId());
            Database.executeBatch(cmb,1);
        }
    }
    
    
}