public class UpdateCaseOwnerController {
    public List<String> idSet{get; set;}
    public List<User> UserList{get; set;}
    public String User{get; set;}
    Public List<SelectOption> options{get; set;}
    public boolean displayPopUp {get; set;}
    public boolean showSpinner {get; set;}
    public String filterId;
    
    public  UpdateCaseOwnerController(ApexPages.StandardSetController standardsetController){
        //showSpinner = true;
        system.debug(standardsetController.getFilterId());
        system.debug(ApexPages.currentPage().getHeaders().get('Host'));
        this.filterId = standardsetController.getFilterId();
        options = new List<SelectOption>();
          for(Group g : [SELECT Id, Name, Type FROM Group where Type = 'Queue']){
            options.add(new SelectOption(g.Id,g.Name));
        }
        for(User u : [SELECT Id,Name FROM User WHERE isActive = true]){
            options.add(new SelectOption(u.Id,u.Name));
        }
      
        
        idSet = new List<String>(); 
        for (sObject ca : (List<sObject>)StandardSetController.getSelected()) { 
            idSet.add(ca.Id);                
        } 
        if(!idSet.isEmpty()){
            displayPopUp = true;
        }else{
            displayPopUp = false;
        }
        system.debug('idSet-->'+idSet);
        system.debug(ApexPages.currentPage().getUrl());
       // showSpinner = false;
    }
    
    
    
    ApexPages.StandardSetController standardsetController;
    public  PageReference closeModal(){
        system.debug(URL.getSalesforceBaseUrl().toExternalForm());
        system.debug(filterId);
        PageReference returnPage = new PageReference('/one/one.app?source=aloha#/sObject/Case/list?filterName='+filterId); 
        returnPage.setRedirect(true);
        system.debug('pr-->'+returnPage);
        return returnPage;
        //System.PageReference = standardsetController
    }
    
    
    public  PageReference updateSelectedCases(){
        system.debug('idSet-->'+idSet);
        system.debug('User-->'+User);
        List<Case> updateList = new List<Case>();
        if(!idSet.isEmpty()){
            for(Case c : [SELECT Id, OwnerId, CaseNumber FROM Case WHERE Id IN :idSet]){ 
                system.debug('cL'+c);
                c.OwnerId = User;
                system.debug('cL'+c);
                updateList.add(c);
            }
        }
        update updateList; 
        PageReference returnPage = new PageReference('/one/one.app?source=aloha#/sObject/Case/list?filterName='+filterId); 
        returnPage.setRedirect(true);
        system.debug('pr-->'+returnPage);
        return returnPage;
    }
}