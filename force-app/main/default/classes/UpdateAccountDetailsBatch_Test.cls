@isTest
public class UpdateAccountDetailsBatch_Test {
    
    public static testmethod void UpsateAccountDetailsBatch_Test()
    {
        UpdateAccountDetailsBatch uADB = new UpdateAccountDetailsBatch();
        
        Account acnt = new Account();
        acnt.name='test';
        acnt.Email__c='test@test.com';
        acnt.External_ID__c='64565555';
        acnt.Credit_Card_Number__c='4567895215';
        acnt.Last_CC_Modified_Date__c=DateTime.parse('05/22/2012 11:45 AM');
        
        insert acnt;
                 
        Account_Old_CC__c acntOldCC =new Account_Old_CC__c();
        acntOldCC.Last_CC_Num__c='jklhsdjsa';
        acntOldCC.Account__c=acnt.id;
        insert acntOldCC;
        
        Account_Old_Email__c acntOldEmail = new Account_Old_Email__c();
        acntOldEmail.Last_Email__c='test12@test.com';
        acntOldEmail.Account__c=acnt.id;
        insert acntOldEmail;
        
        Account_Old_Stripe_Id__c acntOldStripe = new Account_Old_Stripe_Id__c();
        acntOldStripe.Last_Stripe_Id__c='4545';
        acntOldStripe.Account__c=acnt.id;
        insert acntOldStripe;
        
        acnt.Name='testnew';
        acnt.Email__c='testnewnew@new.com';
        acnt.External_ID__c='874121';
        acnt.Credit_Card_Number__c='456787787787895215';
        acnt.Last_CC_Modified_Date__c=DateTime.parse('05/22/2012 11:59 AM');
        acnt.BillingCountry = 'US';
        acnt.ShippingCountry = 'US';
        
        acntOldEmail.Last_Email__c='whynewagain@new.com';
       
        acntOldStripe.Last_Stripe_Id__c='7788';
        
        acntOldCC.Last_CC_Num__c='hsjdhdehuj';
        
        
        update acnt;
        
        update acntOldCC;
        update acntOldEmail;
        update acntOldStripe;
        
        
        Test.startTest();
        Database.executeBatch(uADB);
        Test.stopTest();
    }
    
    public static testmethod void AccountMergeBatch_Test()
    {
        AccountMergeBatch acntMB = new AccountMergeBatch();
            
        Account acnt1 = new Account();
        acnt1.name='test';
        //acnt1.LastModifiedDate='3/4/18';
        acnt1.BillingCity='Dallas';
        acnt1.BillingCountry='US';
        acnt1.BillingStreet='4420 Preston Cir.';
        acnt1.Phone='9723225435';
        acnt1.BillingState='TX';
        acnt1.BillingPostalCode='75211';
        acnt1.Last_Stripe_Modified_Date__c=DateTime.parse('05/22/2012 11:46 AM');
        acnt1.Last_Email_Modified_Date__c=DateTime.parse('05/22/2012 11:49 AM');
        acnt1.Last_CC_Modified_Date__c=DateTime.parse('05/22/2012 11:55 AM');
        
        insert acnt1;
        
        Account acnt2 = new Account();
        acnt2.name='test1';
        acnt2.BillingCity='Dallas1';
        acnt2.BillingCountry='US1';
        acnt2.BillingStreet='4420 Preston Cir.1';
        acnt2.Phone='97232254351';
        acnt2.BillingState='TX1';
        acnt2.BillingPostalCode='752111';
        acnt2.Last_Stripe_Modified_Date__c=DateTime.parse('05/22/2012 10:46 AM');
        acnt2.Last_Email_Modified_Date__c=DateTime.parse('05/22/2012 10:49 AM');
        acnt2.Last_CC_Modified_Date__c=DateTime.parse('05/22/2012 10:55 AM');
        
        insert acnt2;
        
        acnt2.name='test';
        acnt2.BillingCity='Dallas';
        acnt2.BillingCountry='US';
        acnt2.BillingStreet='4420 Preston Cir.';
        acnt2.Phone='9723225435';
        acnt2.BillingState='TX';
        acnt2.BillingPostalCode='75211';
        update acnt2;   
        /*acnt2.Last_CC_Modified_Date__c = DateTime.parse('05/22/2012 11:49 AM');
        acnt2.Last_Email_Modified_Date__c = DateTime.parse('05/22/2012 11:26 AM');
        acnt2.Last_Stripe_Modified_Date__c = DateTime.parse('05/22/2012 11:36 AM');
        update acnt2;
        */
        Test.startTest();
        Database.executeBatch(acntMB);
        AccountMergeBatch b = new AccountMergeBatch();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, b); 
        
        Test.stopTest();
        
    }

}