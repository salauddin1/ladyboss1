@isTest
private class OpportunityLineItemInvoicer2Test
{
    @isTest
    static void itShould()
    {
        ContactTriggerFlag.isContactBatchRunning = true;
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        contact con = new contact();
        con.lastName = 'Tes';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        
        card__c cd = new card__c();
        
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        
        insert cd;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,card__c=cd.Id);
        insert opp;
        
        Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
        insert pro;
        
        Product2 pro2 = new Product2(Name='Test',Type__c='Stripe',isActive=true);
        insert pro2;
        
        PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe;
        
        PricebookEntry pbe2 = new PricebookEntry(Product2Id=pro2.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
        insert pbe2;
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20,Tax_Amount__c=1.2,Tax_Percentage_s__c=5,Shipping_Costs__c=1.8);
        insert oli;
        
        OpportunityLineItem oli2 = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20,Tax_Amount__c=1.2,Tax_Percentage_s__c=5,Shipping_Costs__c=1.8);
        insert oli2;
        
        Test.StartTest(); 
        PageReference pageRef = Page.OpportunityLineItemInvoice2;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(oli2);
        ApexPages.currentPage().getParameters().put('Id',oli2.id);
        
        OpportunityLineItemInvoiceController2 obj = new OpportunityLineItemInvoiceController2(sc1);
        
        Test.StopTest();
        
    }
}