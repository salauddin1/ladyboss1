global class StripeCustomer {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers';
    
    private static final String SERVICE_URLNEW ='https://api.stripe.com/v1/subscriptions';
    
    global static final String TEST_ID = 'cus_00000000000000';
    
    global Integer created;
    global Integer account_balance;
    global String description;
    global StripeError error;
    global Boolean livemode;
    global cls_subscriptions subscriptions;
    global StripeSubscription subscription;
    global String token;
    global String email;
    global String id;
    global Map<String, String> metadata;
    global cls_sources sources;
    global CardList cards;
    global String default_card;
    global String default_source;
    
    global class CardList {
        global Integer count;
        global String url;
        global List<StripeCard> stripeData;
    }
    
    global class cls_sources {
        public String stripeObject;
        public StripeCard[] StripeData;
        public boolean has_more;
        public Integer total_count; //4
        public String url;  ///v1/customers/cus_EEYYSGCq9L091j/sources
    }
    
    global class cls_subscriptions {
        public boolean has_more;
        public Integer total_count; //0
        public String url;  ///v1/customers/cus_EJGIUKvbKLm7Cu/subscriptions
    }    
    // static cache to eliminate multiple lookups in same transaction
    private static Map<String, StripeCustomer> mCustomers = new Map<String, StripeCustomer>();
    
    global static StripeCustomer getCustomer(String customerId) {
        if (mCustomers.containsKey(customerId)) {
            return mCustomers.get(customerId);
        }
        
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URL+'/'+customerId);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData);
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            if (!Test.isRunningTest()) {
                StripeCustomer o = StripeCustomer.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** Stripe Customer: '+o); 
            //if (o.error != null) throw new StripeException(o.error);
            mCustomers.put(customerId, o);
            return o;
            }else{
                StripeCustomer sc = new StripeCustomer();
                sc.sources = new cls_sources(); 
                sc.sources.total_count = 4;
                StripeCard scard = new StripeCard();
                scard.stripeObject = 'card';
                StripeCard scard2 = new StripeCard();
                scard2.stripeObject = 'card';
                List<StripeCard> scardList = new List<StripeCard>();
                scardList.add(scard);
                scardList.add(scard2);
                sc.sources.StripeData = scardList;
sc.subscriptions = new cls_subscriptions();
                sc.subscriptions.total_count =4;
                return sc;
            }
            
        } catch (System.JSONException e) {
            return null;
        }
    }
    
    global static StripeCustomer create(String token, String FullName,String email,String billingAddress,String phone,String conId) {
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',FullName);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        metadata.put('Sales Person',UserInfo.getName());
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        Map<String, String> properties = new Map<String, String>{
            //'source' => token,
            'description' => FullName,
                'email' => email
                
                
                };
                    return StripeCustomer.create(properties, metadata);
    }
    
    global static StripeCustomer createCardWithTokenCUS(String token, String FullName,String email,String billingAddress,String phone,String conId) {
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',FullName);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        metadata.put('Sales Person',UserInfo.getName());
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        Map<String, String> properties = new Map<String, String>{
            //'source' => token,
            'description' => FullName,
                'email' => email
                
                
                };
                    return StripeCustomer.newCustomer(null,properties, metadata);
    }
    
    global static StripeCustomer create(String token, String plan, String description,String email,String billingAddress,String phone,String conId) {
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',description);
        metadata.put('Sales Person',UserInfo.getName());
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        Map<String, String> properties = new Map<String, String>{
            'card' => token,
                'plan' => plan,
                'description' => description,
                'email' => email
                
                };
                    return create(properties, metadata);
    }
    
    
    global static StripeCustomer create(Map<String, String> properties, Map<String, String> metadata) {
        return updateCustomer(null, properties, metadata);      
    }
    
    
    
    
    
    global static StripeCustomer updateCustomer(String customerId, Map<String, String> properties, Map<String, String> metadata) {
        HttpRequest http = new HttpRequest();
        
        if (customerId == null) 
            http.setEndpoint(StripeCustomer.SERVICE_URL);
        else 
            http.setEndpoint(StripeCustomer.SERVICE_URL+'/'+customerId);
        
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String, String> payload = new Map<String, String>();
        if (properties != null) {
            for (String key : properties.keySet()) {
                if (properties.get(key) != null) {
                    payload.put(key, properties.get(key));
                }
            }
        }
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY of updateCustomer:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                system.debug('#### [1]'+ hs.getBody());
                
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody(StripeCustomerTests.testData);
            hs.setStatusCode(200);
        }
        
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCustomer new_customer = StripeCustomer.parse(hs.getBody());
            if (new_customer.error != null) throw new StripeException(new_customer.error);
            
            System.debug(System.LoggingLevel.INFO, '\n**** Customer: '+new_customer); 
            mCustomers.put(new_customer.id, new_customer);
            return new_customer;
        } catch (System.JSONException e) {
            return null;
        }
    }
    
    global static StripeCustomer newCustomer(String customerId, Map<String, String> properties, Map<String, String> metadata) {
        HttpRequest http = new HttpRequest();
        
        if (customerId == null) 
            http.setEndpoint(StripeCustomer.SERVICE_URL);
        else 
            http.setEndpoint(StripeCustomer.SERVICE_URL+'/'+customerId);
        
        http.setMethod('POST');
        
        String authorizationHeader = 'Bearer ' +StripeAPI.ApiKey;
        
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        Map<String, String> payload = new Map<String, String>();
        if (properties != null) {
            for (String key : properties.keySet()) {
                if (properties.get(key) != null) {
                    payload.put(key, properties.get(key));
                }
            }
        }
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY of updateCustomer:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
                system.debug('#### [1]'+ hs.getBody());
                
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody(StripeCustomerTests.testData);
            hs.setStatusCode(200);
        }
        
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        try {
            StripeCustomer new_customer = StripeCustomer.parse(hs.getBody());
            if (new_customer.error != null) throw new StripeException(new_customer.error);
            
            System.debug(System.LoggingLevel.INFO, '\n**** Customer: '+new_customer); 
            mCustomers.put(new_customer.id, new_customer);
            return new_customer;
        } catch (System.JSONException e) {
            return null;
        }
    }
    
    global static StripeCustomer setDefaultCard(String customerId, StripeCard card) {
        Map<String, String> properties = new Map<String, String>{
            'default_card' => card.id
                };
                    
                    return updateCustomer(customerId, properties, null);
    }
    
    
    global static StripeCustomer setDefaultCard(String customerId, String cardId) {
        Map<String, String> properties = new Map<String, String>{
            'default_card' => cardId
                };
                    
                    return updateCustomer(customerId, properties, null);
    }
    
    global static StripeCustomer setDefaultACH(String customerId, String ACHId) {
        Map<String, String> properties = new Map<String, String>{
            'default_source' => ACHId
                };
                    
                    return updateCustomer(customerId, properties, null);
    }
    
    
    global static StripeSubscription cancelSubscription(String customerId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URL+'/'+customerId+'/subscription');
        http.setMethod('DELETE');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData_cancelSubscription);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeSubscription sub = StripeSubscription.parse(response);
            return sub;
        } catch (System.JSONException e) { 
            return null;
        }
        
    }
    
    @future (callout=true)
    global static void cancelSubscription_future(String customerId) {
        cancelSubscription(customerId);
    }
    
    global static StripeSubscription updateSubscription(String customerId, String plan,String Name,String billingAddress,String email, String phone, String contactId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email); 
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
        return updateSubscription(customerId, plan,null,metadata,null);
    }
    global static StripeSubscription updateSubscription(String customerId, String plan,String Name,String billingAddress,String email, String phone, String contactId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson,Decimal salesTaxPercentage) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email); 
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
        return updateSubscription(customerId, plan,null,metadata,salesTaxPercentage);
    }

     global static StripeSubscription updateSubscription(String customerId, Product2 prod,String Name,String billingAddress,String email, String phone, String contactId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson,Decimal salesTaxPercentage) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email); 
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
        return updateSubscription(customerId, prod,null,metadata,salesTaxPercentage);
    }

    
    global static StripeSubscription updateSubscription(String customerId, String plan,String Name,String billingAddress,String email, String phone, String contactId) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        metadata.put('Sales Person',UserInfo.getName());
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        
        return updateSubscription(customerId, plan,null,metadata,null);
    }
    
    global static StripeSubscription updateSubscription(String customerId, String plan, Date trial_end,Map<String, String> metadata,Decimal salesTaxPercentage) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URLNEW);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        Map<String, String> payload = new Map<String, String>();
        if(salesTaxPercentage != null){
            payload = new Map<String, String>{

            'plan' => plan,
                'tax_percent'=>String.valueof(salesTaxPercentage),
                'customer'=> customerId
                };
        }else{
            payload = new Map<String, String>{
            'plan' => plan,
                
                'customer'=> customerId
                };
        }
        
                    /*
if (trial_end != null) { 
DateTime dt = DateTime.newInstanceGmt(trial_end, Time.newInstance(0, 0, 0, 0));
Integer utc = (dt.getTime() / 1000).intValue();
payload.put('trial_end', String.valueOf(utc));
}
*/
                    
                    if (metadata != null) {
                        for (String key : metadata.keySet()) {
                            if (metadata.get(key) != null) {
                                payload.put('metadata['+key+']', metadata.get(key));
                            }
                        }
                    }
        http.setBody(StripeUtil.urlify(payload));   
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData_updateSubscription);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeSubscription o = StripeSubscription.parse(response);
            return o;
        } catch (System.JSONException e) {
            return null;
        }
    }
    global static StripeSubscription updateSubscription(String customerId, Product2 prod, Date trial_end,Map<String, String> metadata,Decimal salesTaxPercentage) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URLNEW);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        Map<String, String> payload = new Map<String, String>();
        List<Id> prodList = new List<Id>();
        if(prod.Product_One__c !=null){
               prodList.add(prod.Product_One__c);
        }if(prod.Product_Two__c !=null){
               prodList.add(prod.Product_Two__c);
        }if(prod.Product_Three__c !=null){
               prodList.add(prod.Product_Three__c);
        }if(prod.Product_Four__c !=null){
               prodList.add(prod.Product_Four__c);
        }if(prod.Product_Five__c !=null){
               prodList.add(prod.Product_Five__c);
        }

        if(salesTaxPercentage != null){

            Integer i=0;
            for(Product2 pr:[Select id,Stripe_plan_Id__c FROM Product2 WHERE Id IN :prodList]){
                payload.put('items['+i+'][plan]',pr.Stripe_plan_Id__c); 
                i++;               
            }
            payload.put('customer',customerId);
            payload.put('tax_percent',String.valueof(salesTaxPercentage));
        }else{
            Integer i=0;
            for(Product2 pr:[Select id,Stripe_plan_Id__c FROM Product2 WHERE Id IN :prodList]){
                payload.put('items['+i+'][plan]',pr.Stripe_plan_Id__c); 
                i++;               
            }
            payload.put('customer',customerId);
        }

                    
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        http.setBody(StripeUtil.urlify(payload));   
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData_updateSubscription);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeSubscription o = StripeSubscription.parse(response);
            return o;
        } catch (System.JSONException e) {
            return null;
        }
    }
    global static List<StripeCustomer> getCustomers(Integer pageSize, Integer offset) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URL+'?count='+pageSize+'&offset='+offset);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String t_json;
        Integer statusCode;
        
        List<StripeCustomer> customers = new List<StripeCustomer>();
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody(StripeCustomerTests.testData_getCustomers);
            hs.setStatusCode(200);
        }
        
        t_json = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            t_json = StripeUtil.cleanJson(t_json);
            t_json = t_json.substring(t_json.indexOf('['), t_json.lastIndexOf(']')+1);
            customers = (List<StripeCustomer>) JSON.deserialize(t_json, List<StripeCustomer>.class);
            return customers;
            
        } catch (System.JSONException e) {
            return null;
        }
    }
    
    public StripeCard getDefaultCard() {
        for (StripeCard card : this.cards.stripeData) {
            if (card.id == this.default_card) {
                return card;
            }
        }
        
        return null;
    }
    
    public static StripeCustomer parse(String json) {
        json = StripeUtil.cleanJson(json);
        System.debug('-----> '+(StripeCustomer) System.JSON.deserialize(json, StripeCustomer.class));
        return (StripeCustomer) System.JSON.deserialize(json, StripeCustomer.class);
    }
    
    global static StripeSubscription updateSubscription(String customerId,String cardId,String plan,String Name,String billingAddress,String email, String phone, String contactId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email); 
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
        return updateSubscription(customerId, cardId, plan,null,metadata,null);
    }

    global static StripeSubscription updateSubscription(String customerId,String cardId,String plan,String Name,String billingAddress,String email, String phone, String contactId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson,Decimal SalestaxPercentage) {
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email); 
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+contactId);
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
        return updateSubscription(customerId, cardId, plan,null,metadata,SalestaxPercentage);
    }

    global static StripeSubscription updateSubscription(String customerId, String cardId, String plan, Date trial_end,Map<String, String> metadata,Decimal salesTaxPercentage) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(StripeCustomer.SERVICE_URLNEW);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        Map<String, String> payload = new Map<String, String>();
        if(salesTaxPercentage != null){
            payload = new Map<String, String>{

            'plan' => plan,
                'tax_percent'=>String.valueof(salesTaxPercentage),
                'customer'=> customerId,
                'default_payment_method'=>cardId
                };
        }else{
            payload = new Map<String, String>{
                'plan' => plan,
                'default_payment_method'=>cardId,
                'customer'=> customerId
                };
        }              
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        http.setBody(StripeUtil.urlify(payload));   
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData_updateSubscription);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeSubscription o = StripeSubscription.parse(response);
            return o;
        } catch (System.JSONException e) {
            return null;
        }
    }
    
}