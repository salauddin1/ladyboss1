global class PlanStripeBatch implements Database.Batchable<Product2>, Database.AllowsCallouts, Database.Stateful{
    
    public String startingAfter {get;set;}
    public Boolean hasMore {get;set;}
    public List<String> lstProductExternalId {get;set;}
    
    public PlanStripeBatch()
    {
        system.debug('###construct PlanStripeBatch');
        lstProductExternalId = new List<String>();
        startingAfter = '';
        hasMore = false;
    }
    
    public PlanStripeBatch(String stAfter)
    {
        system.debug('###construct PlanStripeBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        lstProductExternalId = new List<String>();
        startingAfter = stAfter;
        hasMore = false; 
    }
    
	public List<Product2> start(Database.BatchableContext BC ){
        system.debug('###start PlanStripeBatch');
		List<Product2> lstProduct = new List<Product2>();
        HttpResponse res = new HttpResponse();
        if(startingAfter!=null && startingAfter!=''){
        	res = StripeConnection.HttpRequest('plans','?limit=25&starting_after='+startingAfter);    
        }else{
            res = StripeConnection.HttpRequest('plans','?limit=25');
        }
        
        if(res.getStatusCode()==200){
            StripeListPlan.Plans varPlans;
            JSONParser parse;
            parse = JSON.createParser(res.getBody().replaceAll('currency','currency_data'));
            varPlans = (StripeListPlan.Plans)parse.readValueAs(StripeListPlan.Plans.class);
            Product2 pro;
            
            Datetime createdDate;
            for(StripeListPlan.Data plan : varPlans.data){     
                pro = new Product2();
                pro.Name = plan.name;
                pro.IsActive = true;
                pro.External_ID__c = plan.id;
                pro.Price__c = plan.amount*0.01;
                pro.Currency__c = plan.currency_data;
                pro.Interval__c = plan.interval;
                pro.Interval_Count__c = plan.interval_count;
                pro.Statement_Descriptor__c = plan.statement_descriptor;
                pro.Type__c = 'Stripe';
                
                startingAfter = pro.External_ID__c;
                lstProduct.add(pro);
                lstProductExternalId.add(pro.External_ID__c);
            }
            
            hasMore = varPlans.has_more;
        }
        
        return lstProduct;
	}
	
	public void execute(Database.BatchableContext BC, List<Product2> scope) {
        system.debug('###execute PlanStripeBatch');
        if(scope.size()>0){
            ApexUtil.isTriggerInvoked = true;
            upsert scope External_ID__c;
            ApexUtil.isTriggerInvoked = false;

            Map<String,String> mapPbe = new Map<String,String>();
            for(PricebookEntry pbe : [Select Id, Product2.Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c IN: lstProductExternalId]){
                mapPbe.put(pbe.Product2.External_ID__c.toLowerCase(),pbe.Id);
            }
            
            String pbId = '';
            if(!Test.isRunningTest()){
                pbId = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
            }else{
                pbId = Test.getStandardPricebookId();
            }

            PricebookEntry pbeNew;
            List<PricebookEntry> lstPbe = new List<PricebookEntry>();
            for(Product2 pro : scope){
                if(mapPbe.get(pro.External_ID__c.toLowerCase())==null){
                    pbeNew = new PricebookEntry();
                    pbeNew.IsActive = true;
                    pbeNew.Pricebook2Id = pbId;
                    pbeNew.Product2Id = pro.Id;
                    pbeNew.UnitPrice = pro.Price__c;
                    lstPbe.add(pbeNew);
                }
            }
            
            if(lstPbe.size()>0){
                ApexUtil.isTriggerInvoked = true;
                insert lstPbe;
                ApexUtil.isTriggerInvoked = false;
            }
            
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish PlanStripeBatch');
        system.debug('###finish hasMore: '+hasMore);
        system.debug('###finish startingAfter: '+startingAfter);
        if(!Test.isRunningTest()){
            if(hasMore){ Database.executeBatch(new PlanStripeBatch(startingAfter)); }
        }
    }

}