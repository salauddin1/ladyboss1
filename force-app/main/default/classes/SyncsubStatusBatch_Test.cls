@isTest
private class SyncsubStatusBatch_Test {
	
	@isTest static void test_method_one() {
		contact con = new contact();
        con.lastName='test';
        insert con;

        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        List<OpportunityLineItem> oplineitem = new List<OpportunityLineItem>();
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        
        insert ol;
        oplineitem.add(ol);
        Test.startTest();
        SyncsubStatusBatch ssb = new SyncsubStatusBatch();
        Database.QueryLocator ql = ssb.start(null);
		ssb.execute(null,oplineitem);
		ssb.Finish(null);
        Test.stopTest();
	}
	
}