//@Developer Name - Ashish Sharma
//@Description    - This class moved all the contact,s tagged with product value and have 35 days old latest purchase.
//It moves all such contacts to highest product,s pipeline campaigns.

global class CampaignPipelineMovementBatch implements Database.Batchable<sObject>{
    
    
    global CampaignPipelineMovementBatch () {}
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        String query = 'SELECT Id, Name, Product__c, Highest_Product__c  from Contact where Product__c != null'; 
        return Database.getQueryLocator(query);  
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> contList){
        
        system.debug('====contList===>'+contList);
        Set<Id> conIds = new Set<Id>();        
        for(Contact con:contList){
            conIds.add(con.id);    
        } 
        
        Date toDate = system.Today().addDays(-35);
        List<Opportunity> oppList = new List<Opportunity>();       
        oppList=[Select id, Name, CloseDate, Contact__c from opportunity where createddate>= : toDate and contact__c in:conIds order By createdDate ASC];

        Set<Id> conIdsToOperate = new Set<Id>();
        for(Contact con:contList){  
            Boolean allowRecordCreation = true;
            for(Opportunity opp:oppList){
                if(opp.contact__c==con.id){
                    allowRecordCreation=false;
                    break;
                }        
            }      
            if(allowRecordCreation==true){
                conIdsToOperate.add(con.id);
            }   
        } 
        
        //Get All CampaignMembers with Phone Team Type Campaign
        
        List<CampaignMember> campList = new List<CampaignMember>();
        campList = [select id from CampaignMember where contactId in:conIdsToOperate and campaign.Type='Phone Team'];
        
        system.debug('----campList----'+campList);
        if(campList.size()>0){
            System.enqueueJob(new CampaignMemberPipelineBatchDelQueue(new List<CampaignMember>(campList)));    
        } 
        
        
        Map<String,Campaign> mapPipeLineCampaign = new Map<String,Campaign>();
        for(Campaign cmp:[Select id, Product__c from Campaign where Campaign.Type='Pipeline']){
            mapPipeLineCampaign.put(cmp.Product__c,cmp);    
        }
        
        system.debug('----mapPipeLineCampaign----'+mapPipeLineCampaign);
        
        List<CampaignMember> pipelineCampignMembers = new List<CampaignMember>();
        pipelineCampignMembers = [select id,Campaign.Product__c,contactId from CampaignMember where contactId in:conIdsToOperate and Campaign.Type='Pipeline'];
        
        system.debug('----pipelineCampignMembers----'+pipelineCampignMembers);
        
        
        List<CampaignMember> pipelineCampaignMembers  = new List<CampaignMember>();
        for(Contact con:contList){ 
            Boolean doesExistIntoHighestPipeline = false;
            if(pipelineCampignMembers!=null && conIdsToOperate.contains(con.Id)){
                for(CampaignMember cmp:pipelineCampignMembers){
                    if(cmp.ContactId ==con.Id && cmp.Campaign.Product__c ==con.Highest_Product__c){
                        doesExistIntoHighestPipeline=true;
                    }
                    
                }
            }
            if(conIdsToOperate.contains(con.Id) && doesExistIntoHighestPipeline==false && con.Highest_Product__c!=null && mapPipeLineCampaign.containsKey(con.Highest_Product__c)){
                CampaignMember cmp = new CampaignMember();
                cmp.CampaignId=mapPipeLineCampaign.get(con.Highest_Product__c).Id;
                cmp.ContactId =con.Id;
                pipelineCampaignMembers.add(cmp);
            }
        }
        
        system.debug('----pipelineCampaignMembers----'+pipelineCampaignMembers);
        
        if(pipelineCampaignMembers.size()>0){
            Database.insert(pipelineCampaignMembers,false);
        }
    }
    global void finish(Database.BatchableContext bc){
        
    }    
    
}