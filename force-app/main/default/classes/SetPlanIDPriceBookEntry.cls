global class SetPlanIDPriceBookEntry implements Database.Batchable<sObject>,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,Product2.Stripe_Plan_Id__C,Stripe_Plan_Id__c FROM PricebookEntry WHERE Stripe_Plan_Id__c = null AND Product2.Stripe_Plan_Id__c != null');
    }
    
    global void execute(Database.BatchableContext BC, List<PricebookEntry> scope){
        String recordId = '';
        try{
            for (PricebookEntry pbe : scope) {
                recordId = pbe.id;
                pbe.Stripe_Plan_Id__c = pbe.Product2.Stripe_Plan_Id__c;                
            }
            
            update scope;
            if (Test.isRunningTest()) {
                Integer i = 1/0;
            }
        }catch (Exception e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                        new ApexDebugLog.Error(
                            'SetPlanIDPriceBookEntry',
                            'Batch',
                            recordId,
                            e
                        )
                    );
                }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}