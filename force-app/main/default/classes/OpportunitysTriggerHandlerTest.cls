@isTest
public class OpportunitysTriggerHandlerTest {
    public static testMethod void OpportunitysTriggerTest(){
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        ContactTriggerFlag.isContactBatchRunning=true;
        contact c = new contact();
        c.LastName = 'test';
        c.Email = 'test@gmail.com';
        insert c;
        List<Opportunity> oppList = new List<Opportunity>(); 
        List<Opportunity> oppList1 = new List<Opportunity>(); 
        Opportunity opp = new Opportunity(Name='LB-BURN Bonus',CloseDate=system.today().addDays(5), 
                                          StageName='Closed Won',Scheduled_Payment_Date__c = System.today().addDays(2),
                                          AccountId=acc.Id,Contact__c=c.Id, Payment_Date__c=system.today(),
                                         Core_Amount__c=15, Amount=20,Initiated_From__c = 'Stripe',Stripe_Charge_Id__c='gdsgsh',
                                          Current_Week__c=3,Opportunities_Time_Difference__c=true,Refunded_Date__c=system.today(),
                                          WC_Order_Number__c = '1760(3)',WC_Product_Id__c = '506',Refund_Amount__c = 5
                                         );
        oppList.add(opp);
        Opportunity opp2 = new Opportunity(Name='LB-BURN Bonus',CloseDate=system.today().addDays(5), 
                                          StageName='Closed Won',Scheduled_Payment_Date__c = System.today().addDays(2),
                                          AccountId=acc.Id,Contact__c=c.Id, Payment_Date__c=system.today(),
                                         Core_Amount__c=15, Amount=20,Initiated_From__c = 'Stripe',Stripe_Charge_Id__c='gdsgsh',
                                          Opportunities_Time_Difference__c=true,Refunded_Date__c=system.today(),
                                          WC_Order_Number__c = '1760(3)',WC_Product_Id__c = '506',Refund_Amount__c = 3
                                         );
        
        //Current_Week__c
        oppList.add(opp2);
        
        insert oppList;
        Opportunity opp3 = new Opportunity(Name='LB-BURN Bonus',CloseDate=system.today().addDays(5), 
                                          StageName='Closed Won',Scheduled_Payment_Date__c = System.today().addDays(2),
                                          AccountId=acc.Id,Contact__c=c.Id, Payment_Date__c=system.today(),
                                         Core_Amount__c=15, Amount=20,Initiated_From__c = 'Stripe',Stripe_Charge_Id__c='gdsgsh',
                                          Current_Week__c=3,Opportunities_Time_Difference__c=true,Refunded_Date__c=system.today(),
                                          WC_Order_Number__c = '1760(3)',WC_Product_Id__c = '506',Refund_Amount__c = 1,id = opp2.id
                                         );
        oppList1.add(opp3);
        oppList1.add(opp);
        Triggers_activation__c activation = new Triggers_activation__c();
        activation.SuccessPaymentWithCaseLink__c = true;
        activation.CampaignMemberdeleteHelper__c = true;
        activation.isCampaignAssociate__c = true;
        activation.Opportunitytriggerhandler__c = true;
        insert activation;
        Case ca1 = new Case();
        ca1.Status = 'New';
        ca1.type='Account Recovery';
        ca1.IsFailed_Payment__c = true;
        ca1.UpdateCardStatus__c = 'not Successful';
        ca1.ContactId = c.Id; 
        ca1.Stripe_Customer_Id__c = 'wn';
        ca1.Successful_Payment_DateTime__c = system.today();                                                   
        insert ca1;
        
        Case ca = new Case();
        ca.Status = 'New';
        ca.IsFailed_Payment__c = true;
        ca.UpdateCardStatus__c = 'Payment Successful';
        ca.ContactId = c.Id;       
        ca.Successful_Payment_DateTime__c = system.today();                                                   
        insert ca;
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(ca.Id, yesterday);
        
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.order_id__c = '1760(3)';
        pap.data5__c = '1760(3)';
        pap.product_id__c = '506';
        pap.commission__c = 2;
        Additional_Opportunity_Names__c addnalOpp = new Additional_Opportunity_Names__c();
        addnalOpp.Opportunity_Name__c='LB-BURN Bonus';
        addnalOpp.Name='addopp';
        insert addnalOpp;
        pap.Opportunity__c= opp.Id;
        insert pap;
        
        
        test.startTest();
        OpportunitysTriggerHandler.stripeChargeCreation(oppList);
        OpportunitysTriggerHandler.OpportunityWeekHandler(oppList);
        OpportunitysTriggerHandler.OpportunityTimeDiffHandler(oppList,new Map<Id,Opportunity>(oppList));
        OpportunitysTriggerHandler.rollupAfterUpdate(oppList,new Map<Id,Opportunity>(oppList));
        
        OpportunitysTriggerHandler.NMIStripeBeforeInsert(oppList);
        OpportunitysTriggerHandler.updatePapCommision2(oppList);
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>(oppList);
        opp2.Payment_Date__c = System.today()+1;
        update opp2;
        OpportunitysTriggerHandler.NMIStripeBeforeUpdate(oppList,oppMap);
        OpportunitysTriggerHandler.SuccessPaymentWithCaseLink(oppList);
        OpportunitysTriggerHandler.firstSellCheckHandler(oppList);
        OpportunitysTriggerHandler.rollupInsertUndelete(oppList);
        OpportunitysTriggerHandler.rollupDelete(oppList);
        OpportunitysTriggerHandler.updatePapCommision(oppList);
    //    system.assertequals(10,oppList1[0]);
        
        OpportunitysTriggerHandler.refundPapCommission(oppList,new Map<Id,Opportunity>(oppList1));
        test.stopTest();
    }
    
}