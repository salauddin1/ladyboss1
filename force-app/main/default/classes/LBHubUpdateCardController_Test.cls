@isTest
public class LBHubUpdateCardController_Test {
    	
    static testMethod void testMethod1() {
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
    insert acc;
    contact con = new contact();
    con.lastName = 'Tes';
    con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
    insert con;
     
    card__c cd = new card__c();
    //cd.Name='Test';
    cd.Expiry_Month__c='03';
    cd.Expiry_Year__c='2029';
    cd.Card_Type__c = 'tok_visa';
    cd.contact__c = con.id;
    cd.Credit_Card_Number__c = '564';
    cd.Name_On_Card__c = 'dfsfsdf';
    cd.Billing_Street__c = 'CA';
    cd.Billing_City__c = 'dfg';
    cd.Billing_State_Province__c= 'CA' ;
    cd.Billing_Zip_Postal_Code__c = '2738474';
    cd.Billing_Country__c = 'india';
    cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
    cd.isUpdated__c = true;
    insert cd;
      
    Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',AccountId=acc.Id,card__c=cd.Id);
    insert opp;
    
    Product2 pro = new Product2(Name='Test',Type__c='NMI',isActive=true);
    insert pro;
    
    Product2 pro2 = new Product2(Name='Test',Type__c='Stripe',isActive=true);
    insert pro2;
    
    PricebookEntry pbe = new PricebookEntry(Product2Id=pro.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
    insert pbe;
    
    PricebookEntry pbe2 = new PricebookEntry(Product2Id=pro2.Id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=20,isActive=true);
    insert pbe2;
    
    OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
    insert oli;
    
    OpportunityLineItem oli2 = new OpportunityLineItem(OpportunityId=opp.Id,PricebookEntryId=pbe.Id,Quantity=1,UnitPrice=20);
    insert oli2;
        
    Address__c addr = new Address__c();
        addr.Shipping_City__c = 'dsddsds';
        addr.Shipping_Country__c = 'rere';
        addr.Shipping_State_Province__c='sddsds';
        addr.Shipping_Street__c='sdsdsd';
        addr.Shipping_Zip_Postal_Code__c='sdssd';
        addr.Primary__c = true;
        addr.Contact__c = con.id;
   
        insert addr;
            
    test.startTest();
      LBHubUpdateCardcontroller lc = new LBHubUpdateCardcontroller();
      LBHubUpdateCardcontroller.payPastDueInvoice(oli.id);
 //   lc.MyActionMethod();
 //   lc.updateCustomerCard(cd.id, cd.id, 'dfddf','dffhbj', 'dfbhj', 'djbdb ',12 , 2019, 'test', 'tok_1DVcQJFzCf73siP0aE2hzq0S', 'cgdbcb', false);
      lc.checkSelectedValue();
     
      LBHubUpdateCardcontroller.updateCard1();
      LBHubUpdateCardcontroller.insertAdress('AddressString', con.id);
      LBHubUpdateCardcontroller.getExpireDateValues();
      LBHubUpdateCardcontroller.updateShippingAddress(cd.id, 'cfdcdcd', 'dsddsds', 'rere', 'sddsds', 'sdssd');
      
    test.stopTest();
    }

}