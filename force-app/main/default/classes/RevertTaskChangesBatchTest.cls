@isTest
public class RevertTaskChangesBatchTest {
	@isTest
    public static void test1(){
        Contact ct = new Contact();
        ct.lastName = 'test';
        ct.Email = 'tirth@gmail.com';
        insert ct;
        Account acc = new Account();
        acc.name = 'test';
        insert acc;
        Opportunity op = new Opportunity();
        op.Name = 'test';
        op.StageName  = 'closed won';
        op.CloseDate = system.today();
        op.Amount = 100;
        insert op;
        Task t = new Task();
        t.Account__c = acc.id;
        t.Contact__c = ct.id;
        t.Opportunity__c = op.id;
        insert t;
        RevertTaskChangesBatch bt = new RevertTaskChangesBatch();
        Database.executeBatch(bt);
    }
    @isTest
    public static void test2(){
        Contact ct = new Contact();
        ct.lastName = 'test';
        ct.Email = 'tirth@gmail.com';
        insert ct;
        
        Opportunity op = new Opportunity();
        op.Name = 'test';
        op.StageName  = 'closed won';
        op.CloseDate = system.today();
        op.Amount = 100;
        insert op;
        Task t = new Task();
        t.Contact__c = ct.id;
        t.Opportunity__c = op.id;
        insert t;
        RevertTaskChangesBatch bt = new RevertTaskChangesBatch();
        Database.executeBatch(bt);
    }
}