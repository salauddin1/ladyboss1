public with sharing class UpdatePlanData {
    public List<PlanUpgradeWrapper>  lstplanwrapper {get;set;}
    public List<Plan_Upgrade__c> selectedplnupgr {get;set;}
    public UpdatePlanData(){
        lstplanwrapper = new List<PlanUpgradeWrapper>();
        insertplandata();
    }
    public void insertplandata(){
      lstplanwrapper = new List<PlanUpgradeWrapper>();  
      List<Plan_Upgrade__c> lstplnupdate = [select id,Name,enabled__c,New_Plan_ID__c,Old_Plan_ID__c from Plan_Upgrade__c where enabled__c = true];
        for(Plan_Upgrade__c plnupgrad :lstplnupdate){
            lstplanwrapper.add(new PlanUpgradeWrapper(plnupgrad));
        }  
    }
    public void selectedrecord(){
       	selectedplnupgr = new List<Plan_Upgrade__c>();
        selectedplnupgr.clear();
        for(PlanUpgradeWrapper wraplstplanwrapper : lstplanwrapper)
        {
            if(wraplstplanwrapper.selected == true)
            {
                selectedplnupgr.add(wraplstplanwrapper.plan);
                //updatedplan(selectedplnupgr);
            }
        }
        List<String> lstoldplan = new List<String>();
        for(Plan_Upgrade__c oldplnid :selectedplnupgr){
            lstoldplan.add(oldplnid.Old_Plan_ID__c);
        } 
        if (lstoldplan.size()>0) {
            UpgradePlanBatch upb = new UpgradePlanBatch(lstoldplan);
            Id batchJobId = Database.executeBatch(upb,5);
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Batch Started with id '+batchJobId));
        }else {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Choose atleast one'));
        }
    }
    public void updatedplan(List<Plan_Upgrade__c> pln){
        String recordId = '';
        try{
            List<String> lstoldplan = new List<String>();
            Map<String, Object> results = new Map<String, Object>();
            Map<String, String> subPlanMap = new Map<String, String>();
            Map<String, String> SubLineItemMap = new Map<String, String>();
            Map<String, String> oldNewplanMap = new Map<String, String>();
            for(Plan_Upgrade__c oldplnid :pln){
                lstoldplan.add(oldplnid.Old_Plan_ID__c);
            }
            List<OpportunityLineItem> lstopp = [Select Id,Subscription_Id__c,Plan_ID__c,Product_Plan_Id__c,Status__c  from OpportunityLineItem where Product_Plan_Id__c IN: lstoldplan and Status__c !='InActive'];
              for (OpportunityLineItem oli : lstopp) {
                HttpRequest http = new HttpRequest();
                recordId = oli.Subscription_id__c;
                http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli.Subscription_Id__c);
                http.setMethod('GET');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                String response;
                Integer statusCode;
                if (!Test.isRunningTest()) {
                    hs = con.send(http);
                    response = hs.getBody();
                    statusCode = hs.getStatusCode();
                }else{
               		response ='{ "id": "sub_GICoQyUDJiI9Xp", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1575383446, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "collection_method": "charge_automatically", "created": 1575383446, "current_period_end": 1578061846, "current_period_start": 1575383446, "customer": "cus_GI81AUeX701Z1a", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [ { "id": "txr_1Flbt3FzCf73siP0K7gNGmtm", "object": "tax_rate", "active": true, "created": 1575381477, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": false, "metadata": {}, "percentage": 5.125 } ], "discount": null, "ended_at": null, "invoice_customer_balance_settings": { "consume_applied_balance_on_void": true }, "items": { "object": "list", "data": [ { "id": "si_GICoWELp5G8GzQ", "object": "subscription_item", "billing_thresholds": null, "created": 1575383448, "metadata": {}, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 11726, "amount_decimal": "11726", "billing_scheme": "per_unit", "created": 1571064931, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "LB-LEAN-3 Bags", "product": "prod_FzTuegoLBd817a", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_GICoQyUDJiI9Xp", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_GICoQyUDJiI9Xp" }, "latest_invoice": "in_1FlcOpFzCf73siP0Zw3tNnLA", "livemode": false, "metadata": { "Shipping Address": "PO Box 249 Albuquerque NM US 87103", "Full Name": "Test V11", "Email": "test@shiping.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss--tirthbox.cs22.my.salesforce.com/0031700000wUsW3AAK", "Integration Initiated From": "Salesforce", "Street": "PO Box 249", "City": "Albuquerque", "State": "NM", "Postal Code": "87103", "Country": "US" }, "next_pending_invoice_item_invoice": null, "pending_invoice_item_interval": null, "pending_setup_intent": null, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 11726, "amount_decimal": "11726", "billing_scheme": "per_unit", "created": 1571064931, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "LB-LEAN-3 Bags", "product": "prod_FzTuegoLBd817a", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1575383446, "start_date": 1575383446, "status": "active", "tax_percent": 5.125, "trial_end": null, "trial_start": null }';
                    statusCode = 200;
                }
                if (statusCode == 200) {
                    results = (Map<String, Object>)JSON.deserializeUntyped(response);                    
                    Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('items');
                    List<Object> data = (List<Object>)lstCustomers.get('data');
                    Map<String, Object> obmap = (Map<String, Object>)data[0];
                    Map<String, Object> planmap =  (Map<String, Object>)results.get('plan');
                    subPlanMap.put(oli.Subscription_Id__c, String.valueOf(planmap.get('id')));
                    SubLineItemMap.put(oli.Subscription_Id__c, String.valueOf(obmap.get('id')));
                }
            }
            if (subPlanMap.size()>0) {
                for (Plan_Upgrade__c pu : [SELECT id,New_Plan_ID__c,Old_Plan_ID__c FROM Plan_Upgrade__c WHERE Old_Plan_ID__c IN:subPlanMap.values() AND enabled__c = true]) {
                    oldNewplanMap.put(pu.Old_Plan_ID__c, pu.New_Plan_ID__c);
                }
            }
            if (oldNewplanMap.size()>0) {
                for (OpportunityLineItem oli : lstopp) {
                    if (SubLineItemMap.containsKey(oli.Subscription_Id__c) && oldNewplanMap.containsKey(subPlanMap.get(oli.Subscription_Id__c))) {
                        HttpRequest http1 = new HttpRequest();
                        http1.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli.Subscription_Id__c);
                        http1.setMethod('POST');
                        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                        http1.setHeader('Authorization', authorizationHeader);
                        http1.setHeader('content-type', 'application/x-www-form-urlencoded');
                        http1.setBody('items[0][id]='+SubLineItemMap.get(oli.Subscription_Id__c)+'&items[0][plan]='+oldNewplanMap.get(subPlanMap.get(oli.Subscription_Id__c))+'&prorate=false');
                        Http con1 = new Http();
                        HttpResponse hs1 = new HttpResponse();
                        if (!Test.isRunningTest()) {
                            hs1 = con1.send(http1);
                        }
                        if (hs1.getStatusCode()==200) {
                          oli.Plan_Upgraded__c = true;
                        }
                    }
                }
            }
            update lstopp;
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
        }catch (Exception e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                        new ApexDebugLog.Error(
                            'UpgradePlanData',
                            'Batch',
                            recordId,
                            e
                        )
                    );
                }
    }
    public  class PlanUpgradeWrapper {
        public Plan_Upgrade__c plan {get;set;}
        public boolean selected {get;set;}
        public PlanUpgradeWrapper(Plan_Upgrade__c p){
            plan = p;
            selected =false;
        }
	}
}