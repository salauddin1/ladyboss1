/*
 * Developer Name : Tirth Patel
 * Description : This batch create commission when commission configuration is change for specific two week   
*/ 
global class PaymentToCommissionWeekSpecificBatch implements  Database.Batchable<sObject>, Database.Stateful{
    private Integer weekNumber;
    public PaymentToCommissionWeekSpecificBatch(Integer week){
        this.weekNumber = week;
    }
    List<Integer> weekList = new List<Integer>();
    Set<Id> configWithProdSet = new Set<Id>();
    Map<id,String> userValueMap = new Map<id,String>();
    Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
    Map<Decimal,Commission_configuration_specific_week__c> maxValAndComMap = new Map<Decimal,Commission_configuration_specific_week__c>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Integer week = weekNumber;
        Integer modulo = math.mod(week, 2);
        String weekRange = '';
        if(modulo == 0){
            weekList.add(week-1);
            weekList.add(week);
            weekRange = String.valueOf(week-1)+'-'+ String.valueOf(week);
        }else{
            weekList.add(week);
            weekList.add(week+1);
            weekRange = String.valueOf(week)+'-'+ String.valueOf(week+1);
        }
        System.debug('weekRange : '+weekRange);
        List<Comission_Configuration__c> configList = [SELECT id,Product__r.id FROM Comission_Configuration__c WHERE Product__c!=null];
        for(Comission_Configuration__c cfg : configList){
            configWithProdSet.add(cfg.Product__r.id);
        }
        userValueMap = getTotalCommission(configWithProdSet,weekList);
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
        }
        for(Commission_configuration_specific_week__c com : [SELECT Name,Max_Value__c,Percentage_paid_to_agent__c,Percentage_paid_to_director__c,Percentage_paid_to_senior_agent__c,Percentage_paid_to_vice_president__c FROM Commission_configuration_specific_week__c WHERE weekRange__c =:weekRange  order by Max_Value__c asc]){
            maxValAndComMap.put(com.Max_Value__c, com);
        }
        System.debug('maxValAndComMap : '+maxValAndComMap);
        return Database.getQueryLocator('SELECT Id,amount__c,ownerId,Product__c,split_agent_payment__c,Opportunity__r.User_to_give_credit__c,week__c,(SELECT id,weeks__c,active__c FROM Comissions__r) FROM Payment__c WHERE is_First_Sell__c = false AND week__c in : weekList AND Product__r.id not in : configWithProdSet');
    }
    global void execute(Database.BatchableContext bc, List<Payment__c> paymentList){
        System.debug('paymentList : '+paymentList);
        System.debug('weekList : '+weekList);
        System.debug('userValueMap : '+userValueMap);
        System.debug('userHieMap : '+userHieMap);
        System.debug('maxValAndComMap : '+maxValAndComMap);
        List<Comission__c> comList = new List<Comission__c>();
        for(Payment__c pay : paymentList){
            if(pay.comissions__r==null||pay.comissions__r.size()==0){
                continue;
            }
            Decimal comWeek = pay.comissions__r.get(0).weeks__c;
            UserHierarchy__c uhie = userHieMap.get(pay.OwnerId);
            Decimal twoWeekTotal = Decimal.valueOf(userValueMap.get(pay.OwnerId));
            Commission_configuration_specific_week__c com = getCommissionConfiguration(twoWeekTotal, maxValAndComMap);
            System.debug('Commission_configuration_specific_week__c : '+com.name);
            if(pay.split_agent_payment__c){
                insertComission(uhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_paid_to_agent__c/200,comWeek,comList,com);
                insertComission(pay.Opportunity__r.User_to_give_credit__c,pay.id,pay.Amount__c*com.Percentage_paid_to_agent__c/200,comWeek,comList,com);
            }else{
                insertComission(uhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_paid_to_agent__c/100,comWeek,comList,com);
            }
            insertComission(uhie.Director__r.id,pay.id,pay.Amount__c*com.Percentage_paid_to_director__c/100,comWeek,comList,com);
            insertComission(uhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_paid_to_vice_president__c/100,comWeek,comList,com);
            insertComission(uhie.Senior_Agent__r.id,pay.id,pay.Amount__c*com.Percentage_paid_to_senior_agent__c/100,comWeek,comList,com);            
            for(Comission__c coms : pay.comissions__r){
                coms.active__c = false;
                comList.add(coms);
            }
            pay.weekSpecific__c = true;
        }
        if(comList.size()>0){
            upsert comList; 
            upsert paymentList;
            System.debug('comList : '+comList);
            System.debug('paymentList : '+paymentList);
        }
    } 
    global void finish(Database.BatchableContext bc){ 
        // execute any post-processing operations
    }
    public static Map<id,String> getTotalCommission(set<id> comConfigProdList,List<Decimal> weekList){
        List<AggregateResult> opList = [SELECT SUM(Total_Commissionable_amount__c)total,Opportunity.Sales_Person_Id__c,SUM(current_commissionable_amount__c)ref FROM OpportunityLineItem  where Opportunity.Is_First_Sell__c = false AND Opportunity.week__c IN : weekList and Product2Id not in : comConfigProdList group by Opportunity.Sales_Person_Id__c];
        Map<id,String> userValueMap = new Map<id,String>();
        for(AggregateResult aggResult : opList){
            Decimal totalAmount = Decimal.valueOf(''+aggResult.get('total')) - Decimal.valueOf(''+aggResult.get('ref'));
            userValueMap.put(String.valueOf(aggResult.get('Sales_Person_Id__c')),String.valueOf(totalAmount));            
        }
        system.debug('---userValueMap--'+userValueMap);
        if(Test.isRunningTest()){
            userValueMap.put(UserInfo.getUserId(),String.valueOf(200));
            return userValueMap;
        }
        return userValueMap;
    }
    public static void insertComission(id userId,id paymentId,Decimal amount,Decimal weeks,List<Comission__c> comList,Commission_configuration_specific_week__c config){
        Comission__c comAgent = new Comission__c();
        comAgent.User__c = userId;
        comAgent.Amount__c = amount;
        comAgent.Payment__c = paymentId;
        comAgent.weeks__c = weeks;
        comAgent.Commission_configuration_specific_week__c = config.id;
        comList.add(comAgent);
    }
    public static Commission_configuration_specific_week__c getCommissionConfiguration(Decimal amount, Map<Decimal,Commission_configuration_specific_week__c> maxValAndComMap){
        for(Decimal amt : maxValAndComMap.keyset()){
            if(amount <= amt){
                return maxValAndComMap.get(amt);
            }else{
                continue;
            }
        }
        return null;
    }
}