@isTest
global class ManageCouponHttpCalloutMock3 implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{ "code": "10off", "discount_type": "percent", "amount": "10", "individual_use": true, "exclude_sale_items": true, "minimum_amount": "100.00" }');
        response.setStatusCode(200);
        return response; 
    }
}