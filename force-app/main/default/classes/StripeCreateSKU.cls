global class StripeCreateSKU implements Queueable {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/skus';
    
    global String stripeCurrency;
    global String id;
    public String stripeObject;  //list
    public cls_data[] StripeData;
    public boolean has_more;
    public String url; 
   // global Map<String, Map<String,String>> data ;
    private Product2 prod;
    public StripeCreateSKU(Product2 pr){
        this.prod = pr;
    }
    public void execute(QueueableContext context) {
                
        StripeCreateSKU.createsku('usd', 'infinite',prod.Price__c,prod.Stripe_Product_Id__c,prod.IsActive,prod.Id,prod.Length__c,prod.Height__c,prod.Width__c,prod.Weight__c);
        

    }
    
    
    @future(callout=true)
    global static void createsku(String stripeCurrency, String inventoryType, Decimal prices, String product_id, Boolean active, String sfID, Decimal length,Decimal height,Decimal width, Decimal weight){
        
        Integer price= (prices*100).intValue();
        String s;
        if(length!=null&&height!=null&&width!=null&&weight!=null){
            s='currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&product='+product_id+'&active='+active+'&attributes[sfid]='+sfID+'&package_dimensions[height]='+height+'&package_dimensions[weight]='+weight+'&package_dimensions[length]='+length+'&package_dimensions[width]='+width;
        }else{
            s='currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&product='+product_id+'&active='+active+'&attributes[sfid]='+sfID;
        }
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setBody(s);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    //return null;
                }
        }
        else {
            //hs.setBody(StripeChargeTests.testData_createCharge);
            hs.setStatusCode(200);
        }
         system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
         try {
            StripeCreateSKU o = StripeCreateSKU.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            //return o.id;
            if(o.Id != null && o.Id != ''){
                Product2 pr = [SELECT id,Stripe_Product_Id__c,Stripe_SKU_Id__c,Tax_Code__c FROM Product2 WHERE id =: sfID ];
                pr.Stripe_SKU_Id__c=o.id;
                pr.Stripe_Product_Id__c = product_id;
                if(pr.Tax_Code__c == null){
                    pr.Tax_Code__c = 'NT';
                }
                update pr;
            }
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            //return null;
        }
    }
   global static String createskunotfuture(String stripeCurrency, String inventoryType, Decimal prices, String product_id, Boolean active, String sfID){
        Integer price= (prices*100).intValue();
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setBody('currency='+stripeCurrency+'&inventory[type]='+inventoryType+'&price='+price+'&product='+product_id+'&active='+active+'&attributes[sfid]='+sfID);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }
        else {
            //hs.setBody(StripeChargeTests.testData_createCharge);
            hs.setStatusCode(200);
        }
         system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
         try {
            StripeCreateSKU o = StripeCreateSKU.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            return o.id;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    global static String getSKU(String product_id,  String sfID){
       String endpint ='https://api.stripe.com/v1/skus?limit=1';
        HttpRequest http1 = new HttpRequest();
        http1.setEndpoint(endpint+'&product='+product_id+'&attributes[sfid]='+sfID);
        http1.setMethod('GET');
        http1.setHeader('content-type', 'application/x-www-form-urlencoded');
        system.debug('productid-->'+product_id);
        system.debug('sfID-->'+sfID);
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http1.setHeader('Authorization', authorizationHeader);
        //http1.setBody('product='+product_id+'&attributes[sfid]='+sfID);
        String response1;
        Integer statusCode1;
        Http con1 = new Http();
        HttpResponse hs1 = new HttpResponse();
        if(!Test.isRunningTest()){   
            try {
                    hs1 = con1.send(http1);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }
        else {
            //hs.setBody(StripeChargeTests.testData_createCharge);
            hs1.setStatusCode(200);
        }
         system.debug('#### '+ hs1.getBody());
        
        response1 = hs1.getBody();
        statusCode1 = hs1.getStatusCode();
        system.debug('$$statusCode = '+hs1.getStatusCode());
         try {
            StripeCreateSKU p = StripeCreateSKU.parse(response1);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+p); 
            return p.StripeData[0].id;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    public static StripeCreateSKU parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        system.debug('++--'+System.JSON.deserialize(json, StripeCreateSKU.class));
        return (StripeCreateSKU) System.JSON.deserialize(json, StripeCreateSKU.class);

    }
      //wrapper classes
    class cls_data {
        public String id;   //sku_EbGVE6Ua17erOq
        public String stripeObject;   //sku
        public boolean active;
        public cls_attributes attributes;
        public Integer created; //1551179986
        //usd
        public cls_image image;
        public cls_inventory inventory;
        public boolean livemode;
        public cls_metadata metadata;
        public cls_package_dimensions package_dimensions;
        public Integer price;   //102
        public String product;  //prod_EYEU6cnpTDFMnC
        public Integer updated; //1551179986
    }
    class cls_attributes {
        public String sfid; //3
    }
    class cls_image {
    }
    class cls_inventory {
        public cls_quantity quantity;
        public String StripeType; //infinite
        public cls_value value;
    }
    class cls_quantity {
    }
    class cls_value {
    }
    class cls_metadata {
    }
    class cls_package_dimensions {
    }
}