public class StripeListCustomer {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class Customers {
		//public String object_Z {get;set;} // in json: object
		public List<Data> data {get;set;} 
		public Boolean has_more {get;set;} 
		public String url {get;set;} 

		public Customers(JSONParser parser) {
            String text1;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					text1 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						/*if (text1 == 'object') {
							object_Z = parser.getText();
						} else */
                        if (text1 == 'data') {
                            data = new List<Data>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                data.add(new Data(parser));
                            }
						} else if (text1 == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text1 == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text1);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	/*public class Metadata {

		public Metadata(JSONParser parser) {
            String text2;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					text2 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Metadata consuming unrecognized property: '+text2);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}*/
     public class Metadata {
		public String name {get;set;}
		public String first_name {get;set;} 
		public String last_name {get;set;}
        public String phone {get;set;}

		public Metadata(JSONParser parser) {
            String text4;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					 text4 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text4 == 'name') {
							name = parser.getText();
						} else if (text4 == 'first_name') {
							first_name = parser.getText();
						} else if (text4 == 'last_name') {
							last_name = parser.getText();
						} else if (text4 == 'phone') {
							phone = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Metadata consuming unrecognized property: '+text4);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public String id {get;set;} 
		public String object_data {get;set;} // in json: object
		public Integer account_balance {get;set;} 
		public Integer created {get;set;} 
		public String currencyx {get;set;} 
		public String default_source {get;set;} 
		public Boolean delinquent {get;set;} 
		public String description {get;set;} 
		public Decimal discount {get;set;} 
		public String email {get;set;} 
		public Boolean livemode {get;set;} 
		public Metadata metadata {get;set;} 
		public CustAddress shipping {get;set;} 
		public Sources sources {get;set;} 
		//public Sources subscriptions {get;set;} 

		public Data(JSONParser parser) {
            String text3;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					text3 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text3 == 'id') {
							id = parser.getText();
						} else if (text3 == 'object_data') {
							object_data = parser.getText();
						} else if (text3 == 'account_balance') {
							account_balance = parser.getIntegerValue();
						} else if (text3 == 'created') {
							created = parser.getIntegerValue();
						} else if (text3 == 'currency') {
							currencyx = parser.getText();
						} else if (text3 == 'default_source') {
							default_source = parser.getText();
						} else if (text3 == 'delinquent') {
							delinquent = parser.getBooleanValue();
						} else if (text3 == 'description') {
							description = parser.getText();
						} else if (text3 == 'discount') {
							discount = parser.getDoubleValue();
						} else if (text3 == 'email') {
							email = parser.getText();
						} else if (text3 == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text3 == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text3 == 'shipping') {
							shipping = new CustAddress(parser);
						} else if (text3 == 'sources') {
							sources = new Sources(parser);
						}/* else if (text3 == 'subscriptions') {
							subscriptions = new Sources(parser);
						} */
                        else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text3);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class Address {
		public String city {get;set;}
		public String country {get;set;} 
		public String line1 {get;set;}
        public String line2 {get;set;}
        public String postal_code {get;set;}
        public String state {get;set;}

		public Address(JSONParser parser) {
            String text4;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					 text4 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text4 == 'city') {
							city = parser.getText();
						} else if (text4 == 'country') {
							country = parser.getText();
						} else if (text4 == 'line1') {
							line1 = parser.getText();
						} else if (text4 == 'line2') {
							line2 = parser.getText();
						} else if (text4 == 'postal_code') {
							postal_code = parser.getText();
						} else if (text4 == 'state') {
							state = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'CustAddress consuming unrecognized property: '+text4);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class CustAddress {
		public Address address {get;set;} // in json: object
		public String name {get;set;} 
		public String phone {get;set;} 

		public CustAddress(JSONParser parser) {
            String text5;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					 text5 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text5 == 'address') {
							address = new Address(parser);
						} else if (text5 == 'name') {
							name = parser.getText();
						} else if (text5 == 'phone') {
							phone = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'CustAddress consuming unrecognized property: '+text5);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class Sources {
		public String object_Z {get;set;} // in json: object
		public List<DataSource> data {get;set;} 
		public Boolean has_more {get;set;} 
		public Integer total_count {get;set;} 
		public String url {get;set;} 

		public Sources(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'data') {
							data = new List<DataSource>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data.add(new DataSource(parser));
							}
						} else if (text == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text == 'total_count') {
							total_count = parser.getIntegerValue();
						} else if (text == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Sources consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class DataSource {
		public String id {get;set;} 
		public String object_Z {get;set;} // in json: object
		public String address_city {get;set;} 
		public String address_country {get;set;} 
		public String address_line1 {get;set;} 
		public String address_line1_check {get;set;} 
		public String address_line2 {get;set;} 
		public String address_state {get;set;} 
		public String address_zip {get;set;} 
		public String address_zip_check {get;set;} 
		public String brand {get;set;} 
		public String country {get;set;} 
		public String customer {get;set;} 
		public String cvc_check {get;set;} 
		public String dynamic_last4 {get;set;} 
		public Integer exp_month {get;set;} 
		public Integer exp_year {get;set;} 
		public String fingerprint {get;set;} 
		public String funding {get;set;} 
		public String last4 {get;set;} 
		//public Metadata_Z metadata {get;set;} 
		public String name {get;set;} 
		public String tokenization_method {get;set;} 

		public DataSource(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'address_city') {
							address_city = parser.getText();
						} else if (text == 'address_country') {
							address_country = parser.getText();
						} else if (text == 'address_line1') {
							address_line1 = parser.getText();
						} else if (text == 'address_line1_check') {
							address_line1_check = parser.getText();
						} else if (text == 'address_line2') {
							address_line2 = parser.getText();
						} else if (text == 'address_state') {
							address_state = parser.getText();
						} else if (text == 'address_zip') {
							address_zip = parser.getText();
						} else if (text == 'address_zip_check') {
							address_zip_check = parser.getText();
						} else if (text == 'brand') {
							brand = parser.getText();
						} else if (text == 'country') {
							country = parser.getText();
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'cvc_check') {
							cvc_check = parser.getText();
						} else if (text == 'dynamic_last4') {
							dynamic_last4 = parser.getText();
						} else if (text == 'exp_month') {
							exp_month = parser.getIntegerValue();
						} else if (text == 'exp_year') {
							exp_year = parser.getIntegerValue();
						} else if (text == 'fingerprint') {
							fingerprint = parser.getText();
						} else if (text == 'funding') {
							funding = parser.getText();
						} else if (text == 'last4') {
							last4 = parser.getText();
						} else if (text == 'metadata') {
							//metadata = new Metadata_Z(parser);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'tokenization_method') {
							tokenization_method = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
	/*
	public class Sources {
		//public String object_Z {get;set;} // in json: object
		//public List<Metadata> data_source {get;set;} 
		public Boolean has_more {get;set;} 
		public Integer total_count {get;set;} 
		public String url {get;set;} 

		public Sources(JSONParser parser) {
            String text6;
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					text6 = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text6 == 'object') {
							object_Z = parser.getText();
						} else 
                        if (text6 == 'data') {
                            data_source = new List<Metadata>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data_source.add(new Metadata(parser));
							}
						} else 
                            if (text6 == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text6 == 'total_count') {
							total_count = parser.getIntegerValue();
						} else if (text6 == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Sources consuming unrecognized property: '+text6);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	*/
	
	public static Customers parse(String json) {
		return new Customers(System.JSON.createParser(json));
	}
}