@isTest
public class CampaignMemberTriggerExtra_Test{
	
	public static testmethod void createLead(){
    	Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        insert cnt;
        
        Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'TrialBuy 5-6 Day Upload TEXTING';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
    	
    }
}