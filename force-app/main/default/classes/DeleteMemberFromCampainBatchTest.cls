@isTest
public class DeleteMemberFromCampainBatchTest{ 
    @isTest 
    public static void test1(){
        Campaign_Name_List__c cm1 = new Campaign_Name_List__c();
        cm1.Name = 'gfgdf';
        cm1.CamapignNames__c = 'dfdgfd';
        insert cm1;
        
        Contact con1 = new Contact();
        con1.LastName = 'test lName';
        con1.Product__c = 'UTA';
        con1.Website_Products__c = 'Unlimited';  
        con1.First_Website_Sell__c  = true;
        con1.is_process_using_batch__c = false;
        insert con1;
        
        Campaign c = new Campaign();
        c.Name = 'LVChBuy-4-6 Days';
        c.Type = 'Phone Team';
        insert c;
        
        Campaign c2 = new Campaign();
        c2.Name = 'LVChBuy-4-8 Days';
        c2.Type = 'Phone Team';
        insert c2;
        
        CampaignMember cm = new CampaignMember();
        cm.ContactId = con1.id;
        cm.CampaignId = c.id;
        insert cm;
        
        opportunity op  = new opportunity();
        op.name='Unlimited';
        op.Contact__c =con1.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        Stripe_Product_Mapping__c sp = new Stripe_Product_Mapping__c();
        sp.Stripe__c = 'Unlimited';
        sp.Salesforce__c='Unlimited';
        sp.Name = 'Stripe Site Guest User';
        insert sp;
        
        Workflow_Configuration__c dc = new Workflow_Configuration__c();
        dc.Target_Campaign__c = c2.id;
        dc.Consider_For_CM_Movement__c = true;
        dc.Source_Campaign__c = c.id;
        dc.Products__c = 'unlimited';
        dc.Active__c = true;
        insert dc;
        
        test.startTest();
        DeleteMemberFromCampainBatch d = new DeleteMemberFromCampainBatch();
        Database.executeBatch(d);
        
        System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>(),new List<CampaignMember>(),new List<CampaignMember>()));
        System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>(),new List<CampaignMember>()));
        
        test.stopTest();

        
    }
    @isTest
    public static void myUnitTestMethod(){
        DeleteMemberFromCampainBatch sch = new DeleteMemberFromCampainBatch();
        String sch1 = '0 0 23 * * ?'; 
        system.schedule('Test schedule class', sch1, sch);
        
    }
    
    
}