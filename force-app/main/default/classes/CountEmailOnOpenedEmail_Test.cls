@istest
public class CountEmailOnOpenedEmail_Test {
    public static testMethod void test(){
       
          contact co = new Contact();
           co.email='Grant+newcontactcreatedincf@ladyboss.com';
           co.Opened_Mail_Count__c= 1;
           co.LastName='sdf';
           insert co;
      
        
        Custom_Tracker__c  c = new Custom_Tracker__c  ();
        c.UniqueId__c ='123';
        insert c;
      
        test.startTest();
         PageReference myVfPage = Page.CountEmailOnOpenedEmail;
        Test.setCurrentPage(myVfPage);
        CountEmailOnOpenedEmail cd1 =new CountEmailOnOpenedEmail();
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id',co.id);
         ApexPages.currentPage().getParameters().put('key','123');
         
        CountEmailOnOpenedEmail.updateCount();
        test.stopTest();
    }
}