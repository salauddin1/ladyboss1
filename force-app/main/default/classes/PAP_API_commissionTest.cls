@isTest
public class PAP_API_commissionTest {
      
    @isTest static void testcreateEvent() {   
        RestRequest req = new RestRequest(); 
        req.params.put('actionname', 'testaction');
        req.params.put('type', 'Sales');
        req.params.put('status', 'approved');
        req.params.put('com', '122');
         req.params.put('ordid', 'ordid');
         req.params.put('prodId', 'prodId');
         req.params.put('campaignId', 'campaignId');
         req.params.put('campaignName', 'campaignName');
         req.params.put('refid', 'refid');
        req.params.put('data1','data1');
        req.params.put('data2','data2');
        req.params.put('data4','data4');
        req.params.put('data5','testing');
        req.params.put('totalcost','123');
        RestResponse res = new RestResponse();      
        RestContext.request = req;
        RestContext.response= res;
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/pap_commission';
        
        contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'refid';
        ct.Is_Master__c = true;
        insert ct;
       
        Test.startTest(); 
         PAP_API_commission.createEvent();
        Test.stopTest();
    }
    @isTest static void testcreateEvent1() {   
        RestRequest req = new RestRequest(); 
        req.params.put('actionname', 'testaction');
        req.params.put('type', 'Sales');
        req.params.put('status', 'approved');
        req.params.put('com', '122');
        req.params.put('ordid', 'ordid');
         req.params.put('prodId', 'prodId');
         req.params.put('campaignId', 'campaignId');
         req.params.put('campaignName', 'campaignName');
         req.params.put('refid', 'refid');
        req.params.put('data1','data1');
        req.params.put('data2','data2');
        req.params.put('data4','data4');
        req.params.put('data5','testing');
        req.params.put('totalcost','123');
        RestResponse res = new RestResponse();      
        RestContext.request = req;
        RestContext.response= res;
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/pap_commission';
        
        contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'refid';
        ct.Is_Master__c = true;
        insert ct;
        
        Opportunity op = new Opportunity();
        op.Coupon_Code__c = 'test';
        op.wc_order_id__c = 'ordid';
        op.WC_Product_Id__c = 'prodId';
        op.Name = 'testing';
        op.WC_Order_Number__c = 'testing';
        op.StageName = 'Qualification';
        op.CloseDate = date.today();
        insert op;
        
        Pap_Commission__c pc = new Pap_Commission__c();
        pc.affiliate_ref_id__c = 'refid';
        pc.order_id__c = 'ordid';
        pc.product_id__c = 'prodId';
        pc.data1__c = 'data1';
        pc.data2__c = 'data2';
        pc.data4__c = 'data4';
        pc.Commission_Type__c = 'initial';
        pc.Total_Cost__c = 123;
        insert pc;
        Pap_Commission__c pc2 = new Pap_Commission__c();
        pc2.affiliate_ref_id__c = 'refid';
        pc2.order_id__c = 'ordid';
        pc2.product_id__c = 'prodId';
        pc2.data1__c = 'data1';
        pc2.data2__c = 'data2';
        pc2.data4__c = 'data4';
        pc2.Commission_Type__c = 'refund';
		pc2.Total_Cost__c = 123;
        insert pc2;
       
        Test.startTest(); 
        PAP_API_commission.createEvent();
        Test.stopTest();
    }
    @isTest static void testcreateEvent2() {   
        RestRequest req = new RestRequest(); 
        req.params.put('actionname', 'testaction');
        req.params.put('type', 'Sales');
        req.params.put('status', 'approved');
        req.params.put('com', '122');
        req.params.put('ordid', 'ordid');
         req.params.put('prodId', 'prodId');
         req.params.put('campaignId', 'campaignId');
         req.params.put('campaignName', 'campaignName');
         req.params.put('refid', 'refid');
        req.params.put('data1','data1');
        req.params.put('data2','data2');
        req.params.put('data4','data4');
        req.params.put('data5','initial');
        req.params.put('totalcost','123');
        RestResponse res = new RestResponse();      
        RestContext.request = req;
        RestContext.response= res;
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/pap_commission';
        
        contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAP_refid__c = 'refid';
        ct.Is_Master__c = true;
        insert ct;
        
        Opportunity op = new Opportunity();
        op.Coupon_Code__c = 'test';
        op.wc_order_id__c = 'ordid';
        op.WC_Product_Id__c = 'prodId';
        op.Name = 'testing';
        op.StageName = 'Qualification';
        op.CloseDate = date.today();
        op.Contact__c = ct.id;
        insert op;
        
        Pap_Commission__c pc = new Pap_Commission__c();
        pc.affiliate_ref_id__c = 'refid';
        pc.order_id__c = 'ordid';
        pc.product_id__c = 'prodId';
        pc.data1__c = 'data1';
        pc.data2__c = 'data2';
        pc.data4__c = 'data4';
        pc.Commission_Type__c = 'initial';
        pc.Total_Cost__c = 123;
        pc.Contact__c = ct.id;
        insert pc;
        Pap_Commission__c pc2 = new Pap_Commission__c();
        pc2.affiliate_ref_id__c = 'refid';
        pc2.order_id__c = 'ordid';
        pc2.product_id__c = 'prodId';
        pc2.data1__c = 'data1';
        pc2.data2__c = 'data2';
        pc2.data4__c = 'data4';
        pc2.Commission_Type__c = 'refund';
		pc2.Total_Cost__c = 123;
        pc2.Contact__c = ct.id;
        insert pc2;
       
        Test.startTest(); 
        PAP_API_commission.createEvent();
        Test.stopTest();
    }
}