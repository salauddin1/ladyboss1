global class DeleteMemberFromCampainBatch implements Database.Batchable<SObject>,Database.AllowsCallouts, Database.Stateful ,Schedulable {
    global List<CampaignMember> removelistofcmList;
    
    global DeleteMemberFromCampainBatch(){
        
        removelistofcmList = new List<CampaignMember> ();
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<Workflow_Configuration__c> oldcamplist = [SELECT Products__c,Source_Campaign__c,Target_Campaign__c FROM Workflow_Configuration__c where Products__c != null and Source_Campaign__c != null and Consider_For_CM_Movement__c = true];
        set<String> oldcampaignset = new set<String>();
        for(Workflow_Configuration__c ncm : oldcamplist){
            oldcampaignset.add(ncm.Source_Campaign__c);
        }
        // should we move campaign type too at custom setting level ?
        if(oldcampaignset.size() > 0){
            //use this if you want to consider only first sell contacts//return Database.getQueryLocator([select id,Website_Products__c,Product__c,is_process_using_batch__c from contact where First_Website_Sell__c = true and is_process_using_batch__c = false  and Id in (select contactId from CampaignMember where campaign.Type='Phone Team' and campaignid in : oldcampaignset) order by createddate desc]);
            return Database.getQueryLocator([select id,Website_Products__c,Product__c,is_process_using_batch__c from contact where Id in (select contactId from CampaignMember where campaign.Type='Phone Team' and campaignid in : oldcampaignset) order by createddate desc]);
        }else{
            return null;
        }
        
    }
    global void execute(SchedulableContext ctx) {
        
        DeleteMemberFromCampainBatch DCbatch = new DeleteMemberFromCampainBatch();
        database.executebatch(DCbatch);
        // String sch = '0 0 19 * * ?';
        // System.schedule('Hourly', sch , new DeleteMemberFromCampainBatch() );
    }   
    
    global void execute(Database.BatchableContext BC, List<Contact> listofcontact) {
        if(listofcontact.size() > 0){
            Set<ID> lstcntid = new Set<ID>();
            Map<String,String> stripeProductMappingMap = new Map<String,String>();
            
            List<Stripe_Product_Mapping__c> stripeProductMappings = [SELECT Stripe__c, Salesforce__c  FROM Stripe_Product_Mapping__c ];
            for(Stripe_Product_Mapping__c stripeProductMapping : stripeProductMappings){
                stripeProductMappingMap.put(stripeProductMapping.Stripe__c,stripeProductMapping.Salesforce__c);
            }
            
            Map<Id,Set<String>> contactCategoryMap = new Map<Id,Set<String>>();
            Map<Id,List<String>> contactCategoryMapwithList = new Map<Id,List<String>>();
            for(Contact sobj : listofcontact){
                if(sobj.Website_Products__c!=null && sobj.Website_Products__c!=''){
                    String[] allWebSiteProduct = sobj.Website_Products__c.split(';');
                    if(!contactCategoryMap.containsKey(sobj.Id)){
                        Set<String> categories = new Set<String>();
                        List<String> categoriesList = new List<String>();
                        contactCategoryMap.put(sobj.Id,categories);
                    }
                    for(String websiteProduct : allWebSiteProduct){
                        contactCategoryMap.get(sobj.Id).add(websiteProduct);
                    }
                }
            }
            
            User stripeGuestUser = [select Id from User where name='Stripe Site Guest User' LIMIT 1];
            
            // Any Reason of not checking opportunity which is only created by stripe guest user ?
            //List<Opportunity> opportunities = [select Id,name,Contact__c from Opportunity where Contact__c in :listofcontact and CreatedById=:stripeGuestUser.Id];
            List<Opportunity> opportunities = [select Id,name,Contact__c from Opportunity where Contact__c in :listofcontact order by createddate asc];
            
            
            // Opportunity name should be matching with Stripe__c so we can use Salesforce__c to find the category of that purchase from site
            for(Opportunity opp:opportunities){
                if(stripeProductMappingMap.containsKey(opp.name)){
                    if(!contactCategoryMap.containsKey(opp.Contact__c)){
                        Set<String> categories = new Set<String>();
                        contactCategoryMap.put(opp.Contact__c,categories);
                    }
                    contactCategoryMap.get(opp.Contact__c).add(stripeProductMappingMap.get(opp.name));
                    if(!contactCategoryMapwithList.containsKey(opp.Contact__c)){
                        List<String> categoriesList = new List<String>();
                        contactCategoryMapwithList.put(opp.Contact__c,categoriesList);
                    }
                    contactCategoryMapwithList.get(opp.Contact__c).add(stripeProductMappingMap.get(opp.name));
                }
            }
            
            List<Contact> contactToUpdateWithSiteCategory = new List<Contact>();
            for(Id contactId : contactCategoryMap.keySet()){
                Set<String> categories = contactCategoryMap.get(contactId);
                String category = '';
                for(String ctg: categories){
                    if(category==''){
                        category = category + ctg;    
                    }else{
                        category = category + ';' +ctg;    
                    }
                }
                contactToUpdateWithSiteCategory.add(new Contact(Id=contactId,Website_Products__c=category));
                
            }
            
            if(contactToUpdateWithSiteCategory.size() > 0){
                update contactToUpdateWithSiteCategory;
            }
            
            //first we need to check if contact has that category, if not we need to add them on contact and update contact.
            List<Workflow_Configuration__c> newcamplist = [SELECT Products__c,Source_Campaign__c,Target_Campaign__c FROM Workflow_Configuration__c where Products__c != null and Source_Campaign__c != null and Consider_For_CM_Movement__c = true];
            Map<String,List<Workflow_Configuration__c>> mapofreplacecamp = new Map<String,List<Workflow_Configuration__c>>();
            set<String> newreplacecampaignset = new set<String>();
            set<String> oldcampaignset = new set<String>();
            set<String> productypeset = new set<String>();
            for(Workflow_Configuration__c ncm : newcamplist){
                oldcampaignset.add(ncm.Source_Campaign__c);
                if(mapofreplacecamp.containskey(ncm.Source_Campaign__c)){
                    mapofreplacecamp.get(ncm.Source_Campaign__c).add(ncm);
                }else{
                    mapofreplacecamp.put(ncm.Source_Campaign__c,new List<Workflow_Configuration__c>());
                    mapofreplacecamp.get(ncm.Source_Campaign__c).add(ncm);
                }
                
                newreplacecampaignset.add(ncm.Target_Campaign__c);
                productypeset.add(ncm.Products__c.tolowercase());
            }
            set<Id> unlimitedcon = new set<Id>();
            listofcontact = [select id,Website_Products__c,Product__c,is_process_using_batch__c from contact where Id in : listofcontact];
            for(Contact sobj : listofcontact){
                for(String ptype : productypeset){
                    if(sobj.Website_Products__c !=null && (sobj.Website_Products__c.tolowercase().contains(ptype.tolowercase()))){
                        lstcntid.add(sobj.Id);
                    }
                }
                
            }
            
            if(oldcampaignset.size() > 0 && lstcntid.size() > 0){
                List<CampaignMember> campList = [select id,contactId,campaign.name,campaign.Type,campaign.Check_Product_Index__c,contact.Website_Products__c from CampaignMember where contactId in:lstcntid and campaign.Type='Phone Team' and campaignid in : oldcampaignset ];
                system.debug('-----newreplacecampaignset---00'+newreplacecampaignset);
                if(newreplacecampaignset.size() > 0){
                    //List<Campaign> campaignList = [select id,name,(select contactid from campaignmembers where contact.First_Website_Sell__c = true) from campaign where id in : newreplacecampaignset];
                    //List<Campaign> campaignList = [select id,name,Check_Product_Index__c,(select contactid from campaignmembers where contactid in : lstcntid) from campaign where id in : newreplacecampaignset];
                    List<CampaignMember> insertlistofcm = new List<CampaignMember>();
                    Set<CampaignMember> removelistofcm = new Set<CampaignMember>();
                    Map<id,Set<String>> campidsetMap = new Map<id,Set<String>>();
                    Map<id,Campaign> idcampmap = new Map<id,Campaign>();
                    for(Campaign camp : [select id,name,Check_Product_Index__c,(select contactid from campaignmembers where contactid in : lstcntid) from campaign where id in : newreplacecampaignset]){
                        idcampmap.put(camp.id,camp);
                        for(CampaignMember cmember : camp.campaignmembers){
                            if(cmember.ContactId != null){
                                if(campidsetMap.containskey(camp.Id)){
                                    campidsetMap.get(camp.id).add(cmember.ContactId);
                                }else{
                                    campidsetMap.put(camp.id,new set<String>());
                                    campidsetMap.get(camp.id).add(cmember.ContactId);
                                }
                            }
                        }
                        
                    }
                    
                    //Below for-loop will move contacts to differenct campaigns using custom setting configuration 
                    system.debug('-----campList---00'+campList);
                    for(CampaignMember cmrec : campList){
                        system.debug('-----here---00');
                        if(cmrec.contact.Website_Products__c !=null && mapofreplacecamp.containsKey(cmrec.campaignid) && mapofreplacecamp.get(cmrec.campaignid) != null ){
                            system.debug('-----here---0');
                            for(Workflow_Configuration__c dc : mapofreplacecamp.get(cmrec.campaignid)){
                                system.debug('-----here---1');
                                if(cmrec.contact.Website_Products__c.tolowercase().contains(dc.Products__c.tolowercase())){
                                    system.debug('-----contactCategoryMapwithList---'+contactCategoryMapwithList);
                                    system.debug('-----dc.Target_Campaign__c---2'+dc.Target_Campaign__c);
                                    system.debug('-----idcampmap---'+idcampmap);
                                    //system.debug('-----here---'+campidsetMap.get(dc.Target_Campaign__c)+'----'+campidsetMap.get(dc.Target_Campaign__c).contains(cmrec.ContactId));
                                    if(!(campidsetMap.containsKey(dc.Target_Campaign__c) && campidsetMap.get(dc.Target_Campaign__c) != null && campidsetMap.get(dc.Target_Campaign__c).contains(cmrec.ContactId))  && !(idcampmap.containskey(dc.Target_Campaign__c) && idcampmap.get(dc.Target_Campaign__c) != null &&  idcampmap.get(dc.Target_Campaign__c).Check_Product_Index__c == true && contactCategoryMapwithList.get(cmrec.ContactId) != null && contactCategoryMapwithList.get(cmrec.ContactId).contains('LIVE Challenge') && contactCategoryMapwithList.get(cmrec.ContactId).contains('Unlimited') && contactCategoryMapwithList.get(cmrec.ContactId).indexof('LIVE Challenge') < contactCategoryMapwithList.get(cmrec.ContactId).indexof('Unlimited'))){
                                        if(dc.Target_Campaign__c != null && cmrec.ContactId != null){
                                            CampaignMember cm= New CampaignMember(CampaignId=dc.Target_Campaign__c, ContactId= cmrec.ContactId);
                                            insertlistofcm.add(cm); 
                                        }
                                        
                                    }
                                    if(!(idcampmap.containskey(dc.Target_Campaign__c) && idcampmap.get(dc.Target_Campaign__c) != null &&  idcampmap.get(dc.Target_Campaign__c).Check_Product_Index__c == true && contactCategoryMapwithList.get(cmrec.ContactId) != null && contactCategoryMapwithList.get(cmrec.ContactId).contains('LIVE Challenge') && contactCategoryMapwithList.get(cmrec.ContactId).contains('Unlimited') && contactCategoryMapwithList.get(cmrec.ContactId).indexof('LIVE Challenge') < contactCategoryMapwithList.get(cmrec.ContactId).indexof('Unlimited'))){
                                        removelistofcm.add(cmrec);
                                    }
                                    
                                }
                            }
                        }
                    }
                    if(insertlistofcm.size() > 0){
                        insert insertlistofcm;
                    }
                    if(removelistofcm.size() > 0){
                        //List<CampaignMember> removelistofcmList = new List<CampaignMember>();
                        removelistofcmList.addall(removelistofcm);
                        //enqueue this as when we delete campaignmembers from batch , it calls a future method in five9 which can fail as we can't call future method from batch.
                        
                    }
                }
            }
            for(Contact contct : listofcontact){
                contct.is_process_using_batch__c = true;  
            }
            update listofcontact;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.enqueueJob(new CampaignMemberBatchDelQueue(new List<CampaignMember>(removelistofcmList),new List<CampaignMember>(),new List<CampaignMember>()));
    }
}