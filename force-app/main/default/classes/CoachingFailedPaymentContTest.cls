@IsTest
public class CoachingFailedPaymentContTest {
    static testMethod void test() {
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        insert tsk;
        Case cs = new Case();
         cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
       
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        CoachingFailedPaymentCont.getCaseRecord(cs.id);
        CoachingFailedPaymentCont.saveCase(cs, 'true', 'Facebook_Message__c');
        Test.stopTest();
    }
    static testMethod void test1() {
        Case cs = new Case();
         cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        CoachingFailedPaymentCont.getCaseRecord(cs.id);
        CoachingFailedPaymentCont.saveCase(cs, 'true', 'Recovered_Payment__c');
        Test.stopTest();
    }
    static testMethod void test2() {
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Case cs = new Case();
        
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = ct.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        CoachingFailedPaymentCont.getCaseRecord(cs.id);
        CoachingFailedPaymentCont.saveCase(cs, 'true', 'Stop_Correspondence__c');
        Test.stopTest();
    }
    static testMethod void test3() {
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Case cs = new Case();
         cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = ct.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        
        insert cs;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfCase';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        CoachingFailedPaymentCont.getCaseRecord(cs.id);
        CoachingFailedPaymentCont.saveCase(cs, 'true', 'Saved_Payment__c');
        Test.stopTest();
    }
}