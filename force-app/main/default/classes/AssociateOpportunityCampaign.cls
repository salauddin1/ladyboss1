//Sukesh 
//Fix for the campaign name associating with opportunity for scheduled payments
public class AssociateOpportunityCampaign {
    public static void updateCampaignUpdate (List<opportunity> oppList) {
    try{
    Set<Id> conSetId = new Set<Id>();
    Set<Id> oppSetId = new Set<Id>();
    //loop over new created opportunities , scheduled payment today and 
    
    for(opportunity opp : oppList) {
        
        if(opp.Contact__c!=null && opp.Scheduled_Payment_Date__c==Date.Today() &&  opp.Campaign__c==null) {
            oppSetId.add(opp.id);
            conSetId.add(opp.Contact__c);
        }
    }


        List<opportunity> oppContList = [select id,createddate,Contact__c , Scheduled_Payment_Date__c, Campaign__c  from opportunity where  ID NOT IN :oppSetId  and Contact__c IN : conSetId and SystemModstamp > :Datetime.now().addMinutes(-10)];
        Map <String, List<opportunity>> oppConMap = new Map <String, List<opportunity>> ();
        for(opportunity op : oppContList) {
            if(op.Campaign__c!=null){

                if(oppConMap.containsKey(op.Contact__c)  ){
                    List<opportunity> oppTempList = oppConMap.get(op.Contact__c);
                    List<opportunity> oppPutList = oppTempList;
                    oppPutList.add(op);
                    oppConMap.put(op.Contact__c,oppPutList);

                }else {
                    List<opportunity> oppPutList = new  List<opportunity> ();
                    oppPutList.add(op);
                    oppConMap.put(op.Contact__c,oppPutList);

                }
            }
            
        }
   List<opportunity> oppListSOQL = [select id,createddate,Contact__c , Campaign__c, Scheduled_Payment_Date__c  from opportunity where  ID IN :oppSetId  and Contact__c IN : conSetId order by createddate desc];
List<Opportunity> oppUpdateList = new List<Opportunity> ();
        for(opportunity opp : oppListSOQL) {
            if(opp.Contact__c!=null && opp.Scheduled_Payment_Date__c==Date.Today() &&  opp.Campaign__c==null) {
                if(oppConMap.containsKey(opp.Contact__c)){
                    List<opportunity> oppLists = oppConMap.get(opp.Contact__c);
                    for(opportunity op : oppLists) {
                        if(op.Campaign__c!=null) {
                           opp.Campaign__c= op.Campaign__c;
                        }
                    }
                    oppUpdateList.add(opp);
                }
                 
            }
           
        }
        if(oppUpdateList.size() >0) {
        update oppUpdateList;
        }
        }catch(Exception ex) {
        
        ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('AssociateOpportunityCampaign','Trigger handler','',ex));
        }

    }
    
}