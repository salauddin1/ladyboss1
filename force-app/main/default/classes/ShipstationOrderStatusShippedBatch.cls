global class ShipstationOrderStatusShippedBatch implements Database.Batchable<Integer>,Database.AllowsCallouts,Database.Stateful {
    global Static Boolean flag = false;
    global Integer pagenumber=0;
    Integer count =0;
    
    global Iterable<Integer> start(Database.batchableContext info){
        return new ShipStationUpdateIterable();
    } 
    global void execute(database.batchablecontext bd, list<Integer> scope){
        String header;
        String FaieldOrderId;
        pagenumber = scope[0];
        System.debug('================pagenumber=================='+pagenumber);
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }   
        Date StartDate = Date.today();
        String StartDattime = String.valueOf(StartDate.addDays(-1))+'T00:00:00';
        System.debug('===StartDattime=='+StartDattime);
        String EndDate =String.valueOf(DateTime.now());
        EndDate = EndDate.replace(' ', 'T');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?modifyDateStart='+StartDattime+'&modifyDateEnd='+EndDate+'&orderStatus=shipped&pageSize=1&page='+pagenumber);
        req.setMethod('GET');
        req.setHeader('Authorization', 'Basic '+header);
        req.setTimeout(3000);
        req.setHeader('Accept', 'application/json');
        String ResponseBody;
        try{
            HttpResponse res = new Http().send(req);
            Integer statusCode = res.getStatusCode();
            if(statusCode == 200){
            ResponseBody = res.getBody();
            }
            System.debug('=============ResponseBody=========='+ResponseBody);
        }
        catch(Exception e){}
        if(ResponseBody != null) {
            Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
            List<Object> OrderList = (List<Object>)ordResult.get('orders');
            Map<String, Object> OrderMap = new Map<String, Object> ();
            Map<String, String> OrderIdMap = new Map<String, String> ();
            Set<String> orderids = new Set<String>();
            for(Object obj : OrderList){
                OrderMap = (Map<String, Object>)obj;
                orderids.add(String.valueOf(OrderMap.get('orderId')));
                OrderIdMap.put(String.valueOf(OrderMap.get('orderKey')),String.valueOf(OrderMap.get('orderId')));
            }
            list<ShipStation_Orders__c> query = new list<ShipStation_Orders__c>();
            query = [select orderId__c, orderKey__c,orderStatus__c from ShipStation_Orders__c where orderId__c In: orderids];
            
            if(query != null && !OrderIdMap.isEmpty()){
                for(ShipStation_Orders__c ord : query){
                    if(OrderIdMap.get(ord.orderKey__c) == ord.orderId__c){
                        System.debug('===================='+ String.valueOf(OrderMap.get('orderStatus')));
                        ord.orderStatus__c = String.valueOf(OrderMap.get('orderStatus'));
                    }
                }
                ShipstationOrderStatusShippedBatch.flag = true;
                update query;
            }
            flag = true;
            
        }
    }
    Public void finish(database.batchableContext bc){ 
       Integer i =0;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
}