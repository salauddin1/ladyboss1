public class AddressTriggerHandler{
    public static void primaryAddress(List<Address__c> lstAdd){
        set<id> conIdSet =new set<id>();
       set<id> addIdSet =new set<id>();
       
        for(Address__c ad : lstAdd){
            if(ad.Primary__c ==true){
            conIdSet.add(ad.Contact__c);
            addIdSet.add(ad.id);
            }
        }
        
      system.debug('conIdSet----'+conIdSet);
      system.debug('--addIdSet--'+addIdSet);
         List<Address__c > listAllAddress =[select id,Contact__c,Primary__c from Address__c where Contact__c != null and Contact__c IN : conIdSet];
          List<Address__c > upd= new List<Address__c > ();
         for(Address__c  ad : listAllAddress ){
          system.debug('conIdSet----'+ad.id);
             if(!addIdSet.contains(ad.id)){
             system.debug('conIdSet----'+ad.id);
                 ad.Primary__c =false;
                 upd.add(ad);
             }
         }
         update upd ;
    }

}