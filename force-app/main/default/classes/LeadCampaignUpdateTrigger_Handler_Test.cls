@isTest
private class LeadCampaignUpdateTrigger_Handler_Test {
    public testmethod Static void doAfterInsertTest(){
        List<Lead> leadlist = new List<Lead>();
        Lead ld1 = new Lead();
        ld1.Status = 'New';
        ld1.LastName = 'test ld1Name';
        ld1.Company = 'testing company';
        ld1.Products__c = 'UTA';
        ld1.Estimated_Size_of_List_Following__c = '';
        ld1.Platform_With_Most_Influence__c = '';
        ld1.How_did_you_hear_about_us__c = '';
        ld1.Reason_You_Are_Applying_As_A_Partner__c = '';
        ld1.Please_select_what_most_closely_describe__c = '';
        insert ld1;
        leadlist.add(ld1);
        Test.startTest();
        LeadCampaignUpdateTrigger_Handler.doAfterInsert(new List<Lead>{ld1});
        UpdatePicklistValueFromWufoo.populateAllPicklist(leadlist);
        Test.stopTest();
    }
    
    public testmethod Static void doBeforeUpdateTest(){
        Lead ld1 = new Lead();
        ld1.Status = 'New';
        ld1.LastName = 'test ld2Name';
        ld1.Company = 'testing company';
        ld1.Products__c = 'UTA';
        insert ld1;
        Map<Id,Lead> leadMap = new Map<Id,Lead>();
        leadMap.put(ld1.Id, ld1);
        
        ld1.Products__c = 'Book';
        update ld1;
        Test.startTest();
        LeadCampaignUpdateTrigger_Handler.doBeforeUpdate(new List<Lead>{ld1}, leadMap);
        Test.stopTest();
    }
    
    public testmethod Static void updateCampaignMembersTest(){
        Lead ld1 = new Lead();
        ld1.Status = 'New';
        ld1.LastName = 'test ld3Name';
        ld1.Company = 'testing company';
        ld1.Products__c = 'UTA';
        insert ld1;
        Map<Id,Lead> leadMap = new Map<Id,Lead>();
        leadMap.put(ld1.Id, ld1);
        
        Campaign sc = new Campaign();
        sc.Name = 'source campaign Lead Days 2-5';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
        insert dc;
        
        CampaignMember cm = new CampaignMember();
        cm.LeadId = ld1.Id;
        cm.CampaignId = sc.Id;
        insert cm;
        
        Workflow_Configuration__c wc = new Workflow_Configuration__c();
        wc.Source_Campaign__c = sc.Id;
        wc.Products__c = 'UTA';
        wc.Target_Campaign__c = dc.Id;
        insert wc;
        
        Test.startTest();
        LeadCampaignUpdateTrigger_Handler.updateCampaignMembers(leadMap);
        Test.stopTest();
    }
     public testmethod Static void addPotentialBuyersTest(){
        Lead ld1 = new Lead();
        ld1.Status = 'New';
        ld1.LastName = 'test ld3Name';
        ld1.Company = 'testing company';
       // ld1.Potential_Buyer__c = 'UTALead-1 Day USA/CAN';
        insert ld1;
        Map<Id,Lead> leadMap = new Map<Id,Lead>();
        leadMap.put(ld1.Id, ld1);
    
    Campaign sc = new Campaign();
        sc.Name = 'UTALead-1 Day USA/CAN';
        insert sc;
        
        Campaign dc = new Campaign();
        dc.Name = 'destination campaign Buy Day 1';
        insert dc;
        
        CampaignMember cm = new CampaignMember();
        cm.LeadId = ld1.Id;
        cm.CampaignId = dc.Id;
        insert cm;
        
        Test.startTest();
        LeadCampaignUpdateTrigger_Handler.addPotentialBuyers(leadMap);
        Test.stopTest();
         
     }
}