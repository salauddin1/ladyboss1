@RestResource(urlMapping='/ShopifyOrderCancelation')
global class ShopifyCustomer_Service {
    @HttpPost
    global static void orderCancel() {
     if(RestContext.request.requestBody!=null  ){
            try {
                String str = RestContext.request.requestBody.toString();
                System.debug('-------'+str);
                Shopify_JSON2Apex objShopify_JSON2Apex = new Shopify_JSON2Apex(System.JSON.createParser(str));
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                Map<String, Object> customerMap = new Map<String, Object>();
                customerMap =  (Map<String, Object>)results.get('customer');
                String customerEmail = '';
                customerEmail = String.valueOf(results.get('email')); 
                List<Contact> conList = [select id from Contact where email =:customerEmail ];
                set<id> conIdSet = new set<id>();
                for(Contact con : conList ) {
                    conIdSet.add(con.id);
                }
                List<ShopifyLineItem__c> lstLineItem = [SELECT Contact__c, Item_Id__c,Shopify_Orders__r.Order_Id__c, Id FROM ShopifyLineItem__c where Contact__c IN :conIdSet];
                List<Shopify_Orders__c > lstOrders = [SELECT  Id,Financial_Status__c,Order_Id__c,Refunded_Amount__c FROM Shopify_Orders__c where Contact__c IN :conIdSet];
                Map<string,ShopifyLineItem__c> orderLineItemMap = new Map<string,ShopifyLineItem__c> ();
                for (ShopifyLineItem__c o: lstLineItem ) {
                    if(o.Item_Id__c!=null){
                        orderLineItemMap.put(o.Item_Id__c,o);
                      }
                }
                Map<string,Shopify_Orders__c> orderMap = new Map<string,Shopify_Orders__c> ();
                for (Shopify_Orders__c o: lstOrders ) {
                    if(o.Order_Id__c!=null){
                        orderMap.put(o.Order_Id__c,o);
                      }
                }
                List<Shopify_Orders__c > lstOrdersUpdate = new List<Shopify_Orders__c >();
                List<ShopifyLineItem__c> lstOrderLinesUpdate = new List<ShopifyLineItem__c>();
                if(conList.size() > 0 ) {
                     List<Shopify_JSON2Apex.refunds> lstObbj = objShopify_JSON2Apex.Refunds ;
                     for (Shopify_JSON2Apex.refunds ls:  lstObbj  ) {
                          if(orderMap.Containskey(string.valueOf(ls.order_id))) {
                               Shopify_Orders__c order = orderMap.get(string.valueOf(ls.order_id));
                                for (Shopify_JSON2Apex.Transactions dt : ls.transactions) {
                                       order.Financial_Status__c= String.valueOf(objShopify_JSON2Apex.financial_status);
                                       order.Refunded_Amount__c = Decimal.valueOf(dt.amount);
                                       order.Status__c= 'Canceled';
                                       lstOrdersUpdate.add(order);
                                }
                                for(Shopify_JSON2Apex.Refund_line_items  dt : ls.refund_line_items) {
                                   if(orderLineItemMap.containsKey(string.valueOf(dt.line_item_id))) {
                                       ShopifyLineItem__c lineItem = orderLineItemMap.get(string.valueOf(dt.line_item_id));
                                       if(lineItem != null) {
                                               lineItem.Fulfillable_Quantity__c = Integer.valueOf(dt.line_item.fulfillable_quantity);
                                               lineItem.Status__c = 'Canceled';
                                               lineItem.Refunded_Amount__c= Decimal.valueOf(dt.subtotal);
                                               lineItem.Refunded_Tax_Amount__c =Decimal.valueOf(dt.total_tax); 
                                               lstOrderLinesUpdate.add(lineItem);
                                        }
                                    }
                                }
                            }
                        }
                    }
                if(lstOrdersUpdate.size() > 0) {
                    update lstOrdersUpdate ;
                }
                if(lstOrderLinesUpdate.size() > 0) {
                    update lstOrderLinesUpdate;
                }
            }
            catch(Exception e) {}    
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
 }