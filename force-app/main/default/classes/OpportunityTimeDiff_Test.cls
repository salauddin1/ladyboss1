@isTest
public class OpportunityTimeDiff_Test {
    @isTest
    public static void test() {
      
        Contact con = new Contact();
        con.LastName = 'Prachi test';
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'LadyBoss-LVCH-$1-Dig';
        opp.CloseDate = System.today();
        opp.StageName = 'Closed Won';
        opp.Contact__c = con.Id;
        insert opp;
        Additional_Opportunity_Names__c app = new Additional_Opportunity_Names__c();
        app.Name = 'LadyBoss LABS-BFLRc';
        app.Opportunity_Name__c = 'LadyBoss LABS-BFLRc';
        insert app;
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'LadyBoss LABS-BFLRc';
        opp2.CloseDate = System.today();
        opp2.StageName = 'Closed Won';
        opp2.Contact__c = con.id;
        insert opp2;
       
    }
}