public class FaceBookMessagesAPI {
	public FaceBookMessagesAPI() {
		
	}

	public static string callAPI(String uri,String httpMethod,List<URLParameters> lstUrlParams)  {
        String endPoint =  'https://graph.facebook.com';   
        endPoint+=uri;
        String finalParamString  = '';
        if(lstUrlParams != null && lstUrlParams.size() > 0)  {
            endPoint += '?';
            lstUrlParams.sort();
            List<String> finalParamArray = new List<String>();
            for(URLParameters up : lstUrlParams)  {
                String parameterValue = up.value;
                String parameterName = up.name;
                if(parameterValue == null || parameterValue == '') {
                    parameterValue = '{' + parameterName + '}';
                }
                finalParamArray.add(parameterName + '=' + parameterValue);
            }
            for(Integer i = 0 ; i < finalParamArray.size(); i++)  {
                if(i != 0)  {
                    finalParamString += '&';
                }	
                finalParamString += finalParamArray.get(i);
            }
            endPoint += finalParamString;
        }

        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        String responseBody;

        req.setMethod(httpMethod);
        req.setEndpoint(endPoint);
        try{
            System.debug(req);
            res = http.send(req);
            responseBody = res.getBody();
            System.debug('responseBody is : '+ responseBody);
            return responseBody;
        }catch(Exception e)  {
            System.debug('Exception is : ' + e.getMessage());
            System.debug(res.getBody());
			Log__c logObj = new Log__c();
			logobj.API_Url__c = uri;
			logobj.Response__c = res.getBody();
			logObj.Log_Type__c = 'Error';
			logobj.Exception_Message__c = 'Exception is :' + e;
			insert logObj;
        }

        return null;

	}

    public static string callAPI(String uri,String httpMethod,List<URLParameters> lstUrlParams,String body)  {
        String endPoint =  'https://graph.facebook.com';   
        endPoint+=uri;
        String finalParamString  = '';
        if(lstUrlParams != null && lstUrlParams.size() > 0)  {
            endPoint += '?';
            lstUrlParams.sort();
            List<String> finalParamArray = new List<String>();
            for(URLParameters up : lstUrlParams)  {
                String parameterValue = up.value;
                String parameterName = up.name;
                if(parameterValue == null || parameterValue == '') {
                    parameterValue = '{' + parameterName + '}';
                }
                finalParamArray.add(parameterName + '=' + parameterValue);
            }
            for(Integer i = 0 ; i < finalParamArray.size(); i++)  {
                if(i != 0)  {
                    finalParamString += '&';
                }	
                finalParamString += finalParamArray.get(i);
            }
            endPoint += finalParamString;
        }

        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','application/json');
        Http http = new Http();
        if(body!=null){
            req.setBody(body);
        }
        String responseBody;

        req.setMethod(httpMethod);
        req.setEndpoint(endPoint);
        try{
            System.debug(req);
            System.debug(req.getBody());
            res = http.send(req);
            responseBody = res.getBody();
            System.debug('responseBody is : '+ responseBody);
            return responseBody;
        }catch(Exception e)  {
            System.debug('Exception is : ' + e.getMessage());
            System.debug(res.getBody());
			Log__c logObj = new Log__c();
			logobj.API_Url__c = uri;
			logobj.Response__c = res.getBody();
			logObj.Log_Type__c = 'Error';
			logobj.Exception_Message__c = 'Exception is :' + e;
			insert logObj;
        }

        return null;

	}

	public Class URLParameters implements Comparable {
        public String name;
        public String value;
        public Integer compareTo(Object compareTo) {
            URLParameters up = (URLParameters)compareTo;
            if(this.name != null && up.name != null)  {
                return this.name.compareTo(up.name);
            }
            return 0;
        }
    }

    public static List<URLParameters> convertToListParam(Map<String,String> paramMap){
        List<URLParameters> paramList = new List<URLParameters>();
        URLParameters up = null;
        for(String key : paramMap.keyset()){
            up = new URLParameters();
            up.name = key;
            up.value = paramMap.get(key);
            paramList.add(up);
        }
        return paramList;
    }
}