@isTest
global class CustomerMockCLass implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('content-type', 'application/json');
        res.setBody('{"id": "cus_EouLWFQ92EMWEV","object": "customer","account_balance": 0,"created": 1554326497,"currency": null}');
        res.setStatusCode(200);
        return res;   
    }
}