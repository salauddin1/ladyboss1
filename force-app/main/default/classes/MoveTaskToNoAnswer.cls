//Batch class to  check the  "No Answer" checkbox as "TRUE."  if  on day 4 after the due date if any of the checkboxes not checked. 
global class MoveTaskToNoAnswer implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful ,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Query string to get the tasks
        String dynamicquery = 'select id,Closed_Sale__c,Do_Not_Call__c,No_Answer__c,Contact__c,Not_interested__c,Not_Time_Yet__c,Number_of_Due_days__c,Reordered_Online__c     FROM task  where WhoId!=null and Number_of_Due_days__c=4 and ActivityDate!=null AND (Closed_Sale__c!=true and Do_Not_Call__c !=true and Not_interested__c !=true  and Reordered_Online__c!=true and No_Answer__c!=true) order by createddate desc limit 100000';
        return Database.getQueryLocator(dynamicquery);
    }
    
    global void execute(SchedulableContext ctx) {
        //Runs the batch
        MoveTaskToNoAnswer sdbatch = new MoveTaskToNoAnswer();
        database.executebatch(sdbatch,200);
        
       
    }   
    //This batch will execute the records that is updates the task to no answer
    global void execute(Database.BatchableContext bc, List<Task> records){
         System.debug('11111');// AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfTask' );
        for(Task cs : records){
            cs.No_Answer__c =true;
        }
        update records;
    }    
    
    global void finish(Database.BatchableContext bc){
        
    }    
}