@isTest
public class TransferAccountRecovery_Test {

   public static testMethod void test(){
        
        Case cas = new Case(Status ='New',IsFailed_Payment__c =true, Priority = 'Medium', Origin = 'Email', Reason='new');
        insert cas;
        
        Test.startTest();

        TransferAccountRecovery.getCase(cas.id);
        TransferAccountRecovery.transferOwnership(cas.id);
        Test.stopTest();
    }
}