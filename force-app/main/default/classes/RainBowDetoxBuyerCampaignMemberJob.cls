// Developer Name :- Surendra Patidar
// class contains code about add contact to the RainBow Detox fulfillment from shopify campaign 
// ===============================================================================================
public class RainBowDetoxBuyerCampaignMemberJob {
    public static void addToRainbowCampaign(List<Contact> con) {
        system.debug('========'+con);
        try {
            Set<String> conSet = new Set<String> ();
            List<CampaignMember> cmember = new List<CampaignMember>();
            Map<String, CampaignMember> cmMap = new Map<String, CampaignMember>();
            Set<String> conIdMap = new Set<String> ();
            List<CampaignMember> cmrList = new  List<CampaignMember>() ;
            
            // filter all RainBow Detox campaign contact
            if(con != null && !con.isEmpty()){
                for(contact c : con){
                    if(c.Marketing_Cloud_Tags__c != null){
                        List<String> mctagsList = c.Marketing_Cloud_Tags__c.split(';');
                        if(mctagsList.contains('Rainbow Detox Buyer')) {
                            conset.add(c.Id);
                        }
                    }
                }
            }
            // Query from rain bow Campaignmember and filter contact if it is not in Rain Bow campaign
            if(conset != null && !conset.isEmpty()) {
                cmember = [SELECT Id, CampaignId,Campaign.Name, ContactId, Status, Name FROM CampaignMember where ContactId!=null and ContactId IN : conset and Campaign.Name = 'Rainbow Detox Fulfillment from Shopify'];
                if(cmember != null && !cmember.isEmpty()) {
                    for(CampaignMember cmp : cmember) {
                        cmMap.put(cmp.ContactId,cmp);
                    }
                    
                }
                for(String conId : conset) {
                    if(!cmMap.containsKey(conId)) {
                        conIdMap.add(conId);
                    }
                }
            }
            // Query RainBow Detox fulfillment from shopify campaign
            List<campaign> cmp = [select id,name,type from campaign where type = 'Marketing Cloud-Data Triggers' and name = 'Rainbow Detox Fulfillment from Shopify'];
            
            // add campaign member to the RainBow Detox fulfillment from shopify campaign
            if(cmp != null && !cmp.isEmpty()){
                if(conIdMap != null && !conIdMap.isEmpty()) {
                    for(String str : conIdMap) {
                        CampaignMember newCM = new CampaignMember(CampaignId = cmp[0].Id,ContactId = str,status = 'Sent' );
                        cmrList.add(newCM);
                    }
                }
            }
            //Query Contact for update its rainbow detox buyer flag Because we can not update directly record itself from trigger we need to query it
            List<Contact> conlist = [select id from contact where id In : conIdMap FOR UPDATE];
            for (Contact cup : conlist) {
                cup.Rainbow_Detox_Buyer__c = true;
            }
            if(cmrList.size() > 0  ) {
                //Insert the campaign member
                database.insert (cmrList, false);
            }
            if(con.size() > 0) {
                update conlist;
            }
        }
        catch(Exception e){
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('Rainbow Detox Buyer ','On Insert and update','Trigger for campaign', e));
        }
    }
}