@istest
public class StripeSubscriptioinUpCancel_ServiceTest {
    
    public static testMethod void test(){
        
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 300151;
        insert apex;
        
        try{
        test.startTest();
        system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
            
            String body = '{\"id\":\"evt_1DJduFBwLSk1v1ohKNs5iyTP\",\"object\":\"event\",\"api_version\":\"2018-02-06\",\"created\":1539163383,\"data\":{\"object\":{\"id\":\"sub_DlAEjFZvBklMuQ\",\"object\":\"subscription\",\"application_fee_percent\":null,\"billing\":\"charge_automatically\",\"billing_cycle_anchor\":1539163382,\"cancel_at_period_end\":false,\"canceled_at\":null,\"created\":1539163382,\"current_period_end\":1541841782,\"current_period_start\":1539163382,\"customer\":\"cus_Dl9tzT8tKU51LD\",\"days_until_due\":null,\"items\":{\"object\":\"list\",\"data\":[{\"id\":\"si_DlAEvVPQeDSpGu\",\"object\":\"subscription_item\",\"created\":1539163382,\"metadata\":{},\"plan\":{\"id\":\"plan_DDGQokobMChyR9\",\"active\":true,\"aggregate_usage\":null,\"amount\":23700,\"created\":1531344662,\"currency\":\"usd\",\"interval_count\":1,\"livemode\":false,\"metadata\":{},\"nickname\":\"Transformation System-$237\",\"product\":\"prod_DDGQVEaH03NJ8p\"},\"quantity\":1,\"subscription\":\"sub_DlAEjFZvBklMuQ\"}],\"has_more\":false,\"total_count\":1,\"url\":\"\"},\"livemode\":false,\"metadata\":{},\"plan\":{\"id\":\"plan_DDGQokobMChyR9\",\"object\":\"plan\",\"active\":true,\"amount\":23700,\"billing_scheme\":\"per_unit\",\"created\":1531344662,\"currency\":\"usd\",\"interval\":\"month\",\"interval_count\":1,\"livemode\":false,\"metadata\":{},\"nickname\":\"Transformation System-$237\",\"product\":\"prod_DDGQVEaH03NJ8p\"},\"quantity\":1,\"start\":1539163382,\"status\":\"active\"}},\"livemode\":false,\"pending_webhooks\":3,\"request\":{\"id\":\"req_pmF3soEVdJOuKH\",\"idempotency_key\":null},\"type\":\"customer.subscription.created\"}';
            
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            Account Acc =new Account();
            Acc.Name = 'Test';
            Acc.Credit_Card_Number__c = '2435643';
            Acc.Exp_Month__c = '12' ;
            Acc.Exp_Year__c = 2019 ;
            
            insert Acc ;
            Contact Con =new Contact();
            Con.LastName = 'Test Con';
            
            insert Con;
        
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
 
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
            op.AccountId = Acc.id;
            op.Contact__c = Con.Id;
            op.RecordTypeId= RecordTypeIdopportunity ;
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c='evt_1DJguhBwLSk1v1ohwrZpgJIl';
             insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Subscription_Id__c='sub_DlAEjFZvBklMuQ';   
        
        insert ol;
           
  
            Stripe_Profile__c StripePro =new Stripe_Profile__c();
            StripePro.Stripe_Customer_Id__c = 'cus_Dl9tzT8tKU51LD';
            StripePro.Customer__c = Con.id ;
            
            
            insert StripePro ;
            
            System.debug('===========Opportunity========'+op);
        
        	StripeSubscriptioinUpdateCancel_Service.createCustomer();
        test.stopTest();
        
    }
         catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }  
    } /*
    public static testMethod void test(){
        TRY{
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Id RecordTypeIdContact = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
            Account Acc =new Account();
            Acc.Name = 'Test';
            Acc.Credit_Card_Number__c = '2435643';
            Acc.Exp_Month__c = '12' ;
            Acc.Exp_Year__c = 2019 ;
            
            insert Acc ;
            Contact Con =new Contact();
            Con.LastName = 'Test Con';
            
            insert Con;
            
            
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
            op.AccountId = Acc.id;
            op.Contact__c = Con.Id;
            op.RecordTypeId= RecordTypeIdContact ;
            insert op;
            OpportunityLineItem oli=new OpportunityLineItem();
            oli.Subscription_Id__c='test';
            oli.OpportunityId=op.id;
            insert oli;
            
            Stripe_Profile__c StripePro =new Stripe_Profile__c();
            StripePro.Stripe_Customer_Id__c = 'cus_Dl9tzT8tKU51LD';
            StripePro.Customer__c = Con.id ;
            
            
            insert StripePro ;
            
            System.debug('===========Opportunity========'+op);
            /*RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
            
            String body = '{\"id\":\"evt_1DJduFBwLSk1v1ohKNs5iyTP\",\"object\":\"event\",\"api_version\":\"2018-02-06\",\"created\":1539163383,\"data\":{\"object\":{\"id\":\"sub_DlAEjFZvBklMuQ\",\"object\":\"subscription\",\"application_fee_percent\":null,\"billing\":\"charge_automatically\",\"billing_cycle_anchor\":1539163382,\"cancel_at_period_end\":false,\"canceled_at\":null,\"created\":1539163382,\"current_period_end\":1541841782,\"current_period_start\":1539163382,\"customer\":\"cus_Dl9tzT8tKU51LD\",\"days_until_due\":null,\"items\":{\"object\":\"list\",\"data\":[{\"id\":\"si_DlAEvVPQeDSpGu\",\"object\":\"subscription_item\",\"created\":1539163382,\"metadata\":{},\"plan\":{\"id\":\"plan_DDGQokobMChyR9\",\"active\":true,\"aggregate_usage\":null,\"amount\":23700,\"created\":1531344662,\"currency\":\"usd\",\"interval_count\":1,\"livemode\":false,\"metadata\":{},\"nickname\":\"Transformation System-$237\",\"product\":\"prod_DDGQVEaH03NJ8p\"},\"quantity\":1,\"subscription\":\"sub_DlAEjFZvBklMuQ\"}],\"has_more\":false,\"total_count\":1,\"url\":\"\"},\"livemode\":false,\"metadata\":{},\"plan\":{\"id\":\"plan_DDGQokobMChyR9\",\"object\":\"plan\",\"active\":true,\"amount\":23700,\"billing_scheme\":\"per_unit\",\"created\":1531344662,\"currency\":\"usd\",\"interval\":\"month\",\"interval_count\":1,\"livemode\":false,\"metadata\":{},\"nickname\":\"Transformation System-$237\",\"product\":\"prod_DDGQVEaH03NJ8p\"},\"quantity\":1,\"start\":1539163382,\"status\":\"active\"}},\"livemode\":false,\"pending_webhooks\":3,\"request\":{\"id\":\"req_pmF3soEVdJOuKH\",\"idempotency_key\":null},\"type\":\"customer.subscription.created\"}';
            
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            // StripeSubscriptioinUpdateCancel_Service.dummyCover();
            StripeSubscriptioinUpdateCancel_Service.createCustomer();
            test.stopTest();
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }    
    }
    */
    
}