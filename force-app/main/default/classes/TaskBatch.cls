/*
 * Developer Name : Tirth Patel
 * Discription : This batch updates whoid value to contact field and sets whoid as null on Task
 */
global  class TaskBatch implements Database.Batchable<SObject>{
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator([SELECT Id,whoid,Contact__c,who.type FROM Task where  whoid != null and Contact__c = null and who.type ='contact']);
    } 
    global void execute(Database.BatchableContext context, List<SObject> records) {
        for(sObject taskrecord : records ){
            Task tk = (Task)taskrecord;
            tk.Contact__c = tk.WhoId;
            tk.WhoId = null;  
        }
        update records;
    }
    global void finish(Database.BatchableContext context) {
        
    }
}