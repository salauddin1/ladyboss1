@isTest
private class EmailCaseCommentTest {

    static testMethod void myUnitTest() {
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@gmail.com';
        insert con;
        
		//Insert test case record
        Case cs = new case();
        cs.ContactId  = con.Id;
        cs.Status = 'Open';
        cs.Origin = 'Email';
        cs.Is_Site_User_Case__c = true ;
        insert cs;
        
        //Insert emailmessage for case
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = false;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = cs.Id; 
        insert email;

       
    }
}