// Developer name :- Surendra Patidar
// THis class is for population picklist values in lead object with the lead record type is equal to partners which is come from wufoo lead form
public class UpdatePicklistValueFromWufoo {
    public static void populateAllPicklist(List<Lead> leadlist) {
        System.debug('============'+leadlist.size()+'========'+leadlist);
        // Update Picklist values from coming to the wufoo form to salesforce picklist values with same field as text area.
        try {
            Id devRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Partners Lead').getRecordTypeId();
            System.debug('=============='+devRecordTypeId);
            if(leadlist != null && !leadlist.IsEmpty()) {
                for (lead ld : leadlist) {
                    if(ld.Estimated_Size_of_List_Following__c != null && ld.Platform_With_Most_Influence__c != null && ld.How_did_you_hear_about_us__c != null && ld.Reason_You_Are_Applying_As_A_Partner__c != null && ld.Please_select_what_most_closely_describe__c != null) {
                        //initialize the picklist values on before insert into lead Object
                        ld.Estimated_Size_of_Following__c = ld.Estimated_Size_of_List_Following__c;
                        ld.Platform_With_the_Most_Influence__c = ld.Platform_With_Most_Influence__c;
                        ld.How_did_you_hear_about_the_us__c = ld.How_did_you_hear_about_us__c;
                        ld.Please_select_the_reason_you_are_applyin__c = ld.Reason_You_Are_Applying_As_A_Partner__c;
                        ld.select_what_most_closely_describe__c = ld.Please_select_what_most_closely_describe__c;
                        ld.RecordTypeId = devRecordTypeId;
                    }
                }
            }
        }
        catch (Exception e){}
    }
}