@RestResource(urlMapping='/StripePlanCreate_Service')
global class StripePlanCreate_Service {
    @HttpPost
    global static void updateplan(){
        String CustomerId;
        if(RestContext.request.requestBody!=null){
            try {
                String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
                Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'StripePlanCreate_Service');
                System.debug('-------timestamppp----'+timestamppp);
                System.debug('-------sigFromHmac----'+sigFromHmac);
                
                if((sigFromHmac && sigFromHmac == true) || Test.isRunningTest()){
                    String str = RestContext.request.requestBody.toString();
                    System.debug(str);
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                    Map<String, Object>  lstCustomers = new Map<String, Object>();
                    lstCustomers   = ( Map<String, Object> )results.get('data');
                    Map<String, Object> aryCustomers   = new Map<String, Object>();
                    aryCustomers   =   (Map<String, Object>) lstCustomers.get('object');
                    CustomerId = String.valueOf(aryCustomers.get('id'));
                    String Productid = String.valueOf(aryCustomers.get('product'));
                    Decimal amount = Decimal.valueOf(String.valueOf(aryCustomers.get('amount')));
                    String Interval = String.valueOf(aryCustomers.get('interval'));
                    Double IntervalCount = Double.valueOf(String.valueOf(aryCustomers.get('interval_count')));
                    String pbIdStandard = '';
                    String pbIdNewPlans = '';
                    if(!Test.isRunningTest()){
                        pbIdStandard = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
                        pbIdNewPlans = [Select Id From Pricebook2 WHERE Name = 'PriceBook New Plan' LIMIT 1].Id;
                    }else{
                        pbIdStandard = Test.getStandardPricebookId();
                        pbIdNewPlans = Test.getStandardPricebookId();
                    }

                    List<Product2> prodList = [SELECT id,Stripe_Plan_Id__c,Stripe_Plan_Product_Id__c,Price__c FROM Product2 WHERE Stripe_Plan_Product_Id__c =: Productid];
                    if (prodList.size()>0) {
                        if (prodList[0].Stripe_Plan_Id__c != null) {
                            prodList[0].Stripe_Plan_Id__c = CustomerId;
                            prodList[0].Price__c = amount*0.01;
                            update prodList;
                        }
                        PricebookEntry pbeNew = new PricebookEntry();
                        pbeNew.IsActive = true;
                        pbeNew.Pricebook2Id = pbIdNewPlans;
                        pbeNew.Product2Id = prodList[0].Id;
                        pbeNew.UnitPrice = prodList[0].Price__c;
                        pbeNew.Stripe_Plan_Id__c = CustomerId;
                        insert pbeNew;
                    }else {
                        
                        HttpRequest http = new HttpRequest();
                        http.setEndpoint('https://api.stripe.com/v1/products/'+Productid);
                        http.setMethod('GET');
                        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                        String authorizationHeader = 'BASIC ' +
                        EncodingUtil.base64Encode(headerValue);
                        http.setHeader('Authorization', authorizationHeader);
                        
                        String response;
                        Integer statusCode;
                        
                        Http con = new Http();
                        HttpResponse hs = new HttpResponse();
                        
                        if (!Test.isRunningTest()) {
                            try {
                                hs = con.send(http);
                                response = hs.getBody();
                                statusCode = hs.getStatusCode();
                            } catch (CalloutException e) {
                            
                            }
                        } else {
                            response = '{ "id": "prod_GQKKA3ljlFi2QT", "object": "product", "active": true, "attributes": [ "sfid" ], "caption": null, "created": 1573644397, "deactivate_on": [], "description": null, "images": [], "livemode": false, "metadata": { "tax_code": "NT" }, "name": "$1,000 Product", "package_dimensions": null, "shippable": true, "type": "good", "updated": 1573644398, "url": null }';
                            statuscode = 200;
                        } 
                        Map<String, Object> productMap = (Map<String, Object>)JSON.deserializeUntyped(response);
                        Product2 prod = new Product2();
                        prod.Name = String.valueOf(productMap.get('name'));
                        prod.Stripe_Plan_Id__c = CustomerId;
                        prod.Stripe_Plan_Product_Id__c = Productid;
                        prod.Price__c = amount*0.01;
                        prod.Interval__c = Interval;
                        prod.Interval_Count__c = IntervalCount;
                        prod.Subscription__c = true;
                        prod.IsActive = true;
                        insert prod;
                        
                        PricebookEntry pbeNew = new PricebookEntry();
                        pbeNew.IsActive = true;
                        pbeNew.Pricebook2Id = pbIdStandard;
                        pbeNew.Product2Id = prod.Id;
                        pbeNew.UnitPrice = prod.Price__c;
                        pbeNew.Stripe_Plan_Id__c = CustomerId;
                        insert pbeNew;
                    }
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripePlanCreate_Service',
                        'updateplan',
                        CustomerId
                    )
                );
            }catch(Exception e){
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripePlanCreate_Service',
                        'updateplan',
                         CustomerId,
                         e
                    )
                );
            }
        }
    }
}