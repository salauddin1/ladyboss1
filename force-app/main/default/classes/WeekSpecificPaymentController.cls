/*
 * Developer Name : Tirth Patel
*/
public class WeekSpecificPaymentController {
    public Integer Week {get; set;} 
    public void save(){
        PaymentToCommissionWeekSpecificBatch payBatch = new PaymentToCommissionWeekSpecificBatch(week);
        Database.executeBatch(payBatch);
        week = null;
    }
}