public class ShipStationOrderRelated {
@AuraEnabled
    public static List<ShipStation_Orders__c> getShipStationOrder(String contactId ){
        List<ShipStation_Orders__c> shipVal = new List<ShipStation_Orders__c>();
        List<ShipStation_Order_Items__c> shipValItem = new List<ShipStation_Order_Items__c>();
        shipVal =[select id,Name,orderId__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c,CreateDate__c,orderStatus__c,Tracking_Number__c, addressVerified__c,Ship_To_AddressVerified__c, (select id,ShipStation_Orders__c,name__c from ShipStation_Order_Items__r) from ShipStation_Orders__c where Contact__c =: contactId AND orderStatus__c != 'Draft']; 
        
        //shipVal =[select id,Name,orderId__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c,CreateDate__c,orderStatus__c,ShipStation_Order_Items__r.name__c from ShipStation_Orders__c where Contact__c =: contactId AND orderStatus__c != 'Draft']; 
        return shipVal;
     } 

}