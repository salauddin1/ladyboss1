@isTest
public class CaseAgeTriggerHelperTest {
	@isTest
    public static void CaseAgeTest(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case( Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        getCase.Status = 'open';
        insert getCase ;
        
        
        
    }
    @isTest
    public static void CaseAgeTest1(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        insert getCase ;
        
        getCase.Status = 'open';
        
        update getCase;
        
        
    }
    @isTest
    public static void CaseAgeTest2(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        insert getCase ;
        
        getCase.Status = 'open';
        
        update getCase;
        
        getCase.Status = 'closed';
        
        update getCase;
        
        
    }
    @isTest
    public static void CaseAgeTest3(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        insert getCase ;
        
        getCase.Status = 'open';
        
        update getCase;
        
        getCase.Status = 'closed';
        
        update getCase;
        
        getCase.Status = 'Reopened';
        
        update getCase;
        
        
    }
    @isTest
    public static void CaseAgeTest4(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        insert getCase ;
        
        getCase.Status = 'open';
        
        update getCase;
        
        getCase.Status = 'closed';
        
        update getCase;
        
        getCase.Status = 'Reopened';
        
        update getCase;
        
        getCase.Status = 'Closed-From-Reopened';
        update getCase;
            
   
        getCase.Status = 'Responded';
        update getCase;
        
        
    }
    @isTest
    public static void CaseAgeTest5(){
        Contact  con = new Contact ();
        con.Email = 'badole.salesforce@gmail.com' ;
        con.LastName = 'badole';
        insert con ;
        Case getCase = new Case(Status ='New', Priority = 'Medium', Origin = 'Email');
        getCase.Failed_Amount__c = 40 ;
        getCase.ContactId = con.Id ;
        getCase.Subject = 'Failed payment - $4.00 | InvoiceFail Test | test@test.com | null | Your card was declined. | Payment for invoice 03466AF1-00014';
        getCase.type = 'Account Recovery' ;
        insert getCase ;
        
        getCase.Status = 'open';
        
        update getCase;
        
        getCase.Status = 'closed';
        
        update getCase;
        
        getCase.Status = 'Reopened';
        
        update getCase;
        
        getCase.Status = 'Closed-From-Reopened';
        
        update getCase;
        
        
        
    }
    
}