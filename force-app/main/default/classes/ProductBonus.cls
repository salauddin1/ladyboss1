public class ProductBonus {
    @AuraEnabled
    public static list<product2> getProduct(list<String> FamilyVal){
        list<product2> productItem = new list<product2>();
        productItem = [select Id, Name,Family,Price__c from product2 where IsBonus__c = true AND Family =: FamilyVal AND Family !=: ''];
        return productItem;
    }
@AuraEnabled
    public static list<String> sendMail(String ProductId){
        list<Product2> prod = new list<Product2>();
        list<String> prodAdd = new list<String>();
        prod = [select Id, Name,Email_Body__c,Price__c from Product2 where Id =: ProductId];
        for(Product2 prodVal : prod ){
            prodAdd.add(prodVal.Email_Body__c);
        }
        return prodAdd;
    }
    @AuraEnabled
    public static Product2 getCheckProduct(String ProductId){
        Product2 prod = new Product2();
        prod = [select Id, Name,Email_Body__c,Price__c from Product2 where Id =: ProductId];
		return prod;
    }

}