//Batch to update case owner of Cases which are owned by omni queues and whose status is 'Closed'
global class TaskDueDateBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(Test.isRunningTest()){
            return Database.getQueryLocator('Select id, subject,ActivityDate,Opportunity__c,CreatedDate  from Task where subject Like \'Sold%\' AND CreatedDate < 2019-05-31T00:00:00Z AND ActivityDate != null AND Opportunity__c != null');
        }
        return Database.getQueryLocator('Select id, subject,ActivityDate,Opportunity__c,CreatedDate  from Task where subject Like \'Sold%\' AND CreatedDate < 2019-05-31T00:00:00Z AND ActivityDate != null AND Opportunity__c != null  AND Lastmodifieddate != last_n_days:2 ');
    }   
    global void execute(Database.BatchableContext bc, List<Task> taskList){
    	List<Id> oppID = new List<Id>();
        List<Id> prodIdList = new List<Id>();
        Map<id,Integer> product_and_index = new Map<id,Integer>();
        Map<String, id> prodNameIdMap = new Map<String, id>();
        Map<id, List<String>> oppProductMap = new Map<id, List<String>>();
        for (Task tk : taskList) {
            oppID.add(tk.Opportunity__c);
        }
        for (OpportunityLineItem oli : [SELECT Product2.Name,Product2Id,Product2.isMonthProduct__c,quantity,OpportunityId,CreatedDate,Opportunity.Contact__c,Opportunity.Sales_Person_Id__c,Opportunity.Contact__r.email FROM OpportunityLineItem WHERE OpportunityID in : oppID AND Product2.isMonthProduct__c=true]) {
            prodIdList.add(oli.Product2Id);
            if (!oppProductMap.containsKey(oli.OpportunityId)) {
                oppProductMap.put(oli.OpportunityId, new List<String>());
            }
            oppProductMap.get(oli.OpportunityId).add(oli.Product2.Name);
            prodNameIdMap.put(oli.Product2.Name, oli.Product2Id);
        }
        List<Product2> prodList = [SELECT id,Switch_To_Product_two_month__c,Switch_To_Product_three_month__c,Switch_To_Product_four_month__c,Switch_To_Product_five_month__c,Switch_To_Product_six_month__c FROM Product2 WHERE Switch_To_Product_two_month__c!=null OR Switch_To_Product_three_month__c!=null OR Switch_To_Product_four_month__c!=null OR Switch_To_Product_five_month__c!=null OR Switch_To_Product_six_month__c!=null];
        for(Id prodId : prodIdList){
            for(Product2 prod : prodList){
                if(prod.Switch_To_Product_two_month__c == prodId){
                    product_and_index.put(prodId,2);
                    break;
                }else if(prod.Switch_To_Product_three_month__c == prodId){
                    product_and_index.put(prodId,3);
                    break;
                }else if(prod.Switch_To_Product_four_month__c == prodId){
                    product_and_index.put(prodId,4);
                    break;
                }else if(prod.Switch_To_Product_five_month__c == prodId){
                    product_and_index.put(prodId,5);
                    break;
                }else if(prod.Switch_To_Product_six_month__c == prodId){
                    product_and_index.put(prodId,6);
                    break;
                }
            }            
        }
        for (Task tk : taskList) {
            Date dueDate = tk.CreatedDate.date();
            if (oppProductMap.containsKey(tk.Opportunity__c)) {
                for (String prodName: oppProductMap.get(tk.Opportunity__c)) {
                    if (tk.Subject.contains(prodName)) {
                        if (product_and_index.containsKey(prodNameIdMap.get(prodName))) {
                            tk.ActivityDate = dueDate.addMonths(Integer.valueOf(product_and_index.get(prodNameIdMap.get(prodName)))).addDays(-10); 
                        }
                    }
                }   
            }     
        }    
        update taskList;     
    } 
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }  
}