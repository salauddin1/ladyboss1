@isTest
public class FollowupReadyForTripaltiOnConAccObjTest {
    @isTest
    public static void testmethods() {
        Account act = new account();
        act.First_Name__c = 'test';
        act.Email__c = 'testemail@mail.com';
        act.Name = 'testaccount';
        insert act;
        
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-8);
        l.Created_Date_Time__c = System.today().adddays(-8);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        l.Status = 'Prospect';
        update l;
        Test.startTest();
        Database.executeBatch(new FollowupReadyForTripaltiOnConAccObj());
        System.schedule('Test', '0 0 1 * * ?', new FollowupReadyForTripaltiOnConAccObj());
        Test.stopTest();
    }
}