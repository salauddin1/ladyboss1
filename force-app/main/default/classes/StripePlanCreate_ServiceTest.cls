@isTest
public  class StripePlanCreate_ServiceTest {
    
    @IsTest
    static void methodName1(){
        WebHookSecrets__c wb = new WebHookSecrets__c();
        wb.name = 'StripePlanCreate_Service';
        wb.Webhook_Name__c = 'StripePlanCreate_Service';
        wb.Webhook_Secret__c = 'whsec_cIdDwVu0vpFqtbk13jL35lUQ8RhG2zHS';
        insert wb;
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        String body = '{ "id": "evt_1FtB4KFzCf73siP0SFHdenPj", "object": "event", "api_version": "2018-02-28", "created": 1577185012, "data": { "object": { "id": "bbbbb", "object": "plan", "active": true, "aggregate_usage": null, "amount": 5000, "amount_decimal": "5000", "billing_scheme": "per_unit", "created": 1577185012, "currency": "usd", "interval": "week", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "Weekly", "product": "prod_GQ0TcfDDIfdvoY", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" } }, "livemode": false, "pending_webhooks": 6, "request": { "id": "req_PHVFt7V1J1dNA4", "idempotency_key": null }, "type": "plan.created" }';
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        RestContext.request = req;
   
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        StripePlanCreate_Service.updateplan();
        Test.stopTest();
        
    }
}