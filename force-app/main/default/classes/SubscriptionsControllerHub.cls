public without sharing class SubscriptionsControllerHub {
    @AuraEnabled
    public static List<productWrapper> getAllSubscriptions(String conID){
        List<String> planList = new List<String>{ 'plan_G0cR6HqYNS05Yc', 'plan_FZzPW7swiu2Gvy', 'plan_GPl4hAGAZuVy8z', 'COACHING12PaymentsSubscription', 'plan_DNsW74V6b9CDkP', 'plan_Djr4ju7EI3VlyI', 'plan_DGH610Sy8ohdG2', 'plan_Djr5y2XeVtuIOo', 'plan_DGHRkSoab3br3T', 'SF-COACHING12PaymentsSubscription', 'COACHING12PaymentsSubscription-TA', 'plan_Df4TKobHXJTfB4', 'plan_Did1glucR22BoY', 'plan_DjuK7yknkwjoWA', 'plan_DUEyruaQ3m0Lkc', 'CoachingTwiceAMonth', 'plan_ETxcqmam5L6BPG', 'plan_EUih8HT4lzYcxi', 'plan_DiZgyOjXWNzY34', 'Coaching12Months-SFOrder', 'COACHING12PaymentsSubscription200Plan', 'plan_Djr8C4p20Q2pMn', 'plan_Djqysr9L23VXod', 'COACHING12PaymentsSubscription-AM', 'plan_DjqzcTl5oQLSR8', 'COACHING12PaymentsSubscription-BSK', 'plan_E3YMZAcFnrneTh', 'plan_DqoAob9iy0qWs5', 'plan_Djr0E7cjZGIWIQ', 'COACHING12PaymentsSubscription-JC', 'plan_DjqreAiVBsBmPq', 'Coaching12PaymentsSubscription-JG', 'plan_Djqwyc1mQuHmku', 'COACHING12PaymentsSubscription-JP', '7DayFreeExperience', 'COACHING12PaymentsSubscription-JG', 'plan_Ed1rAzGz8r1oe5', 'plan_Djr2P2jamwIfUF', 'Coaching12PaymentsSubscription-KS', 'plan_DjqxI41yjqAJNp', 'LadyBoss Coaching-12 Monthly Payments-Piper', 'plan_Djr2Tj9y7cgWcv', 'COACHING12PaymentsSubscription-RM', 'plan_Djqzi04kS2OPaX', 'COACHING12PaymentsSubscription-SG', 'plan_Djqzp0aSrdpztc', 'COACHING12PaymentsSubscription-SY', 'plan_DjqyhPdJtHOwvC', 'COACHING12PaymentsSubscription-SP', 'COACHING6PaymentsSubscription', 'Coaching 12 Monthly Payments-Custom End Date', 'Lbuta127plan', 'Lbuta3pay', 'ttmembership', 'ttmembership1', 'LBMonthlySalesTeam', 'plan_F20LuMYqyEa7ed', 'LBMonthlyPlan', 'lbuta127plan', 'lbuta3pay', 'plan_DFphhy99UBrdvs'};
            List<productWrapper> pwList = new List<productWrapper>();
        for (OpportunityLineItem oli: [SELECT id,opportunity.Card_Customer_ID__c, Opportunity.Card__r.Last4__c, Product2.Name, Product2.Stripe_Plan_ID__c,Start__c,TrialPeriodStart__c,TrialPeriodEnd__c,End__c,TotalPrice, Status__c FROM OpportunityLineItem WHERE Opportunity.RecordType.Name = 'Subscription' AND Subscription_Id__c!=null AND Opportunity.Contact__c=:conID AND Opportunity.Contact__c!=null AND Status__c != 'InActive']) {
            productWrapper pw = new productWrapper();
            pw.Id = oli.id;
            pw.Name = oli.Product2.Name;
            pw.periodStart = oli.Start__c;
            pw.periodEnd = oli.End__c;
            pw.Trialstart = oli.TrialPeriodStart__c;
            pw.Trialend = oli.TrialPeriodEnd__c;
            pw.totalPrice = oli.TotalPrice;
            pw.Last4Digit = oli.opportunity.Card__r.Last4__c;
            pw.CustomerID = oli.opportunity.Card_Customer_ID__c;
            if (planList.contains(oli.Product2.Stripe_Plan_Id__c) || oli.Product2.Name.containsIgnoreCase('Trainer Monthly Membership') || oli.Product2.Name.containsIgnoreCase('Coaching 12 Monthly Payments-$497')) {
                pw.HidePause = true;
            }else {
                pw.HidePause = false;
            }
            switch on oli.Status__c {
                when  'Active'{
                    pw.SubStatus = oli.Status__c;
                }
                when  'Past Due'{ 
                    pw.SubStatus = oli.Status__c;
                }
                when  'trialing'{
                    pw.SubStatus = 'Trialing & Paused Memberships';
                }
            }
            pwList.add(pw);
        }
        return pwList;
    }
    @AuraEnabled
    public static void refreshSubscription(String conIDwhen){
        List<OpportunityLineItem> oppLIs = new List<OpportunityLineItem>(); 
        Map<String,Object> subIDs = new Map<String,Object>();
        List<Object> ob = new List<Object>();
        List<Stripe_Profile__c> profLenth = [SELECT Stripe_Customer_Id__c,Customer__c FROM Stripe_Profile__c WHERE Customer__c =: conIDwhen];
        if(profLenth.size() > 0) {
            if(profLenth.size() < 100){
                for (Stripe_Profile__c sp: [SELECT Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Customer__c =: conIDwhen]) {
                    ob.addAll(getSubscriptionsFromStripe(sp.Stripe_Customer_Id__c));
                }
            }
            if(profLenth.size() >= 100){
                Database.executeBatch(new HubSubscriptionFromStripeBatch(profLenth), 50);
            }
        }
        Map<String, Object> obmap1=new Map<String, Object>();
        Map<String,String> subIdSalesPersonMap = new Map<String,String>();
        system.debug('Data object--'+ob);
        for(Object ob1 : ob){
            obmap1=(Map<String, Object>)ob1;
            if((Map<String, Object>)obmap1.get('metadata')!=null && ((Map<String, Object>)obmap1.get('metadata')).get('Sales Person')!=null){
                String sp = (String)((Map<String, Object>)obmap1.get('metadata')).get('Sales Person');
                subIdSalesPersonMap.put(String.valueof(obmap1.get('id')),sp);
            }
            subIDs.put(String.valueof(obmap1.get('id')),ob1);
        }
        List<User> usrList = [select id,Name from user where name in : subIdSalesPersonMap.values()];
        Map<String,User> usrNameToIdMap = new Map<String,User>();
        for(User u : usrList){
            usrNameToIdMap.put(u.name,u);
        }
        Map<String,User> subIdSpMap = new Map<String,User>();
        for(String st : subIdSalesPersonMap.keyset()){
            subIdSpMap.put(st,usrNameToIdMap.get(subIdSalesPersonMap.get(st)));
        }
        System.debug('subIdSpMap : '+subIdSpMap);
        
        List<String> subid = new List<String>();
        subid.addAll(subIDs.keySet());
        Set<String> planIds1= new Set<String>();
        for (String subidFromStripe:subid) {
            Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
            Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
            String planID = String.valueOf(obj.get('id'));
            planIds1.add(planID);//
            System.debug('------All plan ids-----'+planIds1);
        }
        system.debug('subscription ids from stripe--'+subid);
        list<OpportunityLineItem> oliupdate=[select id,OpportunityId,start__c,End__c,Status__c,Subscription_Id__c,TrialPeriodStart__c,TrialPeriodEnd__c  from OpportunityLineItem where Subscription_Id__c IN :subid ];
        system.debug('oliupdate'+oliupdate);
        Map<String,OpportunityLineItem> existingSubOliMap = new Map<String,OpportunityLineItem>();
        for(OpportunityLineItem oli:oliupdate){
            if(!existingSubOliMap.containsKey(oli.Subscription_Id__c)){
                existingSubOliMap.put(oli.Subscription_Id__c,oli);  
                subid.remove(subid.indexOf(oli.Subscription_Id__c));
            }                       
        }
        List<OpportunityLineItem> opliToUpdate = new List<OpportunityLineItem>();
        system.debug('oliupdate'+oliupdate);
        for (String subidFromStripe:existingSubOliMap.keySet()) {            
            OpportunityLineItem opli = existingSubOliMap.get(subidFromStripe);
            Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
            if (obmap!=null) {
                if(obmap.get('status')=='canceled'){
                    opli.Status__c = 'InActive';
                }else if (obmap.get('status')=='active'){
                    opli.Status__c = 'Active';
                }else if (obmap.get('status')=='trialing'){
                    opli.Status__c = 'trialing';
                }else if (obmap.get('status')=='past_due') {
                    opli.Status__c = 'Past Due';
                }
                opli.Number_Of_Payments__c = Invoices.getInvoice(subidFromStripe);
                string unixDatetime1 = String.valueOf(obmap.get('current_period_start'));
                datetime dT = datetime.newInstance(Long.ValueOf(unixDatetime1 )* Long.ValueOf('1000'));
                opli.Start__c = date.newinstance(dT.year(), dT.month(), dT.day());  
                string unixDatetime2 = String.valueOf(obmap.get('current_period_end'));
                datetime dT1 = datetime.newInstance(Long.ValueOf(unixDatetime2 )* Long.ValueOf('1000'));
                opli.End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                if(obmap.get('trial_start') !=null && obmap.get('trial_end') !=null){
                    String datetime2 = String.valueOf(obmap.get('trial_start'));
                    datetime dT2 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                    opli.TrialPeriodStart__c =date.newinstance(dT2.year(), dT2.month(), dT2.day()); 
                    String datetime3 = String.valueOf(obmap.get('trial_end'));
                    datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                    opli.TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                }
                opliToUpdate.add(opli);
            }   
        }
        
        if (subid.size()>0) {
            List<Product2> productToUpdate = new List<Product2>();
            List<PricebookEntry> pricebookToInsert = new List<PricebookEntry>();
            List<product2> prods= [SELECT id,Name,Price__c,Stripe_Plan_Id__c FROM Product2 WHERE Stripe_Plan_Id__c IN :planIDs1];
            System.debug('prods'+prods);
            List<PriceBookEntry> priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_Plan_Id__c FROM PriceBookEntry WHERE Product2Id in :prods AND PriceBook2.isStandard=true];
            System.debug('pricebookentery'+priceBookList);
            Map<String,PriceBookEntry> priceBookMap = new Map<String,PriceBookEntry>();
            Map<String,product2> prodMap = new Map<String,product2>();
            for(PriceBookEntry pbookEntry:priceBookList){
                priceBookMap.put(pbookEntry.Product2.Stripe_Plan_Id__c,pbookEntry);
            }
            for(product2 prod:prods){
                prodMap.put(prod.Stripe_Plan_Id__c,prod);
            }
            Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];
            for (String planID: planIds1) {
                if (!prodMap.containsKey(planID)) {
                    StripeGetPlan splan = StripeGetPlan.getPlan(planID);
                    StripeCreateProduct scp = StripeCreateProduct.getProduct(splan.product);
                    Product2 product = new Product2();                  
                    product.Name = scp.name;                    
                    product.Price__c = splan.amount/100;
                    product.Stripe_Plan_Id__c = planID;
                    productToUpdate.add(product);
                }
            }
            if (productToUpdate.size()>0) {
                insert productToUpdate;
            }
            for (Product2 pr: productToUpdate) {
                PricebookEntry pbe = new PricebookEntry (Pricebook2Id=pb.id, Product2Id=pr.id, IsActive=true, UnitPrice=pr.Price__c);
                pricebookToInsert.add(pbe);             
            }
            if (pricebookToInsert.size()>0) {
                insert pricebookToInsert;
            }
            for (Product2 sObj: productToUpdate) {
                prodMap.put(sObj.Stripe_Plan_Id__c, sObj);
            }
            for (PricebookEntry sObj: [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_Plan_Id__c FROM PriceBookEntry WHERE ID in:pricebookToInsert]) {
                priceBookMap.put(sObj.Product2.Stripe_Plan_Id__c,sObj);
            }
            Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
            List<String> subNewOpp = new List<String>();
            for (Opportunity opp: [SELECT id,Subscription__c FROM Opportunity WHERE Subscription__c in :subid]) {
                oppMap.put(opp.Subscription__c, opp);
            }
            for (String subidFromStripe: subid) {
                if (!oppMap.containsKey(subidFromStripe)) {
                    subNewOpp.add(subidFromStripe);
                }
            }
            if (subNewOpp.size()>0) {               
                List<Opportunity> opps = new List<Opportunity>();
                RecordType rec1 = [SELECT Id FROM RecordType WHERE Name = 'Subscription'];
                for (String subidFromStripe: subNewOpp) {
                    Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                    Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
                    String planID = String.valueOf(obj.get('id'));
                    Product2 prod= prodMap.get(PlanID);
                    Opportunity opp = new Opportunity();
                    opp.Name = prod.Name+'CLUB';
                    opp.Contact__c = conIDwhen;
                    opp.RecordTypeId=rec1.id;
                    if(subIdSpMap.get(subidFromStripe)!=null){
                        opp.Sales_Person_Id__c = subIdSpMap.get(subidFromStripe).Id;
                    }
                    opp.StageName = 'Closed Won';                   
                    opp.Subscription__c = subidFromStripe;
                    string unixDatetime = String.valueOf(obmap.get('current_period_end'));
                    datetime dt = datetime.newInstance(Long.ValueOf(unixDatetime )*Long.ValueOf('1000'));               
                    opp.CloseDate = dt.date();  
                    opps.add(opp);                  
                }
                if (opps.size()>0) { 
                    insert opps;
                }
                for(Opportunity opp:opps){
                    oppMap.put(opp.Subscription__c, opp);
                }
            }
            for (String subidFromStripe:subid) { 
                Map<String, Object> obmap = (Map<String, Object>)subIDs.get(subidFromStripe);
                Map<String, Object> obj = (Map<String, Object>)obmap.get('plan');
                String planID = String.valueOf(obj.get('id'));
                Product2 prod= prodMap.get(planID);
                PriceBookEntry priceBook = new PriceBookEntry();
                priceBook = priceBookMap.get(planID);
                System.debug('==========='+priceBook);
                System.debug('PriceBookEntry'+priceBook);                       
                Opportunity opp = oppMap.get(subidFromStripe);                          
                OpportunityLineItem opline = new OpportunityLineItem();
                opline.OpportunityId = opp.Id;
                try {
                    opline.PricebookEntryId=priceBook.Id;
                    System.debug('---opline.PricebookEntryId----'+opline.PricebookEntryId);
                }
                catch (Exception e){}
                opline.Product2Id = prod.Id;
                System.debug('---opline.Product2Id---'+opline.Product2Id);
                if (String.valueOf(obmap.get('status')) =='active') {
                    opline.Status__c = 'Active';
                }else if (String.valueOf(obmap.get('status')) =='canceled') {
                    opline.Status__c = 'InActive';
                }else if (obmap.get('status')=='trialing'){
                    opline.Status__c = 'trialing';
                }else if (obmap.get('status')=='past_due') {
                    opline.Status__c = 'Past Due';
                }
                string unixDatetime2 = String.valueOf(obmap.get('current_period_end'));
                datetime dt2 = datetime.newInstance(Long.ValueOf(unixDatetime2 )* Long.ValueOf('1000'));    
                opline.End__c = dt2.date();
                opline.TotalPrice = prod.Price__c;
                opline.Subscription_Id__c=subidFromStripe;
                string unixDatetime1 = String.valueOf(obmap.get('current_period_start'));
                datetime dt1 = datetime.newInstance(Long.ValueOf(unixDatetime1 )* Long.ValueOf('1000'));    
                opline.Start__c= dt1.date();
                if(obmap.get('trial_start') !=null && obmap.get('trial_end') !=null){
                    String datetime2 = String.valueOf(obmap.get('trial_start'));
                    datetime dT4 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                    opline.TrialPeriodStart__c =date.newinstance(dT4.year(), dT4.month(), dT4.day()); 
                    String datetime3 = String.valueOf(obmap.get('trial_end'));
                    datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                    opline.TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                }
                opline.quantity =1;              
                opline.Plan_ID__c =planID;  
                oppLIs.add(opline);                 
                System.debug('--oppLIs---'+oppLIs);
            }       
            if (oppLIs.size()>0) {
                PaymentToOliTriggerFlag.runPaymentToOliTrigger = true;
                for(OpportunityLineItem ol : oppLIs){
                    ol.Consider_for_commission__c = true;
                }
                
            }
        }
        if(opliToUpdate.size()>0){
            oppLIs.addAll(opliToUpdate);
        }
        if(oppLIs.size()>0){
            upsert oppLIs;
        }
    }
    public static List<Object> getSubscriptionsFromStripe(String customerID){
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/subscriptions?customer=' + customerID+ '&limit=100&status=all');
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                System.debug('Error in refreshing'+ e.getMessage());
                return null;
            }
        } else {
            hs.setBody('{"data":[{ "id": "sub_EypY8AdfVmqvVo", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1463862018, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "created": 1463862018, "current_period_end": 1558470018, "current_period_start": 1526934018, "customer": "cus_Eug8juCETidS33", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [], "discount": null, "ended_at": null, "items": { "object": "list", "data": [ { "id": "si_18RWRXFzCf73siP0wyYdrGUg", "object": "subscription_item", "billing_thresholds": null, "created": 1463862019, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_EypY8AdfVmqvVo"} ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_EypY8AdfVmqvVo" }, "latest_invoice": "in_1CUKVkFzCf73siP0ka6Oqdrp", "livemode": false, "metadata": {}, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1463862018, "status": "active", "tax_percent": null, "trial_end": 1463862018, "trial_start": 1463862018 }]}') ;
            hs.setStatusCode(200);
        }
        system.debug('#### '+ hs.getBody());
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
        System.debug('results'+results);
        List<Object> data = (List<Object>)results.get('data');
        system.debug('#### cards #### '+ data);
        List<String> subId = new List<String>();
        integer counter=0;
        for(Object ob : data){
            Map<String, Object> obmap = (Map<String, Object>)ob;
            subId.add(String.valueof(obmap.get('id')));
            counter=counter+1;
            if(counter==1)
                break;
        }
        return data;
    }
    
    public class productWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public Date periodStart;
        @AuraEnabled public Date periodEnd;
        @AuraEnabled public Date Trialstart;
        @AuraEnabled public Date Trialend;
        @AuraEnabled public String Id;
        @AuraEnabled public String CustomerID;
        @AuraEnabled public Decimal totalPrice;
        @AuraEnabled public String Last4Digit;
        @AuraEnabled public String SubStatus;
        @AuraEnabled public Boolean HidePause;
    }
    @AuraEnabled
    public static string updateSubscription(String LineItemId,String dateVal,String TimeZone,Boolean isPauseSub,String numOfDays){
        
        String returnSucess='hello ji';
        if (TimeZone == 'AEST' || TimeZone == 'AEDT' ) {
            TimeZone = 'Australia/Sydney';
        }else if (TimeZone == 'ACST' || TimeZone == 'ACDT' ) {
            TimeZone = 'Australia/Adelaide';
        }else if (TimeZone == 'AWST' || TimeZone == 'AWDT' ) {
            TimeZone = 'Australia/Perth';
        }
        Datetime todayDT = Datetime.now();
        String today = todayDT.format('yyyy-MM-dd HH:mm:ss', TimeZone);
        Long timezoneoffset = todayDT.getTime() - Datetime.valueOf(today).getTIme();
        System.debug('Today datetime'+todayDT.format('yyyy-MM-dd HH:mm:ss', TimeZone)+' '+today+' act '+todayDT);
        Date todayD = Date.valueOf(today);
        System.debug(returnSucess+' '+Date.valueOf(dateVal)+'   '+todayD+' actual '+Date.today());
        List<OpportunityLineItem> oppData = new List<OpportunityLineItem>();
        List<Opportunity> opp = new List<Opportunity>();
        List<Card__c> Card = new List<Card__c>();
        if(LineItemId != null && LineItemId != ''){
            oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c,Status__c,End__c,TrialPeriodEnd__c,TrialPeriodStart__c,Start__c,Charged_Sooner_On__c,Paused_for_15_Days_on__c,Paused_for_30_Days_on__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        }
        if(oppData.size()>0){
            opp = [select id,Success_Failure_Message__c,Card__c from Opportunity where Id =:oppData[0].opportunityId];
        }
        if(opp.size()>0){
            Card = [select id,Card_ID__c,Stripe_Card_Id__c from Card__c where id =: opp[0].Card__c limit 1];
        }
        HttpRequest http = new HttpRequest();  
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppData[0].Subscription_Id__c);        
        http.setMethod('POST');
        http.setTimeout(25000);
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String bodyVal;
        Boolean prorateBool = false;
        //bodyVal = '&quantity='+qty+'&prorate='+prorateBool;
        bodyVal = '&prorate='+prorateBool;
        if(dateVal!=null){
            if (Date.valueOf(dateVal) == todayD) {
                bodyVal += '&billing_cycle_anchor=now';
                if (oppData[0].Status__c == 'trialing') {
                    bodyVal += '&trial_end=now';
                }
            }else {
                List<String> dateParts = dateVal.split('-');
                Datetime dateWithTime = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
                
                //Datetime d = Datetime.newInstance(dateWithTime.getTime()+timezoneoffset);
                //d= d.AddDays(1);
                //d= d.AddDays(-1);
                Long l = dateWithTime.getTime();
                system.debug(l);
                Long x = (Long)Datetime.valueOf(l).getTime();
                String a = string.valueOf(x);
                String l1= a.substring(0,a.length()-3);
                
                bodyVal += '&trial_end='+l1;
            }
        }
        if(Card[0].Stripe_Card_Id__c != null && Card[0].Stripe_Card_Id__c != ''){
            bodyVal += '&default_source='+Card[0].Stripe_Card_Id__c+'&default_payment_method='+Card[0].Stripe_Card_Id__c;
        }
        bodyVal += '&';
        System.debug(bodyVal );
        http.setBody(bodyVal );
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        hs = con.send(http);
        if (hs.getstatusCode() == 200){
            StripeListSubscriptionClone.Charges varCharges;
            JSONParser parse;
            system.debug('#### '+ 1);
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());  
            oppData[0].Success_Failure_Message__c = String.valueOf(results.get('status'));
            oppData[0].Status__c = String.valueOf(results.get('status'));          
            if (Date.valueOf(dateVal) != Date.today()) {
                String datetime2 = String.valueOf(results.get('trial_start'));
                if(datetime2 != null) {
                    datetime dT4 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                    oppData[0].TrialPeriodStart__c = date.newinstance(dT4.year(), dT4.month(), dT4.day());
                    oppData[0].Start__c =  date.newinstance(dT4.year(), dT4.month(), dT4.day());
                    String datetime3 = String.valueOf(results.get('trial_end'));
                    datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                    oppData[0].TrialPeriodEnd__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                    oppData[0].End__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
                }
            }else {
                String datetime2 = String.valueOf(results.get('current_period_start'));
                datetime dT4 = datetime.newInstance(Long.ValueOf(datetime2)* Long.ValueOf('1000'));
                oppData[0].Start__c =  date.newinstance(dT4.year(), dT4.month(), dT4.day());
                String datetime3 = String.valueOf(results.get('current_period_end'));
                datetime dT3 = datetime.newInstance(Long.ValueOf(datetime3)* Long.ValueOf('1000'));
                oppData[0].End__c =date.newinstance(dT3.year(), dT3.month(), dT3.day());
            }
            if (isPauseSub) {
                if (numOfDays == '15 Days') {
                    oppData[0].Paused_for_15_Days_on__c = DateTime.now();
                }else if (numOfDays == '30 Days') {
                    oppData[0].Paused_for_30_Days_on__c = DateTime.now();
                }
            }else{
                oppData[0].Charged_Sooner_On__c = DateTime.now();
            }
            update oppData; 
            returnSucess = 'Subscription updated in stripe';
            
            
        }else{
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
            Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
            returnSucess= String.valueOf(errorMap.get('message'));
            if(String.valueOf(errorMap.get('message')).contains('No such subscription')){
                oppData[0].Success_Failure_Message__c = 'customer.subscription.deleted';
                update oppData; 
            }
        }
        
        system.debug(' #### '+ hs.getBody());
        return returnSucess;
    }
    @AuraEnabled
    public static string payPastDueInvoice(String LineItemId){
        String message ;
        List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        if (oppData.size()>0) {
            if (oppData[0].Subscription_Id__c != null) {
                String invoiceId = Invoices.getOpenInvoice(oppData[0].Subscription_Id__c);
                if (invoiceId != null) {
                    message = Invoices.payInvoice(invoiceId);
                }
            }
        }
        return message;
    }
    @AuraEnabled
    public static string pauseSubscriptions(String LineItemId,String numOfDays,String TimeZone){
        Date dateVal ;
        List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c,End__c,TrialPeriodEnd__c,Status__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        if (oppData[0].Status__c == 'trialing') {
            dateVal = oppData[0].TrialPeriodEnd__c;
        }else if (oppData[0].Status__c == 'Active') {
            dateVal = oppData[0].End__c;
        }
        if (numOfDays == '15 Days') {
            dateVal = dateVal.addDays(15);
        }else if (numOfDays == '30 Days') {
            dateVal = dateVal.addDays(30);
        }
        return updateSubscription(LineItemId, String.valueOf(dateVal),TimeZone,true,numOfDays);
    }
}