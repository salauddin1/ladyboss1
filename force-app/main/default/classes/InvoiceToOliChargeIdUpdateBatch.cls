global class InvoiceToOliChargeIdUpdateBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Subscription_Id__c,OpportunityId,Stripe_Charge_Id__c,opportunity.Sales_Person_id__r.name FROM OpportunityLineItem where Subscription_Id__c!=null and stripe_charge_id__c=null and opportunity.week__C!=null and Opportunity.owner.name not in (\'Stripe Site Guest User\') and createdDate>2019-04-08T00:00:00z');
    }
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> oliList){
        try{
        System.debug('oliList : '+oliList);
        List<String> subIdList = new List<String>();
        for(OpportunityLineItem ol : oliList){
            subIdList.add(ol.Subscription_Id__c);
        }
        List<Invoice__c> invList = [SELECT Subscription_Id__c,Charge_Id__c FROM Invoice__c WHERE Subscription_Id__c in : subIdList];
        Map<String,String> subChargeMap = new Map<String,String>();
        if(invList.size() == 0){
            return;
        }
        for(Invoice__c inv : invList){
            subChargeMap.put(inv.Subscription_Id__c,inv.Charge_Id__c);
        }
        for(OpportunityLineItem ol : oliList){
            if(subChargeMap.keyset().contains(ol.Subscription_Id__c)){
                ol.Stripe_Charge_Id__c = subChargeMap.get(ol.Subscription_Id__c);
                if(ol.opportunity.Sales_Person_id__r.name!=null && ol.opportunity.Sales_Person_id__r.name!=''){
                    if(!Test.isRunningTest()){
                    InvoiceToOliChargeHelper.updateChargeCallout(ol,ol.opportunity.Sales_Person_id__r.name,subChargeMap.get(ol.Subscription_Id__c));
                    }
                    system.debug(ol.opportunity.Sales_Person_id__r.name);
                }
            }
        }
        update oliList;
            if(Test.isRunningTest()){
                throw new System.SObjectException();
            }
            
        }catch(Exception e){
                System.debug(e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'InvoiceToOliChargeIdUpdateBatch',
                        'execute',
                        null,
                        e
                    )
                );
        }
    }     
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }  
}