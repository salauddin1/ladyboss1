public class shipStationOrderUpdateBatchCount{
    @future(callout=true)
    public static void Ordercounter () {
         String header;
        String FaieldOrderId;
        ShipStation__c ShipStationUser = new ShipStation__c();
        try{
        ShipStationUser = [select Domain_Name__c,userName__c, password__c,isLive__c from ShipStation__c where isLive__c=true];
        System.debug('---ShipStationUser---'+ShipStationUser);
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }   
        System.debug('--header-'+header);
        ShipStationOrderUpdateBatch_Pages__c orddays = [select Pages__c,Number_of_Days__c from ShipStationOrderUpdateBatch_Pages__c limit 1];
        Date StartDate = Date.today();
        String StartDattime = String.valueOf(StartDate.addDays(-(Integer.valueOf(orddays.Number_of_Days__c))));
        StartDattime = StartDattime.replace(' ', 'T');
        String EndDate =String.valueOf(DateTime.now());
        EndDate = EndDate.replace(' ', 'T');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?modifyDateStart='+StartDattime+'&modifyDateEnd='+EndDate+'&pageSize='+Integer.valueOf(orddays.Pages__c));
        req.setMethod('GET');
        req.setHeader('Authorization', 'Basic '+header);
        req.setTimeout(6000);
        req.setHeader('Accept', 'application/json');
        String ResponseBody;
        try{
            HttpResponse res = new Http().send(req);
            Integer statusCode = res.getStatusCode();
            if(statusCode == 200){
                ResponseBody = res.getBody();
            }
            System.debug('======ResponseBody===='+ResponseBody);
        }
        catch (Exception e){}
        if(ResponseBody != null) {
            Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
            Integer totalcount = Integer.valueOf(ordResult.get('total'));
            Integer totalpages = Integer.valueOf(ordResult.get('pages'));
            System.debug('=====totalcount======'+totalcount+'=========='+totalpages);
            orddays.Start_Page__c = 0;
            orddays.End_page__c = totalpages;
        }
        update orddays;
        }
        catch(exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(new ApexDebugLog.Error('Ordercounter','shipStationOrderUpdateBatchCount',e.getMessage()+e.getLineNumber(), e));
        }
    }
    
}