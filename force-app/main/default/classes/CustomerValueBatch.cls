/*
* Devloper : Sourabh Badole
* Description : batch class to calculate(Rollup) customer value index of Created Opportunity/Order on Contact object.
*/
global class CustomerValueBatch implements Database.Batchable<sObject>{
    
    Integer limitCount = 0 ;
    global CustomerValueBatch(){
    }
    global CustomerValueBatch(Integer Count){
        this.limitCount = count ;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select id,Total_Spend_Amount__c,Spend_Without_Refund__c, ReRunCustomerValue__c  from Contact where ReRunCustomerValue__c != true limit '+ limitCount;
        System.debug('Test Query'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contact> conList){
        try{
            
            
            ContactTriggerFlag.isContactBatchRunning = true ;
            Set<id> conSet = new Set<id>();
            if(conList != null && !conList.isEmpty()){
                for(Contact con: conList) {
                    conSet.add(con.id);
                }
            }
            List<Opportunity>  oppList = new List<Opportunity>(); 
            if(conSet != null && !conSet.isEmpty()){
                oppList    = [select id, Amount ,Refund_Amount__c,contact__c from Opportunity where Contact__c!= null and  Contact__c =: conSet and Amount  != 0];
            }
            if(conList != null && !conList.isEmpty()){
                for(Contact con  : conList) {
                    Decimal oppAmount = 0;
                    Decimal oppAmountWithRefund = 0;
                    if(oppList != null && !oppList.isEmpty()){
                        for(Opportunity opp : oppList) {
                            if(opp.contact__c != null && con.id == opp.contact__c && opp.Amount != null) {
                                oppAmount = oppAmount + opp.Amount ;
                            }
                            if(opp.contact__c != null && con.id == opp.contact__c && opp.Amount != null && opp.Refund_Amount__c != null ) {
                                oppAmountWithRefund  = oppAmountWithRefund + opp.Amount - opp.Refund_Amount__c ;
                            }
                        }
                    }
                    if(oppAmount != null){
                        con.Total_Spend_Amount__c = oppAmount ;
                    }
                    if(oppAmountWithRefund != null){
                        con.Spend_Without_Refund__c = oppAmountWithRefund ;
                    }
                    con.ReRunCustomerValue__c = true ;
                }
            }
            if(conList != null && !conList.isEmpty()){
                Database.Update(conList) ;
            }
        }
        catch(Exception ex){
            String addId = '';
            if(conList != null && !conList.isEmpty()){
                for(Contact cs : conList){ addId = cs.id +'   '+addId;}
            }
            
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('execute','CustomerValueBatch',addId,ex));
            System.debug('***Get Exception***'+ex);
            
        }
    }
    global void finish(Database.BatchableContext BC){
    }
}