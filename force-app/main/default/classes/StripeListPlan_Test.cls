@IsTest
public class StripeListPlan_Test {

	static testMethod void testParse() {
		String json = '{'+
		'    \"object\": \"list\",'+
		'    \"data\": ['+
		'        {'+
		'            \"id\": \"test2\",'+
		'            \"object\": \"plan\",'+
		'            \"amount\": 2655,'+
		'            \"created\": 1508947122,'+
		'            \"currency\": \"usd\",'+
		'            \"interval\": \"week\",'+
		'            \"interval_count\": 2,'+
		'            \"livemode\": false,'+
		'            \"metadata\": {},'+
		'            \"name\": \"Test Product 2\",'+
		'            \"statement_descriptor\": null,'+
		'            \"trial_period_days\": null'+
		'        }'+
		'    ],'+
		'    \"has_more\": true,'+
		'    \"url\": \"/v1/plans\"'+
		'}';
		StripeListPlan.Plans r = StripeListPlan.Plans.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListPlan.Plans objJSON2Apex = new StripeListPlan.Plans(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.object_Z == null);
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.has_more == null);
		System.assert(objJSON2Apex.url == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListPlan.Metadata objMetadata = new StripeListPlan.Metadata(System.JSON.createParser(json));
		System.assert(objMetadata != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListPlan.Data objData = new StripeListPlan.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_Z == null);
		System.assert(objData.amount == null);
		System.assert(objData.created == null);
		System.assert(objData.currency_data == null);
		System.assert(objData.interval == null);
		System.assert(objData.interval_count == null);
		System.assert(objData.livemode == null);
		System.assert(objData.metadata == null);
		System.assert(objData.name == null);
		System.assert(objData.statement_descriptor == null);
		System.assert(objData.trial_period_days == null);
	}
}