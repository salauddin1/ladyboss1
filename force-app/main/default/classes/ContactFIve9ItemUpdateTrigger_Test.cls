@isTest
public class ContactFIve9ItemUpdateTrigger_Test {
   public testmethod Static void batchFive9ItemsTest(){ 
       List<Contact> conlist =new List<Contact>();
       Map <Id, Contact> conMap= new  Map <Id, Contact>();
        Contact con= new Contact();
        con.LastName='Test FIve9';
        con.MobilePhone='1234556678';
        con.Product__c='Trial';
        con.Potential_Buyer__c='BookLead-1 Day';
        con.Campaign_Movement_Not_Allowed__c=false;
        insert con;
        conlist.add(con);
        con.Product__c='Book';
        Update con;
        conMap.put(con.id,con);
        Five9LSP__Five9_List__c lis2 =new Five9LSP__Five9_List__c();
        lis2.Five9LSP__API_URL__c='Https://Test.com';
        lis2.Five9LSP__Five9_Domain__c='TestDomain';
        lis2.Five9LSP__Five9_User_Name__c='TestUser@gmail.com';
        lis2.Five9LSP__Five9_User_Password__c='test@12345';
        lis2.Name=con.Potential_Buyer__c;
        lis2.Product__c='Trial';
        lis2.Max_Age__c=2;
        lis2.Min_Age__c=1;
        lis2.Five9_Processing_Priority__c=1;
        insert lis2;
       
        Five9LSP__Five9_List__c lis =new Five9LSP__Five9_List__c();
        lis.Five9LSP__API_URL__c='Https://Test.com';
        lis.Five9LSP__Five9_Domain__c='TestDomain';
        lis.Five9LSP__Five9_User_Name__c='TestUser@gmail.com';
        lis.Five9LSP__Five9_User_Password__c='test@12345';
        lis.Name='Trial Buy-1Day';
        lis.Product__c='Trial';
        lis.Max_Age__c=2;
        lis.Min_Age__c=1;
        lis.Five9_Processing_Priority__c=1;
        insert lis;
        Workflow_Configuration__c wc=new Workflow_Configuration__c();
        wc.Active__c=true;
        wc.ListMovementActive__c=true;
        wc.Source_Five9_List__c=lis.Id;
        wc.Target_Five9_List__c=lis.Id;
        wc.Products__c=lis.Product__c;
        insert wc;
       
        Test.startTest();
        ContactFIve9ItemUpdateTrigger_Handler.doAfterInsert(conList);
        ContactFIve9ItemUpdateTrigger_Handler.doBeforeUpdate(conList, conMap);
        ContactFIve9ItemUpdateTrigger_Handler.addPotentialBuyers(conMap);
        ContactFIve9ItemUpdateTrigger_Handler.Coverage();
        Test.stopTest();
   }
}