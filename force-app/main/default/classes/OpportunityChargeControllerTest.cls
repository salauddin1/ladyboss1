@isTest
public class OpportunityChargeControllerTest {
    @IsTest
    static void methodName(){
        
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        Hub_Order_Page_Products__c hub = new Hub_Order_Page_Products__c();
        hub.Name = 'Unlimited';
        hub.Product_Name__c = 'Unlimited';
        hub.Url_Link__c='test';
        insert hub;
        
        RecordType rt = [SELECT id FROM RecordType Where DeveloperName='Charge' LIMIT 1];
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Site';
        con.Email = 'test@site.com';
        con.Phone = '78569854126';
        insert con;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='shipped';
        shipOrder.orderNumber__c = 'ch_1234567890';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        shipOrder.Carrier_Code__c = 'UPS';
        shipOrder.Tracking_Number__c = 'trx_1234567890';
        insert shipOrder;
        opportunity op = new opportunity();
        op.Name = 'laptop';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        op.Contact__c = con.id;
        op.RecordTypeId = rt.id;
        op.ShipStation_Orders__c = shipOrder.Id;
        insert op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,Category__c='Unlimited',
                                     Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 5;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        insert oli;
        Test.startTest();
            OpportunityChargeController.getOpportunity(con.id);
            OpportunityChargeController.searchOpportunity(con.id, 'laptop');
        Test.stopTest();
        
    }
    @IsTest
    static void methodName2(){
        
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        Hub_Order_Page_Products__c hub = new Hub_Order_Page_Products__c();
        hub.Name = 'Unlimited';
        hub.Product_Name__c = 'Unlimited';
        hub.Url_Link__c='test';
        hub.Description_name__c = 'Unlim';
        insert hub;
        
        RecordType rt = [SELECT id FROM RecordType Where DeveloperName='Charge' LIMIT 1];
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Site';
        con.Email = 'test@site.com';
        con.Phone = '78569854126';
        insert con;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='shipped';
        shipOrder.orderNumber__c = 'ch_1234567890';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        shipOrder.Carrier_Code__c = 'UPS';
        shipOrder.Tracking_Number__c = 'trx_1234567890';
        insert shipOrder;
        opportunity op = new opportunity();
        op.Name = 'laptop';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        op.Contact__c = con.id;
        op.RecordTypeId = rt.id;
        op.Description = 'Unlim';
        op.ShipStation_Orders__c = shipOrder.Id;
        insert op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 5;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        insert oli;
        Test.startTest();
            OpportunityChargeController.getOpportunity(con.id);
            OpportunityChargeController.searchOpportunity(con.id, 'laptop');
        Test.stopTest();
        
    }
    @IsTest
    static void methodName1(){
        
        ContactTriggerFlag.isContactBatchRunning=true;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        Hub_Order_Page_Products__c hub = new Hub_Order_Page_Products__c();
        hub.Name = 'Unlimited';
        hub.Product_Name__c = 'Unlimited';
        hub.Url_Link__c='test';
        insert hub;
        
        RecordType rt = [SELECT id FROM RecordType Where DeveloperName='Charge' LIMIT 1];
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Site';
        con.Email = 'test@site.com';
        con.Phone = '78569854126';
        insert con;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='shipped';
        shipOrder.orderNumber__c = 'ch_1234567890';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        shipOrder.Carrier_Code__c = 'UPS';
        shipOrder.Tracking_Number__c = 'trx_1234567890';
        insert shipOrder;
        opportunity op = new opportunity();
        op.Name = 'laptop';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        op.Contact__c = con.id;
        op.RecordTypeId = rt.id;
        op.ShipStation_Orders__c = shipOrder.Id;
        insert op;
        
       
        Test.startTest();
            OpportunityChargeController.getOpportunity(con.id);
            OpportunityChargeController.searchOpportunity(con.id, 'laptop');
        Test.stopTest();
        
    }
}