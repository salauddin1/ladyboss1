@isTest
private class MCCampaignMembers_Batch_Test {
    public testmethod Static void batchCampaignMembersTest(){
        
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new  Map<String,String>();
        
        //Contact.Campaign_Movement_NOt_Allowed__c = false and 
        //Campaign.Marketing_Cloud_Tags__c != null and 
        //(NOT Campaign.Name Like \'%trigger%\') and 
        //Age_In_Hours__c > 24 and 
        //MC_Campaign_Movement__c = false and 
        //Contact.Amazon_Customer__c=false and 
        //Campaign.Type=\'Marketing Cloud\'';

        
        Contact ld1 = new Contact(email = 'con@gmail.com', LastName = 'test1Name',FirstName = 'testing1company', Campaign_Movement_NOt_Allowed__c=false, Amazon_Customer__c=false);
        Contact ld2 = new Contact(email = 'con2@gmail.com', LastName = 'test2Name',FirstName = 'testing2company',Campaign_Movement_NOt_Allowed__c=false, Amazon_Customer__c=false);
        List<Contact> ldList = new List<Contact>{ld1,ld2};
            insert ldList;
        
        List<Campaign> cList = new List<Campaign>();
        
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1-4';
        c1.Product__c = 'Book'; 
        c1.Max_Age__c = 4;
        c1.Min_Age__c = 0;
        c1.Marketing_Cloud_Tags__c ='Book Buyer';
        c1.Type ='Marketing Cloud';
        cList.add(c1);
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Buy Days 5-7';
        c2.Product__c = 'Book'; 
        c2.Max_Age__c = 10;
        c2.Min_Age__c = 5;
        c2.Marketing_Cloud_Tags__c ='Book Buyer';
        c2.Type ='Marketing Cloud';
        cList.add(c2);
        
        Campaign c3 =new  Campaign();
        c3.Name = 'Campaign Lead Days 8-10';
        c3.Product__c = 'Book'; 
        c3.Max_Age__c = 10;
        c3.Min_Age__c = 8;
        c3.Marketing_Cloud_Tags__c ='Book Buyer';
        c3.Type ='Marketing Cloud';

        cList.add(c3);
        Campaign c4 =new  Campaign();
        c4.Name = 'Campaign Lead Days 11-30';
        c4.Product__c = 'Book'; 
        c4.Marketing_Cloud_Tags__c ='Book Buyer';
        //c4.Max_Age__c = ;
        c4.Type ='Marketing Cloud';

        c4.Min_Age__c = 11;
        cList.add(c4);        
        insert cList;
        
        
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c1.Id,contactId = ld1.Id , Created_Date__c = d1,Created_Date_Time__c=System.now().addHours(-30),MC_Campaign_Movement__c=false);
        insert cm1;   
        
        
        Date d2 = dt.addDays(-40);
        CampaignMember cm2 = new CampaignMember(CampaignId = c3.Id,contactId = ld2.Id , Created_Date__c = d2 , Created_Date_Time__c=System.now().addHours(-30),MC_Campaign_Movement__c=false);
        insert cm2;    
        
        //List<CampaignMember> cmList =[SELECT Id,Name,CampaignId,Product__c,Created_Date__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Name, Age__c,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = False ];

        //List<Campaign> campaignList = [SELECT Id, Name, Product__c, Max_Age__c, Min_Age__c  FROM Campaign ];
        
        Campaign_Products__c setting = new Campaign_Products__c();
        setting.Name = 'Book';
        insert setting;        
       
        Test.startTest();
        MCCampaignMembers_Batch batch = new MCCampaignMembers_Batch();
        Database.executebatch(batch);
        Test.stopTest();
        
        
    }
}