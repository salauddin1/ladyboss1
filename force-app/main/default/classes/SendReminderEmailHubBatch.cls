global class SendReminderEmailHubBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful,Schedulable{
    global void execute(SchedulableContext sc){
        SendReminderEmailHubBatch b = new SendReminderEmailHubBatch();
        database.executebatch(b,100);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select id,Remind_Me_Later__c,Product2.Name,Opportunity.Contact__r.Email,Opportunity.Contact__r.Id,End__c From OpportunityLineItem WHERE Remind_Me_Later__c = true AND Opportunity.Contact__r.Email != null');
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> ();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
        Id templateId = [select id, name from EmailTemplate where name = : 'ReminderEmailHub'].id;
        for (OpportunityLineItem oli : scope) {
            Date reminderDate = oli.End__c.addDays(-3);
            Date today = Date.today();
            if (today == reminderDate) {
                oli.Remind_Me_Later__c = false;
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTargetObjectId(oli.Opportunity.Contact__r.Id);
                message.setWhatId(oli.Id);
                message.setTemplateId(templateId);
                message.setBccSender(false);
                message.setUseSignature(false);
                message.setSaveAsActivity(false); 
                message.setOrgWideEmailAddressId(owea.get(0).Id);
                messages.add(message);
            }
        }
        if (messages.size()>0) {
            Messaging.sendEmail(messages);
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
   
}