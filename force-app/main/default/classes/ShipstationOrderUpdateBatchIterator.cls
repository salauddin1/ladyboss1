public class ShipstationOrderUpdateBatchIterator implements Iterator<Integer>{
    public Integer page;
    public ShipStationOrderUpdateBatch_Pages__c ord;
    public Integer count;
    public ShipstationOrderUpdateBatchIterator(){
        ord = [select Pages__c,End_page__c,Start_Page__c from ShipStationOrderUpdateBatch_Pages__c limit 1];
        //page =0;
        page = Integer.valueOf(ord.Start_Page__c);
    }
    public boolean hasNext(){
        
        if(page >=Integer.valueOf(ord.End_page__c)) {
           return false;
       } else {
           return true;
       }
    }
    public Integer next(){
        if(page == Integer.valueOf(ord.End_page__c)){return null;}
        page++;
        return page;
    }
}