@isTest
public class ShopifyOrderCreation_Test {
    public static TestMethod void test(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        insert pro;
        Shopify_Parameters__c shopData = new Shopify_Parameters__c();
        shopData.Access_Token__c = 'testToken12';
        shopData.Domain_Name__c = 'https://test.com';
        shopdata.Is_Active__c = true;
        insert shopData;
        Shopify_Orders__c shopOrder = new Shopify_Orders__c();
        shopOrder.phone__c = '2345678';
        shopOrder.address1__c = 'Indore city';
        shopOrder.city__c = 'Indore';
        shopOrder.country__c = 'India';
        shopOrder.Status__c = 'Confirmed';
        insert shopOrder;
        shopOrder.Status__c = 'canceled';
        update shopOrder;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class,new OrderCalloutTest()) ;
        String contactData ='{"sobjectType":"Shopify_Orders__c","Contact__c":"0030j00000FTRfDAAX","First_Name__c":"Ashish test Sharma test","Last_Name__c":"sharma","address1__c":"ram ","city__c":"indore","zip__c":"452001","province__c":"mp","Send_Fulfillment_Receipt__c":true,"Send_Receipt__c":true,"Financial_Status__c":"pending","phone__c":"(505) 288-4551","Modify_Date__c":"2019-02-02","fulfillment_Status__c":"fulfilled","country__c":"Uk"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        ShopifyOrderCreation.getContact(co.Id);
        ShopifyOrderCreation.shipOrder('test');
        ShopifyOrderCreation.CreateShopifyOrders(contactData,productData);
        ShopifyOrderCreation.UpdateOder(shopOrder.Id);
        ShopifyOrderCreation.cancelledOrder(shopOrder.Id);
        Test.stopTest();
    }  
    
}