@isTest
public class CampaignMemberTriggerHandlerTest {
	
    @isTest static void CampMember() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        insert cnt;
        
       /* Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld; */
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        
        //CampaignMemberTriggerHandler cmTr = new CampaignMemberTriggerHandler();
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
        Test.stopTest();
    }
    
    @isTest static void sendingSMS1() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
      /*  Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld; */
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('abc'));
		CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
    }
    
    @isTest static void sendingSMS2() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.OtherPhone = '9876543210';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
      /*  Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld;  */
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
		
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
    }
    
    @isTest static void sendingSMS3() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
  /*      Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld; */
        
        
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
       // cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
    }
    @isTest static void sendingSMS4() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
     Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.AssistantPhone = '9876543210';
        insert cnt;
        
    /*    Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld; */
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
    }
    @isTest static void sendingSMS5() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.Unsubscribe_Me__c = 'Yes';
        cnt.LastName = 'TEst';
        cnt.AssistantPhone = '9876543210';
        insert cnt;   
        
    /*    Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'Test';
        ld.Company = 'Test';
        insert ld; */
        
        Campaign cmp = new Campaign();
        cmp.Name = 'New Campaign Test';
        insert cmp;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        //cmpMem.LeadId = ld.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        
        CampaignMemberTriggerHandler.sendSMS(cmpMem.Id, 'messageData',null);
    }  
}