public class ChangeDueDateController {
    @AuraEnabled
    public static Task getTask(Id taskId){
        List<Task> subsequentList = [select id,Status,Called__c,Recovered_Payment__c,Saved_Payment__c,Contact__c,ActivityDate,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,CallLog_Subject__c,Text_Message__c from Task Where id=:taskId];
        return(subsequentList[0]);
    }  
    @AuraEnabled public static Task saveTask(Task task,String AddRemoveDays,String fieldNameForDays){
        
        AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfTask' );
        if(fieldNameForDays == 'Recovered_Payment__c'){
            recoveredPayment(task, AddRemoveDays);
        }
        else if(myCustomSetting != null){
            Decimal theFieldValue = (Decimal) myCustomSetting.get(fieldNameForDays);
            System.debug('theFieldValue : '+theFieldValue);
            System.debug('AddRemoveDays : '+AddRemoveDays);
            if(AddRemoveDays == 'true'){
                task.ActivityDate = task.ActivityDate.addDays(integer.valueof(theFieldValue));
            }else if(AddRemoveDays == 'false'){
                task.ActivityDate = task.ActivityDate.addDays(0-integer.valueof(theFieldValue));
            }
            if(fieldNameForDays == 'Stop_Correspondence__c'){
            	updateContact(task, AddRemoveDays);
            }
            if(fieldNameForDays == 'Saved_Payment__c'){
                updateTaskStatus(task, AddRemoveDays);
            }
        }
        
        upsert task;
        return task;
    }  
    public static void updateContact(Task task,String flag){
        System.debug('contactId is : '+task.Contact__c);
        System.debug('isTrue : '+flag);
        Boolean value = false;
        if(flag=='true'){
            value = true;
        }
        List<Contact> ctList = [SELECT Id,DoNotCall,Other_Phone_National_DNC__c,Phone_National_DNC__c FROM Contact WHERE Id = : task.Contact__c];
        ctList.get(0).DoNotCall = value; 
        ctList.get(0).Other_Phone_National_DNC__c = value; 
        ctList.get(0).Phone_National_DNC__c = value; 
        update ctList;
    }
    public static void updateTaskStatus(Task task,String flag){
        System.debug('task : '+task);
        System.debug('flag : '+flag);
        if(flag == 'true'){
        	task.Status = 'Completed';
        }else{
            task.Status = 'Open';
        }
        update task;
    }
    public static void recoveredPayment(Task task,String flag){
        if(flag == 'true'){
            task.ActivityDate = System.today();
        }
        update task;
    }
}