// Created By : Sourabh Badole 
// Date       : 8-3-2019
@isTest 
public class CustomerValueBatchTest {
    static testMethod void testMethod1() {
        Contact con = new Contact();
        con.LastName = 'Badole' ;
        con.ReRunCustomerValue__c = false ;
        insert Con;
        
        Opportunity oppCreate = new Opportunity();
        
        oppCreate.StageName = 'Sourcing Demand';
        oppCreate.CloseDate = Date.today() ;
        oppCreate.Name = 'Test Opportunity ';
        oppCreate.Contact__c = Con.id;
        oppCreate.Stripe_Charge_Id__c = 'test';
        oppCreate.Amount = 123.00 ;
        oppCreate.Refund_Amount__c = 23.00 ;
        insert oppCreate;
        
        
        
        Test.startTest();
        
        CustomerValueBatch obj = new CustomerValueBatch(1);
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }
    
}