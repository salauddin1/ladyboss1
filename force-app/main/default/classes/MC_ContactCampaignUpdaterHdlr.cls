//This Clas is to tag the marketing cloud tag 
public class MC_ContactCampaignUpdaterHdlr{
    //To update the Marketing cloud tag field on contact
    @future
    public static void updateContact(Set<Id> idset1 ){
        try{
            set<Id> idset = new set<id>();
            Map<id,CampaignMember> campMap1 = new Map<id,CampaignMember>();
            Map<id,List<String>> campMap = new Map<id,List<String>>();
            //Soql to get CampaignMember
            List<CampaignMember > cmListT = [SELECT Campaign.Name, ContactId, Description, Id, Name, State, Status FROM CampaignMember where id IN: idset1];
            //Loop over the CampaignMember to put the single contact and list of campaign names
            for(CampaignMember cm : cmListT ) {
                if(cm.ContactId!=null) {
                    idset.add(cm.ContactId);
                    if(!campMap.containsKey(cm.ContactId)){
                        List<String> cList = new List<String>();
                        cList.add(cm.Campaign.Name);
                        campMap.put(cm.ContactId,cList);
                    }else{
                        List<String> cList = campMap.get(cm.ContactId);
                        cList.add(cm.Campaign.Name);
                        campMap.put(cm.ContactId,cList);
                    }
                }
                
                
            }
            //TO get the values of the marketing cloud tag field's values
            Schema.DescribeFieldResult field = Contact.Marketing_Cloud_Tags__c.getDescribe();
            Set<String> myPicklist = new Set<String>();
            for (Schema.PicklistEntry f : field.getPicklistValues()){
                myPicklist.add(f.getLabel());
                
            }
            
            List<Contact>  conListUpdate = new List<Contact>();
            List<Contact> conList1 = [SELECT Id,Marketing_Cloud_Tags__c FROM Contact where ID IN : idset ];
            //LOOP over the contact list
            for(Contact c : conList1){
                //If the campaign map contains the contact ID then enter into loop
                if(campMap.containsKey(c.id)){
                    List<String> conList  = campMap.get(c.id);
                    //Loop over the campain name list
                    for(String str: conList) {
                        //If marketing cloud tag field's valus contains the campaign name then add the value to the Marketing_Cloud_Tags__c
                        if(myPicklist.contains(str)){
                            if(c.Marketing_Cloud_Tags__c!=null && !c.Marketing_Cloud_Tags__c.contains(str)){
                                c.Marketing_Cloud_Tags__c = c.Marketing_Cloud_Tags__c+ ';' + str;
                                
                            }else if(c.Marketing_Cloud_Tags__c==null ){
                                c.Marketing_Cloud_Tags__c =str;
                            }
                        }
                        
                    }
                }
                conListUpdate.add(c);
            }
            if(conListUpdate.size() > 0){
                //Update the contact
                update conListUpdate;
            }
            if(test.isRunningtest()){
                integer a=10/0;
                
            }
        }catch(Exception ex){
            system.debug(ex.getMessage()+ex.getLineNumber());
            
            
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'MC_ContactCampaignUpdaterHdlr',
                    'updateContact',
                    'Trigger for campaign',
                    ex
                )
            );
        }
        
    }
    
}