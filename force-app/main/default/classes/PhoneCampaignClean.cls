public class PhoneCampaignClean {
    
    //==If any Contact present into any 'Pipeline' And 'Phone Team' then delete from phoneTeam
    public static List<CampaignMember> cleanPipeline(List<CampaignMember> cMemberPhoneList ,Map <Id,List<CampaignMember>> conCmMap){
        List<CampaignMember> cMembPhoneDelList = new List<CampaignMember>();
        Map<String,CampaignMember> pipProdCamp = new Map<String,CampaignMember>();
        for(CampaignMember campMember:cMemberPhoneList){
            System.debug('=====AllCampId==='+campMember.Id);
            if(conCmMap.containsKey(campMember.ContactId)){
                List<CampaignMember> cMemberList = new List<CampaignMember>(conCmMap.get(campMember.ContactId));
                System.debug('====cMemberList=====>'+cMemberList);
                if(cMemberList != null && !cMemberList.isEmpty()){
                    for(CampaignMember campMemb:cMemberList){
                        if(campMember.ContactId == campMemb.ContactId){
                            if(campMemb.Campaign.Type == 'Pipeline' && campMember.Campaign.Type == 'Phone Team' &&  campMemb.Campaign.Product__c == campMember.Campaign.Product__c){
                                System.debug('====DeleteCampID=====>'+campMember.Id);
                                cMembPhoneDelList.add(campMember);
                            }
                        }
                    }
                }
            }
        }
        // System.debug('=====cMemberPipelineList====>'+cMemberPipelineList);
        System.debug('====cMembPhoneDelList=====>'+cMembPhoneDelList);
        return cMembPhoneDelList;
    }
    
    //== Same Contact Higher priority Product removing Lower priority product
    /*
public static List<CampaignMember> cleanPriority(List<CampaignMember> cMemberPhoneList ,set<Id> contactId,Map <Id,List<CampaignMember>> conCmMap){ 

List<CampaignMember> cMembPhoneDelList = new List<CampaignMember>();
List<Campaign_Product_Heirarchy__c> cusSettinglst = new List<Campaign_Product_Heirarchy__c>([Select Id,Name,Priority__c from Campaign_Product_Heirarchy__c]);
System.debug('==cusSettinglst=='+cusSettinglst);
Map<String,Integer> cusSettingMap = new Map<String,Integer>();
Set<CampaignMember> cmSet = new Set<CampaignMember>();
if(cusSettinglst != null && !cusSettinglst.isEmpty()){
for(Campaign_Product_Heirarchy__c cusSetting : cusSettinglst){
cusSettingMap.put(cusSetting.Name,Integer.valueof(cusSetting.Priority__c.trim()));
}
}
system.debug('===cusSettingMapvalues===>>'+cusSettingMap.values());
system.debug('===cusSettingMapKeyset===>>'+cusSettingMap.keySet());
if(contactId != null && !contactId.isEmpty()){
for(Id conId : contactId){
Integer i=0;
Integer j=0;
Map <String,String> cmHighestPrdMap = new Map <String,String>(); 
if(conCmMap.containsKey(conId)){
List<CampaignMember> cMember = conCmMap.get(ConId);
if(cMember != null && !cMember.isEmpty()){
for(CampaignMember cm : cMember){
if(cm.Campaign.Type == 'Phone Team'){
// system.debug('===Product==>>'+cusSettingMap.get(cm.Campaign.Product__c));
if(j==0){
i= cusSettingMap.get(cm.Campaign.Product__c);
cmHighestPrdMap.put('1',cm.Campaign.Product__c);
j++;

}
if(i<cusSettingMap.get(cm.Campaign.Product__c)){
cmHighestPrdMap.put('1',cm.Campaign.Product__c);
}
}
}
system.debug('===Contact==>>'+conId+'===HighestProduct==>>'+cmHighestPrdMap.get('1'));
for(CampaignMember cm : cMember){
if(cm.Campaign.Product__c == cmHighestPrdMap.get('1') && cm.Campaign.Type == 'Phone Team'){
cmSet.add(cm);
System.debug('=====CampaignMemberNotToDelete======>>'+cm.Id);
}
}
} 
}
}
}
System.debug('====cmSet===='+cmSet);
List<CampaignMember> camLst = new List<CampaignMember>();
camLst.addAll(cmSet);
System.debug('====camLst====>>>'+camLst);
Map<Id,CampaignMember> campMap = new Map<Id,CampaignMember>();
for(CampaignMember campMemb:camLst){
campMap.put(campMemb.Id,campMemb);
}
for(CampaignMember campMember:cMemberPhoneList){
if(!campMap.containsKey(campMember.Id )&& campMember.Campaign.Type == 'Phone Team' && campMember.ContactId != null){
cMembPhoneDelList.add(campMember);
System.debug('====DeleteCampIDcleanPriority=====>'+campMember.Id);
}
}


system.debug('========cMembPhoneDelList=============='+cMembPhoneDelList);
return cMembPhoneDelList;
}*/
    // = If Contact Existing In higher Age Of Same Product remove from lower Age 
    public static List<CampaignMember> cleanAgeHrs(List<CampaignMember> cMemberPhoneList ,set<Id> contactIdSet, Map <Id,List<CampaignMember>> conCampMemMap){ 
        List<CampaignMember> cMembPhoneDelList = new List<CampaignMember>();
        List<List<CampaignMember>> cmList = new List<List<CampaignMember>>();
        set<CampaignMember> cmSet = new Set<CampaignMember>();
        Map<Id,CampaignMember> higestCMembMap = new Map<Id,CampaignMember>();
        //==========
        List<Campaign_Product_Heirarchy__c> cusSettinglst = new List<Campaign_Product_Heirarchy__c>([Select Id,Name,Priority__c from Campaign_Product_Heirarchy__c]);
        System.debug('==cusSettinglst=='+cusSettinglst);
        Map<String,Integer> cusSettingMap = new Map<String,Integer>();
        //Set<CampaignMember> cmSet = new Set<CampaignMember>();
        if(cusSettinglst != null && !cusSettinglst.isEmpty()){
            for(Campaign_Product_Heirarchy__c cusSetting : cusSettinglst){
                cusSettingMap.put(cusSetting.Name,Integer.valueof(cusSetting.Priority__c.trim()));
            }
        }
        system.debug('===cusSettingMapvalues===>>'+cusSettingMap.values());
        system.debug('===cusSettingMapKeyset===>>'+cusSettingMap.keySet());
        //==========
        system.debug('========contactIdSet========'+contactIdSet);
        for(Id conId:contactIdSet){
            //=======
            Integer i=0;
            Integer j=0;
            Map <String,String> cmHighestPrdMap = new Map <String,String>(); 
            
            //=======
            Map <String,List<CampaignMember>> cmMap = new Map <String,List<CampaignMember>>();
            Map<String,Decimal> prdAgeHrsMap = new Map<String,Decimal>();
            System.debug('=======Keycheck======='+conCampMemMap.containsKey(conId));
            if(conCampMemMap.containsKey(conId)){
                List<CampaignMember> camList = conCampMemMap.get(conId);
                system.debug('====camList=====>>'+camList);
                if(camList != null && !camList.isEmpty()){
                    //=======
                    for(CampaignMember cm : camList){
                        if(cm.Campaign.Type == 'Phone Team' && cm.Campaign.Name.Contains('Buy')){
                            // system.debug('===Product==>>'+cusSettingMap.get(cm.Campaign.Product__c));
                            if(j==0){
                                i= cusSettingMap.get(cm.Campaign.Product__c);
                                cmHighestPrdMap.put('1',cm.Campaign.Product__c);
                                j++;
                                
                            }
                            if(i<cusSettingMap.get(cm.Campaign.Product__c)){
                                cmHighestPrdMap.put('1',cm.Campaign.Product__c);
                            }
                        }
                    }
                    System.debug('=======Higest Product====='+cmHighestPrdMap.get('1'));
                    //========
                    //*****
                    List<CampaignMember> higestCMembList = new List<CampaignMember>();
                    
                    if(camList != null && !camList.isEmpty()){
                        for(CampaignMember cm : camList){
                            if(cm.Campaign.Type == 'Phone Team' && !cm.Campaign.Name.Contains('Lead') && cm.Campaign.Product__c == cmHighestPrdMap.get('1')){
                                higestCMembList.add(cm);
                            }
                        }
                    }
                    System.debug('======higestCMembList========'+higestCMembList);
                    if(higestCMembList != null && !higestCMembList.isEmpty()){
                        for(CampaignMember cm : camList){
                            if(cm.Campaign.Type == 'Phone Team' && cm.Campaign.Name.Contains('Lead') && cm.Campaign.Product__c == cmHighestPrdMap.get('1')){
                                higestCMembMap.put(cm.Id,cm);
                            }
                        }
                    }
                    System.debug('======higestCMembMap========'+higestCMembMap.keySet());
                    
                    //******
                    
                    for(CampaignMember cmMem : camList){
                        if(cmMem.Campaign.Type == 'Phone Team' && !cmMem.Campaign.Name.Contains('Lead') && cmMem.Campaign.Product__c == cmHighestPrdMap.get('1')){
                            system.debug('====Product=====>>'+cmMem.Campaign.Product__c+'====ProductKey=====>>'+prdAgeHrsMap.containsKey(cmMem.Campaign.Product__c));
                            if(!prdAgeHrsMap.containsKey(cmMem.Campaign.Product__c) ){
                                system.debug('====if======');
                                system.debug('====put======'+cmMem.Campaign.Product__c+'========'+cmMem.Id);
                                prdAgeHrsMap.put(cmMem.Campaign.Product__c ,cmMem.Age_In_Hours__c);
                                cmMap.put(cmMem.Campaign.Product__c,new List<CampaignMember>{cmMem});
                            }
                            
                            else if(prdAgeHrsMap.containsKey(cmMem.Campaign.Product__c)){
                                system.debug('====Else if======');
                                system.debug('====cmMem.Age_In_Hours__c======'+cmMem.Age_In_Hours__c);
                                system.debug('====prdAgeHrs======'+prdAgeHrsMap.get(cmMem.Campaign.Product__c));
                                system.debug('====cmMem.Age_In_Hours__c======'+cmMem.Age_In_Hours__c);
                                if(cmMem.Age_In_Hours__c > prdAgeHrsMap.get(cmMem.Campaign.Product__c)){
                                    system.debug('====Else if if======');
                                    system.debug('====put======'+cmMem.Campaign.Product__c+'========'+cmMem.Id);
                                    cmMap.put(cmMem.Campaign.Product__c,new List<CampaignMember>{cmMem});
                                }
                            }
                        }
                        
                    }
                    System.debug('=====Allvalues===='+cmMap.values());
                    if(cmMap.values()!= null){
                        cmList.addAll(cmMap.values());
                    }
                }
                
            }
        }
        for(List<CampaignMember> cmLst : cmList){
            cmSet.addAll(cmLst);
        }
        System.debug('====cmSet====>>>'+cmSet);
        List<CampaignMember> camLst = new List<CampaignMember>();
        camLst.addAll(cmSet);
        System.debug('====camLst====>>>'+camLst);
        Map<Id,CampaignMember> campMap = new Map<Id,CampaignMember>();
        for(CampaignMember campMemb:camLst){
            campMap.put(campMemb.Id,campMemb);
        }
        System.debug('====campMap======'+campMap.values());
        
        for(CampaignMember campMember:cMemberPhoneList){
            if(!campMap.containsKey(campMember.Id ) && campMember.Campaign.Type == 'Phone Team' && campMember.ContactId != null && !campMember.Campaign.Name.Contains('Lead') ){
                cMembPhoneDelList.add(campMember);
                System.debug('====DeleteCampIDAgeHrsBuy=====>'+campMember.Id);
            }
            else if(higestCMembMap.containsKey(campMember.Id ) && campMember.Campaign.Type == 'Phone Team' && campMember.ContactId != null && campMember.Campaign.Name.Contains('Lead')){
                cMembPhoneDelList.add(campMember);
                System.debug('====DeleteCampIDAgeHrslead=====>'+campMember.Id);
            }
        }
        return cMembPhoneDelList;
    }
    public static List<CampaignMember> cleanCoachingCampaign(List<CampaignMember> cMemberPhoneList ,set<Id> contactIdSet, Map <Id,List<CampaignMember>> conCampMemMap){ 
        List<CampaignMember> cMembPhoneDelList = new List<CampaignMember>();
        try{
        System.debug('----cMemberPhoneList----'+cMemberPhoneList);
        List<Contact> conList = new List<Contact>();
        if(contactIdSet != null && !contactIdSet.isEmpty() ) {
            conList = [Select id,Name,Product__c,Highest_Product__c,Website_Highest_Product__c from Contact where Id =: contactIdSet];
            System.debug('--conList---'+conList);
        }
        if(cMemberPhoneList != null && !cMemberPhoneList.isEmpty() && conList != null && !conList.isEmpty()){
            for(CampaignMember camp : cMemberPhoneList) {
                for(Contact con : conList) {
                    System.debug('-conid--'+con.id+'---campConta--'+camp.ContactId);
                    if(camp != null &&((con.Website_Highest_Product__c != null && con.Website_Highest_Product__c != '') || (con.Highest_Product__c != null && con.Highest_Product__c != '' ))&& camp.ContactId != null  && camp.ContactId == con.id && camp.Campaign.Name.contains('Coach')  && ( con.Highest_Product__c == 'Coaching' ||  con.Website_Highest_Product__c == 'Coaching')){
                        cMembPhoneDelList.add(camp);
                    } 
                }
            }
        }
        System.debug('---------cMembPhoneDelList-------'+cMembPhoneDelList);
        }
        catch (Exception e){
            ApexDebugLog apex1 = new ApexDebugLog();
apex1.createLog( new ApexDebugLog.Error('cleanCoachingCampaign','PhoneCampaignClean','CleanupJob',e));
System.debug('***Get Exception***'+e);
        }
        return cMembPhoneDelList;
    }
}