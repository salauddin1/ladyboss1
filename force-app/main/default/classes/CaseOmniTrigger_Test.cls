@isTest(seealldata = true)
public class CaseOmniTrigger_Test {
    @IsTest
    static void isInsert(){
        Case cs = new Case();
        cs.OwnerId = '00Gf4000003VtppEAC';
        cs.SuppliedEmail = 'grant@ladyboss.com';
        cs.Origin = 'Email';
        Test.startTest();
        insert cs;
        Test.stopTest();
        
    }
    @IsTest
    static void isUpdate(){
        Case cs = [Select id,routed_from__c from case where SuppliedEmail='grant@ladyboss.com' And Routed_From__c=  null limit 1];
        cs.Routed_From__c = '00Gf4000003VtppEAC';
        Test.startTest();
        update cs;
        Test.stopTest();
        
    }
}