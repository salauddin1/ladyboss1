@isTest
public class AddressTriggerHandler_Test {
     static testMethod void myTest() {
       Test.startTest();
         Account ac = new Account();
         ac.Name='Test';
         ac.Credit_Card_Number__c='123444';
         ac.Exp_Month__c='10';
         ac.Exp_Year__c=1234;
         insert ac;
         
         Contact con = new Contact();
         con.AccountId =ac.id;
         con.LastName ='test';
         con.email='test@TEST.COM';
         insert con;
         
         List<Address__c> adL = new List<Address__c>();
         Address__c ad = new Address__c();
         ad.contact__c = con.id;
         ad.Primary__c =true;
         Address__c ad1= new Address__c();
         ad1.contact__c = con.id;
         ad1.Primary__c =true;
         
         adL.add(ad);
         adL.add(ad1);
         insert adL;
         Address__c adds =[select id,Primary__c  from Address__c where id =: ad1.id];
         adds.Primary__c =true;
         update adds ;
         
         
         
         
         test.stopTest();
     }
}