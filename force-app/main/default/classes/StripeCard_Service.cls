@RestResource(urlMapping='/Stripe_CardService')
global class StripeCard_Service {
    @HttpPost
    global static void createCard() {
        String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
        Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'Stripe_CardService');
        System.debug('-------timestamppp----'+timestamppp);
        System.debug('-------sigFromHmac----'+sigFromHmac);
                             
        if(sigFromHmac && sigFromHmac == true || Test.isRunningTest() ){
        Stripe_webservice_setting__c op = [select id,StripeCard_Service__c,StripeCardDelete_Service__c,StripeCustomer_Service__c from Stripe_webservice_setting__c];
        string CustomerIde;
        if(RestContext.request.requestBody!=null && op.StripeCard_Service__c){
            try {
                
                String str = RestContext.request.requestBody.toString();
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                
                Map<String, Object> data = new map<String, Object>();
                List<String> fieldNames = new List<String>();
                Map<String, Object> account = (Map<String, Object>)results.get('data');
                Map<String,Object> strList =(Map<String,Object>)account.get('object');
                Map<String,Object> strMetadata = (Map<String,Object>)strList.get('metadata');
                Boolean isSource = false;
                Map<String,Object> cardStripe =new Map<String,Object>();
                Map<String,Object> owner =new Map<String,Object>();
                Map<String,Object> addressStripe =new Map<String,Object>();
                if (String.valueOf(strList.get('object')) == 'source') {
                    isSource = true;
                    cardStripe =(Map<String,Object>)strList.get('card');
                    owner =(Map<String,Object>)strList.get('owner');
                    addressStripe =(Map<String,Object>)owner.get('address');
                }
                String customerId = String.Valueof(strList.get('customer'));
                List<Stripe_Profile__c> stripeProfList = [select id ,Stripe_Customer_Id__c,Customer__r.id from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId and Stripe_Customer_Id__c !=null];
                SET<Id> setprofId = new SET<Id> ();
               System.debug('----------stripeProfList -------'+stripeProfList );
                Map<string,string> conStripeMap = new Map<string,string> ();
                 Map<string,string> conStripeMap1 = new Map<string,string> ();
                for(Stripe_Profile__c con : stripeProfList ){
                    conStripeMap.put(con.Stripe_Customer_Id__c,con.id );
                    if(con.Customer__c!= null)
                    conStripeMap1.put(con.Stripe_Customer_Id__c,con.Customer__r.id );
                    setprofId.add(con.id);
 
                }
                Customeride=String.valueOf(setprofId);
               List<Card__c> listOfExistingCard = [select id,Stripe_Card_Id__c from card__c where Stripe_Profile__c in : setprofId];
               Map<string,string> cardExistsMap = new Map<string,string> ();
               for(card__c cd : listOfExistingCard ){
                    if(cd .Stripe_Card_Id__c != null)
                    cardExistsMap.put(cd.Stripe_Card_Id__c ,cd.Stripe_Card_Id__c );
                    
                }
                
                System.debug('---strList -----'+strList ); 
                 System.debug('---strList -----'+conStripeMap1); 
                
                
                if(string.Valueof(strMetadata.get('Integration Initiated From'))!='Salesforce' || Test.isRunningTest()) {
                    Card__c crd = new Card__c ();
                    List<Card__c> lstCard = new List<Card__c>();
                    
                    crd.Stripe_Profile__c = conStripeMap.get(String.Valueof(strList.get('customer')));
                     System.debug('---strList -----'+ conStripeMap1.get(String.Valueof(strList.get('customer'))));
                      System.debug('---customer-----'+ String.Valueof(strList.get('customer')));
                    crd.Contact__c = conStripeMap1.get(String.Valueof(strList.get('customer')));
                    
                    crd.CustomerID__c = String.Valueof(strList.get('customer'));
                    if(strList.equals('object')) {
                        //crd.Card_Type__c = String.Valueof(strList.get(strList));
                    }
                    crd.Billing_Zip_Postal_Code__c = isSource?String.Valueof(addressStripe.get('postal_code')):String.Valueof(strList.get('address_zip'));
                    
                    
                    crd.Card_ID__c = String.Valueof(strList.get('id'));
                    
                    crd.Cvc_Check__c = isSource?String.Valueof(cardStripe.get('cvc_check')):String.Valueof(strList.get('cvc_check'));
                    
                    
                    crd.Billing_Street__c = isSource?String.Valueof(addressStripe.get('line1')):String.Valueof(strList.get('address_line1'));
                    crd.Billing_Country__c = isSource?String.Valueof(addressStripe.get('country')):String.Valueof(strList.get('address_country'));
                    crd.Billing_City__c = isSource?String.Valueof(addressStripe.get('city')):String.Valueof(strList.get('address_city'));
                    crd.Expiry_Year__c = isSource?String.Valueof(cardStripe.get('exp_year')):String.Valueof(strList.get('exp_year'));
                    crd.Billing_State_Province__c=isSource?String.Valueof(addressStripe.get('state')): String.Valueof(strList.get('address_state'));
                    crd.Stripe_Card_Id__c = String.Valueof(strList.get('id'));
                    crd.Last4__c = isSource?String.Valueof(cardStripe.get('last4')):String.Valueof(strList.get('last4'));
                    
                    
                    if(((isSource?Integer.Valueof(cardStripe.get('exp_month')):Integer.Valueof(strList.get('exp_month')))==11 ) || (isSource?Integer.Valueof(cardStripe.get('exp_month')):Integer.Valueof(strList.get('exp_month')))==12){
                        crd.Expiry_Month__c = isSource?String.Valueof(cardStripe.get('exp_month')):String.Valueof(strList.get('exp_month'));
                    }else
                    {
                        crd.Expiry_Month__c = '0'+(isSource?String.Valueof(cardStripe.get('exp_month')):String.Valueof(strList.get('exp_month')));
                        
                    }
                    
                    crd.Name_On_Card__c = isSource?String.Valueof(cardStripe.get('name')):String.Valueof(strList.get('name'));
                    
                    crd.Brand__c= isSource?String.Valueof(cardStripe.get('brand')):String.Valueof(strList.get('brand'));
                    if(crd.Brand__c!=null){
                        if(crd.Brand__c.contains('Visa') || crd.Brand__c.contains('MasterCard') ||crd.Brand__c.contains('Discover') || crd.Brand__c.contains('JCB')){
        	                crd.Credit_Card_Number__c = '000000000000'+crd.Last4__c;
                    	}
	                    else if(crd.Brand__c.contains('American Express')){
    	                    crd.Credit_Card_Number__c = '00000000000'+crd.Last4__c;
            	        }
                	    else if(crd.Brand__c.contains('Diners Club')){
                    	    crd.Credit_Card_Number__c = '0000000000'+crd.Last4__c;
                    	}
                    }
                    
                    crd.Initiated_From__c = 'Stripe';
                    
                    //List<card__c> lstCardExst = [select id from card__c where Stripe_Card_Id__c =: String.Valueof(strList.get('id'))];
                    //System.debug('---lstCardExst-------'+lstCardExst);
                    System.debug('---String.Valueof(strList.get())-------'+String.Valueof(strList.get('id')));
                    
                    if(!cardExistsMap.ContainsKey(String.Valueof(strList.get('id')))|| test.isRunningTest()){
                        insert crd;
                        
                    }
                    if(crd.id!=null && crd.Stripe_Card_Id__c!=null){
                           Set<string> cardIdSet = new Set<String>();
                           Set<string> invoiceIdSet = new Set<String>();
                           List<Card_Opp_Temporary__c > cdOppList =[select id,CardId__c,Invoice_Id__c from Card_Opp_Temporary__c  where CardId__c!=null and CardId__c=:crd.Stripe_Card_Id__c ];
                            if(cdOppList.size() > 0){
                                 for(Card_Opp_Temporary__c  cp : cdOppList ) {
                                    if(cp.CardId__c!=null){
                                        cardIdSet.add(cp.CardId__c);
                                        invoiceIdSet.add(cp.Invoice_Id__c );
                                    }
                                    
                                 }
                            }
                           
                           list<opportunity> oppList = [select id from opportunity where (CardId__c=:String.Valueof(strList.get('id')) and CardId__c!=null) OR (CardId__c IN :cardIdSet) OR (Invoice_Id__c !=null and Invoice_Id__c IN :invoiceIdSet)];
                            if(oppList.size() > 0){
                                for(opportunity opObj : oppList ) {
                                    opObj.card__c = crd.id;
                                }
                                update oppList ;
                            }
                    }
                    List<Address__c> aList = new List<Address__c> ();
                    if((crd.Billing_City__c !=null && crd.Billing_City__c != '' ) || (crd.Billing_State_Province__c!=null && crd.Billing_State_Province__c != '') ||  (crd.Billing_Street__c!=null && crd.Billing_Street__c!= '') || (crd.Billing_Zip_Postal_Code__c!=null && crd.Billing_Zip_Postal_Code__c!= '' ) || (crd.Billing_Country__c!=null && crd.Billing_Country__c!= ''))
                    {
                    Address__c addr = new Address__c();
                    addr.Contact__c = conStripeMap1.get(String.Valueof(strList.get('customer')));
                    addr.CustomerID__c = String.Valueof(strList.get('customer'));
                    addr.Shipping_City__c =crd.Billing_City__c;
                    addr.Shipping_State_Province__c = crd.Billing_State_Province__c;
                    addr.Shipping_Street__c =crd.Billing_Street__c;
                    addr.Shipping_Zip_Postal_Code__c = crd.Billing_Zip_Postal_Code__c;
                    addr.Shipping_Country__c= crd.Billing_Country__c;
                     
                    if((crd.Billing_City__c !=null && crd.Billing_City__c != '' ) && (crd.Billing_State_Province__c!=null && crd.Billing_State_Province__c != '') &&  (crd.Billing_Street__c!=null && crd.Billing_Street__c!= '') && (crd.Billing_Zip_Postal_Code__c !=null && crd.Billing_Zip_Postal_Code__c!= '' ) && (crd.Billing_Country__c!=null && crd.Billing_Country__c!= ''))
                                        {
                     addr.Primary__c =true;
                    }
                   
                    aList.add(addr);
                    }
                        
                    if(aList.size() > 0) {
                        insert aList;
                    }
                } 
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeCard_Service',
                 'createCard',
                 CustomerIde
                 )
                 ); 
                if(test.isRunningTest()){
                    integer i = 1/0;
                }
                    
                }

                
       
            catch(Exception ex) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                 ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                new ApexDebugLog.Error(
                 'StripeCard_Service',
                 'createCard',
                 CustomerIde,
                     ex
                 )
                 );
            }    
            
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
    }
    
}