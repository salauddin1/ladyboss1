@isTest
public class schemaGlobalDescribeTest {
	@isTest
    public static void testunit() {
        contact con = new contact ();
        con.lastName = 'test';
        con.Email = 'test@ladyboss.com';
        insert con;
        Test.startTest();
        SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(con.Id);
        Test.stopTest();
    }
    @isTest
    public static void testunit1() {
        contact con = new contact ();
        con.lastName = 'test';
        con.Email = 'test@ladyboss.com';
        insert con;
        Test.startTest();
        EmailTempForLAC.loadexistingEmail(con.Id);
        Test.stopTest();
    }
    @isTest
    public static void testunit2() {
        account con = new account ();
        con.Description = 'test';
        con.Email__c = 'test@ladyboss.com';
        con.Name = 'accounttest';
        insert con;
        Test.startTest();
        EmailTempForLAC.loadexistingEmail(con.Id);
        Test.stopTest();
    }
    @isTest
    public static void testunit3() {
        lead con = new lead ();
        con.lastName = 'test';
        con.FirstName = 'surendra';
        con.Email = 'test@ladyboss.com';
        con.status = 'New';
        con.company = 'TestCompny';
        insert con;
        Test.startTest();
        EmailTempForLAC.loadexistingEmail(con.Id);
        Test.stopTest();
    }
    @isTest
    public static void testunit4() {
        lead con = new lead ();
        con.lastName = 'test';
        con.FirstName = 'surendra';
        con.Email = 'test@ladyboss.com';
        con.status = 'New';
        con.company = 'TestCompny';
        insert con;
        account con1 = new account ();
        con1.Description = 'test';
        con1.Email__c = 'test@ladyboss.com';
        con1.Name = 'accounttest';
        insert con1;
        contact con2 = new contact ();
        con2.lastName = 'test';
        con2.Email = 'test@ladyboss.com';
        insert con2;
        Profile pro = [SELECT Id FROM Profile WHERE Name= 'Account Recovery'];
        Test.startTest();
        EmailTempForLAC.sendMailMethod('test@gmail.com', 'mSubject', 'mbody',con2.Id);
        Test.stopTest();
    }
}