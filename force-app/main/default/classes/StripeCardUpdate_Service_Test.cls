@isTest
public class StripeCardUpdate_Service_Test{
    
      public static TestMethod void testGetCallout(){
      Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardUpdate_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
           system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
          
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_CardUpdateService'; 
          String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":1,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        req.requestBody = Blob.valueOf(body);
          req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
         
         StripeCardUpdate_Service.updateCard();
         
   Test.stopTest(); 
        
    }
     public static TestMethod void testGetCallout1(){
      Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCardUpdate_Service__c = true;
    insert op;
           system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
          Card_Opp_Temporary__c cd = new Card_Opp_Temporary__c();
          cd.Invoice_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          cd.CardId__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          insert cd;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_CardUpdateService'; 
          String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":12,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        req.requestBody = Blob.valueOf(body);
         req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
         
        StripeCardUpdate_Service.updateCard();
         
   Test.stopTest(); 
        
    }
    
}