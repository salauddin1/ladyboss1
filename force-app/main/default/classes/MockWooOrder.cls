global class MockWooOrder implements HttpCalloutMock{
//{ "id": "re_1DajDDBwLSk1v1ohKVEO0G3M", "object": "refund", "amount": 7690, "balance_transaction": "txn_1DajDDBwLSk1v1ohJs5g6o2B","charge": "ch_1DafZmBwLSk1v1ohUH1yKzvh","created": 1543235355,"currency": "usd", "metadata": { "Integration Initiated From": "Salesforce" }, "reason": "duplicate", "receipt_number": null, "source_transfer_reversal": null, "status": "succeeded", "transfer_reversal": null}
 global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id": "re_1DajDDBwLSk1v1ohKVEO0G3M", "object": "refund", "amount": 7690, "balance_transaction": "txn_1DajDDBwLSk1v1ohJs5g6o2B","charge": "ch_1DafZmBwLSk1v1ohUH1yKzvh","created": 1543235355,"currency": "usd", "metadata": { "Integration Initiated From": "Salesforce" }, "reason": "duplicate", "receipt_number": null, "source_transfer_reversal": null, "status": "succeeded", "transfer_reversal": null}');
        res.setStatusCode(200);
        return res;
 }
}