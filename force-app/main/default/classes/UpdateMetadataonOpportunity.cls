global class UpdateMetadataonOpportunity implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT CreatedDate,Id,Tax__c,Tax_Percentage__c,Core_Amount__c,Charge_Description__c,Stripe_Charge_Id__c FROM Opportunity WHERE Initiated_From__c = \'Stripe\' AND CreatedDate >= 2020-01-01T07:00:00.000Z AND RecordType.Name = \'Charge\' AND Tax__c = null AND Stripe_Charge_Id__c != null');
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        String recordId = '';
        try{
            List<Opportunity> oppList = new List<Opportunity>();
            for (Opportunity opp : scope) {
                if (opp.Charge_Description__c == null ||(opp.Charge_Description__c != null && !opp.Charge_Description__c.contains('Invoice'))) {
                    oppList.add(opp);
                }
            }
            if (oppList.size()>0) {
                for (Opportunity opp : oppList) {
                    StripeCharge sc = StripeCharge.getCharge(opp.Stripe_Charge_Id__c);
                    if (sc.metadata!=null) {
                        if (sc.metadata.containsKey('Tax')) {
                            opp.Tax__c = Decimal.valueOf(sc.metadata.get('Tax'));
                            opp.Tax_Percentage__c = Decimal.valueOf(sc.metadata.get('TaxPercentage'));
                            opp.Core_Amount__c = Decimal.valueOf(sc.metadata.get('Core Amount'));
                        }
                    }
                }
                update oppList;
            }
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
        }catch (Exception e) {
            system.debug('e.getMessage()-->'+e.getMessage());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'UpdateMetadataonOpportunity',
                    'Batch',
                    recordId,
                    e
                )
            );
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}