@isTest
public class HubAccountSetup_Test {
    
    @isTest
    static void AccountSetup(){
        system.Test.setMock(HttpCalloutMock.class, new MockUpdateCard());
        Contact c = new Contact();
        c.FirstName = 'test';
        c.LastName = '1';
        c.Stripe_Customer_Id__c = 'cus_FNX1BAChcIwUCW';
        c.Email = 'test@gmail.com';
        insert c;
        
        Stripe_Profile__c stripe = new Stripe_Profile__c();
        stripe.customer__c = c.id;
        stripe.Stripe_Customer_Id__c = 'cus_FNZ1BAChcIwUCW';
        insert stripe;
        
        Address__c add = new Address__c();
        add.Shipping_City__c = 'ahmedabad';
        add.Shipping_Country__c = 'india';
        add.Shipping_State_Province__c = 'gujarat';
        add.Shipping_Street__c = 'testadd';
        add.Shipping_Zip_Postal_Code__c = '380061';
        add.Primary__c = true;
        add.Contact__c = c.id;
        insert add;
        
        Card__c card = new Card__c();
        card.Last4__c = '1234';
        card.Contact__c = c.id;
        card.Stripe_Profile__c = stripe.id;
        card.Stripe_Card_Id__c = 'card_1F9sUcFzCf73siP0QBlTSQtO';
        insert card;
        
        Opportunity opp = new Opportunity();
        opp.Card__c = card.id;
        opp.Name = 'test';
        opp.StageName = 'Closed Won';
        opp.CloseDate = system.today();
        insert opp;
        
        Test.startTest();
        HubAccountSetup.updateInfoUser(c.id, c.firstname, c.lastName, c.phone, add.Shipping_Street__c, add.Shipping_City__c, add.Shipping_State_Province__c, add.Shipping_Country__c, add.Shipping_Zip_Postal_Code__c);
        HubAccountSetup.getcontact(c.id);
        HubAccountSetup.getAddress(c.id);
        HubAccountSetup.getAllCards(c.id);
        HubAccountSetup.AddShipping('Street',card.Id,'City','State', 'PostalCode', 'Country',c.id);
        HubAccountSetup.getCardSubscription(card.Id,c.Id);
        Test.stopTest();
    }
    
    @istest
    static void AccountSetup2(){
        Contact c = new Contact();
        c.FirstName = 'test';
        c.LastName = '1';
        c.Stripe_Customer_Id__c = 'cus_FNX1BAChcIwUCW';
        c.Email = 'test@test.com';
        insert c;
        
        Card__c card = new Card__c();
        card.Last4__c = '1234';
        card.Contact__c = c.id;
        card.Stripe_Card_Id__c = 'card_1F9sUcFzCf73siP0QBlTSQtO';
        insert card;
        
        Address__c add = new Address__c();
        add.Shipping_City__c = 'ahmedabad';
        add.Shipping_Country__c = 'india';
        add.Shipping_State_Province__c = 'Gj';
        add.Shipping_Street__c = 'testadd';
        add.Shipping_Zip_Postal_Code__c = '380061';
        add.Contact__c = c.id;
        insert add;
        
        Stripe_Profile__c stripe = new Stripe_Profile__c();
        stripe.customer__c = c.id;
        stripe.Stripe_Customer_Id__c = 'cus_FNZ1BAChcIwUCW';
        insert stripe;
        
        Test.startTest();
        HubAccountSetup.updateInfoUser(c.id, c.firstname, c.lastName, c.phone, add.Shipping_Street__c, add.Shipping_City__c, add.Shipping_State_Province__c, add.Shipping_Country__c, add.Shipping_Zip_Postal_Code__c);
        HubAccountSetup.getcontact(c.id);
        HubAccountSetup.getAddress(c.id);
        HubAccountSetup.getCardSubscription(card.Id,c.Id);
        HubAccountSetup.getAllCards(c.id);
        Test.stopTest();
    }
    
}