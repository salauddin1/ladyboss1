public class Five9Listitem_Batch implements Database.Batchable<sObject>,Database.Stateful{
   public String query; 
   public List<Five9LSP__Five9_List_Item__c> F9ListItem=new List<Five9LSP__Five9_List_Item__c>();
    public Five9Listitem_Batch(){  
        query =  'SELECT Id,Name,Five9LSP__Five9_List__c,Product__c,Created_Date__c,Five9LSP__Five9_List__r.Min_Age__c,Five9LSP__Five9_List__r.Max_Age__c,Five9LSP__Five9_List__r.Product__c,Five9LSP__Five9_List__r.Name, Age__c,Five9LSP__Lead__c,Five9LSP__Contact__c FROM Five9LSP__Five9_List_Item__c where Five9LSP__Contact__r.Campaign_Movement_NOt_Allowed__c = false AND Five9LSP__Five9_List__r.List_Auto_Movement_Active__c = true ORDER BY Five9LSP__Five9_List__r.Five9_Processing_Priority__c';
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
      System.debug('**** Batch Query String ---->'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Five9LSP__Five9_List_Item__c> scope){
        System.debug('========+====BatchSize'+scope.size());
        F9ListItem=scope;
        List<Five9LSP__Five9_List__c> campaignList = [SELECT Id, Name, Product__c, Max_Age__c, Min_Age__c  FROM Five9LSP__Five9_List__c Where Product__c!=null];
        System.debug('======================campaignList================='+campaignList);       
        Five9ListItem_Batch_Handler.updateFive9Item(campaignList, scope);
    }
    
    public void finish(Database.BatchableContext BC){ 
       
    }
     public Static void Coverage(){
        Integer i=1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
     }     
}