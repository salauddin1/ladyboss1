@isTest
private class UpdateSubscriptionChargeDateTest {
	@testSetup 
    static void setup() {
                ContactTriggerFlag.isContactBatchRunning=true; 
    	Contact con = new Contact(LastName='test');
    	insert con;
    	Stripe_Profile__c sp = new Stripe_Profile__c(Customer__c = con.id, Stripe_Customer_Id__c = 'cus_000000000');
    	insert sp;
    	List<Product2> prodList = new List<Product2>();
    	for (Integer i=0;i<9;i++) {
    		prodList.add(new Product2(
				Name = 'Test Product', 
				IsActive = true,
                Price__c = 100,
				Stripe_Plan_Id__c='New York '+i
			));
    	}
    	insert prodList;
    	Pricebook2 standardPricebook = new Pricebook2(
			Id = Test.getStandardPricebookId(),
			IsActive = true
		); 
        update standardPricebook;
        List<PricebookEntry> pricebookList = new List<PricebookEntry>();
        for (Product2 sObj: prodList) {
        	pricebookList.add(new PricebookEntry(
				Pricebook2Id = standardPricebook.Id,
				Product2Id = sObj.Id,
				UnitPrice = 100,
				IsActive = true
			));
        }
        insert pricebookList;
        List<Subscription__c> accounts = new List<Subscription__c>();        
        for (Integer i=0;i<10;i++) {
            accounts.add(new Subscription__c(Customer_Id__c='cus_000000000', 
                Plan_ID__c='New York '+i, Subscription_Id__c='USA '+i,Is_Processed__c=false,Period_Start__c = datetime.newInstance(2014, 9, 15, 12, 30, 0),
				Period_End__c = datetime.newInstance(2014, 9, 15, 13, 30, 0)));
        }
        insert accounts;  
        
    }
	
	@isTest static void test_method_one() {
		Test.startTest();
        UpdateSubscriptionChargeDate uca = new UpdateSubscriptionChargeDate(1590649200);
        Id batchId = Database.executeBatch(uca);
        Test.stopTest();
	}	
}