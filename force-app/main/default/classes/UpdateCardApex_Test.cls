@istest(seeAlldata = true)
public class UpdateCardApex_Test {
    public static testMethod void test(){
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        contact co = [select id from contact where email='sukesh@ladyboss.com'];
        /*contact co = new contact();
co.LastName ='vijay';
co.Stripe_Customer_Id__c='cus_E1J7eqtfNLBXuI';
co.email = 'sukesh3089__test233@gmail.com';
co.firstName= 'test';
insert co;*/
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_E3vxtf5JjMiXv6';
        sp.Customer__c =  co.id;
        insert sp;
        
        list<Card__c> cardlist = new list<Card__c>();
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        cd.Contact__c=co.Id;
        cd.Expiry_Month__c = '12';
        cd.Expiry_Year__c ='2018';
        cd.Credit_Card_Number__c='4242424242424242';
        cd.Stripe_Profile__c = sp.id;
        insert cd;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        opp.Card__c  = cd.id;
        opp.RecordTypeId = devRecordTypeId;
        insert opp;
        Case cs = new Case();
        
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = '3454535';
        cs.Invoice_Id__c = '3454535';
        cs.Subject = 'Failed payment';
        cs.ContactId = co.id;
        //     cs.WhoId = lstStripeProf[0].Customer__c;
        
        cs.IsFailed_Payment__c =true;
        cs.Contact_Phone__c = '9903294994';
        
        cs.Contact_Email__c = 'test@twst.com';
        cs.IsFailed_Payment__c =true;
        insert cs;
        cardList.add(cd);
        test.startTest();
        UpdateCardApex.getCards(cs.Id);
        UpdateCardApex.sendEmail(cd.Id,'sukesh3089@gmail.com','cus_E1J7eqtfNLBXuI','4231',cs.id,'');
        UpdateCardApex.sendSMS(cd.Id,'sukesh3089@gmail.com','cus_E1J7eqtfNLBXuI','4231',cs.id,'');
        
        UpdateCardApex.getCardsOpportunity(cs.Id);
        
        test.stopTest();
    }
}