/*
* Devloper : Sourabh Badole
* Description : This Handler class is useing to link Opportunity to Campaign by the help of Campaign Member.
*/
public class CampaignConnectionHandler {
    public static void getCampaignConnection(List<Opportunity> newTriggerVal){
        try{
            if(newTriggerVal != null && !newTriggerVal.isEmpty()){
                Set<Id> conIdSet = new Set<Id>();
                for(Opportunity opp : newTriggerVal){
                    if(opp.Contact__c != null ){
                        conIdSet.add(opp.Contact__c);
                        System.debug('test opp.Contact__c ===== '+opp.Contact__c);
                    }    
                }
                List <CampaignMember> camMembist = new List<CampaignMember>();
                if(conIdSet != null && !conIdSet.isEmpty()){
                    camMembist = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where ContactId =: conIdSet and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null ];
                    System.debug('test camMembist ===== '+camMembist);
                }
                Map<id,String> campMembMap =  new Map<id,String>();
                if(camMembist != null && !camMembist.isEmpty()){
                    for(CampaignMember camp : camMembist){
                        if(camp != null){
                            if(camp.ContactId != null &&   camp.Campaign.Name != null &&   camp.Campaign.Name != ''){
                                if(campMembMap != null && !campMembMap.keySet().isEmpty() && campMembMap.keySet().contains(camp.ContactId)){
                                    String campHolder = '';
                                    campHolder  = campMembMap.get(camp.ContactId);
                                    if(campHolder != null & campHolder != ''){
                                        campHolder = campHolder+', '+ camp.Campaign.Name ;
                                        campMembMap.put(camp.ContactId , campHolder);    
                                        System.debug('test  camp.Campaign.Name 1 ===== '+ camp.Campaign.Name);
                                    }
                                    System.debug('test  camp.Campaign.Name 1===== '+ camp.Campaign.Name);
                                    System.debug('test  camp.Campaign.Name  ContactId 1 ===== '+ camp.ContactId);
                                }
                                else{
                                    campMembMap.put(camp.ContactId , camp.Campaign.Name);    
                                    System.debug('test  camp.Campaign.Name ===== '+ camp.Campaign.Name);
                                    System.debug('test  camp.Campaign.Name  ContactId ===== '+ camp.ContactId);
                                }
                            }
                        }
                    } 
                }
                if(campMembMap != null && !campMembMap.keySet().isEmpty()){
                    for(Opportunity opp : newTriggerVal){
                        if(opp.Contact__c != null ){
                            if(campMembMap.keySet().contains(opp.Contact__c)){
                                opp.Campaign__c = campMembMap.get(opp.Contact__c);
                                System.debug('test opp.Campaign ===== '+opp.Campaign__c);
                            }
                        }    
                    }  
                }
            }
        }
        catch(Exception Ex){
            String addId = '';
            if(newTriggerVal != null && !newTriggerVal.isEmpty()){
                for(Opportunity opp : newTriggerVal){ addId = opp.Contact__c +'   '+addId;}
            }
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('getCampaignConnection','CampaignConnectionHandler',addId ,ex));
            System.debug('***Get Exception***'+ex);
        }
    }
    
}