@IsTest
public class StripeListCharge_Test {

	static testMethod void testParse() {
		String json = '{ \"object\": \"list\", \"data\": [ { \"id\": \"ch_1Bnu6XDiFnu7hVq7FZC78Hpr\", \"object\": \"charge\", \"amount\": 2655, \"amount_refunded\": 0, \"application\": null, \"application_fee\": null, \"balance_transaction\": \"txn_1Bnu6XDiFnu7hVq7vBzOqKyf\", \"captured\": true, \"created\": 1516822817, \"currency\": \"usd\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"description\": null, \"destination\": null, \"dispute\": null, \"failure_code\": null, \"failure_message\": null, \"fraud_details\": {}, \"invoice\": \"in_1BntAHDiFnu7hVq7qGHrvQKO\", \"livemode\": false, \"metadata\": {}, \"on_behalf_of\": null, \"order\": null, \"outcome\": { \"network_status\": \"approved_by_network\", \"reason\": null, \"risk_level\": \"normal\", \"seller_message\": \"Payment complete.\", \"type\": \"authorized\" }, \"paid\": true, \"receipt_email\": null, \"receipt_number\": null, \"refunded\": false, \"refunds\": { \"object\": \"list\", \"data\": [], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/charges/ch_1Bnu6XDiFnu7hVq7FZC78Hpr/refunds\" }, \"review\": null, \"shipping\": null, \"source\": { \"id\": \"card_1BYf1vDiFnu7hVq7kXdTknjq\", \"object\": \"card\", \"address_city\": \"\", \"address_country\": \"United States\", \"address_line1\": \"\", \"address_line1_check\": null, \"address_line2\": null, \"address_state\": \"\", \"address_zip\": \"\", \"address_zip_check\": null, \"brand\": \"Visa\", \"country\": \"US\", \"customer\": \"cus_BwY8b7bUjxbtsl\", \"cvc_check\": null, \"dynamic_last4\": null, \"exp_month\": 2, \"exp_year\": 2022, \"fingerprint\": \"Xxv7pkxy4d1rU6JI\", \"funding\": \"credit\", \"last4\": \"4242\", \"metadata\": {}, \"name\": \"Grant Stripe\", \"tokenization_method\": null }, \"source_transfer\": null, \"statement_descriptor\": null, \"status\": \"succeeded\", \"transfer_group\": null }], \"has_more\": true, \"url\": \"/v1/charges\" }';
		StripeListCharge.Charges r = StripeListCharge.Charges.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCharge.Charges objJSON2Apex = new StripeListCharge.Charges(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.object_Z == null);
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.has_more == null);
		System.assert(objJSON2Apex.url == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCharge.Refunds objRefunds = new StripeListCharge.Refunds(System.JSON.createParser(json));
		System.assert(objRefunds != null);
		System.assert(objRefunds.object_Z == null);
		System.assert(objRefunds.data == null);
		System.assert(objRefunds.has_more == null);
		System.assert(objRefunds.total_count == null);
		System.assert(objRefunds.url == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCharge.Data objData = new StripeListCharge.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.id == null);
		System.assert(objData.object_Z == null);
		System.assert(objData.amount == null);
		System.assert(objData.amount_refunded == null);
		//System.assert(objData.application == null);
		//System.assert(objData.application_fee == null);
		System.assert(objData.balance_transaction == null);
		System.assert(objData.captured == null);
		System.assert(objData.created == null);
		System.assert(objData.currency_data == null);
		System.assert(objData.customer == null);
		System.assert(objData.description == null);
		//System.assert(objData.destination == null);
		//System.assert(objData.dispute == null);
		//System.assert(objData.failure_code == null);
		//System.assert(objData.failure_message == null);
		System.assert(objData.fraud_details == null);
		System.assert(objData.invoice == null);
		System.assert(objData.livemode == null);
		System.assert(objData.metadata == null);
		//System.assert(objData.on_behalf_of == null);
		//System.assert(objData.order == null);
		//System.assert(objData.outcome == null);
		System.assert(objData.paid == null);
		//System.assert(objData.receipt_email == null);
		//System.assert(objData.receipt_number == null);
		System.assert(objData.refunded == null);
		System.assert(objData.refunds == null);
		//System.assert(objData.review == null);
		//System.assert(objData.shipping == null);
		System.assert(objData.source == null);
		//System.assert(objData.source_transfer == null);
		System.assert(objData.statement_descriptor == null);
		System.assert(objData.status == null);
		//System.assert(objData.transfer_group == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCharge.Fraud_details objFraud_details = new StripeListCharge.Fraud_details(System.JSON.createParser(json));
		System.assert(objFraud_details != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripeListCharge.Source objSource = new StripeListCharge.Source(System.JSON.createParser(json));
		System.assert(objSource != null);
		System.assert(objSource.id == null);
		System.assert(objSource.object_Z == null);
		System.assert(objSource.address_city == null);
		System.assert(objSource.address_country == null);
		System.assert(objSource.address_line1 == null);
		System.assert(objSource.address_line1_check == null);
		System.assert(objSource.address_line2 == null);
		System.assert(objSource.address_state == null);
		System.assert(objSource.address_zip == null);
		System.assert(objSource.address_zip_check == null);
		System.assert(objSource.brand == null);
		System.assert(objSource.country == null);
		System.assert(objSource.customer == null);
		//System.assert(objSource.cvc_check == null);
		//System.assert(objSource.dynamic_last4 == null);
		System.assert(objSource.exp_month == null);
		System.assert(objSource.exp_year == null);
		System.assert(objSource.fingerprint == null);
		System.assert(objSource.funding == null);
		System.assert(objSource.last4 == null);
		System.assert(objSource.metadata == null);
		//System.assert(objSource.name == null);
		//System.assert(objSource.tokenization_method == null);
	}
}