@isTest
public class PaymentToComissionTriggerOPHandler_Test {
    public static testmethod void test1(){
        PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        System.debug(fm.id);
   		PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
         
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Senior_Agent__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
         
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
         
          Id pricebookId = Test.getStandardPricebookId();
         
         PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
         UnitPrice = 100, IsActive = true);
        insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
         
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice; 
       
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
         OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        ol1.Consider_For_Commission__c = true;
        insert oL1;
         
          Comission_Configuration__c config = new Comission_Configuration__c();
        config.Amount_Paid_to_Agent_Finalizing_Sale__c=1;
        config.Amount_Paid_to_Agent_Initiating_Sale__c=12;
        config.Amount_Paid_to_Director__c=23;
        config.Amount_Paid_to_Senior_Agent__c=23;
        config.Amount_Paid_to_Vice_President__c=12;
        config.Max_Value__c=100;
        config.Min_Value__c=0;
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        //config.Product__c = prod.id;
        config.Agent_paid_on__c = 0;
        config.Director_paid_on__c = 0;
        config.Vp_paid_on__c = 0;
        config.Special_agent_paid_on__c = 0;
        insert config;
         Comission_Configuration__c config1 = new Comission_Configuration__c();
        config1.Amount_Paid_to_Agent_Finalizing_Sale__c=1;
        config1.Amount_Paid_to_Agent_Initiating_Sale__c=12;
        config1.Amount_Paid_to_Director__c=23;
        config1.Amount_Paid_to_Senior_Agent__c=23;
        config1.Amount_Paid_to_Vice_President__c=12;
        config1.Max_Value__c=200;
        config1.Min_Value__c=101;
        config1.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config1.Percentage_Paid_to_Director__c=12;
        config1.Percentage_Paid_to_Vice_President__c=12;
        config1.Percentage_Paid_to_Senior_Agent__c = 0;
        config1.Agent_paid_on__c = 0;
        config1.Director_paid_on__c = 0;
        config1.Vp_paid_on__c = 0;
        config1.Special_agent_paid_on__c = 0;
        insert config1;
        test.startTest();
        Payment__c pay = new Payment__c();
        pay.Amount__c = 102;
        pay.Product__c = prod.Id;
        pay.week__c = 24;
        pay.Payment_Created_By__c=userinfo.getuserid();
        pay.Opportunity__c = op1.id;
        pay.OwnerId = op1.Sales_Person_Id__c;
        insert pay;
         PaymentToComissionTriggerOPHandler.comission(pay);
        test.stopTest();
    }   
     public static testmethod void test2(){
        PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
        first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
        System.debug(fm.id);         
        UserHierarchy__c uhier = new UserHierarchy__c();
        uhier.Agent__c = userinfo.getuserid();
        uhier.Director__c = userinfo.getuserid(); 
        uhier.Senior_Agent__c = userinfo.getuserid(); 
        uhier.Vice_President__c = userinfo.getuserid(); 
        insert uhier;
         
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
         
          Id pricebookId = Test.getStandardPricebookId();
         
         PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
         UnitPrice = 100, IsActive = true);
        insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
         
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice; 
       
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        ol1.Consider_For_Commission__c = true;
        insert oL1;
         
        Comission_Configuration__c config = new Comission_Configuration__c();
        config.Amount_Paid_to_Agent_Finalizing_Sale__c=1;
        config.Amount_Paid_to_Agent_Initiating_Sale__c=12;
        config.Amount_Paid_to_Director__c=23;
        config.Amount_Paid_to_Senior_Agent__c=23;
        config.Amount_Paid_to_Vice_President__c=12;
        config.Max_Value__c=100;
        config.Min_Value__c=0;
        config.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config.Percentage_Paid_to_Director__c=12;
        config.Percentage_Paid_to_Vice_President__c=12;
        config.Percentage_Paid_to_Senior_Agent__c = 0;
        //config.Product__c = prod.id;
        config.Agent_paid_on__c = 0;
        config.Director_paid_on__c = 0;
        config.Vp_paid_on__c = 0;
        config.Special_agent_paid_on__c = 0;
        insert config;
         Comission_Configuration__c config1 = new Comission_Configuration__c();
        config1.Amount_Paid_to_Agent_Finalizing_Sale__c=1;
        config1.Amount_Paid_to_Agent_Initiating_Sale__c=12;
        config1.Amount_Paid_to_Director__c=23;
        config1.Amount_Paid_to_Senior_Agent__c=23;
        config1.Amount_Paid_to_Vice_President__c=12;
        config1.Max_Value__c=200;
        config1.Min_Value__c=101;
        config1.Percentage_Paid_to_Agent_Making_Sell__c=10;
        config1.Percentage_Paid_to_Director__c=12;
        config1.Percentage_Paid_to_Vice_President__c=12;
        config1.Percentage_Paid_to_Senior_Agent__c = 0;
        config1.Agent_paid_on__c = 0;
        config1.Director_paid_on__c = 0;
        config1.Vp_paid_on__c = 0;
        config1.Special_agent_paid_on__c = 0;
        insert config1;
       	Payment__c pay1 = new Payment__c();
        pay1.Amount__c = 50;
        pay1.Product__c = prod.Id;
        pay1.week__c=24;
        pay1.Payment_Created_By__c=userinfo.getuserid();
        pay1.Opportunity__c = op1.id;
        pay1.Payment_Created_By__c = op1.Sales_Person_Id__c;
        insert pay1;
         test.startTest();
        PaymentToComissionTriggerOPHandler.refund(pay1);
        test.stopTest();
    }
    
}