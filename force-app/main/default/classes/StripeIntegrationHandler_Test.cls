@isTest 
public class StripeIntegrationHandler_Test {
    static testMethod void testMethodPostive1() 
    { 
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        insert add;
       
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='123455';
        sp.Customer__c =  con.id;
        insert sp;
        
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = sp.id;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        opportunity op  = new opportunity();
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        
        insert ol;
        set<id> idset = new set<id>();
        idset.add(op.id);
        test.StartTest();
        Test.setMock(HttpCalloutMock.class, new StripeGetCard_MockTest());
        
        
        StripeIntegrationHandler.cardStripeHandler(card.id);
        StripeIntegrationHandler.cardUpdate(card.id);
        StripeIntegrationHandler.getAllPurchases('1234');
        StripeIntegrationHandler.getAllCard('card_1D7P9xBwLSk1v1ohQezYknMQ','card');
        //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
        op.name= 'testclub';
        update op;
         con.Stripe_Customer_Id__c =null;
         update con;
          //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
       test.stopTest();
        
    }
       static testMethod void testMethodPostive2() 
    { 
        contact con = new contact();
        con.lastName='test';
        con.Stripe_Customer_Id__c ='34223424';
        insert con;
        
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Contact__c =con.id;
        insert add;
       
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='123455';
        sp.Customer__c =  con.id;
        insert sp;
        
        Card__c  card = new Card__c ();
        card.Contact__c = con.id;
        card.Stripe_Profile__c = null;
        card.Credit_Card_Number__c='12333';
        Date d = date.Today();
        
        Date nextWeek = d.addDays(7); 
        card.Expiry_Month__c = string.valueOf(12);
        card.Expiry_Year__c= '2019';
        card.Cvc__c = '123';
        
        insert card;
        opportunity op  = new opportunity();
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        
        insert ol;
        set<id> idset = new set<id>();
        idset.add(op.id);
        test.StartTest();
        StripeIntegrationHandler.cardStripeHandler(card.id);
        StripeIntegrationHandler.cardUpdate(card.id);
        StripeIntegrationHandler.getAllPurchases('1234');
        //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
        op.name= 'testclub';
        update op;
         con.Stripe_Customer_Id__c =null;
         update con;
          //StripeIntegrationHandler.OpportunityStripeHandler(JSON.serialize(new list<Opportunity>{op}),JSON.serialize(ol));
          StripeIntegrationHandler.DummyCover();
       test.stopTest();
        
    }
}