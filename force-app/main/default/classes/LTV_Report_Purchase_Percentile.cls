// Created By : Sourabh Badole 
// Date       : 18-3-2019
public class LTV_Report_Purchase_Percentile {
    
    public Decimal Amount {get;set;}
    public LTV_Report_Purchase_Percentile (ApexPages.StandardController controller){
        List<opportunity> oppList  = new List<Opportunity>();
        String conId   = apexpages.currentpage().getparameters().get('id'); 
        Amount = 0;
        Decimal totAmount = 0 ; 
        
        if(conId != null && conId != ''){
            oppList = [select id, Amount , Stripe_charge_id__c ,Refund_Amount__c,contact__c from Opportunity where Contact__c =: conId and Stripe_charge_id__c != null and Stripe_charge_id__c != 'null' and Stripe_charge_id__c != '' and Contact__c != null and  Amount != null];
            if(oppList != null && !oppList.isEmpty()){
                for(Opportunity opp : oppList) {
                    if(opp.contact__c != null && opp.Amount != null) {
                        totAmount = totAmount + opp.Amount ;
                        if( opp.Refund_Amount__c != null ) {
                            totAmount  = totAmount - opp.Refund_Amount__c ;
                        }
                    }
                }
            }
            if(totAmount != null ){
                if(totAmount >= 2996 ){
                    Amount = 99.80;
                }
                else if(totAmount < 2996 && totAmount >= 2000){
                    Amount = 99.62 ;
                }
                else if(totAmount < 2000 && totAmount >= 1000){
                    Amount = 98.35;
                }
                else if(totAmount < 1000  && totAmount >= 900){
                    Amount = 97.86;
                }
                else if(totAmount < 900  && totAmount >= 800){
                    Amount = 97.21;
                }
                else if(totAmount < 800 && totAmount >= 700){
                    Amount = 96.20;
                }
                else if(totAmount < 700 && totAmount >= 600){
                    Amount = 94.85;
                }
                else if(totAmount < 600 && totAmount >= 500){
                    Amount = 92.71;
                }
                else if(totAmount < 500 && totAmount >= 400){
                    Amount = 89.03;
                }
                else if(totAmount < 400 && totAmount >= 300){
                    Amount = 83.27;
                }
                else if(totAmount < 300 && totAmount >= 200){
                    Amount = 71.26;
                }
                else if(totAmount < 200 && totAmount >= 180){
                    Amount = 66.75;
                }
                else if(totAmount < 180 && totAmount >= 160){
                    Amount = 63.66;
                }
                else if(totAmount < 160 && totAmount >= 140){
                    Amount = 54.75;
                }
                else if(totAmount < 140 && totAmount >= 120){
                    Amount = 51.06;
                }
                else if(totAmount < 120 && totAmount >= 100){
                    Amount = 47.08;
                }
                else if(totAmount < 100 && totAmount >= 80){
                    Amount = 35.90;
                }
                else if(totAmount < 80 && totAmount > 54 ){
                    Amount = 22.16;
                }
                else if(totAmount <= 54 && totAmount > 27){
                    Amount = 11.10;
                }
                else{
                    Amount = 0.00;
                }
            }
        }        
    }
}