public class StripeInvoice {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class Data_Z {
		public Object_data object_data {get;set;} 
		public Previous_attributes previous_attributes {get;set;} 

		public Data_Z(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object_data') {
							object_data = new Object_data(parser);
						} else if (text == 'previous_attributes') {
							previous_attributes = new Previous_attributes(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Data_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class StripeInvoiceObject {
		public String id {get;set;} 
		public String object_data {get;set;} 
		public String api_version {get;set;} 
		public Integer created {get;set;} 
		public Data_Z data {get;set;} 
		public Boolean livemode {get;set;} 
		public Integer pend_dataing_webhooks {get;set;} 
		public Request request {get;set;} 
		public String type_Z {get;set;} // in json: type

		public StripeInvoiceObject(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'api_version') {
							api_version = parser.getText();
						} else if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'data') {
							data = new Data_Z(parser);
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'pend_dataing_webhooks') {
							pend_dataing_webhooks = parser.getIntegerValue();
						} else if (text == 'request') {
							request = new Request(parser);
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Previous_attributes {
		public String description {get;set;} 

		public Previous_attributes(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'description') {
							description = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Previous_attributes consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Metadata {

		public Metadata(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Metadata consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Request {
		public String id {get;set;} 
		//public Object idempotency_key {get;set;} 

		public Request(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'idempotency_key') {
							//idempotency_key = new Object(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Request consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Period {
		public Integer start {get;set;} 
		public Integer end_data {get;set;} 

		public Period(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'start') {
							start = parser.getIntegerValue();
						} else if (text == 'end_data') {
							end_data = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Period consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public String id {get;set;} 
		public String object_data {get;set;} 
		public Integer amount {get;set;} 
		public String currency_data {get;set;} 
		public String description {get;set;} 
		public Boolean discountable {get;set;} 
		public Boolean livemode {get;set;} 
		public Metadata metadata {get;set;} 
		public Period period {get;set;} 
		public Plan plan {get;set;} 
		public Boolean proration {get;set;} 
		public Integer quantity {get;set;} 
		public String subscription {get;set;} 
		public String subscription_item {get;set;} 
		public String type_Z {get;set;} // in json: type

		public Data(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'amount') {
							amount = parser.getIntegerValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'description') {
							description = parser.getText();
						} else if (text == 'discountable') {
							discountable = parser.getBooleanValue();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'period') {
							period = new Period(parser);
						} else if (text == 'plan') {
							plan = new Plan(parser);
						} else if (text == 'proration') {
							proration = parser.getBooleanValue();
						} else if (text == 'quantity') {
							quantity = parser.getIntegerValue();
						} else if (text == 'subscription') {
							subscription = parser.getText();
						} else if (text == 'subscription_item') {
							subscription_item = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class Coupon {
		public String id {get;set;} 
		public String object_data {get;set;} // in json: object
		public Integer amount_off {get;set;} 
		public Integer created {get;set;} 
		public String currency_data {get;set;} 
		public String duration {get;set;} 
		public Integer duration_in_months {get;set;} 
		public Boolean livemode {get;set;} 
		//public Object max_redemptions {get;set;} 
		public Metadata metadata {get;set;} 
		public Integer percent_off {get;set;} 
		//public Object redeem_by {get;set;} 
		public Integer times_redeemed {get;set;} 
		public Boolean valid {get;set;} 

		public Coupon(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'amount_off') {
							amount_off = parser.getIntegerValue();
						} else if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'duration') {
							duration = parser.getText();
						} else if (text == 'duration_in_months') {
							duration_in_months = parser.getIntegerValue();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'max_redemptions') {
							//max_redemptions = new Object(parser);
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'percent_off') {
							percent_off = parser.getIntegerValue();
						} else if (text == 'redeem_by') {
							//redeem_by = new Object(parser);
						} else if (text == 'times_redeemed') {
							times_redeemed = parser.getIntegerValue();
						} else if (text == 'valid') {
							valid = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Coupon consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    public class Discount {
		public String object_data {get;set;} // in json: object
		public Coupon coupon {get;set;} 
		public String customer {get;set;} 
		public Integer end_data {get;set;} // in json: end
		public Integer start {get;set;} 
		public String subscription {get;set;} 

		public Discount(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'coupon') {
							coupon = new Coupon(parser);
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'end_data') {
							end_data = parser.getIntegerValue();
						} else if (text == 'start') {
							start = parser.getIntegerValue();
						} else if (text == 'subscription') {
							subscription = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Discount consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Object_data {
		public String id {get;set;} 
		public String object_data {get;set;} 
		public Integer amount_due {get;set;} 
		public Integer application_fee {get;set;} 
		public Integer attempt_count {get;set;} 
		public Boolean attempted {get;set;} 
		public String billing {get;set;} 
		public String charge {get;set;} 
		public Boolean closed {get;set;} 
		public String currency_data {get;set;} 
		public String customer {get;set;} 
		public Integer date_data {get;set;} 
		public String description {get;set;} 
		public Discount discount {get;set;} 
		public Integer due_date {get;set;} 
		public Integer end_dataing_balance {get;set;} 
		public Boolean forgiven {get;set;} 
		public Lines lines {get;set;} 
		public Boolean livemode {get;set;} 
		public Metadata metadata {get;set;} 
		public String next_payment_attempt {get;set;} 
		public String number_Z {get;set;} // in json: number
		public Boolean paid {get;set;} 
		public Integer period_end_data {get;set;} 
		public Integer period_start {get;set;} 
		public String receipt_number {get;set;} 
		public Integer starting_balance {get;set;} 
		public String statement_descriptor {get;set;} 
		public String subscription {get;set;} 
		public Integer subtotal {get;set;} 
		public Integer tax {get;set;} 
		public Double tax_percent {get;set;} 
		public Integer total {get;set;} 
		public Integer webhooks_delivered_at {get;set;} 

		public Object_data(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'amount_due') {
							amount_due = parser.getIntegerValue();
						} else if (text == 'application_fee') {
							application_fee = parser.getIntegerValue();
						} else if (text == 'attempt_count') {
							attempt_count = parser.getIntegerValue();
						} else if (text == 'attempted') {
							attempted = parser.getBooleanValue();
						} else if (text == 'billing') {
							billing = parser.getText();
						} else if (text == 'charge') {
							charge = parser.getText();
						} else if (text == 'closed') {
							closed = parser.getBooleanValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'date_data') {
							date_data = parser.getIntegerValue();
						} else if (text == 'description') {
							description = parser.getText();
						} else if (text == 'discount') {
							discount = new Discount(parser);
						} else if (text == 'due_date') {
							due_date = parser.getIntegerValue();
						} else if (text == 'end_dataing_balance') {
							end_dataing_balance = parser.getIntegerValue();
						} else if (text == 'forgiven') {
							forgiven = parser.getBooleanValue();
						} else if (text == 'lines') {
							lines = new Lines(parser);
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'next_payment_attempt') {
							next_payment_attempt = parser.getText();
						} else if (text == 'number') {
							number_Z = parser.getText();
						} else if (text == 'paid') {
							paid = parser.getBooleanValue();
						} else if (text == 'period_end_data') {
							period_end_data = parser.getIntegerValue();
						} else if (text == 'period_start') {
							period_start = parser.getIntegerValue();
						} else if (text == 'receipt_number') {
							receipt_number = parser.getText();
						} else if (text == 'starting_balance') {
							starting_balance = parser.getIntegerValue();
						} else if (text == 'statement_descriptor') {
							statement_descriptor = parser.getText();
						} else if (text == 'subscription') {
							subscription = parser.getText();
						} else if (text == 'subtotal') {
							subtotal = parser.getIntegerValue();
						} else if (text == 'tax') {
							tax = parser.getIntegerValue();
						} else if (text == 'tax_percent') {
							tax_percent = parser.getDoubleValue();
						} else if (text == 'total') {
							total = parser.getIntegerValue();
						} else if (text == 'webhooks_delivered_at') {
							webhooks_delivered_at = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Object_data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Plan {
		public String id {get;set;} 
		public String object_data {get;set;} 
		public Integer amount {get;set;} 
		public Integer created {get;set;} 
		public String currency_data {get;set;} 
		public String interval {get;set;} 
		public Integer interval_count {get;set;} 
		public Boolean livemode {get;set;} 
		public Metadata metadata {get;set;} 
		public String name {get;set;} 
		public String statement_descriptor {get;set;} 
		//public Object trial_period_days {get;set;} 

		public Plan(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'amount') {
							amount = parser.getIntegerValue();
						} else if (text == 'created') {
							created = parser.getIntegerValue();
						} else if (text == 'currency_data') {
							currency_data = parser.getText();
						} else if (text == 'interval') {
							interval = parser.getText();
						} else if (text == 'interval_count') {
							interval_count = parser.getIntegerValue();
						} else if (text == 'livemode') {
							livemode = parser.getBooleanValue();
						} else if (text == 'metadata') {
							metadata = new Metadata(parser);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'statement_descriptor') {
							statement_descriptor = parser.getText();
						} else if (text == 'trial_period_days') {
							//trial_period_days = new Object(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Plan consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Lines {
		public String object_data {get;set;} 
		public List<Data> data {get;set;} 
		public Boolean has_more {get;set;} 
		public Integer total_count {get;set;} 
		public String url {get;set;} 

		public Lines(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object_data') {
							object_data = parser.getText();
						} else if (text == 'data') {
							data = new List<Data>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data.add(new Data(parser));
							}
						} else if (text == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text == 'total_count') {
							total_count = parser.getIntegerValue();
						} else if (text == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Lines consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static StripeInvoiceObject parse(String json) {
		return new StripeInvoiceObject(System.JSON.createParser(json));
	}
}