@isTest
public class MC_ProductType_Survey_test {
     public static testMethod void test1(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        //Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.One_Click_Survey__c = 'Poor';
        opp.Is_Coaching_Failed__c = false;
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey_good;
        List<SelectOption> options = oppC.coachingOpt; 
        List<SelectOption> option = oppC.goodregions; 
        List<SelectOption> optiones = oppC.regions; 
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Issue solved';
        system.debug('==============opp.id================'+opp.id);
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Poor');
        
        oppC.successRedirect();
        
    }
    
    public static testMethod void test2(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        //Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey_good;
        List<SelectOption> options = oppC.coachingOpt; 
        List<SelectOption> option = oppC.goodregions; 
        List<SelectOption> optiones = oppC.regions; 
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Issue solved';
        system.debug('==============opp.id================'+opp.id);
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Poor');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test3(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        //Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Issue solved';
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Excellent');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test4(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        // Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Issue solved';
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Ok');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test5(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        // Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Tracking info/order questions answered';
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Great');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test6(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        //Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        
        Test.setCurrentPage(pageRef);
        oppC.selectObject = 'Tracking info/order questions answered';
        pageRef.getParameters().put('id','8577757');
        pageRef.getParameters().put('res','Great');
        
        oppC.successRedirect();
        
    }
    public static testMethod void test7(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        // Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        
        Test.setCurrentPage(pageRef);
        oppC.customerComment = 'hello';
        oppC.selectObject = 'Other';
        oppC.showServerMessage=true;
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Great');
        
        Test.setCurrentPage(pageRef);
        oppC.changeEvent();
        
        oppC.save();
        
        oppC.successRedirect();
        
        
    }
    public static testMethod void test8(){
        Triggers_activation__c activateTrigger = new Triggers_activation__c();
        activateTrigger.isCampaignAssociate__c = true;
        insert activateTrigger;
        // Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
        Opportunity opp =  new Opportunity();
        opp.Name='test';
        opp.StageName='closed-won';
        opp.CloseDate= date.valueof(system.now());
        insert opp;
        
        MC_ProductType_Survey oppC = new MC_ProductType_Survey();
        PageReference pageRef = Page.MC_ProductType_Survey;
        oppC.customerComment = 'hello';
        oppC.selectObject = 'Other';
        oppC.showServerMessage=true;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',opp.id);
        pageRef.getParameters().put('res','Ok');
        oppC.changeEvent();
        
        oppC.save();
        
    }
    
}