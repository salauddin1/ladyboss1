@isTest
public class subTaskOfTaskListControllerTest {
    @isTest
    public static void test1(){
        Case cs = new Case();
       // Id CaserecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Account Recovery').getRecordTypeId();
       // cs.RecordTypeId = CaserecTypeId;
        cs.DueDate__c = system.today().addDays(7);
        cs.Charge_Id__c = 'chargeId';
        cs.Invoice_Id__c = 'invoice';
        cs.Subject = 'Failed payment - $';
   //     cs.ContactId = lstStripeProf[0].Customer__c;
        cs.Stripe_Customer_Id__c = 'test';
        cs.Contact_Phone__c = '345678';
        cs.Contact_Email__c = 'badole.salesforce@gmail.com';
        insert cs;
        Subsequent_Task__c st = new Subsequent_Task__c();
        st.Case_id__c = cs.id;
        st.name = 'sub task';
        st.Charge_Id__c = 'ch_adjnd';
        st.Amount__c = 5654;
        insert st;
        
        Subsequent_Task__c st2 = new Subsequent_Task__c();
        st2.Case_id__c = cs.id;
        st2.name = 'sub task';
        st2.Charge_Id__c = 'ch_adjnd';
        st2.Note__c = 'dsfjdb';
        st2.IsNote__c = true ;
        st2.Amount__c = 5654;
        insert st2;
        subTaskOfTaskListController.getSubTask(cs.id);
        subTaskOfTaskListController.getSubTaskNote(cs.id);
    }
}