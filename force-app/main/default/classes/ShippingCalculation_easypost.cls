global class ShippingCalculation_easypost {
	global list<rates> rates;
	global static ShippingCalculation_easypost getShipping(String shippingName, String shippingStreet, String shippingCity,String shippingState,String shippingPostalCode,String shippingCountry,String phone, String email,List<id> prodID){
		List<Product2> productList= new List<Product2>();
        for(Product2 prod:[SELECT Id,isBundledProduct__c,Length__c,Height__c,Width__c,Weight__c FROM Product2 WHERE Id IN :prodID]){
        	if(prod.isBundledProduct__c==true){
        		Product2 prod1=[Select id,Product_One__c,Product_Two__c,Product_Three__c,Product_Four__c,Product_Five__c FROM Product2 WHERE id =:prod.Id];
		        List<Id> prodList = new List<Id>();
		        if(prod1.Product_One__c !=null){
		               prodList.add(prod1.Product_One__c);
		        }if(prod1.Product_Two__c !=null){
		               prodList.add(prod1.Product_Two__c);
		        }if(prod1.Product_Three__c !=null){
		               prodList.add(prod1.Product_Three__c);
		        }if(prod1.Product_Four__c !=null){
		               prodList.add(prod1.Product_Four__c);
		        }if(prod1.Product_Five__c !=null){
		               prodList.add(prod1.Product_Five__c);
		        }
		      List<Product2> bundledProducts = [Select id,Length__c,Height__c,Width__c,Weight__c FROM Product2 WHERE Id IN :prodList]; 
		      productList.addAll(bundledProducts);
        	}else{
        		productList.add(prod);
        	}
        }
        if(productList.size()>0){
        	system.debug('productList--'+productList);
        	String s='';
	        system.debug('shippingcountry'+shippingCountry);
	        HttpRequest http = new HttpRequest();
	        http.setEndpoint('https://api.easypost.com/v2/shipments');
	        http.setMethod('POST');
	        http.setHeader('content-type', 'application/x-www-form-urlencoded');
	        
	        
	        http.setHeader('Authorization', 'Bearer EZTKfb08273f2de7485f92579fb82ee20864rN4r4GH6blloeJ2Ww9G8NA');
	        Decimal weight =0;
	        Decimal length =0;
	        Decimal width =0;
	        Decimal height =0;
	        for(Integer i=0;i<productList.size();i++){
	        	weight+=productList[i].Weight__c;
	        	length+=productList[i].Length__c;
	        	if(productList[i].Width__c>width){
	        		width=productList[i].Width__c;
	        	}
	        	if(productList[i].Height__c>height){
	        		height=productList[i].Height__c;
	        	}
	        	
	        }
	        s+='&shipment[parcel][length]='+length+'&shipment[parcel][width]='+width+'&shipment[parcel][height]='+height+'&shipment[parcel][weight]='+weight;
	        system.debug('Anydatatype_msg'+s);
	        http.setBody('shipment[to_address][name]='+shippingName+'&shipment[to_address][street1]='+shippingStreet+'&shipment[to_address][city]='+shippingCity+'&shipment[to_address][state]='+shippingState+'&shipment[to_address][zip]='+shippingPostalCode+'&shipment[to_address][country]='+shippingCountry+'&shipment[from_address][name]=Ladyboss&&shipment[from_address][street1]=2000 Main Street&shipment[from_address][city]=Irvine&shipment[from_address][state]=CA&shipment[from_address][zip]=92614&shipment[from_address][country]=US'+s);
	        String response;
	        Integer statusCode;
	        Http con = new Http();
	        HttpResponse hs = new HttpResponse();
	        if(!Test.isRunningTest()){
	            try {
	                    hs = con.send(http);
	                } catch (CalloutException e) {
	                    system.debug('e.getMessage()-->'+e.getMessage());
	                    return null;
	                }
	        }else{
	            hs.setStatusCode(200);
	        }
	         system.debug('#### '+ hs.getBody());
	        
	        response = hs.getBody();
	        statusCode = hs.getStatusCode();
	        system.debug('$$statusCode = '+hs.getStatusCode());
	         try {
	         	ShippingCalculation_easypost o = ShippingCalculation_easypost.parse(response);
	         	return o;
	        } catch (System.JSONException e) {
	            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
	            return null;
	        }
    	}else{
    		return null;
    	}
	}
	 global class rates{
		global String id;
		global String carrier;
		global String rate;
		global String service;
		global Integer delivery_days;
	}

	public static ShippingCalculation_easypost parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (ShippingCalculation_easypost) System.JSON.deserialize(json, ShippingCalculation_easypost.class);
    }
}