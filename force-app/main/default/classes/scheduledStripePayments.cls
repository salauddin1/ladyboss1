global class scheduledStripePayments implements Schedulable {
   global void execute(SchedulableContext SC) {
      /* List<Opportunity> oppList = new List<Opportunity>();
       oppList  =[select id,Name,Amount,Contact__c,Card__c,Description from Opportunity where Scheduled_Payment_Date__c=TODAY and Scheduled_Payment_Processed__c=false];
       
       Set<Id> oppIds = new Set<Id>();
       for(Opportunity opp:oppList){
           oppIds.add(opp.Id);
       }
        
       StripeIntegrationHandler.OpportunityStripeHandler(oppIds); */  
       
        StripeIntegrationHandlerBatch b = new StripeIntegrationHandlerBatch(); 
      database.executebatch(b,1);
   }
  
}