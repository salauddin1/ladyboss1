@isTest
global class StripeGetCardV1_MockTest implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //res.setBody('{"object":"list","data":[{"id":"cus_DYoZR3370en1xo"}]}');
        String body ='{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"email\":\"ashish@gmail.com\",\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}"order\":{\"Integration Initiated From\":\"Salesforce\"}}}';
       res.setBody(body);
        res.setStatusCode(200);
        
        return res;
    }
}