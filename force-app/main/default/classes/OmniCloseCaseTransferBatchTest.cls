@isTest
private class OmniCloseCaseTransferBatchTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        QueueRoutingConfig rconfig =[SELECT Id FROM QueueRoutingConfig Limit 1];
        Group g1 = [SELECT Id FROM Group WHERE QueueRoutingConfigId != null AND Name != 'Grant Omni Test Queue' limit 1];
        Omni_Closed_Case_Queue_Map__c om = new Omni_Closed_Case_Queue_Map__c();
        om.Name = 'test';
        om.Omni_Queue_Id__c = g1.id;
        om.Omni_Closed_Queue_ID__c = g1.id;
        insert om;
        Case caseRec = new Case();
        caseRec.Subject = 'testCase';
        caseRec.Description = 'TTB0333333';
        caseRec.OwnerId =  g1.id; 
        caseRec.Status = 'Closed';
        insert caseRec;
        Test.startTest();
        Database.executeBatch(new OmniCloseCaseTransferBatch());
        Test.stopTest();
        
            
    }
}