@isTest
public class AmazonMwsOrderTest {
    @isTest 
    public static void testMethod1(){ 
        
        Test.setMock(HttpCalloutMock.class, new AmazonOrderMockResponse());
    	AmazonOrder__c ac = new AmazonOrder__c();
        ac.LastUpdatedAfter__c = System.now();
        ac.Name='Amazon Order Keys';
        insert ac;
        AmazonMwsKeys__c mwsKeys = new AmazonMwsKeys__c();
        mwsKeys.Version__c='String';
		mwsKeys.SignatureVersion__c='String';
		mwsKeys.SignatureMethod__c='String';
		mwsKeys.marketplaceidUs__c='String';
		mwsKeys.SellerId__c='String';
		mwsKeys.url__c='String';
		mwsKeys.AWSAccessKeyId__c='String';
		mwsKeys.secretKey__c='String';
		mwsKeys.MWSAuthToken__c='String';
        mwsKeys.Name = 'amazonKeys';
        insert mwsKeys;
        AmazonMwsOrderBatch batch = new AmazonMwsOrderBatch();
        Database.executeBatch(batch);
    }
}