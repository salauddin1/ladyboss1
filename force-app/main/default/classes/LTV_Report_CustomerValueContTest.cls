// Developer Name : Sourabh Badole 
// Date       : 24-4-2019
@isTest
public class LTV_Report_CustomerValueContTest {
    // Test Methods for code coverage LTV_Report_CustomerValueCont controller.
    @isTest
    public static void TestPageCon1(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.ContactId = con.id;
        csRecord.Status = 'open' ;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=23,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon2(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.Status = 'open' ;
        csRecord.ContactId = con.id;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=253,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    
    @isTest
    public static void TestPageCon3(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.Status = 'open' ;
        csRecord.ContactId = con.id;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=3223,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon4(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.ContactId = con.id;
        csRecord.Status = 'open' ;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=823,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon5(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.Status = 'open' ;
        csRecord.ContactId = con.id;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount = 1503,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon6(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.ContactId = con.id;
        csRecord.Status = 'open' ;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=603,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon7(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.ContactId = con.id;
        csRecord.Status = 'open' ;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=903,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
    @isTest
    public static void TestPageCon8(){
        Contact con = new Contact();
        con.LastName = 'Badole';
        insert con ;
        
        Case csRecord = new Case();
        csRecord.ContactId = con.id;
        csRecord.Status = 'open' ;
        insert csRecord ;
        
        Opportunity opp = new Opportunity (
            Name='opp1',
            StageName='Stage 0 - Lead Handed Off',
            CloseDate=Date.today().addDays(7) ,
            Amount=83,
            Stripe_charge_id__c = 'ch_1EDXR7BwLSk1v1oh2lFUFIj8',
            Contact__c =con.id  );
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(csRecord);
        PageReference pageRef = Page.LTV_Report_CustomerValue;
        pageRef.getParameters().put('id', String.valueOf(csRecord.Id));
        Test.setCurrentPage(pageRef);
        
        LTV_Report_CustomerValueCont recPayCon = new LTV_Report_CustomerValueCont(sc);
        
    }
}