global class SetDefaultCardOnCustomerBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT id,Opportunity.Card__c FROM OpportunityLineItem where Subscription_id__c like \'sub%\' AND Status__c = \'Active\' AND Opportunity.Card__r.Stripe_Card_Id__c != null');
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        List<Id> cardId = new List<Id>();
        List<String> StripeCardIdList = new List<String>();
        for (OpportunityLineItem oli : scope) {
            cardId.add(oli.Opportunity.Card__c);
        }
        for (Card__c card : [SELECT id,Stripe_Profile__r.Stripe_Customer_Id__c FROM Card__c WHERE ID IN :cardId AND Stripe_Profile__r.Stripe_Customer_Id__c != null]) {
            StripeCustomer sc = StripeCustomer.getCustomer(card.Stripe_Profile__r.Stripe_Customer_Id__c);
            StripeCardIdList.add(sc.default_source);
        }
        List<Card__c> CardList = [SELECT id,Is_Customer_Default_Card__c,Stripe_Card_Id__c FROM Card__c WHERE Stripe_Card_Id__c IN : StripeCardIdList AND Stripe_Card_Id__c != null];
        if (CardList.size()>0) {
            for (Card__c Card : CardList) {
                Card.Is_Customer_Default_Card__c = true;
            }
            update CardList;
        }
       
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}