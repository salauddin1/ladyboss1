@RestResource(urlMapping='/ShopifyTrackOrders')
global class ShopifyTracking_Service {
    @HttpPost
    global static void trackOrders() {
     if(RestContext.request.requestBody!=null  ){
            try {
                
                String str = RestContext.request.requestBody.toString();
                System.debug('-------'+str);
                if(str != null) {
                     Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                     String order_id =String.valueOf(results.get('id')); 
                     string status = String.valueOf(results.get('status')); 
                     string financial_status = String.valueOf(results.get('financial_status')); 
                      List<Shopify_Orders__c > lstOrdersUpdate  = new  List<Shopify_Orders__c > ();
                     List<Shopify_Orders__c > lstOrders = [SELECT  Id,Status__c,Contact__c,Order_Id__c,Financial_Status__c ,Refunded_Amount__c FROM Shopify_Orders__c where Order_Id__c =:order_id ];
                     for (Shopify_Orders__c o : lstOrders  ) {
                         if(financial_status !=null){
                             //o.Status__c = status ;
                             //if (String.valueOf(results.get('financial_status'))!=null) {
                                 o.Financial_Status__c = String.valueOf(results.get('financial_status')); 
                             //}
                             lstOrdersUpdate.add(o);
                         }
                     }
                     if(lstOrdersUpdate.size() > 0)
                     update lstOrdersUpdate;
                }
            }
            catch(Exception e) {}    
            
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
    }
 }