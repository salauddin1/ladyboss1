@RestResource(urlMapping='/CustomerStripeWebHook/*')
global without sharing class CustomerStripeWebHook {
	@HttpPost
	global static void CustomerStripe() {
		if(RestContext.request.requestBody!=null){
            String server = 'Production';
            try{
                String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
                Pattern MyPattern;
                Matcher MyMatcher;
                String str = RestContext.request.requestBody.toString();
                StripeCustomer varCustomer = StripeCustomer.parse(str);
                
                List<Account> lstAccs;
                lstAccs = [Select Id, Name, First_Name__c, Last_Name__c, Phone, Description, Email__c, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, ShippingStreet, DML_From_Salesforce__c From Account WHERE External_ID__c =: varCustomer.data.object_Z.id];
                if(lstAccs.isEmpty()){
                    if(varCustomer.data.object_Z.metadata.salesforce_id!=null){
                    	lstAccs = [Select Id, Name, First_Name__c, Last_Name__c, Phone, Description, Email__c, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, ShippingStreet, DML_From_Salesforce__c From Account WHERE Id =: varCustomer.data.object_Z.metadata.salesforce_id];
                    }
                }
                
                Account acc = new Account();
                if(lstAccs.size()>0){
                    if(lstAccs.get(0).DML_From_Salesforce__c==true){
                        acc.Id = lstAccs.get(0).Id;
                        acc.DML_From_Salesforce__c = false;
                        acc.External_ID__c = varCustomer.data.object_Z.id;
                        acc.Name = lstAccs.get(0).Name;
                        acc.First_Name__c = lstAccs.get(0).First_Name__c;
                        acc.Last_Name__c = lstAccs.get(0).Last_Name__c;
                        acc.Phone = lstAccs.get(0).Phone;
                        acc.Description = lstAccs.get(0).Description;
                        acc.Email__c = lstAccs.get(0).Email__c;
                        acc.ShippingCity = lstAccs.get(0).ShippingCity;
                        acc.ShippingCountry = lstAccs.get(0).ShippingCountry;
                        acc.ShippingPostalCode = lstAccs.get(0).ShippingPostalCode;
                        acc.ShippingState = lstAccs.get(0).ShippingState;
                        acc.ShippingStreet = lstAccs.get(0).ShippingStreet;
                    }else{
                        acc.Id = lstAccs.get(0).Id;
						acc.External_ID__c = varCustomer.data.object_Z.id;
                        acc.Default_Source__c = varCustomer.data.object_Z.default_source;
                        if(varCustomer.data.object_Z.metadata!=null && varCustomer.data.object_Z.metadata.name!=null){
                            acc.Name = varCustomer.data.object_Z.metadata.name;
                            acc.First_Name__c = varCustomer.data.object_Z.metadata.first_name;
                    		acc.Last_Name__c = varCustomer.data.object_Z.metadata.last_name;
                            if(varCustomer.data.object_Z.metadata.phone!=null) acc.Phone = varCustomer.data.object_Z.metadata.phone;
                        }else{
                            if(varCustomer.data.object_Z.email!=null && varCustomer.data.object_Z.email!=''){
                                acc.Name = varCustomer.data.object_Z.email;
                            }else{
                                acc.Name = varCustomer.data.object_Z.id;
                            }
                        }

                        if(varCustomer.data.object_Z.email!=null && varCustomer.data.object_Z.email!=''){
                            MyPattern = Pattern.compile(emailRegex);
                            MyMatcher = MyPattern.matcher(varCustomer.data.object_Z.email);
                            if (MyMatcher.matches()){
                                acc.Email__c = varCustomer.data.object_Z.email;
                            }
                        }
                        //acc.Email__c = varCustomer.data.object_Z.email;
                        //acc.Type = varCustomer.data.object_Z.object_Z;
                        acc.Description = varCustomer.data.object_Z.description;
                        if(varCustomer.data.object_Z.shipping!=null){
                            if(varCustomer.data.object_Z.shipping.address!=null){
                                acc.ShippingCity = varCustomer.data.object_Z.shipping.address.city;
                                acc.ShippingCountry = varCustomer.data.object_Z.shipping.address.country;
                                acc.ShippingPostalCode = varCustomer.data.object_Z.shipping.address.postal_code;
                                acc.ShippingState = varCustomer.data.object_Z.shipping.address.state;
                                acc.ShippingStreet = varCustomer.data.object_Z.shipping.address.line1;
                            }
                        }   
                        
                        /*if(varCustomer.data.object_Z.sources!=null){
                            for(StripeCustomer.DataSource source : varCustomer.data.object_Z.sources.data){
                                acc.Card_ID__c = source.id;
                                acc.BillingCity = source.address_city;
                                acc.BillingCountry = source.address_country;
                                acc.BillingStreet = source.address_line1;
                                acc.BillingState = source.address_state;
                                acc.BillingPostalCode = source.address_zip;
                                acc.Brand__c = source.brand;
                                acc.Country__c = source.country;
                                acc.Cvc_Check__c = source.cvc_check;
                                acc.Exp_Month__c = source.exp_month;
                                acc.Exp_Year__c = source.exp_year;
                                acc.Last4__c = source.last4;
                                break;
                            }
                        }*/
                    }
                }else{
                    acc.External_ID__c = varCustomer.data.object_Z.id;
                    if(varCustomer.data.object_Z.metadata!=null && varCustomer.data.object_Z.metadata.name!=null){
                        acc.Name = varCustomer.data.object_Z.metadata.name;
                        acc.First_Name__c = varCustomer.data.object_Z.metadata.first_name;
                        acc.Last_Name__c = varCustomer.data.object_Z.metadata.last_name;
                        if(varCustomer.data.object_Z.metadata.phone!=null) acc.Phone = varCustomer.data.object_Z.metadata.phone;
                    }else{
                        if(varCustomer.data.object_Z.email!=null && varCustomer.data.object_Z.email!=''){
                            acc.Name = varCustomer.data.object_Z.email;
                        }else{
                            acc.Name = varCustomer.data.object_Z.id;
                        }
                    }
                    if(varCustomer.data.object_Z.email!=null && varCustomer.data.object_Z.email!=''){
                        MyPattern = Pattern.compile(emailRegex);
                        MyMatcher = MyPattern.matcher(varCustomer.data.object_Z.email);
                        if (MyMatcher.matches()){
                            acc.Email__c = varCustomer.data.object_Z.email;
                        }
                    }
                    //acc.Email__c = varCustomer.data.object_Z.email;
                    //acc.Type = varCustomer.data.object_Z.object_Z;
                    acc.Description = varCustomer.data.object_Z.description;
                    if(varCustomer.data.object_Z.shipping!=null){
                        if(varCustomer.data.object_Z.shipping.address!=null){
                            acc.ShippingCity = varCustomer.data.object_Z.shipping.address.city;
                            acc.ShippingCountry = varCustomer.data.object_Z.shipping.address.country;
                            acc.ShippingPostalCode = varCustomer.data.object_Z.shipping.address.postal_code;
                            acc.ShippingState = varCustomer.data.object_Z.shipping.address.state;
                            acc.ShippingStreet = varCustomer.data.object_Z.shipping.address.line1;
                        }
                    }
                    
                    /*if(varCustomer.data.object_Z.sources!=null){
                        for(StripeCustomer.DataSource source : varCustomer.data.object_Z.sources.data){
                            acc.Card_ID__c = source.id;
                            acc.BillingCity = source.address_city;
                            acc.BillingCountry = source.address_country;
                            acc.BillingStreet = source.address_line1;
                            acc.BillingState = source.address_state;
                            acc.BillingPostalCode = source.address_zip;
                            acc.Brand__c = source.brand;
                            acc.Country__c = source.country;
                            acc.Cvc_Check__c = source.cvc_check;
                            acc.Exp_Month__c = source.exp_month;
                            acc.Exp_Year__c = source.exp_year;
                            acc.Last4__c = source.last4;
                            break;
                        }
                    }*/
                }
               
                acc.Type = 'Customer';
               	ApexUtil.isTriggerInvoked = true;
                if(lstAccs.size()>0){
                    update acc;
                }else{
                    upsert acc External_ID__c;
                }
               	
               	//ApexUtil.isTriggerInvoked = false;
                
               	Contact c = new Contact();
				c.External_ID__c = acc.External_ID__c;
                if(acc.Last_Name__c!=null){
                    c.Firstname = acc.First_Name__c;
                    c.LastName = acc.Last_Name__c;
                }else{
                    if(acc.First_Name__c!=null){
                        c.LastName = acc.First_Name__c;
                    }else{
                        c.LastName = acc.Name;
                    }
                }
                c.Description = acc.Description;
                c.Email = acc.Email__c;
                c.MailingCity = acc.BillingCity;
                c.MailingCountry = acc.BillingCountry;
                c.MailingPostalCode = acc.BillingPostalCode;
                c.MailingState = acc.BillingState;
                c.MailingStreet = acc.BillingStreet;
                c.OtherCity = acc.ShippingCity;
                c.OtherCountry = acc.ShippingCountry;
                c.OtherPostalCode = acc.ShippingPostalCode;
                c.OtherState = acc.ShippingState;
                c.OtherStreet = acc.ShippingStreet;
                c.AccountId = acc.Id;
                
                ApexUtil.isTriggerInvoked = true;
               	List<Contact> lstContact = [Select Id From Contact WHERE External_ID__c=:acc.External_ID__c];
                if(lstContact.size()>0){
                    c.Id = lstContact.get(0).Id;
                    update c;
                }else{
                    upsert c External_ID__c;
                }
                //ApexUtil.isTriggerInvoked = false;
                
                /*Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'});
                mail1.setSubject('Sandbox : CustomerStripeWebHook Request xxx');
                mail1.setHtmlBody(str);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });*/
            }catch(Exception e){
                if(!e.getMessage().contains('duplicate value found: External_ID__c')){
                    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); 
                    mail1.setToAddresses(new String[] {'wynche@cloudcreations.com'}); 
                    mail1.setSubject(server+': CustomerStripeWebHook Request'); 
                    mail1.setHtmlBody(e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + RestContext.request.requestBody.ToString()); 
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 }); 
                }
            }
		}
		else{
    		RestResponse res = RestContext.response; res.statusCode = 400;		    
		}
	}
}