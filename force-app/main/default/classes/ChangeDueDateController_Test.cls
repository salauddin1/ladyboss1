@IsTest
public class ChangeDueDateController_Test {
   
	static testMethod void test() {
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        insert tsk;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        ChangeDueDateController.getTask(tsk.id);
        ChangeDueDateController.saveTask(tsk, 'true', 'Facebook_Message__c');
        Test.stopTest();
	}
    static testMethod void test1() {
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        insert tsk;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        ChangeDueDateController.getTask(tsk.id);
        ChangeDueDateController.saveTask(tsk, 'true', 'Recovered_Payment__c');
        Test.stopTest();
	}
    static testMethod void test2() {
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        tsk.Contact__c = ct.id;
        insert tsk;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        ChangeDueDateController.getTask(tsk.id);
        ChangeDueDateController.saveTask(tsk, 'true', 'Stop_Correspondence__c');
        Test.stopTest();
	}
    static testMethod void test3() {
        Contact ct = new Contact();
        ct.LastName = 'lname';
        ct.DoNotCall = false;
        ct.Phone_National_DNC__c = false;
        ct.Other_Phone_National_DNC__c = false;
        insert ct;
        Task tsk = new Task();
        tsk.Charge_Id__c = '123asd';
        tsk.Email__c = 'sdds@gmds.com';
        tsk.Invoice_Id__c = 'dsdsdsd';
        tsk.ActivityDate = system.today();
        tsk.Contact__c = ct.id;
        insert tsk;
        
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        Test.startTest();
        ChangeDueDateController.getTask(tsk.id);
        ChangeDueDateController.saveTask(tsk, 'true', 'Saved_Payment__c');
        Test.stopTest();
	}
}