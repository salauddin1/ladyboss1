global class CardAddressUpdateBatch  implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT id,Stripe_Profile__r.Stripe_Customer_Id__c,Stripe_Card_Id__c,Contact__c FROM Card__c WHERE Contact__c != null AND Stripe_Card_Id__c != null AND Stripe_Profile__r.Stripe_Customer_Id__c != null AND Address_updated_on_Stripe__c = false');
    }   
    global void execute(Database.BatchableContext bc, List<Card__c> cardList){
        Map<Id, String> cardIdConIdMap = new Map<Id, String>();
        List<String> conIdList = new List<String>();
        Map<String, Address__c> conIdAddressMap = new Map<String, Address__c>();
        for (Card__c card : cardList) {
            cardIdConIdMap.put(card.id,card.Contact__c);
        }
        conIdList.addAll(cardIdConIdMap.values());
        for(Address__c address: [SELECT id,Contact__c,Shipping_City__c,Shipping_Country__c,Shipping_Street__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c IN: conIdList AND Primary__c = true]){
            conIdAddressMap.put(address.Contact__c,address);
        }            
        for (Card__c card : cardList) {
            if (conIdAddressMap.containsKey(card.Contact__c)) {
                CardAddressHandler.updateCard(card.Stripe_Profile__r.Stripe_Customer_Id__c,card.Stripe_Card_Id__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_City__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Country__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Street__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_State_Province__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Zip_Postal_Code__c);
                CardAddressHandler.updateStripeCustomer(card.Stripe_Profile__r.Stripe_Customer_Id__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_City__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Country__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Street__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_State_Province__c,conIdAddressMap.get(cardIdConIdMap.get(card.id)).Shipping_Zip_Postal_Code__c);
            }
            card.Address_updated_on_Stripe__c = true;
        }
        update cardList;
    } 
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }
}