// Modified By   : Sukesh G
// Created Date : 26-03-2019
public class RecoveredByDayController {   
    public Decimal amount           {get;set;}
    public Decimal percentRecovered {get;set;}
    public Integer days             {get;set;}
    public Decimal recoveredAmount  {get;set;}
    public String strSelectOption   {get;set;}
    public Integer endAmount        {get;set;}
    public Integer colorSetAmount   {get;set;}
    public Integer colorSetSecound  {get;set;}
    public List<Case> caseList      {get;set;}
    public Integer StepSetAmount    {get;set;}
    public Date startDate           {get;set;}
    public Date endDate             {get;set;}
   
    public Boolean showOpp{get;set;}
    
    public RecoveredByDayController (){
        recoveredAmount  =0;
        startDate = System.today().toStartOfWeek().addDays(1);
        //startDate = System.today().adddays(-20);
        endDate = System.Today();
    }
    /*
    public list<SelectOption> getStudOption(){
          List<case> caseList1 = [select Recovered_Payment__c,CreatedDate,Recovered__c, LastModifiedDate,  Saved_Payment__c, SF_Email__c, SKA_Video__c, Stunning_Email_Sent__c, Text_Message__c, Facebook_Message__c, Called__c, IsFailed_Payment__c,CaseNumber ,owner.Name,owner.UserName,Failed_Amount__c  from case where IsFailed_Payment__c=true and Charge_Id__c!=null AND (Product_Category__c =:'Unlimited' OR Product_Category__c =:'UTA' OR Product_Category__c =:'Supplement') And createdDate >= : startDate AND createdDate <=: endDate and isProcessedForReport__c=true and Product_Category__c != null  ];
        list<SelectOption>  lstOptions2 = new list<SelectOption>();
        lstOptions2.add(new SelectOption('All', 'All'));
        Set<String> OptionSet = new Set<String>();
        if(CaseList != null && !CaseList.isEmpty()){
            for(Case  getCs : CaseList ){
                OptionSet.add(getCs.owner.Name);
            }    
        }
        for(String  getOpt : OptionSet ){
            if(getOpt != null && getOpt != ''){
                lstOptions2.add(new SelectOption(getOpt, getOpt));
            }
        }
        return lstOptions2;
    }*/
    public void getData() {
    try{
        
        amount =0;
        days =0;
        percentRecovered=0;
        recoveredAmount  = 0;
       caseList= new  List<case> ();
        List<case> caseList1= [select Recovered_Payment__c,Recovered__c,CreatedDate, LastModifiedDate,  Saved_Payment__c, SF_Email__c, SKA_Video__c, Stunning_Email_Sent__c, Text_Message__c, Facebook_Message__c, Called__c, IsFailed_Payment__c,CaseNumber ,owner.Name,owner.UserName,Failed_Amount__c  from case where IsFailed_Payment__c=true and Charge_Id__c!=null AND (Product_Category__c =:'Unlimited' OR Product_Category__c =:'UTA' OR Product_Category__c =:'Supplement') And createdDate >= : startDate AND createdDate <=: endDate and isProcessedForReport__c=true and Product_Category__c != null order by createddate DESC  ];
        //List<case> caseList1= [select Recovered_Payment__c,Recovered__c,CreatedDate, LastModifiedDate,  Saved_Payment__c, SF_Email__c, SKA_Video__c, Stunning_Email_Sent__c, Text_Message__c, Facebook_Message__c, Called__c, IsFailed_Payment__c,CaseNumber ,owner.Name,owner.UserName,Failed_Amount__c  from case where IsFailed_Payment__c=true and Charge_Id__c!=null AND (Product_Category__c =:'Unlimited' OR Product_Category__c =:'UTA' OR Product_Category__c =:'Supplement') And createdDate >= : startDate AND createdDate <=: endDate and isProcessedForReport__c=true and Product_Category__c != null  ];
       for(case cs : caseList1) {
           amount = amount +cs.Failed_Amount__c  ;
           if(cs.Recovered__c) {
               recoveredAmount  = recoveredAmount  +cs.Failed_Amount__c  ;
                caseList.add(cs);
           }
       }
       
       if(recoveredAmount  !=0){
       percentRecovered = (recoveredAmount*100  /(amount )).setScale(2);
       
       }
        days = startDate.daysBetween(endDate);
       if(caseList.size() > 0) {
      
       showOpp =true;
       }else{
       showOpp =false;
       }
        if(test.isRunningTest()){
           errorMail('Run from test');  
        }
      }catch(Exception e){
      errorMail(String.valueOf(e.getMessage()));
      } 
    }   
    
   
    public void errorMail(String issue){
        messaging.SingleEmailMessage email1=new messaging.SingleEmailMessage();
        string[]toAdd=new string[]{'sukesh3089@gmail.com'};
        
        email1.setToAddresses(toAdd);
        
        email1.setSubject('Issue in report VF page');
        email1.setPlainTextBody('Check Recovered_By_Day page report. It has issue = : '+issue);
        email1.setSenderDisplayName('LadyBoss Account recoveryreport');
        messaging.Email[] emails = new messaging.Email[]{email1};
        messaging.sendEmail(emails);
    }

}