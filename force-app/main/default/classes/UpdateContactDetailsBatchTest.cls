@isTest
public class UpdateContactDetailsBatchTest {
    @isTest static void updateField() {
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
        
        Contact cnt = new Contact();
        cnt.AccountId = acnt.Id;
        cnt.LastName = 'TEst';
        cnt.AssistantPhone = '9876543210';
        cnt.Email = 'test@test.com';
        cnt.Email_2__c = 'test2@test.com';
        cnt.Email_3__c = 'test3@test.com';
        cnt.Phone = '9876543210';
        cnt.MobilePhone = '9876543211';
        cnt.OtherPhone = '9876543212';
        cnt.MailingStreet = 'testStreet';
        cnt.MailingCity = 'testCity';
        cnt.MailingState = 'testStreet';
        cnt.MailingCountry = 'USA';
        cnt.MailingPostalCode = '38001';
        insert cnt;
        
        Test.startTest();
        	UpdateContactDetailsBatch btch = new UpdateContactDetailsBatch();
        	database.executebatch(btch);
        Test.stopTest();
    }
}