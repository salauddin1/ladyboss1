@isTest
public class ShipstationOrder_Test {
    public static TestMethod void test(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = co.Id;
        insert shipOrder;
        shipOrder.city__c ='bhopal';
        update shipOrder ;
        ShipStation_Order_Items__c itm = new ShipStation_Order_Items__c();
        itm.sku__c='test';
        itm.ShipStation_Orders__c = shipOrder.Id;
        itm.orderItemId__c = 'hfsbcjcbs';
        itm.name__c = 'testname';
        itm.quantity__c= 10;
        itm.unitPrice__c = 10;
        insert itm;
        
        Test.startTest(); 
        String Contact1 ='{"orderId":"28694680","orderNumber":"'+shipOrder.Order_Number__c+'","Contact__c":"0030j00000FzpcgAAB","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":null,"orderStatus":"awaiti","Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "OrderNo.-0851","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String contactData = '{"Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "'+shipOrder.id+'","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        
        Test.setMock(HttpCalloutMock.class, new ShipstationOrderMock());
        
        ShipstationOrder.getContact(co.Id);
        ShipstationOrder.getShip(co.Id);
        Test.stopTest();
    }  
    public static TestMethod void test1(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = co.Id;
        insert shipOrder;
        shipOrder.city__c ='bhopal';
        update shipOrder ;
        ShipStation_Order_Items__c itm = new ShipStation_Order_Items__c();
        itm.sku__c='test';
        itm.ShipStation_Orders__c = shipOrder.Id;
        itm.orderItemId__c = 'hfsbcjcbs';
        itm.name__c = 'testname';
        itm.quantity__c= 10;
        itm.unitPrice__c = 10;
        insert itm;
        
        Test.startTest(); 
        String Contact1 ='{"orderId":"28694680","orderNumber":"'+shipOrder.Order_Number__c+'","Contact__c":"0030j00000FzpcgAAB","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":null,"orderStatus":"awaiti","Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "OrderNo.-0851","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String contactData = '{"Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "'+shipOrder.id+'","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        
        Test.setMock(HttpCalloutMock.class, new ShipstationOrderMock());
        ShipstationOrder.getOrderNew(contactData,co.Id);
        Test.stopTest();
    }  
    public static TestMethod void test2(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = co.Id;
        insert shipOrder;
        shipOrder.city__c ='bhopal';
        update shipOrder ;
        ShipStation_Order_Items__c itm = new ShipStation_Order_Items__c();
        itm.sku__c='test';
        itm.ShipStation_Orders__c = shipOrder.Id;
        itm.orderItemId__c = 'hfsbcjcbs';
        itm.name__c = 'testname';
        itm.quantity__c= 10;
        itm.unitPrice__c = 10;
        insert itm;
        
        Test.startTest(); 
        String Contact1 ='{"orderId":"28694680","orderNumber":"'+shipOrder.Order_Number__c+'","Contact__c":"0030j00000FzpcgAAB","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":null,"orderStatus":"awaiti","Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "OrderNo.-0851","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String contactData = '{"Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "'+shipOrder.id+'","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        
        Test.setMock(HttpCalloutMock.class, new ShipstationOrderMock());
        ShipstationOrder.updateOrderShip(co.Id,contactData,productData);
        Test.stopTest();
    }  
    public static TestMethod void test3(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = co.Id;
        insert shipOrder;
        shipOrder.city__c ='bhopal';
        update shipOrder ;
        ShipStation_Order_Items__c itm = new ShipStation_Order_Items__c();
        itm.sku__c='test';
        itm.ShipStation_Orders__c = shipOrder.Id;
        itm.orderItemId__c = 'hfsbcjcbs';
        itm.name__c = 'testname';
        itm.quantity__c= 10;
        itm.unitPrice__c = 10;
        insert itm;
        
        Test.startTest(); 
        String Contact1 ='{"orderId":"28694680","orderNumber":"'+shipOrder.Order_Number__c+'","Contact__c":"0030j00000FzpcgAAB","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":null,"orderStatus":"awaiti","Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "OrderNo.-0851","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String contactData = '{"Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "'+shipOrder.id+'","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        
        Test.setMock(HttpCalloutMock.class, new ShipstationOrderMock());
        
        ShipstationOrder.getOrderShip1(co.Id,Contact1,productData);
        Test.stopTest();
    } 
    public static TestMethod void test4(){
        contact co = new contact();
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        Address__c add = new Address__c();
        add.Contact__c = co.Id;
        add.Primary__c = true;
        add.Shipping_City__c = 'Indore';
        add.Shipping_Country__c = 'India';
        add.Shipping_State_Province__c = 'Mp';
        add.Shipping_Zip_Postal_Code__c = '12345';
        insert add;
        Product2 pro  =  new Product2();
        pro.Available_For_Shipstation_Orders__c =true;
        pro.Name = 'test';
        pro.StockKeepingUnit='TestSku';
        pro.Price__c=40;
        insert pro;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = co.Id;
        insert shipOrder;
        shipOrder.city__c ='bhopal';
        update shipOrder ;
        ShipStation_Order_Items__c itm = new ShipStation_Order_Items__c();
        itm.sku__c='test';
        itm.ShipStation_Orders__c = shipOrder.Id;
        itm.orderItemId__c = 'hfsbcjcbs';
        itm.name__c = 'testname';
        itm.quantity__c= 10;
        itm.unitPrice__c = 10;
        insert itm;
        
        Test.startTest(); 
        String Contact1 ='{"orderId":"28694680","orderNumber":"'+shipOrder.Order_Number__c+'","Contact__c":"0030j00000FzpcgAAB","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":null,"orderStatus":"awaiti","Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "OrderNo.-0851","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String contactData = '{"Name": "TestClass","sobjectType":"ShipStation_Orders__c","orderId":"28694680","orderStatus":"awaiti","orderNumber__c": "'+shipOrder.id+'","Contact__c":"0030j00000FzpcgAAB","First_Name__c":"Ashish","Last_Name__c":"Sharma","Email__c":"ashish.sharma.devsfdc929328338@gmail.com","address1__c":"Test Shipping Street","city__c":"Test Shipping City","zip__c":"121004","country__c":"","province__c":"121004","Send_Fulfillment_Receipt__c":false,"Send_Receipt__c":false,"Financial_Status__c":"pending","phone__c":"8130726935","Modify_Date__c":"2019-02-01","fulfillment_Status__c":"fulfilled","Country":"India","orderKey":"22202bf0e7bf4435838c7bb8489b0208","orderDate":"2019-03-02T02:25:00.0000000","createDate":"2019-03-01T23:25:00.8670000","modifyDate":"2019-03-01T23:25:00.8670000","paymentDate":"2019-03-02T02:25:00.0000000","shipByDate":"null"}';
        String productData =   '[{"Quantity":"1","selectedItem":{"val":"01tf4000001OxwSAAS","text":"BURN-1 Bottle CLUB-$39.95","SKU":"TestSKU5","price":"39.95","objName":"Product2"}}]';
        
        Test.setMock(HttpCalloutMock.class, new ShipstationOrderMock());
        ShipstationOrder.updateOrders(shipOrder.Id);
        Test.stopTest();
    }  
}