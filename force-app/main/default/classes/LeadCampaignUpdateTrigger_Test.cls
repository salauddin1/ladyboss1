@isTest
private class LeadCampaignUpdateTrigger_Test {
    public testmethod Static void onInsertTest(){
        Lead ld = new Lead();
        ld.Status = 'New';
        ld.LastName = 'test lName';
        ld.Company = 'testing company';
        ld.Products__c = 'UTA';
        insert ld;
        
        Campaign c = new Campaign();
        c.Name = 'test campaign';
        insert c;
        
        CampaignMember cm = new CampaignMember();
        cm.LeadId = ld.Id;
        cm.CampaignId = c.Id;
        insert cm;
        
        ld.LastName = 'lName';
        Update ld;
    }
}