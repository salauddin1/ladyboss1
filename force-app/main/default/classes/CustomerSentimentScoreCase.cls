//To show the customer survey on contact layout
public class CustomerSentimentScoreCase {
 List<Case> caseList ;
  public String caseId {get;set;}
  public String errorMsg {get;set;}
  public Integer surveyPercent {get;set;}
  public Boolean div1{get;set;}
  public Boolean div2{get;set;}
   public Boolean div3{get;set;}
    public Boolean div4{get;set;}
     public Boolean div5{get;set;}
  
  
       public CustomerSentimentScoreCase(ApexPages.StandardController controller){
       div1=false;
       div2=false;
        div3=false;
       div4=false;
       div5=false;
       
        //get the case ID
         caseId= apexpages.currentpage().getparameters().get('id'); 
         //List of cases with the survey
         List<Case> caseList = [select id,One_Click_Survey__c  from case where Id =: caseId and One_Click_Survey__c!=null];
         Integer count=0;
         //counting the survey percenetage
         if(caseList.size()>0){
         for(Case cs : caseList ) {
             if(cs.One_Click_Survey__c =='Excellent'){
             count = count+100;
             div5 =true;
             }else if(cs.One_Click_Survey__c =='Great'){
              count = count+75;
              div4 =true;
             }
             else if(cs.One_Click_Survey__c =='Ok'){
              count = count+25;
               div3 =true;
             }
             else if(cs.One_Click_Survey__c =='Poor'){
              count = count+0;
              div1 =true;
             }
             surveyPercent = count;
         }
         
         
        
         }else{
          div1 =false;
          div3=false;
       div4=false;
       div5=false;
       
           div2 =true;
         errorMsg = 'No Survey for this contact';
         }
         
         
         
         
    
    }

}