@isTest
public class SubscriptionCancellationBatch_Test {
	@isTest
    public static void test1(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.auto_cancel_after_days__c = 0;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Stripe_Plan_Id__c = 'prod_DFpgbRwyyH7yA5';
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Dynamic_Item_Count_Cost__c = 1;
        oL1.disable_auto_cancellation__c = false;
        oL1.Quantity = 2;
        oL1.Subscription_Id__c = 'sub_dfsdjfnjdfn';
        insert oL1;
        Datetime todayDate = Datetime.now();
        System.debug(todayDate);
		Test.setCreatedDate(oL1.Id, todayDate);
        List<OpportunityLineItem> olList = [SELECT id,CreatedDate,Cancellation_Date__c from OpportunityLineItem where id=:ol1.id];
        System.debug(olList[0].CreatedDate);
        System.debug(olList[0].Cancellation_Date__c);
        Test.startTest();
        Database.executeBatch(new SubscriptionCancellationBatch(),1);
        String sch = '0 0 8 13 2 ?';
        System.schedule('Test Job', sch, new SubscriptionCancellationBatch());
        Test.stopTest();
    }
  
}