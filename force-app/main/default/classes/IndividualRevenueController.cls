public class IndividualRevenueController {   
    public Decimal amount{get;set;}
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public List<userAmount> user_amount{get;set;}
    public Boolean showOpp{get;set;}
    public IndividualRevenueController(){
        startDate = System.today().toStartOfWeek().addDays(1);
        endDate = System.Today();
        
    }    
    public void getData() {
        System.debug(startDate+'      '+endDate);
        if(startDate > endDate){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Start date can not be greater than end date'));
        }
        amount = 0;
        user_Name_List__mdt usersName = [SELECT DeveloperName,user_Name__c FROM user_Name_List__mdt where DeveloperName='user_Name_List'];
        String dynamic_query = 'SELECT SUM(amount)aver,createdBy.Name FROM Opportunity where CloseDate >= : startDate AND CloseDate <=: endDate AND Scheduled_Payment_Date__c=null AND stageName=\'Closed Won\' AND createdby.name Not In '+usersName.user_Name__c+' group by createdBy.Name';
        AggregateResult[] groupedResults = Database.query(dynamic_query); 
        this.user_amount = new List<userAmount>();
        if(groupedResults.size()==0){
            showOpp = false;
        }
        else{
            showOpp = true;
        }
        for(AggregateResult res : groupedResults){
            amount = amount + Decimal.valueOf(String.valueOf(res.get('aver'))); 
            userAmount ua = new userAmount(String.valueOf(res.get('Name')),Decimal.valueOf(String.valueOf(res.get('aver'))));
            user_amount.add(ua);
        }
        System.debug('user_amount : '+user_amount);
        
    }     
    public class userAmount{
        public string userName{get;set;}
        public decimal totalAmount{get;set;}
        public userAmount(string userName,decimal totalAmount){
            this.userName = userName;
            this.totalAmount = totalAmount;
        }
    }
}