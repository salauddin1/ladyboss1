@isTest
public class shopifyOrderUpdat_ServiceTest {
    @isTest
    public static void ordUpdateMethod(){
        String JSONBody ='{"id": 765260857444,"cancelled_at": "22019-01-30T07:18:03-05:00","cancel_reason": null}';
        Shopify_Orders__c ord = new Shopify_Orders__c();
        ord.Order_Id__c='765260857444';
        ord.fulfillment_Status__c='Cancelled';
        ord.Status__c='canceled';
        insert ord;
        Shopify_Parameters__c shopData = new Shopify_Parameters__c();
        shopData.Access_Token__c = 'testToken12';
        shopData.Domain_Name__c = 'https://test.com';
        shopdata.Is_Active__c = true;
        insert shopData;
        Test.startTest();
        RestRequest request = new RestRequest();
        request.requestUri ='https://ashish-ladyboss-support.cs53.force.com/ShipStation/services/apexrest/shopifyOrderUpdat_Service';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONBody);
        RestContext.request = request;
        shopifyOrderUpdat_Service.updateOrder();
        Test.stopTest();
    }
}