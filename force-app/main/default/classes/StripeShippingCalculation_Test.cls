@isTest
public class StripeShippingCalculation_Test {
  
    public static testMethod void test(){
        try{
        	test.startTest();
        Product2 prod = new Product2();
        prod.Name = 'test';
        prod.Stripe_Product_Id__c = 'abcd';
        prod.Stripe_SKU_Id__c = 'efgh';
        prod.IsActive = true;
        insert prod;

        Contact con = new Contact();
        con.LastName= 'test';
        insert con;

        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c = 'cus_0009oiukk';
        sp.Customer__c = con.Id;
        insert sp;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{ "order": { "id": "or_15iahK2eZvKYlo2CzKGgMVNl", "customer":{"id":"cus_0009oiukk"},"created": 1426898562, "object": "order", "livemode": false, "status": "created", "shipping": { "name": "Jenny Rosen", "address": { "line1": "1234 Main street", "line2": null, "city": "Anytown", "state": "CA", "postal_code": "123456", "country": "US" }, "phone": null, "tracking_number": null, "carrier": null }, "items": [ { "amount": 1500, "currency": "usd", "description": "Unisex / M", "object": "order_item", "parent": { "id": "'+ prod.Stripe_SKU_Id__c+'", "object": "sku", "active": true, "attributes": {}, "created": 1445530555, "currency": "usd", "image": null, "inventory": { "quantity": 42, "type":" finite", "value": null }, "livemode": false, "metadata": {}, "package_dimensions":null, "price": 1500, "product": { "id":"'+prod.Stripe_Product_Id__c+'", "active":"'+prod.IsActive+'" }, "updated": 1445530556 }, "type": "sku" } ], "amount": 1500, "currency": "usd", "charge": null } } ';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
   
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
        StripeShippingCalculation.calculateShipping();
        
        test.stopTest();    
            }
         catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        } 
    }  
      
    
}