@istest(SeeAllData = false)
public class ContactSMSUtilityTest{
    @isTest static void sendingSMS() {    
        
        
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.mobilePhone = '9876543210';
        insert cnt;
        
        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        Event s = new Event();
        s.isNew__c = true;
        s.Sent_By__c = userinfo.getuserId();
        s.Phone__c = '+19903569824';
        s.DurationInMinutes =5;
        s.ActivityDateTime = system.now();
        s.Contact__c = cnt.id;
        insert s;
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        PageReference pageRef = Page.ContactSMSUtility;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        
        ContactSMSUtility sms = new ContactSMSUtility();
        sms.toNumber= '+19902189392';
        sms.messageData = 'String';
        sms.smsId = s.Phone__c;
        sms.showTable = true;
        sms.phone = '1234567890';
        sms.sendSMS();
        sms.sendSMSNew();
        sms.showNumber();
        sms.reload();
        sms.back();
        sms.closePopup();
        sms.showPopup();
        Test.stopTest();
       
    }
    
    @isTest static void sendingSMS2() {    
        
        
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.Phone = '9876543210';
        cnt.OtherPhone = '9876543210';
        cnt.mobilePhone = '9876543210';
        insert cnt;
        
        Case caseObject = new Case();
        caseObject.subject = 'test';
        caseObject.contactId = cnt.id;
        insert caseObject;
        
        Event s = new Event();
        s.isNew__c = true;
        s.Sent_By__c = userinfo.getuserId();
        s.Phone__c = '9903569824';
        s.DurationInMinutes =5;
        s.ActivityDateTime = system.now();
        s.Contact__c = cnt.id;
        s.isNew__c= true;
        insert s;
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('tst'));
        PageReference pageRef = Page.ContactSMSUtility;
        pageRef.getParameters().put('id', String.valueOf(cnt.Id));
        Test.setCurrentPage(pageRef);
        
        ContactSMSUtility sms = new ContactSMSUtility();
        sms.toNumber= '9902189392';
        sms.messageData = 'String';
        sms.smsId = s.Phone__c;
        sms.sendSMS();
        sms.sendSMSNew();
        sms.smsId = '(971)3332204';
        sms.showNumber();
        sms.reload();
        sms.back();
        sms.closePopup();
        sms.showPopup();
        Test.stopTest();
       
    }
    
}