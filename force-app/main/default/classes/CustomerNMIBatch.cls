global class CustomerNMIBatch implements Database.Batchable<Account>, Database.AllowsCallouts, Database.Stateful{

    public Boolean startingAfter {get;set;}
    
    public CustomerNMIBatch(Boolean stAfter)
    {
        system.debug('###construct CustomerNMIBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        startingAfter = stAfter;
    }
    
	public List<Account> start(Database.BatchableContext BC ){
        system.debug('###start CustomerNMIBatch');
		List<Account> lstAccount = new List<Account>();
        Map<String,Account> mapAcc = new Map<String,Account>();
        HttpResponse res = new HttpResponse();
        Map<String,String> requestMap = new Map<String,String>();
        requestMap.put('report_type','customer_vault');
        //requestMap.put('start_date','20171201');
        //requestMap.put('end_date','20180102');
        if(startingAfter){
            Datetime dt = [SELECT Created_At__c from Account ORDER BY Created_At__c DESC NULLS LAST limit 1].Created_At__c;
            dt = dt.addSeconds(1);
            String afterDate = String.valueOf(dt.year());
            if(String.valueOf(dt.month()).length()==1){ afterDate = afterDate + '0' + dt.month(); }else{ afterDate = afterDate + dt.month(); }
            if(String.valueOf(dt.day()).length()==1){ afterDate = afterDate + '0' + dt.day(); }else{ afterDate = afterDate + dt.day(); }
            if(String.valueOf(dt.hour()).length()==1){ afterDate = afterDate + '0' + dt.hour(); }else{ afterDate = afterDate + dt.hour(); }
            if(String.valueOf(dt.minute()).length()==1){ afterDate = afterDate + '0' + dt.minute(); }else{ afterDate = afterDate + dt.minute(); }
            if(String.valueOf(dt.second()).length()==1){ afterDate = afterDate + '0' + dt.second(); }else{ afterDate = afterDate + dt.second(); }
            requestMap.put('start_date',afterDate);
        }
        res = PaymentGatewayNMI.HttpRequest(requestMap);

        if(res.getStatusCode()==200){
            //system.debug('##res: '+res.getBody());
            Account acc;
            String name;
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern MyPattern;
            Matcher MyMatcher;
            List<Dom.XmlNode> customers = new List<Dom.XmlNode>();
            customers = PaymentGatewayNMI.XmlParser(res.getBody());   
            for(Dom.XmlNode childElement1 : customers){
                for(Dom.XmlNode childElement2:childElement1.getChildElements()){
                    if(childElement2.getName()=='customer'){
                        acc = new Account();
                		name = '';
                        for(Dom.XmlNode childElement3:childElement2.getChildElements()){
                            system.debug('##childElement3: '+childElement3.getName()+' '+childElement3.getText());
                            if(childElement3.getName()=='customer_vault_id'){
                        		acc.NMI_External_ID__c = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='company'){
                                acc.Name = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='first_name'){
                                acc.First_Name__c = childElement3.getText();
                                name = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='last_name'){
                                acc.Last_Name__c = childElement3.getText();
                                name = name + ' ' + childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='phone'){
                        		acc.Phone = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='website'){
                        		acc.Website = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='email'){
                                if(childElement3.getText()!=null && childElement3.getText()!=''){
                                    MyPattern = Pattern.compile(emailRegex);
                                    MyMatcher = MyPattern.matcher(childElement3.getText());
                                    if (MyMatcher.matches()){
                                        acc.Email__c = childElement3.getText();
                                    }
                                }
                            }
                            
                            if(childElement3.getName()=='fax'){
                        		acc.Fax = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='address_1'){
                        		acc.BillingStreet = childElement3.getText();
                            }
                            if(childElement3.getName()=='city'){
                        		acc.BillingCity = childElement3.getText();
                            }
                            if(childElement3.getName()=='state'){
                        		acc.BillingState = childElement3.getText();
                            }
                            if(childElement3.getName()=='postal_code'){
                        		acc.BillingPostalCode = childElement3.getText();
                            }
                            if(childElement3.getName()=='country'){
                        		acc.BillingCountry = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='shipping_address_1'){
                        		acc.ShippingStreet = childElement3.getText();
                            }
                            if(childElement3.getName()=='shipping_city'){
                        		acc.ShippingCity = childElement3.getText();
                            }
                            if(childElement3.getName()=='shipping_state'){
                        		acc.ShippingState = childElement3.getText();
                            }
                            if(childElement3.getName()=='shipping_postal_code'){
                        		acc.ShippingPostalCode = childElement3.getText();
                            }
                            if(childElement3.getName()=='shipping_country'){
                        		acc.ShippingCountry = childElement3.getText();
                            }
                            
                            if(childElement3.getName()=='cc_number'){
                        		acc.Credit_Card_Number__c = childElement3.getText();
                            }
                            if(childElement3.getName()=='cc_exp'){
                                if(childElement3.getText()!=null && childElement3.getText().trim()!=''){
                                    acc.Exp_Month__c = String.valueOf(Integer.valueOf(childElement3.getText().subString(0,2)));
                                    acc.Exp_Year__c = Decimal.valueOf('20'+childElement3.getText().subString(2,childElement3.getText().length()));
                                }
                            }
                            
                            if(childElement3.getName()=='created'){
                                Integer year = Integer.valueOf(childElement3.getText().subString(0,4));
                                Integer month = Integer.valueOf(childElement3.getText().subString(4,6));
                                Integer day = Integer.valueOf(childElement3.getText().subString(6,8));
                                Integer hour = Integer.valueOf(childElement3.getText().subString(8,10));
                                Integer minute = Integer.valueOf(childElement3.getText().subString(10,12));
                                Integer second = Integer.valueOf(childElement3.getText().subString(12,14));
                                Datetime createdAt = Datetime.newInstance(year, month, day, hour, minute, second);
                        		acc.Created_At__c = createdAt;
                            }
                        }
                        
                        acc.Type = 'Customer';
                        if(acc.Name==null || acc.Name.trim()==''){
                            acc.Name = name;
                            if(acc.Name==null || acc.Name.trim()==''){
                            	acc.Name = acc.Email__c;
                                if(acc.Name==null || acc.Name.trim()==''){
                                    acc.Name = acc.NMI_External_ID__c;
                                }
                            }
                        }
                        //lstAccount.add(acc);
                        mapAcc.put(acc.NMI_External_ID__c,acc);
                    }
                }
            }
        }
        
        if(mapAcc.size()>0){
            lstAccount = mapAcc.values();
        }
        
        return lstAccount;
	}
	
	public void execute(Database.BatchableContext BC, List<Account> scope) {
        system.debug('###execute CustomerNMIBatch');
        if(scope.size()>0){
            ApexUtil.isTriggerInvoked = true;
            upsert scope NMI_External_ID__c;
            ApexUtil.isTriggerInvoked = false;
            
            List<Contact> lstContact = new List<Contact>();
            Contact c;
            for(Account a : scope){
                c = new Contact();
                c.NMI_External_ID__c = a.NMI_External_ID__c;
                if(a.Last_Name__c!=null && a.Last_Name__c.trim()!=''){
                    c.Firstname = a.First_Name__c;
                    c.LastName = a.Last_Name__c;
                }else{
                    if(a.First_Name__c!=null && a.First_Name__c.trim()!=''){
                        c.LastName = a.First_Name__c;
                    }else{
                        c.LastName = a.Name;
                    }
                }
                c.Description = a.Description;
                c.Email = a.Email__c;
                c.MailingCity = a.BillingCity;
                c.MailingCountry = a.BillingCountry;
                c.MailingPostalCode = a.BillingPostalCode;
                c.MailingState = a.BillingState;
                c.MailingStreet = a.BillingStreet;
                c.OtherCity = a.ShippingCity;
                c.OtherCountry = a.ShippingCountry;
                c.OtherPostalCode = a.ShippingPostalCode;
                c.OtherState = a.ShippingState;
                c.OtherStreet = a.ShippingStreet;
                c.Fax = a.Fax;
                c.Phone = a.Phone;
                c.AccountId = a.Id;
                lstContact.add(c);
            }
            
            ApexUtil.isTriggerInvoked = true;
            if(lstContact.size()>0) upsert lstContact NMI_External_ID__c;
            ApexUtil.isTriggerInvoked = false;
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish CustomerNMIBatch');
        if(!Test.isRunningTest()) Database.executeBatch(new TransactionNMIBatch(startingAfter));
    }

}