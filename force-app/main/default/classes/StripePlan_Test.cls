@IsTest
public class StripePlan_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'  \"created\": 1326853478,'+
		'  \"livemode\": false,'+
		'  \"id\": \"evt_00000000000000\",'+
		'  \"type\": \"plan.created\",'+
		'  \"object\": \"event\",'+
		'  \"request\": null,'+
		'  \"pending_webhooks\": 1,'+
		'  \"api_version\": \"2017-08-15\",'+
		'  \"data\": {'+
		'    \"object\": {'+
		'      \"id\": \"test1_00000000000000\",'+
		'      \"object\": \"plan\",'+
		'      \"amount\": 2500,'+
		'      \"created\": 1508947075,'+
		'      \"currency\": \"usd\",'+
		'      \"interval\": \"month\",'+
		'      \"interval_count\": 1,'+
		'      \"livemode\": false,'+
		'      \"metadata\": {'+
		'      },'+
		'      \"name\": \"Test Product 1\",'+
		'      \"statement_descriptor\": null,'+
		'      \"trial_period_days\": null'+
		'    }'+
		'  }'+
		'}';
		StripePlan.StripePlanObject r = StripePlan.StripePlanObject.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripePlan.StripePlanObject objJSON2Apex = new StripePlan.StripePlanObject(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.created == null);
		System.assert(objJSON2Apex.livemode == null);
		System.assert(objJSON2Apex.id == null);
		System.assert(objJSON2Apex.type_Z == null);
		System.assert(objJSON2Apex.object_data == null);
		//System.assert(objJSON2Apex.request == null);
		System.assert(objJSON2Apex.pending_webhooks == null);
		System.assert(objJSON2Apex.api_version == null);
		System.assert(objJSON2Apex.data == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripePlan.Object_Z objObject_Z = new StripePlan.Object_Z(System.JSON.createParser(json));
		System.assert(objObject_Z != null);
		System.assert(objObject_Z.id == null);
		System.assert(objObject_Z.object_data == null);
		System.assert(objObject_Z.amount == null);
		System.assert(objObject_Z.created == null);
		System.assert(objObject_Z.currency_data == null);
		System.assert(objObject_Z.interval == null);
		System.assert(objObject_Z.interval_count == null);
		System.assert(objObject_Z.livemode == null);
		System.assert(objObject_Z.metadata == null);
		System.assert(objObject_Z.name == null);
		System.assert(objObject_Z.statement_descriptor == null);
		System.assert(objObject_Z.trial_period_days == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripePlan.Metadata objMetadata = new StripePlan.Metadata(System.JSON.createParser(json));
		System.assert(objMetadata != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		StripePlan.Data objData = new StripePlan.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.object_data == null);
	}
}