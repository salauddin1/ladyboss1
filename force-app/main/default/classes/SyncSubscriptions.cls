global class SyncSubscriptions implements Database.AllowsCallouts,Database.Batchable<sObject> {
	String condition;

	global SyncSubscriptions(String condition){
		this.condition = condition;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select id, Customer_Id__c, Period_End__c, Is_Processed__c, Period_Start__c, Plan_ID__c,Subscription_Id__c,Status__c,Trial_Start__c,Trial_End__c FROM Subscription__c WHERE '+condition);
	}

	global void execute(Database.BatchableContext BC, List<Subscription__c> scope) {
		List<String> subid = new List<String>();
		List<Product2> productToUpdate = new List<Product2>();
		Map<String, Stripe_Profile__c> sObjectMap = new Map<String, Stripe_Profile__c>();
		List<PricebookEntry> pricebookToInsert = new List<PricebookEntry>();
		Map<String,Subscription__c> subIDs = new Map<String,Subscription__c>();
		Map<String, Opportunity> oppMap = new Map<String, Opportunity>();
		List<String> subNewOpp = new List<String>();
		List<OpportunityLineItem> oppLIs = new List<OpportunityLineItem>();
		for (Subscription__c sObj: scope) {
			sObj.Is_Processed__c = true;
			subIDs.put(sObj.Subscription_Id__c, sObj);      
		}
		subid.addAll(subIDs.keySet());
		
		for (OpportunityLineItem opli: [SELECT id,Subscription_Id__c FROM OpportunityLineItem WHERE Subscription_Id__c in :subid]) {
			if (subid.contains(opli.Subscription_Id__c)) {
				subid.remove(subid.indexOf(opli.Subscription_Id__c));
			}      
		}

		if (subid.size()>0) {
			Set<String> planIds = new Set<String>();
			for (String subidFromStripe:subid) {
				planIds.add(subIDs.get(subidFromStripe).Plan_ID__c);                        
			}          
			List<product2> prods= [SELECT id,Name,Price__c,Stripe_plan_Id__c FROM Product2 WHERE Stripe_plan_Id__c IN :planIDs ORDER BY CreatedDate DESC];          
			List<PriceBookEntry> priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_plan_Id__c FROM PriceBookEntry WHERE Product2Id in :prods AND PriceBook2.isStandard=true AND IsActive=true ORDER BY CreatedDate DESC];
			Map<String,PriceBookEntry> priceBookMap = new Map<String,PriceBookEntry>();
			Map<String,Product2> prodMap = new Map<String,Product2>();
			for(PriceBookEntry pbookEntry:priceBookList){
				priceBookMap.put(pbookEntry.Product2.Stripe_plan_Id__c,pbookEntry);
			}
			for(product2 prod:prods){
				prodMap.put(prod.Stripe_plan_Id__c,prod);
			}
			Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];
			for (String planid: planIds) {
				if (!prodMap.containsKey(planid)) {
					if (planid == 'Unlimited Access - Coaching') {
						planid = ' Unlimited Access - Coaching';
					}

					StripeGetPlan splan = StripeGetPlan.getPlan(planid);
					StripeCreateProduct scp = StripeCreateProduct.getProduct(splan.product);
					Product2 product = new Product2();          
					product.Name = scp.name;          
					product.Price__c = splan.amount/100;
					product.Stripe_Plan_Id__c = planid;
					productToUpdate.add(product);
				}
			}

			for (Opportunity opp: [SELECT id,Subscription__c FROM Opportunity WHERE Subscription__c in :subid ORDER BY CreatedDate DESC]) {
				oppMap.put(opp.Subscription__c, opp);
			}
			for (String subidFromStripe: subid) {
				if (!oppMap.containsKey(subidFromStripe)) {
					subNewOpp.add(subidFromStripe);
				}
			}
			if (subNewOpp.size()>0) {
				Map<String,String> cusMap = new Map<String,String>();
				List<String> customers = new List<String>();
				List<Stripe_Profile__c> spList = new List<Stripe_Profile__c>();
				List<Contact> conToInsert = new List<Contact>();
				for (String subidFromStripe: subNewOpp) {
					cusMap.put(subidFromStripe, subIDs.get(subidFromStripe).Customer_Id__c);
				}
				customers.addAll(cusMap.values());          
				for (Stripe_Profile__c sObj: [SELECT id, Customer__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c =:customers ORDER BY CreatedDate DESC]) {
					sObjectMap.put(sObj.Stripe_Customer_Id__c, sObj);
				}
				for (String subidFromStripe: subNewOpp) {
					if (!sObjectMap.containsKey(cusMap.get(subidFromStripe))) {
						StripeCustomer sc = StripeCustomer.getCustomer(cusMap.get(subidFromStripe));
						Contact con = new Contact();
						if (sc.email!=null) {
							con.LastName = sc.email;
							con.Email = sc.email;
							}else {
								con.LastName  = sc.id;
							}
							con.Customer_Id__c = cusMap.get(subidFromStripe);
							conToInsert.add(con);
						}
					}
					if (conToInsert.size()>0) {
						insert conToInsert;
					}
					for (Contact cont: conToInsert) {
						Stripe_Profile__c sp = new Stripe_Profile__c();
						sp.Customer__c  = cont.id;
						sp.Stripe_Customer_Id__c = cont.Customer_Id__c;
						spList.add(sp);
					}
					if (spList.size()>0) {
						insert spList;
					}
					for (Stripe_Profile__c sObj: spList) {
						sObjectMap.put(sObj.Stripe_Customer_Id__c, sObj);          
					}  
				}  
				if (productToUpdate.size()>0) {
					insert productToUpdate;
				}
				for (Product2 pr: productToUpdate) {
					PricebookEntry pbe = new PricebookEntry (Pricebook2Id=pb.id, Product2Id=pr.id, IsActive=true, UnitPrice=pr.Price__c);
					pricebookToInsert.add(pbe);        
				}
				if (pricebookToInsert.size()>0) {
					insert pricebookToInsert;
				}

				for (Product2 sObj: productToUpdate) {
					prodMap.put(sObj.Stripe_plan_Id__c, sObj);
				}
				for (PricebookEntry sObj: [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_plan_Id__c FROM PriceBookEntry WHERE ID in:pricebookToInsert]) {
					priceBookMap.put(sObj.Product2.Stripe_plan_Id__c,sObj);
				}
				if (subNewOpp.size()>0) {    
					List<Opportunity> opps = new List<Opportunity>();
					RecordType rec = [SELECT Id FROM RecordType WHERE Name = 'Subscription'];
					for (String subidFromStripe: subNewOpp) {

						Product2 prod= prodMap.get(subIDs.get(subidFromStripe).Plan_ID__c);
						Opportunity opp = new Opportunity();
						opp.Name = prod.Name+'CLUB';
						opp.Contact__c = sObjectMap.get(subIDs.get(subidFromStripe).Customer_Id__c).Customer__c;
						opp.RecordTypeId=rec.id;
						opp.StageName = 'Closed Won';          
						opp.Subscription__c = subidFromStripe;
          //datetime dt = datetime.newInstance(Long.ValueOf(subIDs.get(subidFromStripe).Period_End__c)*Long.ValueOf('1000'));
          opp.CloseDate = subIDs.get(subidFromStripe).Period_End__c.date();  
          opps.add(opp);          
        }
        if (opps.size()>0) {
        	insert opps;
        }
        for(Opportunity opp:opps){
        	oppMap.put(opp.Subscription__c, opp);
        }
      }        

      for (String subidFromStripe:subid) {  

      	Product2 prod= prodMap.get(subIDs.get(subidFromStripe).Plan_ID__c);
      	PriceBookEntry priceBook = priceBookMap.get(subIDs.get(subidFromStripe).Plan_ID__c);
      	System.debug('PriceBookEntry'+priceBook);            
      	Opportunity opp = oppMap.get(subidFromStripe);              
      	OpportunityLineItem opline = new OpportunityLineItem();
      	opline.OpportunityId = opp.Id;
      	opline.PricebookEntryId=priceBook.Id;
      	opline.Product2Id = prod.Id;
      	if (subIDs.get(subidFromStripe).Status__c =='active') {
      		opline.Status__c = 'Active';
      		}else if (subIDs.get(subidFromStripe).Status__c =='canceled') {
      			opline.Status__c = 'InActive';
      			}else if (subIDs.get(subidFromStripe).Status__c=='trialing'){
      				opline.Status__c = 'trialing';
      				opline.TrialPeriodStart__c =subIDs.get(subidFromStripe).Trial_Start__c.date(); 

      				opline.TrialPeriodEnd__c =subIDs.get(subidFromStripe).Trial_End__c.date();
      			}

        //datetime dt = datetime.newInstance(Long.ValueOf(subIDs.get(subidFromStripe).Period_End__c)*Long.ValueOf('1000'));
        opline.End__c = subIDs.get(subidFromStripe).Period_End__c.date();
        opline.TotalPrice = prod.Price__c;
        opline.Subscription_Id__c=subidFromStripe;
        //datetime dt1 = datetime.newInstance(Long.ValueOf(subIDs.get(subidFromStripe).Period_Start__c)*Long.ValueOf('1000'));
        opline.Start__c= subIDs.get(subidFromStripe).Period_Start__c.date();
        opline.quantity =1;           
        opline.Plan_ID__c = subIDs.get(subidFromStripe).Plan_ID__c; 

        oppLIs.add(opline);                  

      }    
      if (oppLIs.size()>0) {
      	insert oppLIs;
      }              
    }  
    List<Subscription__c> sc = subIDs.values();
    update sc;                 
  }
  
  global void finish(Database.BatchableContext BC) {

  }
  
}