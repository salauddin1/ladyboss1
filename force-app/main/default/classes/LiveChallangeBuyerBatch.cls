/*
* Developer Name : Tirth Patel
* Description : This batch checks for Live Challenge Customers where classification contains "LIVEChallenge".Then from email of such customers 
* if finds all customer records with similar email on it and checks "Actual LIVE Challenge Buyer" for all such customers. Similarly it also
* checks for Customers where classification contains "FreeInviteeLC".Then from email of such customers it finds all customer records having similar
* email and checks "Actual LIVE Challenge Invitee" for all such customer records.
*/
global class LiveChallangeBuyerBatch implements Database.Batchable<sObject>,Schedulable  {
    global void execute(SchedulableContext ctx) {
        LiveChallangeBuyerBatch cb = new LiveChallangeBuyerBatch();
        Database.executeBatch(cb);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String[] classes = new String[]{'%LIVEChallenge%','%FreeInviteeLC%'};
            return Database.getQueryLocator([SELECT Id,Email_of_Purchaser__c,Classification__c FROM LIVE_Challenge_Customer__c WHERE Classification__c like : classes and Email_of_Purchaser__c!=null]);
    }
    global void execute(Database.BatchableContext bc, List<LIVE_Challenge_Customer__c> records){
        List<String> LIVEChallengeList = new List<String>();
        List<String> emailList = new List<String>();
        for(LIVE_Challenge_Customer__c lc : records){
            if(lc.Classification__c.toLowerCase().contains('livechallenge')){
                LIVEChallengeList.add(lc.Email_of_Purchaser__c.tolowerCase());
            }
            emailList.add(lc.Email_of_Purchaser__c);
        }
        List<LIVE_Challenge_Customer__c> lcList = [SELECT Id,Email_of_Purchaser__c,Actual_LIVE_Challenge_Buyer__c FROM LIVE_Challenge_Customer__c WHERE Email_of_Purchaser__c in : emailList];
        for(LIVE_Challenge_Customer__c lc : lcList){
            if(LIVEChallengeList.Contains(lc.Email_of_Purchaser__c.tolowerCase())){
                lc.Actual_LIVE_Challenge_Buyer__c = true;
            }else{
                lc.Actual_LIVE_Challenge_Invitee__c	= true;
            }
        }
        update lcList;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}