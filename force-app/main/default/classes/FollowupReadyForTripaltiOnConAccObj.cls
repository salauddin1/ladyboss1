// Developer Name :- Surendra Patidar
// class contains code about create task whenever lead spent more then 1 week and one month with organization
// =================================================================================================================
public class FollowupReadyForTripaltiOnConAccObj implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful,Schedulable   {
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //Query lead records basis of hours and hours flags
        return Database.getQueryLocator('select id, name, Age__c, Hours__c, ConvertedAccountId, ConvertedContactId, Email, Status, isconverted, MobilePhone from Lead where Age__c >= 7');
    }
    public void execute(SchedulableContext SC) {
        Database.executeBatch(new FollowupReadyForTripaltiOnConAccObj());
    }
    public void execute(Database.BatchableContext BC, List<Lead> scope) {
        System.debug('=====***************====='+scope);
        Set<String> conIdSet = new Set<String>();
        //Set<String> AccIdSet = new Set<String>();
        List<Contact> conList = new List<Contact>();
        List<Task> taskList = new List<Task>();
        Map<String, Contact> conMap = new Map<String, Contact>();
        List<Account> AccList = new List<Account>();
        List<Task> extTask = new List<Task>();
        Map<String, task> taskMap = new Map<String, task>();
        if(scope.size()>0) {
            for (lead led : scope) {
                if(led.IsConverted == true) {
                    conIdSet.add(led.ConvertedContactId);
                }
                if(Test.isRunningTest())   {
                    Contact con = new Contact();
                    con.LastName= 'MyLastName';
                    con.MailingCity ='US';
                    con.MailingCountry = 'US';
                    con.MailingState ='US';
                    con.MailingPostalCode ='US';
                    insert con;
                    conIdSet.add(con.Id);
                    System.debug('==============='+con.Id);
                    Test.setCreatedDate(con.Id, DateTime.now().addDays(-8));
                }
                //AccIdSet.add(led.ConvertedAccountId);
            }
            extTask = [select id, subject, whoId from task where whoId IN : conIdSet and subject like 'Hello %, This contact record for % still is not ready in Tipalti. Please follow up'];
            System.debug('======exttaskexttaskexttaskexttask======'+exttask);
            for(task t : extTask) {
                taskMap.put(t.whoId, t);
            }
            conList = [select id, Name, age__c,owner.name, createdDate, OwnerId, Ready_in_Tipalti__c, AccountId, Account.Ready_in_Tipalti__c, Account.createddate, Account.OwnerId from contact where Id IN:conIdSet];
            for(contact con : conList) {
                System.debug('===con.age__c===='+con.age__c);
                if(con.age__c >= 7 && con.Ready_in_Tipalti__c == false && !taskMap.containskey(con.Id)) {
                    Task tsk = new Task();
                    tsk.Subject = 'Hello '+con.owner.name+', This contact record for '+con.Name+' still is not ready in Tipalti. Please follow up';
                    tsk.WhoId = con.Id;
                    tsk.WhatId = con.AccountId;
                    tsk.Priority = 'High';
                    tsk.status = 'Open';
                    tsk.OwnerId = con.OwnerId;
                    taskList.add(tsk);
                }
            }
            System.debug('=====***************====='+taskList);
            if(taskList.size()>0) {
                insert taskList;
            }
        }
    }
    public void finish(Database.BatchableContext BC) {
        
    }
}