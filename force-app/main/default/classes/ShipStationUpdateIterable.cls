global class ShipStationUpdateIterable implements iterable<Integer>{
   global Iterator<Integer> Iterator(){
      return new ShipstationUpdateIterator();
   }
}