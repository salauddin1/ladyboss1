//Developer Name : Surendra patidar
//Class is associated with EmailTempLAC component whixh we can directly send email From Lead,Contact and account Page
public class EmailTempForLAC {
    
    @AuraEnabled
    //LACId means lead,account or contact id using this Id load the default email addres on the component
    public static String loadexistingEmail (String LACId) {
        System.debug('========='+LACId);
        String objectName = SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(LACId); 
        System.debug('=============='+objectName);
        String email = '';
        if(objectName == 'Account') {
            Account act = [select id,Email__c,Name,Lead_Email__c from account where Id =: LACId];
            if(act.Email__c != null && act.Email__c != '') {
                email = act.Email__c;
            }
            else if(act.Lead_Email__c != null && act.Lead_Email__c != ''){
                email = act.Lead_Email__c;
            }
            
        }
        if(objectName == 'Contact') {
            Contact con = [select id,Email,Name from contact where Id =: LACId];
            email = con.Email;
        }
        if(objectName == 'Lead') {
            Lead led = [select id,Email,Name from lead where Id =: LACId];
            email = led.Email;
        }
        return email;
    }
    @AuraEnabled 
    public static void sendMailMethod(String mMail ,String mSubject ,String mbody, String recordId){
        System.debug('==============='+recordId);
        String objectName = SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(recordId);
        User usr = [select email,Name from user where Id =:Userinfo.getUserId()];
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
        
        //Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Set list of people who should get the email	
        List<String> sendTo = new List<String>();
        sendTo.add(mMail);
        mail.setToAddresses(sendTo);
        
        // Set who the email is sent from
        mail.setReplyTo(usr.email); // change it with your mail address.
        mail.setSenderDisplayName(usr.Name+' LadyBoss Agent'); 
        
        //Set email contents - you can use variables!
        mail.setSubject(mSubject);
        mail.setHtmlBody(mbody);
        
        //Add your email to the mails list
        mails.add(mail);
        // Send all emails in the master list
        Messaging.SendEmailResult[] results= Messaging.sendEmail(mails);
        System.debug('====================='+results);
        try{
        if(results[0].isSuccess() == true) {
            if(objectName == 'Account') {
                EmailMessage em = new EmailMessage();
                em.FromAddress = usr.email;
                em.ToAddress = mMail;
                em.RelatedToId = recordid;
                em.Subject = mSubject;
                em.HtmlBody = mbody;
                insert em;
            }
            
            if(objectName == 'Contact' || objectName == 'Lead') {
                task tsk1 = new task ();
                tsk1.whoId = recordId;
                tsk1.Mail_Subject__c = mSubject;
                tsk1.From_Address__c = usr.Email;
                tsk1.To_Address__c = mMail;
                tsk1.Priority = 'Normal';
                tsk1.Description = mbody.stripHtmlTags();
                tsk1.status = 'Open';
                tsk1.Subject = usr.Email+' sent an email to '+mMail;//
                tsk1.Email__c = usr.Email;
                insert tsk1;
                
            }
        }
        }
        catch(exception e){}
    }   
}