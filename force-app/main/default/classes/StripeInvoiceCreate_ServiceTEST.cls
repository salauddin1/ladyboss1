@isTest
public class StripeInvoiceCreate_ServiceTEST {
	
    public static testMethod void test2(){
        ContactTriggerFlag.isContactBatchRunning = true;
    	test.startTest();
        Plan_Upgrade__c plu = new Plan_Upgrade__c();
        plu.enabled__c = true;
        plu.Old_Plan_ID__c = 'ttmemberships';
        plu.New_Plan_ID__c='updatedtest';
        plu.Name='test';
        insert plu;
        Add_Tax_To_Subs__c addTax = new Add_Tax_To_Subs__c();
        addTax.Add_Tax__c = true;
        //addTax.Add_Shipping__c = true;
        insert addTax; 
        Product2 prod = new Product2();
        prod.Name = 'test';
        prod.Stripe_Product_Id__c = 'abcd';
        prod.Stripe_SKU_Id__c = 'efgh';
        prod.IsActive = true;
        prod.Is_Eligible_For_Commission_Usage__c = true;
        prod.Price__c = 18;
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;

        Contact con = new Contact();
        con.LastName= 'test';
        con.Available_Commission__c = 20.02;
        insert con;
        Address__c add = new Address__c();
        add.Shipping_City__c ='test city';
        add.Shipping_Country__c = 'test country';
        add.Shipping_Street__c = 'street';
        add.Shipping_Zip_Postal_Code__c ='567890';
        add.Shipping_State_Province__c = 'state';
        add.Contact__c =con.id;
        add.Primary__c =true;
        insert add;

        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c = 'cus_EjBPAu2NGliSje';
        sp.Customer__c = con.Id;
        insert sp;

        Stripe_Profile__c sp1 = new Stripe_Profile__c();
        sp1.Stripe_Customer_Id__c = 'cus_EjBPAu2NGliSjk';
        sp1.Customer__c = con.Id;
        sp1.Balance__c = 2.03;
        insert sp1;

        opportunity op  = new opportunity();
        
        op.name='opclub';
        op.Contact__c =con.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.Clubbed__c = true;
        op.Stripe_Message__c = 'Success';
        op.CloseDate = System.today();
        op.Created_Using__c = 'V10';
        op.Shipping_Cost__c = 10.23;
        insert op;

        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Subscription_Id__c = 'sub_EsAKYsztOxRLZJ';
        ol.Status__c = 'Active';
        
        insert ol;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/StripeInvoiceUpdate_Service'; 
        
        String body = '{ "id": "evt_1EOVYqFzCf73siP0CtvlRQLb", "object": "event", "api_version": "2018-02-28", "created": 1555099280, "data": { "object": { "object": "invoice", "amount_due": 2700, "amount_paid": 0, "amount_remaining": 2700, "application_fee": null, "attempt_count": 0, "attempted": false, "billing": "charge_automatically", "billing_reason": "upcoming", "charge": null, "closed": true, "created": 1555704026, "currency": "usd", "custom_fields": null, "customer": "cus_EjBPAu2NGliSje", "date": 1555704026, "default_source": null, "description": "", "discount": null, "due_date": null, "ending_balance": 0, "finalized_at": null, "footer": null, "forgiven": false, "lines": { "object": "list", "data": [ { "id": "sub_EsAKYsztOxRLZJ", "object": "line_item", "amount": 2700, "currency": "usd", "description": "1 × ttmembership (at $27.00 / month)", "discountable": true, "livemode": false, "metadata": { }, "period": { "end": 1558296026, "start": 1555704026 }, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 2700, "billing_scheme": "per_unit", "created": 1436389677, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": { }, "nickname": "monthly", "product": "prod_BUtVNOL8lmvzIG", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_18RnRDFzCf73siP04XMZ1uCX", "type": "subscription" } ], "has_more": false, "total_count": 1, "url": "/v1/invoices/upcoming/lines?customer=cus_6b2iUj7Y0dFiZL\u0026subscription=sub_6b2iEjeeGraZiR" }, "livemode": false, "metadata": { }, "next_payment_attempt": null, "number": "B1137FA-0026", "paid": false, "period_end": 1555704026, "period_start": 1553025626, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "draft", "status_transitions": { "finalized_at": null, "marked_uncollectible_at": null, "paid_at": null, "voided_at": null }, "subscription": "sub_6b2iEjeeGraZiR", "subtotal": 2700, "tax": null, "tax_percent": null, "total": 2700, "webhooks_delivered_at": null } }, "livemode": false, "pending_webhooks": 5, "request": { "id": null, "idempotency_key": null }, "type": "invoice.upcoming" } ';  
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
   
        
        StripeInvoiceCreate_Service.updateInvoice();
        
        test.stopTest();    
            
    }  
}