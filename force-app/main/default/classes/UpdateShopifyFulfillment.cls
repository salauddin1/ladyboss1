@RestResource(urlMapping='/Fulfillment')
global class UpdateShopifyFulfillment {
	@HttpPost
    global static void getfulfillments() {
         if(RestContext.request.requestBody!=null  ){
            try {
                String str = RestContext.request.requestBody.toString();
                System.debug('-------'+str);
                
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                List<Object> listItemsList = (List<object>)results.get('line_items');
                Map<String, Object> itemIdMap = new Map<String, Object>();
                Set<String> itemIdSet = new Set<String> ();
                String Statusfulfillment;
                
                for(Object item : listItemsList) {
                    itemIdMap = (Map<String, Object>)item;
                    itemIdSet.add(String.valueOf(itemIdMap.get('id')));
                }
                
                List<ShopifyLineItem__c> exixtingItems = [select id,Item_Id__c from ShopifyLineItem__c where Item_Id__c IN : itemIdSet];
                if(!exixtingItems.isEmpty()) {
                    for(ShopifyLineItem__c updateitem: exixtingItems) {
                        updateitem.Status__c = String.valueOf(results.get('status'));
                    }
                }
                update exixtingItems;
            }
             catch(Exception e) {}
         }
    }
}