/* Developer    : Sourabh Badole
* Description : We are performing insert and delete operation the campaign Member from Campaign Members Batch Handler.
*/
public class CampaignMembers_Batch_Helper {
    public static batchWrapper updateCampaignMembers(List<Campaign> campaignList , List<CampaignMember> campaignMemberList,Map<Id,List<Workflow_Configuration__c>> mapWorkflowConfig, Map<Id,List<CampaignMember>> conLeadCampMemberMap){
        
        // Filter Campaign members to be changed and create a map of age       
        Map<Decimal , List<CampaignMember>> changeList = new Map<Decimal , List<CampaignMember>>();
        for(CampaignMember cm : campaignMemberList){
            if(cm.Campaign.Min_Age__c != null && cm.Campaign.Max_Age__c != null){
                if(!(cm.Age_In_Hours__c >= cm.Campaign.Min_Age__c && cm.Age_In_Hours__c <= cm.Campaign.Max_Age__c)){
                    if(changeList.containsKey(cm.Age_In_Hours__c)){
                        changeList.get(cm.Age_In_Hours__c).add(cm);
                    }else{
                        changeList.put(cm.Age_In_Hours__c, new List<CampaignMember>{cm});
                    }
                }
            }else if(cm.Campaign.Min_Age__c != null && cm.Campaign.Max_Age__c == null){
                if(!(cm.Age_In_Hours__c >= cm.Campaign.Min_Age__c)){
                    if(changeList.containsKey(cm.Age_In_Hours__c)){
                        changeList.get(cm.Age_In_Hours__c).add(cm);
                    }else{
                        changeList.put(cm.Age_In_Hours__c, new List<CampaignMember>{cm});
                    }
                }
            }
        }
        system.debug('*** changeList'+changeList);
        
        // Create age campaign map for the campaign members to changed
        Map<Decimal,String> ageCampaignMap = new Map<Decimal,String>();
        for(Campaign c : campaignList){
            for(Decimal age : changeList.keySet()){
                system.debug(c +'' +' '+age);
                if(c.Min_Age__c != null && c.Max_Age__c != null){
                    if(age >= c.Min_Age__c && age <= c.Max_Age__c){
                        ageCampaignMap.put(age, c.Id);
                    }
                }else  if(c.Min_Age__c != null && c.Max_Age__c == null){
                    if(age >= c.Min_Age__c){
                        ageCampaignMap.put(age, c.Id);
                    }
                }
            }
        }
        system.debug('**** ageCampaignMap'+ageCampaignMap);
        
        
        // Create Campaign Members Insert List
        List<CampaignMember> deleteList = new List<CampaignMember>();
        List<CampaignMember> insertList = new List<CampaignMember>();
        
        CampaignMember c = new CampaignMember();
        for(Decimal age : ageCampaignMap.keySet()){
            for(CampaignMember cm : changeList.get(age)){
            
            
                List<CampaignMember> cmLeadCheckList = new List<CampaignMember>();
                if(conLeadCampMemberMap != null && conLeadCampMemberMap.containsKey(cm.ContactId)){
                  cmLeadCheckList = conLeadCampMemberMap.get(cm.ContactId) ;
                  system.debug('*****   cmLeadCheckList    *****'+cmLeadCheckList );
                }
                if(cmLeadCheckList != null && !cmLeadCheckList.isEmpty()){
                    deleteList.addAll(cmLeadCheckList);
                    system.debug('*****  deleteList    *****'+deleteList);
                }
               
            
            
                if(cm.Campaign.Marketing_Cloud_Tags__c==null){
                    deleteList.add(cm);
                }
                
                c= cm.clone();
                
                system.debug('-----------mapWorkflowConfig------------'+mapWorkflowConfig);
                Boolean flag = false ;        
                
                if(mapWorkflowConfig.containsKey(c.CampaignId)){
                    for( Id    mListId : mapWorkflowConfig.keyset() ){
                        List<Workflow_Configuration__c> workConList =  mapWorkflowConfig.get(c.CampaignId);
                        
                        system.debug('-----------workConList------------'+workConList );
                      
                        if( workConList != null ){
                            for(Workflow_Configuration__c wcInfo : workConList ){
                                if(cm.Contact.Product__c != null)
                                if(wcInfo.Special_Movement__c == true && cm.Contact.Product__c.contains(wcInfo.Products__c)){
                                    if(wcInfo.Target_Campaign__c != null){
                                        c.CampaignId = wcInfo.Target_Campaign__c ;   
                                        flag =  true ; 
                                    }
                                }          
                            }
                        }     
                    }
                }
                
                if(flag == false){
                    c.CampaignId = ageCampaignMap.get(age);
                }
                
                
                system.debug('*** updated member '+cm.Campaign.Name);
                insertList.add(c);
            }
        }
        System.debug('*** batch handler delete List*** '+deleteList.size()+' '+deleteList);
        System.debug('*** batch handler Insert List*** '+insertList.size()+' '+insertList);
        
        
        //Sent insert and delete list to queues 
        batchWrapper bw = new batchWrapper();
        bw.CMDeleteList = deleteList;
        System.debug('*** Wrapper deleteList*** '+bw.CMDeleteList.size()+' '+bw.CMDeleteList);
        bw.CMInsertList = insertList;
        System.debug('*** Wrapper InsertList*** '+bw.CMInsertList.size()+' '+bw.CMInsertList);
        return bw;
    }
  
    
  
  
    public class batchWrapper{
        public List<CampaignMember> CMDeleteList;
        public List<CampaignMember> CMInsertList;
    }
    
 
}