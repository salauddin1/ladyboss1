@isTest
global class MockTestClassAddressValidation implements HttpCalloutMock {
    HttpResponse res;
    global MockTestClassAddressValidation(HttpResponse r){
        res = r;
    }
    global HTTPResponse respond(HTTPRequest req) {
        return res;
    }
}