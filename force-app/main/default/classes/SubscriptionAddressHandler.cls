global class SubscriptionAddressHandler {
	
    //Used to change tax percentage on subscrition on address change
	public static void setTaxPercentage(Address__c address){
		for(Opportunity opp:[SELECT id,Name,Created_Using__c FROM Opportunity WHERE Contact__c=:address.Contact__c AND Contact__c != null AND Created_Using__c IN ('V10','V8')]){
			if(opp.Name.contains('club')){
                for(OpportunityLineItem oli:[SELECT id,Subscription_Id__c,Product2Id,Tax_Percentage_s__c,Opportunity.Created_Using__c FROM OpportunityLineItem WHERE OpportunityId=:opp.id AND Subscription_Id__c!=null AND Tax_Percentage_s__c!=null AND Status__c IN ('Active','trialing')]){
                    List<Product2> prodList = [SELECT id,Tax_Code__c FROM Product2 WHERE id=:oli.Product2Id];
                    for(Product2 prod:prodList){
                        If(system.isBatch()){
                           SubscriptionAddressHandler.ratemethodnotfuture(oli.Subscription_Id__c,address.Shipping_Street__c,address.Shipping_City__c,address.Shipping_State_Province__c,address.Shipping_Country__c,address.Shipping_Zip_Postal_Code__c,1,1,prod.Tax_Code__c); 
                        }else{
                          SubscriptionAddressHandler.ratemethodfuture(oli.Subscription_Id__c,address.Shipping_Street__c,address.Shipping_City__c,address.Shipping_State_Province__c,address.Shipping_Country__c,address.Shipping_Zip_Postal_Code__c,1,1,prod.Tax_Code__c);
                        }
                    }                
                }
			}
		}
	}
	//Update subscription with latest tax rate
	global static void updateSubscription(String subscriptionId,Decimal taxPercentage,String line1,String city,String region,String country,String postalCode) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+subscriptionId);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        if(taxPercentage==null){
        	taxPercentage=0;
        }
        String billingAddress = line1+' '+city+' '+region+' '+country+' '+postalCode;
        http.setBody('tax_percent='+taxPercentage+'&metadata[Street]='+line1+'&metadata[City]='+city+'&metadata[State]='+region+'&metadata[Postal Code]='+postalCode+'&metadata[Country]='+country+'&metadata[Shipping Address]='+billingAddress);  
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('Error in updation'+e);
            }
        } else {
            //hs.setBody(StripeCustomerTests.testData_updateSubscription);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        
    }
    //TO calculate tax rate based on updated address
     @future(callout=true)
    public static void ratemethodfuture(String oliId,String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode){
        TaxjarTaxCalculate tx = TaxjarTaxCalculate.ratemethod(line1, city, region, country, postalCode, amount, quantity, taxCode);
        System.debug('taxjar'+tx);
        if (tx != null && tx.tax != null && tx.tax.rate != null) {  
            Decimal taxPercentage = tx.tax.rate*100; 
            updateSubscription(oliId, taxPercentage.setScale(2), line1, city, region, country, postalCode); 
        }else {
            updateSubscription(oliId, 0, line1, city, region, country, postalCode);
        }     
    }
    //TO calculate tax rate based on updated address
    public static void ratemethodnotfuture(String oliId,String line1,String city,String region,String country,String postalCode,Decimal amount, Integer quantity,String taxCode){
        TaxjarTaxCalculate tx = TaxjarTaxCalculate.ratemethod(line1, city, region, country, postalCode, amount, quantity, taxCode);
        if (tx != null && tx.tax.rate != null) {  
            Decimal taxPercentage = tx.tax.rate*100; 
            updateSubscription(oliId, taxPercentage.setScale(2), line1, city, region, country, postalCode); 
        }else {
            updateSubscription(oliId, 0, line1, city, region, country, postalCode);
        }          
    }
}