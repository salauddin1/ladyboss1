@isTest
public class StripeGetPlan_Test {
    
    public static String testData_getPlan {
        get {
            return '{'+
                '  "interval": "year",'+
                '  "name": "Cirrus Insight Annual Subscription",'+
                '  "created": 1354825845,'+
                '  "amount": 0,'+
                '  "currency": "usd",'+
                '  "id": "CI_ANNUALLY",'+
                '  "object": "plan",'+
                '  "livemode": false,'+
                '  "interval_count": 1,'+
                '  "trial_period_days": null,'+
                '  "statement_description": null'+
                '}';
        }
    }
    
    public static String testData_getPlanList {
        get {
            return '{'+
                '  "object": "list",'+
                '  "url": "/v1/plans",'+
                '  "has_more": false,'+
                '  "data": ['+
                '    {'+
                '    "interval": "year",'+
                '    "name": "Cirrus Insight Annual Subscription",'+
                '    "created": 1354825845,'+
                '    "amount": 0,'+
                '    "currency": "usd",'+
                '    "id": "CI_ANNUALLY",'+
                '    "object": "plan",'+
                '    "livemode": false,'+
                '    "interval_count": 1,'+
                '    "trial_period_days": null,'+
                '    "metadata": {'+
                '    },'+
                '    "statement_description": null'+
                '  }'+
                '  ]'+
                '}';
        }
    }
    
    private static testMethod void testParse() {
        String json = testData_getPlan;
        StripeGetPlan plan = StripeGetPlan.parse(json);
        
        System.assertEquals('year', plan.interval);
        System.assertEquals(0, plan.amount);
    }
    private static testMethod void testGetPlan() {
        List<Id> prodList= new List<Id>();
        Product2 p = new Product2();
        p.Name='test';
        p.Stripe_Product_Id__c= 'abc';
        p.Price__c= 100;
        p.IsActive=true;
        insert p;
        prodList.add(p.Id);
        StripeGetPlan plan = StripeGetPlan.getPlan('CI_ANNUALLY');
        StripeGetPlan.getPlan(prodList);
        StripeGetPlan.createPlan(prodList);
        System.assertEquals('year', plan.interval);
        System.assertEquals(0, plan.amount);
    }
}