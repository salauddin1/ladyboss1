/*
 MasterContactBatch bt = new MasterContactBatch();
 Database.executeBatch(bt);
*/
global class MasterContactBatch implements  Database.Batchable<sObject>, Database.Stateful{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT id,email,Is_Master__c from contact where email != null and email!=\'notavail@gmail.com\' and Is_Master__c=false';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Contact> cntList){
        ContactTriggerFlag.isContactBatchRunning = true;
        for(Contact ct : cntList){
            ct.Is_Master__c = true;
        }
        update cntList;
    }
    global void finish(Database.BatchableContext bc){
        
    }
}