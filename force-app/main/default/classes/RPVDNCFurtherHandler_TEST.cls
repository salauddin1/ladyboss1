@isTest
public class RPVDNCFurtherHandler_TEST {
 @isTest
   
    public Static void RpvDNCPhoneTest(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        List<String> Conlist=new List<String>();
        Contact con =new Contact();
        con.FirstName='AshishTest';
        con.LastName='sharma';
        con.Phone='7473467363';
        con.Phone_National_DNC__c=false;
        con.Phone_State_DNC__c=false;
        Insert con;
        con.Phone='4412345667';
        Update con;
        Conlist.add(con.Id);
        Five9Credentials__c credentials=new Five9Credentials__c();
        credentials.UserName__c='Ashish@Username';
        credentials.Password__c='Ashish@1234';
        credentials.RealPhoneValidationKey__c='sdgye322egdt3rt32121372e';
        credentials.IsLive__c=true;
        insert credentials;
        Test.startTest();
        RPVDNCFurtherHandler.RpvDNCforMobile(Conlist,'Phone');
        Test.stopTest();
    }
    @isTest
    public Static void RpvDNCMobileTest(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        List<String> Conlist2=new List<String>();
        Contact con =new Contact();
        con.FirstName='AshishTest';
        con.LastName='sharma';
        con.MobilePhone='1234566777';
        con.Mobile_State_DNC__c=false;
        con.DoNotCall=false;
        Insert con;
        con.MobilePhone='4412345667';
        Update con;
        Conlist2.add(con.Id);
        Five9Credentials__c credentials=new Five9Credentials__c();
        credentials.UserName__c='Ashish@Username';
        credentials.Password__c='Ashish@1234';
        credentials.RealPhoneValidationKey__c='sdgye322egdt3rt32121372e';
        credentials.IsLive__c=true;
        insert credentials;
        Test.startTest();
        RPVDNCFurtherHandler.RpvDNCforMobile(Conlist2,'MobilePhone');
        Test.stopTest();
    }
    @isTest
    public Static void RpvDNCOtherPhoneTest(){
        Test.setMock(WebServiceMock.class, new five9calloutforAddingcontactMock());
        Test.setMock(HttpCalloutMock.class, new RpvDNCBatchMockImpl());
        List<String> Conlist2 = new List<String>();
        Contact con = new Contact();
        con.FirstName = 'AshishTest';
        con.LastName = 'sharma';
        con.OtherPhone = '1234566777';
        con.Mobile_State_DNC__c = false;
        con.DoNotCall = false;
        con.Potential_Buyer__c = 'BookLead-1 Day';
        Insert con;
        con.OtherPhone = '4412345667';
        Update con;
        Conlist2.add(con.Id);
        Five9Credentials__c credentials=new Five9Credentials__c();
        credentials.UserName__c='Ashish@Username';
        credentials.Password__c='Ashish@1234';
        credentials.RealPhoneValidationKey__c='sdgye322egdt3rt32121372e';
        credentials.IsLive__c=true;
        insert credentials;
        Test.startTest();
        RPVDNCFurtherHandler.RpvDNCforMobile(Conlist2,'OtherPhone');
        Test.stopTest();
    }
    
}