@IsTest
public class TransactionNMIBatch_Test {
    static{
        NMI_Parameters__c np = new NMI_Parameters__c(User__c='user',Password__c='pwd',Endpoint__c='https://secure.nmi.com/api/');
        insert np;
    }
    
	static testMethod void test() {
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new TransactionNMIBatch(false));
        Test.stopTest();
	}
    
    static testMethod void test2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today());
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new TransactionNMIBatch(true));
        Test.stopTest();
	}
}