/* Developer    : Sourabh Badole
* Description : CampaignMembers_Batch_Helper_Test is the test class fro code coverage of CampaignMembers_Batch_Helper .
*/
@isTest
private class CampaignMembers_Batch_Helper_Test {
    public testmethod Static void updateCampaignMembersTest(){
        
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new  Map<String,String>();
        
        
        Lead ld1 = new Lead(Status = 'New', LastName = 'test1Name',Company = 'testing1company');
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        List<Lead> ldList = new List<Lead>{ld1,ld2};
            insert ldList;
        
        Contact con =new Contact();
        con.LastName = 'test' ;
        con.Campaign_Movement_NOt_Allowed__c = false ;
        con.Product__c = 'Book' ;
        con.Email = 'badole.salesforce@gmail.com';
        insert con ;
        
        
        List<Campaign> cList = new List<Campaign>();
        
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1-4';
        c1.Product__c = 'Book'; 
        c1.Max_Age__c = 4;
        c1.Min_Age__c = 0;
        c1.Marketing_Cloud_Tags__c ='Book Buyer';
        
        cList.add(c1);
        
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Buy Days 5-7';
        c2.Product__c = 'Book'; 
        c2.Min_Age__c = 5;
        c2.Marketing_Cloud_Tags__c ='Book Buyer';
        
        cList.add(c2);
        
        Campaign c3 =new  Campaign();
        c3.Name = 'Campaign Lead Days 8-10';
        c3.Product__c = 'Book'; 
        c3.Min_Age__c = 8;
        c3.Marketing_Cloud_Tags__c ='Book Buyer';
        
        cList.add(c3);
        Campaign c4 =new  Campaign();
        c4.Name = 'Campaign Lead Days 11-30';
        c4.Product__c = 'Book'; 
        c4.Min_Age__c = 11;
        c4.Marketing_Cloud_Tags__c ='Book Buyer';
        
        cList.add(c4);        
        insert cList;
        
        Campaign cWork1 =new  Campaign();
        cWork1.Name = 'SuplmtBuy-1 Day';
        cWork1.Product__c = 'Supplement'; 
        cWork1.Max_Age__c = 10;
        cWork1.Min_Age__c = 8;
        insert cWork1 ;
        
        Date date0 = Date.today();
        Date date1 = date0.addDays(-2);
        
        CampaignMember camMem = new CampaignMember();
        camMem.CampaignId = cWork1.id ;
        camMem.ContactId = con.id ;
        camMem.Status = 'Sent' ;
        camMem.LeadId = ld2.id ;
        camMem.Created_Date__c = date1 ;
        
        insert camMem ;
        CampaignMember camp = [Select id,CampaignID,Campaign.Min_Age__c,Created_Date__c,Age_In_Hours__c,Campaign.Max_Age__c from CampaignMember where CampaignId =: cWork1.Id];
        System.debug('---camp---'+camp);
        
        
        List<Workflow_Configuration__c> listWorkflowConfig = new List<Workflow_Configuration__c>();
        
        
        Workflow_Configuration__c WorkflowCon1 = new Workflow_Configuration__c();
        
        WorkflowCon1.Source_Campaign__c = c1.id ; 
        WorkflowCon1.Special_Movement__c = true ;   
        WorkflowCon1.Products__c = 'Book';
        WorkflowCon1.Target_Campaign__c =  cWork1.id ;
        
        
        listWorkflowConfig.add(WorkflowCon1);
        
        Workflow_Configuration__c WorkflowCon2 = new Workflow_Configuration__c();
        
        WorkflowCon2.Source_Campaign__c = c1.id ; 
        WorkflowCon2.Special_Movement__c =  false ;   
        WorkflowCon2.Target_Campaign__c =  cWork1.id ;
        WorkflowCon2.Products__c = 'Trial';
        
        listWorkflowConfig.add(WorkflowCon2);
        
        insert listWorkflowConfig ;
        
        
        Map<Id,List<Workflow_Configuration__c>> mapWorkflowConfig = new Map<Id,List<Workflow_Configuration__c>>();
        
        for(Campaign cm : cList) {   
            mapWorkflowConfig.put(cm.id , listWorkflowConfig);
        }
        mapWorkflowConfig.put(cWork1.id , listWorkflowConfig);
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c2.Id,LeadId = ld1.Id , Created_Date__c = d1);
        insert cm1;   
        List<CampaignMember> cmList =[SELECT Id,Name,CampaignId,Product__c,Age_In_Hours__c,Contact.Product__c ,Created_Date__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Marketing_Cloud_Tags__c, Campaign.Name, Age__c,LeadId,ContactId FROM CampaignMember where Contact.Product__c != null];
        List<Campaign> campaignList = [SELECT Id, Name, Product__c, Max_Age__c, Min_Age__c  FROM Campaign ];
        
        Campaign_Products__c setting = new Campaign_Products__c();
        setting.Name = 'Book';
        insert setting;
        Map<Id,List<CampaignMember>> conLeadCampMemberMap = new  Map<Id,List<CampaignMember>>();
        
        Test.startTest();
        CampaignMembers_Batch_Helper.updateCampaignMembers(campaignList, cmList , mapWorkflowConfig, conLeadCampMemberMap);
        Test.stopTest();
    }
    
}