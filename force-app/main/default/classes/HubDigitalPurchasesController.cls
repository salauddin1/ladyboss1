public class HubDigitalPurchasesController {
    @AuraEnabled
    public static List<productWrapper> getOpportunity(String conID){
        List<productWrapper> pwList = new List<productWrapper>();
            List<Hub_Order_Page_Products__c> hubProductList = [SELECT Product_Name__c,Url_Link__c FROM Hub_Order_Page_Products__c where Product_Name__c!=null and Url_Link__c!=null];
            Map<String,String> prodNameLinkMap = new Map<String,String>();
            for(Hub_Order_Page_Products__c hub : hubProductList){
                prodNameLinkMap.put(String.valueOf(hub.Product_Name__c).toLowerCase(),hub.Url_Link__c);
            }
            List<Opportunity> oppList = [SELECT id,name,(SELECT id,Product2.name,Product2.Category__c  FROM OpportunityLineItems WHERE Stripe_Charge_Id__c!=null and (refund__c=null OR refund__c=0)) FROM Opportunity WHERE Contact__c =: conID and (Refund_Amount__c=null OR Refund_Amount__c=0)];
            System.debug('oppList : '+oppList);
            System.debug('prodNameLinkMap keys'+prodNameLinkMap.keyset());
            for(Opportunity opp : oppList){
              /*  if(opp.OpportunityLineItems.size()>0){
                    for(OpportunityLineItem ol : opp.OpportunityLineItems){
                        System.debug('ol category : '+ol.Product2.Category__c);
                        if(ol.Product2.Category__c!=null && ol.Product2.Category__c!='' && prodNameLinkMap.containsKey(String.valueOf(ol.Product2.Category__c).toLowerCase())){
                            productWrapper pd = new productWrapper();
                            pd.Name = ol.Product2.Name;
                            pd.ProductLink = prodNameLinkMap.get(String.valueOf(ol.Product2.Category__c).toLowerCase());
                            pwList.add(pd);
                        }
                    }
                }else{
                    if(opp.name!=null && opp.name!='' && prodNameLinkMap.containsKey(opp.name.toLowerCase())){
                        productWrapper pd = new productWrapper();
                        pd.Name = opp.name;
                        pd.ProductLink = prodNameLinkMap.get(String.valueOf(opp.name).toLowerCase());
                        pwList.add(pd);
                    }
                }   */
                if(opp.name!=null && opp.name!='' && prodNameLinkMap.containsKey(opp.name.toLowerCase())){
                        productWrapper pd = new productWrapper();
                        pd.Name = opp.name;
                        pd.ProductLink = prodNameLinkMap.get(String.valueOf(opp.name).toLowerCase());
                        pwList.add(pd);
                }else{
                    for(OpportunityLineItem ol : opp.OpportunityLineItems){
                        System.debug('ol category : '+ol.Product2.Category__c);
                        if(ol.Product2.Category__c!=null && ol.Product2.Category__c!='' && prodNameLinkMap.containsKey(String.valueOf(ol.Product2.Category__c).toLowerCase())){
                            productWrapper pd = new productWrapper();
                            pd.Name = ol.Product2.Name;
                            pd.ProductLink = prodNameLinkMap.get(String.valueOf(ol.Product2.Category__c).toLowerCase());
                            pwList.add(pd);
                        }
                    }
                }
            }
            System.debug('pwList : '+pwList);
        return pwList;
    }
    @AuraEnabled
    public static List<productWrapper> searchOpportunity(String conID, String searchText){
        if(searchText!=null){
            searchText = searchText.toLowerCase();
        }
        List<productWrapper> pwList = new List<productWrapper>();
        List<productWrapper> newpwList = new List<productWrapper>();
        pwList = getOpportunity(conID);
        if(pwList.size()>0){
            for(productWrapper pw : pwList){
                if(String.valueOf(pw.Name.toLowerCase()).contains(searchText)){
                    newpwList.add(pw);
                }
            }
        }
        System.debug('newpwList : '+newpwList);
        return newpwList;
    }
    public class productWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String ProductLink;
    }
}