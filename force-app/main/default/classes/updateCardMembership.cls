public without sharing class updateCardMembership {
    public String stipeApiKey { get; set; }
    public String calledUsing { get; set;}
    public String country { get; set;}
    public String PostalCode { get; set;}
    public String ShippingState { get; set;}
    public String Street { get; set;}
    public String City { get; set;}
    public Id contactId { get; set; }
    public String cardJson { get; set; }
    public String token { get; set; }
    public String customerId { get; set; }
    public String messageError { get; set; }
    public String addressJson { get; set; }
    public boolean ValueReturned {get; set;}
    public boolean fakeCall {get; set;}
    public boolean isSafe {get; set;}
    public boolean isError {get; set;}
    public boolean shipping;
    public string opplineIdval;
    public string stripeContactId;
    public string cardId;
    Card__c newCard;
    public static String message{get;set;}
    
    public updateCardMembership(){
        List<Contact> con = new List <Contact>();
        List<Stripe_Profile__c> existingStripe = new List<Stripe_Profile__c>();
        List<Address__c> addressList = new List<Address__c>();
        customerId = ApexPages.currentPage().getParameters().get('id');
        existingStripe = [select id,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c=: customerId limit 1];
        con = [select id from Contact where id =: existingStripe[0].Customer__c];
        addressList =[select id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c from Address__c where Contact__c=:con[0].id and Primary__c=true limit 1];
        City=addressList[0].Shipping_City__c;
        Street=addressList[0].Shipping_Street__c;
        ShippingState=addressList[0].Shipping_State_Province__c;
        PostalCode=addressList[0].Shipping_Zip_Postal_Code__c;
        country=addressList[0].Shipping_Country__c;
        try{
            this.isError = false;
            isSafe = false;
            messageError ='';
            Map<String,String> addrMap = new Map<String,String>();
            this.stipeApiKey = StripeAPI.PublishableKey;
            this.contactId = existingStripe[0].Customer__c;
            this.calledUsing = String.valueOf(ApexPages.currentPage().getParameters().get('using'));
            
            String addressError = '';
            if(addressList.size()>0){
                if (calledUsing != 'V6' || calledUsing != 'V7') {
                    addressError = AddressValidation.resolveAddress(addressList[0].Shipping_Street__c,addressList[0].Shipping_City__c,addressList[0].Shipping_State_Province__c,addressList[0].Shipping_Country__c,addressList[0].Shipping_Zip_Postal_Code__c);
                }
                if (addressError == 'No error' || calledUsing == 'V6' || calledUsing == 'V7') {
                    addrMap.put('city',addressList[0].Shipping_City__c);
                    addrMap.put('country',addressList[0].Shipping_Country__c);
                    addrMap.put('state',addressList[0].Shipping_State_Province__c);
                    addrMap.put('zip',addressList[0].Shipping_Zip_Postal_Code__c);
                    addrMap.put('street',addressList[0].Shipping_Street__c);
                }else {
                    addrMap.put('city',null);
                    addrMap.put('country',null);
                    addrMap.put('state',null);
                    addrMap.put('zip',null);
                    addrMap.put('street',null);
                    addrMap.put('error',addressError);                                                      
                }
            }
            else{
                addrMap.put('city',null);
                addrMap.put('country',null);
                addrMap.put('state',null);
                addrMap.put('zip',null);
                addrMap.put('street',null);
            }
            this.addressJson = JSON.serialize(addrMap);
            
            system.debug('addressJson----'+addressJson);
        }
        catch(Exception ex){
        }
    }
    
    public PageReference InsertRecord() {
        Contact cont = new contact();
        Card__c newCard = new Card__c();
        try{
            messageError = '';
            this.isError = false;
            System.debug('cardJson-----'+cardJson);
            System.debug('token-----'+token);
            token  = (String)JSON.deserialize(token, String.class);
            System.debug('token 2-----'+token);
            StripeCard stripeCard = StripeCard.parse(cardJson);
            System.debug('stripeCard-----'+stripeCard);
            stripeContactId = ApexPages.currentPage().getParameters().get('id');
            List<Stripe_Profile__c> existingStripeProfiles = new List<Stripe_Profile__c>();
            existingStripeProfiles = [select id,Customer__c,Customer__r.firstname,Stripe_Customer_Id__c from Stripe_Profile__c where Stripe_Customer_Id__c =:stripeContactId order by createdDate limit 1];
            system.debug('======existingStripeProfiles======='+existingStripeProfiles);
            if(!existingStripeProfiles.isEmpty()){
                system.debug('existingStripeProfiles[0].id');
                newCard.Stripe_Profile__c = existingStripeProfiles[0].id;        
            }else{
                System.debug('else');
                Stripe_Profile__c newStriprProfile = new Stripe_Profile__c();
                newStriprProfile.Customer__c = contactId;
                insert newStriprProfile;
                newCard.Stripe_Profile__c = newStriprProfile.id;
            }
            
            newCard.Token__c = token;
            newCard.Contact__c = existingStripeProfiles[0].Customer__c;
            newCard.Initiated_From__c ='Salesforce';
            newCard.Last4__c = stripeCard.last4;
            newCard.Name_On_Card__c = stripeCard.name;
            newCard.Billing_City__c = city;
            newCard.Billing_Country__c = country;
            newCard.Billing_State_Province__c = ShippingState;
            newCard.Billing_Street__c = Street;
            newCard.Billing_Zip_Postal_Code__c = PostalCode;
            newCard.Stripe_Card_Id__c = String.valueOf(stripeCard.id);
            newCard.Expiry_Year__c = String.valueOf(stripeCard.exp_year);
            newCard.Expiry_Month__c = String.valueOf(stripeCard.exp_month);
            newCard.Credit_Card_Number__c = '************'+stripeCard.last4;
            //newCard.Card_Type__c = stripeCard.brand;
            insert newCard;
            this.newCard = newCard;
            isSafe = true;
            system.debug('insert----'+newCard);
            system.debug('insert----'+newCard.Stripe_Profile__c);
            SYSTEM.debug('===existingStripeProfiles=='+existingStripeProfiles[0].Customer__c);
            cont = [select id,Stripe_Customer_Id__c,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from contact where id =: existingStripeProfiles[0].Customer__c limit 1];
            //Update Shiping Address in stripe contact
            String MailingStreet = String.valueOf(stripeCard.address_line1);
            String MailingCity = String.valueOf(stripeCard.address_city);
            String MailingState= String.valueOf(stripeCard.address_state);
            String MailingPostalCode = String.valueOf(stripeCard.address_zip);
            String MailingCountry= String.valueOf(stripeCard.address_country);
            //update Mailing address in SF Contact
            cont.MailingStreet = MailingStreet;
            cont.MailingCity = MailingCity;
            cont.MailingState= MailingState;
            cont.MailingPostalCode = MailingPostalCode;
            cont.MailingCountry= MailingCountry;
            update cont;
            //update Mailing address in stripe customer
            system.debug('====stripeContactId====='+stripeContactId);
            customerUpdateProfile(stripeContactId,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry);
        }catch(exception ex){
            isSafe = false;
            //delete newCard;
            messageError = 'Salesforce Error - '+ex;
            this.isError = true;
        }
        return null;
    }
    @future(callout=true)
    public static void customerUpdateProfile(String stripeContactId,String street,String city,String ShippingState,String PostalCode,String country) { 
        system.debug('======stripeContactId======='+stripeContactId);
        if(stripeContactId!=null && stripeContactId!=''){
            string endPointUrl = 'https://api.stripe.com/v1/customers/'+stripeContactId;
            HttpRequest httpReq = new HttpRequest();
            httpReq.setEndpoint(endPointUrl);
            httpReq.setMethod('POST');
            httpReq.setTimeout(25000);
            httpReq.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
            string shippingAdd='';
            if (Street != '' && city != '' && ShippingState != '' && country != '' && PostalCode != '') {
                shippingAdd = '&metadata[Shipping Address]='+Street+' '+city +' '+ShippingState +' '+country+' '+ PostalCode+'&';
            }
            String reqBody = '';
            if(shippingAdd!=''){
                reqBody+=shippingAdd;          
            }
            httpReq.setBody(reqBody);
            Integer statusCode1;
            Http con1 = new Http();
            HttpResponse hs1 = new HttpResponse();
            
            //send the request body
            if(!Test.isRunningTest()){
                hs1 = con1.send(httpReq); 
            }
        }
        
    }
    public PageReference CallWebService() {
        String message;
        List <Opportunity> oppList = new List <Opportunity>();
        List <OpportunityLineItem> oppLineList = new List <OpportunityLineItem>();
        opplineIdval = ApexPages.currentPage().getParameters().get('opplineIdval');
        stripeContactId = ApexPages.currentPage().getParameters().get('id');
        try{
            messageError = '';
            this.isError = false;
            if(isSafe != null && isSafe){
                system.debug('====loop==');
                System.debug('newCard-----'+newCard.id);
                if(opplineIdval != null){
                    oppLineList = [SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: opplineIdval LIMIT 1];
                    system.debug('==oppLineList==='+oppLineList);
                    oppList = [select id,Name,Subscription__c,Status__c, RecordType.Name,CustomerID__c,Card__c from Opportunity where Id =: oppLineList[0].OpportunityId AND RecordType.Name =: 'Subscription' AND  Contact__c != null ];
                    system.debug('========oppList==============='+oppList);
                    if(oppList.size()>0){
                        for(Opportunity sub : oppList)
                        {
                            system.debug('====sub==='+sub);
                            sub.Card__c = newCard.id;
                        }
                    }
                    message = StripeIntegrationHandlerChanges.cardWithTokenStripeRealTimeHandler(newCard.Id);
                    System.debug('=========='+message);
                    if(message != null && message !=''){
                        delete newCard;
                        messageError = message;
                        this.isError = true;
                    }else{
                        if(oppList.size()>0){
                            update oppList;
                        }
                        system.debug('============='+stripeContactId+'===='+newCard.Stripe_Card_Id__c);
                        defaultCard(stripeContactId,newCard.Stripe_Card_Id__c,opplineIdval);
                        this.isError = false;
                    }
                }
            }
            
            
        }catch(exception ex){
            delete newCard;
            messageError = 'Error--'+ex;
            this.isError = true;
        }
        return null;
    }
    
    @future(callout = true)
    public static void defaultCard(string stripeContactId ,string newCardId,string opplineIdval){
        if(stripeContactId != null && stripeContactId != '' && newCardId != '' && newCardId != null){
            HttpRequest http = new HttpRequest();
            string url =  'https://api.stripe.com/v1/customers/'+stripeContactId +'/sources/'+newCardId ;
            http.setEndpoint(url );
            http.setMethod('POST');
            http.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            Map<String, Object> results;
            if(!test.isrunningTest()){
                hs = con.send(http);
                response = hs.getBody();
                statusCode = hs.getStatusCode();
            }else
            {
                statusCode =200;
                response = '{"id":"card_1DhIKkBwLSk1v1ohtWCxQrTa","object":"card","address_city":"sdf","address_country":null,"address_line1":"sdf","address_line1_check":"pass","address_line2":null,"address_state":"sdf","address_zip":"66666","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_E1EYecfsRNu1yD","cvc_check":"pass","dynamic_last4":null,"exp_month":12,"exp_year":2034,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"dfgggdfg","tokenization_method":null}';
            }
            
            if(statusCode ==200) {
                system.debug('Updating Default card to new card');
                //set default card
                string updateCustomerURL = 'https://api.stripe.com/v1/customers/'+stripeContactId;
                HttpRequest httpsetDefaultCard = new HttpRequest();
                httpsetDefaultCard.setEndpoint(updateCustomerURL);
                httpsetDefaultCard.setMethod('POST');
                httpsetDefaultCard.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
                map<string,string> defaultCardMap = new Map<string,string>{'default_card' => newCardId};
                    httpsetDefaultCard.setBody(StripeUtil.urlify(defaultCardMap));
                Integer defaultCardStatusCode;
                Http defaultCardHTTP = new Http();
                HttpResponse defaultCard_HS = new HttpResponse();
                defaultCard_HS = defaultCardHTTP.send(httpsetDefaultCard);                    
                defaultCardStatusCode = defaultCard_HS.getStatusCode();
                system.debug('defaultCard_HS==>'+JSON.deserializeUntyped(defaultCard_HS.getBody()));
                payPastDueInvoice(newCardId,opplineIdval);
                System.debug('======='+response );
                if(!test.isrunningTest()){
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                }else{
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(response );
                }
            }
            else
            {
                results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                List<Map<String, Object>> error          =   new List<Map<String, Object>>();
                Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                String returnSucess= String.valueOf(errorMap.get('message'));
                System.debug('sdfsfd'+returnSucess);
            }    
        }
    }
    
    public static void payPastDueInvoice(String cardId,string LineItemId){
        system.debug('====LineItemId='+LineItemId);
        List<OpportunityLineItem> oppData =[ SELECT Id, OpportunityId, Quantity, Subscription_Id__c FROM OpportunityLineItem  where Id =: LineItemId LIMIT 1 ];
        System.debug(' test****** '+oppData);
        if (oppData.size()>0) {
            if (oppData[0].Subscription_Id__c != null) {
                String invoiceId = Invoices.getOpenInvoice(oppData[0].Subscription_Id__c);
                if (invoiceId != null) {
                    message = Invoices.payInvoice(invoiceId);
                    System.debug(' test****** '+message); 
                }
            }
        }
        
        system.debug('cardId'+cardId);
        Opportunity opli = new Opportunity();
        List<Card__c> cardinformation = [select id,Card_ID__c from Card__c where Card_ID__c =: cardId];
        if(!cardinformation.isEmpty() && cardinformation != null){
            System.debug('=========='+cardinformation);
            opli.Id = oppData[0].OpportunityId;
            opli.card__c = cardinformation[0].Id;
        }
        if(cardinformation.size()>0){
            update opli;
        }
    }
}