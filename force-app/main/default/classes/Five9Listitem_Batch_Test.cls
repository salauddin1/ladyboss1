@isTest
private class Five9Listitem_Batch_Test {
    public static testMethod void testschedule() {
        Test.StartTest();
        Five9Listitem_Batch_UpdateQuable sh1 = new Five9Listitem_Batch_UpdateQuable();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest(); 
    }
    public static testMethod void testschedule2() {
        Test.StartTest();
        Five9Listitem_DeletePending sh1 = new Five9Listitem_DeletePending();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest(); 
    }
    public testmethod Static void batchFive9ItemsTest(){ 
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        insert ld2;
        Account ac= new Account();
        ac.Name='TestAccount';
        ac.Email__c='TestEmail@gmail.com';
        ac.Exp_Month__c='1';
        ac.Exp_Year__c=2018;
        ac.Credit_Card_Number__c='1234567890';
        ac.Cvc__c='123';
        insert ac;
        Opportunity op=new Opportunity();
        op.Name='TestOpportunity';
        op.StageName='Closed Won';
        op.CloseDate=System.today();
        insert op;
        Contact con= new Contact();
        con.FirstName='Test2';
        con.LastName='Test FIve9';
        con.MobilePhone='1234556678';
        con.Product__c='Trial';
        con.Potential_Buyer__c='BookLead-1 Day';
        con.Campaign_Movement_Not_Allowed__c=false;
        insert con;
        Five9LSP__Five9_List__c lis =new Five9LSP__Five9_List__c();
        lis.Five9LSP__API_URL__c='https://api.five9.com';
        lis.Five9LSP__Five9_Domain__c='LadyBoss - Developer Program-';
        lis.Five9LSP__Five9_User_Name__c='LadyBoss - Developer Program-';
        lis.Five9LSP__Five9_User_Password__c='eN$+{\':jwk&~S^:Z';
        lis.Name='BookLead-1 Day';
        lis.Product__c='Trial';
        lis.Max_Age__c=2;
        lis.Min_Age__c=1;
        lis.Five9_Processing_Priority__c=1;
        lis.Five9LSP__Controlling_Object__c='Contact';
        insert lis;
        Workflow_Configuration__c wc=new Workflow_Configuration__c();
        wc.ListMovementActive__c=true;
        wc.Active__c=true;
        wc.Target_Five9_List__c=lis.Id;
        wc.Products__c=con.Product__c;
        insert wc;
        Case c= new Case();
        c.ContactId=con.Id;
        c.Status='Open';
        c.Origin='Phone';
        c.Priority='1';
        insert c;
        
        /*Five9LSP__Five9_List_Item__c flt= new Five9LSP__Five9_List_Item__c();
        flt.Created_Date__c=Date.today().addDays(-5);
        flt.Five9LSP__Compound_Key__c=lis.Id+''+con.Id;
        flt.Five9LSP__Contact__c=con.Id;
        flt.Five9LSP__Deleted_Controlling_Object_ID__c=String.valueOf(con.Id);
        flt.Five9LSP__Five9_List__c=wc.Target_Five9_List__c;
        flt.Five9LSP__last_allow_sync__c=true;
        flt.Five9LSP__Last_Sync_Time__c=System.today();	
        flt.Five9LSP__Previous_Update_Time__c=date.today().addDays(-1);
        flt.Five9LSP__Sync_Message__c='The request was successfully processed';
        flt.Five9LSP__Sync_Status__c='0';
        flt.Five9LSP__Lead__c=ld2.Id;
        flt.Five9LSP__Opportunity__c=op.Id;
        flt.Five9LSP__Account__c=ac.Id;
        flt.Five9LSP__Case__c=c.Id;
        insert flt;*/
        
        Test.startTest();
        Five9Listitem_Batch batch = new Five9Listitem_Batch();
        Database.executebatch(batch,1);
        Five9Listitem_Batch.Coverage();
        Five9Listitem_Batch_UpdateQuable.UpdateListItem();
        Five9Listitem_DeletePending.deletePendingItems();
        Test.stopTest();  
    }
    /*public testmethod Static void batchFive9ItemsTestHandler() {     
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        insert ld2;
        Contact con= new Contact();
        con.LastName='Test FIve9';
        con.MobilePhone='1234556678';
        con.Product__c='Trial';
        con.Campaign_Movement_Not_Allowed__c=false;
        insert con;
        
        List<Five9LSP__Five9_List__c> flist=new List<Five9LSP__Five9_List__c>();
        Five9LSP__Five9_List__c lis =new Five9LSP__Five9_List__c();
        lis.Five9LSP__API_URL__c='Https://Test.com';
        lis.Five9LSP__Five9_Domain__c='TestDomain';
        lis.Five9LSP__Five9_User_Name__c='TestUser@gmail.com';
        lis.Five9LSP__Five9_User_Password__c='test@12345';
        lis.Name='Trial Buy-1Day';
        lis.Product__c='Trial';
        lis.Max_Age__c=2;
        lis.Min_Age__c=1;
        lis.Five9_Processing_Priority__c=1;
        insert lis;
        flist.add(lis);
        
        Workflow_Configuration__c wc=new Workflow_Configuration__c();
        wc.Active__c=true;
        wc.Source_Five9_List__c=lis.Id;
        wc.Target_Five9_List__c=lis.Id;
        wc.Products__c=lis.Product__c;
        insert wc;
        List<Five9LSP__Five9_List_Item__c> flistItem=new List<Five9LSP__Five9_List_Item__c>();
        Test.startTest();
        Five9ListItem_Batch_Handler.updateFive9Item(fList, flistItem);
        Five9ListItem_Batch_Handler.Coverage();
        Test.stopTest();        
    }
     public testmethod Static void batchFive9ItemsTestHelper() {     
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        insert ld2;
        Contact con= new Contact();
        con.LastName='Test FIve9';
        con.MobilePhone='1234556678';
        con.Product__c='Trial';
        con.Campaign_Movement_Not_Allowed__c=false;
        insert con;
        
        List<Five9LSP__Five9_List__c> flist=new List<Five9LSP__Five9_List__c>();
        Five9LSP__Five9_List__c lis =new Five9LSP__Five9_List__c();
        lis.Five9LSP__API_URL__c='Https://Test.com';
        lis.Five9LSP__Five9_Domain__c='TestDomain';
        lis.Five9LSP__Five9_User_Name__c='TestUser@gmail.com';
        lis.Five9LSP__Five9_User_Password__c='test@12345';
        lis.Name='Trial Buy-1Day';
        lis.Product__c='Trial';
        lis.Max_Age__c=2;
        lis.Min_Age__c=1;
        lis.Five9_Processing_Priority__c=1;
        insert lis;
        flist.add(lis);
        
        Workflow_Configuration__c wc=new Workflow_Configuration__c();
        wc.Active__c=true;
        wc.Source_Five9_List__c=lis.Id;
        wc.Target_Five9_List__c=lis.Id;
        wc.Products__c=lis.Product__c;
        insert wc;
        List<Five9LSP__Five9_List_Item__c> flistItem=new List<Five9LSP__Five9_List_Item__c>();

        Test.startTest();
        FIve9Listitem_Batch_Helper.updateFive9Item(fList, flistItem);
        FIve9Listitem_Batch_Helper.Coverage();
        Test.stopTest();        
    }*/
}