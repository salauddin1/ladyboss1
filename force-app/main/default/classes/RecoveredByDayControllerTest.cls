@istest
public class RecoveredByDayControllerTest {
    @isTest
    static void TestPageCon1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(Alias = 'standa', Email='badole.test@gmail.com',
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='badole.test@gmail.com');
        insert u1;
        
        List<Case> lstCs = new List<Case>();
        Case cs = new Case();
        cs.IsFailed_Payment__c = true;
        cs.isProcessedForReport__c = false ;
        cs.Charge_Id__c = 'ch_1E8SgWBwLSk1v1oh6AGORpxc';
        cs.OwnerId = u1.id;
        cs.Failed_Amount__c = 500;
        cs.Product_Category__c = 'Supplement';
        cs.Subject = 'Failed payment - $39.94 | null Sourabh Test | badole.salesforce11@gmail.com | null | Your card was declined. | Payment for invoice 13D90D1-0002';
        cs.isProcessedForReport__c = true;
        lstCs.add(cs);
        
        Case cs1 = new Case();
        cs1.IsFailed_Payment__c = true;
        cs1.isProcessedForReport__c = false ;
        cs1.Charge_Id__c = 'ch_1E8SgWBwLSk1v1oh6AGORpxc';
        cs1.OwnerId = u1.id;
        cs1.Failed_Amount__c = 200;
        cs1.Recovered_Payment__c = true;
        cs1.Product_Category__c = 'Supplement';
        cs1.Subject = 'Failed payment - $39.94 | null Sourabh Test | badole.salesforce11@gmail.com | null | Your card was declined. | Payment for invoice 13D90D1-0002';
     cs1.isProcessedForReport__c = true;       
        lstCs.add(cs1);
        
        insert lstCs;
        
        PageReference pageRef = Page.Recovered_By_Day;
        Test.setCurrentPage(pageRef);
        
        RecoveredByDayController recPayCon = new RecoveredByDayController();
      
        recPayCon.endDate = Date.today().addDays(3);
        recPayCon.startDate = Date.today().addDays(-1);
        recPayCon.getData() ;
    }
    
   
    
}