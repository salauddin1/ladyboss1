@isTest
    public class HistoryTracking_Test {
        public static testMethod void test(){
            ContactTriggerFlag.isContactBatchRunning = true;
           system.Test.setMock(HttpCalloutMock.class, new MockTest1());
            String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
            System.debug('----------'+devRecordTypeId);
            contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
            insert co;
            Opportunity opp = new Opportunity();
            opp.name ='oppName';
            opp.RecordTypeId = devRecordTypeId;
            opp.CloseDate = date.today();
            opp.StageName ='Closed won';
            opp.Contact__c = co.Id;
            opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
            opp.Status__c ='active';
            opp.Quantity__c =1;
            insert opp;
            
            Product2 prod = new Product2(Name = 'Laptop X200Laptop X200Laptop X200Laptop X200Laptop X200Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345');
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = opp.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Opportunity_Name__c='CLUB';
            ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
            ol.ServiceDate = date.today();
            insert ol;
            String dateVal = String.valueOf(date.today());
            test.startTest();
            ol.Status__c = 'Active';
            update ol;
            test.stopTest();         
        }
        public static TestMethod void testGetCallout(){
            ContactTriggerFlag.isContactBatchRunning = true;
            
                system.Test.setMock(HttpCalloutMock.class, new MockTest1());
            String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
            System.debug('----------'+devRecordTypeId);
            contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
            insert co;
            Opportunity opp = new Opportunity();
            opp.name ='oppName';
            opp.RecordTypeId = devRecordTypeId;
            opp.CloseDate = date.today();
            opp.StageName ='Closed won';
            opp.Contact__c = co.Id;
            opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
            opp.Status__c ='active';
            opp.Quantity__c =1;
            insert opp;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345');
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = opp.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Opportunity_Name__c='CLUB';
            ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
            ol.ServiceDate = date.today();
            insert ol;
            
            String dateVal = String.valueOf(date.today());
            Test.startTest();
            ol.Cancel_Subscription_At__c = Date.today();
            update ol;
            Test.stopTest(); 
            
        }
    }