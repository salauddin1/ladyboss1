/*
 * Developer Name : Tirth patel
 * Description : This is duplicate of ContactMergeBatch. It will be deleted as soon as task is done.
 */
global  class ContactMergeBatch3 implements Database.Batchable<SObject>,Database.Stateful,Database.AllowsCallouts{
    global Set<Id> alreadyDuplicateList;
    private String sessionId;
    
    global ContactMergeBatch3(String sessionId){
        alreadyDuplicateList = new Set<Id>();
        this.sessionId = sessionId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        Set<String> accFieldList = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet();
        List<String> strList = new List<String>();
        strList.addAll(accFieldList);
        String fields = string.join(strList,',');
        String dynamicquery = 'select '+fields+' from Contact where check_for_dup_3__c = true order by lastmodifieddate asc ';
        return Database.getQueryLocator(dynamicquery);
    }
      
    global void execute(Database.BatchableContext context, List<SObject> records) {
        ContactMergeBatchesHandler.mergeHandler(records, alreadyDuplicateList, sessionId, 'check_for_dup_3__c');
    }
    
    global void finish(Database.BatchableContext context) {
        if(!test.isRunningTest())  {
			BatchFindDuplicates3 bt = new BatchFindDuplicates3();
        	Database.executeBatch(bt);
        }
    }  
}