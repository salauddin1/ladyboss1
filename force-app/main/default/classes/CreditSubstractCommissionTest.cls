@isTest
public class CreditSubstractCommissionTest {
    public static testmethod void test1(){
      /*   Contact ct = new Contact();
        ct.lastName='tst';
        insert ct; */
      test.startTest();
       
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test11@11.com');
          first_monday_of_year__c fm = new first_monday_of_year__c();
        fm.day__c = 31;
        fm.month__c = 12;
        fm.year__c = 2018;
        insert fm;
 
        System.runAs(u) {
            Contact ct = new Contact();
        ct.lastName='tst';
        insert ct;
            CreditSubstractCommission.submitCommission(true, 20.0, 4, u.id, 'TestReason',false,ct.Id,12); 
            CreditSubstractCommission.submitCommission(false, 20.0, 4, u.id, 'TestReason',false,ct.Id,12); 
            CreditSubstractCommission.currentWeek();
        }
      
       test.stopTest();
    }
     public static testmethod void test2(){
          
      test.startTest();
       
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test11@11.com');

        System.runAs(u) {
            Contact ct = new Contact();
        ct.lastName='tst';
        insert ct;
           CreditSubstractCommission.submitCommission(false, -1,2, u.id, 'Test Reason',true,ct.Id,12); 
        }
      
       test.stopTest();
    }
}