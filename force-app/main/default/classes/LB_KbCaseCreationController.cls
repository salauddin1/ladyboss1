public class  LB_KbCaseCreationController {
    
    @AuraEnabled
    Public static String doCreateContact(String cw){
        system.debug('conlled');
        caseWrapper cwrap = (caseWrapper)Json.deserialize(cw, caseWrapper.class);
        system.debug('cwrap'+cwrap);
        String contactId;
        system.debug('con--->'+cw);
        List<Contact> conList = [SELECT Id, LastName,Title, FirstName, Phone, Email FROM Contact WHERE Email = :cwrap.email AND Phone = :cwrap.phone LIMIT 1];
        system.debug('con--->'+conList);
        if(!conList.isEmpty() && conList.size()>0){
            for(Contact c: conList){
                system.debug('con--->'+c);
                contactId = c.Id;
            }
            Case ca = doCreateCase(cwrap,contactId);
            String cNo = [SELECT Id, CaseNumber FROM Case WHERE Id = :ca.Id].CaseNumber;
            return cNo;
        }else{
            Contact con = new Contact();
            con.FirstName = cwrap.fname;
            con.LastName = cwrap.lname;
            con.Email = cwrap.email;
            con.Phone = cwrap.phone;
            con.Title = 'KB-Customer';
            try{
                system.debug('con--->'+con);
                insert con;
                contactId = con.Id;
                Case ca = doCreateCase(cwrap,contactId);
                String cNo = [SELECT Id, CaseNumber FROM Case WHERE Id = :ca.Id].CaseNumber;
                return cNo;
            }catch(exception ex){
                system.debug('ex.getMessage()--->'+ex.getMessage()+ex.getLineNumber());
                throw new AuraHandledException(ex.getMessage()+ex.getLineNumber()+ex.getCause());
            }
        }
    }
    
    Public static Case doCreateCase(caseWrapper cw, String contactId){
        Case ca = new Case();
        ca.ContactId = contactId;
        ca.Subject = cw.subject;
        ca.Description = cw.description;
        ca.Origin = 'Web';
        ca.Status = 'Open';
        try{
            system.debug('ca--->'+ca);
            insert ca;
            return ca;
        }catch(exception ex){
            system.debug('ex.getMessage()--->'+ex.getMessage());
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    Public class caseWrapper{
        @AuraEnabled Public String email;
        @AuraEnabled Public String fname;
        @AuraEnabled Public String lname;
        @AuraEnabled Public String phone;
        @AuraEnabled Public String subject;
        @AuraEnabled Public String description;
    } 
}