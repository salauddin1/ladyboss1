@isTest
public class MoveCaseDueDate_Test {
    static testMethod void MoveCaseDueDateTest(){ 
     	// Create the Case Record.
        Case cas = new Case(Status ='Open', Priority = 'Medium', Origin = 'Email',DueDate__c= System.today() ,Call_1_AM__c = true,Call_2_AM__c=true); 
        insert cas;
          Case cas2 = new Case(Status ='Open',c1pm__c=true, Priority = 'Medium', Origin = 'Email',DueDate__c= System.today(),Call_1_AM__c = true,Call_2_AM__c=true); 
        insert cas2;
		MoveCaseDueDate__c mcdd = new MoveCaseDueDate__c();
        mcdd.Number_Of_Days_To_Move__c = 2;
        mcdd.Field_Api_Name__c = 'c1pm__c';
        mcdd.Index__c =1;
        mcdd.Name ='fdfd';
        mcdd.use_pm_check__c = true;
        mcdd.related_am_field_api_name__c = 'Call_1_AM__c';
        insert mcdd;
        
        MoveCaseDueDate__c mcdd2 = new MoveCaseDueDate__c();
        mcdd2.Number_Of_Days_To_Move__c = 2;
        mcdd2.Field_Api_Name__c = 'c2pm__c';
        mcdd2.Index__c =2;
        mcdd2.Name ='fdfd';
        mcdd2.use_pm_check__c = true;
        mcdd2.related_am_field_api_name__c = 'Call_2_AM__c';
        insert mcdd2;
        
        test.startTest();

        MoveCaseDueDate cb = new MoveCaseDueDate();
        Database.executeBatch(cb,200);
        test.stopTest();
    }
}