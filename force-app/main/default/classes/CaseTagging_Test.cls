@isTest
public class CaseTagging_Test {

   public static testMethod void test(){
              
        Test.startTest();
        Case cas = new Case(Status ='New',Transfer__c=false, Priority = 'Medium', Origin = 'Email', Reason='new');
        insert cas;
        CaseTagging.getCaseRecord(cas.id);
        CaseTagging.saveCase(cas,'true','Transfer__c');
        CaseTagging.saveCase(cas,'true','Cancel__c');
        CaseTagging.saveCase(cas,'true','Recovered__c');
        CaseTagging.saveCase(cas,'true','Not_recovered__c');
        Test.stopTest();

    }
}