public without sharing class CurrentEventOnCommunityCont {
    @AuraEnabled
    public static Event getCurrentEvent(){
        Event eventRecord = new Event();
       // eventRecord = [Select id,Description,StartDateTime from Event where IsForWebSiteUse__c = true and Contact__c =: conId and Contact__c != null order by createddate desc limit 1];
        eventRecord = [Select id,Description,StartDateTime ,EndDateTime, Location , subject from Event where IsForWebSiteUse__c = true order by createddate desc limit 1];
        return eventRecord;
    }
	@AuraEnabled
    public static String getWeekDay(String dayValget){
        String formatVal = '';
        Date dt2 = Date.valueOf(dayValget)   ;
        Datetime dt = DateTime.newInstance(dt2.year(), dt2.month(), dt2.day());
        System.debug('  test day 1 '+dt2);
        System.debug('  test day 2 '+dt);
        formatVal = (String) dt.format('EEEEE')+', '+(String) dt.format('MMMMM') +' '+ dt2.day();
        System.debug('  test day '+formatVal);
      
        return formatVal;
    }
	@AuraEnabled
    public static List<String> getStEndTimeVal(DateTime dayValget, DateTime evtEndVal){
        List<String> timeList = new  List<String>();
        
        String formatVal = '';
        Datetime stDt =dayValget ;
        Datetime endDt = evtEndVal ;
        System.debug('  test stDt 1 '+stDt );
        System.debug('  test endDt 2 '+endDt);
     
        formatVal = String.valueOf( stDt.month())+'/'+String.valueOf( stDt.day())+'/'+String.valueOf( stDt.year())+' '+ String.valueOf( stDt.hour())+':'+ String.valueOf( stDt.minute());
        timeList.add(formatVal);
        System.debug('  test day '+formatVal);
        
        formatVal='';
        formatVal = String.valueOf( endDt.month())+'/'+ String.valueOf( endDt.day())+'/'+String.valueOf( endDt.year())+' '+ String.valueOf( endDt.hour())+':'+ String.valueOf( endDt.minute());
        timeList.add(formatVal);
        
        System.debug('  test day '+formatVal);
      
        return timeList;
    }

}