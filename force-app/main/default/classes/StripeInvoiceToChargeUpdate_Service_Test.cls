@isTest
public class StripeInvoiceToChargeUpdate_Service_Test {
    
    public static testMethod void test1(){
        try{
            Triggers_activation__c activateTrigger = new Triggers_activation__c();
            activateTrigger.isCampaignAssociate__c = true;
            insert activateTrigger;
            ContactTriggerFlag.isContactBatchRunning = true;
            
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c='evt_1DJguhBwLSk1v1ohwrZpgJIl';
            insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345',
                                        Commission_Enabled_Club_Product__c=true,
                                        Price__c =100);
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = op.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Subscription_Id__c='sub_EhEPFeBP0axfo9';   
            ol.Balance_Applied_on_Last_Invoice__c = 5.20;
            insert ol;
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/StripeInvoiceToChargeUpdate_Service'; 
            
            String body = '{ "id": "evt_1ETRRpFzCf73siP0vzttQOMZ", "object": "event", "api_version": "2018-02-28", "created": 1556275109, "data": { "object": { "id": "in_1ETQR9FzCf73siP0ddKAtnEt", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 107, "amount_paid": 107, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_cycle", "charge": "ch_1ETRRiFzCf73siP0jeKjiS5E", "closed": true, "created": 1556271222, "currency": "usd", "custom_fields": null, "customer": "cus_EEYYSGCq9L091j", "customer_address": null, "customer_email": "grant+charginganchor1daytest3@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1556271222, "default_payment_method": null, "default_source": null, "default_tax_rates": [{ "id": "txr_1EDmlGFzCf73siP06wpVp8Mp", "object": "tax_rate", "active": true, "created": 1552543790, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 7 }], "description": "", "discount": null, "due_date": 1558867096, "ending_balance": 0, "finalized_at": 1556275095, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG", "invoice_pdf": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG/pdf", "lines": { "object": "list", "data": [{ "id": "sub_EhEPFeBP0axfo9", "object": "line_item", "amount": 100, "currency": "usd", "description": "1 × Test-SF 1 Day Subscription Product (at $1.00 / day)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "Redding 1657 Riverside Drive CA US 96001", "Full Name": "grant Charginganchor1daytest3", "Email": "grant+charginganchor1daytest3@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000le6gCAAQ", "Integration Initiated From": "Salesforce", "Street": "1657 Riverside Drive", "City": "Redding", "State": "CA", "Postal Code": "96001", "Country": "US" }, "period": { "end": 1556357616, "start": 1556271216 }, "plan": { "id": "plan_DSuGhxO0bUgs0K", "object": "plan", "active": true, "aggregate_usage": null, "amount": 100, "billing_scheme": "per_unit", "created": 1534952447, "currency": "usd", "interval": "day", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Test-SF 1 Day Subscription Product", "product": "prod_DSuGfqF42cNQ5J", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EhEPJPu5eTwtvn", "tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_eed4b21620423c" }], "has_more": false, "total_count": 1, "url": "/v1/invoices/in_1ETQR9FzCf73siP0ddKAtnEt/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "FC3792E-0571", "paid": true, "payment_intent": "pi_1ETRRcFzCf73siP0xlbkf1So", "period_end": 1556271216, "period_start": 1556184816, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": -520, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1556275095, "marked_uncollectible_at": null, "paid_at": 1556275104, "voided_at": null }, "subscription": "sub_EhEPFeBP0axfo9", "subtotal": 100, "tax": 7, "tax_percent": 7, "total": 107, "total_tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "webhooks_delivered_at": 1556271240 } }, "livemode": true, "pending_webhooks": 6, "request": { "id": null, "idempotency_key": null }, "type": "invoice.payment_succeeded" }';
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            StripeInvoiceToChargeUpdate_Service.updateCharge();
            
            test.stopTest();         
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }  
    }
    
    public static testMethod void test2(){
        try{
            Triggers_activation__c activateTrigger = new Triggers_activation__c();
            activateTrigger.isCampaignAssociate__c = true;
            insert activateTrigger;
            ContactTriggerFlag.isContactBatchRunning = true;
            
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
            op.Clubbed__c = true;
            op.Secondary_Stripe_Charge_Id__c = '';
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c='';
            insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345',
                                        Commission_Enabled_Club_Product__c=true,
                                        Price__c =100);
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = op.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Subscription_Id__c='sub_EhEPFeBP0axfo9';   
            
            insert ol;
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/StripeInvoiceToChargeUpdate_Service'; 
            
            String body = '{ "id": "evt_1ETRRpFzCf73siP0vzttQOMZ", "object": "event", "api_version": "2018-02-28", "created": 1556275109, "data": { "object": { "id": "in_1ETQR9FzCf73siP0ddKAtnEt", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 107, "amount_paid": 107, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_cycle", "charge": "ch_1ETRRiFzCf73siP0jeKjiS5E", "closed": true, "created": 1556271222, "currency": "usd", "custom_fields": null, "customer": "cus_EEYYSGCq9L091j", "customer_address": null, "customer_email": "grant+charginganchor1daytest3@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1556271222, "default_payment_method": null, "default_source": null, "default_tax_rates": [{ "id": "txr_1EDmlGFzCf73siP06wpVp8Mp", "object": "tax_rate", "active": true, "created": 1552543790, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 7 }], "description": "", "discount": null, "due_date": 1558867096, "ending_balance": 0, "finalized_at": 1556275095, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG", "invoice_pdf": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG/pdf", "lines": { "object": "list", "data": [{ "id": "sub_EhEPFeBP0axfo9", "object": "line_item", "amount": 100, "currency": "usd", "description": "1 × Test-SF 1 Day Subscription Product (at $1.00 / day)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "Redding 1657 Riverside Drive CA US 96001", "Full Name": "grant Charginganchor1daytest3", "Email": "grant+charginganchor1daytest3@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000le6gCAAQ", "Integration Initiated From": "Salesforce", "Street": "1657 Riverside Drive", "City": "Redding", "State": "CA", "Postal Code": "96001", "Country": "US" }, "period": { "end": 1556357616, "start": 1556271216 }, "plan": { "id": "plan_DSuGhxO0bUgs0K", "object": "plan", "active": true, "aggregate_usage": null, "amount": 100, "billing_scheme": "per_unit", "created": 1534952447, "currency": "usd", "interval": "day", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Test-SF 1 Day Subscription Product", "product": "prod_DSuGfqF42cNQ5J", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EhEPJPu5eTwtvn", "tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_eed4b21620423c" }], "has_more": false, "total_count": 1, "url": "/v1/invoices/in_1ETQR9FzCf73siP0ddKAtnEt/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "FC3792E-0571", "paid": true, "payment_intent": "pi_1ETRRcFzCf73siP0xlbkf1So", "period_end": 1556271216, "period_start": 1556184816, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1556275095, "marked_uncollectible_at": null, "paid_at": 1556275104, "voided_at": null }, "subscription": "sub_EhEPFeBP0axfo9", "subtotal": 100, "tax": 7, "tax_percent": 7, "total": 107, "total_tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "webhooks_delivered_at": 1556271240 } }, "livemode": true, "pending_webhooks": 6, "request": { "id": null, "idempotency_key": null }, "type": "invoice.payment_succeeded" }';
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            StripeInvoiceToChargeUpdate_Service.updateCharge();
            
            test.stopTest();         
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex+ex.getStackTraceString());        
        }  
    }
    public static testMethod void test3(){
        try{
            Triggers_activation__c activateTrigger = new Triggers_activation__c();
            activateTrigger.isCampaignAssociate__c = true;
            insert activateTrigger;
            ContactTriggerFlag.isContactBatchRunning = true;
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c='ch_1ETRRiFzCf73siP0jeKjiS5E';
            insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345',
                                        Commission_Enabled_Club_Product__c=true,
                                        Price__c =100);
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = op.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Subscription_Id__c='sub_EhEPFeBP0axfo8';   
            
            insert ol;
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/StripeInvoiceToChargeUpdate_Service'; 
            
            String body = '{ "id": "evt_1ETRRpFzCf73siP0vzttQOMZ", "object": "event", "api_version": "2018-02-28", "created": 1556275109, "data": { "object": { "id": "in_1ETQR9FzCf73siP0ddKAtnEt", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 107, "amount_paid": 107, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_cycle", "charge": "ch_1ETRRiFzCf73siP0jeKjiS5E", "closed": true, "created": 1556271222, "currency": "usd", "custom_fields": null, "customer": "cus_EEYYSGCq9L091j", "customer_address": null, "customer_email": "grant+charginganchor1daytest3@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1556271222, "default_payment_method": null, "default_source": null, "default_tax_rates": [{ "id": "txr_1EDmlGFzCf73siP06wpVp8Mp", "object": "tax_rate", "active": true, "created": 1552543790, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 7 }], "description": "", "discount": null, "due_date": 1558867096, "ending_balance": 0, "finalized_at": 1556275095, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG", "invoice_pdf": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG/pdf", "lines": { "object": "list", "data": [{ "id": "sub_EhEPFeBP0axfo9", "object": "line_item", "amount": 100, "currency": "usd", "description": "1 × Test-SF 1 Day Subscription Product (at $1.00 / day)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "Redding 1657 Riverside Drive CA US 96001", "Full Name": "grant Charginganchor1daytest3", "Email": "grant+charginganchor1daytest3@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000le6gCAAQ", "Integration Initiated From": "Salesforce", "Street": "1657 Riverside Drive", "City": "Redding", "State": "CA", "Postal Code": "96001", "Country": "US" }, "period": { "end": 1556357616, "start": 1556271216 }, "plan": { "id": "plan_DSuGhxO0bUgs0K", "object": "plan", "active": true, "aggregate_usage": null, "amount": 100, "billing_scheme": "per_unit", "created": 1534952447, "currency": "usd", "interval": "day", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Test-SF 1 Day Subscription Product", "product": "prod_DSuGfqF42cNQ5J", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EhEPJPu5eTwtvn", "tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_eed4b21620423c" }], "has_more": false, "total_count": 1, "url": "/v1/invoices/in_1ETQR9FzCf73siP0ddKAtnEt/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "FC3792E-0571", "paid": true, "payment_intent": "pi_1ETRRcFzCf73siP0xlbkf1So", "period_end": 1556271216, "period_start": 1556184816, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1556275095, "marked_uncollectible_at": null, "paid_at": 1556275104, "voided_at": null }, "subscription": "sub_EhEPFeBP0axfo9", "subtotal": 100, "tax": 7, "tax_percent": 7, "total": 107, "total_tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "webhooks_delivered_at": 1556271240 } }, "livemode": true, "pending_webhooks": 6, "request": { "id": null, "idempotency_key": null }, "type": "invoice.payment_succeeded" }';
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            StripeInvoiceToChargeUpdate_Service.updateCharge();
            
            test.stopTest();         
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }  
    }
    
    public static testMethod void test44(){
        try{
            Triggers_activation__c activateTrigger = new Triggers_activation__c();
            activateTrigger.isCampaignAssociate__c = true;
            insert activateTrigger;
            
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'sub_EhEPFeBP0axfo9';
            op.Success_Failure_Message__c='customer.subscription.updated';
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            //op.Clubbed__c=true;
            //op.Secondary_Stripe_Charge_Id__c='';
            // op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c='ch_1ETRRiFzCf73siP0jeKjiS5E';
            insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345',
                                        Commission_Enabled_Club_Product__c=true,
                                        Price__c =100);
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = op.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Subscription_Id__c='sub_EhEPFeBP0axfo8';   
            
            insert ol;
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/StripeInvoiceToChargeUpdate_Service'; 
            
            String body = '{ "id": "evt_1ETRRpFzCf73siP0vzttQOMZ", "object": "event", "api_version": "2018-02-28", "created": 1556275109, "data": { "object": { "id": "in_1ETQR9FzCf73siP0ddKAtnEt", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 107, "amount_paid": 107, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_cycle", "charge": "ch_1ETRRiFzCf73siP0jeKjiS5E", "closed": true, "created": 1556271222, "currency": "usd", "custom_fields": null, "customer": "cus_EEYYSGCq9L091j", "customer_address": null, "customer_email": "grant+charginganchor1daytest3@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1556271222, "default_payment_method": null, "default_source": null, "default_tax_rates": [{ "id": "txr_1EDmlGFzCf73siP06wpVp8Mp", "object": "tax_rate", "active": true, "created": 1552543790, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 7 }], "description": "", "discount": null, "due_date": 1558867096, "ending_balance": 0, "finalized_at": 1556275095, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG", "invoice_pdf": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG/pdf", "lines": { "object": "list", "data": [{ "id": "sub_EhEPFeBP0axfo9", "object": "line_item", "amount": 100, "currency": "usd", "description": "1 × Test-SF 1 Day Subscription Product (at $1.00 / day)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "Redding 1657 Riverside Drive CA US 96001", "Full Name": "grant Charginganchor1daytest3", "Email": "grant+charginganchor1daytest3@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000le6gCAAQ", "Integration Initiated From": "Salesforce", "Street": "1657 Riverside Drive", "City": "Redding", "State": "CA", "Postal Code": "96001", "Country": "US" }, "period": { "end": 1556357616, "start": 1556271216 }, "plan": { "id": "plan_DSuGhxO0bUgs0K", "object": "plan", "active": true, "aggregate_usage": null, "amount": 100, "billing_scheme": "per_unit", "created": 1534952447, "currency": "usd", "interval": "day", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Test-SF 1 Day Subscription Product", "product": "prod_DSuGfqF42cNQ5J", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EhEPJPu5eTwtvn", "tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_eed4b21620423c" }], "has_more": false, "total_count": 1, "url": "/v1/invoices/in_1ETQR9FzCf73siP0ddKAtnEt/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "FC3792E-0571", "paid": true, "payment_intent": "pi_1ETRRcFzCf73siP0xlbkf1So", "period_end": 1556271216, "period_start": 1556184816, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1556275095, "marked_uncollectible_at": null, "paid_at": 1556275104, "voided_at": null }, "subscription": "sub_EhEPFeBP0axfo9", "subtotal": 100, "tax": 7, "tax_percent": 7, "total": 107, "total_tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "webhooks_delivered_at": 1556271240 } }, "livemode": true, "pending_webhooks": 6, "request": { "id": null, "idempotency_key": null }, "type": "invoice.payment_succeeded" }';
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            StripeInvoiceToChargeUpdate_Service.updateCharge();
            
            test.stopTest();         
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }  
    }
    
/*    @istest
    public static testMethod void test3(){
        try{
            system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
            test.startTest();
            Opportunity op = new Opportunity();
            op.Name = 'opp0' ;
            op.CloseDate = Date.Today();
            op.StageName = 'Pending';
            op.Subscription__c = 'sub_EhEPFeBP0axfo9';
            op.Success_Failure_Message__c='customer.subscription.updated';
            op.Scheduled_Payment_Date__c=null;
            op.RequestId__c='test';
            op.Active__c='true';
            op.Clubbed__c=true;
            op.Secondary_Stripe_Charge_Id__c=null;
            // op.Success_Failure_Message__c='success';
            op.Status__c='test';
            op.Quantity__c=200;
            op.Stripe_Charge_Id__c=null;
            insert op;
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',Stripe_Plan_Id__c='12345');
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            OpportunityLineItem ol = new OpportunityLineItem();
            
            ol.OpportunityId = op.Id;
            ol.Quantity = 1;
            ol.UnitPrice = 2.00;
            ol.PricebookEntryId = customPrice.Id;
            ol.Subscription_Id__c='';   
            ol.Success_Failure_Message__c='customer.subscription.updated';
            insert ol;
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/StripeInvoiceToChargeUpdate_Service'; 
            
            String body = '{ "id": "sub_EhEPFeBP0axfo9", "object": "event", "api_version": "2018-02-28", "created": 1556275109, "data": { "object": { "id": "sub_EhEPFeBP0axfo9", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 107, "amount_paid": 107, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_cycle", "charge": "ch_1ETRRiFzCf73siP0jeKjiS5E", "closed": true, "created": 1556271222, "currency": "usd", "custom_fields": null, "customer": "cus_EEYYSGCq9L091j", "customer_address": null, "customer_email": "grant+charginganchor1daytest3@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1556271222, "default_payment_method": null, "default_source": null, "default_tax_rates": [{ "id": "txr_1EDmlGFzCf73siP06wpVp8Mp", "object": "tax_rate", "active": true, "created": 1552543790, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 7 }], "description": "", "discount": null, "due_date": 1558867096, "ending_balance": 0, "finalized_at": 1556275095, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG", "invoice_pdf": "https://pay.stripe.com/invoice/invst_dNmiPrY1MtFZjdE4cAmPSu1VJG/pdf", "lines": { "object": "list", "data": [{ "id": "sub_EhEPFeBP0axfo9", "object": "line_item", "amount": 100, "currency": "usd", "description": "1 × Test-SF 1 Day Subscription Product (at $1.00 / day)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "Redding 1657 Riverside Drive CA US 96001", "Full Name": "grant Charginganchor1daytest3", "Email": "grant+charginganchor1daytest3@ladyboss.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000le6gCAAQ", "Integration Initiated From": "Salesforce", "Street": "1657 Riverside Drive", "City": "Redding", "State": "CA", "Postal Code": "96001", "Country": "US" }, "period": { "end": 1556357616, "start": 1556271216 }, "plan": { "id": "plan_DSuGhxO0bUgs0K", "object": "plan", "active": true, "aggregate_usage": null, "amount": 100, "billing_scheme": "per_unit", "created": 1534952447, "currency": "usd", "interval": "day", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "Test-SF 1 Day Subscription Product", "product": "prod_DSuGfqF42cNQ5J", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EhEPJPu5eTwtvn", "tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_eed4b21620423c" }], "has_more": false, "total_count": 1, "url": "/v1/invoices/in_1ETQR9FzCf73siP0ddKAtnEt/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "FC3792E-0571", "paid": true, "payment_intent": "pi_1ETRRcFzCf73siP0xlbkf1So", "period_end": 1556271216, "period_start": 1556184816, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1556275095, "marked_uncollectible_at": null, "paid_at": 1556275104, "voided_at": null }, "subscription": "sub_EhEPFeBP0axfo9", "subtotal": 100, "tax": 7, "tax_percent": 7, "total": 107, "total_tax_amounts": [{ "amount": 7, "inclusive": false, "tax_rate": "txr_1EDmlGFzCf73siP06wpVp8Mp" }], "webhooks_delivered_at": 1556271240 } }, "livemode": true, "pending_webhooks": 6, "request": { "id": null, "idempotency_key": null }, "type": "invoice.payment_succeeded" }';
            req.requestBody = Blob.valueOf(body);
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
            RestContext.request = req;
            
            RestContext.response = res;
            System.debug('===========RestContext.request========'+RestContext.request);
            
            StripeInvoiceToChargeUpdate_Service.updateCharge();
            
            test.stopTest();         
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        }  
    }*/
}