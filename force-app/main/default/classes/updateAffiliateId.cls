public class updateAffiliateId {
    public static void updateAffiliate1(List<Contact> updateaffIdList){
        for(Contact c : updateaffIdList){
            if(c.PAP_refid__c != null && c.PAP_refid__c != ''){
                updateAffiliate(c.id,c.PAP_refid__c);
            }
        }
    }
    
    @future(callout=true)
    public static void updateAffiliate(String conId,string newrefId){
        if(conId != null && conId != '' && newrefId != null && newrefId != ''){
            Contact ct = [SELECT id,Email,FirstName,LastName,PAP_refid__c,PAPRPassword__c,PAPUserId__c FROM Contact WHERE id=:conId];
            pap_credentials__c pap_url = [select id,name,pap_url__c,username__c,password__c from pap_credentials__c where Name='pap url' limit 1];
            if(pap_url != null && pap_url.username__c != null && pap_url.username__c != '' && pap_url.password__c != null && pap_url.password__c != '' && pap_url.pap_url__c != null && pap_url.pap_url__c != '' && ct.FirstName != null && ct.FirstName != '' && ct.LastName != null && ct.LastName != '' && ct.PAP_refid__c != null && ct.PAP_refid__c != '' && ct.PAPUserId__c != null && ct.PAPUserId__c != '' && ct.PAPRPassword__c != null && ct.PAPRPassword__c != ''){
                Http http = new Http();
                HttpRequest ht = new HttpRequest();
                ht.setEndpoint(pap_url.pap_url__c);
                ht.setMethod('GET');
                ht.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                system.debug('====ct=='+ct);
                System.debug('newrefId:-'+newrefId);
                String body = 'D={"C":"Gpf_Rpc_Server","M":"run","requests":[{"C":"Pap_Api_AuthService","M":"authenticate","fields":[["name","value","values","error"],["username","'+pap_url.username__c+'",null,""],["password","'+pap_url.password__c+'",null,""],["roleType","M",null,""],["isFromApi","Y",null,""],["apiVersion","5.8.11.1",null,""]]},{"C":"Pap_Merchants_User_AffiliateForm","M":"Save","isFromApi":"Y","fields":[["name","value","values","error"],["username","'+ct.Email+'",null,""],["firstname","'+ct.FirstName+'",null,""],["status","A",null,""],["rstatus","A",null,""],["lastname","'+ct.LastName+'",null,""],["refid","'+newrefId+'",null,""],["Id","'+ct.PAPUserId__c+'"],["rpassword","'+ct.PAPRPassword__c+'"]]}]}';
                ht.setBody(body);
                HttpResponse res = http.send(ht);
                System.debug('res : '+res.getBody());
                List<Object> resp = (List<Object>) Json.deserializeUntyped(res.getBody());
                String msg='';
                for(Object ob : resp){
                    Map<String,Object> ob1 = (Map<String,Object>)ob;
                    if((String)ob1.get('message')!=null){
                        msg = (String)ob1.get('message');
                    }
                    System.debug('msg '+ob1.get('message'));
                    
                }
                
            }
        }
    }
}