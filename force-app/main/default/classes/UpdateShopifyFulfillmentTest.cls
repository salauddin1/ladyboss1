@isTest
public class UpdateShopifyFulfillmentTest {
	@isTest
    public Static void test1(){
        String Body = '{"id":796998631520,"order_id":825756844128,"status":"cancelled","created_at":"2019-02-28T13:49:38-05:00","service":"manual","updated_at":"2019-02-28T13:51:51-05:00","tracking_company":null,"shipment_status":null,"location_id":16461168736,"email":"","destination":{"first_name":"surendra","address1":"test Street","phone":"7067749234","city":"Indore","zip":"452001","province":"Madhya Pradesh","country":"India","last_name":"patidar","address2":"test apart","company":"testcomp","latitude":22.7081955,"longitude":75.8824422,"name":"surendra patidar","country_code":"IN","province_code":"MP"},"tracking_number":null,"tracking_numbers":[],"tracking_url":null,"tracking_urls":[],"receipt":{},"name":"#1013.2","line_items":[{"id":1839519727712,"variant_id":18554992820320,"title":"testProduct","quantity":1,"price":"1.00","sku":"Test@123","variant_title":null,"vendor":"ladyboss.swagagain.com","fulfillment_service":"manual","product_id":1922733342816,"requires_shipping":true,"taxable":true,"gift_card":false,"name":"testProduct","variant_inventory_management":"shopify","properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":0,"total_discount":"0.00","fulfillment_status":null,"price_set":{"shop_money":{"amount":"1.00","currency_code":"USD"},"presentment_money":{"amount":"1.00","currency_code":"USD"}},"total_discount_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}},"discount_allocations":[],"tax_lines":[]}]}';
    	Shopify_Orders__c ord = new Shopify_Orders__c();
        ord.Order_Id__c='765260857444';
        ord.fulfillment_Status__c='fulfilled';
        ord.Status__c='Confirmed';
        insert ord;
        Contact con = new Contact();
        con.LastName='TestLast';
        con.FirstName='TestFirst';
        con.Email='Test@gmail.com';
        con.Phone='7067749234';
        insert con;
        Shopify_Parameters__c tokens = new Shopify_Parameters__c ();
        tokens.Access_Token__c = '1234redcre3edsx23wq3wed32w32wes32w';
        tokens.Domain_Name__c  = 'https://test.com';
        tokens.Is_Active__c    = true;
        insert tokens;
        ShopifyLineItem__c item = new ShopifyLineItem__c();
        item.Shopify_Orders__c=ord.id;
        item.Item_Id__c = '1839519727712';
        item.Status__c  = 'canceled';
        insert item;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new orderCalloutTest());
        RestRequest request = new RestRequest();
        request.requestUri ='https://ashish-ladyboss-support.cs53.force.com/ShipStation/services/apexrest/Fulfillment';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(Body);
        RestContext.request = request;
        UpdateShopifyFulfillment.getfulfillments();
        Test.stopTest();
    }
}