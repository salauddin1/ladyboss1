global class StripeCard {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';

    global String stripeType;
    global String country;
    global Integer exp_month; 
    global String fingerprint;
    global Integer exp_year;
    global String last4;
    global String stripeObject;
    global String id;
    global String name;
    global StripeError error;
    global String customer_id;
    global String address_city;
  global String address_country;
  global String address_line1;
  global String address_line1_check;
  global String address_line2;
  global String address_state;
  global String address_zip;
  global String address_zip_check;
  global String brand;
    global boolean deleted;
    // this is for testing
    
    global Date expirationDate {
        get {
            Date d = Date.newInstance(this.exp_year, this.exp_month, Date.daysInMonth(this.exp_year, this.exp_month));
            return d;
        }
    }

    global static StripeCard create(String customerId, String token,String cardId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/cards');
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Salesforce Card Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+cardId);
        
        Map<String, String> payload = new Map<String, String>{
            'card' => token
        };
         if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeCard o = StripeCard.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }

    global static StripeCard createCardWithToken(String customerId, String token,String cardId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources');
        http.setMethod('POST');
        //Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        //EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Salesforce Card Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+cardId);
        
        Map<String, String> payload = new Map<String, String>{
            'source' => token
        };
         if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeCard o = StripeCard.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    
    global static StripeCard deleteCard(String customerId,String cardId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources/'+cardId);
        http.setMethod('DELETE');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
       
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            if (!Test.isRunningTest()) {
            StripeCard o = StripeCard.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            return o;
            }else{
                StripeCard sc = new StripeCard();
                StripeError se = new StripeError();
                se.code = '32';
                se.decline_code='dsd';
                se.message='sdsdsd';
                sc.error = se;
                return sc;
            }
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }

    /*global static StripeCard getAllCards(String customerId,String cardId) {
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources?object=card&limit=3');
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
       
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        if (!Test.isRunningTest()) {
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response );
        List<Object> cards = (List<Object>)results.get('data');
        system.debug('#### cards #### '+ cards);
        try {
            List<StripeCard> stripeCardList = new List<StripeCard>();
            for(Object perticulerCard : cards){
                stripeCardList.add(StripeCard.parse(JSON.serialize(perticulerCard)));
            }
            system.debug('#### stripeCardList #### '+ stripeCardList);
            StripeCard o = StripeCard.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
        }else{
            return null;
        }
    }*/


    public static StripeCard parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);

        return (StripeCard) System.JSON.deserialize(json, StripeCard.class);
    }
     public static string getCard(String customerID ) {
     String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    String customerIDs ='';
        HttpRequest http = new HttpRequest();
        
        http.setEndpoint(SERVICE_URL+''+customerID );
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        try {
            hs = con.send(http);
            response = hs.getBody();
            statusCode = hs.getStatusCode();
        } catch (CalloutException e) {
            
        }
        
        
        if(statusCode == 200 ){
            System.debug(response);
            try {
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                 system.debug('results -- '+String.valueOf(results ));
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('sources');
                system.debug('response-- '+lstCustomers       );
                List<Object> lstsourcess = (List<Object>)lstCustomers.get('data');
                Object carddata = (object)lstsourcess[0];
                system.debug('response-- '+carddata );
                Map<String,Object> data = (Map<String,Object>)carddata ;
                
                
                String cardId='';
                if(data.get('id')!=null){
                    cardId =string.valueOf(data.get('id'));
                }
                
                if(cardId!=null && cardId!=''){
                    return cardId;
                }else{
                    return null;
                }
                
            }
            catch(CalloutException e) {
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                new ApexDebugLog.Error('StripeCard','getCard','',e) );
            }
        }
    return null;
    }
    
    
}