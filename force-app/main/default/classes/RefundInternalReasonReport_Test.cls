@isTest
public class RefundInternalReasonReport_Test {
    
    public static testMethod void test3(){
        //system.Test.setMock(HttpCalloutMock.class, new RefundSubCharge_Mock());
        
        
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        opp.Refund_Internal_Reason__c='Test';
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        ol.Refund_Internal_Reason__c='Test';
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        ol.refund__c = 137;
        insert ol;
        
        Test.StartTest();
        RefundInternalReasonReport.getOpportunity(co.Id,'All');
        RefundInternalReasonReport.getOpportunityLineItem(co.Id,'All');
       
        Test.stopTest();
    }
  }