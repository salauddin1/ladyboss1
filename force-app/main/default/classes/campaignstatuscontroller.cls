public class campaignstatuscontroller {
    public   Map<id,list<Campaign_Stage__c>> cmpstagemap{get;set;}
    public tree tree{get;set;}
    public list<Campaign> cmplist {get;set;}
    public Map<String,tree> mapStr {get;set;}
    public Integer outerMapSize{get;set;}
    public Integer lstSize{get;set;}
    public String fixLink {get;set;}
    public List<String> lstAlternatives{get;set;}
    public campaignstatuscontroller(){
        fixLink = 'a2';
        lstAlternatives = new List<String>();
        cmpstagemap=new Map<id,list<Campaign_Stage__c>>();
        mapStr = new Map<String,tree>();
        list<tree> treelist=new list<tree>();
        list<Campaign_Stage__c> campstagelist=new LIST<Campaign_Stage__c>();
        
        
        cmpstagemap=new Map<id,list<Campaign_Stage__c>>();
        List<Campaign_Stage__c> cmpstagelist=[SELECT Id,Age__c,Campaign__c,name,Campaign__r.name,SMS_Template__c  FROM Campaign_Stage__c where Campaign__c != null];
        list<String> smslist=new list<string>();
        
        List<Id> lstCampignIds = new List<Id>();
        for(Campaign_Stage__c cmpstage:cmpstagelist){
            smslist.add(cmpstage.SMS_Template__c);
            lstCampignIds.add(cmpstage.Campaign__c);
            if(!cmpstagemap.containsKey(cmpstage.Campaign__c))  {
                cmpstagemap.put(cmpstage.Campaign__c, new List<Campaign_Stage__c>());  
            }
            cmpstagemap.get(cmpstage.Campaign__c).add(cmpstage);  
        }
        
        map<string,id> emailtemmap=new map<string,id>();
        list<EmailTemplate> emailtemlist=[SELECT id,DeveloperName FROM EmailTemplate where developername in :smslist];
        for(EmailTemplate emp: emailtemlist){
            emailtemmap.put(emp.DeveloperName,emp.Id); 
        }
        system.debug(emailtemmap);
        list<Campaign> cmplist=[select id,name from Campaign where id in :lstCampignIds];
        map<id,campaign> campaignmap=new map<id,campaign>();
        for(Campaign camp:cmplist){
            lstAlternatives.add('c'+camp.id);
            campaignmap.put(camp.id,camp);
        }
        system.debug(cmplist);
        system.debug(cmpstagemap.values());
        
        for(id camp:cmpstagemap.keySet()){
            Campaign cmp=campaignmap.get(camp);
            Tree t = new tree('root','c'+camp,true);
            System.debug(t.text);
            t.text.put('name' ,'{ val : "'+ cmp.name + '",href:"/'+camp+'", target: "_blank"}');
            t.innerMapSize = t.text.size();
            mapStr.put(t.HTMLid , t);
            for(list<Campaign_Stage__c> campstage: cmpstagemap.values()){
                for(Campaign_Stage__c cmpstage:campstage){
                    lstAlternatives.add('s'+cmpstage.Campaign__c+''+cmpstage.id);
                    Tree t1 = new  tree('c'+cmpstage.Campaign__c,'s'+cmpstage.Campaign__c+''+cmpstage.id,false);
                    
                    
                    t1.text.put('a3' ,'{ val : "Template : '+ cmpstage.SMS_Template__c + '",href:"/'+emailtemmap.get(cmpstage.SMS_Template__c)+'", target: "_blank"}');
                    t1.text.put('a2' , ''+'Age : '+cmpstage.Age__c);
                    t1.text.put('a1' ,'{ val : "'+ cmpstage.Name + '",href:"/'+cmpstage.id+'", target: "_blank"}');
                    
                    t1.innerMapSize = t1.text.size();
                    mapStr.put(t1.HTMLid , t1);
                }
            }
        }
        outerMapSize = mapStr.size();
        lstSize =lstAlternatives.size(); 
        System.debug(mapStr.size());
    }
    
    public class tree {
        public String HTMLid{get;set;}
        public String parent{get;set;}
        public boolean stackChildren{get;set;}
        public Integer innerMapSize{get;set;}
        public Map<String,String> text{get;set;}
        public tree(string parent,String HTMLid,boolean stackChildren){
            this.parent=parent;
            this.HTMLid=HTMLid;
            this.stackChildren=stackChildren;
            this.text=new Map<String,String>();
        }
    }
    
}