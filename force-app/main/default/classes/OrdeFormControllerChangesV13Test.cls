@isTest
public class OrdeFormControllerChangesV13Test {
    static testMethod void testParse() {
        contact con = new contact();
        con.lastName = 'Tes';
        
        con.MailingStreet = 'gfdg';
        con.MailingState = 'dfg';
        con.MailingPostalCode = '45655';
        //con.MailingAddress = 'fdg';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        card__c cd = new card__c();
        //cd.Name='Test';
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert cd;
        opportunity op = new opportunity();
        op.Name = 'fgdg';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        insert op;
        Product_Discount__c pd = new Product_Discount__c ();
        pd.Discount_Percentage__c = 23;
        insert pd;
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        SiteFieldConfig__c sc=new SiteFieldConfig__c();
        sc.Name='test';
        sc.siteURL__c='test@11';
        sc.Site_Field__c='test45';
        sc.SF_Field__c='hello';
        insert sc;
        Stripe_Profile__c ab=new Stripe_Profile__c();
        ab.Customer__c=con.Id;
        
        ACH_Account__c ac=new ACH_Account__c();
        ac.Name='testhello';
        ac.Stripe_Profile__c=ab.Id;
        insert ac;
        
        
        
        
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
        //
        // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
        test.starttest();
        OrdeFormControllerChangesV13.cardListServer(con.id);
        OrdeFormControllerChangesV13.siteConfig(sc.siteURL__c, sc.Id, sc.Id);
        //OrdeFormControllerChangesV13.ACHListServer(ab.Id, ac.Id);
        OrdeFormControllerChangesV13.ACHVerifiedListServer(ac.Id);
        OrdeFormControllerChangesV13.ACHNonVerifiedListServer(ac.Id);
        OrdeFormControllerChangesV13.fetchLookUpValues('jay','User');
        
        OrdeFormControllerChangesV13.fetchLookUpValuesForKnowledge('jay','User');
        OrdeFormControllerChangesV13.subscribedProductListServer(con.id);
        OrdeFormControllerChangesV13.refreshContactAddressServer(con.id);
        OrdeFormControllerChangesV13.getContacts('tes');
        OrdeFormControllerChangesV13.updateContactWithProduct(con.id,'Book');
        RecursiveTriggerHandler.isFirstTime = true;
        OrdeFormControllerChangesV13.updateCardInStripe(cd.id);
        OrdeFormControllerChangesV13.sendmailACHVerificationToCustomer(con.id);
        String s = '{"LastName":"Tes","MailingStreet":"gfdg","MailingState":"dfg","MailingPostalCode":"45655","Stripe_Customer_Id__c":"card_1CiJbsBwLSk1v1ohyZkWjSEc","Id":"0031F0000063z1aQAA","ContactId":"0031F0000063z1aQAA"},"address":{"Id":"ad.id"}';
        //OrderFormController.updateContactDetails(s);
        test.stopTest();
        
    }
    static testMethod void testParse22() {
        contact con = new contact();
        con.lastName = 'Tes';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        card__c cd = new card__c();
        //cd.Name='Test';
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert cd;
        opportunity op = new opportunity();
        op.Name = 'fgdg';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        // insert op;
        Product_Discount__c pd = new Product_Discount__c ();
        pd.Discount_Percentage__c = 23;
        insert pd;
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
        //
        // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
        test.startTest();
        
        
        
        
        OrdeFormControllerChangesV13.cardListServer(con.id);
        OrdeFormControllerChangesV13.subscribedProductListServer(con.id);
        OrdeFormControllerChangesV13.refreshContactAddressServer(con.id);
        OrdeFormControllerChangesV13.getContacts('tes');
        OrdeFormControllerChangesV13.updateContactWithProduct(con.id,'Book');
        test.stopTest();
        
        
    }
    static testMethod void testParse11() {
        contact con = new contact();
        con.lastName = 'Tes';
        con.Email ='test@gmail.com';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        card__c cd = new card__c();
        //cd.Name='Test';
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert cd;
        opportunity op = new opportunity();
        op.Name = 'fgdg';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        insert op;
        Product_Discount__c pd = new Product_Discount__c ();
        pd.Discount_Percentage__c = 23;
        insert pd;
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        list<Task> tskId = new list<Task> ();
        Task ts = new Task();
        ts.Priority ='Normal';
        ts.Status='Open';
        ts.MailStatus__c ='Send';
        tskId.add(ts);
        insert tskId;
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
        //
        // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
        list<String> emailBody = new list<String>();
        emailBody.add('this is email test');
        list<String> subBody = new list<String>();
        subBody.add(' email test');
        
        test.startTest();
        
        
        OrdeFormControllerChangesV13.cardListServer(con.id);
        OrdeFormControllerChangesV13.subscribedProductListServer(con.id);
        OrdeFormControllerChangesV13.refreshContactAddressServer(con.id);
        OrdeFormControllerChangesV13.getContacts('tes');
        OrdeFormControllerChangesV13.updateContactWithProduct(con.id,'Book');
        OrdeFormControllerChangesV13.sendEmail(con.Id,emailBody,subBody);
        test.stopTest();
        
        
    }
    static testMethod void testParse31() {
        HttpResponse res = new HttpResponse();
        res.setBody('{"error":{"code":"ADDRESS.VERIFY.FAILURE","message":"Unable to verify address.","errors":[{"code":"E.ADDRESS.NOT_FOUND","field":"address","message":"Address not found","suggestion":null},{"code":"E.HOUSE_NUMBER.MISSING","field":"street1","message":"House number is missing","suggestion":null}]}}');
        res.setStatusCode(400);
        Test.setMock(HttpCalloutMock.class, new MockTestClassAddressValidation(res));
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        contact con = new contact();
        con.lastName = 'Tes';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        card__c cd = new card__c();
        //cd.Name='Test';
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert cd;
        //
        ACH_Account__c ac = new ACH_Account__c();
        ac.Account_Holder_Name__c = 'wdsdewd';
        ac.Account_Holder_Type__c='sds';
        ac.ACH_Token__c='dsdsd3e';
        ac.Bank_Name__c='sddsd';
        ac.Contact__c = con.id;
        insert ac;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //User u = [Select id, name, email from User where id=: UserInfo.getUserId() AND ProfileId = profileId.id];
        
        opportunity op = new opportunity();
        op.Name = 'fgdg';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        // insert op;
        Product_Discount__c pd = new Product_Discount__c ();
        pd.Discount_Percentage__c = 23;
        insert pd;
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        
        
        
        Product2 prod2 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod2;
        
        Product2 prod3 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod3;
        
        Product2 prod4 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod4;
        
        Product2 prod5 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod5;
        
        Product2 prod6 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Switch_To_Product_two_month__c=prod5.id,
                                      availableForProfiles__c = 'System Administrator',Stripe_Plan_Id__c='cus_0000');
        insert prod6;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Switch_To_Product_two_month__c=prod2.id,
                                     Switch_To_Product_three_month__c=prod3.id,
                                     Switch_To_Product_four_month__c=prod4.id,
                                     Switch_To_Product_five_month__c=prod5.id,
                                     Switch_To_Product_six_month__c=prod6.id,
                                     availableForProfiles__c ='System Administrator',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 5;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        //insert oli;
        //
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 2;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        
        ProductCombination__c pc = new ProductCombination__c();
        pc.Product__c = oli.id;
        pc.ProductToHide__c = oli2.id;
        insert pc;
        
        
        OrdeFormControllerChangesV13.contactWrapper cw=new OrdeFormControllerChangesV13.contactWrapper();
        cw.address = ad;
        cw.ContactId = con.id;
        cw.LastName ='tes';
        
        cw.Name ='ds';
        
        String cwwrapper = JSON.serialize(cw);
        String addresswrapper = JSON.serialize(ad);
        String cardwrapper = JSON.serialize(cd);
        String selectedProd ='[{"checkValue":true,"club":false,"Id":"01t1F000001lECKQA2","isSelectable":true,"months":1,"Name":"Big Fat Lies Book","price":7.95,"productCategory":"Book","productsToSwitchMap":{"0":{"quanititySelector":false}},"quanititySelector":true,"quantity":1,"quantityPrice":0,"standAloneClubProd":false}]';
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
        //
        // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
        test.startTest();
        List<string> lstStr = new List<string>();
        lstStr.add('1');
        OrdeFormControllerChangesV13.formatString(lstStr);    
        OrdeFormControllerChangesV13.getOpportunityDetails(con.id);
        //OrdeFormControllerChangesV13.getContactDetails(con.id);
        OrdeFormControllerChangesV13.cardListServer(con.id);
        OrdeFormControllerChangesV13.subscribedProductListServer(con.id);
        OrdeFormControllerChangesV13.refreshContactAddressServer(con.id);
        OrdeFormControllerChangesV13.updateContactWithProduct(con.id,'Book');
        //String s = '{"LastName":"Tes","MailingStreet":"gfdg","MailingState":"dfg","MailingPostalCode":"45655","Stripe_Customer_Id__c":"card_1CiJbsBwLSk1v1ohyZkWjSEc","Id":"0031F0000063z1aQAA","ContactId":"'+con.id+'"},"address":{"Id":"'+ad.id+'"}';
        OrdeFormControllerChangesV13.updateContactDetails(cwwrapper,false);
        OrdeFormControllerChangesV13.createNewAddress(addresswrapper,con.id,true,cwwrapper);
        OrdeFormControllerChangesV13.createCardWithBilling(cardwrapper,con.id);
        OrdeFormControllerChangesV13.updateCardWithBilling(cardwrapper,cd.id);
        OrdeFormControllerChangesV13.createCardWithShipping(cardwrapper,con.id,addresswrapper);
        OrdeFormControllerChangesV13.insertOpportunity(selectedProd,cwwrapper,true,String.valueof(system.now()),'[{"card":"a001F000001t9BSQAY","nonClubamount":0,"usedForClub":true}]',66,00,56,'{"Id": "0051F000001Y89NQAS", "Username": "tirthpatel.5892@gmail.com.devs", "Name": "Tirth Patel"}',true,true,null,null,null,7.75,selectedProd,false,false,'Free REST,Free BURN','test','newtest10');
        OrdeFormControllerChangesV13.calloutACHForVerification(ac.id, 32, 45);
        // OrdeFormControllerChangesV13.DummyCover();
        test.stopTest();
        
        
    }
    static testMethod void testParse32() {
        HttpResponse res = new HttpResponse();
        res.setBody('{"error":{"code":"ADDRESS.VERIFY.FAILURE","message":"Unable to verify address.","errors":[{"code":"E.ADDRESS.NOT_FOUND","field":"address","message":"Address not found","suggestion":null},{"code":"E.HOUSE_NUMBER.MISSING","field":"street1","message":"House number is missing","suggestion":null}]}}');
        res.setStatusCode(400);
        Test.setMock(HttpCalloutMock.class, new MockTestClassAddressValidation(res));
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        contact con = new contact();
        con.lastName = 'Tes';
        con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
        insert con;
        card__c cd = new card__c();
        //cd.Name='Test';
        cd.Expiry_Month__c='03';
        cd.Expiry_Year__c='2029';
        cd.Card_Type__c = 'tok_visa';
        cd.contact__c = con.id;
        cd.Credit_Card_Number__c = '564';
        cd.Name_On_Card__c = 'dfsfsdf';
        cd.Billing_Street__c = 'CA';
        cd.Billing_City__c = 'dfg';
        cd.Billing_State_Province__c= 'CA' ;
        cd.Billing_Zip_Postal_Code__c = '2738474';
        cd.Billing_Country__c = 'india';
        cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
        //insert cd;
        //
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //User u = [Select id, name, email from User where id=: UserInfo.getUserId() AND ProfileId = profileId.id];
        
        opportunity op = new opportunity();
        op.Name = 'fgdg';
        op.StageName= 'Open';
        op.CloseDate = Date.Today();
        // insert op;
        Product_Discount__c pd = new Product_Discount__c ();
        pd.Discount_Percentage__c = 23;
        insert pd;
        Address__c ad = new Address__c();
        ad.Shipping_Street__c = 'te';
        ad.Shipping_City__c= 'sdg';
        ad.Primary__c = true;
        ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
        ad.Contact__c= con.id;
        insert ad;
        
        List<Product2> prodList = new List<Product2>();
        
        Product2 prod2 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod2;
        prodList.add(prod2);
        Product2 prod3 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod3;
        prodList.add(prod3);
        Product2 prod4 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod4;
        
        Product2 prod5 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Stripe_Plan_Id__c='cus_0000');
        insert prod5;
        
        Product2 prod6 = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                      Family = 'Hardware',Switch_To_Product_two_month__c=prod5.id,
                                      availableForProfiles__c = 'System Administrator',Stripe_Plan_Id__c='cus_0000');
        insert prod6;
        
        Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware',Switch_To_Product_two_month__c=prod2.id,
                                     Switch_To_Product_three_month__c=prod3.id,
                                     Switch_To_Product_four_month__c=prod4.id,
                                     Switch_To_Product_five_month__c=prod5.id,
                                     Switch_To_Product_six_month__c=prod6.id,
                                     availableForProfiles__c ='System Administrator',Stripe_Plan_Id__c='cus_0000');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 5;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        //insert oli;
        //
        OpportunityLineItem oli2 = new OpportunityLineItem();
        oli.OpportunityId = op.Id;
        oli.Quantity = 2;
        oli.PricebookEntryId = standardPrice.Id;
        oli.TotalPrice = 1 * standardPrice.UnitPrice;
        
        ProductCombination__c pc = new ProductCombination__c();
        pc.Product__c = oli.id;
        pc.ProductToHide__c = oli2.id;
        insert pc;
        
        
        OrdeFormControllerChangesV13.contactWrapper cw=new OrdeFormControllerChangesV13.contactWrapper();
        cw.address = ad;
        cw.ContactId = con.id;
        cw.LastName ='tes';
        
        cw.Name ='ds';
        
        String cwwrapper = JSON.serialize(cw);
        String addresswrapper = JSON.serialize(ad);
        String cardwrapper = JSON.serialize(cd);
        String selectedProd ='[{"checkValue":true,"club":false,"Id":"01t1F000001lECKQA2","isSelectable":true,"months":1,"Name":"Big Fat Lies Book","price":7.95,"productCategory":"Book","productsToSwitchMap":{"0":{"quanititySelector":false}},"quanititySelector":true,"quantity":1,"quantityPrice":0,"standAloneClubProd":false}]';
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
        //
        // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
        test.startTest();
        List<string> lstStr = new List<string>();
        lstStr.add('1');
        OrdeFormControllerChangesV13.formatString(lstStr);    
        OrdeFormControllerChangesV13.getOpportunityDetails(con.id);
        OrdeFormControllerChangesV13.getContactDetails(con.id);
        OrdeFormControllerChangesV13.cardListServer(con.id);
        OrdeFormControllerChangesV13.subscribedProductListServer(con.id);
        OrdeFormControllerChangesV13.refreshContactAddressServer(con.id);
        OrdeFormControllerChangesV13.updateContactWithProduct(con.id,'Book');
        //String s = '{"LastName":"Tes","MailingStreet":"gfdg","MailingState":"dfg","MailingPostalCode":"45655","Stripe_Customer_Id__c":"card_1CiJbsBwLSk1v1ohyZkWjSEc","Id":"0031F0000063z1aQAA","ContactId":"'+con.id+'"},"address":{"Id":"'+ad.id+'"}';
        OrdeFormControllerChangesV13.updateContactDetails(cwwrapper,false);
        OrdeFormControllerChangesV13.createNewAddress(addresswrapper,con.id,true,cwwrapper);
        OrdeFormControllerChangesV13.createCardWithBilling(cardwrapper,con.id);
        // OrdeFormControllerChangesV13.updateCardWithBilling(cardwrapper,cd.id);
        //OrdeFormControllerChangesV13.dummycoverage();
        OrdeFormControllerChangesV13.getUserList('IsActive');
        OrdeFormControllerChangesV13.createCardWithShipping(cardwrapper,con.id,addresswrapper);
        OrdeFormControllerChangesV13.insertOpportunity(selectedProd,cwwrapper,true,String.valueof(system.now()),'[{"card":"a001F000001t9BSQAY","nonClubamount":0,"usedForClub":true}]',66,00,56,'{"Id": "0051F000001Y89NQAS", "Username": "tirthpatel.5892@gmail.com.devs", "Name": "Tirth Patel"}',false,false,null,null,null,7.75,selectedProd,false,false,'Free REST,Free BURN','test','newtest10');
        OrdeFormControllerChangesV13.setProductWrapper(prodList);
        OrdeFormControllerChangesV13.fetchLookUpValue1('Laptop','Product2',prodList,con.id);
        test.stopTest();
    }    
}