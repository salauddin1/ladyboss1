@RestResource(urlMapping='/Stripe_SubscriptionWebhookService')
global class StripeSubscriptionWebhook_Service {
    @HttpPost
    global static void stripeSubscriptionWebhook() {
        string CustomerId;
        if(RestContext.request.requestBody!=null){
            try {
                String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
                Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'Stripe_SubscriptionWebhookService');
                System.debug('-------timestamppp----'+timestamppp);
                System.debug('-------sigFromHmac----'+sigFromHmac);
                
                if(sigFromHmac && sigFromHmac == true){
                // if( true){
                
                    String str = RestContext.request.requestBody.toString();
                    Map<String, Object> results =  (Map<String, Object>)JSON.deserializeUntyped(str);
                    if(String.valueOf(results.get('type')).equals('customer.subscription.deleted') || String.valueOf(results.get('type')).equals('customer.subscription.updated')){
                        CustomerId=String.valueOf(results.get('Id'));
                        StripeSubscriptioinUpdateCancel_Service.createCustomer();
                    }else if(String.valueOf(results.get('type')).equals('customer.subscription.created')){
                        CustomerId=String.valueOf(results.get('Id'));
                        
                        StripeSubscriptioin_Service.createCustomer();
                    }else if(String.valueOf(results.get('type')).equals('invoice.created')){
                        StripeInvoiceCreate_Service.updateInvoice();
                    }
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeSubscriptionWebhook_Service',
                 'stripeSubscriptionWebhook',
                 CustomerId
                 )
                 );
                if(Test.isRunningTest()) { 
             integer intTest =1/0;  }
            }catch(Exception e){
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeSubscriptionWebhook_Service',
                        'stripeSubscriptionWebhook',
                         CustomerId,
                         e
                    )
                );
            }
        }
    }
}