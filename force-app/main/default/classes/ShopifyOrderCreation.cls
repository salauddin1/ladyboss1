public class ShopifyOrderCreation {
    public Static Boolean isrecursive = false;
@AuraEnabled
    public static List<Product2> shipOrder(String searchKey){
        List<Product2> pro = new List<Product2>();
        String key = '%' +searchKey + '%';
        pro = [select id,Name,StockKeepingUnit,Available_For_Shipstation_Orders__c from Product2 where Available_For_Shipstation_Orders__c =: true and Name LIKE :key LIMIT 10 ]; 
        return pro;
    }
    @AuraEnabled
    public static List<Contact> getContact(String contactId){
        List<Contact> con = new List<Contact>();
        con = [select id,FirstName,lastName,Name,MobilePhone,Phone,Email,MailingStreet,MailingCity,MailingAddress,MailingState,MailingCountry,MailingPostalCode, (select id,Shipping_Street__c,Shipping_City__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c,Shipping_Country__c from Addresses__r where Primary__c=true) from Contact where Id =: contactId LIMIT 1 ]; 
        return con;
    }
    @AuraEnabled
    public Static String CreateShopifyOrders(String OrderId, String listItems) {
        String accessToken;
        System.debug('==============================OrderId'+OrderId);
        system.debug('=======listItems'+listItems);
        Map<String, Object> OrderMap = (Map<String, Object>)JSON.deserializeUntyped(OrderId);
        Order ordJson = new Order();
        shopifyLineItemWrapper Items = new shopifyLineItemWrapper();
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c=true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        List<Object> ProdList = (List<Object>)JSON.deserializeUntyped(listItems);
        Map<String, Object> producmap    = new Map<String, Object>();
        Map<String, Object> lineItemData = new Map<String, Object> ();
        ordJson.line_items = new List <shopifyLineItemWrapper>();
        for(Object lineItems : ProdList) {
            producmap    = (Map<String, Object>)lineItems;
            lineItemData = (Map<String, Object>)producmap.get('selectedItem');
            Items = new shopifyLineItemWrapper();
            Items.quantity = Integer.valueOf(producmap.get('Quantity'));
            Items.title    = String.valueOf(lineItemData.get('text'));
            Items.price    = Double.valueOf(lineItemData.get('price'));
            Items.sku      = String.valueOf(lineItemData.get('SKU'));
            ordJson.line_items.add(Items);
        }
        ordJson.email                       = String.valueOf(OrderMap.get('Email__c'));
        ordJson.total_tax                   = Double.valueOf(OrderMap.get('Total_Tax__c'));
        ordjson.fulfillment_status          = null;
        ordjson.send_fulfillment_receipt    = Boolean.valueOf(OrderMap.get('Send_Fulfillment_Receipt__c'));
        ordjson.send_receipt                = Boolean.valueOf(OrderMap.get('Send_Receipt__c'));
        ordjson.financial_status            = 'paid';
        ordJson.shipping_address = new ShippingAddress();
        ordJson.shipping_address.first_name = String.valueOf(OrderMap.get('First_Name__c'));
        ordJson.shipping_address.last_name  = String.valueOf(OrderMap.get('Last_Name__c'));
        ordJson.shipping_address.address1   = String.valueOf(OrderMap.get('address1__c'));
        ordJson.shipping_address.phone      = String.valueOf(OrderMap.get('phone__c'));
        ordJson.shipping_address.city       = String.valueOf(OrderMap.get('city__c'));
        ordJson.shipping_address.province   = String.valueOf(OrderMap.get('province__c'));
        ordJson.shipping_address.country    = String.valueOf(OrderMap.get('country__c'));
        ordJson.shipping_address.zip        = String.valueOf(OrderMap.get('zip__c'));
        ordJson.billing_address = new BillingAddress();
        ordJson.billing_address.first_name  = String.valueOf(OrderMap.get('First_Name__c'));
        ordJson.billing_address.last_name   = String.valueOf(OrderMap.get('Last_Name__c'));
        ordJson.billing_address.address1    = String.valueOf(OrderMap.get('address1__c'));
        ordJson.billing_address.phone       = String.valueOf(OrderMap.get('phone__c'));
        ordJson.billing_address.city        = String.valueOf(OrderMap.get('city__c'));
        ordJson.billing_address.province    = String.valueOf(OrderMap.get('province__c'));
        ordJson.billing_address.country     = String.valueOf(OrderMap.get('country__c'));
        ordJson.billing_address.zip         = String.valueOf(OrderMap.get('zip__c'));
        ordJson.customer = new CustomerInfo();
        ordJson.customer.first_name         = String.valueOf(OrderMap.get('First_Name__c'));
        ordJson.customer.last_name          = String.valueOf(OrderMap.get('Last_Name__c')); 
        ordJson.customer.email              = String.valueOf(OrderMap.get('Email__c'));
        ordJson.note_attributes             = new List<ExtraInfo>();
        ExtraInfo HideRecursive = new ExtraInfo();
        HideRecursive.name                  = 'InitiatedFromSalesforce';
        HideRecursive.value                 = 'InitiatedFromSalesforce';
        ordJson.note_attributes.add(HideRecursive);
        String Formatedjson = JSON.serialize(ordjson);
        System.debug('============Formatedjson======='+Formatedjson);
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders.json');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            req.setBody('{"order":'+Formatedjson+'}');
            Http htp =new Http();
            HttpResponse res= htp.send(req);
            System.debug('==========='+RES.getBody());
            Map<String, Object> results = (Map <String, Object>)JSON.deserializeUntyped(res.getBody());
            Map<String, Object> order   =  (Map<String, Object>)results.get('order');
            Map<String, Object> addressArray =  (Map<String, Object>)order.get('shipping_address');
            String idprod = String.valueOf(order.get('id'));
            if(results != null){
                Shopify_Orders__c ord = new Shopify_Orders__c();
                ord.Order_Id__c                 = String.valueOf(order.get('id'));
                ord.Order_Number__c             = String.valueOf(order.get('order_number'));
                ord.Financial_Status__c         = String.valueOf(order.get('financial_status'));
                ord.Total_Line_Items_Price__c   = Double.valueOf(order.get('total_line_items_price'));
                ord.Total_Price__c              = Double.valueOf(order.get('total_price'));
                ord.Total_Tax__c                = Double.valueOf(order.get('total_tax'));
                ord.Subtotal_Price__c           = Double.valueOf(order.get('subtotal_price'));
                ord.Contact__c                  = String.valueOf(OrderMap.get('Contact__c'));
                ord.Status__c                   = 'Confirmed';
                ord.Email__c                    = String.valueOf(order.get('email'));
                ord.created_at__c               = String.valueOf(order.get('created_at'));
                ord.fulfillment_Status__c       = String.valueOf(order.get('fulfillment_status'));
                ord.Send_Fulfillment_Receipt__c = Boolean.valueOf(OrderMap.get('Send_Fulfillment_Receipt__c'));
                ord.Send_Receipt__c             = Boolean.valueOf(OrderMap.get('Send_Receipt__c'));
                if(order.containsKey('shipping_address')) {
                    ord.First_Name__c               = String.valueOf(addressArray.get('first_name'));
                    ord.Last_Name__c                = String.valueOf(addressArray.get('last_name'));
                    ord.address1__c                 = String.valueOf(addressArray.get('address1'));
                    ord.city__c                     = String.valueOf(addressArray.get('city'));
                    ord.province__c                 = String.valueOf(addressArray.get('province'));
                    ord.country__c                  = String.valueOf(addressArray.get('country'));
                    ord.phone__c                    = String.valueOf(addressArray.get('phone'));
                    ord.zip__c                      = String.valueOf(addressArray.get('zip'));
                }
                isrecursive = true;
                insert ord;
                List<ShopifyLineItem__c> productItems   = new List<ShopifyLineItem__c>();
                List<Object> LineItems                  = (List<Object>)order.get('line_items');
                Map<String, Object> itemMap             = new Map<String, Object>();
                for(Object obj : LineItems){
                    itemMap = (Map <String, Object>)obj;
                    ShopifyLineItem__c shopfyItems = new ShopifyLineItem__c();
                    shopfyItems.Shopify_Orders__c       =   ord.Id;
                    shopfyItems.Item_Id__c              = String.valueOf(itemMap.get('id'));
                    shopfyItems.Price__c                = Double.valueOf(itemMap.get('price'));
                    shopfyItems.Quantity__c             = Double.valueOf(itemMap.get('quantity'));
                    shopfyItems.sku__c                  = String.valueOf(itemMap.get('sku'));
                    shopfyItems.Title__c                = String.valueOf(itemMap.get('title')); 
                    shopfyItems.Name                    = String.valueOf(itemMap.get('title')); 
                    shopfyItems.Contact__c              = String.valueOf(OrderMap.get('Contact__c'));
                    shopfyItems.Fulfillable_Quantity__c = Double.valueOf(itemMap.get('fulfillable_quantity'));
                    productItems.add(shopfyItems);
                }
                insert productItems;
            }
        }
        catch(Exception e){}
        return OrderId;  
    }
    @future(callout=true)
    public Static void UpdateOder(String OrdId){
        String accessToken;
        Shopify_Orders__c OrdList=[select Order_Id__c,phone__c,province__c,zip__c, Email__c,city__c,address1__c,fulfillment_Status__c,First_Name__c,Last_Name__c,country__c from Shopify_Orders__c where Id =: OrdId limit 1];
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        try{
            cancelOrder cord = new cancelOrder();
            cord.id = OrdList.Order_Id__c;
            cord.email = OrdList.Email__c;
            String phone = OrdList.phone__c;
            phone = phone.remove('(');
            phone = phone.remove(')');
            phone = phone.remove('-');
            phone = phone.remove(' ');
            String country                   = OrdList.country__c;
            if(country.equalsIgnoreCase('India')){
                cord.phone = '+91'+phone;
            }
            else{
                cord.phone = '+1'+phone;
            }
            cord.shipping_address = new ShippingAddress();
            cord.shipping_address.first_name = OrdList.First_Name__c;
            cord.shipping_address.last_name  = OrdList.last_Name__c;
            cord.shipping_address.address1   = OrdList.address1__c;
            cord.shipping_address.phone      = OrdList.phone__c;
            cord.shipping_address.city       = OrdList.city__c;
            cord.shipping_address.province   = OrdList.province__c;
            cord.shipping_address.zip        = OrdList.zip__c;
            cord.shipping_address.country    = OrdList.country__c;    
            String jsonForUpdate = JSON.serialize(cord);     
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'.json');
            req.setMethod('PUT');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            req.setBody('{"order":'+jsonForUpdate+'}');
            Http htp =new Http();
            HttpResponse res= htp.send(req);
            System.debug('============='+res.getBody());
        }
        catch(Exception e){}
    }
    @future(callout=true)
    public Static Void cancelledOrder(String OrderId){
        String accessToken;
        Shopify_Orders__c OrdList=[select Order_Id__c,phone__c,province__c,zip__c, Email__c,city__c,address1__c,fulfillment_Status__c,First_Name__c,Last_Name__c,country__c from Shopify_Orders__c where Id =: OrderId limit 1];
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'/cancel.json');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            req.setBody('{}');
            Http htp =new Http();
            HttpResponse res= htp.send(req);
            System.debug('======response======='+res.getBody());
        }
        catch(Exception e){}
    }
    public class Order {
        public Double total_tax;
        public String email;
        public String fulfillment_status;
        public String financial_status;
        public Boolean send_receipt;
        public Boolean send_fulfillment_receipt;
        public List <shopifyLineItemWrapper> line_items;
        public ShippingAddress shipping_address;
        public BillingAddress billing_address;
        public CustomerInfo customer;
        public List<ExtraInfo> note_attributes;
    }
     public class shopifyLineItemWrapper {
        public String title;
        public decimal price;
        public String sku;
        public Integer quantity;
    }
    public class ExtraInfo {
        public String name;
        public String value;
    }
    public class CustomerInfo {
        public String first_name;
        public String last_name;
        public String email;
    }
    public class cancelOrder{
        public String email;
        public String id;
        public String phone;
        public ShippingAddress shipping_address;
    }
    public class ShippingAddress {
        public String first_name;
        public String last_name;
        public String address1;
        public String phone;
        public String city;
        public String province;
        public String country;
        public String zip;
    }
    public class BillingAddress {
        public String first_name;
        public String last_name;
        public String address1;
        public String phone;
        public String city;
        public String province;
        public String country;
        public String zip;
    }
}