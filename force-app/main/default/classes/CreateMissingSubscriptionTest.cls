@isTest
public class CreateMissingSubscriptionTest {
    @isTest
    public static void test1(){
        Missing_Subscription__c ms = new Missing_Subscription__c();
        ms.cust_id__c = 'cusId';
        insert ms;
        
        Contact ct = new Contact();
        ct.LastName = 'Tirth';
        insert ct;
        
        Stripe_Profile__c str = new Stripe_Profile__c();
        str.Stripe_Customer_Id__c = 'cusId';
        str.Customer__c = ct.ID;
        insert str;
        CreateMissingSubscription cs = new CreateMissingSubscription();
        Database.executeBatch(cs,1);
    }
}