@RestResource(urlMapping='/StripeInvoiceToChargeUpdate_Service')
global class StripeInvoiceToChargeUpdate_Service {
    @HttpPost
    global static void updateCharge() {
        String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
        Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'StripeInvoiceToChargeUpdate_Service');
        System.debug('-------timestamppp----'+timestamppp);
        System.debug('-------sigFromHmac----'+sigFromHmac);
        if(sigFromHmac && sigFromHmac == true || Test.isRunningTest()){
        string ChargeId;
        String subscriptionID;
        if(RestContext.request.requestBody!=null){
            try {
                String str = RestContext.request.requestBody.toString();
                System.debug('-------firsttttt twooo----StripeInvoice_ServiceDemo'+str);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                ChargeId                         = String.valueOf(results.get('id')); 
                Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('data');
                Map<String, Object>  lstCustomers2 = (Map<String, Object> )lstCustomers.get('object');
                Map<String, Object>  lstCustomers3 = (Map<String, Object> )lstCustomers2.get('lines');
                List<Object> data = (List<Object>)lstCustomers3.get('data');
                List<Map<String,Object>> dataList = new List<Map<String, Object>>();
                String charge = (String)lstCustomers2.get('charge');
                String invoice = (String)lstCustomers2.get('id');
                Integer endingBalance = -(Integer.valueOf(String.valueOf(lstCustomers2.get('ending_balance'))));
                Integer startingBalance = -(Integer.valueOf(String.valueOf(lstCustomers2.get('starting_balance'))));
                System.debug('starting and ending balance '+ endingBalance+ ' '+ startingBalance);
                String invoiceNumber = (String)lstCustomers2.get('number');
                ChargeId=(String)lstCustomers2.get('id');
                System.debug('-------charge--'+charge+'-----invoice---'+invoice);
                Set<String> subId = new Set<String>();
                Map<String, Object> periodMap = new Map<String, Object>();
                for(Object ob : data){
                    Map<String, Object> obmap = (Map<String, Object>)ob;
                    
                    periodMap.put(String.valueof(obmap.get('id')), ob);
                    subId.add(String.valueof(obmap.get('id')));
                    System.debug('-------res----data--'+obmap.get('id'));
                    dataList.add(obmap);
                }
                
                System.debug('-------res----data--'+subId);
                 //String subVar = (String)lstCustomers2.get('subscription');  
                 subscriptionID = (String)lstCustomers2.get('subscription');             
                  
                 
                 //list<OpportunityLineItem> oppSubList= [select id, Subscription_Id__c from OpportunityLineItem where Subscription_Id__c in : subId];
                List<OpportunityLineItem> oppLineItemList = [SELECT id,OpportunityId,Success_Failure_Message__c,opportunity.Stripe_Charge_Id__c,opportunity.sales_person__c,Subscription_Id__c,Number_Of_Payments__c,Stripe_Charge_Id__c,Start__c,End__c,Status__c,Product2.Price__c FROM OpportunityLineItem where Subscription_Id__c in : subId AND Stripe_Charge_Id__c=null];
                System.debug('--->oppLineItemList---first ---'+oppLineItemList);
                system.debug('--->oppLineItemList-->'+String.valueOf(oppLineItemList) );
                set<id> opplineoppSet = new Set<id>();
                for(OpportunityLineItem opline : oppLineItemList){
                    if (charge != null) {
                        opLine.Stripe_charge_id__c  = charge;                    
                        Map<String, Object> obmap = (Map<String, Object>)periodMap.get(opline.Subscription_Id__c);
                        Map<String, Object> period = (Map<String, Object>)obmap.get('period');
                        Datetime dT = DateTime.newInstance(Long.valueOf(String.valueOf(period.get('start'))) * 1000);
                        opLine.Start__c = date.newinstance(dT.year(), dT.month(), dT.day());    
                        Datetime dT1 = DateTime.newInstance(Long.valueOf(String.valueOf(period.get('end'))) * 1000);
                        opLine.End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                        opLine.Status__c ='Active';
                    }
                    
                    opplineoppSet.add(opline.OpportunityId);
                }
                List<OpportunityLineItem> opLineItemChargeIdUpdate = [SELECT id,Last_Charge_Id__c,Balance_Applied_on_Last_Invoice__c,opportunity.OwnerId,Subscription_Id__c,Same_Balance_as_Before__c,OpportunityId,Product2.Commission_Enabled_Club_Product__c,Product2.Price__c,Product2.Is_Eligible_For_Affiliate_Creation__c  FROM OpportunityLineItem where Subscription_Id__c in : subId];
                List<String> subAlreadyPresent = new List<String>();
                Set<Id> opportunityIds = new Set<Id>();
                for(OpportunityLineItem oli: opLineItemChargeIdUpdate){
                    opportunityIds.add(oli.OpportunityId);
                }
                List<Pap_Commission__c> insertPAPList = new List<Pap_Commission__c>();
                List<Opportunity> oppoList = [select id,Coupon_Code__c,Contact__c,wc_order_id__c,WC_Product_Id__c,WC_Order_Number__c ,Contact__r.PAP_refid__c,Contact__r.Email,Contact__r.Name from Opportunity where id in : opportunityIds];
                Map<Id,Opportunity> oppoMap = new Map<Id,Opportunity>(oppoList);
                List<Pap_Commission__c> pAPList = [select id, commission__c, Opportunity__c, Contact__c, Campaign_Id__c,
                                                   Campaign_name__c, affiliate_ref_id__c, order_id__c, product_id__c,
                                                   data1__c, data2__c, data4__c 
                                                   from Pap_Commission__c where Contact__c != null AND Opportunity__c in :opportunityIds order by createddate ASC];
                Map<Id,Pap_Commission__c> papMap = new Map<Id,Pap_Commission__c>();
                for(Pap_Commission__c pap :pAPList){
                    if(!papMap.containsKey(pap.Opportunity__c)){
                        papMap.put(pap.Opportunity__c,pap);
                    }
                }
                for (OpportunityLineItem opline: opLineItemChargeIdUpdate) {
                    if (charge != null) {
                        if(test.isRunningTest()){
                            Contact ct = new Contact();
                            ct.LastName = 'sdgs';
                            ct.PAP_refid__c = 'bvxcb';
                            ct.Email = 'sfdgsdg@gfdg.com';
                            ct.FirstName = 'sdfsdg';
                            insert ct;
                            Opportunity opp = oppoMap.get(opline.OpportunityId);
                            opp.Contact__c = ct.Id;
                            opp.Coupon_Code__c = 'sdgsg';
                            update opp;
                            oppoMap.put(opp.Id,opp);
                            opline.Last_Charge_Id__c = 'fvbx';
                            opline.Product2.Commission_Enabled_Club_Product__c = true;
                            opline.Product2.Is_Eligible_For_Affiliate_Creation__c = true;
                            //System.debug('Contact__r.Pap_refId__c:-'+oppoMap.get(opline.OpportunityId).Contact__r.Pap_refId__c);
                            //System.debug('Commission_Enabled_Club_Product__c:-'+opline.Product2.Commission_Enabled_Club_Product__c);
                            //System.debug('Is_Eligible_For_Affiliate_Creation__c:-'+opline.Product2.Is_Eligible_For_Affiliate_Creation__c);
                            //System.debug('Last_Charge_Id__c:-'+opline.Last_Charge_Id__c);
                            //System.debug('charge:-'+charge);
                        }
                        if(oppoMap.get(opline.OpportunityId).Coupon_Code__c != null 
                           && opline.Last_Charge_Id__c != null
                           && opline.Last_Charge_Id__c != charge 
                           && papMap.containsKey(opline.OpportunityId) 
                           && (oppoMap.get(opline.OpportunityId).Contact__c != papMap.get(opline.OpportunityId).Contact__c 
                           || startingBalance == endingBalance)){
                            Pap_Commission__c pap = new Pap_Commission__c();
                            pap.Contact__c = papMap.get(opline.OpportunityId).Contact__c;
                            pap.commission__c = papMap.get(opline.OpportunityId).commission__c;
                            pap.Commission_Type__c = 'repeated';
                            pap.Opportunity__c = opline.OpportunityId;
                            pap.Campaign_Id__c = papMap.get(opline.OpportunityId).Campaign_Id__c;
                            pap.Campaign_name__c = papMap.get(opline.OpportunityId).Campaign_name__c;
                            pap.affiliate_ref_id__c = papMap.get(opline.OpportunityId).affiliate_ref_id__c;
                            pap.order_id__c = papMap.get(opline.OpportunityId).order_id__c;
                            pap.product_id__c = papMap.get(opline.OpportunityId).product_id__c;
                            pap.data1__c = papMap.get(opline.OpportunityId).data1__c;
                            pap.data2__c = papMap.get(opline.OpportunityId).data2__c;
                            pap.data4__c = papMap.get(opline.OpportunityId).data4__c;
                            pap.data5__c = oppoMap.get(opline.OpportunityId).WC_Order_Number__c;
                            pap.Total_Cost__c = opline.Product2.Price__c;
                            insertPAPList.add(pap);
                           } 
                        if(oppoMap.get(opline.OpportunityId).Contact__r.Pap_refId__c != null
                           && opline.Product2.Commission_Enabled_Club_Product__c 
                           && opline.Last_Charge_Id__c != null
                           && opline.Last_Charge_Id__c != charge 
                           &&  startingBalance == endingBalance){
                               Pap_Commission__c pap = new Pap_Commission__c();
                               pap.Contact__c = oppoMap.get(opline.OpportunityId).Contact__c;
                               pap.commission__c = (opline.Product2.Price__c * 33.34)/100;
                               pap.Commission_Type__c = 'repeated';
                               pap.Opportunity__c = opline.OpportunityId;
                               pap.Campaign_Id__c = '2605c613';
                               pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
                               pap.affiliate_ref_id__c = oppoMap.get(opline.OpportunityId).Contact__r.Pap_refId__c;
                               pap.order_id__c = oppoMap.get(opline.OpportunityId).wc_order_id__c;
                               pap.product_id__c = oppoMap.get(opline.OpportunityId).WC_Product_Id__c;
                               pap.data1__c = oppoMap.get(opline.OpportunityId).Contact__r.Email;
                               pap.data2__c = oppoMap.get(opline.OpportunityId).Contact__r.Name;
                               pap.data4__c = String.valueOf(opline.Product2.Price__c);
                               pap.data5__c = oppoMap.get(opline.OpportunityId).WC_Order_Number__c; 
                               pap.Total_Cost__c = opline.Product2.Price__c;
                               insertPAPList.add(pap);
                           }
                        else if((oppoMap.get(opline.OpportunityId).Contact__r.Pap_refId__c == null 
                                 || oppoMap.get(opline.OpportunityId).Contact__r.Pap_refId__c == '') 
                                && opline.Last_Charge_Id__c != null
                                && opline.Last_Charge_Id__c != charge
                                && opline.Product2.Is_Eligible_For_Affiliate_Creation__c ){
                                    String paprefId = createAffiliateController.checkRefId(oppoMap.get(opline.OpportunityId).Contact__c);
                                    if(!test.isRunningTest()){
                                        createAffiliateController.createAffiliate(oppoMap.get(opline.OpportunityId).Contact__c);
                                    }
                                    if(opline.Product2.Commission_Enabled_Club_Product__c){
                                        Pap_Commission__c pap = new Pap_Commission__c();
                                        pap.Contact__c = oppoMap.get(opline.OpportunityId).Contact__c;
                                        pap.commission__c = (opline.Product2.Price__c * 33.34)/100;
                                        pap.Commission_Type__c = 'repeated';
                                        pap.Opportunity__c = opline.OpportunityId;
                                        pap.Campaign_Id__c = '2605c613';
                                        pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
                                        pap.affiliate_ref_id__c = paprefId;
                                        pap.order_id__c = oppoMap.get(opline.OpportunityId).wc_order_id__c;
                                        pap.product_id__c = oppoMap.get(opline.OpportunityId).WC_Product_Id__c;
                                        pap.data1__c = oppoMap.get(opline.OpportunityId).Contact__r.Email;
                                        pap.data2__c = oppoMap.get(opline.OpportunityId).Contact__r.Name;
                                        pap.data4__c = String.valueOf(opline.Product2.Price__c);
                                        pap.data5__c = oppoMap.get(opline.OpportunityId).WC_Order_Number__c;
                                        pap.Total_Cost__c = opline.Product2.Price__c;
                                        insertPAPList.add(pap);
                                    }
                        }
                        opLine.Last_Charge_Id__c = charge;
                    }
                    if (startingBalance != 0 && startingBalance != endingBalance) {
                        if (opline.Balance_Applied_on_Last_Invoice__c != (startingBalance - endingBalance)*0.01) {
                            opline.Balance_Applied_on_Last_Invoice__c = (startingBalance - endingBalance)*0.01;
                        }else {
                            opline.Same_Balance_as_Before__c = true;
                        }
                    }
                    subAlreadyPresent.add(opline.Subscription_Id__c);
                }
                if(insertPAPList.size()>0){
                    insert insertPAPList;
                }
                
                List<Opportunity> oppList = [SELECT id,Coupon_Code__c,Stripe_Charge_Id__c,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c,User_to_give_credit__c  from Opportunity where id in : opplineoppSet AND Clubbed__c = true AND Secondary_Stripe_Charge_Id__c=null AND Stripe_Charge_Id__c=null];
                List<Opportunity> oppListToBeUpdated = new List<Opportunity>();
                system.debug('--------------------oppList----------------------'+oppList);
                
                for(Opportunity opp : oppList){
                    if(charge!=null && opp!=null){
                        
                        opp.Stripe_Charge_Id__c = charge;
                        if (invoiceNumber!=null) {
                            opp.Charge_Description__c = 'Invoice '+ invoiceNumber;
                        }
                        System.debug(opp.Stripe_Charge_Id__c);
                        List<card__c> card = SubscriptionCardLink_cls.getCard(charge,opp);
                        if(card != null && card.size() > 0) {
                            opp.card__c = card[0].id;
                            opp.cardId__c = card[0].Stripe_Card_Id__c;
                        }
                    }
                    //opp.Number_Of_Payments__c=opp.Number_Of_Payments__c+1;
                    oppListToBeUpdated.add(opp);
                }
                
               /* update oppListToBeUpdated;
                System.debug('charge id added now ');
                if(oppLineItemList.size() > 0){
                    system.debug('-oppLineItemList-->'+String.valueOf(oppLineItemList));
                    update  oppLineItemList;
                }*/       
                
                List<Invoice__c> existedInv = [Select id,Subscription_Id__c,Charge_Id__c,Invoice_Id__c from Invoice__c where Charge_Id__c =: charge];
                List<Invoice__c> invList = new List<Invoice__c>();
                if(!(existedInv.size() > 0)){
                    system.debug('========subId======'+subId);
                    system.debug('========subAlreadyPresent========='+subAlreadyPresent);
                    for(String sub : subId){
                        if (!subAlreadyPresent.contains(sub)) {
                            Invoice__c inv = new Invoice__c();
                            inv.Charge_Id__c = charge;
                            inv.Subscription_Id__c = sub;
                            inv.Invoice_Id__c = invoice;
                            Map<String, Object> obmap = (Map<String, Object>)periodMap.get(sub);
                            Map<String, Object> period = (Map<String, Object>)obmap.get('period');
                            Datetime dT = DateTime.newInstance(Long.valueOf(String.valueOf(period.get('start'))) * 1000);
                            inv.Period_Start__c = date.newinstance(dT.year(), dT.month(), dT.day());    
                            Datetime dT1 = DateTime.newInstance(Long.valueOf(String.valueOf(period.get('end'))) * 1000);
                            inv.Period_End__c = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                            if (invoiceNumber!=null) {
                                inv.Invoice_Number__c = invoiceNumber;
                            }
                            invList.add(inv); 
                            
                            
                            
                            
                        }
                        
                    }
                                      
                }
                system.debug('--->oppListToBeUpdated-->'+String.valueOf(oppListToBeUpdated) );
                if(oppListToBeUpdated.size() > 0){
                    for(Opportunity opp : oppListToBeUpdated){
                        OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate(opp);
                    }
                }
                if(oppLineItemList.size()  > 0){
                    system.debug('--->oppLineItemList inside-->'+oppLineItemList);
                    Set<String> setof = new Set<String>();
                    for(OpportunityLineItem opli :oppLineItemList ){
                        if(opli.Stripe_charge_id__c != opli.opportunity.Stripe_Charge_Id__c){
                            if(setof.add(opli.stripe_charge_id__c)){
                                if(opli.opportunity.sales_person__c!=null && opli.opportunity.sales_person__c!=''){
                                    OpportunityWithStipeChargeIdHandler.createMetadataAndUpdateOLI(opli,opli.opportunity.sales_person__c);
                                }
                            }
                        }
                        
                    }
                }
                if (opLineItemChargeIdUpdate.size()>0) {
                    update opLineItemChargeIdUpdate;
                }  
                if (invList.size()>0) {
                    insert invList;
                }
                if (oppListToBeUpdated.size()>0) {
                   update oppListToBeUpdated;   
                }  
                if(oppLineItemList.size() > 0){
                    update  oppLineItemList;
                } 
                
                try{
                    List<Opportunity> oppListcharge = [select id,ownerID,Subscription__c,(select id,Subscription_Id__c,Success_Failure_Message__c  from OpportunityLineItems ), Success_Failure_Message__c ,Stripe_Charge_Id__c from opportunity where (Stripe_Charge_Id__c!=null and Stripe_Charge_Id__c=:charge) ]; 
                    List<OpportunityLineItem> oliListSub = [select id,opportunity.ownerID,Subscription_Id__c,Success_Failure_Message__c    from OpportunityLineItem where (Subscription_Id__c!=null and Subscription_Id__c =:subscriptionID) and Success_Failure_Message__c='customer.subscription.updated']; 
                    List<Opportunity> opListUpdateOwner =   new List<Opportunity>();      
                    system.debug('----oppListcharge-----'+oppListcharge);
                    system.debug('----charge-----'+charge);
                    Map<string, opportunity> oppMap = new Map<string, opportunity>();
                    Map<string, opportunity> oppMapCharge = new Map<string, opportunity>();
                    if(oppListcharge.size() >0){
                        for(Opportunity op : oppListcharge) {
                            if(oliListSub.size() > 0 && op.ownerID!=oliListSub[0].opportunity.ownerId ){
                                op.OwnerId = oliListSub[0].opportunity.ownerId;
                                opListUpdateOwner.add(op);
                            }
                        
                        
                        } 
                    }
                    
                    
                    
                    if(opListUpdateOwner.size() >0){
                        update opListUpdateOwner;
                    }
                }catch(Exception e) {
                    RestResponse res = RestContext.response; 
                    res.statusCode = 400;
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                    new ApexDebugLog.Error(
                    'StripeInvoiceToChargeUpdate_Service',
                    'ownerUpdate',
                    ChargeId,
                    e
                    )
                    );
                }
                
                
                
                
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeInvoiceToChargeUpdate_Service',
                 'updateCharge',
                 ChargeId
                 )
                 );               
                if(Test.isRunningTest())  
                    integer intTest =1/0;
            } catch(Exception e) {
                RestResponse res = RestContext.response; 
            res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeInvoiceToChargeUpdate_Service',
                        'updateCharge',
                        ChargeId,
                        e
                    )
                );
            }
        }
        
    }
    }  
}