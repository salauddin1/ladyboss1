@isTest
public class  AssociateOpportunityCampaignTest {
 public static testMethod void test(){
 Triggers_activation__c tc = new Triggers_activation__c();
 tc.SuccessPaymentWithCaseLink__c = true;
 tc.isCampaignAssociate__c = true;
 
 insert tc;
     Contact Con =new Contact();
        Con.LastName = 'Test Con';
        insert Con;
Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.contact__c = Con.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.OwnerId= userinfo.getuserid();
        op1.campaign__c = 'test';
        insert op1;
        
        
        Opportunity op2 = new Opportunity();
        op2.Amount = 100;
        op2.contact__c = Con.id;
        op2.Scheduled_Payment_Date__c = Date.today();
        op2.Name = 'opp1';
        op2.Sales_Person_Id__c = userinfo.getuserid();
        op2.StageName = 'Closed Won';
        op2.CloseDate = System.today();
        op2.OwnerId= userinfo.getuserid();
        op2.campaign__c = null;
        insert op2;
    List<opportunity> oplist = new List<opportunity>();
    oplist.add(op2);
    AssociateOpportunityCampaign.updateCampaignUpdate(oplist);
        }

}