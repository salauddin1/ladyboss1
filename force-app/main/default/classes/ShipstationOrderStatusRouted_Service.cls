global class ShipstationOrderStatusRouted_Service implements Database.Batchable<Sobject>,Database.AllowsCallouts {
 	global Static Boolean flag = false;
	global database.querylocator start(database.batchableContext bc){
         String query = 'select orderId__c, orderKey__c,orderStatus__c from ShipStation_Orders__c where CreatedDate=LAST_N_DAYS:7 AND orderStatus__c !=\'cancelled\' AND orderStatus__c != \'shipped\'';
         return database.getquerylocator(query);
   }
 global void execute(database.batchablecontext bd, list<ShipStation_Orders__c> scope){
     	String header;
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }   
        	Datetime StartDate = DateTime.now();
        	String StartDattime = String.valueOf(StartDate.addDays(-7));
        	StartDattime = StartDattime.replace(' ', 'T');
        	String EndDate =String.valueOf(DateTime.now());
        	EndDate = EndDate.replace(' ', 'T');
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?createDateStart='+StartDattime+'&createDateEnd='+EndDate+'&orderStatus=cancelled');
            req.setMethod('GET');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Accept', 'application/json');
     		String ResponseBody;
     		try{
            	HttpResponse res = new Http().send(req);
                ResponseBody = res.getBody();
            }
     		catch(Exception e){}
        	Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
        	List<Object> OrderList = (List<Object>)ordResult.get('orders');
       		Map<String, Object> OrderMap = new Map<String, Object> ();
        	Map<String, String> OrderIdMap = new Map<String, String> ();
            Set<String> orderids = new Set<String>();
            for(Object obj : OrderList){
            OrderMap = (Map<String, Object>)obj;
            orderids.add(String.valueOf(OrderMap.get('orderId')));
            OrderIdMap.put(String.valueOf(OrderMap.get('orderKey')),String.valueOf(OrderMap.get('orderId')));
        }
     if(!scope.isEmpty() && !OrderIdMap.isEmpty()){
        for(ShipStation_Orders__c ord : scope){
            if(OrderIdMap.get(ord.orderKey__c) == ord.orderId__c){
                ord.orderStatus__c = String.valueOf(OrderMap.get('orderStatus'));
            }
        }
     }
     	flag = true;
        update scope;
 }
 Public void finish(database.batchableContext bc){ 
 }
}