@isTest
public class updateCard_Test {
  
    public static testMethod void test(){
        try{
       
        test.startTest();
        
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
                Con.Product__c = 'Unlimited';
        insert Con;
      
      
          String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        op.RecordTypeId = devRecordTypeId;
       op.CardId__c= 'card_1EGAqnFzCf73siP0e9xVOl8s';
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_Eje9LCKHjiRRn3';
      //  op.Stripe_Charge_Id__c = 'evt_1DJguhBwLSk1v1ohwrZpgJIl';
        op.Stripe_Charge_Id__c = 'ch_1DJdaWBwLSk1v1oh16CirbyJ';
        op.Contact__c = Con.Id;
        insert op;
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_Eje9LCKHjiRRn3';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        
        updateCard.meth();
        
        test.stopTest();    
            }
         catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        } 
    }  
       
    
}