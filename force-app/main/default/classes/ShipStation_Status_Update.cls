public class ShipStation_Status_Update {
     @future(callout=true)
    public static void getStatusTotal(){
        String header;
        ShipstationUpdate_pages__c ord;
        ShipstationCancel_pages__c objCan ;
        Date StartDate = Date.today();
        String StartDattime = String.valueOf(StartDate.addDays(-1))+'T00:00:00';
        System.debug('===StartDattime=='+StartDattime);
        String EndDate =String.valueOf(DateTime.now());
        EndDate = EndDate.replace(' ', 'T');
        
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        try{
            HttpRequest req1 = new HttpRequest();
            req1.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?modifyDateStart='+StartDattime+'&modifyDateEnd='+EndDate+'&orderStatus=shipped&pageSize=1');
            req1.setMethod('GET');
            req1.setHeader('Authorization', 'Basic '+header);
            //req1.setTimeout(3000);
            req1.setHeader('Accept', 'application/json');
            String ResponseBody1;
            HttpResponse res1 = new Http().send(req1);
            Integer statusCode = res1.getStatusCode();
            if(statusCode == 200){
                
                ResponseBody1 = res1.getBody();
            }
            System.debug('=============ResponseBody====shipped======'+ResponseBody1);
            Map<String,Object> response1 = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody1);
            System.debug('=============ResponseBody====response1======'+response1);
            Integer total = Integer.valueOf(response1.get('total'));
            System.debug('=============ResponseBody====total======'+total);
            
            HttpRequest req2 = new HttpRequest();
            req2.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?modifyDateStart='+StartDattime+'&modifyDateEnd='+EndDate+'&orderStatus=cancelled&pageSize=1');
            req2.setMethod('GET');
            req2.setHeader('Authorization', 'Basic '+header);
            req2.setHeader('Accept', 'application/json');
            req2.setTimeout(6000);
            String ResponseBody2;
            HttpResponse res2 = new Http().send(req2);
            Integer statusCode1 = res2.getStatusCode();
            if(statusCode1 == 200){
                
                ResponseBody2 = res2.getBody();
            }
            System.debug('=================ResponseBody Cancelled==========='+ResponseBody2);
            Map<String,Object> response2 = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody2);
            System.debug('=============ResponseBody====response1======'+response2);
            Integer totalCan = Integer.valueOf(response2.get('total'));
            System.debug('=============ResponseBody====total======'+totalCan);
            
            if(ResponseBody1 != null) {
                ord = [select End_page__c,Start_Page__c from ShipstationUpdate_pages__c limit 1];
                ord.End_Page__c = total; 
                update ord ;
            }
            if(ResponseBody2 != null) {
                objCan = [select End_page__c,Start_Page__c from ShipstationCancel_pages__c limit 1];
                objCan.End_Page__c = totalCan; 
                update objCan ;
            }
        }
        catch(exception e){}
        
    }
}