global class LinkContactWithCardAndAddress implements Database.Batchable<sObject>, Schedulable  {	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select id,Customer_Id__c,Salesforce_Record_Type__c,Salesforce_Record_Id__c From Webhook_Table__c');
	}

	 global void execute(SchedulableContext sc) {
            LinkContactWithCardAndAddress d = new LinkContactWithCardAndAddress();
              database.executebatch(d, 10);
        }
        
   	global void execute(Database.BatchableContext BC, List<Webhook_Table__c> scope) {
		String custId;
		try{
				
		   		Map<Id,String> addMap = new Map<Id,String>();
		   		Map<Id,String> cardMap = new Map<Id,String>();
		   		List<Webhook_Table__c> wtToBeDeleted = new List<Webhook_Table__c>();
		   		Set<String> cusId = new Set<String>();
				for(Webhook_Table__c wt:scope){
					if(wt.Salesforce_Record_Type__c=='Address__c'){
						addMap.put(wt.Salesforce_Record_Id__c, wt.Customer_Id__c);
						wtToBeDeleted.add(wt);
					}else if(wt.Salesforce_Record_Type__c=='Card__c'){
						cardMap.put(wt.Salesforce_Record_Id__c, wt.Customer_Id__c);
						wtToBeDeleted.add(wt);
					}	
					cusId.add(wt.Customer_Id__c);	
				}
				custId = String.valueOf(cusId);
				if(addMap.size()>0){
					system.debug('addMap '+addMap);
					Set<Id> addId = addMap.keySet();
					List<String> customerId = addMap.values();
					List<Address__c> addList= [Select Id,Contact__c,CustomerID__c From Address__c Where Id IN : addId AND Contact__c=null ];
					List<Stripe_Profile__c> conList = [select Customer__c,Stripe_Customer_Id__c from Stripe_Profile__c where Stripe_Customer_Id__c IN : customerId and Stripe_Customer_Id__c!=null and Customer__c!=null]; 
                    
					if(addList.size()>0 && conList.size()>0){
						system.debug('addList '+addList);
						system.debug('conList '+conList);
						for(Address__c address:addList){
							for(Stripe_Profile__c con:conList){
								if(address.CustomerID__c==con.Stripe_Customer_Id__c){
									address.Contact__c=con.Customer__c;							
								}
							}
						}
						update addList;
					}
				}

				if(cardMap.size()>0){
					system.debug('cardMap '+cardMap);
					Set<Id> cardId = cardMap.keySet();
					List<String> customerIde = cardMap.values();
					List<Card__c> cardList= [Select Id,Contact__c,CustomerID__c From Card__c Where Id IN : cardId AND Contact__c=null ];
					List<Stripe_Profile__c> conList1 =  [select Customer__c,Stripe_Customer_Id__c from Stripe_Profile__c where Stripe_Customer_Id__c IN : customerIde and Stripe_Customer_Id__c!=null and Customer__c!=null];
					
					if(cardList.size()>0 && conList1.size()>0){
						system.debug('cardList '+cardList);
						system.debug('conList1 '+conList1);
						for(Card__c card:cardList){
							for(Stripe_Profile__c con:conList1){
								if(card.CustomerID__c==con.Stripe_Customer_Id__c){
									card.Contact__c=con.Customer__c;
								}
							}
						}
						update cardList;
						
					}
				}
				if(wtToBeDeleted.size()>0){
					system.debug('wtToBeDeleted '+wtToBeDeleted);
					delete wtToBeDeleted;
				}

			}catch(Exception ex){
				ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                new ApexDebugLog.Error(
                 'LinkContactWithCardAndAddress',
                 'BatchForWebhookTable',
                 custId,
                 ex
                 )
                 );
                throw ex;
			}

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
    
    global static void dummyCoverage()  {
        Integer i = 0;
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;        
        i++;
        
    }
	
}