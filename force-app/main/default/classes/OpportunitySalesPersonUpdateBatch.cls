global class OpportunitySalesPersonUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
    List<first_monday_of_year__c> fmList = [SELECT day__c,month__c,year__c FROM first_monday_of_year__c];
    datetime dt_time = datetime.newInstance(Integer.valueof(fmList.get(0).year__c), Integer.valueof(fmList.get(0).month__c), Integer.valueof(fmList.get(0).day__c),0,0,0);
    date dt = date.newInstance(Integer.valueof(fmList.get(0).year__c), Integer.valueof(fmList.get(0).month__c), Integer.valueof(fmList.get(0).day__c));
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT id,Opportunity.Sales_Person_Id__c,Opportunity.ownerId FROM Opportunity where Stripe_Charge_Id__c!=null AND (createdDate >= : dt_time OR Scheduled_Payment_Date__c >= : dt) AND Sales_Person_Id__c=null AND owner.name!=\'Stripe Site Guest User\'');
    }
    global void execute(Database.BatchableContext bc, List<Opportunity> oppList){
        System.debug('oppList : '+oppList);
        for(Opportunity op : oppList){
                op.Sales_Person_Id__c = op.OwnerId;
        }
        try{
        	update oppList;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        System.debug('oppList : '+oppList);
    }
    global void finish(Database.BatchableContext bc){ 
        // execute any post-processing operations
    }
}