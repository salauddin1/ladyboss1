public class ACHPaymentController {
    
    public String stipeApiKey { get; set; }
    
    public Id contactId { get; set; }
    public String ACHJson { get; set; }
    public String token { get; set; }
    public String messageError { get; set; }
    public String addressJson { get; set; }
    public boolean ValueReturned {get; set;}
    public boolean fakeCall {get; set;}
    public boolean isSafe {get; set;}
    public boolean isError {get; set;}
    ACH_Account__c newACH;
     
    public ACHPaymentController(){
        this.isError = false;
        isSafe = false;
        messageError ='';
        Map<String,String> addrMap = new Map<String,String>();
        this.stipeApiKey = StripeAPI.PublishableKey;
        this.contactId = Id.valueOf(ApexPages.currentPage().getParameters().get('cntId'));
    }
    
    public PageReference InsertRecord() {
        try{
            messageError = '';
            this.isError = false;
            System.debug('ACHJson-----'+ACHJson);
            System.debug('token-----'+token);
            token  = (String)JSON.deserialize(token, String.class);
            System.debug('token 2-----'+token);
            Map<String,Object> ACHAccount = (Map<String,Object>) Json.deserializeUntyped(ACHJson);
            System.debug('ACHAccount-----'+ACHAccount);
            
            ACH_Account__c newACH = new ACH_Account__c();
            
            List<Stripe_Profile__c> existingStripeProfiles = new List<Stripe_Profile__c>();
            existingStripeProfiles =[select id,Customer__c,Customer__r.firstname from Stripe_Profile__c where Customer__c=:contactId order by createdDate limit 1];
            if(!existingStripeProfiles.isEmpty()){
                newACH.Stripe_Profile__c = existingStripeProfiles[0].id;        
            }else{
                Stripe_Profile__c newStriprProfile = new Stripe_Profile__c();
                newStriprProfile.Customer__c = contactId;
                insert newStriprProfile;
                newACH.Stripe_Profile__c = newStriprProfile.id;
            }
            
            newACH.ACH_Token__c = token;
            newACH.Contact__c = contactId;

            newACH.Last4__c = String.valueOf(ACHAccount.get('last4'));
            newACH.Account_Holder_Type__c = String.valueOf(ACHAccount.get('account_holder_type'));
			newACH.Account_Holder_Name__c = String.valueOf(ACHAccount.get('account_holder_name'));
            
            insert newACH;
            this.newACH = newACH;
            isSafe = true;
            system.debug('insert----'+newACH);
            
        }catch(exception ex){
            isSafe = false;
            messageError = 'Salesforce Error - '+ex;
            this.isError = true;
        }
        return null;
    }
    
    public PageReference CallWebService() {
        String message;
        //this.isError = false;
        System.debug('------before callout-----');
        try{
            if(isSafe != null && isSafe){
            System.debug('newCard-----'+newACH.id);
            message = StripeIntegrationHandlerChanges.ACHWithTokenStripeRealTimeHandler(newACH.Id);
            if(message != null && message !=''){
                delete newACH;
                messageError = message;
                this.isError = true;
            }else{
                this.isError = false;
            }
        }
        }catch(exception ex){
            delete newACH;
            messageError = 'Error--'+ex;
            this.isError = true;
        }
        return null;
    }
}