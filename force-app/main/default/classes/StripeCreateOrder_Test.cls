@isTest
public class StripeCreateOrder_Test {
    static testMethod void test(){
        Map<String, Decimal> skuID = new Map<String, Decimal>();
        skuID.put('test', 2.00);
        Map<String, String> metadata = new Map<String, String>();
        metadata.put('abc', 'def');
        List<StripeCreateOrder.shipping_methods> smList = new List<StripeCreateOrder.shipping_methods>();
        StripeCreateOrder.shipping_methods sm = new StripeCreateOrder.shipping_methods();
        sm.amount = 3652;
        sm.delivery_days = 5;
        sm.description = 'USPS: Express';
       smList.add(sm);
        StripeCreateOrder sc = new StripeCreateOrder();
        sc.charge = 'asd';
        
        sc.shipping_methods = smList;
        
        test.startTest();
        StripeCreateOrder.createOrder('type','sd', skuID, 'stripeCurrency', 'shippingName', 'shippingStreet', 'shippingCity','ca','12345','us');
        StripeCreateOrder.payOrder('1234', 'abc', metadata);
        StripeCreateOrder.payOrder('test232','test232','test232','test1234','test232','test232','test@test.com', '435345435','123456','','','','','','','');
        StripeCreateOrder.parse('{"object":"test","date":"test","end":"test","data":"test","currency":"test","type":"test","customer":"test","invoice":"test"}');
        StripeCreateOrder.updateOrder(sc, 'abc', 'def',5);
        test.stopTest();
    }
}