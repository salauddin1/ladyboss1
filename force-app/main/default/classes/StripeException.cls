public class StripeException extends Exception {
    public StripeError error;
    public StripeChargeError chargeError;
    
    public StripeException(StripeError err) {
        this.error = err;
    }
    
    public StripeException(StripeChargeError err) {
        this.chargeError = err;
    }

    public override String getMessage() {
        return (this.error!=null ? this.error.message : '') + (this.chargeError!=null ? this.chargeError.message : '');
    }
    
    public StripeError getError() {
        return this.error;
    }

}