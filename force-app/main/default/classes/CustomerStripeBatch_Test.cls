@IsTest
public class CustomerStripeBatch_Test {
    static{
        Stripe_Parameters__c sp = new Stripe_Parameters__c(Token__c='test');
        insert sp;
    }
    
	static testMethod void test() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BUOWQlyttHhSXd');
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new CustomerStripeBatch());
        Test.stopTest();
	}
    
    static testMethod void test2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BUOWQlyttHhSXd');
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new CustomerStripeBatch('test'));
        Test.stopTest();
	}
}