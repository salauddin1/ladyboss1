global class StripeConnection {
    public static HttpResponse HttpRequest(String obj, String params){
        Stripe_Parameters__c sp = Stripe_Parameters__c.getOrgDefaults();
        String endpoint = sp.Endpoint__c;
            
        HTTP http = new HTTP(); 
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(endpoint + obj + params);
        req.setMethod('GET');      
        req.setTimeout(120000);
        Blob headerValue = Blob.valueOf(sp.Token__c);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);  
        HttpResponse res = new HttpResponse();
		
        res = http.send(req);         
        //system.debug('StripeConnection.HttpRequest res.getStatusCode()'+res.getStatusCode());
        //system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());			
        return res;
    }
    
    @future(callout=true)
    public static void HttpSend(String obj, String externalId, string objId, Map<String,String> params){
        Stripe_Parameters__c sp = Stripe_Parameters__c.getOrgDefaults();
        String endpoint = sp.Endpoint__c;
        
        HTTP http = new HTTP();
        HTTPRequest req = new HTTPRequest();
        if(externalId==''){
        	req.setEndpoint(endpoint + obj);
        }else{
            req.setEndpoint(endpoint + obj + '/' + externalId);
        }
        req.setMethod('POST');      
        Blob headerValue = Blob.valueOf(sp.Token__c);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
		Integer cont = 0;
		String body = '';
        for(String key : params.keySet()){
			cont++;
			if(cont==1)
				body = key + '=' + params.get(key);
			else
            	body = body + '&' + key + '=' + params.get(key);
        }

        req.setBody(body);
        HttpResponse res = new HttpResponse();
		
        res = http.send(req);            
        system.debug('StripeConnection.HttpRequest res.getStatusCode(): '+ res.getStatusCode());	
        system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());	
        if(res.getStatusCode()==200){
            if(obj=='customers'){
                Account acc = [Select Id, Name, Card_ID__c, Credit_Card_Number__c, Exp_Month__c, Exp_Year__c, Cvc__c, Brand__c, Country__c, 
                               BillingStreet, BillingState, BillingCity, BillingPostalCode, BillingCountry
                               From Account WHERE Id =: objId];
                StripeCustomer.Object_Z varCustomer = (StripeCustomer.Object_Z) json.deserialize(res.getBody(),StripeCustomer.Object_Z.class);
                //Account acc = new Account();
               	String messageError = '';
                if(acc.Card_ID__c==null){
                    if(acc.Exp_Month__c!=null && acc.Exp_Year__c!=null && acc.Credit_Card_Number__c!=null && acc.Cvc__c!=null){
                        req.setEndpoint(endpoint + 'customers/' + varCustomer.id + '/sources');
                        req.setMethod('POST');  
                        params = new Map<String,String>();
                        params.put('source[object]','card');
                        params.put('source[exp_month]',String.valueOf(acc.Exp_Month__c));
                        params.put('source[exp_year]',String.valueOf(acc.Exp_Year__c));
                        params.put('source[number]',acc.Credit_Card_Number__c);
                        params.put('source[cvc]',acc.Cvc__c);
                        params.put('source[name]',acc.Name);
                        if(acc.BillingStreet!='' && acc.BillingStreet!=null) params.put('source[address_line1]',acc.BillingStreet); else params.put('source[address_line1]','');
                        if(acc.BillingCity!='' && acc.BillingCity!=null) params.put('source[address_city]',acc.BillingCity); else params.put('source[address_city]','');
                        if(acc.BillingCountry!='' && acc.BillingCountry!=null) params.put('source[address_country]',acc.BillingCountry); else params.put('source[address_country]','');
                        if(acc.BillingState!='' && acc.BillingState!=null) params.put('source[address_state]',acc.BillingState); else params.put('source[address_state]','');
                        if(acc.BillingPostalCode!='' && acc.BillingPostalCode!=null) params.put('source[address_zip]',acc.BillingPostalCode); else params.put('source[address_zip]','');
                        cont = 0;
                        body = '';
                        for(String key : params.keySet()){
                            cont++;
                            if(cont==1)
                                body = key + '=' + params.get(key);
                            else
                                body = body + '&' + key + '=' + params.get(key);
                        }
                        req.setBody(body);
                        res = http.send(req);
                        if(res.getStatusCode()==200){
                            acc.Invalid_Card__c = false;
                            messageError = '';
                            StripeCard.Object_Z varCard = (StripeCard.Object_Z) json.deserialize(res.getBody().replaceAll('object','object_data'),StripeCard.Object_Z.class);
                            acc.Card_ID__c = varCard.id;
                            if(varCard.address_city!=null) acc.BillingCity = varCard.address_city;
                            if(varCard.address_country!=null) acc.BillingCountry = varCard.address_country;
                            if(varCard.address_line1!=null) acc.BillingStreet = varCard.address_line1;
                            if(varCard.address_state!=null) acc.BillingState = varCard.address_state;
                            if(varCard.address_zip!=null) acc.BillingPostalCode = varCard.address_zip;
                            acc.Brand__c = varCard.brand;
                            acc.Country__c = varCard.country;
                            acc.Cvc_Check__c = varCard.cvc_check;
                            acc.Exp_Month__c = String.valueOf(varCard.exp_month);
                            acc.Exp_Year__c = varCard.exp_year;
                            acc.Last4__c = varCard.last4;
                        }else{
                            acc.Invalid_Card__c = true;
                            JSONParser parser = JSON.createParser(res.getBody());
                            while (parser.nextToken() != null) { if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'message')) { parser.nextToken(); messageError = parser.getText(); } }
                        }
                    }
                }else{
                    system.debug('##LLEGO5');
                    req.setEndpoint(endpoint + 'customers/' + varCustomer.id + '/sources/'+acc.Card_ID__c);
                    req.setMethod('POST');  
                    params = new Map<String,String>();
                    params.put('exp_month',String.valueOf(acc.Exp_Month__c));
                    params.put('exp_year',String.valueOf(acc.Exp_Year__c));
                    params.put('name',acc.Name);
                    if(acc.BillingStreet!='' && acc.BillingStreet!=null) params.put('address_line1',acc.BillingStreet); else params.put('address_line1','');
                    if(acc.BillingCity!='' && acc.BillingCity!=null) params.put('address_city',acc.BillingCity); else params.put('address_city','');
                    if(acc.BillingCountry!='' && acc.BillingCountry!=null) params.put('address_country',acc.BillingCountry); else params.put('address_country','');
                    if(acc.BillingState!='' && acc.BillingState!=null) params.put('address_state',acc.BillingState); else params.put('address_state','');
                    if(acc.BillingPostalCode!='' && acc.BillingPostalCode!=null) params.put('address_zip',acc.BillingPostalCode); else params.put('address_zip','');
                    cont = 0;
                    body = '';
                    for(String key : params.keySet()){
                        cont++;
                        if(cont==1)
                            body = key + '=' + params.get(key);
                        else
                            body = body + '&' + key + '=' + params.get(key);
                    }
                    req.setBody(body);
                    res = http.send(req);
                    if(res.getStatusCode()==200){
                        system.debug('Done');
                    }
                }
                
                acc.Id = objId;
                acc.Invalid_Message__c = messageError;
                acc.External_ID__c = varCustomer.id;
                ApexUtil.isTriggerInvoked = true;
                update acc;
                ApexUtil.isTriggerInvoked = false;
            }else if(obj=='plans'){
                StripePlan.StripePlanObject varPlan = (StripePlan.StripePlanObject) json.deserialize(res.getBody().replace('object','object_data').replace('currency','currency_data'),StripePlan.StripePlanObject.class);
                Product2 pro = [Select Id, Name From Product2 WHERE Id =: objId];
                pro.Id = objId;
                pro.External_ID__c = varPlan.id;
                ApexUtil.isTriggerInvoked = true;
                update pro;
                ApexUtil.isTriggerInvoked = false;
            }
        }
    }
    
    webservice static String pay(String oppId){
        Stripe_Parameters__c sp = Stripe_Parameters__c.getOrgDefaults();
        String endpoint = sp.Endpoint__c;
        
        Opportunity opp = [Select Id, StageName, Description, Amount, currency__c, AccountId, External_ID__c, Subscription__c, Billing__c, Days_Until_Due__c, Account.External_ID__c From Opportunity WHERE Id=:oppId];
        
        if(opp!=null){
            List<OpportunityLineItem> olis = [Select Id, OpportunityId, PricebookEntry.Product2.External_ID__c, PricebookEntry.Product2.Type__c, Quantity, PricebookEntry.Product2.One_Time_Payment__c From OpportunityLineItem WHERE OpportunityId=:oppId];
            if(olis.size()>0){
                if(opp.AccountId!=null){ 
                    Boolean errorValidation = false;
                    Boolean oneTimeValidation = false;
                    for(OpportunityLineItem oli : olis){
                        if(oli.PricebookEntry.Product2.Type__c!='Stripe'){
                            errorValidation = true;
                        }
                        if(oli.PricebookEntry.Product2.One_Time_Payment__c){
                            oneTimeValidation = true;
                        }
                    }
                    
                    HTTP http;
                    HTTPRequest req;
                    Blob headerValue;
                    String authorizationHeader;
                    HttpResponse res;
                    String body;
                    Integer cont;
                    
                    if(!errorValidation){
                        if(oneTimeValidation){
                            if(opp.currency__c!=null){
                                if(opp.External_ID__c==null && opp.Subscription__c==null && opp.StageName!='Approved'){
                                    Map<String,Object> mapCharge = new Map<String,Object>();
                                    mapCharge.put('customer',opp.Account.External_ID__c);
                                    mapCharge.put('amount',String.valueOf(opp.Amount).replace('.',''));
                                    if(opp.Description!=null) mapCharge.put('description',opp.Description);
                                    mapCharge.put('currency',opp.currency__c);
                                    
                                    body = '';
                                    cont = 0;
                                    for(String key : mapCharge.keySet()){
                                        cont++;
                                        if(cont==1)
                                            body = key + '=' + mapCharge.get(key);
                                        else
                                            body = body + '&' + key + '=' + mapCharge.get(key);
                                    }
                                    
                                    system.debug('##body: '+body);
                                    http = new HTTP();
                                    req = new HTTPRequest();
                                    req.setEndpoint(endpoint + 'charges');
                                    req.setMethod('POST');      
                                    headerValue = Blob.valueOf(sp.Token__c);
                                    authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                                    req.setHeader('Authorization', authorizationHeader);
                                    req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                                    req.setBody(body);
                                    res = new HttpResponse();
                                    
                                    res = http.send(req);            
                                    system.debug('StripeConnection.HttpRequest res.getStatusCode(): '+ res.getStatusCode());	
                                    system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());	
                                    if(res.getStatusCode()==200){
                                        StripeCharge.Object_Z varCharge = (StripeCharge.Object_Z) json.deserialize(res.getBody(),StripeCharge.Object_Z.class);
                                        Opportunity oppUpsert = new Opportunity(); 
                                        oppUpsert.Id = opp.Id;
                                        oppUpsert.DML_From_Salesforce__c = true;
                                        oppUpsert.Subscription__c = varCharge.id;
                                        oppUpsert.External_ID__c = varCharge.id;
                                        ApexUtil.isTriggerInvoked = true;
                                        update oppUpsert;
                                        ApexUtil.isTriggerInvoked = false;
                                    }else{
                                        String msg = res.getBody().trim();
                                        Integer msgPos = msg.indexOf('"message": "');
                                        if(msgPos>-1){    
                                            msg = msg.subString(msgPos+12,msg.length());
                                            msgPos = msg.indexOf('"');
                                            if(msgPos > -1){
                                                msg = msg.subString(0,msgPos);
                                            }
                                        }
                                        return 'Error: ' + msg;
                                    }
                                }else{
                                    return 'This opportunity already registers a payment.';
                                }
                            }else{
                                return 'This opportunity needs a currency.';
                            }
                        }else{
                            if(opp.External_ID__c==null && opp.Subscription__c==null && opp.StageName!='Approved'){
                                Map<String,String> mapCustomerExternal = new Map<String,String>();
                                Map<String,String> mapCustomerCard = new Map<String,String>();
                                
                                Account acc = [Select Id, External_ID__c, Card_ID__c From Account WHERE Id =: opp.AccountId];
                                String customerId = acc.External_ID__c;
                                String sourceCard = acc.Card_ID__c;
                                
                                if(opp.Billing__c!=null){
                                    if((sourceCard!=null && opp.Billing__c=='charge_automatically') || (opp.Billing__c=='send_invoice')){
                                        Map<String,Object> mapSubscription = new Map<String,Object>();
                                        //mapSubscription.put('description',opp.Description);
                                        //mapSubscription.put('amount',String.valueOf(opp.Amount).replace('.',''));
                                        //mapSubscription.put('currency',opp.currency__c);
                                        if(opp.Billing__c=='send_invoice'){
                                            if(opp.Days_Until_Due__c!=null){
                                                mapSubscription.put('days_until_due',opp.Days_Until_Due__c);
                                            }else{
                                                mapSubscription.put('days_until_due',30);
                                            }
                                        }
                                        if(opp.Billing__c!=null) mapSubscription.put('billing',opp.Billing__c);
                                        //mapSubscription.put('plan',olis.get(0).PricebookEntry.Product2.External_ID__c);
                                        mapSubscription.put('customer',customerId);
                                        
                                        Map<String,Map<String,Object>> items = new Map<String,Map<String,Object>>();
                                        Map<String,Object> item;
                                        Integer contItem = 0;
                                        for(OpportunityLineItem oli : olis){
                                            item = new Map<String,Object>();
                                            item.put('plan',String.valueOf(oli.PricebookEntry.Product2.External_ID__c));
                                            item.put('quantity',String.valueOf(Integer.valueOf(oli.Quantity)));
                                            items.put(String.valueOf(contItem),item);
                                            contItem++; 
                                        }
                                        mapSubscription.put('items',items);
                                        system.debug('##mapSubscription: '+mapSubscription);
                                        http = new HTTP();
                                        req = new HTTPRequest();
                                        req.setEndpoint(endpoint + 'subscriptions');
                                        req.setMethod('POST');      
                                        headerValue = Blob.valueOf(sp.Token__c);
                                        authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                                        req.setHeader('Authorization', authorizationHeader);
                                        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                                        
                                        cont = 0;
                                        body = '';
                                        Map<String,Object> maptemp;
                                        Map<String,Object> maptemp2;
                                        for(String key : mapSubscription.keySet()){
                                            cont++;
                                            if(key=='items'){
                                                maptemp = (Map<String,Map<String,Object>>) mapSubscription.get(key);
                                                for(String key2 : maptemp.keySet()){
                                                    maptemp2 = (Map<String,Object>) maptemp.get(key2);
                                                    for(String key3 : maptemp2.keySet()){
                                                        body = body + '&items' + '[' + key2 + '][' + key3 + ']=' + maptemp2.get(key3);
                                                    }
                                                }
                                            }else{
                                                if(cont==1)
                                                    body = key + '=' + mapSubscription.get(key);
                                                else
                                                    body = body + '&' + key + '=' + mapSubscription.get(key);
                                            }
                                        }
                                        system.debug('##body: '+body);
                                        req.setBody(body);
                                        res = new HttpResponse();
                                        
                                        res = http.send(req);            
                                        system.debug('StripeConnection.HttpRequest res.getStatusCode(): '+ res.getStatusCode());	
                                        system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());	
                                        if(res.getStatusCode()==200){
                                            StripeCharge.Object_Z varCharge = (StripeCharge.Object_Z) json.deserialize(res.getBody(),StripeCharge.Object_Z.class);
                                            Opportunity oppUpsert = new Opportunity(); 
                                            oppUpsert.Id = opp.Id;
                                            oppUpsert.DML_From_Salesforce__c = true;
                                            oppUpsert.Subscription__c = varCharge.id;
                                            ApexUtil.isTriggerInvoked = true;
                                            update oppUpsert;
                                            ApexUtil.isTriggerInvoked = false;
                                        }else{
                                            String msg = res.getBody().trim();
                                            Integer msgPos = msg.indexOf('"message": "');
                                            if(msgPos>-1){    
                                                msg = msg.subString(msgPos+12,msg.length());
                                                msgPos = msg.indexOf('"');
                                                if(msgPos > -1){
                                                    msg = msg.subString(0,msgPos);
                                                }
                                            }
                                            return 'Error: ' + msg;
                                        }
                                    }else{
                                        return 'The account does not have a registered card.';
                                    }
                                }else{
                                    return 'Select a billing option.';
                                }
                            }else{
                                return 'This opportunity already registers a payment.';
                            }
                        }
					}else{
                        return 'Select the correct payment processing.';
                    }
                }else{
                    return 'Do not have an account.';
                }
            }else{
                return 'You must add a product.';
            }
        }else{
            return 'Opportunity does not exist.';
        }
                        
        return 'Ok';
    }
    
    @AuraEnabled
    public static String payLightning(Id oppId){
        Stripe_Parameters__c sp = Stripe_Parameters__c.getOrgDefaults();
        String endpoint = sp.Endpoint__c;
        
        Opportunity opp = [Select Id, StageName, Description, Amount, currency__c, AccountId, External_ID__c, Subscription__c, Billing__c, Days_Until_Due__c, Account.External_ID__c From Opportunity WHERE Id=:oppId];
        
        if(opp!=null){
            List<OpportunityLineItem> olis = [Select Id, OpportunityId, PricebookEntry.Product2.External_ID__c, PricebookEntry.Product2.Type__c, Quantity, PricebookEntry.Product2.One_Time_Payment__c From OpportunityLineItem WHERE OpportunityId=:oppId];
            if(olis.size()>0){
                if(opp.AccountId!=null){ 
                    Boolean errorValidation = false;
                    Boolean oneTimeValidation = false;
                    for(OpportunityLineItem oli : olis){
                        if(oli.PricebookEntry.Product2.Type__c!='Stripe'){
                            errorValidation = true;
                        }
                        if(oli.PricebookEntry.Product2.One_Time_Payment__c){
                            oneTimeValidation = true;
                        }
                    }
                    
                    HTTP http;
                    HTTPRequest req;
                    Blob headerValue;
                    String authorizationHeader;
                    HttpResponse res;
                    String body;
                    Integer cont;
                    
                    if(!errorValidation){
                        if(oneTimeValidation){
                            if(opp.currency__c!=null){
                                if(opp.External_ID__c==null && opp.Subscription__c==null && opp.StageName!='Approved'){
                                    Map<String,Object> mapCharge = new Map<String,Object>();
                                    mapCharge.put('customer',opp.Account.External_ID__c);
                                    mapCharge.put('amount',String.valueOf(opp.Amount).replace('.',''));
                                    if(opp.Description!=null) mapCharge.put('description',opp.Description);
                                    mapCharge.put('currency',opp.currency__c);
                                    
                                    body = '';
                                    cont = 0;
                                    for(String key : mapCharge.keySet()){
                                        cont++;
                                        if(cont==1)
                                            body = key + '=' + mapCharge.get(key);
                                        else
                                            body = body + '&' + key + '=' + mapCharge.get(key);
                                    }
                                    
                                    system.debug('##body: '+body);
                                    http = new HTTP();
                                    req = new HTTPRequest();
                                    req.setEndpoint(endpoint + 'charges');
                                    req.setMethod('POST');      
                                    headerValue = Blob.valueOf(sp.Token__c);
                                    authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                                    req.setHeader('Authorization', authorizationHeader);
                                    req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                                    req.setBody(body);
                                    res = new HttpResponse();
                                    
                                    res = http.send(req);            
                                    system.debug('StripeConnection.HttpRequest res.getStatusCode(): '+ res.getStatusCode());	
                                    system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());	
                                    if(res.getStatusCode()==200){
                                        StripeCharge.Object_Z varCharge = (StripeCharge.Object_Z) json.deserialize(res.getBody(),StripeCharge.Object_Z.class);
                                        Opportunity oppUpsert = new Opportunity(); 
                                        oppUpsert.Id = opp.Id;
                                        oppUpsert.DML_From_Salesforce__c = true;
                                        oppUpsert.Subscription__c = varCharge.id;
                                        oppUpsert.External_ID__c = varCharge.id;
                                        ApexUtil.isTriggerInvoked = true;
                                        update oppUpsert;
                                        ApexUtil.isTriggerInvoked = false;
                                    }else{
                                        String msg = res.getBody().trim();
                                        Integer msgPos = msg.indexOf('"message": "');
                                        if(msgPos>-1){    
                                            msg = msg.subString(msgPos+12,msg.length());
                                            msgPos = msg.indexOf('"');
                                            if(msgPos > -1){
                                                msg = msg.subString(0,msgPos);
                                            }
                                        }
                                        return 'Error: ' + msg;
                                    }
                                }else{
                                    return 'This opportunity already registers a payment.';
                                }
                            }else{
                                return 'This opportunity needs a currency.';
                            }
                        }else{
                            if(opp.External_ID__c==null && opp.Subscription__c==null && opp.StageName!='Approved'){
                                Map<String,String> mapCustomerExternal = new Map<String,String>();
                                Map<String,String> mapCustomerCard = new Map<String,String>();
                                
                                Account acc = [Select Id, External_ID__c, Card_ID__c From Account WHERE Id =: opp.AccountId];
                                String customerId = acc.External_ID__c;
                                String sourceCard = acc.Card_ID__c;
                                
                                if(opp.Billing__c!=null){
                                    if((sourceCard!=null && opp.Billing__c=='charge_automatically') || (opp.Billing__c=='send_invoice')){
                                        Map<String,Object> mapSubscription = new Map<String,Object>();
                                        //mapSubscription.put('description',opp.Description);
                                        //mapSubscription.put('amount',String.valueOf(opp.Amount).replace('.',''));
                                        //mapSubscription.put('currency',opp.currency__c);
                                        if(opp.Billing__c=='send_invoice'){
                                            if(opp.Days_Until_Due__c!=null){
                                                mapSubscription.put('days_until_due',opp.Days_Until_Due__c);
                                            }else{
                                                mapSubscription.put('days_until_due',30);
                                            }
                                        }
                                        if(opp.Billing__c!=null) mapSubscription.put('billing',opp.Billing__c);
                                        //mapSubscription.put('plan',olis.get(0).PricebookEntry.Product2.External_ID__c);
                                        mapSubscription.put('customer',customerId);
                                        
                                        Map<String,Map<String,Object>> items = new Map<String,Map<String,Object>>();
                                        Map<String,Object> item;
                                        Integer contItem = 0;
                                        for(OpportunityLineItem oli : olis){
                                            item = new Map<String,Object>();
                                            item.put('plan',String.valueOf(oli.PricebookEntry.Product2.External_ID__c));
                                            item.put('quantity',String.valueOf(Integer.valueOf(oli.Quantity)));
                                            items.put(String.valueOf(contItem),item);
                                            contItem++; 
                                        }
                                        mapSubscription.put('items',items);
                                        system.debug('##mapSubscription: '+mapSubscription);
                                        http = new HTTP();
                                        req = new HTTPRequest();
                                        req.setEndpoint(endpoint + 'subscriptions');
                                        req.setMethod('POST');      
                                        headerValue = Blob.valueOf(sp.Token__c);
                                        authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                                        req.setHeader('Authorization', authorizationHeader);
                                        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                                        
                                        cont = 0;
                                        body = '';
                                        Map<String,Object> maptemp;
                                        Map<String,Object> maptemp2;
                                        for(String key : mapSubscription.keySet()){
                                            cont++;
                                            if(key=='items'){
                                                maptemp = (Map<String,Map<String,Object>>) mapSubscription.get(key);
                                                for(String key2 : maptemp.keySet()){
                                                    maptemp2 = (Map<String,Object>) maptemp.get(key2);
                                                    for(String key3 : maptemp2.keySet()){
                                                        body = body + '&items' + '[' + key2 + '][' + key3 + ']=' + maptemp2.get(key3);
                                                    }
                                                }
                                            }else{
                                                if(cont==1)
                                                    body = key + '=' + mapSubscription.get(key);
                                                else
                                                    body = body + '&' + key + '=' + mapSubscription.get(key);
                                            }
                                        }
                                        system.debug('##body: '+body);
                                        req.setBody(body);
                                        res = new HttpResponse();
                                        
                                        res = http.send(req);            
                                        system.debug('StripeConnection.HttpRequest res.getStatusCode(): '+ res.getStatusCode());	
                                        system.debug('StripeConnection.HttpRequest res.getBody(): '+ res.getBody());	
                                        if(res.getStatusCode()==200){
                                            StripeCharge.Object_Z varCharge = (StripeCharge.Object_Z) json.deserialize(res.getBody(),StripeCharge.Object_Z.class);
                                            Opportunity oppUpsert = new Opportunity(); 
                                            oppUpsert.Id = opp.Id;
                                            oppUpsert.DML_From_Salesforce__c = true;
                                            oppUpsert.Subscription__c = varCharge.id;
                                            ApexUtil.isTriggerInvoked = true;
                                            update oppUpsert;
                                            ApexUtil.isTriggerInvoked = false;
                                        }else{
                                            String msg = res.getBody().trim();
                                            Integer msgPos = msg.indexOf('"message": "');
                                            if(msgPos>-1){    
                                                msg = msg.subString(msgPos+12,msg.length());
                                                msgPos = msg.indexOf('"');
                                                if(msgPos > -1){
                                                    msg = msg.subString(0,msgPos);
                                                }
                                            }
                                            return 'Error: ' + msg;
                                        }
                                    }else{
                                        return 'The account does not have a registered card.';
                                    }
                                }else{
                                    return 'Select a billing option.';
                                }
                            }else{
                                return 'This opportunity already registers a payment.';
                            }
                        }
					}else{
                        return 'Select the correct payment processing.';
                    }
                }else{
                    return 'Do not have an account.';
                }
            }else{
                return 'You must add a product.';
            }
        }else{
            return 'Opportunity does not exist.';
        }
                        
        return 'Ok';
    }
}