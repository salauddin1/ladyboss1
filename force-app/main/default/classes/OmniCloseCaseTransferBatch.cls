//Batch to update case owner of Cases which are owned by omni queues and whose status is 'Closed'
global class OmniCloseCaseTransferBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id,ownerid FROM Case WHERE Status =\'Closed\' AND OwnerId IN (SELECT Id FROM Group WHERE QueueRoutingConfigId != null AND Name != \'Grant Omni Test Queue\')');
    }   
    global void execute(Database.BatchableContext bc, List<Case> caseList){
    	List<id> ownerIdList = new List<Id>();
    	Map<Id,Id> omniClosedCaseQueueMap  = new Map<Id,Id>();
        for(Case cs:caseList){
            ownerIdList.add(cs.ownerid);
        }
        for(Omni_Closed_Case_Queue_Map__c om:[SELECT id,Omni_Queue_Id__c,Omni_Closed_Queue_ID__c FROM Omni_Closed_Case_Queue_Map__c WHERE Omni_Queue_Id__c IN :ownerIdList]){
        	omniClosedCaseQueueMap.put(om.Omni_Queue_Id__c,om.Omni_Closed_Queue_ID__c);
        }
        for(Case cs:caseList){
            if (omniClosedCaseQueueMap.containsKey(cs.ownerid)) {
                cs.ownerid = omniClosedCaseQueueMap.get(cs.ownerid);
            }
            
        }
        if(caseList.size()>0){
            update caseList;
        }
            
    } 
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }  
}