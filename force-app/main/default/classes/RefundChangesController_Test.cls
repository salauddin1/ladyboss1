@isTest
public class RefundChangesController_Test {
    
    public static testMethod void test3(){
        //system.Test.setMock(HttpCalloutMock.class, new RefundSubCharge_Mock());
         ContactTriggerFlag.isContactBatchRunning = true;
        
        
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =136;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_E0ZChaE2riw6oq';
        ol.refund__c = 137;
        insert ol;
        
        Test.StartTest();
        RefundChangesController.getOpportunity(co.Id);
        RefundChangesController.getOpportunityLineItem(co.Id);
        RefundChangesController.getPartiallyPaidOpportunity(co.Id);
         RefundChangesController.getrefundedOpportunity(co.Id);
        RefundChangesController.getOpportunityLineItemRefund(co.Id);
        RefundChangesController.orderRefundWithOrderIdWithoutOpp('oppOrderId');
        RefundChangesController.chargeRefundWithChargeIdWithoutOpp(123, 'oppChargeId');
        RefundChangesController.getUpdate('contactId', opp.id, ol.id, 12, 'reason', 'oppChargeId', 'internalReason', 'irDetails');
        RefundChangesController.getUpdate('contactId', null, null, 12, 'reason', opp.Stripe_Charge_Id__c, 'internalReason', 'irDetails');
        RefundChangesController.getCustomer( 'subscriptionId');
        RefundChangesController.getOpportunityLineItem1(co.Id);
        RefundChangesController.getOpportunity1(co.Id);
        RefundChangesController.subscriptionChargeRefund(co.Id,opp.Id,ol.Id,12, 'dropVal', 'internalReason', 'irDetails');
        RefundChangesController.subscriptionWithChargeIdRefund(opp.Id,ol,12, 'dropVal',  'internalReason',  'irDetails');
        RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,12, 'dropVal',  'internalReason',  'irDetails');
        //RefundChangesController.dummy();
        Test.stopTest();
    }
    
    public static testMethod void test1(){
        //system.Test.setMock(HttpCalloutMock.class, new MockCharge());
         ContactTriggerFlag.isContactBatchRunning = true;
        contact co = new contact();
        co.LastName ='vijay';
        //co.Stripe_Customer_Id__c='cus_E0ZBk0uyJfzLXB';
        insert co;
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
        opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.Secondary_Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='succeeded';
        opp.Quantity__c =1;
        insert opp;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 7690, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.refund__c= 137;
        //ol.s ='ch_1DafZmBwLSk1v1ohUH1yKzvh';
        insert ol;
        
        Test.StartTest();
        // RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,7690,'duplicate');
        // RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,7690,'fraudulent');
        // RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,7690,'Requested_by_customer');
        // RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,7690,'other');
        RefundChangesController.getUpdate('contactId', null, null, 12, 'reason', opp.Secondary_Stripe_Charge_Id__c, 'internalReason', 'irDetails');
        RefundChangesController.getUpdate('contactId', null, ol.Id, 12, 'reason', null, 'internalReason', 'irDetails');
        RefundChangesController.subscriptionChargeRefund(co.Id,opp.Id,ol.Id,11, 'dropVal', 'internalReason', 'irDetails');
        RefundChangesController.subscriptionWithChargeIdRefund(opp.Id,ol,11, 'dropVal',  'internalReason',  'irDetails');
        RefundChangesController.chargeRefund(co.Id,opp.Id,ol.Id,11, 'dropVal',  'internalReason',  'irDetails');
        RefundChangesController.chargeRefundWithChargeId(opp,11, 'dropVal',opp.Secondary_Stripe_Charge_Id__c,'Secondary_Stripe_Charge_Id__c', 'internalReason', 'irDetails');
        RefundChangesController.getCustomerByCharge('ch_1DafZmBwLSk1v1ohUH1yKzvh');
        RefundChangesController.updateCustomerMetdata('hello','ch_1DafZmBwLSk1v1ohUH1yKzvh');
        Test.stopTest();
    }
    
}