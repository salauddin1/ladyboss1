global class CardAddressHandler {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    public static void setCardAddress(Address__c address){
        system.debug('first=>'+address.Contact__c);
        List<Card__C> cardList = [SELECT id,Stripe_Profile__r.Stripe_Customer_Id__c,Stripe_Card_Id__c FROM Card__c WHERE Contact__c = :address.Contact__c AND Contact__c != null AND Stripe_Card_Id__c != null AND Stripe_Profile__r.Stripe_Customer_Id__c != null];
        List<Id> cardIdList = new List<Id>();
        Set<Id> cardIdSet = new Set<Id>();
        for (Card__c card : cardList) {
            cardIdList.add(card.Id);
        }
        system.debug('first=>'+cardIdList);
        for (OpportunityLineItem opli : [SELECT id,Opportunity.Card__c FROM OpportunityLineItem WHERE Opportunity.Card__c in :cardIdList AND Status__c != 'InActive' AND Status__c != null]) {
            cardIdSet.add(opli.Opportunity.Card__c);
        }
        
        system.debug('first=>'+cardIdSet);
        for (Card__c card : cardList) {
            if (cardIdSet.contains(card.Id)) {
                updateCard(card.Stripe_Profile__r.Stripe_Customer_Id__c,card.Stripe_Card_Id__c,address.Shipping_City__c,address.Shipping_Country__c,address.Shipping_Street__c,address.Shipping_State_Province__c,address.Shipping_Zip_Postal_Code__c);
            }
        }
        for (Stripe_Profile__c stprofile : [SELECT id,Customer__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Customer__c =: address.Contact__c AND Customer__c != null AND Stripe_Customer_Id__c != null AND createdby.name != 'Stripe Site Guest User']) {
            updateStripeCustomer(stprofile.Stripe_Customer_Id__c,address.Shipping_City__c,address.Shipping_Country__c,address.Shipping_Street__c,address.Shipping_State_Province__c,address.Shipping_Zip_Postal_Code__c);
        }
    }
   // @future(callout=true)
    public static void updateCard(String customerId,String cardId,String city,String country,String line1,String state,String zip){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId + '/sources/'+cardId);
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        http.setBody('address_city='+city+'&address_line1='+line1+'&address_country='+country+'&address_state='+state+'&address_zip='+zip);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                
            }
        } 
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            if (!Test.isRunningTest()) {
                StripeCard o = StripeCard.parse(response);
                System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            }    
           
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
   }
   public static void updateStripeCustomer(String customerId,String city,String country,String line1,String state,String zip){
        String BillingAddress = city+' '+line1+' '+state+' '+country+' '+zip;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL + customerId );
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        http.setBody('metadata[Shipping Address]='+BillingAddress);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                
            }
        } 
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            if (!Test.isRunningTest()) {
                StripeCard o = StripeCard.parse(response);
                System.debug(System.LoggingLevel.INFO, '\n**** StripeCard object: '+o); 
            }    
           
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
   }  
}