public class WooPaypalSubscriptionHandler {
    public static void create(String str){        
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
        String payment_method = (String)results.get('payment_method_title');
        if(payment_method.toLowerCase()=='paypal'){
            ContactTriggerFlag.isContactBatchRunning = true;
            List<Object> oliLineItems = (List<Object>)results.get('line_items');
            Map<String,String> prodIdPlanIdMap = new Map<String,String>();
            String OppName = '';
            for( Object oli : oliLineItems){
                Map<String, Object> ol = (Map<String, Object>)oli;
                String ol_name = (String)ol.get('name');
                OppName = OppName + ol_name;
                String pro_id = String.valueOf(ol.get('product_id'));
                String strplanid = getStripePlanId(pro_id,'PlayGround');
                prodIdPlanIdMap.put(pro_id,strplanid);
            }
            if(OppName.length()>250){
                OppName = OppName.substring(0, 250);
            }
            Map<String, Object> billing = (Map<String, Object>)results.get('billing');
            String mail = (String)billing.get('email');
            String phone = (String)billing.get('phone');
            String first_name = (String)billing.get('first_name');
            String last_name = (String)billing.get('last_name');
            String address_1 = (String)billing.get('address_1');
            String address_2 = (String)billing.get('address_2');
            String city = (String)billing.get('city');
            String state = (String)billing.get('state');
            String postcode = (String)billing.get('postcode');
            String country = (String)billing.get('country');
            //create contact if does not exist
            List<Contact> ctList = [SELECT id FROM Contact WHERE email=:mail order by createdDate LIMIT 1];
            Contact ct = new Contact();
            if(ctList.size()==0){
                ct.email = mail;
                ct.phone = phone;
                if(last_name=='' || last_name==null){
                    ct.LastName = first_name;
                }else{
                    ct.FirstName = first_name;
                    ct.LastName = last_name;
                }
                insert ct;
                Address__c addr = new Address__c();
                String street='';
                if(address_2!=null || address_2!=''){
                    street = address_1+','+address_2;
                }
                addr.Shipping_Street__c = street;
                addr.Shipping_City__c = city;
                addr.Shipping_State_Province__c = state;
                addr.Shipping_Country__c = country;
                addr.Shipping_Zip_Postal_Code__c = postcode;
                addr.Contact__c = ct.id;
                insert addr;
            }else{
                ct = ctList.get(0);
            }
            
            //create Opportunity
            String next_payment_date= (String)results.get('next_payment_date');
            String billing_period = (String)results.get('billing_period');
            String billing_interval = (String)results.get('billing_interval');
            String start_date = (String)results.get('start_date');
            String end_date = (String)results.get('end_date');
            String trial_end_date = (String)results.get('trial_end_date');
            String status = (String)results.get('status');
            String subs_id = String.valueOf(results.get('id'));
            String subs_number = (String)results.get('number');
            String total = (String)results.get('total');
            Opportunity opp = new Opportunity();
            opp.Name = OppName;
            opp.CloseDate = System.today();
            opp.StageName = 'closed won';
            opp.Status__c = status;
            opp.WP_Subs_Id__c = subs_id;
            opp.WP_Subs_Number__c = subs_number;
            opp.Contact__c = ct.Id;
            opp.WP_Total__c = Decimal.valueOf(total);
            opp.WP_Next_Payment_Date__c = Datetime.valueOf(next_payment_date.replace('T',' ')).addHours(-6);
            opp.WP_Billing_Period__c = billing_period;
            opp.WP_Billing_Interval__c = billing_interval;
            opp.WP_Start_Date__c = Datetime.valueOf(start_date.replace('T',' ')).addHours(-6);
            if(end_date!=null && end_date!=''){
                opp.WP_End_Date__c =Datetime.valueOf(end_date.replace('T',' ')).addHours(-6);
            }
            
            if(trial_end_date!=null && trial_end_date!=''){
                opp.WP_Trial_End_Date__c =Datetime.valueOf(trial_end_date.replace('T',' ')).addHours(-6);
            }
            insert opp;
            //List<Object> oliLineItems = (List<Object>)results.get('line_items');
            for( Object oli : oliLineItems){
                Map<String, Object> ol = (Map<String, Object>)oli;
                String product_id = String.valueOf(ol.get('product_id'));
                String oli_id = String.valueOf(ol.get('id'));
                String oli_name = (String)ol.get('name');
                String quantity = String.valueOf(ol.get('quantity'));
                String stripeplanid = prodIdPlanIdMap.get(product_id);
                Decimal total_amt = Decimal.valueOf((String)ol.get('total')) + Decimal.valueOf((String)ol.get('total_tax'));
                List<Product2> prod = [SELECT id,Stripe_Plan_Id__c FROM Product2 where Stripe_Plan_Id__c='burn' LIMIT 1];
                List<PriceBookEntry> priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name,Product2.Stripe_Plan_Id__c FROM PriceBookEntry WHERE PriceBook2.isStandard=true AND Product2.Stripe_Plan_Id__c!=null];
                Map<String,PriceBookEntry> priceBookMap = new Map<String,PriceBookEntry>();
                for(PriceBookEntry pbookEntry:priceBookList){
                    priceBookMap.put(pbookEntry.Product2.Stripe_Plan_Id__c.toLowerCase(),pbookEntry);
                }
                OpportunityLineItem opline = new OpportunityLineItem();
                opline.OpportunityId = opp.Id;
                opline.PricebookEntryId = priceBookMap.get(stripeplanid.toLowerCase()).Id;
                opline.Quantity = Integer.valueOf(quantity);
                opline.Product2Id = prod.get(0).Id;
                opline.TotalPrice = total_amt;
                opline.WP_Oli_Id__c	= oli_id;
                opline.WP_StripeplanId__c = stripeplanid;
                insert opline;
            }
            
        }
    }
    
    public static String getStripePlanId(String prodid,String siteName){
        String planId='';
        List<Woocommerce_Status_Update__c> siteList =  [SELECT consumer_key__c,consumer_secret__c,site_url__c FROM Woocommerce_Status_Update__c where name=:siteName];
        String site_url = siteList.get(0).site_url__c.replace('orders','products');
        String consumer_key = siteList.get(0).consumer_key__c;
        String consumer_secret = siteList.get(0).consumer_secret__c;
        HttpResponse response=null;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = site_url+'/'+prodid +'?consumer_key='+consumer_key
            +'&consumer_secret='+consumer_secret;
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json');
        if(Test.isRunningTest()){
            HttpResponse rep = new HttpResponse();
            rep.setBody('[{"id":4554,"status": "wcf-main-order"}]');
            rep.setStatusCode(200);
            response = rep;
        }else{
            response = http.send(request);
        }
        if(response.getStatusCode()==200){
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            List<Object> metas = (List<Object>)results.get('meta_data');
            for(Object mt : metas){
                Map<String, Object> meta = (Map<String, Object> )mt;
                if(String.valueOf(meta.get('key')) == 'stripeplanid'){
                    System.debug('plan id is : '+meta.get('value'));
                    planId = (String)meta.get('value');
                }
            }
        }
        return planId;
    }
}