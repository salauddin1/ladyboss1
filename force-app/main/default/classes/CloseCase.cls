global class CloseCase {
    WebService static void updateStatus(Id RecordID,String caseStatus,String caseReason) {    
        
        list<Case> cases = [select id,Status,Reason from Case where id =: RecordID];
        cases[0].Status = caseStatus;
        cases[0].Reason = caseReason;
        
        update cases;
    }
    
    @AuraEnabled
    public static Case updateCase(Id caseId){
        
        Case caseToUpdate = [Select id,Status, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId];
        if(caseToUpdate.Status == 'Reopened')  {
            caseToUpdate.Status = 'Closed-From-Reopened';
        }else {
            caseToUpdate.Status = 'Closed';
        }
        update caseToUpdate;
        
        return([Select id,Status, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId]);
    }   
    
    @AuraEnabled
    public static Case transferOwnership(Id caseId,String queueName){
        system.debug('heyyy'+queueName);
        try{
            Group gr = [SELECT CreatedDate,DeveloperName FROM Group where DeveloperName =: queueName];
        
        Case caseToUpdate = [Select id,Status,OwnerId, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId];
        caseToUpdate.OwnerId = gr.id;
        update caseToUpdate;
        
        return([Select id,Status, Contact.Name, ContactId, CreatedDate, LastModifiedDate From Case Where id=:caseId]);    
        }catch(Exception e){
            throw new AuraHandledException('No queue named '+queueName+' found '+ e.getMessage()); 
            //return null;
        }
        
    }   
}