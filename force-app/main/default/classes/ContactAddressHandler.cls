public class ContactAddressHandler {
    public static void addAddress(List<Contact> contactList) {
    List<Address__c> lstAdd = new List<Address__c>();
        if(contactList.size() > 0) {
            for (Contact conObj : contactList) {
            
            if((conObj.MailingCity !=null && conObj.MailingCity!= '' ) && (conObj.MailingCountry!=null && conObj.MailingCountry!= '') && (conObj.MailingState!=null && conObj.MailingState!= '') && (conObj.MailingStreet!=null && conObj.MailingStreet!= '' )&& (conObj.MailingPostalCode!=null && conObj.MailingPostalCode!= '')) {
                 Address__c addObj = new Address__c();
                 if(conObj.MailingCity !=null && conObj.MailingCity!= '') 
                addObj.Shipping_City__c=conObj.MailingCity;
               if(conObj.MailingCountry!=null && conObj.MailingCountry!= '')  
                addObj.Shipping_Country__c = conObj.MailingCountry;
                if(conObj.MailingState!=null && conObj.MailingState!= '')  
                addObj.Shipping_State_Province__c = conObj.MailingState;
                if(conObj.MailingStreet!=null && conObj.MailingStreet!= '') 
                addObj.Shipping_Street__c = conObj.MailingStreet;
                if(conObj.MailingPostalCode!=null && conObj.MailingPostalCode!= '')  
                addObj.Shipping_Zip_Postal_Code__c = conObj.MailingPostalCode;
                addObj.Contact__c = conObj.id;
                if((conObj.MailingCity !=null ) && (conObj.MailingCountry!=null) && (conObj.MailingState!=null )  && (conObj.MailingStreet!=null  ) && (conObj.MailingPostalCode!=null )) {
                     
                    addObj.Primary__c = true;
                }
                lstAdd.add(addObj);
            }
            
            }
            
            if(lstAdd.size () > 0){
                insert lstAdd;
            }
        
        }
    }

}