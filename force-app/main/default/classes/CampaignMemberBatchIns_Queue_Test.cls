@isTest
private class CampaignMemberBatchIns_Queue_Test {
  static testmethod void test1() {
        Lead ld1 = new Lead(Status = 'New', LastName = 'test1Name',Company = 'testing1company');
        Lead ld2 = new Lead(Status = 'New', LastName = 'test2Name',Company = 'testing2company');
        List<Lead> ldList = new List<Lead>{ld1,ld2};
            insert ldList;
        
        List<Campaign> cList = new List<Campaign>();
        Campaign c1 =new  Campaign();
        c1.Name = 'Campaign Buy Day 1';        
        cList.add(c1);
        Campaign c2 =new  Campaign();
        c2.Name = 'Campaign Lead Days 2-5';
        cList.add(c2);
        insert cList;
        
        Date dt = Date.today();
        Date d1 = dt.addDays(-5);
        CampaignMember cm1 = new CampaignMember(CampaignId = c1.Id,LeadId = ld1.Id , Created_Date__c = d1);
        
        Date d2 = dt.addDays(-6);
        CampaignMember cm2 = new CampaignMember(CampaignId = c2.Id,LeadId = ld2.Id , Created_Date__c = d2);    
        List<CampaignMember> cml= new List<CampaignMember>{cm1,cm2}; 
            
        
        Test.startTest();        
        System.enqueueJob(new CampaignMemberBatchInsQueue(cml));
        Test.stopTest();
        
    }
}