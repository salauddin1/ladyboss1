//@Developer - Ashish Sharma
//@Modified by - Sourabh Badole
//@Description - This job moves phone team campaign members to their next campaign as per product and age.
public class CampaignMembers_Batch implements Database.Batchable<sObject>{
    public String query;
    
    public CampaignMembers_Batch(){
        List<Campaign_Member_Batch_QueryLimit__c> camMemBatchQuery = new List<Campaign_Member_Batch_QueryLimit__c>([Select Id,Name,Limit_Size__c from Campaign_Member_Batch_QueryLimit__c limit 1]);
        System.debug('-----camMemBatchQuery[0].Limit_Size__c----'+camMemBatchQuery[0].Limit_Size__c);
        if(camMemBatchQuery[0].Limit_Size__c == null || camMemBatchQuery[0].Limit_Size__c == 0){
            query =  'SELECT Id,Name,CampaignId,Product__c,Campaign.Type,Marketing_Cloud_Tag__c,Contact.Product__c,Created_Date__c,Created_Date_Time__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Marketing_Cloud_Tags__c,Campaign.Name, Age__c, Age_In_Hours__c ,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = false and Campaign.Marketing_Cloud_Tags__c = null and Contact.Amazon_Customer__c=false and Campaign.Type=\'Phone Team\'';
        }
        else{
            String limitVal = String.valueOf(camMemBatchQuery[0].Limit_Size__c);
            System.debug('----limitVal-----'+limitVal);
       query =  'SELECT Id,Name,CampaignId,Product__c,Campaign.Type,Marketing_Cloud_Tag__c,Contact.Product__c,Created_Date__c,Created_Date_Time__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c,Campaign.Marketing_Cloud_Tags__c,Campaign.Name, Age__c, Age_In_Hours__c ,LeadId,ContactId FROM CampaignMember where Contact.Campaign_Movement_NOt_Allowed__c = false and Campaign.Marketing_Cloud_Tags__c = null and Contact.Amazon_Customer__c=false and Campaign.Type=\'Phone Team\' order by createdDate DESC limit '+limitVal;
        System.debug(query);
        }
        //query =  'SELECT Id,Name, CampaignId,Created_Date__c,Product__c,Campaign.Min_Age__c,Campaign.Max_Age__c,Campaign.Product__c, Campaign.Name, Age__c , LeadId, ContactId FROM CampaignMember WHERE LeadId = \'00Q2F000002lNbS\'';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('**** Batch Query String ---->'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<CampaignMember> scope){
        Map<String,String> campaignIdMap = new Map<String,String>();
        Map<String,String> idCampaignMap = new Map<String,String>();
        List<Campaign> campaignList= new List<Campaign>();
         campaignList   = [SELECT Id, Name, Product__c, Marketing_Cloud_Tags__c, Max_Age__c, Min_Age__c  FROM Campaign ];
        System.debug('**** Batch Query String - Test--->'+scope);
        System.debug('**** campaignList--->'+campaignList);
        Map<Id,List<Workflow_Configuration__c>> mapWorkflowConfig = new Map<Id,List<Workflow_Configuration__c>>();
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign c:campaignList){
            campaignIds.add(c.Id);
        }
        
        for(Workflow_Configuration__c  wc:[select id,Source_Campaign__c ,Target_Campaign__c,Products__c,Special_Movement__c from Workflow_Configuration__c where Source_Campaign__c in:campaignIds]){
            if(mapWorkflowConfig.containskey(wc.Source_Campaign__c)){
                mapWorkflowConfig.get(wc.Source_Campaign__c).add(wc);
            }else{
                mapWorkflowConfig.put(wc.Source_Campaign__c,new List<Workflow_Configuration__c>{wc});
            }
        }
        
        System.debug('*****  mapWorkflowConfig     *****'+mapWorkflowConfig );
        
        Map<Id,List<CampaignMember>> conLeadCampMemberMap = new  Map<Id,List<CampaignMember>>();
        Set<Id> contactIds = new Set<Id>();
        for(CampaignMember ca : scope){
            contactIds.add(ca.ContactId);
            if(ca.Campaign.Name.Contains('Day') && ca.Campaign.Name.Contains('Lead') && ca.Campaign.Type == 'Phone Team'){
                
                system.debug('*****  CampaignMember-Test   *****'+ca);
                // for duplecad lead data in contact campaign member
                if(conLeadCampMemberMap != null && conLeadCampMemberMap.containsKey(ca.ContactId)){
                    conLeadCampMemberMap.get(ca.ContactId).add(ca);
                }else{
                    conLeadCampMemberMap.put(ca.ContactId,new List<CampaignMember>{ca});
                }
            }
            
        }
        System.debug('*****  conLeadCampMemberMap-Test     *****'+conLeadCampMemberMap );
        
        Map<Id,List<CampaignMember>> conCampMemberMap = new  Map<Id,List<CampaignMember>>();
        for(Campaignmember cm:[select id,contactId,Age_In_Hours__c,CampaignId,Campaign.Name,Campaign.Type,Campaign.Product__c from campaignmember where contactId in:contactIds]){
            if(conCampMemberMap.containskey(cm.contactId)){
                conCampMemberMap.get(cm.ContactId).add(cm);   
            }else{
                conCampMemberMap.put(cm.ContactId,new List<CampaignMember>{cm});
            }
        }
        
        
        Set<CampaignMember> cmDeleteSet = new Set<CampaignMember> ();
        List<CampaignMember> cmDeleteList = new List<CampaignMember> ();
        List<CampaignMember> cmMovementList = new  List<CampaignMember>();
        List<CampaignMember> cmDelPhoneTeamList = PhoneCampaignClean.cleanPipeline(scope,conCampMemberMap);
        system.debug('*****  cmDelPhoneTeamList   *****'+cmDelPhoneTeamList);
        //List<CampaignMember> cmDelPriorityList = PhoneCampaignClean.cleanPriority(scope,contactIds,conCampMemberMap);
        //system.debug('*****  cmDelPriorityList   *****'+cmDelPriorityList);
        List<CampaignMember> cmDelAgeHrsList = PhoneCampaignClean.cleanAgeHrs(scope,contactIds,conCampMemberMap);
        List<CampaignMember> coachCampList = PhoneCampaignClean.cleanCoachingCampaign(scope,contactIds,conCampMemberMap);
        system.debug('*****  cmDelAgeHrsList   *****'+cmDelAgeHrsList);
         if(cmDelPhoneTeamList != null && !cmDelPhoneTeamList.isEmpty()){
        cmDeleteSet.addAll(cmDelPhoneTeamList);
             
         }
         if(coachCampList != null && !coachCampList.isEmpty()){
             cmDeleteSet.addAll(coachCampList);
         }
        if(cmDelAgeHrsList != null && !cmDelAgeHrsList.isEmpty()){
             cmDeleteSet.addAll(cmDelAgeHrsList);
         }
         if(cmDeleteSet != null && !cmDeleteSet.isEmpty()){
             cmDeleteList.addAll(cmDeleteSet);
         }
        
        system.debug('*****  cmDeleteList   *****'+cmDeleteList);
        Map<Id,CampaignMember> campMap = new Map<Id,CampaignMember>();
        if(cmDeleteList != null && !cmDeleteList.isEmpty()){
            for(CampaignMember cMember : cmDeleteList){
                campMap.put(cMember.Id,cMember); 
                System.debug('***** ContactID *****'+cMember.ContactId+'***** CampaignMemberDeleteID *****'+cMember.Id);
            }
        }
        for(CampaignMember ca : scope){
            if(!campMap.containsKey(ca.Id)){
                cmMovementList.add(ca); 
                System.debug('***** ContactID *****'+ca.ContactId+'*****  cmMovementID   *****'+ca.Id);
            }
        }
        System.debug('*****  cmMovementList   *****'+cmMovementList);
        
        CampaignMembers_Batch_Handler.updateCampaignMembers(campaignList,cmMovementList ,mapWorkflowConfig,conLeadCampMemberMap,cmDeleteList);
    }
    
    public void finish(Database.BatchableContext BC){
        //database.executeBatch(new MCCampaignMemberDelBatch());
    }
    
}