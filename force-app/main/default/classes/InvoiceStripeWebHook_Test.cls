@isTest
public class InvoiceStripeWebHook_Test {
    @isTest static void test(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BcOxelyfXsr0Ki');
        insert acc;
        
        //Opportunity opp = new Opportunity(Name='Test opp',CloseDate=system.today(),StageName='Qualification',External_ID__c='inv_123456789');
        //insert opp;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/InvoiceStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        //String body = '{ \"id\": \"evt_1BGxKyDiFnu7hVq7Fa0Ui798\", \"object_data\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508970420, \"data\": { \"object_data\": { \"id\": \"in_1BGvc2DiFnu7hVq7DNLFCBWE\", \"object_data\": \"invoice\", \"amount_due\": 2750, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": false, \"billing\": \"send_data_invoice\", \"charge\": null, \"closed\": false, \"currency_data\": \"usd\", \"customer\": \"cus_BUPh8oOgx5CXG2\", \"date\": 1508963789, \"description\": \"TEST H\", \"discount\": null, \"due_date\": 1511555789, \"end_dataing_balance\": 0, \"forgiven\": false, \"lines\": { \"object_data\": \"list\", \"data\": [ { \"id\": \"sub_BeE40s8qr26QUX\", \"object_data\": \"line_item\", \"amount\": 2500, \"currency_data\": \"usd\", \"description\": null, \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1508963789, \"end_data\": 1511642189 }, \"plan\": { \"id\": \"test1\", \"object_data\": \"plan\", \"amount\": 2500, \"created\": 1508947075, \"currency_data\": \"usd\", \"interval\": \"month\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"name\": \"Test Product 1\", \"statement_descriptor\": null, \"trial_period_days\": null }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_1BGvc1DiFnu7hVq7RbuZfvdv\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1BGvc2DiFnu7hVq7DNLFCBWE/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"c097b8d110-0001\", \"paid\": false, \"period_end_data\": 1508963789, \"period_start\": 1508963789, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_BeE40s8qr26QUX\", \"subtotal\": 2500, \"tax\": 250, \"tax_percent\": 10.0, \"total\": 2750, \"webhooks_delivered_at\": 1508963790 }, \"previous_attributes\": { \"description\": null } }, \"livemode\": false, \"pend_dataing_webhooks\": 1, \"request\": { \"id\": \"req_5a0jhAQhqcE4gh\", \"idempotency_key\": null }, \"type\": \"invoice.updated\" }';
        String body = '{'+
                      '\"created\": 1326853478,'+
                      '\"livemode\": false,'+
                      '\"id\": \"evt_00000000000000\",'+
                      '\"type\": \"invoice.created\",'+
                      '\"object\": \"event\",'+
                      '\"request\": null,'+
                      '\"pending_webhooks\": 1,'+
                      '\"api_version\": \"2017-08-15\",'+
                      '\"data\": {'+
                        '\"object\": {'+
                          '\"id\": \"in_00000000000000\",'+
                          '\"object\": \"invoice\",'+
                          '\"amount_due\": 2500,'+
                          '\"application_fee\": null,'+
                          '\"attempt_count\": 0,'+
                          '\"attempted\": false,'+
                          '\"billing\": \"send_invoice\",'+
                          '\"charge\": null,'+
                          '\"closed\": false,'+
                          '\"currency\": \"usd\",'+
                          '\"customer\": \"cus_00000000000000\",'+
                          '\"date\": 1508953433,'+
                          '\"description\": null,'+
                          '\"discount\": {'+
            			  		'\"object\": \"discount\",'+
            					'\"coupon\": {'+
            						'"amount_off": 10'+
            					'},'+
            					'\"customer\": \"cus_123456789\"'+
            			  '},'+
                          '\"due_date\": 1511545433,'+
                          '\"ending_balance\": 0,'+
                          '\"forgiven\": false,'+
                          '\"lines\": {'+
                            '\"data\": ['+
                              '{'+
                                '\"id\": \"sub_BeBHpau6kqb0To\",'+
                                '\"object\": \"line_item\",'+
                                '\"amount\": 2500,'+
                                '\"currency\": \"usd\",'+
                                '\"description\": null,'+
                                '\"discountable\": true,'+
                                '\"livemode\": true,'+
                                '\"metadata\": null,'+
                                '\"period\": {'+
                                  '\"start\": 1516902233,'+
                                  '\"end\": 1519580633'+
                                '},'+
                                '\"plan\": {'+
                                  '\"id\": \"test1\",'+
                                  '\"object\": \"plan\",'+
                                  '\"amount\": 2500,'+
                                  '\"created\": 1508947075,'+
                                  '\"currency\": \"usd\",'+
                                  '\"interval\": \"month\",'+
                                  '\"interval_count\": 1,'+
                                  '\"livemode\": false,'+
                                  '\"metadata\": null,'+
                                  '\"name\": \"Test Product 1\",'+
                                  '\"statement_descriptor\": null,'+
                                  '\"trial_period_days\": null'+
                                '},'+
                                '\"proration\": false,'+
                                '\"quantity\": 1,'+
                                '\"subscription\": null,'+
                                '\"subscription_item\": \"si_1BGsuzDiFnu7hVq7Qn6AS1Jo\",'+
                                '\"type\": \"subscription\"'+
                              '}'+
                            '],'+
                            '\"has_more\": false,'+
                            '\"object\": \"list\",'+
                            '\"url\": \"/v1/invoices/in_1BGsuzDiFnu7hVq7rShEd3dr/lines\"'+
                          '},'+
                          '\"livemode\": false,'+
                          '\"metadata\": null,'+
                          '\"next_payment_attempt\": null,'+
                          '\"number\": \"ccbdeec3db-0001\",'+
                          '\"paid\": true,'+
                          '\"period_end\": 1508953433,'+
                          '\"period_start\": 1508953433,'+
                          '\"receipt_number\": null,'+
                          '\"starting_balance\": 0,'+
                          '\"statement_descriptor\": null,'+
                          '\"subscription\": \"sub_00000000000000\",'+
                          '\"subtotal\": 2500,'+
                          '\"tax\": null,'+
                          '\"tax_percent\": null,'+
                          '\"total\": 2500,'+
                          '\"webhooks_delivered_at\": 1508953433'+
                        '}'+
                      '}'+
                    '}';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	InvoiceStripeWebHook.InvoiceStripe();
        Test.stopTest();
    }
    
    @isTest static void test2(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BcOxelyfXsr0Ki');
        insert acc;
        
        //Opportunity opp = new Opportunity(Name='Test opp',CloseDate=system.today(),StageName='Qualification',External_ID__c='inv_123456789');
        //insert opp;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/InvoiceStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        //String body = '{ \"id\": \"evt_1BGxKyDiFnu7hVq7Fa0Ui798\", \"object_data\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1508970420, \"data\": { \"object_data\": { \"id\": \"in_1BGvc2DiFnu7hVq7DNLFCBWE\", \"object_data\": \"invoice\", \"amount_due\": 2750, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": false, \"billing\": \"send_data_invoice\", \"charge\": null, \"closed\": false, \"currency_data\": \"usd\", \"customer\": \"cus_BUPh8oOgx5CXG2\", \"date\": 1508963789, \"description\": \"TEST H\", \"discount\": null, \"due_date\": 1511555789, \"end_dataing_balance\": 0, \"forgiven\": false, \"lines\": { \"object_data\": \"list\", \"data\": [ { \"id\": \"sub_BeE40s8qr26QUX\", \"object_data\": \"line_item\", \"amount\": 2500, \"currency_data\": \"usd\", \"description\": null, \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1508963789, \"end_data\": 1511642189 }, \"plan\": { \"id\": \"test1\", \"object_data\": \"plan\", \"amount\": 2500, \"created\": 1508947075, \"currency_data\": \"usd\", \"interval\": \"month\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"name\": \"Test Product 1\", \"statement_descriptor\": null, \"trial_period_days\": null }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_1BGvc1DiFnu7hVq7RbuZfvdv\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1BGvc2DiFnu7hVq7DNLFCBWE/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"c097b8d110-0001\", \"paid\": false, \"period_end_data\": 1508963789, \"period_start\": 1508963789, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_BeE40s8qr26QUX\", \"subtotal\": 2500, \"tax\": 250, \"tax_percent\": 10.0, \"total\": 2750, \"webhooks_delivered_at\": 1508963790 }, \"previous_attributes\": { \"description\": null } }, \"livemode\": false, \"pend_dataing_webhooks\": 1, \"request\": { \"id\": \"req_5a0jhAQhqcE4gh\", \"idempotency_key\": null }, \"type\": \"invoice.updated\" }';
        String body = '{'+
                      '\"created\": 1326853478,'+
                      '\"livemode\": false,'+
                      '\"id\": \"evt_00000000000000\",'+
                      '\"type\": \"invoice.created\",'+
                      '\"object\": \"event\",'+
                      '\"request\": null,'+
                      '\"pending_webhooks\": 1,'+
                      '\"api_version\": \"2017-08-15\",'+
                      '\"data\": {'+
                        '\"object\": {'+
                          '\"id\": \"in_00000000000000\",'+
                          '\"object\": \"invoice\",'+
                          '\"amount_due\": 2500,'+
                          '\"application_fee\": null,'+
                          '\"attempt_count\": 0,'+
                          '\"attempted\": false,'+
                          '\"billing\": \"send_invoice\",'+
                          '\"charge\": null,'+
                          '\"closed\": false,'+
                          '\"currency\": \"usd\",'+
                          '\"customer\": \"cus_00000000000000\",'+
                          '\"date\": 1508953433,'+
                          '\"description\": null,'+
                          '\"discount\": {'+
            			  		'\"object\": \"discount\",'+
            					'\"coupon\": {'+
            						'"percent_off": 10'+
            					'},'+
            					'\"customer\": \"cus_123456789\"'+
            			  '},'+
                          '\"due_date\": 1511545433,'+
                          '\"ending_balance\": 0,'+
                          '\"forgiven\": false,'+
                          '\"lines\": {'+
                            '\"data\": ['+
                              '{'+
                                '\"id\": \"sub_BeBHpau6kqb0To\",'+
                                '\"object\": \"line_item\",'+
                                '\"amount\": 2500,'+
                                '\"currency\": \"usd\",'+
                                '\"description\": null,'+
                                '\"discountable\": true,'+
                                '\"livemode\": true,'+
                                '\"metadata\": null,'+
                                '\"period\": {'+
                                  '\"start\": 1516902233,'+
                                  '\"end\": 1519580633'+
                                '},'+
                                '\"plan\": {'+
                                  '\"id\": \"test1\",'+
                                  '\"object\": \"plan\",'+
                                  '\"amount\": 2500,'+
                                  '\"created\": 1508947075,'+
                                  '\"currency\": \"usd\",'+
                                  '\"interval\": \"month\",'+
                                  '\"interval_count\": 1,'+
                                  '\"livemode\": false,'+
                                  '\"metadata\": null,'+
                                  '\"name\": \"Test Product 1\",'+
                                  '\"statement_descriptor\": null,'+
                                  '\"trial_period_days\": null'+
                                '},'+
                                '\"proration\": false,'+
                                '\"quantity\": 1,'+
                                '\"subscription\": null,'+
                                '\"subscription_item\": \"si_1BGsuzDiFnu7hVq7Qn6AS1Jo\",'+
                                '\"type\": \"subscription\"'+
                              '}'+
                            '],'+
                            '\"has_more\": false,'+
                            '\"object\": \"list\",'+
                            '\"url\": \"/v1/invoices/in_1BGsuzDiFnu7hVq7rShEd3dr/lines\"'+
                          '},'+
                          '\"livemode\": false,'+
                          '\"metadata\": null,'+
                          '\"next_payment_attempt\": null,'+
                          '\"number\": \"ccbdeec3db-0001\",'+
                          '\"paid\": true,'+
                          '\"period_end\": 1508953433,'+
                          '\"period_start\": 1508953433,'+
                          '\"receipt_number\": null,'+
                          '\"starting_balance\": 0,'+
                          '\"statement_descriptor\": null,'+
                          '\"subscription\": \"sub_00000000000000\",'+
                          '\"subtotal\": 2500,'+
                          '\"tax\": null,'+
                          '\"tax_percent\": null,'+
                          '\"total\": 2500,'+
                          '\"webhooks_delivered_at\": 1508953433'+
                        '}'+
                      '}'+
                    '}';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	InvoiceStripeWebHook.InvoiceStripe();
        Test.stopTest();
    }
}