@RestResource(urlMapping='/Stripe_ChargeWebhookService')
global class StripeChargeWebhook_Service {
    @HttpPost
    global static void stripeChargeWebhook() {
        string ChargeId;
        if(RestContext.request.requestBody!=null){
            try {
                String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
                Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'Stripe_ChargeWebhookService');
                System.debug('-------timestamppp----'+timestamppp);
                System.debug('-------sigFromHmac----'+sigFromHmac);
                             
               if(sigFromHmac && sigFromHmac == true){
               
             
                
                    String str = RestContext.request.requestBody.toString();
                    
                    Map<String, Object> results =  (Map<String, Object>)JSON.deserializeUntyped(str);
                    
                    
                    if(String.valueOf(results.get('type')).equals('charge.refunded')){
                        ChargeId=String.valueOf(results.get('Id'));
                        System.debug('charge.update-->'+str);
                        StripeChargeUpdate_Service.createCustomer();
                    }else if(String.valueOf(results.get('type')).equals('charge.succeeded')){
                      ChargeId=String.valueOf(results.get('Id'));
                        System.debug('charge.succeeded-->'+str);
                        StripeCharge_Service.createCustomer();
                        
                    }else if(String.valueOf(results.get('type')).equals('charge.failed')){
                        ChargeId=String.valueOf(results.get('Id'));
                        System.debug('charge.Failed-->'+str);
                        StripeFailedCharge_Service.upsertCase(); 
                    }
                }
                
                 ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                 'StripeChargeUpdate_Service',
                 'createCustomer',
                 ChargeId
                 )
                 ); 
                if(Test.isRunningTest())  
           integer intTest =1/0; 
            }catch(Exception e){
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                  ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeChargeWebhook_Service',
                        'stripeChargeWebhook',
                        ChargeId,
                        e
                    )
                );
            }
        }
    }
}