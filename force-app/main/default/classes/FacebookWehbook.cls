@RestResource(urlMapping='/FacebookWehbook/*')
global class FacebookWehbook  {

    @HttpGet
    global static Integer show() {
        RestRequest request = RestContext.request;
        return Integer.valueOf(request.params.get('hub.challenge'));
    }

    @HttpPost
    global static String create() {
        RestRequest request = RestContext.request;
        try{
            if(request.requestBody.toString()!=null){
                String requestBody = request.requestBody.toString().replaceAll('"object"','"objectData"');
                //System.debug(requestBody);
                Map<String,Object> wehbookObject = (Map<String,Object>)JSON.deserializeUntyped(requestBody);
                if((String)wehbookObject.get('objectData') == 'page'){
                    List<String> pageIdList = new List<String>();
                    Map<String,List<String>> userIdMap = new Map<String,List<String>>();
                    List<String> contactIdList = new List<String>();
                    List<String> conversationIdList = new List<String>();
                    for(Object entryObject : (List<Object>)wehbookObject.get('entry')){
                        Map<String,Object> entryObjectMap = (Map<String,Object>)entryObject;
                        if(entryObjectMap.get('messaging')!=null){
                            for(Object messageObjectMapObject : (List<Object>)entryObjectMap.get('messaging')){
                                Map<String,Object> messageObjectMap = (Map<String,Object>)messageObjectMapObject;
                                //FacebookWrappers.ReciepientDetails recipient = (FacebookWrappers.ReciepientDetails)JSON.deserialize((String)messageObjectMap.get('recipient'),FacebookWrappers.ReciepientDetails.class);
                                //FacebookWrappers.ReciepientDetails sender = (FacebookWrappers.ReciepientDetails)JSON.deserialize((String)messageObjectMap.get('sender'),FacebookWrappers.ReciepientDetails.class);
                                FacebookWrappers.WehbookMessageWrapper messageObject = (FacebookWrappers.WehbookMessageWrapper)JSON.deserialize(JSON.serialize(messageObjectMap),FacebookWrappers.WehbookMessageWrapper.class);                            
                                pageIdList.add(messageObject.recipient.id);
                                if(!userIdMap.containsKey(messageObject.recipient.id)){
                                    userIdMap.put(messageObject.recipient.id,new List<String>());
                                }
                                userIdMap.get(messageObject.recipient.id).add(messageObject.sender.id);
                                contactIdList.add(messageObject.sender.id);
                                conversationIdList.add(messageObject.recipient.id+messageObject.sender.id);
                            }
                        }
                    }

                    List<FacebookPage__c> facebookPagesList = [SELECT Id,isActive__c,Last_Fetch_Time__c,LongLive_User_Token__c,Page_Id__c,Page_Name__c,Page_Token__c,username__c FROM FacebookPage__c Where Page_Id__c IN :pageIdList];
                    Map<String,FacebookPage__c> conversationPageMap = new Map<String,FacebookPage__c>();
                    for(FacebookPage__c fb : facebookPagesList){
                        conversationPageMap.put(fb.Page_Id__c,fb);
                    }    
                    List<Contact> contactList = [SELECT id,Fb_User_Id__c,lastname from contact WHERE Fb_User_Id__c IN :contactIdList];
                    Map<String,Contact> contactMap = new Map<String,Contact>();
                    for(Contact contactObject : contactList){
                        contactMap.put(contactObject.Fb_User_Id__c,contactObject);
                    }
                    for(String key : userIdMap.keySet()){
                        for(String userId : userIdMap.get(key)){
                            if(contactMap.get(userId) == null){
                                Map<String,String> paramMap = new Map<String,String>();
                                paramMap.put('access_token',conversationPageMap.get(key).Page_Token__c);
                                paramMap.put('fields','first_name,last_name');
                                List<FaceBookMessagesAPI.URLParameters> paramList = FaceBookMessagesAPI.convertToListParam(paramMap);
                                String response = FaceBookMessagesAPI.callAPI('/'+userId,'GET',paramList);
                                System.debug(response);
                                FacebookWrappers.User userObject = (FacebookWrappers.User)JSON.deserialize(response,FacebookWrappers.User.class);
                                Contact contactObject = new Contact();
                                contactObject.FirstName = userObject.first_name;
                                contactObject.LastName = userObject.last_name;
                                contactObject.Fb_User_Id__c = userObject.id;
                                contactMap.put(contactObject.Fb_User_Id__c,contactObject);
                                System.debug(contactObject);
                            }
                        }
                    }
                    if(contactMap.keyset().size()>0){
                        upsert contactMap.values() Fb_User_Id__c;
                    }
                    List<Case> caseFromDBList = [SELECT id,ClosedDate,Conversation_Id__c,status,Conversation_UpdateTime__c FROM CASE WHERE Conversation_Id__c IN :conversationIdList];
                    Map<String,List<Case>> caseFromDbMap = new Map<String,List<Case>>();
                    Map<Id,Case> idCaseMap = new Map<Id,Case>();
                    for(Case caseObject : caseFromDBList){
                        if(!caseFromDbMap.containsKey(caseObject.Conversation_Id__c)){
                            caseFromDbMap.put(caseObject.Conversation_Id__c,new List<Case>());
                        }
                        caseFromDbMap.get(caseObject.Conversation_Id__c).add(caseObject);
                        idCaseMap.put(caseObject.id,caseObject);
                    }
                    Datetime currentDateTime = Datetime.now().addDays(-7);
                    Map<String,Case> newConversationIdCaseMap = new Map<String,Case>();
                    Map<String,FacebookWrappers.WehbookMessageWrapper> messageMap = new Map<String,FacebookWrappers.WehbookMessageWrapper>();
                    
                    for(Object entryObject : (List<Object>)wehbookObject.get('entry')){
                        Map<String,Object> entryObjectMap = (Map<String,Object>)entryObject;
                        System.debug(entryObjectMap);
                        if(entryObjectMap.get('messaging')!=null){
                            List<Case> caseList = new List<Case>();
                            for(Object messageObjectMapObject : (List<Object>)entryObjectMap.get('messaging')){
                                Map<String,Object> messageObjectMap = (Map<String,Object>)messageObjectMapObject;
                                FacebookWrappers.WehbookMessageWrapper messageObject = (FacebookWrappers.WehbookMessageWrapper)JSON.deserialize(JSON.serialize(messageObjectMap),FacebookWrappers.WehbookMessageWrapper.class);
                                //return JSON.serialize(messageObject);
                                Boolean isOpen = false;
                                Boolean isReOpen = false;
                                Case openId = null;
                                Case reOpenId = null;
                                String conversationId = messageObject.recipient.id+messageObject.sender.id;
                                messageMap.put(conversationId,messageObject);
                                if(caseFromDbMap.containsKey(conversationId)){
                                    List<Case> saperateCaseList = caseFromDbMap.get(conversationId);
                                    for(Case caseObject : saperateCaseList){
                                        if(caseObject.status != 'closed'){
                                            isOpen = true;
                                            openId = caseObject;
                                        }else if(caseObject.ClosedDate>=currentDateTime){
                                            reOpenId = caseObject;
                                            isReOpen = true;
                                        }
                                    }
                                }

                                if(!isOpen && !isReOpen){
                                    Case caseObj = new Case();
                                    caseObj.Conversation_Id__c = conversationId;
                                    //caseObj.Conversation_UpdateTime__c = DateTime.valueOf(conversationMap.get(conversationId).updated_time.replace('T',' ').substring(0,conversationMap.get(conversationId).updated_time.indexOf('+')));
                                    caseObj.status = 'Open';
                                    String fromStr = contactMap.get(messageObject.sender.id).lastname;
                                    caseObj.subject = 'Inbox Message from: '+fromStr+' to: '+conversationPageMap.get(messageObject.recipient.id).Page_Name__c;
                                    caseObj.origin = 'facebook';
                                    caseObj.contact = new Contact(Fb_User_Id__c=messageObject.sender.id);
                                    caseObj.FB_Page__c = conversationPageMap.get(messageObject.recipient.id).id;
                                    newConversationIdCaseMap.put(conversationId,caseObj);
                                }else if(isOpen){
                                    newConversationIdCaseMap.put(conversationId,openid);
                                }else if(isReOpen){
                                    newConversationIdCaseMap.put(conversationId,reOpenid);
                                    newConversationIdCaseMap.get(conversationId).status = 'reopened';
                                }
                            }
                        }
                    }
                    if(newConversationIdCaseMap.values().size()>0){
                        upsert newConversationIdCaseMap.values();
                    }

                    List<CaseComment> caseCommentToInsertList = new List<CaseComment>();                
                    List<ConversationAndMessage__c> convMessageToInsertList = new List<ConversationAndMessage__c>();                
                    for(String conversationId : messageMap.keyset()){
                        FacebookWrappers.WehbookMessageWrapper messageObject = messageMap.get(conversationId);
                        CaseComment caseCommentObject = new CaseComment();
                        caseCommentObject.ParentId = newConversationIdCaseMap.get(conversationId).id;
                        caseCommentObject.CommentBody = contactMap.get(messageObject.sender.id).lastName +' ('+DateTime.newInstance(Long.valueOf(messageObject.timestamp)) +') : '+ messageObject.message.text;
                        if(messageObject.message.attachments!=null && messageObject.message.attachments.size()>0){
                            caseCommentObject.CommentBody+=' '+messageObject.message.attachments.get(0).payload.url;
                        }
                        caseCommentToInsertList.add(caseCommentObject);
                        ConversationAndMessage__c conv = new ConversationAndMessage__c();
                        conv.Conversation_Id__c = conversationId;
                        conv.Message_Id__c = messageObject.message.mid;
                        convMessageToInsertList.add(conv);
                    }

                    if(caseCommentToInsertList.size()>0){
                        CaseCommmentStaticFlagClass.flag = false;
                        upsert caseCommentToInsertList;
                        insert convMessageToInsertList;
                    }

                }
            }
        }catch(Exception e){
            System.debug('Exception is : ' + e.getMessage());
			Log__c logObj = new Log__c();
			logobj.API_Url__c = request.requestURI;
			logobj.Response__c = request.requestBody.toString();
			logObj.Log_Type__c = 'Error';
			logobj.Exception_Message__c = 'Exception is :' + e;
			insert logObj;
        }
        return null;

    }

}