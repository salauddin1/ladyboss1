public class OpportunityAfterUpdateTriggerHandler {
	public static void insertComission(id userId, id paymentId, Decimal amount,Decimal weeks,List<Comission__c> comList,Comission_Configuration__c comcfg,String hierarchy,Decimal com_percent){
        Comission__c comAgent = new Comission__c();
        comAgent.User__c = userId;
        comAgent.Amount__c = amount;
        comAgent.Payment__c = paymentId;
        comAgent.weeks__c = weeks;
        comAgent.hierarchy__c = hierarchy;
        comAgent.commission_percent__c = com_percent;
        comAgent.Comission_Configuration__c = comcfg.id;
        comList.add(comAgent);
    }
    
    public static Map<Decimal,Comission_Configuration__c> getTotalCommissionConfiguration(){
        Map<Decimal,Comission_Configuration__c> comiconfig = new Map<Decimal,Comission_Configuration__c>();
        String dQuery = 'SELECT Max_Value__c,Min_Value__c,'+
            'Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,'+
            'Percentage_Paid_to_Vice_President__c,Percentage_Paid_to_Senior_Agent__c '+
            'FROM Comission_Configuration__c where product__c=null AND For_First_Sell__c=true order by Max_Value__c';
        List < Comission_Configuration__c > lstOfRecords = Database.query(dQuery);
        
        for(Comission_Configuration__c cfg : lstOfRecords){
                comiconfig.put(cfg.Max_Value__c,cfg);
        }
        return comiconfig;     
    }
    public static Comission_Configuration__c getCommissionConfiguration(Decimal amount, Map<Decimal,Comission_Configuration__c> maxValAndComMap){
        for(Decimal amt : maxValAndComMap.keyset()){
            if(amount <= amt){
                return maxValAndComMap.get(amt);
            }else{
                continue;
            }
        }
        return null;
    }
    public static Map<id,String> twoWeekTotal(Set<Id> agentIdSet,Decimal Week,set<id> comConfigProdList){
        Integer  currentWeekNum = Integer.valueOf(Week);
        System.debug('currentWeekNum : '+currentWeekNum);
        List<Integer> weekList = PayToCommTriggerHelper.getWeekList(currentWeekNum);
        System.debug('weekList : '+weekList);
        List<AggregateResult> opList = [SELECT SUM(Total_Commissionable_amount__c)total,Opportunity.Sales_Person_Id__c,SUM(current_commissionable_amount__c)ref FROM OpportunityLineItem  where Opportunity.Sales_Person_Id__c IN : agentIdSet AND Opportunity.week__c IN : weekList AND Product2Id not in : comConfigProdList group by Opportunity.Sales_Person_Id__c];
        System.debug('opList '+opList);
        Map<id,String> userValueMap = new Map<id,String>();
        for(AggregateResult aggResult : opList){
            Decimal totalAmount = Decimal.valueOf(''+aggResult.get('total')) - Decimal.valueOf(''+aggResult.get('ref'));
            userValueMap.put(String.valueOf(aggResult.get('Sales_Person_Id__c')),String.valueOf(totalAmount));            
        }
        system.debug('---userValueMap--'+userValueMap);
        for( Id user : agentIdSet){
            if(!userValueMap.keyset().contains(user)){
                userValueMap.put(user,String.valueOf(0));
            }
        }
        if(Test.isRunningTest()){
            userValueMap.put(UserInfo.getUserId(),String.valueOf(200));
            System.debug('Intest : userValueMap : '+userValueMap);
            return userValueMap;
        }
        return userValueMap;
    }
}