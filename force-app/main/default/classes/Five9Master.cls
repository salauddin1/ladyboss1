public class Five9Master{
    
    public static void addtoMasterList(List<Contact> newCon,Map<Id,Contact> newMap,Map<Id,Contact> oldMap) {  
        if(!Test.isRunningTest()){
            Contact con1 = new Contact();
            con1 = [Select Id,Product__c,Potential_Buyer__c,Product_Add_Date__c from Contact where id =: newCon[0].Id LIMIT 1 FOR UPDATE];
            Set<id> conIdSet= new Set<id>();
            boolean booleanVal = false;
            String myLabel = System.Label.ProductValue;
            System.debug('============'+myLabel);
            String []splitVal = myLabel.split(' ');
            System.debug('=====booleanVal========='+booleanVal);
            for(Contact con : newCon) {
                
                if( newMap.get(con.Id).Product__c == null && newMap.get(con.Id).Potential_Buyer__c != null && newMap.get(con.Id).Potential_Buyer__c != oldMap.get(con.Id).Potential_Buyer__c){
                    if(oldMap.get(con.Id).Product_Add_Date__c != null ){  
                        if(oldMap.get(con.Id).Potential_Buyer__c == 'BookLead-1 Day' && (newMap.get(con.Id).Potential_Buyer__c == 'TrialLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UnlimLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'SuplmtLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break;
                        }
                        else if(oldMap.get(con.Id).Potential_Buyer__c == 'TrialLead-1 Day' && (newMap.get(con.Id).Potential_Buyer__c == 'UnlimLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'SuplmtLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break;
                        }
                        else if(oldMap.get(con.Id).Potential_Buyer__c == 'UnlimLead-1 Day' && (newMap.get(con.Id).Potential_Buyer__c == 'SuplmtLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break;
                        }
                        else if(oldMap.get(con.Id).Potential_Buyer__c == 'SuplmtLead-1 Day' && (newMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break;
                        }
                        else if(oldMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' && (newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break;
                        }
                        else if(oldMap.get(con.Id).Potential_Buyer__c == null && (newMap.get(con.Id).Potential_Buyer__c == 'BookLead-1 Day' ||newMap.get(con.Id).Potential_Buyer__c == 'TrialLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UnlimLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'SuplmtLead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'UTALead-1 Day' || newMap.get(con.Id).Potential_Buyer__c == 'CoachLead-1 Day' )){
                            con.Product_Add_Date__c = System.Now();
                            break; 
                        }
                    }
                    else{
                        con.Product_Add_Date__c = System.Now();
                        break; 
                    }
                }
                if(con.Product__c!=null && newMap.get(con.Id).Product__c!=oldMap.get(con.Id).Product__c){  
                    if(oldMap.get(con.Id).Product_Add_Date__c != null ){  
                        String oldVal = oldMap.get(con.Id).Product__c;
                        String newVal = newMap.get(con.Id).Product__c;
                        List<String> NewValAr = (newVal.contains(';'))?newVal.split(';'):new List<String>{newVal};
                        List<String> oldValAr = (oldVal!=null && oldVal.contains(';'))?oldVal.split(';'):new List<String>{oldVal};
                        String val = '';
                        for(String ab : oldValAr){
                            if(ab != null){
                                val = ab;
                            }
                        }
                        for(Integer i= 0; i < NewValAr.size(); i++ ){
                            if(NewValAr.size()==1){
                                con.Product_Add_Date__c = System.Now(); 
                            }
                            else{
                                System.debug('=====oldValAr=========='+val);
                                if(val == 'Book' && (NewValAr[i] == 'Trial' || NewValAr[i] == 'Unlimited' || NewValAr[i] == 'Supplement' || NewValAr[i] == 'UTA' || NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if(val == 'Trial' &&(NewValAr[i] == 'Unlimited' || NewValAr[i] == 'Supplement' || NewValAr[i] == 'UTA' || NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if(val == 'Unlimited' &&( NewValAr[i] == 'Supplement' || NewValAr[i] == 'UTA' || NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if(val == 'Supplement' &&( NewValAr[i] == 'UTA' || NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if(val == 'UTA' &&(NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if(val == 'Coaching' &&(NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                                else if((val == null || val == '') && (NewValAr[i] == 'Book'||NewValAr[i] == 'Trial' || NewValAr[i] == 'Unlimited' || NewValAr[i] == 'Supplement' || NewValAr[i] == 'UTA' || NewValAr[i] == 'Coaching' ||NewValAr[i] == 'LadyBoss UnlimitedLVC' )){
                                    con.Product_Add_Date__c = System.Now();
                                    break;
                                }
                            } 
                        } 
                    }
                    else{
                        con.Product_Add_Date__c = System.Now();
                         break; 
                    }
                } 
                
                conIdSet.add(con.id);   
            }
            Five9LSP__Five9_List__c F9List = new    Five9LSP__Five9_List__c();
            F9List = [select id from Five9LSP__Five9_List__c where Name='Master List' limit 1 FOR UPDATE];   
            List<Five9LSP__Five9_List_Item__c> f9Listitems = [Select id,Name,Five9LSP__Contact__c,Five9LSP__Five9_List__c from 
                                                              Five9LSP__Five9_List_Item__c Where Five9LSP__Five9_List__c=:F9List.id and Five9LSP__Contact__c in:conIdSet FOR UPDATE];
            
            Map<Id,Five9LSP__Five9_List_Item__c> five9ItemMap = new Map<Id,Five9LSP__Five9_List_Item__c>(); 
            List<Five9LSP__Five9_List_Item__c> five9ListToInsert = new List<Five9LSP__Five9_List_Item__c>();
            if(!f9Listitems.isEmpty()){
                for(Five9LSP__Five9_List_Item__c f9item :f9Listitems){
                    five9ItemMap.put(f9item.Five9LSP__Contact__c,f9item);
                }
            }
            for(Contact con : newCon){
              
            
               // if(newMap.get(con.Id).Product__c!=null && five9ItemMap!=null && !five9ItemMap.containsKey(con.Id) && (newMap.get(con.Id).Potential_Buyer__c!=oldMap.get(con.Id).Potential_Buyer__c || newMap.get(con.Id).Product__c!=oldMap.get(con.Id).Product__c)&&(newMap.get(con.Id).Product__c == splitVal[0]||newMap.get(con.Id).Product__c ==splitVal[1])){
                if(newMap.get(con.Id).Product__c!=null && five9ItemMap!=null && !five9ItemMap.containsKey(con.Id) && (newMap.get(con.Id).Potential_Buyer__c!=oldMap.get(con.Id).Potential_Buyer__c || newMap.get(con.Id).Product__c!=oldMap.get(con.Id).Product__c)){
                
                    System.debug('==============myLabel========'+myLabel);
                    Five9LSP__Five9_List_Item__c newItem = new Five9LSP__Five9_List_Item__c();
                    newItem.Five9LSP__Five9_List__c  = F9List.Id;
                    newItem.Five9LSP__Contact__c = con.Id; 
                    five9ListToInsert.add(newItem);         
                }
            }
            try{
                if(!five9ListToInsert.isEmpty()){
                    insert five9ListToInsert;
                     System.debug('==============five9ListToInsert========'+five9ListToInsert);
                }
            }catch(Exception de){}       
        }
    }
    public static void addtoMasterListI(List<Contact> newCon,Map<Id,Contact> newMap) {  
        if(!Test.isRunningTest()){
            list<Contact> con1 = new list<Contact>();
            con1 = [Select Id,Product__c,Potential_Buyer__c,Product_Add_Date__c from Contact where id =: newCon[0].Id LIMIT 1];
            System.debug('========con1==============='+con1);
            Set<id> conIdSet= new Set<id>();
            boolean booleanVal = false;
            for(Contact con : con1) {
                String newVal = newMap.get(con.Id).Product__c;
                if(con.Product__c != null && con.Product__c != '' ){
                    System.debug('========con.Product===============');
                    con.Product_Add_Date__c = System.Now();
                    break;
                }
                else If(con.Potential_Buyer__c != null && con.Potential_Buyer__c != ''){
                    con.Product_Add_Date__c = System.Now();
                    break;
                }
                conIdSet.add(con.id); 
                
            }
            update con1;
            
            String myLabel = System.Label.ProductValue;
            System.debug('============'+myLabel);
            String []splitVal = myLabel.split(' ');
            
            Five9LSP__Five9_List__c F9List = new    Five9LSP__Five9_List__c();
            F9List = [select id from Five9LSP__Five9_List__c where Name='Master List' limit 1 FOR UPDATE];   
            System.debug('===========F9List==================='+F9List);
            List<Five9LSP__Five9_List_Item__c> f9Listitems = [Select id,Name,Five9LSP__Contact__c,Five9LSP__Five9_List__c from 
                                                              Five9LSP__Five9_List_Item__c Where Five9LSP__Five9_List__c=:F9List.id and Five9LSP__Contact__c in:conIdSet];
            
            Map<Id,Five9LSP__Five9_List_Item__c> five9ItemMap = new Map<Id,Five9LSP__Five9_List_Item__c>(); 
            List<Five9LSP__Five9_List_Item__c> five9ListToInsert = new List<Five9LSP__Five9_List_Item__c>();
            if(!f9Listitems.isEmpty()){
                for(Five9LSP__Five9_List_Item__c f9item :f9Listitems){
                    five9ItemMap.put(f9item.Five9LSP__Contact__c,f9item);
                }
            }
            for(Contact con : newCon){
                if(con.id != null){
                    //if(five9ItemMap!=null && !five9ItemMap.containsKey(con.Id) &&(newMap.get(con.Id).Product__c == splitVal[0]||newMap.get(con.Id).Product__c ==splitVal[1])){
                    if(five9ItemMap!=null && !five9ItemMap.containsKey(con.Id)){
                    
                        System.debug('==========222222222=======');
                        Five9LSP__Five9_List_Item__c newItem = new Five9LSP__Five9_List_Item__c();
                        newItem.Five9LSP__Five9_List__c  = F9List.Id;
                        newItem.Five9LSP__Contact__c = con.Id; 
                        five9ListToInsert.add(newItem);         
                    }
                }
            }
            try{
                if(!five9ListToInsert.isEmpty()){
                    insert five9ListToInsert;
                }
            }catch(Exception de){}       
        }
    }
    public static void dummy(){
        integer i = 0;   
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
}