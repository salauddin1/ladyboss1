@isTest
public class ContactFromCaseBatchTest {
    @isTest 
    public static void createContact(){
        Case cs = new Case();
        cs.SuppliedName = 'abc xyz';
        cs.SuppliedEmail = 'tirthtest@gmail.com';
        cs.contactCreated__c = false;
        
        insert cs;
        ContactFromCaseBatch cst = new ContactFromCaseBatch();
        Database.executeBatch(cst);
    }
    @isTest 
    public static void createContact1(){
    
        Case cs = new Case();
        cs.SuppliedName = 'abc';
        cs.SuppliedEmail = 'tirthtest@gmail.com';
        cs.SuppliedPhone = '565456456';

        insert cs;
        ContactFromCaseBatch cst = new ContactFromCaseBatch();
        Database.executeBatch(cst);
    }
    @isTest 
    public static void createContact2(){
    
        Case cs = new Case();
        //cs.SuppliedName = 'abc xyz';
        cs.SuppliedEmail = 'tirthtest@gmail.com';

        insert cs;
        ContactFromCaseBatch cst = new ContactFromCaseBatch();
        Database.executeBatch(cst);
    }
}