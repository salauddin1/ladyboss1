public class ChangeCaseDueDateControllerV1 {
    @AuraEnabled
    public static case getCaseRecord(Id caseId){
        List<case> subsequentList = [select id,Status,Facebook_Message__c,Called__c,Call_1_AM__c,Call_2_AM__c,Call_3_AM__c,Call_4_AM__c,Call_5_AM__c,Do_Not_Contact__c,Never_Answered__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c from Case Where id=:caseId];
        return(subsequentList[0]);
    }  
    @AuraEnabled public static case saveCase(case cas,String AddRemoveDays,String fieldNameForDays){
        
        AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfCase' );
        
         if(fieldNameForDays == 'Call_1_AM__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Call_1_AM__c = true;
            }else{
               cas.Call_1_AM__c = false; 
            }
           
        }
        else if(fieldNameForDays == 'Call_2_AM__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Call_2_AM__c = true;
            }else{
               cas.Call_2_AM__c = false; 
            }
        }
        else if(fieldNameForDays == 'Call_3_AM__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Call_3_AM__c = true;
            }else{
               cas.Call_3_AM__c = false; 
            }
        }
       else if(fieldNameForDays == 'Call_4_AM__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Call_4_AM__c = true;
            }else{
               cas.Call_4_AM__c = false; 
            }
        }
       else if(fieldNameForDays == 'Call_5_AM__c'){
            
            if(AddRemoveDays == 'true'){
                cas.Call_5_AM__c = true;
            }else{
               cas.Call_5_AM__c = false; 
            }
        }
       else if(fieldNameForDays == 'Do_Not_Contact__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Do_Not_Contact__c = true;
                cas.status= 'Closed';
            }else{
               cas.Do_Not_Contact__c = false; 
            }
        }
        else if(fieldNameForDays == 'Never_Answered__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Never_Answered__c = true;
                cas.status= 'Closed';
            }else{
               cas.Never_Answered__c = false; 
            }
        }
       else if(fieldNameForDays == 'Not_recovered__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Not_recovered__c = true;
                cas.status= 'Closed';
            }else{
               cas.Not_recovered__c = false; 
            }
        }
       else if(fieldNameForDays == 'Saved_Payment__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Saved_Payment__c = true;
               
            }else{
               cas.Saved_Payment__c = false; 
            }
        }
       else if(fieldNameForDays == 'Text_Message__c'){
            
            if(AddRemoveDays == 'true'){
                cas.Text_Message__c = true;
                cas.status= 'Closed';
            }else{
               cas.Text_Message__c = false; 
            }
        }
       else if(fieldNameForDays == 'Recovered_Payment__c'){
           
            if(AddRemoveDays == 'true'){
                cas.Recovered_Payment__c = true;
                cas.status= 'Closed';
            }else{
               cas.Recovered_Payment__c = false; 
            }
        }
        else if(myCustomSetting != null){
            Decimal theFieldValue = (Decimal) myCustomSetting.get(fieldNameForDays);
            System.debug('theFieldValue : '+theFieldValue);
            System.debug('AddRemoveDays : '+AddRemoveDays);
            if(AddRemoveDays == 'true'){
                cas.DueDate__c = cas.DueDate__c.addDays(integer.valueof(theFieldValue));
            }else if(AddRemoveDays == 'false'){
                cas.DueDate__c = cas.DueDate__c.addDays(0-integer.valueof(theFieldValue));
            }
            if(fieldNameForDays == 'Stop_Correspondence__c'){
                updateContact(cas, AddRemoveDays);
            }
            if(fieldNameForDays == 'Saved_Payment__c'){
                updateCaseStatus(cas, AddRemoveDays);
            }
        }
        
        upsert cas;
        
        return cas;
    }  
    public static void updateContact(Case cas,String flag){
        System.debug('contactId is : '+cas.ContactId);
        System.debug('isTrue : '+flag);
        Boolean value = false;
        if(flag=='true'){
            value = true;
            
        }
        List<Contact> ctList = [SELECT Id,DoNotCall,Other_Phone_National_DNC__c,Phone_National_DNC__c FROM Contact WHERE Id = : cas.ContactId];
        ctList.get(0).DoNotCall = value; 
        ctList.get(0).Other_Phone_National_DNC__c = value; 
        ctList.get(0).Phone_National_DNC__c = value; 
        update ctList;
    }
    public static void updateCaseStatus(Case cas,String flag){
        System.debug('task : '+cas);
        System.debug('flag : '+flag);
        if(flag == 'true'){
            cas.Status = 'Completed';
        }else{
            cas.Status = 'Open';
        }
        update cas;
    }
    
    public static void recoveredPayment(Case cas,String flag){
        if(flag == 'true'){
            cas.DueDate__c = System.today();
        }
        update cas;
    }
    @AuraEnabled
    public static case getCase(Id caseId){
        Case caseOb = [select id,Status,Called__c,Call_1_AM__c,Call_2_AM__c,Call_3_AM__c,Call_4_AM__c,Call_5_AM__c,Do_Not_Contact__c,Never_Answered__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Facebook_Message__c,Text_Message__c from Case Where id=:caseId];
        return(caseOb );
    }  
    
}