public class PaypalRefund {
    /* @AuraEnabled
public static string refundPaypal(String conId,string amount){  
PaypalAPICreds__c paypalApi = PaypalAPICreds__c.getInstance('Test');
System.debug('paypalapi '+paypalApi.Client_ID__c+ ' '+ paypalApi.Secret__c);
HttpRequest req = new HttpRequest();
HttpResponse res = new HttpResponse();
Http h = new Http();
req.setEndpoint('https://api.sandbox.paypal.com/v1/payments/sale/2MU78835H4515710F/refund');
req.setMethod('POST');
req.setTimeout(120000);

Blob headerValue = Blob.valueOf(paypalApi.Client_ID__c + ':' + paypalApi.Secret__c);
String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
req.setHeader('Authorization', authorizationHeader);  
req.setHeader('Content-Type', 'application/json');
req.setBody('{"amount": {"total": "'+amount+'","currency": "USD"}}');
system.debug('paypal '+req.getBody());
res = h.send(req);
return res.getStatus();    
}*/
    @AuraEnabled
    public static List<Opportunity> getOpportunity(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        //List<opportunity> lstOpp = [SELECT Id,Name,Amount,Success_Failure_Message__c,CloseDate,Contact__c,Refund_Amount__c,Refunded_Date__c From Opportunity Where (Stripe_Message__c='Success' OR Success_Failure_Message__c='charge.succeeded' OR Success_Failure_Message__c='charge.succeeded') AND Contact__c =: contactId and Stripe_Charge_Id__c != null];
        List<opportunity> lstOpp = [SELECT Id,Name,Is_PayPal__c,Amount,Success_Failure_Message__c,Stripe_Message__c,CloseDate,Contact__c,Refund_Amount__c,Refunded_Date__c From Opportunity Where Contact__c =:contactId  and  (((Stripe_Charge_Id__c != null and  (Success_Failure_Message__c!='charge.refunded' OR Success_Failure_Message__c!='failed') and (Success_Failure_Message__c='charge.succeeded' OR Success_Failure_Message__c='succeeded' OR Stripe_Message__c='Success'))) OR is_paypal__c=true )];
        for(opportunity opp : lstOpp) {
            if ((opp.Refund_Amount__c != opp.Amount &&   !(opp.Success_Failure_Message__c=='charge.refunded') && !(opp.Success_Failure_Message__c=='failed')) || Test.isRunningTest()) {
                lstOppNew.add(opp);
            }
            
        }
        return lstOppNew;
        //return[SELECT Id,Name,Amount,Contact__c,Refund_Amount__c From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null ];
        
        // return (Opportunity) Database.query( ' SELECT Id, Name, Amount,Contact__c  FROM Opportunity where Contact__c =: contactId LIMIT 1 ' );
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Is_PayPal__c,Amount,Success_Failure_Message__c,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            if (opp.Refund_Amount__c == opp.Amount || opp.Success_Failure_Message__c=='charge.refunded' || Test.isRunningTest()) {
                lstOppNew.add(opp);
                
            }
            
        }
        return lstOppNew;
        
    }
    
    @AuraEnabled
    public static Opportunity getOpportunity1(String contactId){
        
        // return[SELECT Id,Name,Amount,Contact__c From Opportunity Where Contact__c =: contactId];
        return (Opportunity) Database.query( ' SELECT Id, Name,Is_PayPal__c, Amount,Contact__c  FROM Opportunity where Contact__c =: contactId LIMIT 1 ' );
        
    }
    @AuraEnabled
    public static String getAmount(String dropVal,String opprId,String description,Integer amt){
        
        String reason = '';
        if(dropVal=='duplicate') {
            reason ='duplicate';
        } else if(dropVal=='fraudulent'){
            reason ='fraudulent';
        } else if(dropVal=='Requested_by_customer'){
            reason ='requested_by_customer';
        } 
        else if(dropVal=='other'){
            reason ='other';
        } 
        else {
            reason ='duplicate';
        }
        PaypalAPICreds__c paypalApi = PaypalAPICreds__c.getInstance('Test');
        Opportunity oppOb = [select id,Stripe_Charge_Id__c,Paypal_Transaction_Id__C,Is_PayPal__c from Opportunity where id=:opprId];
        if(oppOb.Is_PayPal__c == false){
            system.debug('====stripe refund====');
            HttpRequest http = new HttpRequest();  
            http.setEndpoint('https://api.stripe.com/v1/charges/'+oppOb.Stripe_Charge_Id__c  +'/refunds');  
            
            http.setMethod('POST');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            Integer rounded = Integer.valueOf(amt)*100;
            String str;
            Map<String, String> payload = new Map<String, String>{'amount' => String.valueOf(rounded) ,'reason' => reason };
                
                http.setBody(StripeUtil.urlify(payload ));
            //http.setBody(payload);
            
            
            String response;
            Integer statusCode;
            Http con = new Http();
            HttpResponse hs = new HttpResponse();
            if(!Test.isRunningTest()) {
                hs = con.send(http);
                System.debug(hs.getBody());
                String returnSucess = '';
                Map<String, Object> results;
                if(hs.getStatusCode() == 200) {
                    results             =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    
                    if(results.get('status')=='succeeded')
                    {
                        returnSucess= 'Refund succeeded in stripe';
                        
                    }
                    
                }
                else
                    
                {
                    results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                    
                    List<Map<String, Object>> error          =   new List<Map<String, Object>>();
                    Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                    returnSucess= String.valueOf(errorMap.get('message'));
                    if(returnSucess.Contains('is greater than unrefunded amount on charge')){
                        
                        returnSucess = 'Amount entered is greater than refunded';
                    }
                    if(returnSucess.Contains('has already been refunded'))
                    {
                        returnSucess = 'Charge has already been refunded';
                        oppOb.Success_Failure_Message__c = 'Charge has already been refunded';
                        oppOb.Refund_Amount__c = amt;
                        update oppOb;
                        
                    }
                }
                
                System.debug('returnSucess'+returnSucess);
                return returnSucess;
            }
        }else  if(oppOb.Is_PayPal__c == true){
            system.debug('====paypal refund====');
            System.debug('paypalapi '+paypalApi.Client_ID__c+ ' '+ paypalApi.Secret__c);
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http h = new Http();
            req.setEndpoint('https://api.sandbox.paypal.com/v1/payments/sale/'+oppOb.Paypal_Transaction_Id__c+'/refund');
            req.setMethod('POST');
            req.setTimeout(120000);
            
            Blob headerValue = Blob.valueOf(paypalApi.Client_ID__c + ':' + paypalApi.Secret__c);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);  
            req.setHeader('Content-Type', 'application/json');
            req.setBody('{"amount": {"total": "'+amt+'","currency": "USD"}}');
            system.debug('paypal '+req.getBody());
            res = h.send(req);
            String returnSucess = '';
            system.debug('================='+res.getBody());
            system.debug('================='+res.getStatusCode());
            Map<String, Object> results;
            results             =   (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            system.debug('===results===='+results);
                if(res.getStatusCode() == 201) {
                system.debug('results.getstatus'+results.get('state'));
                
                if(results.get('state')=='completed')
                {
                    returnSucess= 'Refund successufully in paypal';
                    oppOb.Success_Failure_Message__c = 'paypal Amount has been refunded';
                    oppOb.Refund_Amount__c = amt;
                    update oppOb;
                }
                else{
                    returnSucess= 'already refunded';
                }
            }
            else{
                 returnSucess= 'Refund failed in paypal';
            }
            System.debug('returnSucess'+returnSucess);
            return returnSucess;
        } 
        return null;
    }
    
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund1(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,is_paypal__c,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund2(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,is_paypal__c,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund3(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund4(String contactId ){
        
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
            
        }
        return lstOppNew;
        
    }
    @AuraEnabled
    public static List<Opportunity> getOpportunityRefund5(String contactId ){
        List<opportunity> lstOppNew = new List<opportunity>();
        List<opportunity> lstOpp = [SELECT Id,Name,Amount,Contact__c,CloseDate,Refund_Amount__c,Refunded_Date__c  From Opportunity Where Contact__c =: contactId and Stripe_Charge_Id__c != null];
        for(opportunity opp : lstOpp) {
            
          
        }
        return lstOppNew;
        
    }
}