@isTest
public class CampaignMemberdeleteHelper_Test {
    @isTest
    public static void CampaignMemberdelete() {
    ContactTriggerFlag.isContactBatchRunning=false;
        List<CampaignMember> camMemberIdSet = new List<CampaignMember>(); 
        Set<id> conIdSet = new Set<id>();
        Triggers_activation__c tAct = new Triggers_activation__c();
        tAct.CampaignMemberdeleteHelper__c=true;
        tAct.Name = 'test';
        insert tAct;
        Account acc = new Account(Name='Test',Created_At__c=system.today(),NMI_External_ID__c='1234567');
        insert acc;
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.Email = 'test@test.com';
        insert cnt;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'testcmp1';
        cmp.IsActive = TRUE;
        cmp.type = 'Phone Team';
        insert cmp;
        Campaign cmp1 = new Campaign();
        cmp1.Name = 'testcmp2';
        cmp1.IsActive = TRUE;
        cmp1.type = 'Phone Team';
        insert cmp1;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = cnt.Id;
        insert cmpMem;
        
       CampaignMember cmpMem1 = new CampaignMember();
        cmpMem1.CampaignId = cmp1.Id;
        cmpMem1.ContactId = cnt.Id;
        insert cmpMem1;
        List<CampaignMember> campM = new List<CampaignMember>();
        campM.add(cmpMem);
        camMemberIdSet.add(cmpMem); 
        campM.add(cmpMem1);
        camMemberIdSet.add(cmpMem1); 
       
        conIdSet.add(cnt.id);
        camMemberIdSet = [select id,CampaignId,ContactId,Campaign.Name from CampaignMember where CampaignId =: cmp.Id AND ContactId =:cnt.Id limit 1];
        
         Async_Apex_Limit__c apex = new Async_Apex_Limit__c();
        apex.Name = 'Async Limit Left';
        apex.Remaining__c = 300151;
        insert apex;
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = cnt.Id;
        opp.Name = 'test';
        opp.AccountId=acc.Id;
        opp.Payment_Date__c=system.today();
        opp.Amount=20;
        opp.StageName = 'Qualification';
        opp.CloseDate = System.today();
        opp.Campaign__c = 'testcmp3';
        insert opp;
        
        
        
        
        //Opportunity opp2 = [select Id,contact_email__c ,Contact__r.Email from Opportunity limit 1];
        //System.debug('==========='+opp2.contact_email__c+'--------'+opp2.Contact__r.Email);
       
        opp.Payment_Date__c = opp.Payment_Date__c.addDays(2);
        update opp;
        
        Test.startTest();
        DeleteCampaignTriggerHandler.handlerMethod(campM);
        Test.stopTest();
        
        
        //update cmpMem; 
        delete cmpMem;
        
    }
    @isTest 
    public static void CampMemberTest4() {
        
        Contact con = new Contact();
        con.LastName = 'badole';
        con.Email = 'sourabh@test.com';
        insert con;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Sourabh Test';
        cmp.Type = 'Phone team';
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Sourabh Test 2';
        cmp2.Type = 'Phone team';
        insert cmp2;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        
        insert cmpMem;
        
        CampaignMember cmpMem2 = new CampaignMember();
        cmpMem2.CampaignId = cmp2.Id;
        cmpMem2.ContactId = con.Id;
        cmpMem2.status='Sent';
        
        
        insert cmpMem2;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Qualification', Amount=20, contact__c = con.id);
        insert opp;
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        CamMemberIdSet.add(cmpMem2.id);
        conIdSet.add(con.id);
        
        List<CampaignMember> camMembList = new List<CampaignMember>() ;
        camMembList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where id =: CamMemberIdSet and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null  ];
        
        
        Test.startTest();
        DeleteCampaignTriggerHandler.handlerMethod(camMembList);
        Test.stopTest();
        
    }
    

}