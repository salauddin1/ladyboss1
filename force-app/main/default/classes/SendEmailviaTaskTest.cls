@isTest
public class SendEmailviaTaskTest {
    
    @isTest
    public static void insertTask(){
        String ids = UserInfo.getUserId();
        Send_Email_for_Phone_2_min_survey__c s = new Send_Email_for_Phone_2_min_survey__c();
        s.Send_Email__c = false;
        s.Name = 'test';
        insert s;
        Phone_2_minute_Survey_agents__c ph = new Phone_2_minute_Survey_agents__c();
        ph.Salesforce_ID__c = ids;
        ph.Call_Duration__c = 90;
        ph.Name = 'test';
        insert ph;
        Contact cnt = new Contact();
        cnt.lastname = 'test';
        cnt.Email = 'test@test.com';
        insert cnt;
        
        Task tsk = new Task();
        tsk.WhoId = cnt.id;
        tsk.Five9__Five9AgentExtension__c = 'test';
        tsk.CallDurationInSeconds = 200;
        tsk.ownerid = UserInfo.getUserId();
        insert tsk;
        
        Lead ld = new Lead();
        ld.lastname = 'test';
        ld.company = 'test company';
        ld.status = 'New';
        insert ld;
        
        Task tsk1 = new Task();
        tsk1.WhoId = ld.id;
        tsk1.Five9__Five9AgentExtension__c = 'test';
        tsk.CallDurationInSeconds = 200;
        insert tsk1;
    }
    @isTest
    public static void insertTask2(){
        String ids = UserInfo.getUserId();
        Send_Email_for_Phone_2_min_survey__c s = new Send_Email_for_Phone_2_min_survey__c();
        s.Send_Email__c = false;
        s.Name = 'test';
        insert s;
        Phone_2_minute_Survey_agents__c ph = new Phone_2_minute_Survey_agents__c();
        ph.Salesforce_ID__c = ids;
        ph.Call_Duration__c = 90;
        ph.Name = 'test';
        insert ph;
        
        
        Lead ld = new Lead();
        ld.lastname = 'test';
        ld.company = 'test company';
        ld.status = 'New';
        insert ld;
        
        Task tsk1 = new Task();
        tsk1.WhoId = ld.id;
        tsk1.Five9__Five9AgentExtension__c = 'test';
        tsk1.CallDurationInSeconds = 200;
        insert tsk1;
    }
    @isTest
    public static void insertTask3(){
        Send_Email_for_Phone_2_min_survey__c s = new Send_Email_for_Phone_2_min_survey__c();
        s.Send_Email__c = false;
        s.Name = 'test';
        insert s;
        Phone_2_minute_Survey_agents__c ph = new Phone_2_minute_Survey_agents__c();
        ph.Salesforce_ID__c = '003dlkdfjkla';
        ph.Call_Duration__c = 90;
        ph.Name = 'test';
        insert ph;
        
        
        Contact cnt = new Contact();
        cnt.lastname = 'test';
        cnt.Email = 'test@test.com';
        insert cnt;
        User u = [Select id from user where name like '%nikolas%'];
        System.runAs(u){
            Task tsk = new Task();
            tsk.WhoId = cnt.id;
            tsk.Five9__Five9AgentExtension__c = 'test';
            tsk.CallDurationInSeconds = 200;
            insert tsk;
        }
        
    }
    
    @isTest
    public static void insertTask4(){
        Send_Email_for_Phone_2_min_survey__c s = new Send_Email_for_Phone_2_min_survey__c();
        s.Send_Email__c = false;
        s.Name = 'test';
        insert s;
        Phone_2_minute_Survey_agents__c ph = new Phone_2_minute_Survey_agents__c();
        ph.Salesforce_ID__c = 'ids';
        ph.Call_Duration__c = 90;
        ph.Name = 'test';
        insert ph;
        Contact cnt = new Contact();
        cnt.lastname = 'test';
        cnt.Email = 'test@test.com';
        insert cnt;
        
        Task tsk = new Task();
        tsk.WhoId = cnt.id;
        tsk.Five9__Five9AgentExtension__c = 'test';
        tsk.CallDurationInSeconds = 200;
        tsk.ownerid = UserInfo.getUserId();
        insert tsk;
        
        Lead ld = new Lead();
        ld.lastname = 'test';
        ld.company = 'test company';
        ld.status = 'New';
        insert ld;
        User u = [Select id from user where name like '%nikolas%'];
        System.runAs(u){
            Task tsk1 = new Task();
            tsk1.WhoId = ld.id;
            tsk1.Five9__Five9AgentExtension__c = 'test';
            tsk.CallDurationInSeconds = 200;
            insert tsk1;
        }
        
    }
}