global class InvoiceToChargeUpdateBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable  {
	
    global void execute(SchedulableContext SC) {
      InvoiceToChargeUpdateBatch batchObj = new InvoiceToChargeUpdateBatch(); 
      Database.executeBatch(batchObj,10);
   	}
	
	global InvoiceToChargeUpdateBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('Select id,Subscription_Id__c,Charge_Id__c,Invoice_Id__c,Invoice_Number__c,Period_Start__c,Period_End__c from Invoice__c WHERE Charge_Id__c!=null  ORDER BY CreatedDate ASC');
    }
    global void execute(Database.BatchableContext bc, List<Invoice__c> scope){
        try{
            
        	System.debug('Anydatatype_msg'+scope);
            Map<String, Set<Invoice__c>> subChargeMap = new Map<String, Set<Invoice__c>>();
            Map<String,id> oppLineItemMap = new Map<String,id>();
            Map<id,String> oppIdChargeMap = new Map<id,String>();
             Map<id, String> oppIdInvoiceNumberMap = new Map<id, String>();
            Map<String, Integer> numOfPayments = new Map<String, Integer>();    
            for (Invoice__c inv: scope) {
                if (!subChargeMap.containsKey(inv.Subscription_Id__c)) {
                    subChargeMap.put(inv.Subscription_Id__c, new Set<Invoice__c>());
                }
                subChargeMap.get(inv.Subscription_Id__c).add(inv);
                Integer numberofinvoice  = Invoices.getInvoice(inv.Subscription_Id__c);
                numOfPayments.put(inv.Subscription_Id__c, numberofinvoice);
            }
            system.debug('subChargeMap '+subChargeMap);
            List<Invoice__c> invDateList = [SELECT Id,Subscription_Id__c,Started_On__c FROM Invoice__c WHERE Started_On__c!=null and Subscription_Id__c in : subChargeMap.keyset()];
            Map<String,Invoice__c> subIdInvMap = new Map<String,Invoice__c>();
            for(Invoice__c inv : invDateList ){
                subIdInvMap.put(inv.Subscription_Id__c,inv);
            }
            List<Invoice__c> invoiceToDelete = new List<Invoice__c>();
            List<OpportunityLineItem> oppListTobeUpdated = new List<OpportunityLineItem>();
            Set<Id> setDeleteIds = new Set<Id>();
            Map<Id,OpportunityLineItem> prodOppline = new Map<Id,OpportunityLineItem>();
            List<Id> conId = new List<Id>();
            Map<id,Address__c> addCon = new Map<id,Address__c>();
            Map<id,Contact> conIdCon = new Map<id,Contact>();
            for (OpportunityLineItem opli: [SELECT id,Subscription_Id__c,Stripe_charge_id__c,Last_Charge_Id__c,Start__c,End__c,Number_Of_Payments__c,OpportunityId,opportunity.Stripe_Charge_Id__c,opportunity.sales_person__c,Product2Id,Tax_Amount__c,Opportunity.Contact__c,Shipping_Cost__c,Opportunity.Created_Using__C FROM OpportunityLineItem WHERE Subscription_Id__c IN: subChargeMap.keyset()]) {
                oppLineItemMap.put(opli.Subscription_Id__c, opli.OpportunityId);
            	Set<Invoice__c> invoiceSet = subChargeMap.get(opli.Subscription_Id__c);
                for (Invoice__c invl: invoiceSet) {
                    if (opli.Stripe_charge_id__c == null) {
                       opli.Stripe_charge_id__c = invl.Charge_Id__c; 
                    }                    
                    opli.Last_Charge_Id__c = invl.Charge_Id__c;
                    if (invl.Period_Start__c != null) {
                        opli.Start__c = invl.Period_Start__c;
                    }
                    if (invl.Period_End__c != null) {
                        opli.End__c = invl.Period_End__c;
                    }
                    if (invl.Period_End__c != null) {
                        opli.End__c = invl.Period_End__c;
                    }
                    if(subIdInvMap.containsKey(opli.Subscription_Id__c) && subIdInvMap.get(opli.Subscription_Id__c).Started_On__c!=null){
                        opli.Started_On__c = subIdInvMap.get(opli.Subscription_Id__c).Started_On__c;
                        invoiceToDelete.add(subIdInvMap.get(opli.Subscription_Id__c));
                    }
                    opli.Number_Of_Payments__c = numOfPayments.get(opli.Subscription_Id__c);
                    if(setDeleteIds.add(invl.Id))
                        invoiceToDelete.add(invl);
                }
                if ((opli.Tax_Amount__c != null && opli.Tax_Amount__c != 0) ) {
                    if ( opli.Opportunity.Created_Using__c == 'WooCommerce' && opli.Stripe_charge_id__c != opli.Last_Charge_Id__c) {
                        prodOppline.put(opli.Product2Id, opli);
                        conId.add(opli.Opportunity.Contact__c);
                    }else if(opli.Opportunity.Created_Using__c != 'WooCommerce'){
                        prodOppline.put(opli.Product2Id, opli);
                        conId.add(opli.Opportunity.Contact__c);
                    }
                }

                    oppListTobeUpdated.add(opli);
                
            }
            if (prodOppline.size()>0) {
                for(Address__c add: [SELECT Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c,Contact__c FROM Address__c WHERE Contact__c IN : conId and Contact__c!=null  AND Primary__c = true]){
                    addCon.put(add.Contact__c, add);
                }
                for (Contact con : [SELECT Id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry FROM Contact WHERE MailingCity != null AND Id IN:conId AND MailingState != null]) {
                    conIdCon.put(con.Id,con);
                }
                for(Product2 prod:[SELECT Id,Name,Price__c,Tax_Code__c,Statement_Descriptor__c FROM Product2 WHERE id IN :prodOppline.keySet()]){
                    String statDesc;
                    if(prod.Statement_Descriptor__c != null){
                        statDesc = prod.Statement_Descriptor__c;
                    }else {
                        statDesc = prod.Name;
                    }
                    String shippingStreet = '10010 Indian School Rd. NE';
                    String shippingCity = 'Albuquerque';
                    String shippingState = 'NM';
                    String shippingZipCode = '87112';
                    String shippingCountry = 'US';
                    if (addCon.containsKey(prodOppline.get(prod.Id).Opportunity.Contact__c)) {
                        shippingStreet = addCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).Shipping_Street__c;
                        shippingCity = addCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).Shipping_City__c;
                        shippingState = addCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).Shipping_State_Province__c;
                        shippingZipCode = addCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).Shipping_Zip_Postal_Code__c;
                        shippingCountry = addCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).Shipping_Country__c;
                    }else if (conIdCon.containsKey(prodOppline.get(prod.Id).Opportunity.Contact__c)) {
                        shippingStreet = conIdCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).MailingStreet;
                        shippingCity = conIdCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).MailingCity;
                        shippingState = conIdCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).MailingState;
                        shippingZipCode = conIdCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).MailingPostalCode;
                        shippingCountry = conIdCon.get(prodOppline.get(prod.Id).Opportunity.Contact__c).MailingCountry;
                    }
                    if (shippingCountry == 'USA' || shippingCountry == 'United States' || shippingCountry.contains('United')) {
                        shippingCountry = 'US';
                    }
                    TaxjarTaxCalculate.createTransactionNotFuture(shippingStreet,shippingCity,shippingState,shippingCountry,shippingZipCode,prod.Price__c, 1,prodOppline.get(prod.Id).Tax_Amount__c, statDesc,prodOppline.get(prod.Id).Last_Charge_Id__c, prod.Name,prodOppline.get(prod.Id).Shipping_Cost__c,prodOppline.get(prod.Id).Opportunity.Created_Using__c);
                }
            }
            
            for (Invoice__c inv: scope) {
                if(oppLineItemMap.containsKey(inv.Subscription_Id__c)){
                    oppIdChargeMap.put(oppLineItemMap.get(inv.Subscription_Id__c),inv.Charge_Id__c);
                    if (inv.Invoice_Number__c != null) {
                        oppIdInvoiceNumberMap.put(oppLineItemMap.get(inv.Subscription_Id__c), inv.Invoice_Number__c);
                    }
                }
            }
            List<Opportunity> oppList = [SELECT id,Stripe_Charge_Id__c,Charge_Description__c,Owner.name,Contact__c,Contact__r.firstname,Contact__r.lastname,Contact__r.email,Contact__r.phone,Sales_Person__c,User_to_give_credit__c,(select id,OpportunityId,Stripe_Charge_Id__c from OpportunityLineItems where Stripe_Charge_Id__c!=null) from Opportunity where id in : oppIdChargeMap.keySet() And Clubbed__c = true];
            
            List<Opportunity> opListToBeUpdated = new List<Opportunity>();
            for(Opportunity opp : oppList){
                opp.Stripe_Charge_Id__c = oppIdChargeMap.get(opp.id);
                if (oppIdInvoiceNumberMap.containsKey(opp.Id)) {
                    opp.Charge_Description__c = 'Invoice '+oppIdInvoiceNumberMap.get(opp.Id);
                }
                opListToBeUpdated.add(opp);
            }
            if (opListTobeUpdated.size()>0) {
                for(Opportunity opp : opListToBeUpdated){
                    OpportunityWithStipeChargeIdHandler.createMetadataAndUpdate(opp);
                    String salesPerson;
                    if(opp.Sales_Person__c != null && opp.Sales_Person__c !=''){
                        salesPerson = opp.Sales_Person__c;
                    }else{
                        salesPerson = opp.Owner.name;
                    }
                    if(oppListTobeUpdated.size()  > 0){
                        system.debug('--->oppLineItemToUpdate-->'+opp.OpportunityLineItems);
                        Set<String> setof = new Set<String>();
                        for(OpportunityLineItem opli :oppListTobeUpdated ){
                            if(opli.Stripe_charge_id__c != opp.Stripe_Charge_Id__c){
                                if(setof.add(opli.stripe_charge_id__c)){
                                    if(salesPerson!=null && salesPerson!=''){
                                        OpportunityWithStipeChargeIdHandler.createMetadataAndUpdateOLI2(opli,opp.sales_person__c,opp.Id);
                                    }
                                }
                            }                            
                        }
                    }
                    
                }
            	update opListTobeUpdated;
            }
            if (oppListTobeUpdated.size()>0) {
                update oppListTobeUpdated;
            }
            if (invoiceToDelete.size()>0) {
            	delete invoiceToDelete;
            }
            if (Test.isRunningTest()) {
            	Integer i = 1/0;
            }
        }catch(Exception e){
                System.debug(e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'InvoiceToChargeUpdateBatch',
                        'execute',
                        null,
                        e
                    )
                );
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}