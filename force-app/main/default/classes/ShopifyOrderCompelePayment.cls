public class ShopifyOrderCompelePayment {
    @AuraEnabled
    public static List<Product2> shipOrder(String searchKey){
        List<Product2> pro = new List<Product2>();
        String key = '%' +searchKey + '%';
        pro = [select id,Name,StockKeepingUnit,Available_For_Shipstation_Orders__c from Product2 where Available_For_Shipstation_Orders__c =: true and Name LIKE :key LIMIT 10 ]; 
        return pro;
    }
    @AuraEnabled
    public static List<Contact> getContact(String contactId){
        List<Contact> con = new List<Contact>();
        con = [select id,FirstName,lastName,Name,MobilePhone,Phone,Email,MailingStreet,MailingCity,MailingAddress,MailingState,MailingCountry,MailingPostalCode, (select id,Shipping_Street__c,Shipping_City__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c,Shipping_Country__c from Addresses__r where Primary__c=true) from Contact where Id =: contactId LIMIT 1 ]; 
        return con;
    }
    @AuraEnabled
    public static List<Card__c> getCard(String contactId){
        List<Card__c> card = new List<Card__c>();
        card = [select id,Last4__c,Card_ID__c,Contact__r.Stripe_Customer_Id__c from Card__c where Contact__c =: contactId]; 
        return card;
    }
    
    @AuraEnabled
    public Static String CreateShopifyOrders(String OrderId, String listItems,String cardId) {
        String idprod='';
        String accessToken = '';
        Double ExpectedAmount = 0.00;
        Integer amt;
        String errorMsg;
        Boolean addFlag;
        System.debug('======cardId========='+cardId);
        System.debug('==============================OrderId'+OrderId);
        system.debug('=======listItems'+listItems);
        Map<String, Object> OrderMap = (Map<String, Object>)JSON.deserializeUntyped(OrderId);
        Order ordJson = new Order();
        shopifyLineItemWrapper Items = new shopifyLineItemWrapper();
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c=true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        List<Object> ProdList = (List<Object>)JSON.deserializeUntyped(listItems);
        List<String> productid = new List<String> ();
        Map<String, Object> producmap    = new Map<String, Object>();
        Map<String, Object> lineItemData = new Map<String, Object> ();
        ordJson.line_items = new List <shopifyLineItemWrapper>();
        for(Object lineItems : ProdList) {
            producmap    = (Map<String, Object>)lineItems;
            lineItemData = (Map<String, Object>)producmap.get('selectedItem');
            if(lineItemData != null) {
                Items = new shopifyLineItemWrapper();
                Items.quantity = Integer.valueOf(producmap.get('Quantity'));
                Items.title    = String.valueOf(lineItemData.get('text'));
                Items.price    = Double.valueOf(lineItemData.get('price'));
                Items.sku      = String.valueOf(lineItemData.get('SKU'));
                productid.add(String.valueOf(lineItemData.get('val')));
                ordJson.line_items.add(Items);
                ExpectedAmount += Double.valueOf(lineItemData.get('price'))*Double.valueOf(producmap.get('Quantity'));
            }
        }
        String addressErrorMessage = AddressValidation.resolveAddress(String.valueOf(OrderMap.get('address1__c')),String.valueOf(OrderMap.get('city__c')),String.valueOf(OrderMap.get('province__c')),'US',String.valueOf(OrderMap.get('zip__c')));
        System.debug('========alawara========='+addressErrorMessage);
        
        
        if( addressErrorMessage == 'No error') {
            amt = Integer.valueOf(ExpectedAmount*100);
            ordJson.transactions = new List<trnsaction>();
            trnsaction t= new trnsaction ();
            t.amount = ExpectedAmount;
            ordJson.transactions.add(t);
            ordJson.email                       = String.valueOf(OrderMap.get('Email__c'));
            ordJson.total_tax                   = Double.valueOf(OrderMap.get('Total_Tax__c'));
            ordjson.fulfillment_status          = null;
            ordjson.financial_status            = 'paid';
            ordJson.shipping_address = new ShippingAddress();
            ordJson.shipping_address.first_name = String.valueOf(OrderMap.get('First_Name__c'));
            ordJson.shipping_address.last_name  = String.valueOf(OrderMap.get('Last_Name__c'));
            ordJson.shipping_address.address1   = String.valueOf(OrderMap.get('address1__c'));
            ordJson.shipping_address.phone      = String.valueOf(OrderMap.get('phone__c'));
            ordJson.shipping_address.city       = String.valueOf(OrderMap.get('city__c'));
            ordJson.shipping_address.province   = String.valueOf(OrderMap.get('province__c'));
            ordJson.shipping_address.country    = 'US';
            ordJson.shipping_address.zip        = String.valueOf(OrderMap.get('zip__c'));
            ordJson.billing_address = new BillingAddress();
            ordJson.billing_address.first_name  = String.valueOf(OrderMap.get('First_Name__c'));
            ordJson.billing_address.last_name   = String.valueOf(OrderMap.get('Last_Name__c'));
            ordJson.billing_address.address1    = String.valueOf(OrderMap.get('address1__c'));
            ordJson.billing_address.phone       = String.valueOf(OrderMap.get('phone__c'));
            ordJson.billing_address.city        = String.valueOf(OrderMap.get('city__c'));
            ordJson.billing_address.province    = String.valueOf(OrderMap.get('province__c'));
            ordJson.billing_address.country     = 'US';
            ordJson.billing_address.zip         = String.valueOf(OrderMap.get('zip__c'));
            ordJson.customer = new CustomerInfo();
            ordJson.customer.first_name         = String.valueOf(OrderMap.get('First_Name__c'));
            ordJson.customer.last_name          = String.valueOf(OrderMap.get('Last_Name__c')); 
            ordJson.customer.email              = String.valueOf(OrderMap.get('Email__c'));
            ordJson.note_attributes             = new List<ExtraInfo>();
            ExtraInfo HideRecursive = new ExtraInfo();
            HideRecursive.name                  = 'InitiatedFromSalesforce';
            HideRecursive.value                 = 'InitiatedFromSalesforce';
            ordJson.note_attributes.add(HideRecursive);
            String Formatedjson = JSON.serialize(ordjson);
            
            Card__c card = new Card__c();
            card = [select id,Last4__c,Card_ID__c,Contact__r.Stripe_Customer_Id__c from Card__c where Id =: cardId limit 1]; 
            if(card != null) {
                HttpRequest http2 = new HttpRequest();
                HttpResponse hs2 = new HttpResponse();
                Http con3 = new Http();
                try {
                    http2.setEndpoint('https://api.stripe.com/v1/customers/'+card.Contact__r.Stripe_Customer_Id__c);
                    http2.setMethod('POST');
                    http2.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
                    http2.setBody('default_source='+card.Card_ID__c);        
                    hs2 = con3.send(http2);
                    System.debug('============'+hs2.getBody());
                }
                catch(Exception e) {}
                Map<String, Object> defultsource = (Map <String, Object>)JSON.deserializeUntyped(hs2.getBody());
                System.debug('============Formatedjson======='+Formatedjson);
                contact crd = [select Id, Stripe_Customer_Id__c from contact where Id =: String.valueOf(OrderMap.get('Contact__c'))];
                if(crd != null) {
                    if(hs2.getStatus() == 'OK') {
                        try{
                            HttpRequest chrgreq = new HttpRequest();
                            chrgreq.setEndpoint('https://api.stripe.com/v1/charges');
                            chrgreq.setMethod('POST');
                            chrgreq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                            chrgreq.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
                            chrgreq.setBody('amount='+amt+'&currency=usd&description=Charged for Shopify Order Items&customer='+crd.Stripe_Customer_Id__c);
                            Http chrghtp =new Http();
                            HttpResponse chrgres= chrghtp.send(chrgreq);
                            System.debug('======STripe charge====='+chrgres.getBody());
                            Map<String, Object> charge = (Map <String, Object>)JSON.deserializeUntyped(chrgres.getBody());
                            
                            if(String.valueOf(charge.get('status')) == 'succeeded') {
                                HttpRequest req = new HttpRequest();
                                req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders.json');
                                req.setMethod('POST');
                                req.setHeader('Content-Type', 'application/json');
                                req.setHeader('X-Shopify-Access-Token', accessToken);
                                req.setBody('{"order":'+Formatedjson+'}');
                                Http htp =new Http();
                                HttpResponse res= htp.send(req);
                                System.debug('==========='+RES.getBody());
                                
                                if(res.getStatusCode() == 201) {
                                    List<Product2> p = [select id,Email_Body__c from product2 where id IN:productid];
                                    for(product2 mailbody : p) {
                                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                        message.toAddresses = new String[] { String.valueOf(OrderMap.get('Email__c')) };
                                            message.optOutPolicy = 'FILTER';
                                        message.subject = 'Order Confirmation email';
                                        String Body =  mailbody.Email_Body__c;
                                        message.setHtmlBody(Body);
                                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                                    }
                                }
                                Map<String, Object> results = (Map <String, Object>)JSON.deserializeUntyped(res.getBody());
                                Map<String, Object> order   =  (Map<String, Object>)results.get('order');
                                Map<String, Object> addressArray =  (Map<String, Object>)order.get('shipping_address');
                                idprod = String.valueOf(order.get('id'));
                                
                                if(results != null){
                                    Shopify_Orders__c ord = new Shopify_Orders__c();
                                    ord.Transaction_Id__c           = String.valueOf(charge.get('balance_transaction'));
                                    ord.Charge_Id__c                = String.valueOf(charge.get('id'));
                                    ord.Stripe_Charge_Status__c     = 'Payment Succeeded';
                                    ord.Order_Id__c                 = String.valueOf(order.get('id'));
                                    ord.Order_Number__c             = String.valueOf(order.get('order_number'));
                                    ord.Financial_Status__c         = String.valueOf(order.get('financial_status'));
                                    ord.Total_Line_Items_Price__c   = Double.valueOf(order.get('total_line_items_price'));
                                    ord.Total_Price__c              = Double.valueOf(order.get('total_price'));
                                    ord.Total_Tax__c                = Double.valueOf(order.get('total_tax'));
                                    ord.Subtotal_Price__c           = Double.valueOf(order.get('subtotal_price'));
                                    ord.Description__c              = String.valueOf(charge.get('receipt_url'));
                                    ord.Contact__c                  = String.valueOf(OrderMap.get('Contact__c'));
                                    ord.Status__c                   = 'Confirmed';
                                    ord.Email__c                    = String.valueOf(order.get('email'));
                                    ord.created_at__c               = String.valueOf(order.get('created_at'));
                                    ord.fulfillment_Status__c       = String.valueOf(order.get('fulfillment_status'));
                                    ord.Send_Fulfillment_Receipt__c = Boolean.valueOf(OrderMap.get('Send_Fulfillment_Receipt__c'));
                                    ord.Send_Receipt__c             = Boolean.valueOf(OrderMap.get('Send_Receipt__c'));
                                    
                                    
                                    if(order.containsKey('shipping_address')) {
                                        ord.First_Name__c               = String.valueOf(addressArray.get('first_name'));
                                        ord.Last_Name__c                = String.valueOf(addressArray.get('last_name'));
                                        ord.address1__c                 = String.valueOf(addressArray.get('address1'));
                                        ord.city__c                     = String.valueOf(addressArray.get('city'));
                                        ord.province__c                 = String.valueOf(addressArray.get('province'));
                                        ord.country__c                  = String.valueOf(addressArray.get('country'));
                                        ord.phone__c                    = String.valueOf(addressArray.get('phone'));
                                        ord.zip__c                      = String.valueOf(addressArray.get('zip'));
                                    }
                                    
                                    shopifyordercreation.isrecursive = true;
                                    insert ord;
                                    List<ShopifyLineItem__c> productItems   = new List<ShopifyLineItem__c>();
                                    List<Object> LineItems                  = (List<Object>)order.get('line_items');
                                    Map<String, Object> itemMap             = new Map<String, Object>();
                                    
                                    for(Object obj : LineItems){
                                        itemMap = (Map <String, Object>)obj;
                                        ShopifyLineItem__c shopfyItems = new ShopifyLineItem__c();
                                        shopfyItems.Shopify_Orders__c       =   ord.Id;
                                        shopfyItems.Item_Id__c              = String.valueOf(itemMap.get('id'));
                                        shopfyItems.Price__c                = Double.valueOf(itemMap.get('price'));
                                        shopfyItems.Quantity__c             = Double.valueOf(itemMap.get('quantity'));
                                        shopfyItems.sku__c                  = String.valueOf(itemMap.get('sku'));
                                        shopfyItems.Title__c                = String.valueOf(itemMap.get('title')); 
                                        shopfyItems.Name                    = String.valueOf(itemMap.get('title')); 
                                        shopfyItems.Contact__c              = String.valueOf(OrderMap.get('Contact__c'));
                                        shopfyItems.Fulfillable_Quantity__c = Double.valueOf(itemMap.get('fulfillable_quantity'));
                                        productItems.add(shopfyItems);
                                    }
                                    insert productItems;
                                }
                            }
                            
                            else {
                                Map<String, Object> chErrorMsg = (Map<String, Object>)charge.get('error');
                                errorMsg =   String.valueOf(chErrorMsg.get('message')) +' Please retry!';
                            }
                        }
                        catch(Exception e){
                             ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShopifyOrderCompelePayment',
                                 'createShopifyOrder',
                                    idprod,
                                     e
                                 ));
                        }
                    }
                    else{
                        Map<String, Object> chErrorMsg = (Map<String, Object>)defultsource.get('error');
                        errorMsg =   String.valueOf(chErrorMsg.get('message'));
                    }
                }
            }
        }
        else {
		errorMsg= '';
            errorMsg = addressErrorMessage;
        }
        return errorMsg;  
    }
    @future(callout=true)
    public Static void UpdateOder(String OrdId){
        String accessToken;
        Shopify_Orders__c OrdList=[select Order_Id__c,phone__c,province__c,zip__c, Email__c,city__c,address1__c,fulfillment_Status__c,First_Name__c,Last_Name__c,country__c from Shopify_Orders__c where Id =: OrdId limit 1];
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        try{
            cancelOrder cord = new cancelOrder();
            cord.id = OrdList.Order_Id__c;
            cord.email = OrdList.Email__c;
            String phone = OrdList.phone__c;
            phone = phone.remove('(');
            phone = phone.remove(')');
            phone = phone.remove('-');
            phone = phone.remove(' ');
            String country                   = OrdList.country__c;
            if(country.equalsIgnoreCase('India')){
                cord.phone = '+91'+phone;
            }
            else{
                cord.phone = '+1'+phone;
            }
            cord.shipping_address = new ShippingAddress();
            cord.shipping_address.first_name = OrdList.First_Name__c;
            cord.shipping_address.last_name  = OrdList.last_Name__c;
            cord.shipping_address.address1   = OrdList.address1__c;
            cord.shipping_address.phone      = OrdList.phone__c;
            cord.shipping_address.city       = OrdList.city__c;
            cord.shipping_address.province   = OrdList.province__c;
            cord.shipping_address.zip        = OrdList.zip__c;
            cord.shipping_address.country    = OrdList.country__c;    
            String jsonForUpdate = JSON.serialize(cord);     
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'.json');
            req.setMethod('PUT');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            req.setBody('{"order":'+jsonForUpdate+'}');
            Http htp =new Http();
            HttpResponse res= htp.send(req);
            System.debug('============='+res.getBody());
        }
        catch(Exception e){}
    }
    @future(callout=true)
    public Static Void cancelledOrder(String OrderId){
        String accessToken;
        Shopify_Orders__c OrdList = [select Order_Id__c,phone__c,province__c,zip__c, Email__c,city__c,address1__c,fulfillment_Status__c,First_Name__c,Last_Name__c,country__c from Shopify_Orders__c where Id =: OrderId limit 1];
        Shopify_Parameters__c Tokens = [select Access_Token__c,Domain_Name__c from Shopify_Parameters__c where Is_Active__c = true limit 1];
        if(Tokens != null){
            accessToken = Tokens.Access_Token__c;
        }
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Tokens.Domain_Name__c+'/admin/orders/'+OrdList.Order_Id__c+'/cancel.json');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('X-Shopify-Access-Token', accessToken);
            req.setBody('{}');
            Http htp =new Http();
            HttpResponse res= htp.send(req);
            System.debug('======response======='+res.getBody());
        }
        catch(Exception e){}
    }
    public class Order {
        public Double total_tax;
        public String email;
        public String fulfillment_status;
        public String financial_status;
        public Boolean send_receipt;
        public Boolean send_fulfillment_receipt;
        public List <shopifyLineItemWrapper> line_items;
        public ShippingAddress shipping_address;
        public BillingAddress billing_address;
        public CustomerInfo customer;
        public List<ExtraInfo> note_attributes;
        public List<trnsaction> transactions;
    }
    public class shopifyLineItemWrapper {
        public String title;
        public decimal price;
        public String sku;
        public Integer quantity;
    }
    public class ExtraInfo {
        public String name;
        public String value;
    }
    public class CustomerInfo {
        public String first_name;
        public String last_name;
        public String email;
    }
    public class cancelOrder{
        public String email;
        public String id;
        public String phone;
        public ShippingAddress shipping_address;
    }
    public class ShippingAddress {
        public String first_name;
        public String last_name;
        public String address1;
        public String phone;
        public String city;
        public String province;
        public String country;
        public String zip;
    }
    public class BillingAddress {
        public String first_name;
        public String last_name;
        public String address1;
        public String phone;
        public String city;
        public String province;
        public String country;
        public String zip;
    }
    public class trnsaction {
        public String kind = 'sale';
        public String status = 'success';
        public Double amount;
    }
    public class validatorWrapper {
        public String street;
        public String city;
        public String state;
        public String zip;
        public String country;
        
    }
}