@isTest
public class StripeCustomer_Service_Test {
     
    @isTest static void test(){
   Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
        Contact c=new Contact();
        c.email='ashish.sharma.devsfdc75@gmail.com';
        c.LastName='sharma';
       insert c;
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
       Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
       opp.CustomerID__c = 'cus_BUOWQlyttHhSXd';
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;

        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        //String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"ashish.sharma.devsfdc75@gmail.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        String body = '{"id":"evt_1D9DuHFzCf73siP0ZP2JH3Ml","object":"event","api_version":"2018-02-28","created":1536680161,"data":{"object":{"id":"cus_DLs0AtmAAzZHWC","object":"customer","balance":0,"created":1533329552,"currency":null,"default_source":"card_1CvAGBFzCf73siP0Ec1q4K3P","delinquent":false,"description":"grant+1jug423823409@ladyboss.com","discount":null,"email":"grant+1jug423823409@ladyboss.com","invoice_prefix":"5C3AF04","livemode":false,"metadata":{"name":"grant 1jug23492349","first_name":"grant","last_name":"1jug23492349"},"shipping":{"address":{"city":"rr","country":"US","line1":"Tester","line2":"Test2","postal_code":"","state":""},"name":"","phone":null},"sources":{"object":"list","data":[{"id":"card_1CvAGBFzCf73siP0Ec1q4K3P","object":"card","address_city":"98234hasjdfh","address_country":"United States of America","address_line1":"jasdkflj as8384","address_line1_check":"pass","address_line2":null,"address_state":"asdfasd","address_zip":"89098","address_zip_check":"pass","brand":"Visa","country":"CA","customer":"cus_DLs0AtmAAzZHWC","cvc_check":"pass","dynamic_last4":null,"exp_month":1,"exp_year":2019,"fingerprint":"89Hw4xtIbs92mApd","funding":"credit","last4":"1881","metadata":{},"name":"grant 1jug23492349","tokenization_method":null}],"has_more":false,"total_count":1,"url":"/v1/customers/cus_DLs0AtmAAzZHWC/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/customers/cus_DLs0AtmAAzZHWC/subscriptions"},"tax_info":null,"tax_info_verification":null},"previous_attributes":{"shipping":{"address":{"city":""}}}},"livemode":false,"pending_webhooks":6,"request":{"id":"req_fWppucCtJ6VQA0","idempotency_key":null},"type":"customer.updated"}';
        req.requestBody = Blob.valueOf(body);
          req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        RestContext.request = req;
       // req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        Test.startTest();
          Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
          StripeCustomer_Service.createCustomer();
        Test.stopTest();
    }
    
    @isTest static void test1(){
    Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
        Contact c=new Contact();
        c.email='ashish.sharma.devsfdc75@gmail.com';
        c.LastName='sharma';
       insert c;
        
         Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_BUOWQlyttHhSXd';
           sp.Customer__c=c.id;
        insert sp;
          String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
       opp.CustomerID__c = 'cus_BUOWQlyttHhSXd';
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
           opp.Stripe_Profile__c = null;

    
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"ashish.sharma.devsfdc75@gmail.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        req.requestBody = Blob.valueOf(body);
          req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        RestContext.request = req;
       // req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        Test.startTest();
 Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        StripeCustomer_Service.createCustomer();
        Test.stopTest();
    }
    @isTest static void test3(){
    Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
        Contact c=new Contact();
        c.email='ashish.sharma.devsfdc75@gmail.com';
        c.LastName='sharma';
      // insert c;
        
         Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_BUOWQlyttHhSXd';
           sp.Customer__c=c.id;
        insert sp;

         String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.CustomerID__c = 'cus_BUOWQlyttHhSXd';
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Stripe_Profile__c = null;
        opp.Quantity__c =1;
             
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"ashish.sharma.devsfdc75@gmail.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        req.requestBody = Blob.valueOf(body);
          req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        RestContext.request = req;
       // req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        
        Test.startTest();
          Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
          StripeCustomer_Service.createCustomer();
        StripeCustomer_Service.updateCustomer('cus_BUOWQlyttHhSXd', 'ashish.sharma.devsfdc75@gmail.com');
        Test.stopTest();
    }
}