@isTest
public class CaseCommunityPagecontroller_Test {
    @isTest
    public static void test() {
        Profile pf= [Select Id from profile where Name='System Administrator'];
        User u = new User(
            LastName = 'Site Guest User',
           // FirstName	 = 'TestCommunity',
            FirstName	 = 'LadyBossHub',
            Email = 'sourabh@ladyBoss.com',
            Username = 'sourabh@ladyBoss.com' ,
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'guest',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = pf.Id
            
        );
        insert u;
        
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@gmail.com';
        insert con;
        Case cs = new case();
            cs.ContactId  = con.Id;
            cs.Status = 'Open';
            cs.Origin = 'Email';
            cs.Is_Site_User_Case__c = true ;
            insert cs;
            
        System.runAs(u) {
            // The following code runs as user 'u' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            
            CaseComment csCom = new CaseComment();
            csCom.ParentId = cs.id;
            csCom.CommentBody = 'Test Body';
            insert csCom;
            
        }
        CaseComment csCom2 = new CaseComment();
        csCom2.ParentId = cs.id;
        csCom2.CommentBody = 'Test Body 2';
        insert csCom2;
        
        Object wr = CaseCommunityPagecontroller.getcaserec(con.Id);
        List<Object>  wrList = CaseCommunityPagecontroller.getCaseComentValues(cs.Id,'sourabh');
        CaseCommunityPagecontroller.setCaseComment(cs.Id, csCom2.CommentBody);
        Object wr2 = CaseCommunityPagecontroller.setCsObj() ;
    }
    @isTest
    public static void test2() {
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@gmail.com';
        insert con;
        
        Case cs2 = new case(); 
        cs2.ContactId  = con.Id;
        cs2.Status = 'Open';
        cs2.Origin = 'Web';
        cs2.Is_Site_User_Case__c = true ;
        String str = '{"wrCase":{"Subject":"LABS Supplements","Customer_Name__c":"Granttester","Description":"Test dat test"}}';
       	Map<object,Map<object,object>> wrMap =  new Map<object,Map<object,object>>();
        Map<object,object> caseDetail2 = new  Map<object,object>() ; 
        object st = 'Subject';
        object st1 = 'LABS Supplements';
        object st2 = 'Customer_Name__c';
        object st3 = 'Granttester';
        object st4 = 'Description';
        object st5 = 'Test dat test';
        object st6 = 'wrCase';
        caseDetail2.put(st,st1);
        caseDetail2.put(st2,st3);
        caseDetail2.put(st4,st5);
        wrMap.put(st6,caseDetail2);
        
     //   Map<object,object> caseDetail =( Map<object,object> )JSON.deserializeUntyped(str);
      //  Map<object,object> caseDetail2 = new  Map<object,object>() ; 
        Object wr3 = CaseCommunityPagecontroller.InsertNewCase(wrMap,con.Id);
        
    }
}