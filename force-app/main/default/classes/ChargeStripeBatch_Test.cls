@IsTest
public class ChargeStripeBatch_Test {
    static{
        Stripe_Parameters__c sp = new Stripe_Parameters__c(Token__c='test');
        insert sp;
    }
    
	static testMethod void test() {
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new ChargeStripeBatch());
        Test.stopTest();
	}
    
    static testMethod void test2() {
        system.Test.setMock(HttpCalloutMock.class, new StripeMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new ChargeStripeBatch('test'));
        Test.stopTest();
	}
}