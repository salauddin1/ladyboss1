@RestResource(urlMapping='/StripeInvoiceCreate_Service')
global class StripeInvoiceCreate_Service {
    @HttpPost
    global static void updateInvoice() {
        string ChargeId;
        if(RestContext.request.requestBody!=null){
            try {
                String str = RestContext.request.requestBody.toString();
                System.debug('-------firsttttt twooo----StripeInvoice_ServiceDemo'+str);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str);
                ChargeId                         = String.valueOf(results.get('id')); 
                Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('data');
                Map<String, Object>  lstCustomers2 = (Map<String, Object> )lstCustomers.get('object');
                Map<String, Object>  lstCustomers3 = (Map<String, Object> )lstCustomers2.get('lines');
                String charge = (String)lstCustomers2.get('charge');
                Decimal subtotal = 0;
                List<Object> data = (List<Object>)lstCustomers3.get('data');
                List<Map<String,Object>> dataList = new List<Map<String, Object>>();
                String customer = (String)lstCustomers2.get('customer');
                Set<String> subId = new Set<String>();
                Map<String, Object> subMap = new Map<String, Object>();
                if (charge == null) {
                    for(Object ob : data){
                        Map<String, Object> obmap = (Map<String, Object>)ob;
                        if (String.valueof(obmap.get('type')) == 'subscription') {
                            subtotal += Decimal.valueOf(String.valueOf(obmap.get('amount')))*0.01;
                        }
                        subMap.put(String.valueof(obmap.get('id')), ob);
                        System.debug('-------res----data--'+obmap.get('id'));
                        dataList.add(obmap);
                    }
                    subId.addAll(subMap.keySet());
                    for (OpportunityLineItem opli: [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c,Opportunity.Shipping_Cost__c FROM OpportunityLineItem where Subscription_Id__c in : subId AND Opportunity.Contact__c != null AND Opportunity.Shipping_Cost__c != null AND Opportunity.Shipping_Cost__c != 0]) {
                        StripeCreateInvoiceItem.createInvoiceItem('Shipping Cost', customer, 'usd', opli.Opportunity.Shipping_Cost__c, opli.Subscription_Id__c);
                    }
                    Add_Tax_To_Subs__c addTax = Add_Tax_To_Subs__c.getInstance();
                    Map<String, Id> subConMap = new Map<String, Id>();
                    Map<String, Id> subProdMap = new Map<String, Id>();
                    Map<Id, Address__c> addMap = new Map<Id, Address__c>();
                    Map<Id, Card__c> cardMap = new Map<Id, Card__c>();
                    Map<String, OpportunityLineItem> opliMap = new Map<String, OpportunityLineItem>();
                    Map<Id, String> taxCode = new Map<Id, String>();
                    if (addTax.Add_Tax__c == true) {
                        for (OpportunityLineItem opli: [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Contact__c FROM OpportunityLineItem where Subscription_Id__c in : subId AND Opportunity.Contact__c != null AND Tax_Percentage_s__c =null AND Opportunity.Created_Using__c != null]) {
                                subConMap.put(opli.Subscription_Id__c,opli.Opportunity.Contact__c);
                                subProdMap.put(opli.Subscription_Id__c,opli.Product2Id);
                                opliMap.put(opli.Subscription_Id__c, opli);
                            
                        }
                    }
                    if (subConMap.size()>0) {
                        for (Address__c add: [SELECT id, Shipping_City__c, Shipping_Street__c, Shipping_Country__c, Shipping_State_Province__c, Contact__c, Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c IN :subConMap.values()]) {
                            addMap.put(add.Contact__c, add);
                        } 
                        for (Card__c add: [SELECT id, Billing_City__c, Billing_Street__c, Billing_Country__c, Billing_State_Province__c, Contact__c, Billing_Zip_Postal_Code__c FROM Card__c WHERE Contact__c IN :subConMap.values()]) {
                            cardMap.put(add.Contact__c, add);
                        } 
                        for (Product2 prod: [SELECT id,Tax_Code__c FROM Product2 WHERE id IN : subProdMap.values() AND Tax_Code__c!=null]) {
                            taxCode.put(prod.Id, prod.Tax_Code__c);
                        }
                        for (String sub: subId) {
                            String street = 'LadyBoss Weight Loss 10010 Indian School Rd. NE';
                            String state = 'NM';
                            String city = 'Albuquerque';
                            String country = 'US';
                            String zipcode = '87112';
                            if (addMap.containsKey(subConMap.get(sub))) {
                                street = addMap.get(subConMap.get(sub)).Shipping_Street__c;
                                state = addMap.get(subConMap.get(sub)).Shipping_State_Province__c;
                                city = addMap.get(subConMap.get(sub)).Shipping_City__c;
                                country = addMap.get(subConMap.get(sub)).Shipping_Country__c;
                                zipcode = addMap.get(subConMap.get(sub)).Shipping_Zip_Postal_Code__c;
                            }else if (cardMap.containsKey(subConMap.get(sub)) && cardMap.get(subConMap.get(sub)).Billing_Street__c != null && cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c != null && cardMap.get(subConMap.get(sub)).Billing_City__c != null && cardMap.get(subConMap.get(sub)).Billing_State_Province__c != null && cardMap.get(subConMap.get(sub)).Billing_Country__c != null) {
                                street =  cardMap.get(subConMap.get(sub)).Billing_Street__c;
                                state = cardMap.get(subConMap.get(sub)).Billing_State_Province__c;
                                city = cardMap.get(subConMap.get(sub)).Billing_City__c;
                                country = cardMap.get(subConMap.get(sub)).Billing_Country__c;
                                zipcode = cardMap.get(subConMap.get(sub)).Billing_Zip_Postal_Code__c;
                            }
                            String codetax;
                            if (taxCode.get(subProdMap.get(sub)) == null) {
                                codetax = '99999';
                            }else {
                                codetax = taxCode.get(subProdMap.get(sub));
                            }
                            TaxjarTaxCalculate tx= new TaxjarTaxCalculate();
                            Map<String, Object> obmap1 = (Map<String, Object>)subMap.get(sub);
                            Decimal amount = Decimal.valueOf(String.valueOf(obmap1.get('amount')))*0.01;
                            tx = TaxjarTaxCalculate.ratemethod(street, city, state, country, zipcode, amount, 1, codetax);
                            Decimal taxRate = tx.tax.amount_to_collect;
                            Decimal taxPercentage = 0;
                            if(taxRate!=0.0){
                                taxPercentage = (taxRate/amount)*100;
                            }
                            Subscriptions.updateSubscription(sub,taxPercentage);
                        }  
                    }
                    /*Map<Id,Decimal> prodShip = new Map<Id,Decimal>();
Map<Id,String> prodSubscription = new Map<Id,String>();
System.debug('-------res----data--'+subId);
List<id> productIds = new List<Id>();
List<id> oppId  = new List<id>();
List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
if (addTax.Add_Shipping__c == true) {
oppLineItemList = [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Created_Using__c FROM OpportunityLineItem where Subscription_Id__c in : subId];
}else {
oppLineItemList = [SELECT id,Product2Id,Subscription_Id__c,OpportunityId,Tax_Percentage_s__c,Opportunity.Created_Using__c FROM OpportunityLineItem where Subscription_Id__c in : subId AND (Opportunity.Created_Using__c = 'V10' OR Opportunity.Add_Shipping_to_Subscription__c = true)];
}

system.debug('oppLineItemList  '+oppLineItemList);
List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();

if(oppLineItemList.size()>0){
for(OpportunityLineItem oli:oppLineItemList){
system.debug(oli.Opportunity.Created_Using__c);

List<OpportunityLineItem> oppLineItemList1 = [SELECT id,Product2Id,Subscription_Id__c,OpportunityId FROM OpportunityLineItem where OpportunityId = : oli.OpportunityId];
system.debug('oppLineItemList1  '+oppLineItemList1);
if(oppLineItemList1.size()>0 ){
for(OpportunityLineItem opli:oppLineItemList1){
productIds.add(opli.Product2Id);
prodSubscription.put(opli.Product2Id, opli.Subscription_Id__c);
}   
prodShip = totalShippingCost.getShippingCost(productIds);
for(Id prodId:prodShip.keySet()){
if(oli.Product2Id == prodId){
StripeCreateInvoiceItem.createInvoiceItem('Shipping Cost',customer,'usd',prodShip.get(prodId),prodSubscription.get(prodId));//Adding shipping cost to the subscription
oli.Shipping_Costs__c = prodShip.get(prodId);
oliToUpdate.add(oli);
if(oli.Tax_Percentage_s__c!=0 && prodShip.get(prodId)!=0){
Decimal a = -((prodShip.get(prodId)*oli.Tax_Percentage_s__c)/100);
StripeCreateInvoiceItem.createInvoiceItem('Tax on Shipping',customer,'usd',a,prodSubscription.get(prodId));//Subtracting the tax on shipping from the overall amount
}

}
}
} 
}
}
if(oliToUpdate.size()>0){
update oliToUpdate;
} */                    
                    List<OpportunityLineItem> opliList = [SELECT id,Subscription_id__c,Product2.Is_Eligible_For_Commission_Usage__c FROM OpportunityLineItem WHERE Subscription_Id__c IN : subId AND Product2.Is_Eligible_For_Commission_Usage__c = true];
                    if (opliList.size()>0) {
                        List<Stripe_Profile__c> spList = [SELECT id,Customer__r.Available_Commission__c,Customer__c,Balance__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c = : customer AND Customer__c != null AND Customer__r.Available_Commission__c != null AND Customer__r.Available_Commission__c > 0];
                        if (spList.size()>0) {    
                            Contact con = [SELECT id,Available_Commission__c FROM Contact WHERE id = : spList[0].Customer__c];   
                            List<OpportunityLineItem> opSubList = [SELECT id FROM OpportunityLineItem WHERE Status__c = 'Active' AND Opportunity.Contact__c =: con.Id AND Product2.Price__c <= :con.Available_Commission__c]; 
                            if (opSubList.size()>0) {
                                Decimal amt =  - (con.Available_Commission__c);
                                if (subtotal >= con.Available_Commission__c) {
                                    amt =  - (con.Available_Commission__c);
                                    con.Available_Commission__c = 0;
                                    con.Update_Balance_Reason__c  = ' ';
                                }else {
                                    amt = -subtotal;
                                    con.Available_Commission__c = con.Available_Commission__c - subtotal;
                                    con.Update_Balance_Reason__c  = 'Used for payment of renewal';
                                }
                                Http http = new Http();
                                HttpRequest req = new HttpRequest();
                                req.setEndpoint('https://api.stripe.com/v1/customers/'+spList[0].Stripe_Customer_Id__c+'/balance_transactions');
                                req.setMethod('POST');
                                req.setHeader('content-type', 'application/x-www-form-urlencoded');
                                Blob headerValue1 = Blob.valueOf(StripeAPI.ApiKey + ':');
                                req.setBody('amount='+(amt*100).intValue()+'&currency=usd&description=PAP Balance');
                                String authorizationHeader1 = 'BASIC ' +
                                    EncodingUtil.base64Encode(headerValue1);
                                req.setHeader('Authorization', authorizationHeader1);
                                HttpResponse res = new HttpResponse();
                                if (!Test.isRunningTest()) {
                                    res = http.send(req);
                                }
                                if (res.getStatusCode() == 200  || Test.isRunningTest()) {
                                    update con;
                                    PAP_Commission_Usage__c pap = new PAP_Commission_Usage__c();
                                    pap.Contact__c = con.Id;
                                    pap.commission__c = amt;
                                    List<OpportunityLineItem> opli = [SELECT id,OpportunityId FROM OpportunityLineItem WHERE Subscription_id__c IN:subId ANd Subscription_id__c != null LIMIT 1];
                                    pap.Opportunity__c = opli[0].OpportunityId;
                                    pap.Usage_Reason__c = 'Applied to invoice '+ (String)lstCustomers2.get('id');
                                    insert pap;
                                }
                            }     
                        }
                    }
                }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripeInvoiceCreate_Service',
                        'updateInvoice',
                        ChargeId
                    )
                );               
                if(Test.isRunningTest())  
                    integer intTest =1/0;
            }
            catch(Exception e) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeInvoiceCreate_Service',
                        'updateInvoice',
                        ChargeId,
                        e
                    )
                );
            }
        }
        
    }
    
}