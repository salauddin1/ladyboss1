@isTest
public class LiveChallangeBuyerBatchTest {
    @isTest
    public static void test1(){
        LIVE_Challenge_Customer__c lc1 = new LIVE_Challenge_Customer__c();
        lc1.Name = 'cus1';
        lc1.Email_of_Purchaser__c = 'test1@gmail.com';
        lc1.Classification__c = 'livechallenge';
        insert lc1;
        LIVE_Challenge_Customer__c lc2 = new LIVE_Challenge_Customer__c();
        lc2.Name = 'cus2';
        lc2.Email_of_Purchaser__c = 'test2@gmail.com';
        lc2.Classification__c = 'FreeInviteeLC';
        insert lc2;
        LiveChallangeBuyerBatch cb = new LiveChallangeBuyerBatch();
        Database.executeBatch(cb);
        String cronExp = '0 0 0 15 3 ? 2022';
        System.schedule('jobName', cronExp, cb);
    }
}