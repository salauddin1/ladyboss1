/*
Developer name : Tirth Patel
Discription : This batch creates payments records and commission records for the opportunityLineItems filered from the condition given in constructor. It creates refund commission 
records also if opportunityLineItems are refunded.Always keep batch size as one and delete all existing payment and commission records related to opportunities of opportunityLineItems
for which batch is going to be executed. 
*/ 
global class PaymentToCommissionBatch implements  Database.Batchable<sObject>, Database.Stateful {
    String whereCon;
    public PaymentToCommissionBatch(String whereCon){
        this.whereCon = whereCon;
    }
    //Query all OpportunityLineItems 
    global Database.QueryLocator start(Database.BatchableContext bc) {
            System.debug('whereCon : '+whereCon);
            return Database.getQueryLocator(' SELECT Id,Total_Commissionable_amount__c,Opportunity.Refunded_Date__c,Subscription_Id__c,stripe_charge_id__c,refund__c,Opportunity.User_to_give_credit__c,Opportunity.recordType.name,Opportunity.Sales_Person_Id__c,Opportunity.ownerId,opportunity.week__c,Opportunity.AgentInitializingSale__c,Product2Id,current_commissionable_amount__c,TotalPrice,opportunityId,opportunity.Stripe_Charge_Id__c,opportunity.Scheduled_Payment_Date__c,opportunity.createdDate   FROM OpportunityLineItem where '+whereCon);
    }
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> oliList){
        System.debug('oliList : '+oliList);
        Map<id,Payment__c> payDeleteMap = new Map<id,Payment__c>([SELECT id FROM Payment__c Where Opportunity__c =: oliList.get(0).OpportunityId and createdNow__c=false]);
        if(payDeleteMap.size()>0){
        	List<Comission__c> comDelList = [Select id From Comission__c Where Payment__c in : payDeleteMap.keyset()];
            if(comDelList.size()>0){
            	delete comDelList;
            }
        }
        if(payDeleteMap.values().size()>0){
            delete payDeleteMap.values();
        }
        List<Payment__c> payList = new List<Payment__c>();
        if((oliList.get(0).Opportunity.recordType.name=='charge')||(oliList.get(0).Opportunity.recordType.name=='Subscription' && (oliList.get(0).stripe_charge_id__c!=null || oliList.get(0).Subscription_Id__c !=null))){
            Payment__c pay = new Payment__c();
            pay.Opportunity__c = oliList.get(0).OpportunityId;
            pay.Amount__c = oliList.get(0).Total_Commissionable_amount__c;
            pay.Actual_amount__c = oliList.get(0).TotalPrice;
            pay.Product__c = oliList.get(0).Product2Id;
            pay.createdNow__c = true;
            pay.week__c = oliList.get(0).opportunity.week__c;
            pay.Payment_Created_By__c = oliList.get(0).Opportunity.Sales_Person_Id__c;
            payList.add(pay);
            if(oliList.get(0).current_commissionable_amount__c!=0){
                Payment__c pay1 = new Payment__c();
                pay1.Opportunity__c = oliList.get(0).OpportunityId;
                pay1.Amount__c = oliList.get(0).current_commissionable_amount__c*(-1);
                pay1.Actual_amount__c = oliList.get(0).refund__c*(-1);
                pay1.Product__c = oliList.get(0).Product2Id;
                pay1.createdNow__c = true;
                pay1.week__c = oliList.get(0).opportunity.week__c;
                pay1.Payment_Created_By__c = oliList.get(0).Opportunity.Sales_Person_Id__c;
                payList.add(pay1);
            }
            PaymentToCommissionTriggerFalg.runPayTOcomTrigger = false;
            try{
                insert payList;
                createCommission(payList.get(0),oliList.get(0));   
                if(payList.size()>1){
                    createCommission(payList.get(1),oliList.get(0));
                }
            }catch(Exception e){
                System.debug(e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'PaymentToCommissionBatch',
                        'execute',
                        null,
                        e
                    )
                );
            }
            
        }
    }    
    global void finish(Database.BatchableContext bc){
        UpdatePaymentBatch paybtch = new UpdatePaymentBatch();
        Database.executeBatch(paybtch);
    }
    //Creates Commission records 
    public static void createCommission(Payment__c pay,OpportunityLineItem oli){
        List<Comission__c> comList = new List<Comission__c>();
        Map<id,UserHierarchy__c> userHieMap = new Map<id,UserHierarchy__c>();
        Comission_Configuration__c emptyConfig = new Comission_Configuration__c();
        Map<id,Comission_Configuration__c> config_with_product_Map = new Map<id,Comission_Configuration__c>();
        for(Comission_Configuration__c comconfig : [SELECT id,Product__c,Amount_Paid_to_Agent_Finalizing_Sale__c,Amount_Paid_to_Agent_Initiating_Sale__c ,Amount_Paid_to_Director__c,Amount_Paid_to_Vice_President__c,Amount_Paid_to_Senior_Agent__c,Agent_paid_on__c,Director_paid_on__c,Vp_paid_on__c,Special_agent_paid_on__c,Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,Percentage_Paid_to_Senior_Agent__c,Percentage_Paid_to_Vice_President__c,For_First_Sell__c FROM Comission_Configuration__c WHERE Product__c!=null]){
            if(comconfig.Product__c!=null){
                config_with_product_Map.put(comconfig.Product__c,comconfig);
            }
        }
        Map<Decimal,Comission_Configuration__c> maxValAndComMap = PaymentToCommissionHelper.getTotalCommissionConfiguration();
        Map<Decimal,Comission_Configuration__c> firstSellComMap = PaymentToCommissionHelper.getFsConfiguration();
        List<Id> userIdList = new List<Id>();
        for(UserHierarchy__c userhierarki : [select id,Agent__c,Agent__r.id,Director__c,Director__r.id,Senior_Agent__c,Senior_Agent__r.id,Vice_President__c,Vice_President__r.id from UserHierarchy__c where Agent__c =: pay.Payment_Created_By__c] ){
            userHieMap.put(userhierarki.Agent__r.id,userhierarki);
            userIdList.add(userhierarki.Agent__c);
            userIdList.add(userhierarki.Director__c);
            userIdList.add(userhierarki.Vice_President__c);
            userIdList.add(userhierarki.Senior_Agent__c);
        }
        if(userHieMap.size()==0){
            return ;
        } 
        Date WeekDate;
        if(oli.opportunity.Scheduled_Payment_Date__c!=null){
            WeekDate = oli.opportunity.Scheduled_Payment_Date__c;
        }else{
            WeekDate = date.newinstance(oli.opportunity.createdDate.year(), oli.opportunity.createdDate.month(), oli.opportunity.createdDate.day());
        }
        UserHierarchy__c userhie = userHieMap.get(pay.Payment_Created_By__c);
        if(userhie == null){
            return;
        }
        
        Map<id,User> specialcomMap = new Map<id,User>();
        for(User ur : [SELECT id,special_com_as_agent__c,IsActive,special_com_as_director__c,special_com_as_senior_agent__c,special_com_as_vp__c FROM USER WHERE id in : userIdList ]){
            specialcomMap.put(ur.id,ur);
        }
                
        List<Payment__c> payList2 = [Select id,split_agent_payment__c,refund_week__c,is_First_Sell__c  from Payment__c where id=:pay.id];

        if(payList2.get(0).is_First_Sell__c == true && !config_with_product_Map.keyset().contains(pay.Product__c)){
            Decimal totalPay = PaymentToCommissionHelper.getFsTotal(pay.Payment_Created_By__c,config_with_product_Map.keyset(),pay.week__c);
            Comission_Configuration__c com = PaymentToCommissionHelper.getCommissionConfiguration(totalPay, firstSellComMap); 
            if(!payList2.get(0).split_agent_payment__c){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Agent',com.Percentage_Paid_to_Agent_Making_Sell__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent',com.Percentage_Paid_to_Agent_Making_Sell__c);
                }
            }else{
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200),pay.week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200),pay.week__c,System.today(),true,comList,com,'Agent With Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                }else{
                    PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Percentage_Paid_to_Agent_Making_Sell__c*(pay.Amount__c/200),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent With Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);  
                }
            } 
            if(userhie.Director__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,com.Percentage_Paid_to_Director__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Director',com.Percentage_Paid_to_Director__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,com.Percentage_Paid_to_Director__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Director',com.Percentage_Paid_to_Director__c);
                }
            }
            if(userhie.Vice_President__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Percentage_Paid_to_Vice_President__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Vice President',com.Percentage_Paid_to_Vice_President__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Percentage_Paid_to_Vice_President__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Vice President',com.Percentage_Paid_to_Vice_President__c);   
                }
            }
            if(!payList2.get(0).split_agent_payment__c){
                if(pay.Actual_amount__c>0){
                    if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    	PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,com.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Senior Agent',com.Percentage_Paid_to_Senior_Agent__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Senior Agent Com. Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                    }
                }else{
                    if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    	PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,com.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent',com.Percentage_Paid_to_Senior_Agent__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent Com. Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                    }
                }
            }else{
                if(pay.Actual_amount__c>0){
                    if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP',com.Percentage_Paid_to_Senior_Agent__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100),pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                    }
                }else{
                    if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Percentage_Paid_to_Senior_Agent__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent Com. To VP',com.Percentage_Paid_to_Senior_Agent__c);
                    }else{
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c*(pay.Amount__c/100),payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent Com. To VP Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);    
                    }
                }
            }
        }
        else if(config_with_product_Map.get(pay.Product__c)!=null){
            Comission_Configuration__c com = config_with_product_Map.get(pay.Product__c);
            if(oli.Opportunity.AgentInitializingSale__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(oli.Opportunity.AgentInitializingSale__c,pay.id,com.Amount_Paid_to_Agent_Initiating_Sale__c,com.Agent_paid_on__c,WeekDate,false,comList,com,'Initializing Agent',com.Amount_Paid_to_Agent_Initiating_Sale__c);
                }else{
                    PaymentToCommissionHelper.insertComission(oli.Opportunity.AgentInitializingSale__c,pay.id,com.Amount_Paid_to_Agent_Initiating_Sale__c*(-1),com.Agent_paid_on__c,oli.Opportunity.Refunded_Date__c,false,comList,com,'Initializing Agent',com.Amount_Paid_to_Agent_Initiating_Sale__c);
                }
            }
            if(userhie.Director__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,com.Amount_Paid_to_Director__c,com.Director_paid_on__c,WeekDate,false,comList,com,'Director',com.Amount_Paid_to_Director__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,com.Amount_Paid_to_Director__c*(-1),com.Director_paid_on__c,oli.Opportunity.Refunded_Date__c,false,comList,com,'Director',com.Amount_Paid_to_Director__c);
                }
            }
            if(userhie.Vice_President__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Amount_Paid_to_Vice_President__c,com.Vp_paid_on__c,WeekDate,false,comList,com,'Vice President',com.Amount_Paid_to_Vice_President__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,com.Amount_Paid_to_Vice_President__c*(-1),com.Vp_paid_on__c,oli.Opportunity.Refunded_Date__c,false,comList,com,'Vice President',com.Amount_Paid_to_Vice_President__c);
                }
            }
            if(userhie.Senior_Agent__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,com.Amount_Paid_to_Senior_Agent__c,com.Special_agent_paid_on__c,WeekDate,false,comList,com,'Senior Agent',com.Amount_Paid_to_Senior_Agent__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,com.Amount_Paid_to_Senior_Agent__c*(-1),com.Special_agent_paid_on__c,oli.Opportunity.Refunded_Date__c,false,comList,com,'Senior Agent',com.Amount_Paid_to_Senior_Agent__c);
                }
            }
            if(userhie.Agent__c!=null){
                if(pay.Actual_amount__c>0){
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Amount_Paid_to_Agent_Finalizing_Sale__c,com.Agent_paid_on__c,WeekDate,false,comList,com,'Finalizing Agent',com.Amount_Paid_to_Agent_Finalizing_Sale__c);
                }else{
                    PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,com.Amount_Paid_to_Agent_Finalizing_Sale__c*(-1),com.Agent_paid_on__c,oli.Opportunity.Refunded_Date__c,false,comList,com,'Finalizing Agent',com.Amount_Paid_to_Agent_Finalizing_Sale__c);
                }
            }
        }
        else{
            Decimal totalPay = PaymentToCommissionHelper.getTotalCommission(pay.Payment_Created_By__c,config_with_product_Map.keyset(),pay.week__c);
            Comission_Configuration__c com = PaymentToCommissionHelper.getCommissionConfiguration(totalPay, maxValAndComMap); 
            if(userhie.Agent__c!=null){
                if(!payList2.get(0).split_agent_payment__c){
                    if(pay.Actual_amount__c>0){
                        if(specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==null || specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==0){
                            if(specialcomMap.get(userhie.Agent__c).IsActive == true){
                            	PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/100,pay.week__c,System.today(),true,comList,com,'Agent',com.Percentage_Paid_to_Agent_Making_Sell__c);
                            }else{
                                PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/100,pay.week__c,System.today(),true,comList,com,'Agent Com. To VP',com.Percentage_Paid_to_Agent_Making_Sell__c);
                            }
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/100,pay.week__c,System.today(),true,comList,com,'Agent Com. Special',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c);
                        }
                    }else{
                        if(specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==null || specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==0){
                            if(specialcomMap.get(userhie.Agent__c).IsActive == true){
                            	PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent',com.Percentage_Paid_to_Agent_Making_Sell__c);
                            }else{
                                PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent Com. To VP',com.Percentage_Paid_to_Agent_Making_Sell__c);
                            }
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent Com. Special',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c);
                        }
                        
                    }
                }else{
                    if(pay.Actual_amount__c>0){
                        if(specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==null || specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==0){
                            if(specialcomMap.get(userhie.Agent__c).IsActive == true){
                            	PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,pay.week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            	PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,pay.week__c,System.today(),true,comList,com,'Agent With Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            }else{
                                PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,pay.week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            	PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,pay.week__c,System.today(),true,comList,com,'Agent With Credit User Com. to VP',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            }
                        }else{
                            PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/200,pay.week__c,System.today(),true,comList,com,'Credit User Com. Special',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/2);
                            PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/200,pay.week__c,System.today(),true,comList,com,'Agent With Credit User Com. Special',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/2);
                        }
                    }else{
                        if(specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==null || specialcomMap.get(userhie.Agent__c).special_com_as_agent__c==0){
                            if(specialcomMap.get(userhie.Agent__c).IsActive == true){
                            	PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            	PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent With Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            }else{
                                PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Credit User',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            	PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Agent_Making_Sell__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent with Credit User Com To VP',com.Percentage_Paid_to_Agent_Making_Sell__c/2);
                            }
                        }else{
                            PaymentToCommissionHelper.insertComission(oli.Opportunity.User_to_give_credit__c,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Credit User Com. Special',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/2);
                            PaymentToCommissionHelper.insertComission(userhie.Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/200,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Agent with Credit User Com To VP',specialcomMap.get(userhie.Agent__c).special_com_as_agent__c/2);
                        }
                    }
                }
            }
            if(userhie.Director__c!=null){
                if(pay.Actual_amount__c>0){
                    if(specialcomMap.get(userhie.Director__c).special_com_as_director__c==null || specialcomMap.get(userhie.Director__c).special_com_as_director__c==0){
                        PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Director__c/100,pay.week__c,System.today(),true,comList,com,'Director',com.Percentage_Paid_to_Director__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Director__c).special_com_as_director__c/100,pay.week__c,System.today(),true,comList,com,'Director Com. Special',specialcomMap.get(userhie.Director__c).special_com_as_director__c);
                    }
                }else{
                    if(specialcomMap.get(userhie.Director__c).special_com_as_director__c==null || specialcomMap.get(userhie.Director__c).special_com_as_director__c==0){
                        PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Director__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Director',com.Percentage_Paid_to_Director__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Director__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Director__c).special_com_as_director__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Director Com. Special',specialcomMap.get(userhie.Director__c).special_com_as_director__c);
                    }
                }
            }
            if(userhie.Vice_President__c!=null){
                if(pay.Actual_amount__c>0){
                    if(specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c==null || specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c==0){
                        PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Vice_President__c/100,pay.week__c,System.today(),true,comList,com,'Vice President',com.Percentage_Paid_to_Vice_President__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c/100,pay.week__c,System.today(),true,comList,com,'Vice President Com. Special',specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c);
                    }
                }else{
                    if(specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c==null || specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c==0){
                        PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Vice_President__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Vice President',com.Percentage_Paid_to_Vice_President__c);
                    }else{
                        PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Vice President',specialcomMap.get(userhie.Vice_President__c).special_com_as_vp__c);
                    }
                }
            }
            if(userhie.Senior_Agent__c!=null){
                if(!payList2.get(0).split_agent_payment__c){
                    if(pay.Actual_amount__c>0){
                        if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                            PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Senior_Agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent',com.Percentage_Paid_to_Senior_Agent__c);
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent Com. Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                        }
                    }else{
                        if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                            PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Senior_Agent__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent',com.Percentage_Paid_to_Senior_Agent__c);
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Senior_Agent__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c/100,payList2.get(0).refund_week__c,System.today(),true,comList,com,'Senior Agent Com. Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                        }
                    }
                }else{
                    if(pay.Actual_amount__c>0){
                        if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                            PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Senior_Agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP',com.Percentage_Paid_to_Senior_Agent__c);   
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c); 
                        }
                    }else{
                        if(specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==null || specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c==0){
                            PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*com.Percentage_Paid_to_Senior_Agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP',com.Percentage_Paid_to_Senior_Agent__c);    
                        }else{
                            PaymentToCommissionHelper.insertComission(userhie.Vice_President__r.id,pay.id,pay.Amount__c*specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c/100,pay.week__c,System.today(),true,comList,com,'Senior Agent Com. To VP Special',specialcomMap.get(userhie.Senior_Agent__c).special_com_as_senior_agent__c);
                        }
                    }
                }
            }
        }           
        insert comList;
        System.debug('comList : '+comList);
    }    
}