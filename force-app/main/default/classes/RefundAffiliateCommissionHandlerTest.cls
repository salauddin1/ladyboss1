@isTest
public class RefundAffiliateCommissionHandlerTest {
    public static testMethod void RefundAffiliateCommission(){
        Test.setMock(HttpCalloutMock.class, new RefundAffiliateCommissionHandlerTestMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        insert pa;
        
        Contact ct = new Contact();
        ct.LastName = 'test';
        ct.PAPUserId__c = 'test';
        insert ct;
        List<Pap_Commission__c> papToInsert = new List<Pap_Commission__c>();        
        
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.Contact__c = ct.id;
       // pap.Opportunity__c = opp.Id;
        pap.commission__c = -10;
        pap.Commission_Type__c = 'refund';
        pap.Campaign_Id__c = '11111111';
        pap.Campaign_name__c = 'test';
        pap.affiliate_ref_id__c = 'test';
        pap.order_id__c = 'test';
        pap.product_id__c = 'test';
        pap.data1__c = 'test';
        pap.data2__c = 'test';
        pap.data4__c = 'test';
        papToInsert.add(pap);
        
        
        insert papToInsert;
        Set<String> commissionIdSet = new Set<String>();
        commissionIdSet.add(papToInsert[0].id);
        
        test.startTest();
        RefundAffiliateCommissionHandler.updateAffiliateCommission(commissionIdSet);
        test.stopTest();
        
    }
    
}