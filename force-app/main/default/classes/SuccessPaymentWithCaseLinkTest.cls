@isTest
public class SuccessPaymentWithCaseLinkTest {
    
    public static TestMethod void testGetCallout(){
    	Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        Test.startTest(); 
        Triggers_activation__c wc = new Triggers_activation__c();
        wc.SuccessPaymentWithCaseLink__c =true;
        insert wc;
        contact co = new contact();
        co.LastName ='test';
        co.email='test@test.com';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        case cs = new case();
        cs.Stripe_Customer_Id__c = 'cus_DnqJ74SW8eLIZ2';
        cs.type='Account Recovery';
        cs.contactId = co.id;
        cs.IsFailed_Payment__c =true;
        insert cs;
        Id SubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity oppSub = new Opportunity();
        oppSub.Name = 'opp0' ;
        oppSub.RecordTypeId = SubRecordTypeId;
        oppSub.CloseDate = Date.Today();
        oppSub.StageName = 'Pending';
       oppSub.CustomerID__c = 'cus_DnqJ74SW8eLIZ2';
        oppSub.Stripe_Charge_Id__c = '123444';
        oppSub.Contact__c = Co.Id;
        insert oppSub;
       
         
        
        
        Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout2(){
    	Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Name = 'Async Limit Left';
        ap.Remaining__c = 0;
        insert ap;
        Test.startTest(); 
        Triggers_activation__c wc = new Triggers_activation__c();
        wc.SuccessPaymentWithCaseLink__c =true;
        insert wc;
        contact co = new contact();
        co.LastName ='test';
        co.email='test@test.com';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        case cs = new case();
        cs.Stripe_Customer_Id__c = 'cus_DnqJ74SW8eLIZ2';
        cs.type='Account Recovery';
        cs.contactId = co.id;
        cs.IsFailed_Payment__c =true;
        cs.Is_Coaching_Failed__c = true;
        cs.Failed_Amount__c = 44;
        cs.Statement_Descriptor__c = 'test';
        insert cs;
        Id SubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity oppSub = new Opportunity();
        oppSub.Name = 'opp0' ;
        oppSub.RecordTypeId = SubRecordTypeId;
        oppSub.CloseDate = Date.Today();
        oppSub.StageName = 'Pending';
       oppSub.CustomerID__c = 'cus_DnqJ74SW8eLIZ2';
        oppSub.Stripe_Charge_Id__c = '123444';
        oppSub.Contact__c = Co.Id;
        oppSub.Amount = 44;
        insert oppSub;
       
         
        
        
        Test.stopTest(); 
        
    }
 }