global class StripeCharge {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/charges';
    
    global Integer amount;
    global Integer created;
    global String stripeCurrency;
    global String customer_id;
    global StripeCustomer customer;
    global String description;
    global Boolean disputed;
    global Integer fee;
    global String id;
    global Boolean livemode;
    global String stripeObject;
    global Boolean paid;
    global Boolean refunded;
    global Integer amount_refunded;
    global StripeCard card;
    global List<Fee_details> fee_details;
    global StripeChargeError error;
    global String invoice_id;
    //global StripeInvoice invoice;
    global String failure_message;
    global Map<String, String> metadata;
    
    // This has changed in the 2013-02-13 API
    // https://stripe.com/docs/upgrades#2013-02-13
    // And more recently on 2013-08-13
    // https://stripe.com/docs/upgrades#2013-08-13
    global class Fee_details {
        global String stripeType;
        global String description;
        global Object application;
        global Integer amount;
        global String stripeCurrency;
    }
    
    global static StripeCharge getCharge(String chargeId) {
        return StripeCharge.getCharge(chargeId, false);
    }
    
    global static StripeCharge getCharge(String chargeId, Boolean expandInvoice) {
        HttpRequest http = new HttpRequest();
        String endPoint = SERVICE_URL+'/'+chargeId;
        if (expandInvoice) endPoint += '?expand[]=invoice';
        System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge.getCharge endpoint: '+endPoint); 
        http.setEndpoint(endPoint);
        http.setMethod('GET');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            //hs.setBody(StripeChargeTests.testData_getCharge);
            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        
        try {
            StripeCharge o = StripeCharge.parse(response);
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** '+e); 
            return null;
        }
    }
    
    global static StripeCharge create(String customerId, Decimal amount,String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson) {
        Map<String, String>  properties =new Map<String, String>();
        properties.put('description',prodDescription);
        if(stripStatementDescriptor!=null){
            properties.put('stripStatementDescriptor',stripStatementDescriptor);
        }
        
        system.debug('-----------------------------'+properties.get('stripStatementDescriptor'));
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
    
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
    
        system.debug('metadata'+metadata);
        return create(customerId, amount, properties,metadata);
    }

    global static StripeCharge create(String customerId, Decimal amount,String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson,Decimal taxPercentage) {
        Map<String, String>  properties =new Map<String, String>();
        Decimal tax = amount*taxPercentage/100;
        properties.put('description',prodDescription);
        if(stripStatementDescriptor!=null){
            properties.put('stripStatementDescriptor',stripStatementDescriptor);
        }
        
        system.debug('-----------------------------'+properties.get('stripStatementDescriptor'));
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        
        metadata.put('Tax',String.valueOf(tax.setScale(2)));
        metadata.put('TaxPercentage',String.valueOf(taxPercentage));
        metadata.put('Core Amount',String.valueOf(amount));
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);   
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
                 
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
    
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
    
        system.debug('metadata'+metadata);
        amount += tax;
        return create(customerId, amount, properties,metadata);
    }

    global static StripeCharge create(String customerId, Decimal amount,String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson,Decimal taxPercentage, Decimal shippingCost) {
        if(shippingCost == null){
            shippingCost = 0.00;
        }
        
        Map<String, String>  properties =new Map<String, String>();
        Decimal tax = amount*taxPercentage/100;
        properties.put('description',prodDescription);
        if(stripStatementDescriptor!=null){
            properties.put('stripStatementDescriptor',stripStatementDescriptor);
        }
        system.debug('-----------------------------'+properties.get('stripStatementDescriptor'));
        Map<String,String> metadata = new Map<String,String>();
        
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        metadata.put('Tax',String.valueOf(tax.setScale(2)));
        metadata.put('TaxPercentage',String.valueOf(taxPercentage));
        metadata.put('Shipping Cost',String.valueOf(shippingCost.setScale(2)));
        metadata.put('Core Amount',String.valueOf(amount));
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
    
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
    
        system.debug('metadata'+metadata);
        amount += tax + shippingCost;
        return create(customerId, amount, properties,metadata);
    }
    
    global static StripeCharge create(String customerId, Decimal amount,String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId) {
        Map<String, String>  properties =new Map<String, String>();
        properties.put('description',prodDescription);
        if(stripStatementDescriptor!=null){
            properties.put('stripStatementDescriptor',stripStatementDescriptor);
        }
        
        system.debug('-----------------------------'+properties.get('stripStatementDescriptor'));
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Shipping Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        metadata.put('Sales Person',UserInfo.getName());
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
        
        system.debug('metadata'+metadata);
        return create(customerId, amount, properties,metadata);
    }
    
    global static StripeCharge create(String customerId, Decimal amount, Map<String, String> properties, Map<String, String> metadata) {
        system.debug('customerId-->'+customerId);
        system.debug('amount-->'+amount);
        system.debug('properties-->'+properties);
        system.debug('metadata-->'+metadata);
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        
        Integer amount_int = Math.round(amount * 100);//.intValue();
        
        system.debug('----------properties---------------'+String.valueOf(properties.get('description')));
        
        Map<String, String> payload = new Map<String, String>{
            'customer' => customerId,
                'amount' => String.valueOf(amount_int),
                'description' => String.valueOf(properties.get('description'))
                
                };
                    
                    if(properties.containsKey('stripStatementDescriptor')&& properties.get('stripStatementDescriptor') !=null && properties.get('stripStatementDescriptor') !=''){
                        payload.put('statement_descriptor',properties.get('stripStatementDescriptor'));
                    }
        
        // set the default currency to USD if it's not specified
        if (properties == null || properties.containsKey('usd') == false) {
            payload.put('currency', 'usd');
        }
        
        if (properties != null) {
            for (String key : properties.keySet()) {
                if (properties.get(key) != null && key!='stripStatementDescriptor') {
                    payload.put(key, properties.get(key));
                }
            }
        }
        
        if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http.setBody(StripeUtil.urlify(payload));
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+http.getBody());    
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                system.debug('e.getMessage()-->'+e.getMessage());
                return null;
            }
        } else {
            //hs.setBody(StripeChargeTests.testData_createCharge);

            hs.setStatusCode(200);
        }
        
        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
        
        //For Testing error code
       // response = '{"error": {"charge": "ch_1DKNGIFzCf73siP0ypSsxVGt","code": "card_declined","decline_code": "insufficient_funds","doc_url": "https://stripe.com/docs/error-codes/card-declined","message": "Your card has insufficient funds.","type": "card_error"}}';

        try {
            StripeCharge o = StripeCharge.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCharge object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    
    public static StripeCharge parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (StripeCharge) System.JSON.deserialize(json, StripeCharge.class);
    }
    
}