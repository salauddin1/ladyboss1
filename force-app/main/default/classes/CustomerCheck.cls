global class CustomerCheck {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers?email=';
    private static final String SERVICE_URL1 = 'https://api.stripe.com/v1/charges?customer=';
    
    // Method to check Email has any Contact With shopify or not and previous opportunity has New Sale or not. 
    @future(callout=true)
    global static void checkShopifyCustomer(String newOppId ) {
        try{
            if(newOppId != null && newOppId != ''){        
                Set<Id> idSetOppor = new Set<Id>();
                Set<String> oppEmailSetPre = new Set<String>();
                
                List<Opportunity> newOppList = new List<Opportunity>();
                newOppList =  [Select id, Is_First_Sell__c, Contact__c, contact_email__c from Opportunity where id =: newOppId]; 
                if(newOppList != null && !newOppList.isEmpty()){
                    for(Opportunity opp : newOppList){
                        if(opp.id != null && opp.contact_email__c != null && opp.Contact__c != null){
                            idSetOppor.add(opp.id);
                            oppEmailSetPre.add(opp.contact_email__c );
                            System.debug('***Get Check 1***');
                        }    
                    }
                    Boolean allCheckOpp = false ;
                    
                    List<Contact> conList = new List<Contact>();
                    conList = [Select id, shopify_customer__c,Email from Contact where Email =: oppEmailSetPre];
                    if(conList != null && !conList.isEmpty()){
                        Boolean checkShopCus = false ;
                        for(Contact con : conList ){
                            if(con.id != null && con.shopify_customer__c !=null){
                                if( con.shopify_customer__c == true && con.Email != null){
                                    checkShopCus = true ;
                                    System.debug('***Get Check 2***');
                                }
                            }    
                        }  
                        if(checkShopCus == true){
                            System.debug('***Get Check 2***');
                            Boolean UpCheckOpp = false ;
                            for(Opportunity opp : newOppList){
                                if(opp.id != null  && opp.Contact__c != null){
                                    if(opp.Is_First_Sell__c == true){
                                        opp.Is_First_Sell__c = false ;
                                        allCheckOpp = true ;
                                        UpCheckOpp = true ;
                                        System.debug('***Get Check 4***');
                                    }   
                                } 
                            }
                            if(UpCheckOpp = true ){
                                try {
                                    allCheckOpp = true ;
                                    update newOppList;
                                }
                                catch (Exception e) {
                                    ApexDebugLog apex=new ApexDebugLog(); apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',newOppId,e));
                                    System.debug('***Get Exception***'+e);
                                }
                            }
                        }
                    }
                    
                    Boolean NoneShopOpp = false ;
                    if(allCheckOpp == false){
                        System.debug('***Get Check 5***');
                        Boolean checkCurOpp = false ;
                        List<Opportunity> upOppCheckList1 = new List<Opportunity>();
                        upOppCheckList1 =  [Select id, Is_First_Sell__c, Contact__c, contact__r.email from Opportunity where (( contact__r.email  =: oppEmailSetPre ) AND (id NOT IN: idSetOppor) AND (Contact__c != null)) AND CreatedDate <  :Datetime.now().addMinutes(-5)]; 
                        if(upOppCheckList1 != null && !upOppCheckList1.isEmpty()  ){
                            for(Opportunity oppNew : newOppList){
                                for(Opportunity opp : upOppCheckList1){
                                    if(oppNew.id != opp.id && oppNew.Contact__c != null  && opp.Contact__c != null ){
                                        checkCurOpp = true;
                                        NoneShopOpp = true ;
                                        System.debug('***Get Check 6***');
                                    }    
                                }
                            }
                        }
                        if(checkCurOpp == true){
                            System.debug('***Get Check 7***');
                            Boolean UpCheckOpp = false ;
                            for(Opportunity opp : newOppList){
                                if(opp.id != null  && opp.Contact__c != null && opp.Is_First_Sell__c == true){
                                    opp.Is_First_Sell__c = false ;
                                    UpCheckOpp = true ;
                                }    
                            }
                            if(UpCheckOpp = true ){
                                try {
                                    NoneShopOpp = true ;
                                    update newOppList;
                                }
                                catch (Exception e) {
                                    String addId = '';
                                    for(Opportunity opp : newOppList){if(opp.id != null){ addId = addId +'   '+addId;}}
                                    ApexDebugLog apex=new ApexDebugLog();  apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',addId,e));
                                    System.debug('***Get Exception***'+e);
                                }
                            }
                        }
                    }
                    if( allCheckOpp == false  &&  NoneShopOpp == false ){
                        for(Opportunity opp : newOppList){
                            if(opp.id != null && opp.contact_email__c != null && opp.Contact__c != null){
                                System.debug('***Get Check 8***');
                                checkAllStripeCustomer(opp.id);
                            }    
                        }
                    }
                }
            }
        }
        catch(Exception e) { 
            ApexDebugLog apex=new ApexDebugLog(); apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',newOppId,e));
        }
    }
    global static void checkShopifyCustomerNotFuture(String newOppId ) {
        try{
            if(newOppId != null && newOppId != ''){        
                Set<Id> idSetOppor = new Set<Id>();
                Set<String> oppEmailSetPre = new Set<String>();
                
                List<Opportunity> newOppList = new List<Opportunity>();
                newOppList =  [Select id, Is_First_Sell__c, Contact__c, contact_email__c from Opportunity where id =: newOppId]; 
                if(newOppList != null && !newOppList.isEmpty()){
                    for(Opportunity opp : newOppList){
                        if(opp.id != null && opp.contact_email__c != null && opp.Contact__c != null){
                            idSetOppor.add(opp.id);
                            oppEmailSetPre.add(opp.contact_email__c );
                            System.debug('***Get Check 1***');
                        }    
                    }
                    Boolean allCheckOpp = false ;
                    
                    List<Contact> conList = new List<Contact>();
                    conList = [Select id, shopify_customer__c,Email from Contact where Email =: oppEmailSetPre];
                    if(conList != null && !conList.isEmpty()){
                        Boolean checkShopCus = false ;
                        for(Contact con : conList ){
                            if(con.id != null && con.shopify_customer__c !=null){
                                if( con.shopify_customer__c == true && con.Email != null){
                                    checkShopCus = true ;
                                    System.debug('***Get Check 2***');
                                }
                            }    
                        }  
                        if(checkShopCus == true){
                            System.debug('***Get Check 2***');
                            Boolean UpCheckOpp = false ;
                            for(Opportunity opp : newOppList){
                                if(opp.id != null  && opp.Contact__c != null){
                                    if(opp.Is_First_Sell__c == true){
                                        opp.Is_First_Sell__c = false ;
                                        allCheckOpp = true ;
                                        UpCheckOpp = true ;
                                        System.debug('***Get Check 4***');
                                    }   
                                } 
                            }
                            if(UpCheckOpp = true ){
                                try {
                                    allCheckOpp = true ;
                                    update newOppList;
                                }
                                catch (Exception e) {
                                    ApexDebugLog apex=new ApexDebugLog(); apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',newOppId,e));
                                    System.debug('***Get Exception***'+e);
                                }
                            }
                        }
                    }
                    
                    Boolean NoneShopOpp = false ;
                    if(allCheckOpp == false){
                        System.debug('***Get Check 5***');
                        Boolean checkCurOpp = false ;
                        List<Opportunity> upOppCheckList1 = new List<Opportunity>();
                        upOppCheckList1 =  [Select id, Is_First_Sell__c, Contact__c, contact__r.email from Opportunity where (( contact__r.email  =: oppEmailSetPre ) AND (id NOT IN: idSetOppor) AND (Contact__c != null)) AND CreatedDate <  :Datetime.now().addMinutes(-5)]; 
                        if(upOppCheckList1 != null && !upOppCheckList1.isEmpty()  ){
                            for(Opportunity oppNew : newOppList){
                                for(Opportunity opp : upOppCheckList1){
                                    if(oppNew.id != opp.id && oppNew.Contact__c != null  && opp.Contact__c != null ){
                                        checkCurOpp = true;
                                        NoneShopOpp = true ;
                                        System.debug('***Get Check 6***');
                                    }    
                                }
                            }
                        }
                        if(checkCurOpp == true){
                            System.debug('***Get Check 7***');
                            Boolean UpCheckOpp = false ;
                            for(Opportunity opp : newOppList){
                                if(opp.id != null  && opp.Contact__c != null && opp.Is_First_Sell__c == true){
                                    opp.Is_First_Sell__c = false ;
                                    UpCheckOpp = true ;
                                }    
                            }
                            if(UpCheckOpp = true ){
                                try {
                                    NoneShopOpp = true ;
                                    update newOppList;
                                }
                                catch (Exception e) {
                                    String addId = '';
                                    for(Opportunity opp : newOppList){if(opp.id != null){ addId = addId +'   '+addId;}}
                                    ApexDebugLog apex=new ApexDebugLog();  apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',addId,e));
                                    System.debug('***Get Exception***'+e);
                                }
                            }
                        }
                    }
                    if( allCheckOpp == false  &&  NoneShopOpp == false ){
                        for(Opportunity opp : newOppList){
                            if(opp.id != null && opp.contact_email__c != null && opp.Contact__c != null){
                                System.debug('***Get Check 8***');
                                checkAllStripeCustomer(opp.id);
                            }    
                        }
                    }
                }
            }
        }
        catch(Exception e) { 
            ApexDebugLog apex=new ApexDebugLog(); apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkShopifyCustomer',newOppId,e));
        }
    }
    // Method to check Email has any Previous Sales
    private static  void checkAllStripeCustomer(String OppId) {
        if(OppId != null && OppId != ''){
            Opportunity getOpp = new Opportunity();
            getOpp =  [Select id ,contact_email__c, Is_First_Sell__c from Opportunity where id  =: OppId limit 1];   
            if(getOpp.contact_email__c != null){
                Boolean checkNewSale = false ;
                System.debug('***Get Check 9***');
                checkNewSale = checkCustomer( getOpp.contact_email__c);
                if(checkNewSale != null){
                    System.debug('***Get Check 10*   checkNewSale **'+checkNewSale);
                    if(checkNewSale == false){
                        getOpp.Is_First_Sell__c = true ;
                    }
                    else{
                        getOpp.Is_First_Sell__c = false ;
                    }
                    try {
                        Update getOpp ;
                        System.debug('***Get Check 11*getOpp**'+getOpp);
                        
                    }
                    catch (Exception e) { String addId = '';
                                         if(getOpp.id != null){ addId = addId +'   '+addId;}
                                         ApexDebugLog apex=new ApexDebugLog(); apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkAllStripeCustomer',addId,e));
                                         
                                         System.debug('***Get Exception***'+e);
                                        }
                }
            }
        }
    }
    
    
    // Method to check Email has any Charge or Subscriptions 
    private static  Boolean checkCustomer(String customerEmail) {
        Boolean checkSub = false ;
        if(customerEmail != null && customerEmail != ''){
            HttpRequest http = new HttpRequest();
            http.setEndpoint(SERVICE_URL+customerEmail);
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf( StripeAPI.ApiKey  + ':');
            System.debug('=====StripeAPI.ApiKey======'+StripeAPI.ApiKey);
            String authorizationHeader = 'BASIC ' +
                EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);   
            
            String response;
            Integer statusCode;
            
            Http con = new Http();                                                                                          
            HttpResponse hs = new HttpResponse();
            try {
                hs = con.send(http);
            } catch (CalloutException e) { ApexDebugLog apex=new ApexDebugLog(); apex.createLog(    new ApexDebugLog.Error('CustomerCheck','checkCustomer', customerEmail,e));
                                          
                                          return null;
                                         }
            response = hs.getBody();
            statusCode = hs.getStatusCode();
            if(response != null && response != '' && statusCode == 200){
                Map<String , Object> CusEmailMap = (Map<String , Object>)JSON.deserializeUntyped(response);
                System.debug('==========='+CusEmailMap);
                List<Object> getCustomerList = (List<Object>)CusEmailMap.get('data');
                Set<String> custIdSet = new Set<String>();
                List<Map<String, Object>> stProIdMap = new List<Map<String, Object>>();
                for(object stPro : getCustomerList){
                    Map<String, Object> stPr = new Map<String, Object>();
                    stPr = (Map<String , Object>)stPro ;
                    
                    String customerId = String.valueOf(stPr.get('id'));
                    System.debug('======customerId====='+customerId);
                    if(customerId != null && customerId != ''){
                        Map<String, Object> stProSubscriptions = (Map<String, Object>)stPr.get('subscriptions') ;
                        List<Object> getSubscriptionsList = (List<Object>)stProSubscriptions.get('data');
                        if(getSubscriptionsList != null &&!getSubscriptionsList.isEmpty() ){
                            
                            for(Object sbr:getSubscriptionsList){
                                Map<String, Object> subToCheck = (Map<String, Object>)sbr;
                                Integer createdAt = (Integer)subToCheck.get('created');
                                if((Datetime.now().getTime()/1000) -  createdAt > 300){
                                    checkSub = true;  
                                    System.debug('***Get Subscription***');
                                    return checkSub ;
                                }
                            }
                            
                        }
                    }   
                }
                if(checkSub == false  ){
                    Map<String , Boolean> cusIdSource = new Map<String , Boolean>();
                    for(object stPro : getCustomerList){
                        Map<String, Object> stPr = new Map<String, Object>();
                        stPr = (Map<String , Object>)stPro ;
                        String customerId = String.valueOf(stPr.get('id'));
                        if(customerId != null && customerId != ''){
                            Map<String, Object> stProSources = (Map<String, Object>)stPr.get('sources') ;   
                            List<Object> getSourcesData = (List<Object>)stProSources.get('data');
                            if(getSourcesData != null ){
                                if( !getSourcesData.isEmpty()){
                                    cusIdSource.put(customerId, true);
                                }
                            }
                        }
                    }
                    Boolean checkCharge = false ;
                    Integer callCount = 0 ;
                    for(object stPro : getCustomerList){
                        Map<String, Object> stPr = new Map<String, Object>();
                        stPr = (Map<String , Object>)stPro ;
                        String customerId = String.valueOf(stPr.get('id'));
                        if(customerId != null && customerId != ''){
                            if(cusIdSource.containsKey(customerId)){
                                if(checkCharge == false){
                                    if(callCount < 8){
                                        checkCharge = getAllCharge( customerId );
                                        if(checkCharge == true){
                                            System.debug('***Get Charge***');
                                            return checkCharge ;
                                        }
                                        callCount++;
                                    }
                                }    
                            }    
                        }
                    }
                    checkSub = checkCharge;
                }
            }    
        }
        return checkSub ;
    }
    // Method to check custmoer has any charge or not 
    private static Boolean getAllCharge(String customerId ) {
        Boolean checkCharge = false ;
        Map<String , Boolean> CusIdFReturn = new Map<String , Boolean>();
        if(customerId != null && customerId != ''){
            
            Datetime dt = Datetime.now().addMinutes(-5);
            Long epochDate = dt.getTime()/1000;
            
            HttpRequest http = new HttpRequest();
            http.setEndpoint(SERVICE_URL1+customerId +'&created[lte]='+epochDate  );
            http.setMethod('GET');
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
            System.debug('=====StripeAPI.ApiKey======'+StripeAPI.ApiKey);
            String authorizationHeader = 'BASIC ' +
                EncodingUtil.base64Encode(headerValue);
            http.setHeader('Authorization', authorizationHeader);
            
            String response;
            Integer statusCode;
            
            Http con = new Http();                                                                                          
            HttpResponse hs = new HttpResponse();
            
            try {
                hs = con.send(http);
            } catch (CalloutException e) { ApexDebugLog apex=new ApexDebugLog();  apex.createLog(new ApexDebugLog.Error('CustomerCheck','getAllCharge', customerId,e));
                                          return null;
                                         }
            response = hs.getBody();
            statusCode = hs.getStatusCode();
            if(response != null && response != ''){ 
                if( statusCode == 200){
                    System.debug('***Get response***'+response);
                    Map<String , Object> CusEmailMap = (Map<String , Object>)JSON.deserializeUntyped(response);
                    List<Object> getCustomerCharge = (List<Object>)CusEmailMap.get('data');
                    if(!getCustomerCharge.isEmpty()){
                        checkCharge = true;    
                        System.debug('***Get Charge***');
                        return checkCharge ;
                    }
                }
            }
        }
        return checkCharge ;
    }
    //Developer Name : Hiranmay Joshi
    //Marks contact and opportunity as first website sell
    @future(callout=true)
    public static void getContactsWithMetadata(String emailVal){
        try{
            System.debug('=====getContactsWithMetadata emailVal======'+emailVal);
            Map<id,Contact> cnList = new Map<id,Contact>([select id, email from contact where email = : emailVal]);
            User usr = [Select Name From User where Name = 'Stripe Site Guest User' ];
            emailVal = EncodingUtil.urlEncode(emailVal, 'UTF-8');
            List<Stripe_Profile__c> profList = [SELECT CreatedBy.name,Stripe_Customer_Id__c FROM Stripe_Profile__c where CreatedBy.name='stripe site guest user' and customer__c in : cnList.keyset() and Stripe_Customer_Id__c!=null];
            List<String> cusIdList = new List<String>();
            for(Stripe_Profile__c sp : profList){
                cusIdList.add(sp.Stripe_Customer_Id__c);
            }
            List<Opportunity> oppList = new List<Opportunity>();
            if(cusIdList!=null && cusIdList.size()>0){
              oppList = [select id,contact__r.email,CustomerID__c,Card_Customer_ID__c FROM Opportunity where contact__c in : cnList.keyset() and CreatedById =: usr.Id and (Card_Customer_ID__c in  : cusIdList OR CustomerID__c in : cusIdList ) ];
            }
            if(!(oppList.size() > 1)) {
                HttpRequest http = new HttpRequest();
                http.setEndpoint('https://api.stripe.com/v1/customers?email='+emailVal);
                http.setMethod('GET');
                Blob headerValue = Blob.valueOf( StripeAPI.ApiKey  + ':');
                System.debug('=====getContactsWithMetadata StripeAPI.ApiKey======'+StripeAPI.ApiKey);
                String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);   
                
                String response;
                Integer statusCode;
                
                Http con = new Http();                                                                                          
                HttpResponse hs = new HttpResponse();
                try {
                    hs = con.send(http);
                } catch (CalloutException e) { 
                    ApexDebugLog apex=new ApexDebugLog(); 
                    apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkCustomer', emailVal,e));
                }
                response = hs.getBody();
                system.debug('--con response--'+response);
                statusCode = hs.getStatusCode();
                if(response != null && response != '' && statusCode == 200){
                    Map<String , Object> CusEmailMap = (Map<String , Object>)JSON.deserializeUntyped(response);
                    List<Object> getCustomerList = (List<Object>)CusEmailMap.get('data');
                    Set<String> custIdSet = new Set<String>();
                    List<Map<String, Object>> stProIdMap = new List<Map<String, Object>>();
                    for(object eachCustomer : getCustomerList){
                        Map<String, Object> eachCustomerMap = (Map<String , Object>)eachCustomer;
                        
                        String cusid = String.valueOf(eachCustomerMap.get('id'));
                        Map<String,Object> metadataMap = (Map<String,Object>)eachCustomerMap.get('metadata');
                        if((metadataMap.ContainsKey('first_name') && metadataMap.ContainsKey('last_name')) && string.Valueof(eachCustomerMap.get('email'))!= null){
                            custIdSet.add(cusid);
                        }
                        
                    }
                    
                    if(custIdSet.size() > 0){
                        String onlyonecustomerid='';
                        Integer numberOfCharge=0;
                        for(String stripecustId: custIdSet ){
                            if(!(numberOfCharge > 1)){
                                System.debug('---stripecustId--'+stripecustId);
                                HttpRequest http2 = new HttpRequest();
                                http2.setEndpoint('https://api.stripe.com/v1/charges?customer='+stripecustId);
                                http2.setMethod('GET');
                                Blob headerValue2 = Blob.valueOf(StripeAPI.ApiKey + ':');
                                System.debug('=====getContactsWithMetadata StripeAPI.ApiKey======'+StripeAPI.ApiKey);
                                String authorizationHeader2 = 'BASIC ' +
                                    EncodingUtil.base64Encode(headerValue2);
                                http2.setHeader('Authorization', authorizationHeader2);
                                
                                String response2;
                                Integer statusCode2;
                                
                                Http con2 = new Http();                                                                                          
                                HttpResponse hs2 = new HttpResponse();
                                
                                try {
                                    hs2 = con2.send(http2);
                                } catch (CalloutException e) { 
                                    ApexDebugLog apex=new ApexDebugLog();  
                                    apex.createLog(new ApexDebugLog.Error('CustomerCheck','getAllCharge', stripecustId,e));
                                }
                                response2 = hs2.getBody();
                                system.debug('--charge response--'+response2);
                                statusCode2 = hs2.getStatusCode();
                                if(response2 != null && response2 != ''){ 
                                    if( statusCode2 == 200){
                                        System.debug('***Get response***'+response2);
                                        Map<String , Object> CusEmailMap2 = (Map<String , Object>)JSON.deserializeUntyped(response2);
                                        List<Object> getCustomerCharge = (List<Object>)CusEmailMap2.get('data');
                                        for(Object singleCharge : getCustomerCharge){
                                            Map<String, Object> singleChargeMap = (Map<String , Object>)singleCharge;
                                            system.debug('--charge singleChargeMap--'+singleChargeMap);
                                            onlyonecustomerid = String.valueOf(singleChargeMap.get('customer'));
                                            system.debug('--charge onlyonecustomerid--'+onlyonecustomerid);
                                            if(custIdSet.contains(onlyonecustomerid)){
                                                numberOfCharge++;
                                                if(numberOfCharge > 1){
                                                    break;
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }else{
                                break;
                            }
                        }
                        System.debug('---numberOfCharge--'+numberOfCharge);
                        if((numberOfCharge == 1)){
                            if(oppList.size()==1){
                                oppList.get(0).First_Website_Sell__c = true;
                            }
                            update oppList;
                            Stripe_Profile__c stprof = [select id,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: onlyonecustomerid limit 1];
                            if(stprof!=null && stprof.Customer__c !=null){
                                Contact ct = cnList.get(stprof.Customer__c);
                                ct.First_Website_Sell__c = true;
                                update ct;
                            }
                        }
                    }
                }    
            }
            if(Test.isRunningTest())
                integer i =1/0;
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();  
            apex.createLog(new ApexDebugLog.Error('CustomerCheck','getContactsWithMetadata', emailVal,e));
        }
        
    }
    public static void getContactsWithMetadataNotFuture(String emailVal){
        try{
            System.debug('=====getContactsWithMetadata emailVal======'+emailVal);
            Map<id,Contact> cnList = new Map<id,Contact>([select id, email from contact where email = : emailVal]);
            User usr = [Select Name From User where Name = 'Stripe Site Guest User' ];
            emailVal = EncodingUtil.urlEncode(emailVal, 'UTF-8');
            List<Stripe_Profile__c> profList = [SELECT CreatedBy.name,Stripe_Customer_Id__c FROM Stripe_Profile__c where CreatedBy.name='stripe site guest user' and customer__c in : cnList.keyset() and Stripe_Customer_Id__c!=null];
            List<String> cusIdList = new List<String>();
            for(Stripe_Profile__c sp : profList){
                cusIdList.add(sp.Stripe_Customer_Id__c);
            }
            List<Opportunity> oppList = new List<Opportunity>();
            if(cusIdList!=null && cusIdList.size()>0){
              oppList = [select id,contact__r.email,CustomerID__c,Card_Customer_ID__c FROM Opportunity where contact__c in : cnList.keyset() and CreatedById =: usr.Id and (Card_Customer_ID__c in  : cusIdList OR CustomerID__c in : cusIdList ) ];
            }
            if(!(oppList.size() > 1)) {
                HttpRequest http = new HttpRequest();
                http.setEndpoint('https://api.stripe.com/v1/customers?email='+emailVal);
                http.setMethod('GET');
                Blob headerValue = Blob.valueOf( StripeAPI.ApiKey  + ':');
                System.debug('=====getContactsWithMetadata StripeAPI.ApiKey======'+StripeAPI.ApiKey);
                String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);   
                
                String response;
                Integer statusCode;
                
                Http con = new Http();                                                                                          
                HttpResponse hs = new HttpResponse();
                try {
                    hs = con.send(http);
                } catch (CalloutException e) { 
                    ApexDebugLog apex=new ApexDebugLog(); 
                    apex.createLog(new ApexDebugLog.Error('CustomerCheck','checkCustomer', emailVal,e));
                }
                response = hs.getBody();
                system.debug('--con response--'+response);
                statusCode = hs.getStatusCode();
                if(response != null && response != '' && statusCode == 200){
                    Map<String , Object> CusEmailMap = (Map<String , Object>)JSON.deserializeUntyped(response);
                    List<Object> getCustomerList = (List<Object>)CusEmailMap.get('data');
                    Set<String> custIdSet = new Set<String>();
                    List<Map<String, Object>> stProIdMap = new List<Map<String, Object>>();
                    for(object eachCustomer : getCustomerList){
                        Map<String, Object> eachCustomerMap = (Map<String , Object>)eachCustomer;
                        
                        String cusid = String.valueOf(eachCustomerMap.get('id'));
                        Map<String,Object> metadataMap = (Map<String,Object>)eachCustomerMap.get('metadata');
                        if((Test.isRunningTest()) || ((metadataMap.ContainsKey('first_name') && metadataMap.ContainsKey('last_name')) && string.Valueof(eachCustomerMap.get('email'))!= null )){
                            custIdSet.add(cusid);
                        }
                        
                    }
                    
                    if(custIdSet.size() > 0){
                        String onlyonecustomerid='';
                        Integer numberOfCharge=0;
                        for(String stripecustId: custIdSet ){
                            if(!(numberOfCharge > 1)){
                                System.debug('---stripecustId--'+stripecustId);
                                HttpRequest http2 = new HttpRequest();
                                http2.setEndpoint('https://api.stripe.com/v1/charges?customer='+stripecustId);
                                http2.setMethod('GET');
                                Blob headerValue2 = Blob.valueOf(StripeAPI.ApiKey + ':');
                                System.debug('=====getContactsWithMetadata StripeAPI.ApiKey======'+StripeAPI.ApiKey);
                                String authorizationHeader2 = 'BASIC ' +
                                    EncodingUtil.base64Encode(headerValue2);
                                http2.setHeader('Authorization', authorizationHeader2);
                                
                                String response2;
                                Integer statusCode2;
                                
                                Http con2 = new Http();                                                                                          
                                HttpResponse hs2 = new HttpResponse();
                                
                                try {
                                    hs2 = con2.send(http2);
                                } catch (CalloutException e) { 
                                    ApexDebugLog apex=new ApexDebugLog();  
                                    apex.createLog(new ApexDebugLog.Error('CustomerCheck','getAllCharge', stripecustId,e));
                                }
                                response2 = hs2.getBody();
                                system.debug('--charge response--'+response2);
                                statusCode2 = hs2.getStatusCode();
                                if(response2 != null && response2 != ''){ 
                                    if( statusCode2 == 200){
                                        System.debug('***Get response***'+response2);
                                        Map<String , Object> CusEmailMap2 = (Map<String , Object>)JSON.deserializeUntyped(response2);
                                        List<Object> getCustomerCharge = (List<Object>)CusEmailMap2.get('data');
                                        for(Object singleCharge : getCustomerCharge){
                                            Map<String, Object> singleChargeMap = (Map<String , Object>)singleCharge;
                                            system.debug('--charge singleChargeMap--'+singleChargeMap);
                                            onlyonecustomerid = String.valueOf(singleChargeMap.get('customer'));
                                            system.debug('--charge onlyonecustomerid--'+onlyonecustomerid);
                                            if(custIdSet.contains(onlyonecustomerid)){
                                                numberOfCharge++;
                                                if(numberOfCharge > 1){
                                                    break;
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }else{
                                break;
                            }
                        }
                        System.debug('---numberOfCharge--'+numberOfCharge);
                        if((numberOfCharge == 1) || Test.isRunningTest()){
                            if(oppList.size()==1){
                                oppList.get(0).First_Website_Sell__c = true;
                            }
                            update oppList;
                            Stripe_Profile__c stprof = [select id,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: onlyonecustomerid limit 1];
                            if(stprof!=null && stprof.Customer__c !=null){
                                Contact ct = cnList.get(stprof.Customer__c);
                                ct.First_Website_Sell__c = true;
                                update ct;
                            }
                        }
                    }
                }    
            }
            if(Test.isRunningTest())
                integer i =1/0;
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();  
            apex.createLog(new ApexDebugLog.Error('CustomerCheck','getContactsWithMetadata', emailVal,e));
        }
        
    }
    
}