global  class Invoices {
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/invoices';
    global String id;
    global String subscription;
    global error error;
    global static Integer getInvoice(String subId){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'?status=paid&limit=100&subscription='+subId);
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return 0;
            }
        } else {
            hs.setBody('{ "object": "list", "url": "/v1/invoices", "has_more": false, "data": [ { "id": "in_1AZjeUFzCf73siP0rlkhQ51s", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 2700, "amount_paid": 2700, "amount_remaining": 0, "application_fee_amount": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription", "charge": "ch_1AZkajFzCf73siP06F91rZ38", "created": 1498669710, "currency": "usd", "custom_fields": null, "customer": "cus_EjRpUJiHU4cVp9", "customer_address": null, "customer_email": "grant@ladybossweightloss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "default_payment_method": null, "default_source": null, "default_tax_rates": [], "description": null, "discount": null, "due_date": null, "ending_balance": 0, "footer": null, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_9yGx3147iIn5erlSvagAvEMyqk", "invoice_pdf": "https://pay.stripe.com/invoice/invst_9yGx3147iIn5erlSvagAvEMyqk/pdf", "lines": { "data": [ { "id": "sli_c8ed818af6e260", "object": "line_item", "amount": 19900, "currency": "usd", "description": "1 × 1yrmembership (at $199.00 / year)", "discountable": true, "livemode": false, "metadata": {}, "period": { "end": 1590092418, "start": 1558470018 }, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": "sub_8UfvbTOhALKzNy", "subscription_item": "si_18RWRXFzCf73siP0wyYdrGUg", "tax_amounts": [], "tax_rates": [], "type": "subscription" } ], "has_more": false, "object": "list", "url": "/v1/invoices/in_1AZjeUFzCf73siP0rlkhQ51s/lines" }, "livemode": false, "metadata": {}, "next_payment_attempt": null, "number": "B64A014-0005", "paid": true, "payment_intent": null, "period_end": 1498669495, "period_start": 1495991095, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1498673321, "marked_uncollectible_at": null, "paid_at": 1498673321, "voided_at": null }, "subscription": "sub_AKVQrNjncVCdpS", "subtotal": 2700, "tax": null, "tax_percent": null, "total": 2700, "total_tax_amounts": [], "webhooks_delivered_at": 1498669716 } ] } ');
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        try {
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response );
            List<Object> data = (List<Object>)results.get('data') ;
            return data.size();
        } catch(Exception ex) {
            system.debug('Anydatatype_msg'+ex);
            return 0;
        }
        
    }
    global static String getOpenInvoice(String subId){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'?status=open&limit=1&subscription='+subId);
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{ "object": "list", "url": "/v1/invoices", "has_more": false, "data": [ { "id": "in_1AZjeUFzCf73siP0rlkhQ51s", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 2700, "amount_paid": 2700, "amount_remaining": 0, "application_fee_amount": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription", "charge": "ch_1AZkajFzCf73siP06F91rZ38", "created": 1498669710, "currency": "usd", "custom_fields": null, "customer": "cus_EjRpUJiHU4cVp9", "customer_address": null, "customer_email": "grant@ladybossweightloss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "default_payment_method": null, "default_source": null, "default_tax_rates": [], "description": null, "discount": null, "due_date": null, "ending_balance": 0, "footer": null, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_9yGx3147iIn5erlSvagAvEMyqk", "invoice_pdf": "https://pay.stripe.com/invoice/invst_9yGx3147iIn5erlSvagAvEMyqk/pdf", "lines": { "data": [ { "id": "sli_c8ed818af6e260", "object": "line_item", "amount": 19900, "currency": "usd", "description": "1 × 1yrmembership (at $199.00 / year)", "discountable": true, "livemode": false, "metadata": {}, "period": { "end": 1590092418, "start": 1558470018 }, "plan": { "id": "1yrmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 19900, "billing_scheme": "per_unit", "created": 1462724260, "currency": "usd", "interval": "year", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": null, "product": "prod_BUY0QMLroRveju", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": "sub_8UfvbTOhALKzNy", "subscription_item": "si_18RWRXFzCf73siP0wyYdrGUg", "tax_amounts": [], "tax_rates": [], "type": "subscription" } ], "has_more": false, "object": "list", "url": "/v1/invoices/in_1AZjeUFzCf73siP0rlkhQ51s/lines" }, "livemode": false, "metadata": {}, "next_payment_attempt": null, "number": "B64A014-0005", "paid": true, "payment_intent": null, "period_end": 1498669495, "period_start": 1495991095, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1498673321, "marked_uncollectible_at": null, "paid_at": 1498673321, "voided_at": null }, "subscription": "sub_AKVQrNjncVCdpS", "subtotal": 2700, "tax": null, "tax_percent": null, "total": 2700, "total_tax_amounts": [], "webhooks_delivered_at": 1498669716 } ] }');
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        try {
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response );
            List<Object> data = (List<Object>)results.get('data') ;
            for (Object d : data) {
                Map<String,Object> mapData = (Map<String, Object>)d;
                return String.valueOf(mapData.get('id')); 
            }
            return 'No open Invoice';
            
        } catch(Exception ex) {
            system.debug('Anydatatype_msg'+ex);
            return null;
        }
        
    }
    global static String payInvoice(String invoiceID){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'/'+invoiceID+'/pay');
        http.setMethod('POST');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{ "id": "in_1EPZhYFzCf73siP0F9K6XAL1", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 5904, "amount_paid": 5904, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_update", "charge": "ch_1EPZhYFzCf73siP0j3Tvecwk", "closed": true, "collection_method": "charge_automatically", "created": 1555353524, "currency": "usd", "custom_fields": null, "customer": "cus_E8Qp0Gw3FV45y0", "customer_address": null, "customer_email": "grant+multipletesttesttest@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1555353524, "default_payment_method": null, "default_source": null, "default_tax_rates": [ { "id": "txr_1CLEiqFzCf73siP0R9ciie0u", "object": "tax_rate", "active": true, "created": 1524766896, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 0 } ], "description": "", "discount": null, "due_date": 1557945524, "ending_balance": 0, "finalized_at": 1555353524, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_hQC82rgkXi4mUf8Zhximj1DXyx", "invoice_pdf": "https://pay.stripe.com/invoice/invst_hQC82rgkXi4mUf8Zhximj1DXyx/pdf", "lines": { "object": "list", "data": [ { "id": "ii_1EPZhXFzCf73siP0CarONUmv", "object": "line_item", "amount": 978, "currency": "usd", "description": "Shipping cost", "discountable": true, "invoice_item": "ii_1EPZhXFzCf73siP0CarONUmv", "livemode": true, "metadata": {}, "period": { "end": 1555353523, "start": 1555353523 }, "plan": null, "proration": false, "quantity": 1, "subscription": null, "tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "tax_rates": [], "type": "invoiceitem" }, { "id": "sub_EtMQI0bKEYfNCn", "object": "line_item", "amount": 4926, "currency": "usd", "description": "1 × LadyBoss LEAN 1 Bag Subscription-PRE ORDER-SF (at $49.26 / month)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "New York 47 West 13th Street NY United States 10011", "Full Name": "Grant Multipletesttesttest", "Email": "grant+multipletesttesttest@ladyboss.com", "Phone": "234234234", "Sales Person": "Tirth Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000krtS0AAI", "Integration Initiated From": "Salesforce", "Street": "47 West 13th Street", "City": "New York", "State": "NY", "Postal Code": "10011", "Country": "United States" }, "period": { "end": 1557945524, "start": 1555353524 }, "plan": { "id": "plan_ErY4EHO2VUdNmC", "object": "plan", "active": true, "aggregate_usage": null, "amount": 4926, "billing_scheme": "per_unit", "created": 1554935543, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "LadyBoss LEAN 1 Bag Subscription-PRE ORDER-SF", "product": "prod_ErY3y7GLJ9PrE5", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EtMQzQkLp7oBeS", "tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_138e7fd66336ee" } ], "has_more": false, "total_count": 2, "url": "/v1/invoices/in_1EPZhYFzCf73siP0F9K6XAL1/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "09BE533-0067", "paid": true, "payment_intent": "pi_1EPZhYFzCf73siP0sUvJJDzb", "period_end": 1555353524, "period_start": 1555353524, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1555353524, "marked_uncollectible_at": null, "paid_at": 1555353526, "voided_at": null }, "subscription": "sub_EtMQI0bKEYfNCn", "subtotal": 5904, "tax": 0, "tax_percent": 0, "total": 5904, "total_tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "webhooks_delivered_at": 1555353530 }');
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        try {
            Invoices p = Invoices.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** Invoices object: '+p); 
            if (p.id != null && p.error == null) {
               return 'Success'; 
            }else if (p.error != null && p.error.message != null) {
                return p.error.message;
            } 
            return null;
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    global static String getSubID(String invoiceID){
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL+'/'+invoiceID);
        http.setMethod('GET');
        String authorizationHeader = 'bearer ' +StripeAPI.ApiKey;
        http.setHeader('Authorization', authorizationHeader);
        http.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        String response;
        
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            try {
                hs = con.send(http);
            } catch (CalloutException e) {
                return null;
            }
        } else {
            hs.setBody('{ "id": "in_1EPZhYFzCf73siP0F9K6XAL1", "object": "invoice", "account_country": "US", "account_name": "LadyBoss ", "amount_due": 5904, "amount_paid": 5904, "amount_remaining": 0, "application_fee": null, "attempt_count": 1, "attempted": true, "auto_advance": false, "billing": "charge_automatically", "billing_reason": "subscription_update", "charge": "ch_1EPZhYFzCf73siP0j3Tvecwk", "closed": true, "collection_method": "charge_automatically", "created": 1555353524, "currency": "usd", "custom_fields": null, "customer": "cus_E8Qp0Gw3FV45y0", "customer_address": null, "customer_email": "grant+multipletesttesttest@ladyboss.com", "customer_name": null, "customer_phone": null, "customer_shipping": null, "customer_tax_exempt": "none", "customer_tax_ids": [], "date": 1555353524, "default_payment_method": null, "default_source": null, "default_tax_rates": [ { "id": "txr_1CLEiqFzCf73siP0R9ciie0u", "object": "tax_rate", "active": true, "created": 1524766896, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": true, "metadata": {}, "percentage": 0 } ], "description": "", "discount": null, "due_date": 1557945524, "ending_balance": 0, "finalized_at": 1555353524, "footer": null, "forgiven": false, "hosted_invoice_url": "https://pay.stripe.com/invoice/invst_hQC82rgkXi4mUf8Zhximj1DXyx", "invoice_pdf": "https://pay.stripe.com/invoice/invst_hQC82rgkXi4mUf8Zhximj1DXyx/pdf", "lines": { "object": "list", "data": [ { "id": "ii_1EPZhXFzCf73siP0CarONUmv", "object": "line_item", "amount": 978, "currency": "usd", "description": "Shipping cost", "discountable": true, "invoice_item": "ii_1EPZhXFzCf73siP0CarONUmv", "livemode": true, "metadata": {}, "period": { "end": 1555353523, "start": 1555353523 }, "plan": null, "proration": false, "quantity": 1, "subscription": null, "tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "tax_rates": [], "type": "invoiceitem" }, { "id": "sub_EtMQI0bKEYfNCn", "object": "line_item", "amount": 4926, "currency": "usd", "description": "1 × LadyBoss LEAN 1 Bag Subscription-PRE ORDER-SF (at $49.26 / month)", "discountable": true, "livemode": true, "metadata": { "Shipping Address": "New York 47 West 13th Street NY United States 10011", "Full Name": "Grant Multipletesttesttest", "Email": "grant+multipletesttesttest@ladyboss.com", "Phone": "234234234", "Sales Person": "Tirth Patel", "Salesforce Contact Link": "https://ladyboss.my.salesforce.com/003f400000krtS0AAI", "Integration Initiated From": "Salesforce", "Street": "47 West 13th Street", "City": "New York", "State": "NY", "Postal Code": "10011", "Country": "United States" }, "period": { "end": 1557945524, "start": 1555353524 }, "plan": { "id": "plan_ErY4EHO2VUdNmC", "object": "plan", "active": true, "aggregate_usage": null, "amount": 4926, "billing_scheme": "per_unit", "created": 1554935543, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": true, "metadata": {}, "nickname": "LadyBoss LEAN 1 Bag Subscription-PRE ORDER-SF", "product": "prod_ErY3y7GLJ9PrE5", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "proration": false, "quantity": 1, "subscription": null, "subscription_item": "si_EtMQzQkLp7oBeS", "tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "tax_rates": [], "type": "subscription", "unique_line_item_id": "sli_138e7fd66336ee" } ], "has_more": false, "total_count": 2, "url": "/v1/invoices/in_1EPZhYFzCf73siP0F9K6XAL1/lines" }, "livemode": true, "metadata": {}, "next_payment_attempt": null, "number": "09BE533-0067", "paid": true, "payment_intent": "pi_1EPZhYFzCf73siP0sUvJJDzb", "period_end": 1555353524, "period_start": 1555353524, "post_payment_credit_notes_amount": 0, "pre_payment_credit_notes_amount": 0, "receipt_number": null, "starting_balance": 0, "statement_descriptor": null, "status": "paid", "status_transitions": { "finalized_at": 1555353524, "marked_uncollectible_at": null, "paid_at": 1555353526, "voided_at": null }, "subscription": "sub_EtMQI0bKEYfNCn", "subtotal": 5904, "tax": 0, "tax_percent": 0, "total": 5904, "total_tax_amounts": [ { "amount": 0, "inclusive": false, "tax_rate": "txr_1CLEiqFzCf73siP0R9ciie0u" } ], "webhooks_delivered_at": 1555353530 }');
            hs.setStatusCode(200);
        }

        system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        try {
            Invoices p = Invoices.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** Invoices object: '+p); 
            return p.subscription;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    public static Invoices parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        system.debug('++--'+System.JSON.deserialize(json, Invoices.class));
        return (Invoices) System.JSON.deserialize(json, Invoices.class);

    }
    
	class error {
		public String charge;	//ch_1FMDJoFzCf73siP0MVtx8qEM
		public String code;	//card_declined
		public String decline_code;	//generic_decline
		public String doc_url;	//https://stripe.com/docs/error-codes/card-declined
		public String message;	//Your card was declined.
		public String type;	//card_error
	}
}