@isTest
public class UpdateMetadataonOpportunityTest {
    @IsTest
    static void methodName(){        
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
        opportunity op  = new opportunity();
        op.name='CLUB';
        op.StageName = 'Closed Won';
        op.RecordTypeId = devRecordTypeId;
        op.Amount = 3000;
        op.Initiated_From__c = 'Stripe';
        op.CloseDate = System.today();
        op.Stripe_Charge_Id__c = 'test';
        op.Charge_Description__c = 'test';
        insert op;
        Test.startTest();
        UpdateMetadataonOpportunity b = new UpdateMetadataonOpportunity();
        database.executebatch(b,10);
        Test.stopTest();
        
    }
}