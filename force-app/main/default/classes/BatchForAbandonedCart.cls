global class BatchForAbandonedCart implements Database.Batchable<sObject>, Database.AllowsCallouts,Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select id, Contact__c,Contact__r.Email FROM Abandoned_Cart_Contact__c');
    }
    global void execute(SchedulableContext ctx) {
        BatchForAbandonedCart batch = new BatchForAbandonedCart();
        Database.executeBatch(batch,100);
    }

    global void execute(Database.BatchableContext jobId, List<Abandoned_Cart_Contact__c> recordList) {
        String custo;
        try{
            Map<String, String> conSourceMap = new Map<String, String>();
            List<String> conEmailList = new List<String>();            
            List<String> conIdList = new List<String>();            
            List<String> conIdListInserted = new List<String>();
            Map<String, List<Contact>> emailConMap = new Map<String, List<Contact>>();
            for (Abandoned_Cart_Contact__c acc : recordList) {
                conEmailList.add(acc.Contact__r.Email);
                conIdListInserted.add(acc.Contact__c);
            }
            List<Contact> conList = [SELECT id,Email,WordPress_Buyer_Source__c,WordPress_Contact_Source__c,Abandoned_Cart__c FROM Contact WHERE Id IN: conIdListInserted];
            
            for (Contact con : conList) {
                Set<String> abandonedcart = new Set<String>();
                abandonedcart.addAll(con.WordPress_Contact_Source__c.split(';'));
                Set<String> abandonedcartclone = new Set<String>();
                abandonedcartclone.addAll(abandonedcart);
                DateTime dt = Datetime.now().addHours(-1);
                for (Opportunity opp : [SELECT id,Name,Contact__r.Email FROM Opportunity WHERE Contact__r.Email IN :conEmailList AND CreatedDate >: dt]) {
                    if (con.Email == opp.Contact__r.Email) {
                        for (String st : abandonedcartclone) {
                            if (opp.Name.contains(st)) {
                                abandonedcart.remove(st);
                            }
                        }
                    }
                }
                String consource = '';
                for (String st : abandonedcart) {
                    consource = st + ';';
                }
                consource = consource.removeEnd(';');
                conSourceMap.put(con.id, consource);
            }
            for (Contact con : conList) {
                if (conSourceMap.get(con.Id) != '') {
                    con.Abandoned_Cart__c = conSourceMap.get(con.Id);
                }
            }
            
            update conList;
            delete recordList;
        }catch(Exception ex){
            ApexDebugLog apex=new ApexDebugLog(); 
            apex.createLog(
                new ApexDebugLog.Error('BatchForAbandonedCart','MailofContact',custo,ex));
            
        } 


    }

    global void finish(Database.BatchableContext jobIdParam) {
       
    }
}