Public class CampaignMemberPipelineBatchDelQueue implements Queueable {
    List<CampaignMember> deleteRecords;
    public CampaignMemberPipelineBatchDelQueue(List<CampaignMember> deleteList) {
        this.deleteRecords = deleteList;
    }
    public void execute(QueueableContext context) {
        system.debug('** queue delete list '+ deleteRecords);
        delete deleteRecords;
        
    }
}