@isTest 
private class CaseOwnerTriggerHandlerTest {
    
    Public static testmethod void doBeforeUpdateTest(){
        Group g = new Group(Name = 'test group',type = 'Queue');
        insert g;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = g.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        
        Case c = new Case();
        c.Status = 'New';
        c.Origin = 'web';
        c.OwnerId = g.Id;
        c.Status = 'Closed';
        insert c;
        System.debug('**Test **'+c);
        
        Test.startTest();
        CaseOwnerTriggerHandler.doBeforeUpdate(new List<Case> {c});
        
        Test.stopTest();
    }
}