@isTest 
private class LB_KbCaseCreationController_Test {
    public static testmethod void doCreateContacttest(){
        LB_KbCaseCreationController.caseWrapper cw = new LB_KbCaseCreationController.caseWrapper();
        cw.email = 'test@test.com';
        cw.phone = '1234567890';
        cw.fname = 'test';
        cw.lname = 'name';
        cw.subject = 'test sub';
        cw.description = 'test des';
        String cwstring = Json.serialize(cw);
        test.startTest();
        LB_KbCaseCreationController.doCreateContact(cwstring);
        test.stopTest();
    }
}