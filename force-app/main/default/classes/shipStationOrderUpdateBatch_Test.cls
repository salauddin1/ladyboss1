@isTest
public class shipStationOrderUpdateBatch_Test {
    @isTest
    public static void test() {
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'shipstationtesting@gmail.com';
        insert con;
        
        ShipStation_Orders__c shpOrd = new ShipStation_Orders__c();
        shpOrd.orderNumber__c = '4544492';
        shpOrd.orderId__c = '4035814';
        shpOrd.customerEmail__c = 'shipstationtesting@gmail.com';
        shpOrd.orderStatus__c = 'awaiting_shipment';
        shpOrd.Contact__c = con.id;
        insert shpOrd;
        
        ShipStation_Order_Items__c shI = new ShipStation_Order_Items__c();
        shI.orderItemId__c = '6083153';
        shI.name__c = 'BURN-1 Bottle CLUB-$39.95';
        shI.quantity__c = 1;
        Insert shI;
        
        ShipStation__c sh = new ShipStation__c();
        sh.Domain_Name__c = 'http://ssapi11.shipstation.com';
        sh.userName__c = '35ede470eab34234a407c092d5258b0f';
        sh.password__c = '71e53c268e2d42fba9c9608fec5dd5db';
        sh.IsLive__c = true;
        insert sh;
        ShipStationOrderUpdateBatch_Pages__c pages = new ShipStationOrderUpdateBatch_Pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        pages.Pages__c = 10;
        pages.Number_of_Days__c = 60;
        insert pages;
        
        shipStationOrderUpdateBatch shBatch = new shipStationOrderUpdateBatch();
        Test.setMock(HttpCalloutMock.class, new shipStationOrderBtach_Mock());
        
        test.startTest();
        Database.executeBatch(shBatch);
        test.stopTest();
        
    }
    @isTest
    public static void test3() {
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'shipstationtesting@gmail.com';
        insert con;
        
        ShipStation_Orders__c shpOrd = new ShipStation_Orders__c();
        shpOrd.orderNumber__c = '4544492';
        shpOrd.orderId__c = '4035814';
        shpOrd.customerEmail__c = 'shipstationtesting@gmail.com';
        shpOrd.orderStatus__c = 'awaiting_shipment';
        shpOrd.Contact__c = con.id;
        insert shpOrd;
        
        ShipStation_Order_Items__c shI = new ShipStation_Order_Items__c();
        shI.orderItemId__c = '6083153';
        shI.name__c = 'BURN-1 Bottle CLUB-$39.95';
        shI.quantity__c = 1;
        Insert shI;
        
        ShipStation__c sh = new ShipStation__c();
        sh.Domain_Name__c = 'http://ssapi11.shipstation.com';
        sh.userName__c = '35ede470eab34234a407c092d5258b0f';
        sh.password__c = '71e53c268e2d42fba9c9608fec5dd5db';
        sh.IsLive__c = true;
        insert sh;
        ShipStationOrderUpdateBatch_Pages__c pages = new ShipStationOrderUpdateBatch_Pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        pages.Pages__c = 10;
        pages.Number_of_Days__c = 60;
        insert pages;
        
        Test.setMock(HttpCalloutMock.class, new shipStationOrderBtach_Mock());
        
        test.startTest();
        shipStationOrderUpdateBatchCount.Ordercounter();
        System.schedule('shipStationOrderUpdateBatchCoun', '0 18 * * * ?', new ShipstationOrderUpdatecountSchedular());
        System.schedule('shipStationOrderUpdateBatchSchedular', '0 18 * * * ?', new shipStationOrderUpdateBatchSchedular());
        test.stopTest();
        
    }
    @isTest
    public static void test2() {
        
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'shipstationtesting@gmail.com';
        insert con;
        
        ShipStation_Orders__c shpOrd = new ShipStation_Orders__c();
        shpOrd.orderNumber__c = '4544492';
        shpOrd.orderId__c = '4035814';
        shpOrd.orderStatus__c = 'awaiting_shipment';
        shpOrd.customerEmail__c = 'shipstationtesting@gmail.com';
        shpOrd.Contact__c = con.id;
        insert shpOrd;
        
        ShipStation_Order_Items__c shI = new ShipStation_Order_Items__c();
        shI.orderItemId__c = '6083153';
        shI.name__c = 'BURN-1 Bottle CLUB-$39.95';
        shI.quantity__c = 1;
        shI.ShipStation_Orders__c = shpOrd.Id;
        Insert shI;
        
        ShipStation__c sh = new ShipStation__c();
        sh.Domain_Name__c = 'http://ssapi11.shipstation.com';
        sh.userName__c = '35ede470eab34234a407c092d5258b0f';
        sh.password__c = '71e53c268e2d42fba9c9608fec5dd5db';
        sh.IsLive__c = true;
        insert sh;
        
        ShipStationOrderUpdateBatch_Pages__c pages = new ShipStationOrderUpdateBatch_Pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        pages.Pages__c = 10;
        pages.Number_of_Days__c = 60;
        insert pages;
        
        shipStationOrderUpdateBatch shBatch = new shipStationOrderUpdateBatch();
        
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new shipStationOrderBtach_Mock());
        Database.executeBatch(shBatch);
        
        test.stopTest();
        
    }
}