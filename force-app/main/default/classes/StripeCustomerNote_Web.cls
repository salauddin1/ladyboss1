@RestResource(urlMapping='/StripeCustomerNote_Web')
global class StripeCustomerNote_Web {
    @HttpPost
    global static void stripeRoutoingMethod(){
        String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
        Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'StripeCustomerNote_Web');
        System.debug('-------timestamppp----'+timestamppp);
        System.debug('-------sigFromHmac----'+sigFromHmac);
        String CustomerId;
        if(sigFromHmac && sigFromHmac == true || Test.isRunningTest()){
            if(RestContext.request.requestBody != null){
                try {
                    Boolean value ;
                    String str=RestContext.request.requestBody.toString();
                    Map<String, Object> StripeResult = (Map<String, Object>)JSON.deserializeUntyped(str);
                    Map<String, Object> CustomerInfo = (Map<String, Object>)StripeResult.get('data');
                    Map<String, Object> objectInfo = (Map<String, Object>)CustomerInfo.get('object');
                    Map<String, Object> newMetaData = (Map<String, Object>)objectInfo.get('metadata');
                    Map<String, Object> preData = (Map<String, Object>)CustomerInfo.get('previous_attributes');
                    Map<String, Object> updateMetaData = (Map<String, Object>)preData.get('metadata');
                    System.debug('---------updateMetaData--------'+updateMetaData);
                    String custId = String.valueOf(objectInfo.get('id'));
                    CustomerId = custId;
                    System.debug('---------custId--------'+custId);
                    list<Customer_Note__c> custVar = new list<Customer_Note__c>();
                    list<Customer_Note__c> updateCustVar = new list<Customer_Note__c>();
                    list<String> keyVal = new list<String>();
                    Stripe_Profile__c stProfile  = new Stripe_Profile__c ();
                    list<Customer_Note__c> custNoteList = new list<Customer_Note__c> ();
                    Decimal balance = Decimal.valueOf(String.valueOf(objectInfo.get('balance')));
                    if(custId != null && custId != ''){
                        stProfile = [select id,Name,Stripe_Customer_Id__c,Balance__c, Customer__c,Case__c from Stripe_Profile__c where Stripe_Customer_Id__c =: custId limit 1];
                        custNoteList = [select id,Customer_Note__c,Customer_key__c,Stripe_Profile__c from Customer_Note__c where Stripe_Profile__c =: stProfile.id];
                    }
                    map<String,string> check = new   map<String,string> ();
                    if(custNoteList != null && !custNoteList.isEmpty()){
                        for(Customer_Note__c cust :custNoteList){
                            check.put(cust.Customer_key__c, cust.Customer_Note__c);
                        }
                    }
                    if (balance != null) {
                        stProfile.Balance__c = -balance*0.01;
                        update stProfile;
                    }
                    if( newMetaData != null && updateMetaData != null){
                        for(String newSt : newMetaData.keySet()){
                            for(String updateSt : updateMetaData.keySet()){
                                if(newSt == updateSt &&  updateMetaData.get(updateSt) == null){
                                    Customer_Note__c custNote = new Customer_Note__c();
                                    custNote.Customer_Note__c = String.valueOf(newMetaData.get(newSt));
                                    custNote.Contact__c = stProfile.Customer__c;
                                    custNote.Stripe_Profile__c = stProfile.id;
                                    custNote.Customer_key__c = newSt;
                                    custNote.Case__c = stProfile.Case__c;
                                    custVar.add(custNote);
                                }
                                else if(newSt == updateSt){
                                    keyVal.add(newSt);
                                    if(check.keySet().contains(newSt)){
                                        check.put(newSt,  String.valueOf(newMetaData.get(newSt)));
                                    }
                                }
                            }
                        }
                    }
                    if(custVar != null && !custVar.isEmpty()){
                        insert custVar;
                    }
                    if(custNoteList != null && !custNoteList.isEmpty()){
                        for(Customer_Note__c cust :custNoteList){
                            if(check.keySet().contains(cust.Customer_key__c)){
                                cust.Customer_Note__c = check.get(cust.Customer_key__c);
                            }
                        }
                        update custNoteList;
                    }
                    System.debug('========custNoteList====='+custNoteList);
                    System.debug('========custVar====='+custVar);
                    if(test.isrunningTest()){
                        Integer a=10/0;
                    }
                }
                catch(exception ex){
                    RestResponse res = RestContext.response; 
                    res.statusCode = 400;
                    System.debug('The following exception has occurred: ' + ex.getMessage());
                    System.debug('Exception e '+ex.getLineNumber());
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                        new ApexDebugLog.Error(
                            'StripeCustomerNote_Web',
                            'stripeRoutoingMethod',
                            CustomerId,
                            ex
                        )
                    );
                }
            }
        }
    }
}