@isTest
public class StripeCreateProduct_Test {
	static testMethod void test() {
        List<Product2> prodList= new List<Product2>();
        Product2 p = new Product2();
        p.Name='test';
        p.Stripe_Product_Id__c= 'abc';
        p.Price__c= 100;
        p.IsActive=true;

        insert p;
        prodList.add(p);
        //System.enqueuejob(new  StripeCreateProduct(prodList));
        StripeCreateProduct.createProduct(p.Name, 'good', p.Id);
        p.Stripe_Product_Id__c='efg';
        update p;
        test.startTest();
        QueueableContext qc = null;
        
        
        StripeCreateProduct sc = new StripeCreateProduct(prodList);
        sc.execute(qc);
        
        
        StripeCreateProduct.setTaxProduct(p.Stripe_Product_Id__c, p.Id);
        StripeCreateProduct.getProduct('test');
        StripeCreateProduct.parse('{"object":"test","date":"test","end":"test","data":"test","currency":"test","type":"test","customer":"test","invoice":"test"}');
        test.stopTest();
    }
}