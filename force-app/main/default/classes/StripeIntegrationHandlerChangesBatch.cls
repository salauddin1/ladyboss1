global class StripeIntegrationHandlerChangesBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful,Schedulable{
    global void execute(SchedulableContext sc){
        StripeIntegrationHandlerChangesBatch b = new StripeIntegrationHandlerChangesBatch();
        database.executebatch(b,1);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,Name,Scheduled_Payment_Date__c,Contact__c,Card__c,Clubbed__c,Primary_Amount__c,Sales_Person__c,Secondary_Amount__c,Secondary_Card__c,Amount,Stripe_Message__c,Opportunity_Unique_Name__c,Success_Failure_Message__c,Stripe_Charge_Id__c,Secondary_Stripe_Charge_Id__c,Is_Paypal__c, Paypal_Billing_Agreement__c,Coupon_Code__c, (SELECT Id,Quantity, OpportunityId,product2id,product2.Name,product2.Email_Body__c,Opportunity_Unique_Name__c,Tax_Percentage_s__c,TotalPrice ,Tax_Amount__c FROM OpportunityLineItems ) FROM Opportunity where Scheduled_Payment_Processed__c= false and Scheduled_Payment_Date__c =today');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        String message ='';
        Decimal salesTaxPercentage = 0.00;
        List<Opportunity> oppList = (List<Opportunity>)scope;
        system.debug('oppList---'+oppList);
        List<Opportunity> oppToBeUpdated = new List<Opportunity>();
        
        for(Opportunity opp : oppList){
            Map<String,Opportunity> insertOpportunityMap = new Map<String,Opportunity>();
            Map<String,List<OpportunityLineItem>> opptyItemList = new Map<String,List<OpportunityLineItem>>();
            Map<String,String> cardMap = new Map<String,String>();
            List<OrderFormControllerChangesTax.SelectedCard> cardlist = new List<OrderFormControllerChangesTax.SelectedCard>();
            if(opp.Clubbed__c){
                if(opp.Card__c != null){
                    OrderFormControllerChangesTax.SelectedCard card1 = new OrderFormControllerChangesTax.SelectedCard();
                    card1.card=opp.Card__c;
                    card1.nonClubamount =0;
                    card1.usedForClub = true;
                    cardlist.add(card1);
                }
                insertOpportunityMap.put('club',opp);
                if(!opptyItemList.containsKey('club')){
                    opptyItemList.put('club',opp.OpportunityLineItems);
                }
                //opptyItemList.get('club').addall(opp.OpportunityLineItems);
            }else{
                if(opp.Card__c != null){
                    OrderFormControllerChangesTax.SelectedCard card1 = new OrderFormControllerChangesTax.SelectedCard();
                    card1.card=opp.Card__c;
                    if(opp.Secondary_Card__c != null && opp.Secondary_Amount__c != null){
                        card1.nonClubamount =opp.Primary_Amount__c;
                    }else{
                        card1.nonClubamount =opp.Amount;
                    }
                    
                    card1.usedForClub = true;
                    cardlist.add(card1);
                }
                if(opp.Secondary_Card__c != null && opp.Secondary_Amount__c != null){
                    OrderFormControllerChangesTax.SelectedCard card2 = new OrderFormControllerChangesTax.SelectedCard();
                    card2.card=opp.Secondary_Card__c;
                    card2.nonClubamount =opp.Secondary_Amount__c;
                    card2.usedForClub = false;
                    cardlist.add(card2);
                }
                insertOpportunityMap.put('nonClub',opp);
                if(!opptyItemList.containsKey('nonClub') && opp.OpportunityLineItems.size() > 0){
                    opptyItemList.put('nonClub',opp.OpportunityLineItems);
                }
                //opptyItemList.get('nonClub').addall(opp.OpportunityLineItems);
            }
            Decimal averageTax =0.00;
            Decimal sumTax = 0.00;
            Integer countTax = 0;
            for (OpportunityLineItem o : opp.OpportunityLineItems) {
                if(o.Tax_Percentage_s__c != null){
                    sumTax += o.Tax_Percentage_s__c;
                    countTax++;
                } 
            }
            if(sumTax != 0.00 && countTax>0){
                averageTax = sumTax/countTax;
                salesTaxPercentage = averageTax;
            }
            if((cardlist.size() > 0 || opp.Is_Paypal__c) && !insertOpportunityMap.isEmpty() && opptyItemList.size() > 0 && !opptyItemList.isEmpty()){
                system.debug('----oplist--'+opptyItemList);
                try{
                    if (opp.Is_Paypal__c) {
                        message = PaypalIntegrationHandlerChanges.OpportunityPaypalHandler(opp.Paypal_Billing_Agreement__c, insertOpportunityMap, opptyItemList, false, opp.Sales_Person__c, salesTaxPercentage, false, false);
                    }else {
                        message = StripeIntegrationHandlerChangesTax.OpportunityStripeHandler(cardList, insertOpportunityMap, opptyItemList, false, opp.Sales_Person__c, salesTaxPercentage, 'productOptionsOne', true,false);                        
                    }
                }catch(Exception ex){
                    ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeIntegrationHandlerChangesBatch',
                        'bacth',
                        '',
                        ex
                    )
                );
                    message = 'Failure';
                }
                
            }
            
            if(message != null && message == 'Success'){
                if(opp.Coupon_Code__c != null) {
                    List<Id> productIds = new List<Id>();
                    List<Id> opportunityIds = new List<Id>();
                    Map<String,String> olicouponmap = new Map<String,String>();
                    Map<String,Contact> couponconmap = new Map<String,Contact>();
                    Map<id, Decimal> prodpriccemap = new Map<id, Decimal>();
                    for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                        productIds.add(oli.Product2Id);
                        olicouponmap.put(oli.Id, opp.Coupon_Code__c);
                    }
                    for (Contact con : [SELECT id,Name,Email,Pap_RefId__c FROM Contact WHERE Pap_Refid__c IN:olicouponmap.values()]) {
                        couponconmap.put(con.PAP_refid__c, con);
                    }
                    for(Product2 prod:[SELECT id, Commission_Enabled_Club_Product__c,Price__c FROM Product2 WHERE id IN : productIds AND Commission_Enabled_Club_Product__c = true]) {
                        prodpriccemap.put(prod.id,prod.Price__c);
                    }
                    List<Pap_Commission__c> paptoinsert = new List<Pap_Commission__c>();
                    for(OpportunityLineItem oli: opp.OpportunityLineItems){
                        if (oli.Product2Id != null && prodpriccemap.containsKey(oli.Product2Id)) {
                            Pap_Commission__c pap= new Pap_Commission__c();
                            pap.Contact__c = couponconmap.get(olicouponmap.get(oli.Id)).id;
                            pap.commission__c = prodpriccemap.get(oli.Product2Id)* 33.34/100;
                            pap.Commission_Type__c = 'initial';
                            pap.Opportunity__c = oli.OpportunityId;
                            pap.Campaign_name__c = 'SUBSCRIPTION Physical Products';
                            pap.Campaign_Id__c = '2605c613';
                            pap.affiliate_ref_id__c = olicouponmap.get(oli.Id);
                            pap.data1__c = couponconmap.get(olicouponmap.get(oli.Id)).Email;
                            pap.data2__c = couponconmap.get(olicouponmap.get(oli.Id)).Name;
                            pap.data4__c = String.valueOf(prodpriccemap.get(oli.Product2Id));
                            paptoinsert.add(pap);
                        }
                    }
                    if (paptoinsert.size()>0) {
                        insert paptoinsert;
                    }
                }
                opp.Scheduled_Payment_Processed__c = true;
                oppToBeUpdated.add(opp);
            }
        }
        
        if(oppToBeUpdated.size() > 0 ){
            update oppToBeUpdated;
            sendMailToUser(oppToBeUpdated);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    public static void sendMailToUser(List<Opportunity> oppToBeUpdated){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'support@ladyboss.com'];
        Set<Id> oppIds = new  Set<Id>();
        for(Opportunity opp : oppToBeUpdated){
            oppIds.add(opp.Id);
            system.debug('opp---'+opp);
        } 
        for(Opportunity opp : [select id,Contact__r.Email,Stripe_Message__c,(select id ,product2Id,product2.Email_Body__c,product2.Name from OpportunityLineItems) from Opportunity where id =:oppIds ]){
            
            
            for(OpportunityLineItem lineItem : opp.OpportunityLineItems){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] { opp.Contact__r.Email});
                
                mail.setSubject('LadyBoss Receipt - ' + lineItem.product2.Name);
                mail.setHtmlBody((lineItem.product2.Email_Body__c !=null ? lineItem.product2.Email_Body__c: 'Thank you for your purchase. If you have any questions or concerns, please email us at support@ladyboss.com.'));
                
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                
                PageReference pdf = Page.OpportunityLineItemInvoice1;
                pdf.getParameters().put('id',lineItem.Id);
                Blob body;                
                try{
                    body = pdf.getContent();
                }catch(VisualforceException e){
                    body=Blob.valueOf('Some text');
                }            
                Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                attach.setContentType('application/pdf');
                attach.setFileName('LadyBoss Receipt.pdf');
                attach.setInline(false);
                attach.Body = body;
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
                mails.add(mail);
                
            }
        }
        if(mails.size() >0 && !test.isRunningTest())
            Messaging.sendEmail(mails);
        
    }
    
}