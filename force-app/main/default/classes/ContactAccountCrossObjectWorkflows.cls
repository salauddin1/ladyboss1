// Developer Name :- Surendra Patidar
// class contains code about check the ready for tripalti checkbox from contact to account and account to contact
// =================================================================================================================

public class ContactAccountCrossObjectWorkflows {
    public static boolean accFlag = false;
    public static boolean conFlag = false;
    public Static Void UpdateAccountChekBox(List<Contact> con) {
        System.debug('==========*****checkforflag*****============='+accFlag);
        if(accFlag == false) {
            try{ // variavles initialization
                List<Account> Acclist = new List<Account>();
                List<Contact> conlist = new List<Contact>();
                Set<String> AccIdSET = new Set<String> ();
                Set<String> ConIdSET = new Set<String> ();
                Map<String, Account> AccMap = new Map<String, Account>();
                // filter all 'ready for tripalti' tags contacts 
                
                if(con != null && !con.isEmpty() && con.size()>0){
                    for(contact c : con){
                            if(c.AccountId != null){
                            AccIdSET.add(c.AccountId);
                            ConIdSET.add(c.id);
                        }
                    }
                }
                //query account if contact flag 'ready for tripalti' become to true
                AccList = [select id, Ready_in_Tipalti__c from account where Id IN: AccIdSET];
                // Query contact again for update 'ready in tripalti' flag to true if account same flag have already true
                conlist = [select id, Ready_in_Tipalti__c, AccountId from Contact where Id IN: ConIdSET];
                if(AccList.size()>0) {
                    for(Account act : AccList) {
                        AccMap.put(act.Id, act);
                    }
                }
                System.debug('=========************=========='+AccList);
                System.debug('=========************=========='+conlist);
                // CHecking here if account flag have already true then update contact flag autometically
                if(conlist.size()>0 && !AccMap.isEmpty()) {
                    for(Contact co : conlist) {
                        Account chkAct = AccMap.get(co.AccountId);
                        if(chkAct.Ready_in_Tipalti__c == true) {
                            co.Ready_in_Tipalti__c = true;
                        }
                        if(chkAct.Ready_in_Tipalti__c == false) {
                            chkAct.Ready_in_Tipalti__c = true;
                        }
                    }
                }
                // Update account and contact after all Set
                if(AccMap.values().size()>0)
                    Update AccMap.values();
                if(conlist.size()>0)
                    update conlist;
                
            }
            catch (exception e) {
                system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(con.size()>0) {   for(contact c : con) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('UpdateAccountChekBox ','ContactAccountCrossObjectWorkflows',Ids, e));
            }
        }        
    }
    public Static Void UpdateContactChekBox(List<Account> acclist) {
        try{
            List<Contact> Conlist = new List<Contact>();
            Set<String> AccIdSET = new Set<String> ();
            
            // filter all Account if'ready for tripalti' tags become to true
            if(acclist != null && !acclist.isEmpty()){
                for(Account a : acclist){
                    if(a.Id != null && a.Ready_in_Tipalti__c == true){
                        AccIdSET.add(a.Id);
                    }
                }
            }
            // Query respected contacts of Account.
            ConList = [select id, Ready_in_Tipalti__c from Contact where AccountId IN: AccIdSET];
            if(ConList.size()>0) {
                for(Contact ct : ConList) {
                    if(ct.Ready_in_Tipalti__c == false) {
                        ct.Ready_in_Tipalti__c = true;
                    }
                }
                accFlag = true;
                if(ConList.size()>0)
                    Update ConList;
                
            }
        }
        catch (exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String Ids = 'ErrorIds   ';
            if(acclist.size()>0) {   for(Account c : acclist) { Ids += c.Id+'     '; } }
            apex.createLog(new ApexDebugLog.Error('UpdateContactChekBox ','ContactAccountCrossObjectWorkflows',Ids, e));
        }
    }
}