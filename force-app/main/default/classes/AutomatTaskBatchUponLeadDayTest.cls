@isTest
public class AutomatTaskBatchUponLeadDayTest {
	@isTest
    public static void doTest() {
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-1);
        l.Created_Date_Time__c = System.today().adddays(-1);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        Test.startTest();
        Database.executeBatch(new AutometicTaskBatchUponLeadRec());
        Test.stopTest();
    }
    @isTest
    public static void doTest1() {
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-3);
        l.Created_Date_Time__c = System.today().adddays(-3);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        Test.startTest();
        Database.executeBatch(new AutometicTaskBatchUponLeadRec());
        Test.stopTest();
    }
    @isTest
    public static void doTest3() {
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-8);
        l.Created_Date_Time__c = System.today().adddays(-8);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        Test.startTest();
        Database.executeBatch(new AutomatTaskBatchUponLeadDay());
        Test.stopTest();
    }
    @isTest
    public static void doTest4() {
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-31);
        l.Created_Date_Time__c = System.today().adddays(-31);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        Test.startTest();
        Database.executeBatch(new AutomatTaskBatchUponLeadDay());
        Test.stopTest();
    }
      @isTest
    public static void doTest5() {
        Lead l = new lead ();
        l.LastName = 'testlead';
        l.Created_Date__c = System.today().adddays(-91);
        l.Created_Date_Time__c = System.today().adddays(-91);
        l.Company = 'testcomp';
        l.Email = 'Test@gmail.com';
        insert l;
        Test.startTest();
        Database.executeBatch(new AutomatTaskBatchUponLeadDay());
        Test.stopTest();
    }
}