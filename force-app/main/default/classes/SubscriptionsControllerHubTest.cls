@isTest
public class SubscriptionsControllerHubTest {
    public static TestMethod void testGetCallout(){
        ContactTriggerFlag.isContactBatchRunning = true;
        system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        Triggers_activation__c t = new Triggers_activation__c();
        t.Opportunitytriggerhandler__c = false;
        t.isCampaignAssociate__c = false;
        insert t;
        
        Card__c card = new Card__c();
        card.Stripe_Card_Id__c = 'card_1G69qjFzCf73siP0MC7SifTq';
        insert card;
        
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Card__c = card.id;
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 pro = new Product2();
        pro.Name='pro';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        insert pro;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        insert ol;
        
        String dateVal = String.valueOf(date.today());
        Test.startTest();
        SubscriptionsControllerHub.updateSubscription(ol.Id,dateVal,'AWDT',false,'zero');
        Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout1(){
        ContactTriggerFlag.isContactBatchRunning = true;
        
        Triggers_activation__c t = new Triggers_activation__c();
        t.Opportunitytriggerhandler__c = false;
        t.isCampaignAssociate__c = false;
        insert t;
        system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        System.debug('----------'+devRecordTypeId);
        contact co = new contact();
        co.LastName ='vijay';
        co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
        insert co;
        
        Card__c card = new Card__c();
        card.Stripe_Card_Id__c = 'card_1G69qjFzCf73siP0MC7SifTq';
        insert card;
        
        Opportunity opp = new Opportunity();
        opp.name ='oppName';
        opp.Card__c = card.id;
        opp.RecordTypeId = devRecordTypeId;
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = co.Id;
        opp.Subscription__c ='sub_DyJjwBxrWQKsZf';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        Product2 pro = new Product2();
        pro.Name='pro';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        insert pro;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_Dxxlhyn7eAHH6v';
        ol.TrialPeriodStart__c=System.today()-1;
        ol.TrialPeriodEnd__c=System.today(); 
        ol.Status__c = 'trialing';
        insert ol;
        
        String dateVal = String.valueOf(date.today());
        Test.startTest();
        SubscriptionsControllerHub.pauseSubscriptions(ol.Id, '30 Days','AWDT');
        Test.stopTest(); 
        
    }
    static testmethod void testmethod2(){
        ContactTriggerFlag.isContactBatchRunning = true;
        Triggers_activation__c t = new Triggers_activation__c();
        t.Opportunitytriggerhandler__c = false;
        t.isCampaignAssociate__c = false;
        insert t;
        system.Test.setMock(HttpCalloutMock.class, new MockTest1());
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        contact con = new contact();
        con.lastName='lastName';
        con.firstName='firstName';
        con.Email = 'test@test.com';
        con.Phone = '12345';
        insert con;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Remaining__c =1000;
        ap.Name='Async Limit Left';
        insert ap;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_Eug8juCETidS33';
        sp.Customer__c = con.id;
        insert sp;
        
        ACH_Account__c ac = new ACH_Account__c(); 
        ac.ACH_Token__c = null;
        ac.Name = 'test';
        ac.Stripe_Profile__c = sp.id;
        ac.Stripe_ACH_Id__c='achiddskfdsf';
        insert ac;
        
        Product2 pro = new Product2();
        pro.Name='pro';
        pro.Price__c=12345;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        insert pro; 
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        opportunity op  = new opportunity();
        op.name=pro.Name+'CLUB';
        op.Contact__c =con.Id;
        op.RecordTypeId = devRecordTypeId;
        op.StageName = 'Closed Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        
        OpportunityLineItem olpo = new OpportunityLineItem();        
        olpo.OpportunityId = op.Id;
        olpo.Quantity = 3;
        olpo.UnitPrice = 2.00;
        olpo.Subscription_Id__c='sub_EypY8AdfVmqvVo';
        olpo.Start__c=System.today()-1;
        olpo.End__c=System.today();    
        olpo.Opportunity_Unique_Name__c = op.Id;
        olpo.PricebookEntryId = standardPrice.Id;
        olpo.Status__c='Active';
        insert olpo;
        
        
        Test.startTest();
        SubscriptionsControllerHub subscriptioncontroller = new SubscriptionsControllerHub();
        SubscriptionsControllerHub.getAllSubscriptions(con.Id);
        SubscriptionsControllerHub.getSubscriptionsFromStripe('jsondata');
        SubscriptionsControllerHub.refreshSubscription(con.Id);
        Test.stopTest();
    }
    static testmethod void testmethod1(){
        ContactTriggerFlag.isContactBatchRunning = true;
        Triggers_activation__c t = new Triggers_activation__c();
        t.Opportunitytriggerhandler__c = false;
        t.isCampaignAssociate__c = false;
        insert t;
        Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
        ap.Remaining__c =1000;
        ap.Name='Async Limit Left';
        insert ap;
        contact con = new contact();
        con.lastName='lastNametest this';
        con.firstName='firstName check this ';
        con.Email = 'test@testing.com';
        con.Phone = '123456789';
        insert con;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c ='cus_Eug8juCETidS33';
        sp.Customer__c = con.id;
        insert sp;
        
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        
        
        Product2 pro = new Product2();
        pro.Name='Stripe Product name';
        pro.Price__c=39.93;
        pro.Family = 'Hardware';
        pro.Stripe_Plan_Id__c='1yrmembership';
        pro.IsActive=true;
        insert pro; 
        
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id =standardPricebook.Id, Product2Id = pro.Id,
            UnitPrice = 100000, IsActive = true);
        insert standardPrice;
        
        
        
        
        SubscriptionsControllerHub.refreshSubscription(String.valueof(con.Id));
    }
}