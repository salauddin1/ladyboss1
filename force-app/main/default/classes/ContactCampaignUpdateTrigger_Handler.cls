public class ContactCampaignUpdateTrigger_Handler {
    
    
    public static void movetoPipelineCampaigns(Map<Id,Contact> conNewMap,Map<Id,Contact> conOldMap){
        List<String> highestProductSet = new List<String>();
        Set<Id> conIdSet = new Set<Id>();
        for(Contact con:conNewMap.values()){
            conIdSet.add(con.Id);
            System.debug('----New product--'+conNewMap.get(con.id).Product__c);
            System.debug('----New Highest--'+conNewMap.get(con.id).Highest_Product__c);
            System.debug('----old Highest--'+conOldMap.get(con.Id).Highest_Product__c);
            if(conNewMap.get(con.id).Highest_Product__c != conOldMap.get(con.Id).Highest_Product__c){
                highestProductSet.add(conNewMap.get(con.id).Highest_Product__c);        
            }
        } 
        
        Map<String,Campaign> mapPipeLineCampaign = new Map<String,Campaign>();
        for(Campaign cmp:[Select id, Product__c from Campaign where Campaign.Type='Pipeline' and Product__c in:highestProductSet]){
            mapPipeLineCampaign.put(cmp.Product__c,cmp);    
        } 
        
        List<CampaignMember> campList = new List<CampaignMember>();
        campList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where contactId in:conIdSet and campaign.Type='Phone Team'];
        
        List<CampaignMember> pipelineCampignMembers = new List<CampaignMember>();
        pipelineCampignMembers = [select id,Campaign.Product__c,contactId from CampaignMember where contactId in:conIdSet and Campaign.Type='Pipeline'];
        System.debug('');
        List<CampaignMember> pipelineCampaignMembers  = new List<CampaignMember>();
        List<CampaignMember> pipelineCampaignMembersToDelete  = new List<CampaignMember>();
        
        for(Contact con:conNewMap.values()){ 
            Boolean doesExistIntoHighestPipeline = false;
            if(pipelineCampignMembers!=null){
                for(CampaignMember cmp:pipelineCampignMembers){
                    if(cmp.ContactId ==con.Id && cmp.Campaign.Product__c ==con.Highest_Product__c){
                        doesExistIntoHighestPipeline=true;
                    }
                    if(cmp.ContactId ==con.Id && cmp.Campaign.Product__c !=con.Highest_Product__c){
                        pipelineCampaignMembersToDelete.add(cmp);
                    }
                    
                }
            }
            if(doesExistIntoHighestPipeline==false && con.Highest_Product__c!=null && mapPipeLineCampaign.containsKey(con.Highest_Product__c)){
                CampaignMember cmp = new CampaignMember();
                cmp.CampaignId=mapPipeLineCampaign.get(con.Highest_Product__c).Id;
                cmp.ContactId =con.Id;
                pipelineCampaignMembers.add(cmp);
            }
        }
        
        system.debug('----pipelineCampaignMembers----'+pipelineCampaignMembers);
        
        if(pipelineCampaignMembers.size()>0){
            Database.insert(pipelineCampaignMembers,false);
        }
        
        if(campList.size()>0){
            if(!test.isrunningTest()){
                CampaignConnectionHelper.getCampOppByContactUpdateTrigger(campList, conIdSet);
                system.debug('==campList= test=='+campList+'=========conIdSet======'+conIdSet );
            }
            delete campList;
        }
        
        if(pipelineCampaignMembersToDelete.size()>0){
            delete pipelineCampaignMembersToDelete;
        }
    }
    
    Public Static void doAfterInsert(List<Contact> contactList){
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        Map<Id,Contact> cContactMap = new Map<Id,Contact>();
        for(Contact con : contactList){
            if(con.Website_Products__c != null){
                contactMap.put(con.Id, con);
            }else if(con.Website_Products__c == null && con.Potential_Buyer__c != null ){
                cContactMap.put(con.Id,con);
            }
        }
        if(!contactMap.isEmpty()){
            updateCampaignMembers(contactMap);
        }else if(contactMap.isEmpty() && !cContactMap.isEmpty()){
            addPotentialBuyers(cContactMap);
        }          
    }
    
    
    
    Public Static void doBeforeUpdate(List<Contact> contactList , Map<Id,Contact> contactIdMap){
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        Map<Id,Contact> cContactMap = new Map<Id,Contact>();
        
        for(Contact con : contactList){
           if(con.Website_Products__c != null){
                if(con.Website_Products__c != contactIdMap.get(con.Id).Website_Products__c)
                    contactMap.put(con.Id, con);
            }else if(con.Website_Products__c == null && con.Potential_Buyer__c != null ){
                cContactMap.put(con.Id,con);
            }
        }
        system.debug(cContactMap);
        if(!contactMap.isEmpty()){
            updateCampaignMembers(contactMap);
        }else if(contactMap.isEmpty() && !cContactMap.isEmpty()){
            addPotentialBuyers(cContactMap);
        }  
    }
    
    Public Static void updateCampaignMembers(Map<Id,Contact> contactMap){
        Map<String,List<String>> campaignContactMap = new Map<String,List<String>>();
        Set<CampaignMember> targetCampaignMembersSet = new Set<CampaignMember>();        
        Set<String> cMemberContactIdSet = new Set<String>();
        Set<CampaignMember> cMemberDeletionSet = new Set<CampaignMember>();
        Map<String,List<Workflow_Configuration__c>> sourceConfigurationMap = new Map<String,List<Workflow_Configuration__c>>();
        
        System.debug('contactMap---'+contactMap);
        Set<String> campaignIdSet = new Set<String>();
        List<CampaignMember> cMemberList = [SELECT Id, CampaignId,Campaign.Name,Campaign.Type,ContactId FROM CampaignMember WHERE ContactId IN :contactMap.keySet() And ( Campaign.Type ='Phone Team' OR Campaign.Type ='Pipeline' ) ];
        
        
        Map<Id,List<CampaignMember>> contactPipelineCampaignMembersMap = new Map<Id,List<CampaignMember>>();
        
        System.debug('======cMemberList======'+cMemberList);
        if(!cMemberList.isEmpty()){
            System.debug('======cMemberList======'+cMemberList);
            for(CampaignMember cm: cMemberList){
                if(cm.Campaign.Type == 'Pipeline'){
                    if(contactPipelineCampaignMembersMap.containsKey(cm.ContactId)){
                        contactPipelineCampaignMembersMap.get(cm.ContactId).add(cm);
                    }else{
                        contactPipelineCampaignMembersMap.put(cm.ContactId,new List<CampaignMember>{cm});
                    }
                }
                System.debug('======cm.Campaign.Name======'+cm.Campaign.Name);
                if(cm.Campaign.Name.Contains('Day')  && (cm.Campaign.Name.Contains('Buy') || cm.Campaign.Name.Contains('Lead')))
                    campaignIdSet.add(cm.CampaignId);
            }
            System.debug('cMemberList---'+cMemberList);
            System.debug('cMemberList---'+cMemberList.size());
            if(!campaignIdSet.isEmpty()){
                for(Workflow_Configuration__c wc : [SELECT Id, Source_Campaign__c, Target_Campaign__c, Products__c FROM Workflow_Configuration__c WHERE Source_Campaign__c IN :campaignIdSet AND Active__c=true]){
                    if(sourceConfigurationMap.containsKey(wc.Source_Campaign__c))
                        sourceConfigurationMap.get(wc.Source_Campaign__c).add(wc);
                    else
                        sourceConfigurationMap.put(wc.Source_Campaign__c, new List<Workflow_Configuration__c>{wc});
                }
                System.debug('sourceConfigurationMap---'+sourceConfigurationMap);
                System.debug('sourceConfigurationMap---'+sourceConfigurationMap.size());
                
                
                for(CampaignMember cm : cMemberList){
                    if(contactMap.containsKey(cm.ContactId) && sourceConfigurationMap.containsKey(cm.CampaignId)){
                        
                        for(Workflow_Configuration__c wc : sourceConfigurationMap.get(cm.campaignId)){
                            Contact con = contactMap.get(cm.ContactId);
                            String productName = con.Website_Products__c.Split(';').get(con.Website_Products__c.Split(';').size()-1);
                            if(wc.Source_Campaign__c == cm.campaignId && wc.Products__c == productName){
                                System.debug('wc.Source_Campaign__c-->'+wc.Source_Campaign__c);
                                System.debug('lcw.campaignIdc-->'+cm.campaignId);
                                cMemberContactIdSet.add(cm.ContactId);
                                if(campaignContactMap.containsKey(wc.Target_Campaign__c)){
                                    campaignContactMap.get(wc.Target_Campaign__c).add(cm.ContactId);
                                }else
                                    campaignContactMap.put(wc.Target_Campaign__c, new List<String>{cm.ContactId});
                            }
                        }
                    }
                }
 
                
                for(CampaignMember cm : cMemberList){
                    for(String contactId : cMemberContactIdSet){
                        if(cm.ContactId == contactId && !cMemberDeletionSet.contains(cm)){
                            cMemberDeletionSet.add(cm);
                        }
                    }
                }
                System.debug('campaignContactMap---'+campaignContactMap);
                Map<String,String> existingCMMap = new Map<String,String>();
                for(String targetId : campaignContactMap.keySet()){
                    for(String contactId : campaignContactMap.get(targetId)){
                        if(!contactPipelineCampaignMembersMap.containsKey(contactId)) {
                            CampaignMember cm = new CampaignMember();
                            cm.ContactId = contactId;
                            cm.CampaignId = targetId;
                            existingCMMap.put(targetId, contactId);
                            if(!targetCampaignMembersSet.contains(cm))
                                targetCampaignMembersSet.add(cm);
                        }
                    }
                }
                
                System.debug('targetCampaignMembersSet---'+targetCampaignMembersSet);
                System.debug('cMemberDeletionSet---'+cMemberDeletionSet);
                
                List<CampaignMember> cmExistingList = [SELECT Id, CampaignId, ContactId FROM CampaignMember WHERE ContactId IN :existingCMMap.values() AND CampaignId IN :existingCMMap.keySet() And ( Campaign.Type ='Phone Team' OR Campaign.Type ='Pipeline' )];
                List<CampaignMember> deletionList = new List<CampaignMember>(cMemberDeletionSet);
                if(!cmExistingList.isEmpty()){
                    deletionList.addAll(cmExistingList);
                }
                try{
                    Database.delete( deletionList);
                    try{
                        Database.upsert(new List<CampaignMember>(targetCampaignMembersSet));
                        
                    }Catch(Exception ex){
                        System.debug('Upsert Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                    }
                }Catch(Exception ex){
                    System.debug('XDelete Exception --->'+ex.getMessage()+''+ex.getLineNumber());
                }                        
            }
        }else{
            
            Map<String,String> productContactMap = new Map<String,String>();
            Map<String,String> productCampaignMap = new Map<String,String>();
            List<String> productList = new List<String>();
            Set<CampaignMember> cmList = new Set<CampaignMember>();
            for(String lid : contactMap.keySet()){
                Contact ld = contactMap.get(lid);
                String productName = ld.Website_Products__c.Split(';').get(ld.Website_Products__c.Split(';').size()-1);
                productContactMap.put(productName, lid);
                productList.add(productName);
            }
            system.debug('productContactMap-->'+productContactMap);
            system.debug('productList-->'+productList);
            if(!productList.isEmpty()){
                for(Workflow_Configuration__c wc : [SELECT Id, Source_Campaign__c, Target_Campaign__c, Products__c FROM Workflow_Configuration__c WHERE Products__c IN :productList AND Active__c=true]){
                    if(!productCampaignMap.containsKey(wc.Products__c)&& wc.Target_Campaign__c != null )
                        productCampaignMap.put(wc.Products__c,wc.Target_Campaign__c);
                }
                if(!productCampaignMap.isEmpty()){
                    for(String p : productContactMap.keySet()){
                        CampaignMember cm = new CampaignMember();
                        cm.ContactId = productContactMap.get(p);
                        cm.CampaignId =productCampaignMap.get(p);
                        system.debug('cm--->'+cm);
                        cmList.add(cm);
                    }
                    if(!cmList.isEmpty()){
                        system.debug('cmList--->'+cmList);
                        //if(cMemberDeletionSet == null && cMemberDeletionSet.isEmpty()){
                        insert new List<CampaignMember>(cmList);
                        //}
                    }
                }
            }
        }
    }
    
    public static void addPotentialBuyers(Map<Id,Contact> contactMap){
        Map<String,CampaignMember> delCMMap = new Map<String,CampaignMember>();
        Set<String> potentialCampaignSet = new Set<String>();
        Map<String,String> productContactMap = new Map<String,String>();
        Map<String,Campaign> campaignNameMap = new Map<String,Campaign>(); 
        Set<CampaignMember> insertCMSet = new Set<CampaignMember >();
        
        for(CampaignMember cm : [SELECT Id, CampaignId,Campaign.Name, ContactId FROM CampaignMember WHERE ContactId IN :contactMap.keySet() And ( Campaign.Type ='Phone Team' OR Campaign.Type ='Pipeline' )]){  
            delCMMap.put(cm.Id,cm);
        }
        system.debug('delCMMap--->'+delCMMap);
        
        for(String cid : contactMap.keySet()){
            Contact con = contactMap.get(cid);
            String productName = con.Potential_Buyer__c;
            system.debug('potential buyer --->'+productName);
            potentialCampaignSet.add(productName);
            productContactMap.put(cid, productName);
        }
        system.debug('potentialCampaignSet--> '+potentialCampaignSet);
        system.debug('productContactMap--> '+productContactMap);
        
        for(Campaign c : [SELECT Id,Name From Campaign WHERE Name IN :potentialCampaignSet]){
            campaignNameMap.put(c.Name, c);
        }
        system.debug('campaignNameMap---> '+campaignNameMap);
        
        if(!productContactMap.isEmpty()){
            for(String cid : productContactMap.keySet()){
                string cname = productContactMap.get(cid);
                system.debug('==cname==='+cname+'==============='+campaignNameMap.get(cname) );
                if(cname != null && campaignNameMap.get(cname) != null){
                    Campaign c = campaignNameMap.get(cname);
      
                   CampaignMember cm = new CampaignMember();
                    cm.CampaignId = c.Id;
                    cm.ContactId = cid;
                    insertCMSet.add(cm);
                }
            }
            system.debug('insertCMSet---> '+insertCMSet);
        }
        
        List<CampaignMember> deleteList = delCMMap.values();
        system.debug('deleteList--->'+deleteList);
        if(!deleteList.isEmpty()){
            try{
                delete deleteList;
                if(!insertCMSet.isEmpty()){
                    try{
                        insert new List<CampaignMember>(insertCMSet);
                    }catch(Exception ex){ 
                        system.debug(ex.getMessage()+ex.getLineNumber());
                    }
                }
            }catch(Exception ex){
                system.debug(ex.getMessage()+ex.getLineNumber());
            }
        }else if(!insertCMSet.isEmpty()){
            insert new List<CampaignMember>(insertCMSet);
            /*
try{
insert new List<CampaignMember>(insertCMSet);
}catch(Exception ex){ 
system.debug(ex.getMessage()+ex.getLineNumber());
}
*/
        }
    }
}