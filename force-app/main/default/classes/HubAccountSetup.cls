public without sharing class HubAccountSetup{
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    
    //To Get Customer Details From Salesforce
    @AuraEnabled
    public static ContactListWrapper getcontact (String conId) {
        ContactListWrapper conWrapper = new ContactListWrapper();
        List<Contact> conList =new List<Contact>();
        if(conId!=null && conId!=''){
            conList = [SELECT id,FirstName,LastName,Email,Phone FROM Contact WHERE id = :conId];
        }
        if(conList.size() > 0){
            conWrapper.contactList = conList[0];
        }
        return conWrapper;
    }
    
    //To Get Address Details
    @AuraEnabled
    public static AddressListWrapper getAddress (String conId) {
        AddressListWrapper addWrapper = new AddressListWrapper();
        List <Address__c> addList = new List <Address__c>();
        if(conId!=null && conId!=''){
            addList = [SELECT Id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c =: conId AND Primary__c = true];
        }
        if(addList.size() > 0){
            String addre = addList[0].Shipping_Zip_Postal_Code__c;
            AddWrapper.AddressList = addList[0];
        }
        return addWrapper;
    }
    
    @Auraenabled	
    public static string validateAddress(string street,string city,string state,string country , string zipCode){	
        String adr = AddressValidation.resolveAddress(street,city,state,country,zipCode);	
        System.debug('adr : '+adr);	
        return adr;	
    }
    
    //To update customer Info in Salesforce
    @Auraenabled
    public static string updateInfoUser(string idparam,string firstname,string lastName,string phone, string street,string city,string state,string country , string zipCode){
        string registerSuccessMsg='';
        List<Contact> conList1 = [SELECT id,FirstName,LastName,Email,Phone,MailingStreet,MailingCity, MailingState,MailingPostalCode,MailingCountry FROM Contact WHERE id = :idparam];
        string email = conList1[0].email;
        if(email != null && email != ''){
            Address__c add;
            
            List<Address__c> addList = [SELECT Id,Shipping_City__c,Shipping_Country__c,Shipping_State_Province__c,Shipping_Street__c,Shipping_Zip_Postal_Code__c FROM Address__c WHERE Contact__c =: idparam AND Primary__c = true];
            if (conList1.size()>0) {
                Contact con = conList1[0];
                con.FirstName = firstname;
                con.LastName = lastName;
                con.Email = email;
                con.MailingStreet = street;	
                con.MailingCity = city;	
                con.MailingState= state;	
                con.MailingPostalCode = zipCode;	
                con.MailingCountry= country;
                if (addList.size()>0) {
                    add = addList[0];
                    add.Shipping_Street__c = street;
                    add.Shipping_City__c = city;
                    add.Shipping_Country__c = country;
                    add.Shipping_State_Province__c = state;
                    add.Shipping_Zip_Postal_Code__c = zipCode;
                    update add;
                }else {
                    if (street != '' && city != '' && state != '' && zipCode != '' && country != '') {
                        add = new Address__c();
                        add.Shipping_Street__c = street;
                        add.Shipping_City__c = city;
                        add.Shipping_Country__c = country;
                        add.Shipping_State_Province__c = state;
                        add.Shipping_Zip_Postal_Code__c = zipCode;
                        add.Primary__c = true;
                        add.Contact__c = con.Id;
                        insert add;
                    }else {
                        registerSuccessMsg = 'Address field is incomplete, address not saved.';
                    }
                }
                if (phone!=null) {
                    con.Phone = phone;
                } 
                string contactname = con.FirstName + ' '+con.LastName;
                update con;
                List< Stripe_Profile__c > sfList = [select id, Stripe_Customer_Id__c from Stripe_Profile__c where Customer__c=:con.id];
                Set<string> custIdSet = new Set<string>();
                if(sfList.size() > 0){
                    for(Stripe_Profile__c sp :sfList){
                        custIdSet.add(sp.Stripe_Customer_Id__c);
                    }
                }
                if(custIdSet.size() > 0){
                    for(string sp :custIdSet){
                        HubAccountSetup.updateProfile(sp,firstname,lastName,phone,street,city,country,state,zipCode);
                    }
                    
                }
                //error = false;
                Boolean regiterSuccess = true;
                if (registerSuccessMsg != null && registerSuccessMsg != '') {
                    registerSuccessMsg = 'Succesfully updated Details, But '+registerSuccessMsg; 
                }else {
                    registerSuccessMsg = 'Succesfully updated Details'; 
                }
            }
            
        }
        return registerSuccessMsg;
    }
    
    //To Update Customer info in stripe 
    @future(callout=true)
    public static void updateProfile(String customerId,String firstName, String Lastname, String phoneNumTxt, String street,string city,string state,string countrytxt,String zipCodetxt) { 
        if(customerId!=null && customerId!=''){
            string endPointUrl = 'https://api.stripe.com/v1/customers/'+customerId;
            HttpRequest httpReq = new HttpRequest();
            httpReq.setEndpoint(endPointUrl);
            httpReq.setMethod('POST');
            httpReq.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
            Map<String, String> updateCustomerPayload = new Map<string,string>{'name' =>firstName +' '+Lastname };
                String nameString = StripeUtil.urlify(updateCustomerPayload);
            String metaName = '';
            if(firstName!='' || Lastname!=''){
                metaName = '&metadata[Full Name]='+firstName+' '+Lastname+'&';
            }
            String phoneNum ='' ;
            
            phoneNum = '&metadata[Phone]='+phoneNumTxt+'&';
            string shippingAdd='';
            if (street != '' && city != '' && state != '' && countrytxt != '' && zipCodetxt != '') {
                shippingAdd = '&metadata[Shipping Address]='+street+' '+city +' '+state +' '+countrytxt+' '+ zipCodetxt+'&';
            }
            
            String reqBody = '';
            
            if(nameString!=''){
                reqBody+=nameString;          
            }
            if(metaName!=''){
                reqBody+=metaName;          
            }
            if(phoneNum!='' && (phoneNumTxt!=null && phoneNumTxt!='')){
                reqBody+=phoneNum;          
            }
            if(shippingAdd!=''){
                reqBody+=shippingAdd;          
            }
            
            httpReq.setBody(reqBody);
            
            Integer statusCode1;
            Http con1 = new Http();
            HttpResponse hs1 = new HttpResponse();
            
            //send the request body
            if(!Test.isRunningTest()){
                hs1 = con1.send(httpReq); 
            }
        }
        
    }
    
    //To Get All Cards of Customer to show on "Update Card & Shipping" Tab
    @AuraEnabled
    public static List<StripeCardWrapper> getAllCards (String conId) {
        List <StripeCardWrapper> wrapList = new List <StripeCardWrapper>();
        set <string> stripeProfileId    = new set <string>();
        set <string> cardId    = new set <string>();
        List <Card__c> listCard = new List <Card__C>();
        Map <string,List<Card__c>> cardMap = new Map <string,List<Card__C>>();
        Map <string,List<Opportunity>> oppMap = new Map <string,List<Opportunity>>();
        List <Opportunity> allSubscriptionList = new List <Opportunity>();
        listCard =[select id,Name,Contact__c,Credit_Card_Number__c,Last4__c,Card_ID__c from Card__c where Contact__c =: conId];
        if(listCard.size()>0){
            for(Card__c card : listCard){
                cardId.add(card.Id);
            }
        }
        //allSubscriptionList = [select id,Name,Status__c,RecordType.Name,CustomerID__c,Card__c from Opportunity where Card__c =: cardId and RecordType.Name =: 'Subscription'];
        allSubscriptionList = [select id, Name, Status__c, RecordType.Name, CustomerID__c, Card__c, (Select id,Status__c,Product2.Name, Opportunity.RecordType.Name, OpportunityId, Opportunity.CustomerID__c, Opportunity.Card__c from opportunityLineItems where  Subscription_Id__c !=null AND ( Status__c = 'Active' OR Status__c = 'past due' OR Status__c = 'trialing') ) from Opportunity where Card__c =: cardId and RecordType.Name =: 'Subscription' AND  Contact__c != null ];
        List <Opportunity> oppMainList = new List <Opportunity>();
        
        if(allSubscriptionList.size() > 0 ){
            for(opportunity opp : allSubscriptionList ){
                if(opp.OpportunityLineItems != null && ! opp.OpportunityLineItems.isEmpty() ){ 
                    oppMainList.add( opp); 
                }
                else {
                    if(opp.Status__c != null &&  ( opp.Status__c == 'Active' || opp.Status__c == 'past due' || opp.Status__c == 'trialing') && ( opp.Status__c != 'unpaid' && opp.Status__c != 'canceled' && opp.Status__c != 'incomplete' && opp.Status__c != 'expired' ) ){
                        oppMainList.add(opp);    
                    }
                    
                }
            }
        }
        if(oppMainList.size()>0){
            for(Opportunity Opp : oppMainList){
                if(oppMap.containskey(opp.Card__c)){
                    oppMap.get(opp.Card__c).add(opp); 
                }else{
                    oppMap.put(opp.Card__c,new List<Opportunity>{opp});
                }
            }
        }     
        if(listCard.size()>0){
            for(Card__c card : listCard){
                if(oppMap != null && oppMap.containsKey(card.id) && oppMap.get(card.id) != null){
                    system.debug('======card.Last4__c====='+card.Last4__c);
                    if(cardMap.containskey(card.Last4__c)){
                        cardMap.get(card.Last4__c).add(card); 
                    }else{
                        cardMap.put(card.Last4__c,new List<Card__c>{card});
                    }
                }
            }
        }
        for (String cardNumber : cardMap.keySet()){
            system.debug('=========cardNumber=========='+cardNumber);
            StripeCardWrapper subWrapper = new StripeCardWrapper(cardMap.get(cardNumber),false);
            wrapList.add(subWrapper);
        }
        return wrapList;
    }
    
    //To Get All Subscriptions From selected Card
    @AuraEnabled
    public static List<subscribeWrapper> getCardSubscription (string cardId,string conId) {
        system.debug('==cardId=='+cardId);
        List <subscribeWrapper> wrapList = new List <subscribeWrapper>();
        List<card__c> listCard          = new List<card__c>();
        List <Opportunity> allSubscriptionList = new List <Opportunity>();
        listCard =[select id,Name,Contact__c,Last4__c,Credit_Card_Number__c,Card_ID__c from Card__c where Contact__c =: conId AND Id =: cardId];
        //allSubscriptionList = [select id,Name,Status__c,RecordType.Name,CustomerID__c,Card__c from Opportunity where Card__c =: listCard[0].id and RecordType.Name =: 'Subscription'];
        
        allSubscriptionList = [select id,Name,Status__c, RecordType.Name,CustomerID__c,Card__c, (Select id,Status__c,Product2.Name, Opportunity.RecordType.Name, OpportunityId, Opportunity.CustomerID__c, Opportunity.Card__c from opportunityLineItems where  Subscription_Id__c !=null AND ( Status__c = 'Active' OR Status__c = 'past due' OR Status__c = 'trialing') ) from Opportunity where Card__c =: listCard[0].id and RecordType.Name =: 'Subscription' AND  Contact__c != null ];
        Map <String, List<OpportunityLineItem>> subOppLineItemMap = new Map <String, List<OpportunityLineItem>> ();
        List <OpportunityLineItem> oppLineItem = new List<OpportunityLineItem>(); 
        
        
        List <Opportunity> oppMainList = new List <Opportunity>();
        
        if(allSubscriptionList.size() > 0 ){
            for(opportunity opp : allSubscriptionList ){
                system.debug('=======opp name ==='+opp.Name);
                if(opp.OpportunityLineItems != null && ! opp.OpportunityLineItems.isEmpty() ){ 
                    oppMainList.add( opp);
                    system.debug('=======opp.OpportunityLineItems========='+opp.OpportunityLineItems);
                    //      	subOppLineItemMap.put(opp.id, opp.OpportunityLineItems );    
                }
                else {
                    if(opp.Status__c != null &&  ( opp.Status__c == 'Active' || opp.Status__c == 'past due' || opp.Status__c == 'trialing') && ( opp.Status__c != 'unpaid' && opp.Status__c != 'canceled' && opp.Status__c != 'incomplete' && opp.Status__c != 'expired' ) ){
                        oppMainList.add(opp);    
                    }
                    
                }
            }
        }
        
        if(oppMainList.size() > 0){
            subscribeWrapper subWrapper = new subscribeWrapper(oppMainList,true);
            wrapList.add(subWrapper);
        }        
        return wrapList;
    }
    
    //To Get All Subscription Name to show with card  
    @AuraEnabled
    public static List <subscriptionCardWrapper> getAllSubscriptionName (List<string> cardList) {
        system.debug('=====card======'+cardList);
        List <Opportunity> allSubscriptionList = new List <Opportunity>();
        List <Card__c> cardObjectList = new List<Card__c>();
        Map <string,List<Opportunity>> oppMap = new Map <string,List<Opportunity>>();
        List <subscriptionCardWrapper> wrapList = new List <subscriptionCardWrapper>();
        set <string> cardId = new set <string>();
        if(cardList.size()>0){
            for(string card : cardList){
                cardId.add(card);
            }
        }
        cardObjectList = [select Id,Last4__c from Card__c where id =: cardId];
        //allSubscriptionList = [select id,Name,Status__c,RecordType.Name,CustomerID__c,Card__c from Opportunity where Card__c =: cardId and RecordType.Name =: 'Subscription'];
        allSubscriptionList = [select id, Name, Status__c, RecordType.Name, CustomerID__c, Card__c, (Select id,Status__c,Product2.Name, Opportunity.RecordType.Name, OpportunityId, Opportunity.CustomerID__c, Opportunity.Card__c from opportunityLineItems where  Subscription_Id__c !=null AND ( Status__c = 'Active' OR Status__c = 'past due' OR Status__c = 'trialing') ) from Opportunity where Card__c =: cardId and RecordType.Name =: 'Subscription' AND  Contact__c != null ];
        List <Opportunity> oppMainList = new List <Opportunity>();
        if(allSubscriptionList.size() > 0 ){
            for(opportunity opp : allSubscriptionList ){
                if(opp.OpportunityLineItems != null && ! opp.OpportunityLineItems.isEmpty() ){ 
                    oppMainList.add( opp); 
                }
                else {
                    if(opp.Status__c != null &&  ( opp.Status__c == 'Active' || opp.Status__c == 'past due' || opp.Status__c == 'trialing') && ( opp.Status__c != 'unpaid' && opp.Status__c != 'canceled' && opp.Status__c != 'incomplete' && opp.Status__c != 'expired' ) ){
                        oppMainList.add(opp);    
                    }
                    
                }
            }
        }
        if(oppMainList.size()>0){
            for(Opportunity Opp : oppMainList){
                if(oppMap.containskey(opp.Card__c)){
                    oppMap.get(opp.Card__c).add(opp); 
                }else{
                    oppMap.put(opp.Card__c,new List<Opportunity>{opp});
                }
            }
        }     
        string subName;
        if(cardList.size()>0){
            for(Card__c card : cardObjectList){
                system.debug('=========card=========='+card.id);
                for(Opportunity opp : oppMap.get(card.id)){
                    if(!opp.OpportunityLineItems.isempty() && opp.OpportunityLineItems != null){
                        system.debug('====opp lineitem======='+opp.OpportunityLineItems);
                        
                        for(Integer i=0;i<opp.OpportunityLineItems.size();i++){
                            system.debug('oppLine'+opp.OpportunityLineItems[i].Product2.Name);
                            if(oppMap.get(card.id).size()>1){
                                string [] sublist ;
                                if(subName != null && subName != ''){
                                    sublist = (opp.OpportunityLineItems[i].Product2.Name).split(' ');
                                    subName += ','+ sublist[0];
                                }
                                else{
                                    sublist = (opp.OpportunityLineItems[i].Product2.Name).split(' ');
                                    subName = sublist[0];
                                }
                            }
                            else{
                                subName = opp.OpportunityLineItems[i].Product2.Name;
                            }
                        }
                    }
                    else{
                        system.debug('=====opp name====='+opp.Name);
                        if(oppMap.get(card.id).size()>1){
                            string [] sublist ;
                            if(subName != null && subName != ''){
                                sublist = (opp.Name).split(' ');
                                subName += ','+ sublist[0];
                            }
                            else{
                                sublist = (opp.Name).split(' ');
                                subName = sublist[0];
                            }
                        }
                        else{
                            subName = opp.Name;
                        }
                    }
                }
                if(subName != null && subName != ''){
                    subName = '"'+subName+'"';
                }
                subscriptionCardWrapper subWrapper = new subscriptionCardWrapper(card,subName);
                wrapList.add(subWrapper);
                subName = '';
            }
        }
        return wrapList;
    }
    
    //To Update Shipping Address in Salesforce And Stripe customer
    @AuraEnabled
    public static void AddShipping (string Street,Id cardCheck,string City,string State,string PostalCode, string Country, string recordId) {
        // if (Street != '' && city != '' && state != '' && countrytxt != '' && zipCodetxt != '') {
        card__c Card          = new card__c();
        Address__c add = new Address__c();
        Stripe_Profile__c stripeProfile = new Stripe_Profile__c(); 
        Contact cont = new contact();
        
        //update Mailing address in SF Contact
        cont = [select id,Stripe_Customer_Id__c,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from contact where id =: recordId limit 1];
        cont.MailingStreet = street;
        cont.MailingCity = city;
        cont.MailingState= state;
        cont.MailingPostalCode = PostalCode;
        cont.MailingCountry= country;
        
        //update Mailing address in SF ContactCard
        Card = [select id,Billing_City__c,Billing_Country__c,Billing_Street__c,Billing_Zip_Postal_Code__c,Billing_State_Province__c,Stripe_Card_Id__c,Stripe_Profile__c from card__c where id =: cardCheck limit 1];
        card.Billing_City__c = city;
        card.Billing_Country__c = country;
        card.Billing_Street__c = Street;
        card.Billing_Zip_Postal_Code__c = PostalCode;
        card.Billing_State_Province__c = State;
        
        //Insert Mailing address in SF ContactAddress Record
        add.Contact__c = recordId;
        add.Shipping_City__c = city;
        add.Shipping_Country__c = country;
        add.Shipping_Zip_Postal_Code__c = PostalCode;
        add.Shipping_Street__c = Street;
        add.Shipping_State_Province__c = State;
        add.Primary__c = true;
        
        stripeProfile = [Select id,Stripe_Customer_Id__c from Stripe_Profile__c where id =: card.Stripe_Profile__c limit 1];
        //update Mailing address in Stripe Contact
        if(stripeProfile.Stripe_Customer_Id__c !=null && stripeProfile.Stripe_Customer_Id__c!=''){
            string endPointUrl = 'https://api.stripe.com/v1/customers/'+stripeProfile.Stripe_Customer_Id__c;
            HttpRequest httpReq = new HttpRequest();
            httpReq.setEndpoint(endPointUrl);
            httpReq.setMethod('POST');
            httpReq.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
            string shippingAdd='';
            if (Street != '' && city != '' && State != '' && country != '' && PostalCode != '') {
                shippingAdd = '&metadata[Shipping Address]='+Street+' '+city +' '+State +' '+country+' '+ PostalCode+'&';
            }
            String reqBody = '';
            if(shippingAdd!=''){
                reqBody+=shippingAdd;          
            }
            httpReq.setBody(reqBody);
            Integer statusCode1;
            Http con1 = new Http();
            HttpResponse hs1 = new HttpResponse();
            
            //send the request body
            if(!Test.isRunningTest()){
                hs1 = con1.send(httpReq); 
            }
        }
        
        //update Mailing address in SF ContactCard
        if(stripeProfile.Stripe_Customer_Id__c !=null && stripeProfile.Stripe_Customer_Id__c!='' && Card.Stripe_Card_Id__c != null && Card.Stripe_Card_Id__c != ''){
            system.debug('========================card update in stripe================================');
            HttpRequest httpReq = new HttpRequest();
            string Cardurl =  'https://api.stripe.com/v1/customers/'+stripeProfile.Stripe_Customer_Id__c +'/sources/'+Card.Stripe_Card_Id__c ;
            httpReq.setEndpoint(Cardurl );
            httpReq.setMethod('POST');
            httpReq.setHeader('Authorization', 'bearer '+ StripeAPI.ApiKey);
            
            Map<String, String> payload = new Map<String, String>{'address_zip' =>PostalCode,'address_city' =>City ,
                'address_country' => Country ,
                'address_state' => State,'address_zip' => String.valueOf(PostalCode),
                'address_line1' => Street
                };
                    system.debug('StripeUtil.urlify(payload ) :'+StripeUtil.urlify(payload ));
            httpReq.setBody(StripeUtil.urlify(payload ));
            
            String resp;
            // Integer statusCode;
            Http ht = new Http();
            HttpResponse httpRes = new HttpResponse();
            try{
                httpRes = ht.send(httpReq);
                resp = httpRes.getBody();
                // return statusCode = hs.getStatusCode();
                system.debug('========resp==========='+resp);
            }catch(exception e){}
        }
        insert add;
        update cont;
        update card;
    }
    
    //=======All Wrapper Classes=============
    public class ContactListWrapper{
        @AuraEnabled
        public Contact contactList{get;set;}
    }
    public class AddressListWrapper{
        @AuraEnabled
        public Address__c AddressList{get;set;}
    }
    public class subscriptionCardWrapper{
        @AuraEnabled
        public card__c cardList{get;set;}
        @AuraEnabled
        public string SubscriptionName{get;set;}
        
        public subscriptionCardWrapper(card__c cardList,string SubscriptionName) {
            this.cardList = cardList;
            this.SubscriptionName = SubscriptionName;
        }
    }
    public class StripeCardWrapper{
        @AuraEnabled
        public List <card__c> cardList{get;set;}
        @AuraEnabled
        public Boolean SubscriptionOnCard{get;set;}
        
        public StripeCardWrapper(List <card__c> cardList,boolean SubscriptionOnCard) {
            this.cardList = cardList;
            this.SubscriptionOnCard = SubscriptionOnCard;
        }
    }
    public class subscribeWrapper{
        @AuraEnabled
        public List <Opportunity> subscriptionList{get;set;}
        @AuraEnabled
        public Boolean multipleSubscription{get;set;}
        
        public subscribeWrapper(List <Opportunity> subscriptionList, boolean multipleSubscription) {
            this.subscriptionList = subscriptionList;
            this.multipleSubscription = multipleSubscription;
        }
    }
    
}