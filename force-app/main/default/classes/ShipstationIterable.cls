global class ShipstationIterable implements iterable<Integer>{
   global Iterator<Integer> Iterator(){
      return new ShipstationOrderIterator();
   }
}