/*
 * Developer Name : Tirth Patel
 * Description :  This batch sends sms right away and since we are past 8 pm schedule it to be 8 am next day
*/
global class CampaignSmsBatch implements Database.Batchable<sObject>,Schedulable {
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select id,Age__c,Campaign__c,From_Number__c,SMS_Template__c from Campaign_Stage__c '+
                                        'where Campaign__c != null AND Age__c != null AND SMS_Template__c !=null');
    }
    global void execute(SchedulableContext ctx) {
        CampaignSmsBatch sdbatch = new CampaignSmsBatch();
        database.executebatch(sdbatch,1);
    } 
    
    global void execute(Database.BatchableContext BC, List<Campaign_Stage__c> scope){
        system.debug('----campaign---'+scope);
        List<Id> campaignIdList = new List<Id>();
        List<String> templateNameList = new List<String>();
        Map<String,Campaign_Stage__c> campaignStageMap = new  Map<String,Campaign_Stage__c>();
        List<Campaign_Sms_Time__c> cst = [select id,Start_Hour__c,End_Hour__c from Campaign_Sms_Time__c];
   		List<Integer> lstInteger = new List<Integer>();
        for(Campaign_Stage__c campaignStage : scope){
            campaignIdList.add(campaignStage.Campaign__c);
            campaignStageMap.put(campaignStage.id, campaignStage);
            templateNameList.add(campaignStage.SMS_Template__c);
            if(campaignStage.Age__c != null)  {
            	lstInteger.add(Integer.valueOf(campaignStage.Age__c));
            }
        }
        
        Map<String,EmailTemplate> emailtemplateMap = new  Map<String,EmailTemplate>();
        List<EmailTemplate> emailtemplateList = [select id,body,DeveloperName from EmailTemplate where DeveloperName in : templateNameList];
        if(emailtemplateList.size() > 0){
            for(EmailTemplate emailTemplate : emailtemplateList ){
                emailtemplateMap.put(emailTemplate.DeveloperName, emailTemplate);
            }
        }
        
        List<CampaignMember> campaignMemberList = [select id,Age_In_Hours__c,CampaignId,ContactId,contact.email from CampaignMember where CampaignId in :campaignIdList and Age_In_Hours__c in :lstInteger];
        Map<Id,List<CampaignMember>> mapCampignToMember = new Map<Id,List<CampaignMember>>();
        for(CampaignMember cmb : campaignMemberList)  {
            if(!mapCampignToMember.containsKey(cmb.CampaignId))  {
                mapCampignToMember.put(cmb.CampaignId, new List<CampaignMember>());
            }
            mapCampignToMember.get(cmb.CampaignId).add(cmb);
        }
        
        Map<Id,List<CampaignMember>> campaignStageAndMemberMap = new Map<Id,List<CampaignMember>>();
        List<Id> ctIdsList = new List<Id>();
        for(Campaign_Stage__c campaignStage : scope){
            if(mapCampignToMember.containsKey(campaignStage.Campaign__c))  {
                for(CampaignMember campaignMember: mapCampignToMember.get(campaignStage.Campaign__c)){
                    if(campaignMember.CampaignId == campaignStage.Campaign__c 
                       && campaignMember.Age_In_Hours__c != null 
                       && Integer.valueOf(campaignMember.Age_In_Hours__c) == Integer.valueOf(campaignStage.Age__c)){
                           ctIdsList.add(campaignMember.ContactId);
                           if(campaignStageAndMemberMap.containsKey(campaignStage.Id)){
                               campaignStageAndMemberMap.get(campaignStage.Id).add(campaignMember);
                           }else{
                               campaignStageAndMemberMap.put(campaignStage.Id,new List<campaignMember>());
                               campaignStageAndMemberMap.get(campaignStage.Id).add(campaignMember);
                           }
                       }
                }
            }
        }
        List<Contact> ctList = [select id,MailingPostalCode,OtherPostalCode from Contact where id in :ctIdsList];
        List<String> zipCodeList = new List<String>();
        Map<Id,String> ctZipCodeMap = new Map<Id,String>();
        for(Contact ct : ctList){
            if(ct.MailingPostalCode!=null){
                ctZipCodeMap.put(ct.Id, ct.MailingPostalCode);
            	zipCodeList.add(ct.MailingPostalCode);
            }else if(ct.OtherPostalCode != null){
                ctZipCodeMap.put(ct.Id, ct.OtherPostalCode);
            	zipCodeList.add(ct.OtherPostalCode);
            }
        }
        Map<String,String> zipCodeAndTimeZoneMap = new Map<String,String>();
        List<TimeZoneByZipCode__c> timeZoneList = [select id,Time_Zone__c,ZipCode__c from TimeZoneByZipCode__c where ZipCode__c  in : zipCodeList];
        for(TimeZoneByZipCode__c tm : timeZoneList){
            zipCodeAndTimeZoneMap.put(tm.ZipCode__c,tm.Time_Zone__c);
        }
        
        List<ScheduleSMS__c> smList = new List<ScheduleSMS__c>();
        List<Contact> lstCUpdate = new List<Contact>();
        Set<Id> setIdsToU = new Set<Id>();
        for(Id campaignStage:campaignStageAndMemberMap.keySet()){
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            Savepoint sp = Database.setSavepoint();
            for(CampaignMember campaignMember:campaignStageAndMemberMap.get(campaignStage)){
                System.debug('Got member:--'+campaignMember);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateID(emailtemplateMap.get(campaignStageMap.get(campaignStage).SMS_Template__c).id); 
                mail.setSaveAsActivity(false);
                mail.setTargetObjectId(campaignMember.ContactId);
                mail.setWhatId(campaignMember.Id);
                if(campaignMember.contact.email == null){
                    if(setIdsToU.add(campaignMember.ContactId))  {
                    	Contact tempContact;
                    	tempContact = new Contact(Id=String.valueof(campaignMember.ContactId));
                    	tempContact.email = 'dummy@dummy.com';
						lstCUpdate.add(tempContact);
                    }    

                }
                emails.add(mail);
            }
            //Savepoint sp = Database.setSavepoint();
			update lstCUpdate;
            if(!Test.isRunningTest()){
                Messaging.sendEmail(emails,true);
            }
            Database.rollback(sp);
            for(Messaging.SingleEmailMessage mail : emails){
                ScheduleSMS__c sm = new ScheduleSMS__c();
                sm.CampaignMemberId__c = mail.whatid;
                sm.IsSchedule__c = true;
                sm.Contact__c = mail.targetobjectid;
                sm.MessageData__c = mail.getPlainTextBody();
                sm.From_Number__c = campaignStageMap.get(campaignStage).From_Number__c;
                
                DateTime dt = DateTime.now();
                DateTime dttime = DateTime.now();
                
                Integer offsetval = 0;
                
                if(ctZipCodeMap.get(mail.getTargetObjectId()) != null && zipCodeAndTimeZoneMap.get(ctZipCodeMap.get(mail.getTargetObjectId())) != null)  {
                    Timezone tz = Timezone.getTimeZone(zipCodeAndTimeZoneMap.get(ctZipCodeMap.get(mail.getTargetObjectId())));
                    dttime = dt.addSeconds((tz.getOffset(dt)/1000));
                    offsetval = tz.getOffset(dt)/1000;
                }
                System.debug('contact name:--'+mail.getPlainTextBody());
                System.debug('date Hour:--'+dttime.hour());
                
                if(dttime.hour() >= cst[0].Start_Hour__c && dttime.hour() < cst[0].End_Hour__c){
                    //we can send sms right away
                }else{
                    if(dttime.hour() < cst[0].Start_Hour__c)  {
                        //Schedule it to be 8 am today
                    }else  {
                        //since we are past 8 pm schedule it to be 8 am next day
                        dttime = dttime.addDays(1);
                    }
                    System.debug('cst:--'+cst[0]);
                    dttime = dttime.addHours(Integer.valueOf(cst[0].Start_Hour__c) - dttime.hour());
                    dttime = dttime.addMinutes(0 - dttime.minute());
                    dttime = dttime.addSeconds(0 - dttime.second());
                    sm.Schedule_Time__c = dttime.addSeconds(0- offsetval);
                }
                //system.debug('Formatted Time ' + dt.addSeconds((tz.getOffset(dt)/1000)));
                
                smList.add(sm);
            }
        }
        System.debug('smList:--'+smList);
        if(smList.size()>0){
            insert smList;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        SchedulerOfSMSBatch sdbatch = new SchedulerOfSMSBatch();
        if(!test.isRunningTest()){
            database.executebatch(sdbatch,1);
        }

    }
}