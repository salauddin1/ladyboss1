@IsTest
public class CardDeleteController_Test {
     static testMethod void test1() {
          Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
         sp.Stripe_customer_Id__c = 'cus_DYWAlCN3vsdH4k';
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
         
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
         c.Token__c = 'tok_sdsfdsf3434334cce';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
         CardDeleteController.deleteCard([Select Id , Credit_Card_Number__c,Contact__c,createdDate,Stripe_Card_Id__c,Error_Message__c,
                    Name_On_Card__c,Billing_City__c,Billing_Country__c,Billing_State_Province__c,Billing_Street__c,
                    Billing_Zip_Postal_Code__c,Card_Type__c,Expiry_Month__c,Expiry_Year__c,Cvc__c,Last4__c,Stripe_Profile__r.Stripe_Customer_Id__c
                    FROM Card__c where id =: c.id]);
          CardDeleteController.deleteCardFromSaleforce(c);
         CardDeleteController.getCards(co.id);
         CardDeleteController.dummy();
         
        
         
   Test.stopTest(); 
        
      
    }

    
}