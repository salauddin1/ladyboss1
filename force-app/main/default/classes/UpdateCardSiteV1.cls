//Card update by custom VF page
public without sharing class UpdateCardSiteV1{
    public boolean showUpdateCard {get;set;}
    public String MyActionMethod { get; set; }
    //set endpoint url
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers/';
    public  String  headerdata{get;set;}
    public string urlvalue{get;set;}
    public string url{get;set;}
    public string cardType{get;set;}
    public string cardLast4Didit{get;set;}
    public string cardInfo {get;set;}
    public string customerID {get;set;}
    public string uID {get;set;}
    public string cardAction {get;set;}
    public Boolean isUpdate {get;set;}
    public Boolean isShowEntry {get;set;}
    public String selectedValue { get; set; }
    public String caseId{ get; set; }
    public String failedAmount{ get; set; }
    //To showupdate radio as checked
    public void openUpdateForm(){
        system.debug('openUpdateForm is fired');
        showUpdateCard = true;
        system.debug('showUpdateCard ==>'+showUpdateCard);
        
        
    }
    public void MyActionMethod(){
        selectedValue = 'Update Card';
        cardAction = 'Update Card';
        isUpdate = true;
        isShowEntry =false;
        //Get the parameters from URL
        customerID =ApexPages.currentPage().getParameters().get('id');
        uID =ApexPages.currentPage().getParameters().get('key');
        caseId=ApexPages.currentPage().getParameters().get('CaseId');
        System.debug('====customerID ===='+customerID );
        HttpRequest http = new HttpRequest();
        //check if profile exists or not 
        List<Stripe_Profile__c> stripeProfList = [select id ,Card_Processed__c,Stripe_Customer_Id__c,Customer__c from Stripe_Profile__c where Stripe_Customer_Id__c =: customerId ];
 List<case> caseList =[select id, Failed_Amount__c from case where id=: caseId];       System.debug('===========stripeProfList========='+stripeProfList);
        if(caseList.size()> 0 && caseList[0].Failed_Amount__c!=null){
                    failedAmount = String.valueOf(caseList[0].Failed_Amount__c);
                }
        Contact conOb = [select id,Link_Clicked__c from Contact where Id=:stripeProfList[0].Customer__c ];
        List<Custom_Tracker__c  > custTrack = new List<Custom_Tracker__c  >();
        List<Custom_Tracker__c>  cTrack =[select id,Is_Clicked__c,Link_Click_Count__c,Email_Link_Click_Date_Time__c from Custom_Tracker__c  where UniqueId__c =:uID  ];
        if(cTrack.size()>0){
            cTrack[0].Is_Clicked__c =  true;
            cTrack[0].Email_Link_Click_Date_Time__c = System.Now();
            if(cTrack[0].Link_Click_Count__c!=null) {
                cTrack[0].Link_Click_Count__c=cTrack[0].Link_Click_Count__c+1;
                //custTrack.add(cTrack);
            }
            else
            {
                cTrack[0].Link_Click_Count__c=1;
                //custTrack.add(cTrack);
            }
        }
        List<Contact> conList = new  List<Contact>();
        if(conOb !=null) {
            if(conOb.Link_Clicked__c !=null ) {
                conOb.Link_Clicked__c = conOb.Link_Clicked__c+1;
                conList.add(conOb);
            }
            else {
                conOb.Link_Clicked__c = 1;
                conList.add(conOb);
            }
            
        }
        Map<string,string> conStripeMap = new Map<string,string> ();
        Map<string,string> conStripeMap1 = new Map<string,string> ();
        
        
        
        
        http.setEndpoint(SERVICE_URL+''+customerId );
        http.setMethod('GET');
        //Blob headerValue = Blob.valueOf('sk_test_7oNYREAid74CQJ6JtMrVfkxa' + ':');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        //system.debug('####---StripeAPI.ApiKey--- '+StripeAPI.ApiKey);
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        //system.debug('####---auth--- '+authorizationHeader);
        http.setHeader('Authorization', authorizationHeader);
        
        
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        try {
            hs = con.send(http);
            response = hs.getBody();
            statusCode = hs.getStatusCode();
        } catch (CalloutException e) {
            
        }
        
        system.debug('#### '+ hs);
        
        system.debug('response-- '+response);
        system.debug('statusCode-- '+statusCode);
        //If success parse and display last4 digit on page
        if(statusCode == 200 ){
            System.debug(response);
            try {
                //StripeCard o = (StripeCard.parse(response));
                //System.debug('---------StripeCard-o-----------'+o);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response);
                
                Map<String, Object>  lstCustomers       =   (Map<String, Object> )results.get('sources');
                system.debug('response-- '+lstCustomers       );
                List<Object> lstsourcess = (List<Object>)lstCustomers.get('data');
                Object carddata = (object)lstsourcess[0];
                system.debug('response-- '+carddata );
                Map<String,Object> data = (Map<String,Object>)carddata ;
                
                //Magic!
                system.debug('dfgdgdf'+data.get('brand'));
                system.debug('dfgdgdf'+data.get('last4'));
                // return null;
                
                cardLast4Didit = string.valueOf(data.get('last4'));
                cardType=string.valueOf(data.get('brand'));
                System.debug('--------------cardType------------'+cardType);
                System.debug('--------------cardLast4Didit------------'+cardLast4Didit);
                
            } catch (System.JSONException e) {
                
            }
            
        }
        
        if(conList.size() > 0){
            
            update conList;
            
        }
        if(cTrack.size() > 0) {
            update cTrack;
        }
    }
    //To show thw selected radio button's value
    public void checkSelectedValue() {
        isShowEntry =true;
        system.debug('Selected value is: ' + selectedValue); 
        if(selectedValue == 'update') {
            cardAction = 'Update Card';
            isUpdate = true;
        } else if(selectedValue == 'new')  {
            cardAction = 'New Card';
            isUpdate = false;
        }
    }
    @auraEnabled
    public static Boolean updateCard1(){
        system.debug('fired===>');
        return true;
    }
    @auraEnabled
    public static Boolean updateCustomerCard(string cardId, string customerId,String address_city,
                                             string address_country,String address_state,string address_zip,
                                             string exp_month,string exp_year,string name,string tokenId,string address_line1,boolean isUpate,string keyVal,string caseId)
    {system.debug('caseId==>'+caseId);
     List<Contact> conList1 = new List<Contact>();
     //update customer
     String updateCustomerURL = 'https://api.stripe.com/v1/customers/'+customerId;
     if(!isUpate) updateCustomerURL =  updateCustomerURL + '/sources';
     //updateCustomerURL =  updateCustomerURL + '/sources';
     
     HttpRequest httpUpdateCustomer = new HttpRequest();
     httpUpdateCustomer.setEndpoint(updateCustomerURL);
     httpUpdateCustomer.setMethod('POST');
     httpUpdateCustomer.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
     //setting the tokenid
     Map<String, String> updateCustomerPayload = new Map<string,string>{'source' => tokenId };
         
         httpUpdateCustomer.setBody(StripeUtil.urlify(updateCustomerPayload));
     try {
         Integer statusCode1;
         Http con1 = new Http();
         HttpResponse hs1 = new HttpResponse();
         //send the request
         hs1 = con1.send(httpUpdateCustomer);
         
         statusCode1 = hs1.getStatusCode();
         system.debug('statusCode1==>'+statusCode1);
         system.debug('update result-->'+JSON.deserializeUntyped(hs1.getBody()));
         if(statusCode1 ==200 || test.isRunningTest()) {
             
             HttpRequest http = new HttpRequest();
             //to update the cardId and customer ID
             string url =  'https://api.stripe.com/v1/customers/'+customerId +'/sources/'+cardId ;
             http.setEndpoint(url );
             http.setMethod('POST');
             system.debug('tokenId==>'+tokenId);
             
             http.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
             Map<String,String> metadata = new Map<String,String>();
             system.debug('isUpate==>'+isUpate);
             //metadata.put('Integration Initiated From','Salesforce');
              metadata.put('Updated via','Salesforce VF');
             if(isUpate){
                 metadata.put('Updated','True');
             }
             if(!isUpate) {
                 metadata.put('Created','True');
             }
             System.debug('---metadata----'+metadata);
             //Create the map of the address and exp year and month
             Map<String, String> payload = new Map<String, String>{'address_zip' =>address_zip ,'address_city' =>address_city ,
                 'address_country' => address_country ,
                 'address_state' => address_state,'name' => name,'address_zip' => String.valueOf(address_zip),
                 'exp_month' => String.valueOf(exp_month),'exp_year' => String.valueOf(exp_year),'address_line1' => address_line1
                 };
                     
                     if (metadata != null) {
                         
                         for (String key : metadata.keySet()) {
                             if (metadata.get(key) != null) {
                                 payload.put('metadata['+key+']', metadata.get(key));
                             }
                         }
                     }
             
             system.debug('StripeUtil.urlify(payload ) :'+StripeUtil.urlify(payload ));
             http.setBody(StripeUtil.urlify(payload ));
             
             String response;
             Integer statusCode;
             Http con = new Http();
             HttpResponse hs = new HttpResponse();
             Map<String, Object> results;
             if(!test.isrunningTest()){
                 hs = con.send(http);
                 response = hs.getBody();
                 statusCode = hs.getStatusCode();
             }else
             {
                 statusCode =200;
                 response = '{"id":"card_1DhIKkBwLSk1v1ohtWCxQrTa","object":"card","address_city":"sdf","address_country":null,"address_line1":"sdf","address_line1_check":"pass","address_line2":null,"address_state":"sdf","address_zip":"66666","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_E1EYecfsRNu1yD","cvc_check":"pass","dynamic_last4":null,"exp_month":12,"exp_year":2034,"fingerprint":"eGF2QMV6K3vhvUL3","funding":"credit","last4":"4242","metadata":{},"name":"dfgggdfg","tokenization_method":null}';
             }
             
             if(statusCode ==200) {
                 if(!isUpate){
                 //to update the default card to new card
                     system.debug('Updating Default card to new card');
                     //set default calrd
                     updateCustomerURL = 'https://api.stripe.com/v1/customers/'+customerId;
                     HttpRequest httpsetDefaultCard = new HttpRequest();
                     httpsetDefaultCard.setEndpoint(updateCustomerURL);
                     httpsetDefaultCard.setMethod('POST');
                     httpsetDefaultCard.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
                     map<string,string> defaultCardMap = new Map<string,string>{'default_card' => cardId};
                         httpsetDefaultCard.setBody(StripeUtil.urlify(defaultCardMap));
                     Integer defaultCardStatusCode;
                     Http defaultCardHTTP = new Http();
                     HttpResponse defaultCard_HS = new HttpResponse();
                     defaultCard_HS = defaultCardHTTP.send(httpsetDefaultCard);                    
                     defaultCardStatusCode = defaultCard_HS.getStatusCode();
                     system.debug('defaultCard_HS==>'+JSON.deserializeUntyped(defaultCard_HS.getBody()));
                 }
                 
                 System.debug('======='+response );
                 if(!test.isrunningTest()){
                     results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                 }else{
                     results      =   (Map<String, Object>)JSON.deserializeUntyped(response );
                 }
                 List<Stripe_Profile__c> lstStripeProfile = [SELECT Id, Name,Customer__c, Stripe_Customer_Id__c FROM Stripe_Profile__c where Stripe_Customer_Id__c =:customerId ];
                 Contact conOb = [select id,New_Cards__c,Updated_Cards__c from contact where id= : lstStripeProfile[0].Customer__c];
                 //String uID =ApexPages.currentPage().getParameters().get('key');
                 List<Custom_Tracker__c>  cTrack =[select id,New_Card_Count__c,Update_Card_Count__c from Custom_Tracker__c  where UniqueId__c!=null and UniqueId__c =:keyVal   ];
                 
                 List<Custom_Tracker__c  >  cListTrack = new List<Custom_Tracker__c  >();
                 
                 if(conOb !=null ) {
                     
                     if( !isUpate){
                         if(cTrack.size() > 0){
                             if(cTrack[0].New_Card_Count__c!=null) {
                                 cTrack[0].New_Card_Count__c=cTrack[0].New_Card_Count__c+1;
                                 cTrack[0].New_Card_Created_DateTime__c = System.Now();
                                 cListTrack .add(cTrack[0]);
                             }
                             else
                             {
                                 cTrack[0].New_Card_Count__c=1;
                                 cTrack[0].New_Card_Created_DateTime__c = System.Now();
                                 cListTrack .add(cTrack[0]);
                             }
                         }
                         System.debug('---isUpate----'+isUpate);
                         if(conOb.New_Cards__c!=null ){
                             conOb.New_Cards__c =conOb.New_Cards__c +1;
                         }
                         else
                         {
                             conOb.New_Cards__c = 1;
                         }
                         conList1.add(conOb);
                     }
                     if( isUpate){
                         if(cTrack.size() > 0){
                             if(cTrack[0].Update_Card_Count__c!=null) {
                                 cTrack[0].Update_Card_Count__c=cTrack[0].Update_Card_Count__c+1;
                                 cTrack[0].Update_Card_Date_Time__c = SyStem.Now();
                                 cListTrack.add(cTrack[0]);
                             }
                             else
                             {
                                 cTrack[0].Update_Card_Count__c=1;
                                 cTrack[0].Update_Card_Date_Time__c = System.Now();
                                 cListTrack.add(cTrack[0]);
                             }
                             //update cTrack;
                         }
                         
                         System.debug('---isUpate----'+isUpate);
                         if(conOb.Updated_Cards__c!=null ){
                             conOb.Updated_Cards__c =conOb.Updated_Cards__c +1;
                         }
                         else
                         {
                             conOb.Updated_Cards__c = 1;
                         }
                         conList1.add(conOb);
                     }
                     
                 }
                 
                 if(lstStripeProfile .size() > 0) {
                 //to upadte the address 
                     updateShippingAddress(customerId , address_line1, address_city,address_country, address_state,address_zip,caseId);
                     if(conList1.size() >0){
                         update conList1;
                         System.debug('---conList1----'+conList1);
                     }
                     if(cListTrack.size() > 0) {
                         update cListTrack ;
                     }
                     List<Address__c> aList = new List<Address__c> ();
                     
                     if((address_city !=null && address_city!= '' ) || (address_state!=null && address_state!= '') ||  (address_line1!=null && address_line1!= '') || (address_zip!=null && address_zip!= '' ) || (address_country!=null && address_country!= ''))
                     {//crate address on the SF
                         Address__c addr = new Address__c();
                         addr.Contact__c =lstStripeProfile[0].Customer__c;
                         addr.Shipping_City__c =address_city;
                         addr.Shipping_State_Province__c = address_state;
                         addr.Shipping_Street__c =address_line1;
                         addr.Shipping_Zip_Postal_Code__c = address_zip;
                         addr.Shipping_Country__c= address_country;
                         
                         if((address_city !=null && address_city!= '' ) && (address_state!=null && address_state!= '') &&  (address_line1!=null && address_line1!= '') && (address_zip!=null && address_zip!= '' ) && (address_country!=null && address_country!= ''))
                         {
                             addr.Primary__c =true;
                         }
                         
                         aList.add(addr);
                     }
                     
                     if(aList.size() > 0) {
                         insert aList;
                         string JSONString = JSON.serialize(aList);
                         // insertAdress(JSONString ,string.valueOf(lstStripeProfile[0].Customer__c));
                     }
                     
                 } 
             }
             else
             {
                 results      =   (Map<String, Object>)JSON.deserializeUntyped(hs.getBody());
                 List<Map<String, Object>> error          =   new List<Map<String, Object>>();
                 Map<String, Object>  errorMap       =   (Map<String, Object> )results.get('error');
                 String returnSucess= String.valueOf(errorMap.get('message'));
                 System.debug('sdfsfd'+returnSucess);
             }
         }
         
         
         
     } catch (CalloutException e) {
         
     }
     return true;
    }
    @auraEnabled
    public static Map<String,Object> getExpireDateValues(){
        Map<String,Object> returnValue = new Map<String,Object>();
        Set<Integer> months = new Set<Integer> ();
        Set<Integer> years =  new Set<Integer>();
        for(Integer i=1 ;i<13;i++){
            months.add(i);
        }
        Integer currentYear = Date.today().Year();
        for(Integer i=0 ;i<20;i++){
            years.add(currentYear+i);
        }
        returnValue.put('months',months);
        returnValue.put('years',years);
        return returnValue;
    }
    //setting the shipping address on the card
    public static integer updateShippingAddress(String customerId , string address_line1,String address_city,
                                                string address_country,String address_state,string address_zip,string caseId) {
                                                    HttpRequest http = new HttpRequest();
                                                    string url =  'https://api.stripe.com/v1/customers/'+customerId  ;
                                                    http.setEndpoint(url );
                                                    http.setMethod('POST');
                                                    http.setHeader('Authorization', 'bearer '+StripeAPI.ApiKey);
                                                    String reqBody = '&metadata[Shipping Address]='+address_line1+' '+address_city +' '+address_state +' '+address_country+' '+ address_zip+'&';
                                                    http.setBody(reqBody);
                                                    
                                                    String response;
                                                    Integer statusCode;
                                                    Http con = new Http();
                                                    HttpResponse hs = new HttpResponse();
                                                    try{
                                                        hs = con.send(http);
                                                        response = hs.getBody();
                                                        System.debug('0000000'+response );
                                                        if(caseId!=null && caseId!=''){
                                                            List<Case> caseList = [select id, Card_Updated_Date_Time__c ,UpdateCardStatus__c  from case where id=:caseId ];
                                                            caseList[0].UpdateCardStatus__c = 'Card Updated';
                                                            caseList[0].Card_Updated_Date_Time__c=System.now();
                                                            if(caseList.size() > 0){
                                                                update caseList; 
                                                            }
                                                            
                                                        } 
                                                        
                                                        return statusCode = hs.getStatusCode();
                                                    }catch(exception e){
                                                        System.debug('=Exception='+e.getMessage());
                                                    }
                                                    return null;
                                                }
    @future
    public static void insertAdress(string AddressString,string conId){
        //Address__c updateAddress = (Address__c) JSON.deserialize(AddressString, Address__c.class);
        //updateAddress.Contact__c = conId;
        //update updateAddress;
        // list<Address__c> li_TurnoverTargetstoUpdate = (list<Address__c>) JSON.deserialize(AddressString, list<Address__c>.class);
        //insert li_TurnoverTargetstoUpdate ;
        //System.debug('========='+li_TurnoverTargetstoUpdate );
    }
}