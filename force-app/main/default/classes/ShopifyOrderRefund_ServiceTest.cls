@isTest
public class ShopifyOrderRefund_ServiceTest {
    @isTest
    public Static void unitTest(){
        Contact con = new Contact();
        con.LastName='TestLast';
        con.FirstName='TestFirst';
        con.Email='Test@gmail.com';
        con.Phone='7067749234';
        insert con;
        Shopify_Orders__c ord = new Shopify_Orders__c();
        ord.Order_Id__c='820982911946154508';
        ord.fulfillment_Status__c='fulfilled';
        ord.Refunded_Amount__c=123;
        ord.Total_Price__c=12;
        ord.Status__c='confirmed';
        ord.Contact__c=con.id;
        insert ord;
        ShopifyLineItem__c itm  = new ShopifyLineItem__c();
        itm.Contact__c=con.id;
        itm.Refunded_Amount__c=43;
        itm.Refunded_Tax_Amount__c=23;
        itm.Item_Id__c='976318377106520349';
        itm.Shopify_Orders__c=ord.Id;
        insert itm;
        Shopify_Parameters__c tokens = new Shopify_Parameters__c ();
        tokens.Access_Token__c='1234redcre3edsx23wq3wed32w32wes32w';
        insert tokens;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new orderCalloutTest());
        String JSONBody='{"id":890088186047892319,"order_id":820982911946154508,"created_at":null,"note":"Things were damaged","user_id":30895702116,"processed_at":null,"restock":false,"refund_line_items":[{"id":487817672276298627,"quantity":1,"line_item_id":487817672276298554,"location_id":null,"restock_type":"no_restock","subtotal":89.99,"total_tax":0.0,"subtotal_set":{"shop_money":{"amount":"89.99","currency_code":"INR"},"presentment_money":{"amount":"89.99","currency_code":"INR"}},"total_tax_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"line_item":{"id":487817672276298554,"variant_id":null,"title":"Aviator sunglasses","quantity":1,"price":"89.99","sku":"SKU2006-001","variant_title":null,"vendor":null,"fulfillment_service":"manual","product_id":788032119674292922,"requires_shipping":true,"taxable":true,"gift_card":false,"name":"Aviator sunglasses","variant_inventory_management":null,"properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":100,"total_discount":"0.00","fulfillment_status":null,"price_set":{"shop_money":{"amount":"89.99","currency_code":"INR"},"presentment_money":{"amount":"89.99","currency_code":"INR"}},"total_discount_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"discount_allocations":[],"tax_lines":[]}},{"id":976318377106520422,"quantity":1,"line_item_id":976318377106520349,"location_id":null,"restock_type":"no_restock","subtotal":154.99,"total_tax":0.0,"subtotal_set":{"shop_money":{"amount":"154.99","currency_code":"INR"},"presentment_money":{"amount":"154.99","currency_code":"INR"}},"total_tax_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"line_item":{"id":976318377106520349,"variant_id":null,"title":"Mid-century lounger","quantity":1,"price":"159.99","sku":"SKU2006-020","variant_title":null,"vendor":null,"fulfillment_service":"manual","product_id":788032119674292922,"requires_shipping":true,"taxable":true,"gift_card":false,"name":"Mid-century lounger","variant_inventory_management":null,"properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":1000,"total_discount":"5.00","fulfillment_status":null,"price_set":{"shop_money":{"amount":"159.99","currency_code":"INR"},"presentment_money":{"amount":"159.99","currency_code":"INR"}},"total_discount_set":{"shop_money":{"amount":"5.00","currency_code":"INR"},"presentment_money":{"amount":"5.00","currency_code":"INR"}},"discount_allocations":[{"amount":"5.00","discount_application_index":0,"amount_set":{"shop_money":{"amount":"5.00","currency_code":"INR"},"presentment_money":{"amount":"5.00","currency_code":"INR"}}}],"tax_lines":[]}}],"transactions":[],"order_adjustments":[]}';
        RestRequest request = new RestRequest();
        request.requestUri ='https://ashish-ladyboss-support.cs53.force.com/ShipStation/services/apexrest/ShopifyOrderRefund';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONBody);
        RestContext.request = request;
        ShopifyOrderRefund_Service.getcustomer();
        Test.stopTest();
    }
}