@isTest
public class CardAddressUpdateBatch_Test {
    
    
    static testMethod void myTest() {
        
        
        Contact con = new Contact();
        con.LastName ='test';
        con.email='test@TEST.COM';
        insert con;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Customer__c = con.id;
        sp.Stripe_Customer_Id__c = 'cus_test';
        insert sp;
        Card__c card = new Card__c();
        card.contact__c = con.id;
        card.Stripe_Card_id__c = 'card_iuouo';
        card.Stripe_Profile__c = sp.id;
        insert card;
        
        Address__c ad = new Address__c();
        ad.contact__c = con.id;
        ad.Primary__c =true;
        ad.Shipping_City__c = 'city';
        ad.Shipping_Country__c='country';
        ad.Shipping_State_Province__c = 'state';
        ad.Shipping_Street__c = 'street';
        ad.Shipping_Zip_Postal_Code__c='zip';
        
        insert ad;
        test.startTest();
        Database.executeBatch(new CardAddressUpdateBatch(),10);
        test.stopTest();
    }
    
    
}