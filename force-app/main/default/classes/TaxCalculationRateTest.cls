@isTest
private class TaxCalculationRateTest {
    @isTest static void TestTaxRate() {
       test.startTest();
     
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Something'; //Request URL
        req.httpMethod = 'POST';

       //ap<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(res);
        Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                             Family = 'Hardware',Tax_Code__c='12345',Price__c=10000);
                
        insert prod;
        Product2 prod1 = new Product2(Name = 'Laptop X201', 
                                     Family = 'Hardware',Tax_Code__c='12345',Price__c=10000);
            
        insert prod1;
        Product2 prod2 = new Product2(Name = 'Laptop Bundle', 
                                     Family = 'Hardware',Product_One__c = prod.Id,Product_Two__c=prod1.Id,Price__c=20000);
            
        insert prod2;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        PricebookEntry standardPrice1 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod1.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice1;
        PricebookEntry standardPrice2 = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod2.Id,
            UnitPrice = 20000, IsActive = true);
        insert standardPrice2;
        TaxCalculationRate.ratemethod('abc','2000 Main Street','Irvine','CA','US',100.00,2,'test');
        TaxCalculationRate.setTransaction('abc','2000 Main Street','Irvine','CA','US',100.00,2,'test','planId', 'chargeId',  'prodName', 2.5, 'createdUsing');
        TaxCalculationRate.voidTransaction('test');
        test.stopTest();
    }
}