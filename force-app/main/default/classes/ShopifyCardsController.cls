public class ShopifyCardsController {
    public String cardExpMoth { get; set; }
    private static final String SERVICE_URL = 'https://api.stripe.com/v1/customers';
    public  String  CongMessage{get;set;}
    public string urlvalue{get;set;}
    public string APICredentials{get;set;}
    public string Message{get;set;}
    public string cardType{get;set;}
    public string cardLast4Didit{get;set;}
    public string cardInfo {get;set;}
    public string customerID {get;set;}
    public string cardAction {get;set;}
    public Boolean isShowEntry {get;set;}
    public Boolean errormsg {get;set;}
    public String selectedValue { get; set; }
    public boolean showUpdateCard {get;set;}
    public String cardName {get;set;}
    public ShopifyCardsController(){
        APICredentials = StripeAPI.PublishableKey;
        System.debug('====='+APICredentials);
    }
    public void openUpdateForm() {
       // isShowEntry =true;
        Message = 'Card Detail';
        Map<String, Object> cardResult1 = (Map<String, Object>)JSON.deserializeUntyped(cardInfo);
        Map<String, Object> cardsdat = (Map<String, Object>)cardResult1.get('card');
              urlvalue = String.valueOf(cardsdat.get('id'));
        	  cardExpMoth = String.valueOf(cardsdat.get('exp_month'));
        	  cardLast4Didit = String.valueOf(cardsdat.get('last4'));
        	  selectedValue = String.valueOf(cardsdat.get('exp_year'));
              cardType = String.valueOf(cardsdat.get('brand'));
               
        try{
            System.debug('=========='+StripeAPI.ApiKey);
               String contactId = ApexPages.currentPage().getParameters().get('id');
               contact con = [select email, firstName, lastName, Phone, MobilePhone,Stripe_Customer_Id__c from Contact Where Id =: contactId limit 1];
               Map<String, Object> cardResult = (Map<String, Object>)JSON.deserializeUntyped(cardInfo);
               String tokens = String.valueOf(cardResult.get('id'));
               system.debug('==========tokens ============' +tokens);
               Map<String, Object> cards = (Map<String, Object>)cardResult.get('card');
                String Body =  'description=This Customer is associated with Shopify&email='+con.Email;
                Map<String, Object> cusResult = new Map<String, Object> ();
                if(con.Stripe_Customer_Id__c == null || con.Stripe_Customer_Id__c == '') {

                    HttpRequest http = new HttpRequest();
                    http.setEndpoint(SERVICE_URL);
                    http.setMethod('POST');
                    http.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
                    http.setBody(Body);        
                    Http con1 = new Http();
                    HttpResponse hs = new HttpResponse();
                    hs = con1.send(http);
                    System.debug('============'+hs.getBody());
                    cusResult = (Map<String,Object>)Json.deserializeUntyped(hs.getBody());
                    con.Stripe_Customer_Id__c = String.valueOf(cusResult.get('id'));
                    
                }
        		    
            
                    HttpRequest httpreq = new HttpRequest();
                    if(String.valueOf(con.Stripe_Customer_Id__c) == null || String.valueOf(con.Stripe_Customer_Id__c) == '') {
                    	httpreq.setEndpoint('https://api.stripe.com/v1/customers/'+String.valueOf(String.valueOf(cusResult.get('id')))+'/sources');
                    }
                        else{
                            httpreq.setEndpoint('https://api.stripe.com/v1/customers/'+String.valueOf(con.Stripe_Customer_Id__c)+'/sources');
                        }
                    httpreq.setMethod('POST');
                    httpreq.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
                    httpreq.setBody('source='+tokens);        
                    Http con2 = new Http();
                    HttpResponse hsres = new HttpResponse();
                    hsres = con2.send(httpreq);
                    System.debug('============'+hsres.getBody());
                     
            
                    HttpRequest http2 = new HttpRequest();
                   	if(String.valueOf(con.Stripe_Customer_Id__c) == null || String.valueOf(con.Stripe_Customer_Id__c) == '') {
                    	http2.setEndpoint(SERVICE_URL+'/'+String.valueOf(String.valueOf(cusResult.get('id'))));
                   	}
                   	else{
                   		http2.setEndpoint(SERVICE_URL+'/'+con.Stripe_Customer_Id__c);
            		}
                    http2.setMethod('POST');
                    http2.setHeader('Authorization', 'Bearer '+StripeAPI.ApiKey);
                    http2.setBody('default_source='+String.valueOf(cards.get('id')));        
                    Http con3 = new Http();
                    HttpResponse hs2 = new HttpResponse();
                    hs2 = con3.send(http2);
                    System.debug('============'+hs2.getBody());
            		Map<String, Object> UpdatedCustomer = (Map<String, Object>)JSON.deserializeUntyped(hs2.getBody());
            		if(hsres.getStatusCode() == 200) {
                        errormsg = false;
                        isShowEntry =true;
                        CongMessage = 'Card Succesfully Created';
            		    Card__c crd = new Card__c (); 
                        crd.Token__c=tokens;
                        crd.Credit_Card_Number__c = String.valueOf(cards.get('last4'));
                        crd.Card_ID__c = String.valueOf(cards.get('id'));
                        crd.Expiry_Month__c = String.valueOf(cards.get('exp_month'));
                        crd.Expiry_Year__c = String.valueOf(cards.get('exp_year'));
                        crd.Shopify_Card__c = true;
                        crd.Contact__c = contactId;
                        crd.Name_On_Card__c = cardName;
                        crd.Last4__c = String.valueOf(cards.get('last4'));
                        crd.Cvc_Check__c = 'Pass';
                        crd.Cvc__c = '***';
                        crd.Brand__c = String.valueOf(cards.get('brand'));
                        crd.Default_Source__c = String.valueOf(UpdatedCustomer.get('default_source'));
                        insert crd;
            		}
                    else {
                        isShowEntry = false;
                        errormsg =true;
                		Map<String, Object> cardData = (Map<String, Object>)JSON.deserializeUntyped(hsres.getBody());
                        Map<String, Object> errormsg = (Map<String, Object>)cardData.get('error');
                          CongMessage =   String.valueOf(errormsg.get('message')) +'Please retry!';
                        System.debug('===CongMessage===='+CongMessage);
            		}
           	        update con;
        }
        catch(Exception e) {
            
        }
        
    }

}