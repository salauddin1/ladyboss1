@RestResource(urlMapping='/pap_affiliate')
global class PAP_API_affiliate {
    @HttpGet
    global static void createEvent() {
        RestRequest req = RestContext.request;
        System.debug(req.params);
        Map<String,String> paramValueMap = req.params;
        System.debug('paramValueMap : '+paramValueMap);
        String email,fname,lname,refId;
        if(paramValueMap.get('username')!=null){
        	email = paramValueMap.get('username').toLowerCase();
        }
        if(paramValueMap.get('fname')!=null){
        	fname = paramValueMap.get('fname');
        }
        if(paramValueMap.get('lname')!=null){
        	lname = paramValueMap.get('lname');
        }
        if(paramValueMap.get('refId')!=null){
        	refId = paramValueMap.get('refId');
            system.debug('refId'+refId);
        }
        System.debug(email+' '+fname+' '+lname+' '+refId);
        List<Contact> cntList = [SELECT id,PAP_refid__c FROM Contact WHERE email =: email order by createddate asc limit 1];
        if(cntList.size()>0){
            Contact ct = cntList.get(0);
            ct.PAP_refid__c = refId;
            update ct;
        }else{
            Contact cnt = new Contact();
            cnt.PAP_refid__c = refId;
            cnt.Email = email;
            if(lname!=null){
                cnt.LastName = lname;
                cnt.FirstName = fname;
            }else{
                cnt.lastname = fname;
            }
            insert cnt;
        }
    }
}