@isTest(seeAlldata=true)
public class Report_Failed_Recovered_DollarsTest {
  
    static testMethod void testSubscription(){
         Contact Con =new Contact();
        Con.LastName = 'Test Con';
        insert Con;
         String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
       
        Stripe_Product_Mapping__c sp = new Stripe_Product_Mapping__c();
        sp.name = 'dfg';
        sp.Stripe_Nick_Name__c='Transformation System-$237';
        sp.Salesforce_Product__c='prod_DDGQVEaH03NJ8p';
        insert sp;
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_Dl9tzT8tKU51LD';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        //insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        //insert customPrice;
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.Amount=23700;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        op.Subscription__c = 'sub_DlAEjFZvBklMuQ';
        //op.AccountId = Acc.id;
        op.Contact__c = Con.Id;
        op.RecordTypeId = devRecordTypeId;
       
        insert op;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = standardPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        ol.Subscription_Id__c ='sub_DlAEjFZvBklMuQ';
        ol.refund__c = 137;
        insert ol;
        
        
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
       
      /*  Opportunity opp = new Opportunity();
          opp.Account = acc;
          opp.CloseDate = date.today() ;
          opp.StageName = 'Pending';
          opp.Name ='eijhfj' ;
        insert opp;*/
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
       Test.startTest();
       
       
        Report_Failed_Recovered_Dollars s = new Report_Failed_Recovered_Dollars();
       //s.getData();
       Report_Failed_Recovered_Dollars.methodDummy();
       Test.stopTest();
 
    }
    
}