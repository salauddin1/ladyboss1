// Created By : Sourabh Badole 
// Date       : 15-3-2019
@isTest 
public class ReportUpdateCaseBatchTest {
    static testMethod void testMethod1() {
        system.Test.setMock(HttpCalloutMock.class, new ReportUpdateCaseBatchMock());
        Stripe_Product_Mapping__c mp = new Stripe_Product_Mapping__c();
        mp.Salesforce__c = 'Supplement' ;
        mp.Stripe__c = 'LB BURN 30 Day Supply' ;
        mp.Name = 'test';
        insert mp ;
        List<Case> lstCs = new List<Case>();
            Case cs = new Case();
            cs.IsFailed_Payment__c = true;
            cs.isProcessedForReport__c = false ;
            cs.Charge_Id__c = 'ch_1E8SgWBwLSk1v1oh6AGORpxc';
            lstCs.add(cs);
        
        insert lstCs;
        
        
        Test.startTest();
        
        ReportUpdateCaseBatch obj = new ReportUpdateCaseBatch();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }
}