@isTest
public class StripeCard_Service_Test{
    
  /*    public static TestMethod void testGetCallout(){
       Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
                Con.Product__c = 'Unlimited';
        insert Con;
      Stripe_webservice_setting__c opp = new Stripe_webservice_setting__c ();
    opp.StripeCard_Service__c = true;
    opp.StripeCardDelete_Service__c = true;
    opp.StripeCustomer_Service__c = true;
    insert opp;
           system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
           Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_E0w3qq3i41QeYa';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
          
          Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        op.AccountId = Acc.id;
        op.Description = 'hhj';
        op.CustomerID__c = 'cus_E0w3qq3i41QeYa';
        op.Stripe_Charge_Id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        op.Contact__c = Con.Id;
           op.Stripe_Profile__c = StripePro.id;
        insert op;
           Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Stripe_charge_id__c = 'ch_1Dl9xnFzCf73siP0LyKD4qMk';
        insert ol;
           WebHookSecrets__c wb = new WebHookSecrets__c();
        wb.name = 'Stripe_ChargeWebhookService';
        wb.Webhook_Name__c = 'Stripe_ChargeWebhookService';
        wb.Webhook_Secret__c = 'whsec_cIdDwVu0vpFqtbk13jL35lUQ8RhG2zHS';
        insert wb;
    
          
          
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_CardService'; 
          String body ='{\"data\":{\"object\":{\"id\":\"card_1D7P9xBwLSk1v1ohQezYknMQ\",\"object\":\"card\",\"address_city\":\"Delhi\",\"address_country\":\"US\",\"address_line1\":\"T5-1D\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"HR\",\"address_zip\":\"121004\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":\"cus_DYWAlCN3vsdH4k\",\"cvc_check\":null,\"dynamic_last4\":null,\"exp_month\":11,\"exp_year\":2021,\"fingerprint\":\"eGF2QMV6K3vhvUL3\",\"funding\":\"credit\",\"last4\":\"4242\",\"name\":\"Ashish Sharma\",\"tokenization_method\":null,\"metadata\":{\"Integration Initiated From\":\"Salesforce\"}}}}';
        req.requestBody = Blob.valueOf(body);
          req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
         
        StripeCard_Service.createCard();
         
   Test.stopTest(); 
        
    } */
     public static TestMethod void testGetCallout1(){
         
      Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
           system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
          Card_Opp_Temporary__c cd = new Card_Opp_Temporary__c();
          cd.Invoice_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          cd.CardId__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          insert cd;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_CardService'; 
          String body ='{ "id": "evt_1FX3uMFzCf73siP0DL6VBBR2", "object": "event", "api_version": "2018-02-28", "created": 1571914269, "data": { "object": { "id": "card_1FX3uCFzCf73siP0sSCiQwPU", "object": "card", "address_city": "NY", "address_country": "US", "address_line1": "2000 Main Street", "address_line1_check": null, "address_line2": null, "address_state": "NY", "address_zip": "10001", "address_zip_check": null, "brand": "American Express", "country": "US", "customer": "cus_ExUwi3Ny6ptXkB", "cvc_check": "unavailable", "dynamic_last4": null, "exp_month": 7, "exp_year": 2022, "fingerprint": "KfQdByKUuKuf6iEL", "funding": "credit", "last4": "1014", "metadata": { }, "name": null, "tokenization_method": null } }, "livemode": true, "pending_webhooks": 7, "request": { "id": "req_SmlEBnNn9j5XBX", "idempotency_key": "76239c13-f6b5-4825-925a-cc9d0319d804" }, "type": "customer.source.created" }';
        req.requestBody = Blob.valueOf(body);
           req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
         
        StripeCard_Service.createCard();
         
   Test.stopTest(); 
        
    }
    public static TestMethod void testGetCallout2(){
         
      Stripe_webservice_setting__c op = new Stripe_webservice_setting__c ();
    op.StripeCard_Service__c = true;
    op.StripeCardDelete_Service__c = true;
    op.StripeCustomer_Service__c = true;
    insert op;
           system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        Test.startTest(); 
          contact co = new contact();
            co.LastName ='vijay';
            co.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          insert co;
     
          Stripe_Profile__c sp = new Stripe_Profile__c();
          sp.Stripe_Customer_Id__c='cus_DYWAlCN3vsdH4k';
          sp.Customer__c =co.id;
          insert sp;
         // customer ='cus_DYWAlCN3vsdH4k';
                   
         Card__c c = new Card__c ();
          c.Stripe_Profile__c=sp.id;
          c.contact__c=co.id;
          c.Billing_Zip_Postal_Code__c='6863';
          c.Card_ID__c ='79203';
          c.Cvc_Check__c='pass';
          c.Billing_Zip_Postal_Code__c ='121004';
          c.Card_ID__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          c.Cvc_Check__c =null;
          c.Billing_Street__c = 'HR';
           c.Billing_Country__c ='US';
           c.Billing_City__c = 'Delhi';
           c.Expiry_Year__c = '2021';
           c.Billing_State_Province__c='HR'; 
           c.Stripe_Card_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
           c.Last4__c = '4242';
          c.Expiry_Month__c = '11';
          c.Name_On_Card__c ='Ashish Sharma';
          c.Brand__c = 'visa';
          insert c;
          Card_Opp_Temporary__c cd = new Card_Opp_Temporary__c();
          cd.Invoice_Id__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          cd.CardId__c = 'card_1D7P9xBwLSk1v1ohQezYknMQ';
          insert cd;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_CardService'; 
          String body ='{ "id": "evt_1FWiICFzCf73siP01W5wIHKO", "object": "event", "api_version": "2018-02-28", "created": 1571831179, "data": { "object": { "id": "src_1FWiI8FzCf73siP0DAJm1WoW", "object": "source", "amount": null, "card": { "exp_month": 8, "exp_year": 2023, "last4": "2012", "country": "US", "brand": "American Express", "address_line1_check": "unavailable", "address_zip_check": "unavailable", "cvc_check": "unavailable", "funding": "credit", "fingerprint": "Zp34Ghmo06R08KxI", "three_d_secure": "not_supported", "name": null, "tokenization_method": null, "dynamic_last4": null }, "client_secret": "src_client_secret_G2ntNEEmqm7czyWgl6RLJqGu", "created": 1571831176, "currency": null, "customer": "cus_DYWAlCN3vsdH4k", "flow": "none", "livemode": true, "metadata": { }, "owner": { "address": { "city": "Albuquerque", "country": "US", "line1": "a09sjdfsdf", "line2": null, "postal_code": "87112", "state": "NM" }, "email": "grant+testtirthcheckout102319@ladyboss.com", "name": "Grant test", "phone": null, "verified_address": null, "verified_email": null, "verified_name": null, "verified_phone": null }, "statement_descriptor": null, "status": "chargeable", "type": "card", "usage": "reusable" } }, "livemode": true, "pending_webhooks": 6, "request": { "id": "req_d355ecpOU4LiwG", "idempotency_key": null }, "type": "customer.source.created" }';
        req.requestBody = Blob.valueOf(body);
           req.addHeader('Stripe-Signature', 't=1545993867,v1=df908d1ffae78cbdc9e5e060c0f05420fe6f105556da979a074be3b3352ae286');
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
          
        RestContext.response = res;
         
        StripeCard_Service.createCard();
         
   Test.stopTest(); 
        
    }
    
}