@istest
public class StripeSubscriptioin_Service_test {
    public static testMethod void test(){
        ContactTriggerFlag.isContactBatchRunning=true;
        String devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
         Test.setMock(HttpCalloutMock.class, new SubscriptionMock_Test());
        //system.Test.setMock(HttpCalloutMock.class, new StripeGetCardV1_MockTest());
        test.startTest();
        Account Acc =new Account();
        Acc.Name = 'Test';
        Acc.Credit_Card_Number__c = '2435643';
        Acc.Exp_Month__c = '12' ;
        Acc.Exp_Year__c = 2019 ;
        
        insert Acc ;
        Contact Con =new Contact();
        Con.LastName = 'Test Con';
        insert Con;
        
        Stripe_Product_Mapping__c sp = new Stripe_Product_Mapping__c();
        sp.name = 'dfg';
        sp.Stripe_Nick_Name__c='Transformation System-$237';
        sp.Salesforce_Product__c='prod_DDGQVEaH03NJ8p';
        insert sp;
        
        Stripe_Profile__c StripePro =new Stripe_Profile__c();
        StripePro.Stripe_Customer_Id__c = 'cus_Dl9tzT8tKU51LD';
        StripePro.Customer__c = Con.id ;
        
        
        insert StripePro ;
        
        
        Product2 prod = new Product2(Name = 'Transformation System-$237', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='plan_DDGQokobMChyR9');
        insert prod;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op = new Opportunity();
        op.Name = 'opp0' ;
        op.Amount=23700;
        op.CloseDate = Date.Today();
        op.StageName = 'Pending';
        //op.Subscription__c = 'sub_DlAEjFZvBklMuQ';
        op.AccountId = Acc.id;
        op.Contact__c = Con.Id;
        op.RecordTypeId = devRecordTypeId;
        insert op;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
        //ol.Subscription_Id__c ='sub_DlAEjFZvBklMuQ';
        ol.refund__c = 137;
        insert ol;
        
        System.debug('===========Opportunity========'+op);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body = '{ "id": "evt_1Dfl3cBwLSk1v1ohIMuxu1nW",  "object": "event", "api_version": "2018-02-06", "created": 1544434087, "data": { "object": { "id": "sub_DxxfD4W2fJc9Q6","object": "subscription","application_fee_percent": null,"billing": "charge_automatically","billing_cycle_anchor": 1542114948,"cancel_at_period_end": false,"canceled_at": 1544434087,"created": 1542114948,"current_period_end": 1544706948, "current_period_start": 1542114948,"customer": "cus_Dtr4r3VWpDRh4D","days_until_due": null,"default_source": null,"discount": null,"ended_at": 1544434087,"items": { "object": "list","data": [{ "id": "si_Dxxf9SQf3s8jz3", "object": "subscription_item", "created": 1542114948,"metadata": { }, "plan": {"id": "plan_DDGQokobMChyR9","object": "plan","active": true, "aggregate_usage": null, "amount": 23700, "billing_scheme": "per_unit", "created": 1531344662,"currency": "usd","interval": "month", "interval_count": 1,"livemode": false, "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p","tiers": null, "tiers_mode": null,"transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "subscription": "sub_DxxfD4W2fJc9Q6" }], "has_more": false,"total_count": 1, "url": "/v1/subscription_items?subscription=sub_DxxfD4W2fJc9Q6"},"livemode": false,"metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan","active": true, "aggregate_usage": null, "amount": 23700,"billing_scheme": "per_unit", "created": 1531344662, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false,  "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "start": 1542114948, "status": "canceled", "tax_percent": 5.12, "trial_end": null, "trial_start": null } }, "livemode": false, "pending_webhooks": 5, "request": { "id": "req_wKLg8pf7yeBaFH", "idempotency_key": null },  "type": "customer.subscription.deleted"}';
        
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        System.debug('===========RestContext.request========'+RestContext.request);
        
       
        StripeSubscriptioin_Service.createCustomer();
        
        test.stopTest();         
    }
    
    static testMethod void testSubscription1(){
        Account acc = new Account();
         acc.Name = 'testAcc';
         acc.Credit_Card_Number__c = '8327' ;
         acc.Exp_Month__c = '6';
         acc.Exp_Year__c = 2040;
             
        contact con = new contact();
         con.LastName = 'vikas';
         insert con;
        
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        cd.Card_ID__c= '234';
        insert cd;
        
      Stripe_Profile__c stpro = new Stripe_Profile__c();
        stpro.Customer__c = con.id;
        stpro.Subscription_Processed__c = false;
         stpro.Stripe_Customer_Id__c = 'cus_Dtr4r3VWpDRh4D';
        insert stpro;
        update stpro;
         Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        //opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
       // opp.Subscription__c = 'sub_DxxfD4W2fJc9Q6';
        //opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = con.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
       // ol.Subscription_Id__c ='sub_DxxfD4W2fJc9Q6';
        ol.refund__c = 137;
        insert ol;
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
        
       Test.setMock(HttpCalloutMock.class, new SubscriptionMock_Test());
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
     test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body ='{ "id": "evt_1Dfl3cBwLSk1v1ohIMuxu1nW",  "object": "event", "api_version": "2018-02-06", "created": 1544434087, "data": { "object": { "id": "sub_DxxfD4W2fJc9Q6","object": "subscription","application_fee_percent": null,"billing": "charge_automatically","billing_cycle_anchor": 1542114948,"cancel_at_period_end": false,"canceled_at": 1544434087,"created": 1542114948,"current_period_end": 1544706948, "current_period_start": 1542114948,"customer": "cus_Dtr4r3VWpDRh4D","days_until_due": null,"default_source": null,"discount": null,"ended_at": 1544434087,"items": { "object": "list","data": [{ "id": "si_Dxxf9SQf3s8jz3", "object": "subscription_item", "created": 1542114948,"metadata": { }, "plan": {"id": "plan_DDGQokobMChyR9","object": "plan","active": true, "aggregate_usage": null, "amount": 23700, "billing_scheme": "per_unit", "created": 1531344662,"currency": "usd","interval": "month", "interval_count": 1,"livemode": false, "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p","tiers": null, "tiers_mode": null,"transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "subscription": "sub_DxxfD4W2fJc9Q6" }], "has_more": false,"total_count": 1, "url": "/v1/subscription_items?subscription=sub_DxxfD4W2fJc9Q6"},"livemode": false,"metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan","active": true, "aggregate_usage": null, "amount": 23700,"billing_scheme": "per_unit", "created": 1531344662, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false,  "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "start": 1542114948, "status": "canceled", "tax_percent": 5.12, "trial_end": null, "trial_start": null } }, "livemode": false, "pending_webhooks": 5, "request": { "id": "req_wKLg8pf7yeBaFH", "idempotency_key": null },  "type": "customer.subscription.deleted"}';
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        
        StripeSubscriptioin_Service.createCustomer();
        
        test.stopTest();
    }


static testMethod void testSubscription2(){
    try{
        Account acc = new Account();
         acc.Name = 'testAcc';
         acc.Credit_Card_Number__c = '8327' ;
         acc.Exp_Month__c = '6';
         acc.Exp_Year__c = 2040;
             
        contact con = new contact();
         con.LastName = 'vikas';
         insert con;
        
        Card__c cd = new Card__c();
        cd.CustomerID__c = 'jdlkoueo';
        cd.Card_ID__c= '234';
        insert cd;
        
      Stripe_Profile__c stpro = new Stripe_Profile__c();
        stpro.Customer__c = con.id;
        stpro.Subscription_Processed__c = false;
         stpro.Stripe_Customer_Id__c = 'cus_Dtr4r3VWpDRh4D';
        insert stpro;
        update stpro;
         Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
         Opportunity opp = new Opportunity();
        opp.name ='oppName';
        //opp.RecordTypeId = devRecordTypeId;
        //opp.Stripe_Charge_Id__c= 'ch_1DafZmBwLSk1v1ohUH1yKzvh';
        opp.Amount = 137;
        opp.Refund_Amount__c =137;
       // opp.Subscription__c = 'sub_DxxfD4W2fJc9Q6';
        //opp.Success_Failure_Message__c = 'charge.succeeded';
        opp.CloseDate = date.today();
        opp.StageName ='Closed won';
        opp.Contact__c = con.Id;
        opp.Stripe_Message__c= 'Success';
        opp.Status__c ='active';
        opp.Quantity__c =1;
        insert opp;
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.OpportunityId = opp.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 137;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Name__c='CLUB';
       // ol.Subscription_Id__c ='sub_DxxfD4W2fJc9Q6';
        ol.refund__c = 137;
        insert ol;
       /* StripeListSubscriptionClone.Data stripedata = new StripeListSubscriptionClone.Data();*/
        
       Test.setMock(HttpCalloutMock.class, new SubscriptionMock_Test());
        List<Stripe_Profile__c > lst = new List<Stripe_Profile__c >();
     test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Stripe_SubscriptionService'; 
        
        String body ='{ "id" "evt_1Dfl3cBwLSk1v1ohIMuxu1nW,  "object": "event", "api_version": "2018-02-06", "created": 1544434087, "data": { "object": { "id": "sub_DxxfD4W2fJc9Q6","object": "subscription","application_fee_percent": null,"billing": "charge_automatically","billing_cycle_anchor": 1542114948,"cancel_at_period_end": false,"canceled_at": 1544434087,"created": 1542114948,"current_period_end": 1544706948, "current_period_start": 1542114948,"customer": "cus_Dtr4r3VWpDRh4D","days_until_due": null,"default_source": null,"discount": null,"ended_at": 1544434087,"items": { "object": "list","data": [{ "id": "si_Dxxf9SQf3s8jz3", "object": "subscription_item", "created": 1542114948,"metadata": { }, "plan": {"id": "plan_DDGQokobMChyR9","object": "plan","active": true, "aggregate_usage": null, "amount": 23700, "billing_scheme": "per_unit", "created": 1531344662,"currency": "usd","interval": "month", "interval_count": 1,"livemode": false, "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p","tiers": null, "tiers_mode": null,"transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "subscription": "sub_DxxfD4W2fJc9Q6" }], "has_more": false,"total_count": 1, "url": "/v1/subscription_items?subscription=sub_DxxfD4W2fJc9Q6"},"livemode": false,"metadata": { }, "plan": { "id": "plan_DDGQokobMChyR9", "object": "plan","active": true, "aggregate_usage": null, "amount": 23700,"billing_scheme": "per_unit", "created": 1531344662, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false,  "metadata": { }, "nickname": "Transformation System-$237","product": "prod_DDGQVEaH03NJ8p", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed"}, "quantity": 1, "start": 1542114948, "status": "canceled", "tax_percent": 5.12, "trial_end": null, "trial_start": null } }, "livemode": false, "pending_webhooks": 5, "request": { "id": "req_wKLg8pf7yeBaFH", "idempotency_key": null },  "type": "customer.subscription.deleted"}';
        req.requestBody = Blob.valueOf(body);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        
        RestContext.response = res;
        
        StripeSubscriptioin_Service.createCustomer();
        
        test.stopTest();
        }
        catch(Exception ex) 
        {     
            System.debug('----e---'+ex);        
        } 
    }
    
    
}