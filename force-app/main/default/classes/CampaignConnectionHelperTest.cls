@isTest (SeeAllData=true)
public class CampaignConnectionHelperTest {
    
    @isTest static void CampMemberTest() {
        
        Contact con = new Contact(); 
        con.LastName = 'badole';
        con.Email = 'test@test.com';
        insert con;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Closed Won',Amount=20,
                                         contact__c = con.id);
        insert opp;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'test';
        cmp.Type = 'Phone team';
        insert cmp; 
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        insert cmpMem;
        
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        conIdSet.add(con.id);
        List<CampaignMember> camMembList = new List<CampaignMember>() ;
        camMembList.add(cmpMem);
        
        Test.startTest();
        CampaignConnectionHelper.getCampaignConnection(CamMemberIdSet, conIdSet);
        Test.stopTest();
    }
    @isTest static void CampMemberTest1() {
        
        Contact con = new Contact();
        con.LastName = 'badole';
        con.Email = 'sourabh@test.com';
        insert con;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Qualification', Amount=20, contact__c = con.id, campaign__c = 'TestcamMem');
        insert opp;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Sourabh Test';
        cmp.Type = 'Phone team';
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Sourabh Test 2';
        cmp2.Type = 'Phone team';
        insert cmp2;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        
        insert cmpMem;
        
        CampaignMember cmpMem2 = new CampaignMember();
        cmpMem2.CampaignId = cmp2.Id;
        cmpMem2.ContactId = con.Id;
        cmpMem2.status='Sent';
        
        
        insert cmpMem2;
        
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        CamMemberIdSet.add(cmpMem2.id);
        conIdSet.add(con.id);
                
        Test.startTest();
        CampaignConnectionHelper.getCampaignConnection(CamMemberIdSet, conIdSet);
        Test.stopTest();
        
    }
    @isTest static void CampMemberTest2() {
        
        Contact con = new Contact(); 
        con.LastName = 'badole sf';
        con.Email = 'test@testsf.com';
        insert con;
        
        Opportunity opp = new Opportunity(Name='Test 2',CloseDate=system.today(), StageName='Closed Won',Amount=20,
                                         contact__c = con.id);
        insert opp;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'test 2';
        cmp.Type = 'Phone team';
        insert cmp; 
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        insert cmpMem;
        
        
        Set<id> conIdSet = new Set<id>();
        List<CampaignMember> camMembList = new List<CampaignMember>() ;
        conIdSet.add(con.id);
        camMembList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where id =: cmpMem.id and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null  ];
                
        
        Test.startTest();
        CampaignConnectionHelper.getCampOppByContactUpdateTrigger(camMembList, conIdSet);
        Test.stopTest();
    }
 
    @isTest static void CampMemberTest3() {
        
        Contact con = new Contact();
        con.LastName = 'badole';
        con.Email = 'sourabh@test.com';
        insert con;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Sourabh Test';
        cmp.Type = 'Phone team';
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Sourabh Test 2';
        cmp2.Type = 'Phone team';
        insert cmp2;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        
        insert cmpMem;
        
        CampaignMember cmpMem2 = new CampaignMember();
        cmpMem2.CampaignId = cmp2.Id;
        cmpMem2.ContactId = con.Id;
        cmpMem2.status='Sent';
        
        
        insert cmpMem2;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Qualification', Amount=20, contact__c = con.id);
        insert opp;
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        CamMemberIdSet.add(cmpMem2.id);
        conIdSet.add(con.id);
                
        Test.startTest();
        CampaignConnectionHelper.getCampaignConnection(CamMemberIdSet, conIdSet);
        Test.stopTest();
        
    }
    @isTest static void CampMemberTest4() {
        
        Contact con = new Contact();
        con.LastName = 'badole';
        con.Email = 'sourabh@test.com';
        insert con;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Sourabh Test';
        cmp.Type = 'Phone team';
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Sourabh Test 2';
        cmp2.Type = 'Phone team';
        insert cmp2;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        
        insert cmpMem;
        
        CampaignMember cmpMem2 = new CampaignMember();
        cmpMem2.CampaignId = cmp2.Id;
        cmpMem2.ContactId = con.Id;
        cmpMem2.status='Sent';
        
        
        insert cmpMem2;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Qualification', Amount=20, contact__c = con.id);
        insert opp;
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        CamMemberIdSet.add(cmpMem2.id);
        conIdSet.add(con.id);
        
        List<CampaignMember> camMembList = new List<CampaignMember>() ;
        camMembList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where id =: CamMemberIdSet and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null  ];
        
        
        Test.startTest();
        CampaignConnectionHelper.getCampOppByContactUpdateTrigger(camMembList, conIdSet);
        Test.stopTest();
        
    }
    @isTest static void CampMemberTest5() {
        
        Contact con = new Contact();
        con.LastName = 'badole';
        con.Email = 'sourabh@test.com';
        insert con;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), StageName='Qualification', Amount=20, contact__c = con.id , Campaign__c = 'CampMembtest');
        insert opp;
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Sourabh Test';
        cmp.Type = 'Phone team';
        insert cmp;
        
        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Sourabh Test 2';
        cmp2.Type = 'Phone team';
        insert cmp2;
        
        CampaignMember cmpMem = new CampaignMember();
        cmpMem.CampaignId = cmp.Id;
        cmpMem.ContactId = con.Id;
        cmpMem.status='Sent';
        
        insert cmpMem;
        
        CampaignMember cmpMem2 = new CampaignMember();
        cmpMem2.CampaignId = cmp2.Id;
        cmpMem2.ContactId = con.Id;
        cmpMem2.status='Sent';
        
        
        insert cmpMem2;
        
        Set<id> CamMemberIdSet = new Set<id>() ;
        Set<id> conIdSet = new Set<id>();
        CamMemberIdSet.add(cmpMem.id);
        CamMemberIdSet.add(cmpMem2.id);
        conIdSet.add(con.id);
        
        List<CampaignMember> camMembList = new List<CampaignMember>() ;
        camMembList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where id =: CamMemberIdSet and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null  ];
        
        
        Test.startTest();
        CampaignConnectionHelper.getCampOppByContactUpdateTrigger(camMembList, conIdSet);
        Test.stopTest();
        
    }
}