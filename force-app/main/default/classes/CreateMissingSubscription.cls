global class CreateMissingSubscription implements  Database.Batchable<sObject>,Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT cust_id__c,Processed__c FROM Missing_Subscription__c where Processed__c=false AND cust_id__c!=null');
    }
    
    global void execute(Database.BatchableContext bc, List<Missing_Subscription__c> msList){
        List<Stripe_Profile__c> strProfile = [SELECT Customer__c,Stripe_Customer_Id__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c=:msList.get(0).cust_id__c];
        String contId = strProfile.get(0).Customer__c;
        System.debug('contId '+contId);
        try{
            if(!Test.isRunningTest()){
            	SubscriptionsController.refreshSubscription(contId);
            }else{
                System.debug(1/0);
            }
        }catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error('CreateMissingSubscription','batch',msList.get(0).cust_id__c,e) );
        }
        msList.get(0).Processed__c = true;
        update msList;
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}