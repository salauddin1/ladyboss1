global class StripeCreateInvoiceItem {
	private static final String SERVICE_URL = 'https://api.stripe.com/v1/invoiceitems';
	global String id;
    global StripeChargeError error;
    global String invoice;
	global static StripeCreateInvoiceItem createInvoiceItem(String description,String customerId,String stripeCurrency,Decimal amount){
		amount = Integer.valueOf(amount*100);
		HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setBody('currency='+stripeCurrency+'&customer='+customerId+'&description='+description+'&amount='+amount+'&tax_rates[0]=txr_1CLEiqFzCf73siP0R9ciie0u');
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){
            try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }else{
            hs.setStatusCode(200);
        }
         system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
         try {
            StripeCreateInvoiceItem o = StripeCreateInvoiceItem.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
	}
    global static void createInvoiceItem(String description,String customerId,String stripeCurrency,Decimal amount,String subId){
        amount = Integer.valueOf(amount*100);
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        http.setBody('currency='+stripeCurrency+'&customer='+customerId+'&description='+description+'&amount='+amount+'&subscription='+subId+'&tax_rates[0]=txr_1CLEiqFzCf73siP0R9ciie0u');
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){
            try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    
                }
        }else{
            hs.setStatusCode(200);
        }
         system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
         try {
            StripeCreateInvoiceItem o = StripeCreateInvoiceItem.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            
        }
    }
	public static StripeCreateInvoiceItem parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (StripeCreateInvoiceItem) System.JSON.deserialize(json, StripeCreateInvoiceItem.class);
    }
}