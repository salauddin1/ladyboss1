/*
* Devloper : Sourabh Badole
* Description : This Class is use as test class to cover code of OpportunityRollUp trigger and LREngine.
*/
@isTest
public class OpportunityRollUpTest {
    @isTest
    Public static void testOppRollup(){
        Async_Apex_Limit__c  asyncLimit = new Async_Apex_Limit__c();
        asyncLimit.Remaining__c  = 100000 ;
        asyncLimit.Name = 'Async Limit Left';
        insert asyncLimit ;
        Contact con = new Contact(
            FirstName ='Sourabh',
            LastName = 'Badole',
            Email = 'badole.salesforce1@gmail.com');
        
        insert con; 
        
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id ;
        opp.Name = 'Test ' ;
        opp.StageName = 'Qualification' ;
        opp.CloseDate = Date.today();
        opp.Amount = 50.00;
        
        Test.StartTest(); 
        insert opp ;
        
        opp.Amount = 100.00;
        
        update opp ;
        
        Test.StopTest();
    }
    @isTest
    public static void CampaignConnectionHandlerTest(){
        Async_Apex_Limit__c  asyncLimit = new Async_Apex_Limit__c();
        asyncLimit.Remaining__c  = 100000 ;
        asyncLimit.Name = 'Async Limit Left';
        insert asyncLimit ;
        
        Campaign camp1 = new Campaign(Name = 'oppCampaign', type = 'Phone Team' , IsActive = True);
        insert camp1;
        Campaign camp2 = new Campaign(Name = 'oppCampaign2',type = 'Phone Team' , IsActive = True);
        insert camp2;
        Contact con = new Contact();
        con.LastName = 'Badole';
        con.Email = 'badole.salesforce@gmail.com';
        insert con ; 
        
        CampaignMember newMember  = new CampaignMember(ContactId = con.id, status='Sent', campaignid = camp1.id);
        insert newMember;
        
        CampaignMember newMember2 = new CampaignMember(ContactId = con.id, status='Sent', campaignid = camp2.id);
        insert newMember2;
        
        Triggers_activation__c op = new Triggers_activation__c();
        op.SuccessPaymentWithCaseLink__c = true;
        insert op ;
        Opportunity testOpp = new Opportunity (Name = 'Test Name',                       
                                               StageName = 'Open',
                                               Amount = 5000.00,
                                               
                                               CloseDate = System.today(),
                                               Contact__c = con.id 
                                              );
        insert testOpp;
        
    }
}