@isTest
public class StripeBalanceComponentV1ContTest {
    @IsTest
    static void methodName(){
        Contact c=new Contact();
        c.email='ashish.sharma.devsfdc75@gmail.com';
        c.LastName='sharma';
        insert c;
        
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Stripe_Customer_Id__c='cus_BUOWQlyttHhSXd';
        sp.Customer__c=c.id;
        insert sp;
        StripeBalanceComponentV1Cont.conWrapper cw = new StripeBalanceComponentV1Cont.conWrapper();
        cw.id = sp.id;
        cw.customerId = sp.Stripe_Customer_Id__c;
        Test.startTest();
        StripeBalanceComponentV1Cont.getConDetails(c.id);
        StripeBalanceComponentV1Cont.UpdateBalance(JSON.serialize(cw), 2.0, 3.25);
        StripeBalanceComponentV1Cont.refreshBalance(c.ID);
        Test.stopTest();
        
    }
}