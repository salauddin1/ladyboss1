@isTest
public class ShipStation_Status_Update_Test {
    @isTest
    public static void test(){
         ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
       
        ShipstationUpdate_pages__c pages = new ShipstationUpdate_pages__c();
        pages.End_page__c =100;
        pages.Start_Page__c =1;
        insert pages;
        ShipstationCancel_pages__c pages1 = new ShipstationCancel_pages__c();
        pages1.End_page__c =100;
        pages1.Start_Page__c =1;
        insert pages1;
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new SHipStationRoutingWebhookClassMock());
        ShipStation_Status_Update.getStatusTotal();
         System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new ShipStationStatus_Schedule());
        test.stopTest();
    }
}