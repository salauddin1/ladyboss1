//Basically class used for to show the subscriptions and to send sms/Email to
//Customer to notify card updates
Public class UpdateCardApex{
    //Get the list of cards by case ID
    @AuraEnabled
    public static List<card__c> getCards(String caseId ){
        
        Case ts = [select id,ContactId from case where id=: caseId];
        if(ts!=null && ts.ContactId !=null){
            List<card__c> cardList = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email,Contact__r.phone, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: ts.ContactId and Error_Message__c != 'Your card number is incorrect.'];
            return cardList ;
        }else{return null;}
    }
    //Get all the opportunityLineItems by the case Id
    @AuraEnabled
    public static List<OpportunityLineItem > getCardsOpportunity(String caseId ){
        Case ts = [select id,ContactId from Case where id=: caseId];
        if(ts!=null && ts.ContactId !=null){
            
            List<card__c> cardList1 = [SELECT Id,Credit_Card_Number__c , Expiry_Year__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where Contact__c =: ts.ContactId and Error_Message__c != 'Your card number is incorrect.'];
            Set<Id> cardSet = new Set<ID>();
            for(card__c c : cardList1 ) {
                
                cardSet.add(c.id);
                
            }
            //List the subscription and return the subscriptions
            //List<Opportunity> lstOpp = [select id , name,Card__c  from Opportunity where Card__c IN : cardSet and recordType.developername = 'Subscription' and     (Success_Failure_Message__c !='customer.subscription.deleted' ) ];
            List<OpportunityLineItem > lstOpp = [SELECT Id, Product2.name,Opportunity.Card__c  ,OpportunityId, Name FROM OpportunityLineItem where Opportunity.Card__c IN : cardSet  and  Opportunity.recordType.developername = 'Subscription'  and   (Opportunity.Success_Failure_Message__c !='customer.subscription.deleted' and Opportunity.Success_Failure_Message__c !='canceled')];
            return lstOpp ;
        }else{return null;}
    }
    //Method will send the SMS to the customer about the card update notification
    @AuraEnabled
    public static string sendSMS(string CardId,string EmailId,string customerId,string Last4Digit,string caseId,String phoneNum) {
        System.debug('==phoneNum===='+phoneNum);
        if(phoneNum!=null) {
           //List all the cards
           List<card__c> cardList = [SELECT Id, Expiry_Year__c,Credit_Card_Number__c,Stripe_Profile__r.Customer__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where id =: CardId];
        if(cardList.size() > 0){
            Integer randNum = Math.round(Math.random()*10000000);
            String str = cardList[0].Credit_Card_Number__c;
            //List<Card__c> cardL = [select id,isCardUpdateNotified__c,Stripe_Profile__r.Customer__c  from card__c where id=: CardId];
            Contact c = [Select Id, FirstName,email FROM Contact WHERE Id=: cardList[0].Stripe_Profile__r.Customer__c ]; 
            cardList[0].UniqueEmailId__c = String.valueOf(randNum); 
            //Create the message body 
            String str1 = 'Hello '+c.firstName+',\n We noticed that your card might need some lovin\'. Please Click this https://ladyboss-support.secure.force.com/UpdateCardCloneCase?id='+str+'&source=SMS&key='+cardList[0].UniqueEmailId__c +' to update your card ending in '+cardList[0].Last4__c +'.\n This update method is 100% safe and secure, backed by the highest industry standards for credit/debit card security.\nThank you!\nTeam LadyBoss'; 
            
            Map<String,Object> mapOutput = new  Map<String,Object>();
            //Get the twilio configurations 
            List<Twilio_Configuration__mdt> lstTwilliConfigs = [SELECT AccountSID__c, AuthToken__c, From_Number__c FROM Twilio_Configuration__mdt where MasterLabel = 'TwilioMain'];
            String accountSID = lstTwilliConfigs.get(0).AccountSID__c;  //'AC44099911ff7ec99c40bc97943ad6c0ca';
            String authToken = lstTwilliConfigs.get(0).AuthToken__c; //'6a386f07923a55ec0729eff283d747a3';
            String fromNumber = lstTwilliConfigs.get(0).From_Number__c; //'15053226116';
            String url = 'https://api.twilio.com/2010-04-01/Accounts/'+accountSID+'/Messages.json';
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            //Set the request body,
            req.setBody('To='+ EncodingUtil.urlEncode(phoneNum,'UTF-8')+'&From='+fromNumber+'&Body='+EncodingUtil.urlEncode(str1 ,'UTF-8'));
            Blob headerValue = Blob.valueOf(accountSID + ':' + authToken);
            String authorizationHeader = 'Basic '+EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            Http http = new Http();
            TwillioResponse tl;
            if(!test.isrunningTest()){
                //Send the request
                HTTPResponse res = http.send(req);
                tl = (TwillioResponse) System.JSON.deserialize(res.getBody(), TwillioResponse.class);
                
                if(tl.sid != null)  {
                    System.debug('tl=='+str1 );
                    
                    return 'SMS Send Successful';
                    
                }else  {
                    return 'SMS Send Failed';
                }
            }
        } 
        }
        
        return 'SMS Not sent!!';
        
    }
    
    
    //Send email to the customer to notify the card update
    @AuraEnabled
    public static string sendEmail(string CardId,string EmailId,string customerId,string Last4Digit,string caseId,String phoneNum) {
        List<card__c> cardList = [SELECT Id, Expiry_Year__c,CaseId__c,Credit_Card_Number__c,Stripe_Profile__r.Customer__c,Stripe_Profile__r.Stripe_Customer_Id__c, Expiry_Month__c, Contact__r.email, CustomerID__c,Last4__c , Stripe_Profile__c FROM Card__c where id =: CardId];
        if(cardList.size() > 0){
            String str = cardList[0].Credit_Card_Number__c;
            //List<Card__c> cardL = [select id,isCardUpdateNotified__c,Stripe_Profile__r.Customer__c  from card__c where id=: CardId];
            Contact c = [Select Id, FirstName,email FROM Contact WHERE Id=: cardList[0].Stripe_Profile__r.Customer__c ]; 
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            Integer randNum = Math.round(Math.random()*10000000);
            cardList[0].UniqueEmailId__c = String.valueOf(randNum); 
            cardList[0].CaseId__c= caseId;
            update cardList;
            if(EmailId!='' && customerId!=''){
                
                //Get the email template
                EmailTemplate emailTemplate = [select Id,developerName, Subject, HtmlValue, Body from EmailTemplate where developerName = 'Update_CardCase_Email_Template' limit 1];
                //Prepare for the single email messgae object
                Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
                
                
                //email.setUseSignature(false);
                email.setTargetObjectId(c.id);
                email.setWhatId(cardList[0].id);
                
                email.setSaveAsActivity(false);
                email.setTemplateId(emailTemplate.Id);
                
                
                try{
                    if(!test.isrunningTest()){
                    //Send the email
                        Messaging.sendEmail(new Messaging.SingleEmailmessage[] {email});}
                    //This code snippet will mve the case to account recovery and also status set to Email sent
                    List<Case> csList =  new List<case>();
                    Case ts= [select id,IsFailed_Payment__c,Subject,ContactId,DueDate__c,Contact_Email__c,Contact_Phone__c,Called__c,Recovered_Payment__c,Saved_Payment__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c  from case where id= :caseId];
                    if(ts!=null) 
                    {
                        Group gr = [SELECT CreatedDate,DeveloperName FROM Group where DeveloperName =: 'Account_Recovery'];
                        if( gr!=NULL) {
                            if(gr.id!=null && ts.IsFailed_Payment__c){
                                ts.OwnerId = gr.id;
                                ts.UpdateCardStatus__c = 'Email Sent';
                            }
                            csList.add(ts);
                            
                        }
                        if(csList.size() > 0  && !test.isrunningTest()){
                            update csList;
                        }
                        //Creates the email tracker
                        Custom_Tracker__c ct = new Custom_Tracker__c();
                        ct.Contact__c = c.id;
                        ct.Card__c = cardList[0].id;
                        ct.UniqueId__c =String.valueOf(randNum); 
                        insert ct;
                    }
                    
                    return 'Email sent to '+EmailId;
                }catch(exception e){
                    return 'Email not sent!!';
                }
                
                
            }
            
        }   
        return null;
    }
    class TwillioResponse  {
        public String sid;  
        public String message;
    }
}