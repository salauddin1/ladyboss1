@isTest
public class createAffiliateControllerTest {
    @isTest
    public static void testMethod1(){
        Test.setMock(HttpCalloutMock.class, new createAffiliateMock());
        pap_credentials__c pa = new pap_credentials__c();
        pa.username__c='test@test.com';
        pa.password__c='test';
        pa.pap_url__c='https://test.postaffiliatepro.com';
        pa.Name='pap url';
        pa.X3FF_Campaign_Id__c = 'test';
        insert pa;
        Create_Coupon__c cc = new Create_Coupon__c();
        cc.Name = 'Default';
        cc.Discount_Type__c = 'percent';
        cc.Amount__c = 10;
        cc.Product_Categories__c = '32,22';
        cc.Product_Exclude_Categories__c = '22,32';
        insert cc;
        Create_Site_Coupon__c cs = new Create_Site_Coupon__c();
        cs.Name ='Store';
        cs.Site_Url__c = 'www.test.com';
        cs.Create_Coupon_Site_URL__c = 'https://ladyboss.com/PLAYGROUNDtestonlywfioevbou4y3g847rg7ryifvow8eyvfq97foufbq3874qvf7yfvi/wp-json/wc/v3/coupons?consumer_key=ck_59ade5ae29888b0f779ab12ebcefe38aab63367b&consumer_secret=cs_5a498e1a3aa905b1a857697f2c5f42e5e2740ac4';
        insert cs;     
        
        Contact ct = new Contact();
        ct.Email = 'tirth@test.com';
        ct.LastName = 'test';
        ct.FirstName = 'test';
        insert ct;
        
        Contact ct1 = new Contact();
        ct1.Email = 'tirth@test.com';
        ct1.LastName = 'test';
        ct1.FirstName = 'test';
        ct1.PAP_refid__c ='test1';
        insert ct1;
        
        ct1.PAP_refid__c ='test2';
        ct1.PAPUserId__c = 'abcd';
        ct1.PAPRPassword__c = 'abcd';
        //update ct1;
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity opp = new Opportunity(Name='Test',CloseDate=system.today(), 
                                          StageName='Closed Won',Contact__c=ct.Id,
                                         RecordtypeId = recordTypeId);
        insert opp;
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        String jsonBody = JSON.serialize(oppList);
        createAffiliateController ca = new createAffiliateController(jsonBody);
        createAffiliateController.getContact(ct.id);
        Test.startTest();
        createAffiliateController.createAffiliate(ct.id);
        createAffiliateController.updateAffiliate(ct.id,'abc');
        Test.stopTest();
    }
}