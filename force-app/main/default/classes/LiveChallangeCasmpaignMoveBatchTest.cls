@isTest
public class LiveChallangeCasmpaignMoveBatchTest {
    @isTest
    public static void test1(){
        Campaign_Name_List__c cp = new Campaign_Name_List__c();
        cp.name= 'dsfsfgsfg';
        cp.CamapignNames__c = 'Live Campaign-1,Live Campaign-2';
        insert cp;
            
        Campaign live_campaign = new Campaign();
        live_campaign.Name = 'Live Challange Campaign';
        live_campaign.Live_Challenge_Campaign__c=true;
        live_campaign.Calling_Start_Date__c = Date.today().addDays(-2);
        live_campaign.Calling_End_Date__c = Date.today().addDays(2);
        live_campaign.Purchase_End_Date__c = Date.today().addDays(2);
        live_campaign.Purchase_Start_Date__c = Date.today().addDays(-2);
        live_campaign.Live_Challenge_Campaign__c = true;
        insert live_campaign;

		Contact ct1 = new Contact();
		ct1.First_Website_Sell__c = true;
        ct1.LastName = 'dfwefwe';
        insert ct1;
        
        CampaignMember cmpmem1 = new CampaignMember();
        cmpmem1.ContactID = ct1.id;
        cmpmem1.CampaignId = live_campaign.id;
        insert cmpmem1;
        Contact ct2 = new Contact();
		ct2.First_Website_Sell__c = true;
        ct2.LastName = 'svsfdg';
        insert ct2;
        Campaign cmp1 = new Campaign();
        cmp1.name='Live Campaign-1';
        insert cmp1;
        CampaignMember cmpmem2 = new CampaignMember();
        cmpmem2.ContactID = ct2.id;
        cmpmem2.CampaignId = cmp1.id;
        insert cmpmem2;        
        LiveChallangeCasmpaignMoveBatch bt = new LiveChallangeCasmpaignMoveBatch();
        Database.executeBatch(bt);
    }
}