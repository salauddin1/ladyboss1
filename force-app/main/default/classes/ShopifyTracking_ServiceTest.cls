@isTest
public class ShopifyTracking_ServiceTest {
    @isTest
    public Static void unittest(){
        String JSONBody='{"id":820982911946154508,"order_id":820982911946154508,"financial_status": "fulfilled","status":"pending","created_at":"2019-02-05T07:15:32-05:00","service":null,"updated_at":"2019-02-05T07:15:32-05:00","tracking_company":"UPS","shipment_status":null,"location_id":null,"email":"jon@doe.ca","destination":{"first_name":"Steve","address1":"123 Shipping Street","phone":"555-555-SHIP","city":"Shippington","zip":"40003","province":"Kentucky","country":"United States","last_name":"Shipper","address2":null,"company":"Shipping Company","latitude":null,"longitude":null,"name":"Steve Shipper","country_code":"US","province_code":"KY"},"tracking_number":"1z827wk74630","tracking_numbers":["1z827wk74630"],"tracking_url":"http://wwwapps.ups.com/etracking/tracking.cgi?InquiryNumber1=1z827wk74630/u0026TypeOfInquiryNumber=T/u0026AcceptUPSLicenseAgreement=yes/u0026submit=Track","tracking_urls":["http://wwwapps.ups.com/etracking/tracking.cgi?InquiryNumber1=1z827wk74630/u0026TypeOfInquiryNumber=T/u0026AcceptUPSLicenseAgreement=yes/u0026submit=Track"],"receipt":{},"name":"#9999.1","line_items":[{"id":487817672276298554,"variant_id":null,"title":"Aviator sunglasses","quantity":1,"price":"89.99","sku":"SKU2006-001","variant_title":null,"vendor":null,"fulfillment_service":"manual","product_id":788032119674292922,"requires_shipping":true,"taxable":true,"gift_card":false,"name":"Aviator sunglasses","variant_inventory_management":null,"properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":100,"total_discount":"0.00","fulfillment_status":"fulfilled","price_set":{"shop_money":{"amount":"89.99","currency_code":"INR"},"presentment_money":{"amount":"89.99","currency_code":"INR"}},"total_discount_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"discount_allocations":[],"tax_lines":[]},{"id":976318377106520349,"variant_id":null,"title":"Mid-century lounger","quantity":1,"price":"159.99","sku":"SKU2006-020","variant_title":null,"vendor":null,"fulfillment_service":"manual","product_id":788032119674292922,"requires_shipping":true,"taxable":true,"gift_card":false,"name":"Mid-century lounger","variant_inventory_management":null,"properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":1000,"total_discount":"5.00","fulfillment_status":null,"price_set":{"shop_money":{"amount":"159.99","currency_code":"INR"},"presentment_money":{"amount":"159.99","currency_code":"INR"}},"total_discount_set":{"shop_money":{"amount":"5.00","currency_code":"INR"},"presentment_money":{"amount":"5.00","currency_code":"INR"}},"discount_allocations":[{"amount":"5.00","discount_application_index":0,"amount_set":{"shop_money":{"amount":"5.00","currency_code":"INR"},"presentment_money":{"amount":"5.00","currency_code":"INR"}}}],"tax_lines":[]}]}';
        Contact con = new Contact();
        con.LastName='TestLast';
        con.FirstName='TestFirst';
        con.Email='Test@gmail.com';
        con.Phone='7067749234';
        insert con;
        Shopify_Orders__c ord = new Shopify_Orders__c();
        ord.Order_Id__c='820982911946154508';
        ord.fulfillment_Status__c='fulfilled';
        ord.Status__c='confirmed';
        insert ord;
       
        Shopify_Parameters__c tokens = new Shopify_Parameters__c ();
        tokens.Access_Token__c='1234redcre3edsx23wq3wed32w32wes32w';
        insert tokens;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new orderCalloutTest());
        RestRequest request = new RestRequest();
        request.requestUri ='https://ashish-ladyboss-support.cs53.force.com/ShipStation/services/apexrest/ShopifyTrackOrders';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONBody);
        RestContext.request = request;
        ShopifyTracking_Service.trackOrders();
        Test.stopTest();
    }
}