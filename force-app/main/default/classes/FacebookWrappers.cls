public class FacebookWrappers {
	public FacebookWrappers() {
		
	}
    
	public Class Paging{
		public Cursor cursors{get;set;}
		public String next{get;set;}
		public String previous{get;set;}
	}

	public Class Cursor{
		public String before{get;set;}
		public String after{get;set;}
	}

	public Class User{
		public String first_name{get;set;}
		public String last_name{get;set;}
		public String profile_pic{get;set;}
		public String id{get;set;}
	}

	public Class WehbookMessageWrapper{
		public ReciepientDetails recipient{get;set;}
		public ReciepientDetails sender{get;set;}
		public String timestamp{get;set;}
		public WehbookMessage message{get;set;}
	}

	public Class WehbookMessage{
		public String mid{get;set;}
		public String seq{get;set;}
		public String text{get;set;}
		public List<WehbookAttachment> attachments{get;set;}
	}

	public Class WehbookAttachment{
		public String type{get;set;}
		public WehbookPayload payload{get;set;}
	}

	public class WehbookPayload{
		public String url{get;set;}
	}

	public Class ImageData{
		public String url{get;set;}
		public String preview_url{get;set;}
	}

	public Class AttachmentData{
		public String id{get;set;}
		public String name{get;set;}
		public ImageData image_data{get;set;}
	}

	public Class MultipleAttachments{
		public List<AttachmentData> data{get;set;}
	}

	public Class PageObject{
		public String access_token{get;set;}		
		public String category{get;set;}		
		public String name{get;set;}		
		public String username{get;set;}		
		public String about{get;set;}		
		public String id{get;set;}
		public List<Category> category_list{get;set;}		
		public List<String> perms{get;set;}		
	}

	public Class ConversationObject{
		public String id{get;set;}
		public String link{get;set;}
		public Integer unread_count{get;set;}
		public ToDataWrapper senders{get;set;}
		public String updated_time{get;set;}		
	}

	public Class AccessToken{
        public String access_token{get;set;}
        public String token_type{get;set;}
        public String expires_in{get;set;}
    }

	public Class Category{
		public String id{get;set;}
		public String name{get;set;}
	}

	public Class PageWrapper{
		public List<PageObject> data{get;set;}
		public Paging paging{get;set;}
	}

	public Class ConversationWrapper{
		public List<ConversationObject> data{get;set;}
		public Paging paging{get;set;}
	}

	public Class ReciepientDetails{
		public String name{get;set;}
		public String email{get;set;}
		public String id{get;set;}
	}

	public Class ToDataWrapper{
		public List<ReciepientDetails> data{get;set;}
	}

	public Class MessageObject{
		public String message{get;set;}
		public String message_id{get;set;}
		public ReciepientDetails fromData{get;set;}
		public String created_time{get;set;}
		public String id{get;set;}
		public ToDataWrapper to{get;set;}
		public MultipleAttachments attachments{get;set;}
	}

	public Class MessageWrapper{
		public List<MessageObject> data{get;set;}
		public Paging paging{get;set;}
	}
}