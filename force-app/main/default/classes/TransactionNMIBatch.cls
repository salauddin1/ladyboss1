global class TransactionNMIBatch implements Database.Batchable<Opportunity>, Database.AllowsCallouts, Database.Stateful{

    public Boolean startingAfter {get;set;}
    public Map<String,Account> mapAccountTransaction {get;set;}
    public Map<String,Product2> mapProductTransaction {get;set;}
    public List<OpportunityLineItem> lstOlis {get;set;}
    
    public TransactionNMIBatch(Boolean stAfter)
    {
        system.debug('###construct TransactionNMIBatch with startingAfter');
        system.debug('###startingAfter: '+stAfter);
        startingAfter = stAfter;
    }
    
	public List<Opportunity> start(Database.BatchableContext BC ){
        system.debug('###start TransactionNMIBatch');
		List<Opportunity> lstOpportunity = new List<Opportunity>();
        HttpResponse res = new HttpResponse();
        Map<String,String> requestMap = new Map<String,String>();
        //requestMap.put('report_type','transaction');
        if(startingAfter){
            Datetime dt = system.now().addMinutes(-10);
            dt = dt.addSeconds(1);
            String afterDate = String.valueOf(dt.year());
            if(String.valueOf(dt.month()).length()==1){ afterDate = afterDate + '0' + dt.month(); }else{ afterDate = afterDate + dt.month(); }
            if(String.valueOf(dt.day()).length()==1){ afterDate = afterDate + '0' + dt.day(); }else{ afterDate = afterDate + dt.day(); }
            if(String.valueOf(dt.hour()).length()==1){ afterDate = afterDate + '0' + dt.hour(); }else{ afterDate = afterDate + dt.hour(); }
            if(String.valueOf(dt.minute()).length()==1){ afterDate = afterDate + '0' + dt.minute(); }else{ afterDate = afterDate + dt.minute(); }
            if(String.valueOf(dt.second()).length()==1){ afterDate = afterDate + '0' + dt.second(); }else{ afterDate = afterDate + dt.second(); }
            requestMap.put('start_date',afterDate);
        }
        system.debug('###requestMap: '+requestMap);
        res = PaymentGatewayNMI.HttpRequest(requestMap);

        if(res.getStatusCode()==200){
            //system.debug('##res: '+res.getBody());
            Opportunity opp;
            String name;
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern MyPattern;
            Matcher MyMatcher;
            mapAccountTransaction = new Map<String,Account>();
            mapProductTransaction = new Map<String,Product2>();
            lstOlis = new List<OpportunityLineItem>();
            List<Dom.XmlNode> transactions = new List<Dom.XmlNode>();
            transactions = PaymentGatewayNMI.XmlParser(res.getBody());   
            Account accTemp;
            Product2 proTemp;
            OpportunityLineItem oli;
            List<String> lstCustomerId = new List<String>();
            for(Dom.XmlNode childElement1 : transactions){
                if(childElement1.getName()=='transaction'){
                    for(Dom.XmlNode childElement2:childElement1.getChildElements()){
                        if(childElement2.getName()=='customerid'){
                            if(childElement2.getText()!='' && childElement2.getText()!=null) lstCustomerId.add(childElement2.getText());
                        }
                        if(childElement2.getName()=='product'){
                            proTemp = new Product2();
                            for(Dom.XmlNode childElement3:childElement2.getChildElements()){
                                if(childElement3.getName()=='description'){
                                    proTemp.Name = childElement3.getText();
                                }
                                if(childElement3.getName()=='sku'){
                                    proTemp.External_ID__c = childElement3.getText();
                                }
                            }
                            proTemp.Type__c = 'NMI';
                            proTemp.isActive = true;
                            if(proTemp.External_ID__c!=null && proTemp.External_ID__c!='' && proTemp.External_ID__c!='null') mapProductTransaction.put(proTemp.External_ID__c.trim().toLowerCase(),proTemp);
                        }
                    }
                }
            }
            
            system.debug('##lstCustomerId: '+lstCustomerId);
            Map<String,String> mapAccount = new Map<String,String>();
            for(Account acc : [Select Id, NMI_External_ID__c From Account WHERE NMI_External_ID__c IN: lstCustomerId]){
                mapAccount.put(acc.NMI_External_ID__c,acc.Id);
            }
            
            String oppIdFromNMI;
            for(Dom.XmlNode childElement1 : transactions){
                if(childElement1.getName()=='transaction'){
                    oppIdFromNMI = '';
                    opp = new Opportunity();
                    accTemp = new Account();
                    for(Dom.XmlNode childElement2:childElement1.getChildElements()){
                        //system.debug('##childElement2: '+childElement2.getName()+' '+childElement2.getText());
                        if(childElement2.getName()=='merchant_defined_field'){
                            oppIdFromNMI = childElement2.getText();
                        }
                        
                        if(childElement2.getName()=='transaction_id'){
                            opp.NMI_External_ID__c = childElement2.getText();
                            opp.Name = childElement2.getText();
                        }
                        
                        if(childElement2.getName()=='customerid'){
                            if(mapAccount.get(childElement2.getText())!=null){
                                opp.AccountId = mapAccount.get(childElement2.getText());
                            }
                        }
                        
                        if(childElement2.getName()=='condition'){
                            if(childElement2.getText()=='pendingsettlement'){
                            	opp.StageName = 'Approved';
                            }else if(childElement2.getText()=='canceled'){
                            	opp.StageName = 'Cancelled';
                            }else if(childElement2.getText()=='failed'){
                            	opp.StageName = 'Declined';
                            }else{
                            	opp.StageName = 'Pending';
                            }
                        }
                        
                        if(childElement2.getName()=='action'){
                            for(Dom.XmlNode childElement3:childElement2.getChildElements()){
                                if(childElement3.getName()=='amount'){
                            		opp.Amount = Decimal.valueOf(childElement3.getText());
                                }
                                
                                if(childElement3.getName()=='date'){
                                    Integer year = Integer.valueOf(childElement3.getText().subString(0,4));
                                    Integer month = Integer.valueOf(childElement3.getText().subString(4,6));
                                    Integer day = Integer.valueOf(childElement3.getText().subString(6,8));
                                    Date closeDate =  Date.newInstance(year, month, day);
                            		opp.CloseDate = closeDate;
                                }

                            }
                        }
                        
                        //Account
                        if(childElement2.getName()=='company'){
                            accTemp.Name = childElement2.getText();
                        }
                        if(childElement2.getName()=='first_name'){
                            accTemp.First_Name__c = childElement2.getText();
                            name = accTemp.First_Name__c;
                        }
                        if(childElement2.getName()=='last_name'){
                            accTemp.Last_Name__c = childElement2.getText();
                            name = name + ' ' + accTemp.Last_Name__c;
                        }
                        if(childElement2.getName()=='phone'){
                            accTemp.Phone = childElement2.getText();
                        }
                        
                        if(childElement2.getName()=='website'){
                            accTemp.Website = childElement2.getText();
                        }
                        if(childElement2.getName()=='email'){
                            if(childElement2.getText()!=null && childElement2.getText()!=''){
                                MyPattern = Pattern.compile(emailRegex);
                                MyMatcher = MyPattern.matcher(childElement2.getText());
                                if (MyMatcher.matches()){
                                    accTemp.Email__c = childElement2.getText();
                                }
                            }
                        }
                        if(childElement2.getName()=='fax'){
                            accTemp.Fax = childElement2.getText();
                        }
                        if(childElement2.getName()=='address_1'){
                            accTemp.BillingStreet = childElement2.getText();
                        }
                        if(childElement2.getName()=='city'){
                            accTemp.BillingCity = childElement2.getText();
                        }
                        if(childElement2.getName()=='state'){
                            accTemp.BillingState = childElement2.getText();
                        }
                        if(childElement2.getName()=='postal_code'){
                            accTemp.BillingPostalCode = childElement2.getText();
                        }
                        if(childElement2.getName()=='country'){
                            accTemp.BillingCountry = childElement2.getText();
                        }
                        if(childElement2.getName()=='shipping_address_1'){
                            accTemp.ShippingStreet = childElement2.getText();
                        }
                        if(childElement2.getName()=='shipping_city'){
                            accTemp.ShippingCity = childElement2.getText();
                        }
                        if(childElement2.getName()=='shipping_state'){
                            accTemp.ShippingState = childElement2.getText();
                        }
                        if(childElement2.getName()=='shipping_postal_code'){
                            accTemp.ShippingPostalCode = childElement2.getText();
                        }
                        if(childElement2.getName()=='shipping_country'){
                            accTemp.ShippingCountry = childElement2.getText();
                        }
                        
                        if(childElement2.getName()=='cc_number'){
                            accTemp.Credit_Card_Number__c = childElement2.getText();
                        }
                        if(childElement2.getName()=='cc_exp'){
                            if(childElement2.getText()!=null && childElement2.getText().trim()!=''){
                                accTemp.Exp_Month__c = String.valueOf(Integer.valueOf(childElement2.getText().subString(0,2)));
                                accTemp.Exp_Year__c = Decimal.valueOf('20'+childElement2.getText().subString(2,childElement2.getText().length()));
                            }
                        }
                        
                        //Opportunity Line Items
                        if(childElement2.getName()=='product'){
                            oli = new OpportunityLineItem();
                            oli.Opportunity_External_ID__c = opp.NMI_External_ID__c;
                            for(Dom.XmlNode childElement3:childElement2.getChildElements()){
                                if(childElement3.getName()=='quantity'){
                                    oli.Quantity = Integer.valueOf(childElement3.getText());
                                }
                                if(childElement3.getName()=='amount'){
                                    oli.UnitPrice = Decimal.valueOf(childElement3.getText());
                                }
                                if(childElement3.getName()=='sku'){
                                    oli.Product_ID__c = childElement3.getText();
                                }
                            }
                            if(oli.Product_ID__c!=null && oli.Product_ID__c!='' && oli.Product_ID__c!='null') lstOlis.add(oli);
                        }
                        
                    }
                    
                    if(oppIdFromNMI==''){
                        if(opp.CloseDate==null) opp.CloseDate = system.today();
                        lstOpportunity.add(opp);
                        
                        //Account
                        accTemp.NMI_External_ID__c = 'transaction-'+opp.NMI_External_ID__c;
                        accTemp.Type = 'Customer';
                        if(name!=null && name!='') name = name.trim();
                        if(accTemp.Name==null || accTemp.Name.trim()==''){
                            accTemp.Name = name;
                            if(accTemp.Name==null || accTemp.Name.trim()==''){
                                accTemp.Name = accTemp.Email__c;
                                if(accTemp.Name==null || accTemp.Name.trim()==''){
                                    accTemp.Name = accTemp.NMI_External_ID__c;
                                }
                            }
                        }
                        mapAccountTransaction.put(accTemp.NMI_External_ID__c,accTemp);
                    }
                }
            }
        }
        //system.debug('##lstOpportunity: '+lstOpportunity);
        return lstOpportunity;
	}
	
	public void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        system.debug('###execute TransactionNMIBatch');
        if(scope.size()>0){
            //Accounts
            Account accNew;
            List<Account> lstAccountInsert = new List<Account>();
            for(Opportunity opp : scope){
                if(opp.AccountId==null){
                    if(mapAccountTransaction.get('transaction-'+opp.NMI_External_ID__c)!=null){
                        accNew = mapAccountTransaction.get('transaction-'+opp.NMI_External_ID__c);
                        lstAccountInsert.add(accNew);
                    }
                }
            }
            ApexUtil.isTriggerInvoked = true;
            upsert lstAccountInsert NMI_External_ID__c;
            upsert mapProductTransaction.values() External_ID__c;
            ApexUtil.isTriggerInvoked = false;
            
            List<String> lstProductExt = new List<String>();
            for(OpportunityLineItem oli : lstOlis){
                if(oli.Product_ID__c!=null){
                    lstProductExt.add(oli.Product_ID__c);
                }
            }
            
            Map<String,String> mapPbe = new Map<String,String>();
            for(PricebookEntry pbe : [Select Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c IN: lstProductExt]){
                mapPbe.put(pbe.Product2.External_ID__c.trim().toLowerCase(),pbe.Id);
            }
            
            String pbId = '';
            if(!Test.isRunningTest()){
                pbId = [Select Id From Pricebook2 WHERE isStandard = true LIMIT 1].Id;
            }else{
                pbId = Test.getStandardPricebookId();
            }
            
            PricebookEntry pbeNew;
            Map<String,PricebookEntry> mapPbeInsert = new Map<String,PricebookEntry>();
            for(OpportunityLineItem oli : lstOlis){
                if(mapPbe.get(oli.Product_ID__c.trim().toLowerCase())==null && mapProductTransaction.get(oli.Product_ID__c.trim().toLowerCase())!=null){
                    pbeNew = new PricebookEntry();
                    pbeNew.IsActive = true;
                    pbeNew.Pricebook2Id = pbId;
                    pbeNew.Product2Id = mapProductTransaction.get(oli.Product_ID__c.trim().toLowerCase()).Id;
                    pbeNew.UnitPrice = oli.UnitPrice;
                    mapPbeInsert.put(oli.Product_ID__c.trim().toLowerCase(),pbeNew);
                }
            }
            
            ApexUtil.isTriggerInvoked = true;
            if(mapPbeInsert.size()>0) insert mapPbeInsert.values();
            upsert scope NMI_External_ID__c;
            ApexUtil.isTriggerInvoked = false;

            for(PricebookEntry pbe : [Select Id, Product2.External_ID__c From PricebookEntry WHERE Product2.External_ID__c IN: lstProductExt]){
                mapPbe.put(pbe.Product2.External_ID__c.trim().toLowerCase(),pbe.Id);
            }

            List<OpportunityLineItem> lstOlisInsert = new List<OpportunityLineItem>();
            Set<String> setOppId = new Set<String>();
            for(Opportunity opp : scope){
                if(opp.AccountId==null){
                    for(Account accx : lstAccountInsert){
                        if(accx.NMI_External_ID__c=='transaction-'+opp.NMI_External_ID__c){
                            opp.AccountId = accx.Id;
                        }
                    }
                }
                for(OpportunityLineItem oli : lstOlis){
                    setOppId.add(opp.Id);
                    if(oli.Opportunity_External_ID__c==opp.NMI_External_ID__c && mapPbe.get(oli.Product_ID__c.trim().toLowerCase())!=null){
                        oli.OpportunityId = opp.Id;
                        oli.PricebookEntryId = mapPbe.get(oli.Product_ID__c.trim().toLowerCase());
                    	lstOlisInsert.add(oli);
                    }
                }
            }
            
            ApexUtil.isTriggerInvoked = true;
            upsert scope NMI_External_ID__c;
            if(lstOlisInsert.size()>0){
                delete [Select id From OpportunityLineItem WHERE OpportunityId IN: setOppId];
                insert lstOlisInsert;
            }
            ApexUtil.isTriggerInvoked = false;
        }
	}
	
	public void finish(Database.BatchableContext BC) {
    	system.debug('###finish TransactionNMIBatch');
    }

}