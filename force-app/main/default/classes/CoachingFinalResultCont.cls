public class CoachingFinalResultCont {
    @AuraEnabled
    public static case getCaseRecord(Id caseId){
        List<case> subsequentList = [select id,Status,Cancel__c,Recovered__c,Not_recovered__c,Transfer__c,ContactId,Upsold__c,No_Response__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c from Case Where id=:caseId];
        return(subsequentList[0]);
    }  
    @AuraEnabled public static case saveCase(case cas,String AddRemoveDays,String fieldNameForDays){
        
        Boolean check = false ;
        if(fieldNameForDays == 'Transfer__c' && AddRemoveDays == 'true'){
            cas.status='Open';
            check = true ;
        }
        else if(fieldNameForDays == 'Cancel__c' && AddRemoveDays == 'true'){
            cas.status='Closed';
            check = true ;
        }
        else if(fieldNameForDays == 'Recovered__c' && AddRemoveDays == 'true'){
            cas.status='Closed';
            check = true ;           
        }
        else if(fieldNameForDays == 'Not_recovered__c' && AddRemoveDays == 'true'){
            cas.status='Closed';
            check = true ;
        }
        else if(fieldNameForDays == 'Upsold__c' && AddRemoveDays == 'true'){
            cas.status='Closed';
            check = true ;
        }
        else if(fieldNameForDays == 'No_Response__c' && AddRemoveDays == 'true'){
            cas.status='Closed';
            check = true ;
        }
        
        if(check == true ){
            try {
                update cas;    
            }
            catch(Exception ex){
                System.debug('** Exception **'+ex) ;
            }
        }
        
        
        return cas;
    }  
}