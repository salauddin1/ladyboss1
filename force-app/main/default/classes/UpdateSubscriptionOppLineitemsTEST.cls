@isTest
private class UpdateSubscriptionOppLineitemsTEST {
	
	@isTest static void test_method_one() {
		opportunity op  = new opportunity();
        
        op.name='opclub';
        
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.Clubbed__c = true;
        op.Stripe_Message__c = 'Success';
        op.CloseDate = System.today();
        insert op;
        op.Opportunity_Unique_Name__c = op.id;
        update op;
        
        
        
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        prod.Stripe_SKU_Id__c = 'abc';
        prod.Stripe_Product_Id__c = 'test';
        insert prod;
        
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        
        
        

		List<OpportunityLineItem> lstAccount= new List<OpportunityLineItem>();
        for(Integer i=0 ;i <200;i++){
            OpportunityLineItem ol = new OpportunityLineItem();
        
	        ol.Product2Id =prod.id;
	        ol.OpportunityId = op.Id;
	        ol.Quantity = 1;
	        ol.UnitPrice = 2.00;
	        ol.PricebookEntryId = customPrice.Id;
	        ol.Opportunity_Unique_Name__c = op.Id;
	        ol.Subscription_Id__c = 'test'+i;
            lstAccount.add(ol);
        }
        
        insert lstAccount;
        
        Test.startTest();

            UpdateSubscriptionOpportunityLineitems obj = new UpdateSubscriptionOpportunityLineitems('Subscription_Id__c != null');
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
	}
	
	
	
}