public without sharing class ReopenCaseFromCommuntiy {
    public static void reopenCase(List<CaseComment> caseComent){
        Set<String> IdSet = new Set<String>();
        Set<String> caseIdSet = new Set<String>();
        for(CaseComment csecmmt : caseComent) {
                IdSet.add(csecmmt.Id);
        }
        for(CaseComment csecmt : [select id, createdBy.Name,ParentId from casecomment where Id IN : IdSet]) {
            System.debug('==================='+csecmt.CreatedBy.Name);
            if(csecmt.CreatedBy.Name == 'LadyBossHub Site Guest User') {
                caseIdSet.add(csecmt.ParentId);
            }
        }
        List<Case> caseList = [select id, status from case where Id IN : caseIdSet];
        if(caseList.size()>0) {
            for(Case cseObj : caseList) {
                if(cseObj.Status == 'Closed') {
                    cseObj.Status = 'Reopened';
                }
            }
            
            update caseList;
        }
    }
}