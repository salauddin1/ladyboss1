@isTest
public class PAPTrigger_Test {
	@isTest
    public static void PapCommissionTest(){
        ContactTriggerFlag.isContactBatchRunning = true;
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'test@gmail.com';
        con.PAP_refid__c = 'test';
        insert con;
        Contact con1 = new Contact();
        con1.LastName = 'test';
        con1.Email = 'test@gmail.com';
        insert con1;
        
         
        opportunity op  = new opportunity();
        op.wc_order_id__c = '1706';
        op.WC_Product_Id__c = '506';
        op.name='opclub';
        op.Contact__c =con.id;
       	op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CustomerID__c = 'cus_GsBRLv0RyvXmjg';
        op.Clubbed__c = true;
        
        op.CloseDate = System.today();
        insert op;
        op.Opportunity_Unique_Name__c = op.id;
        update op;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345',
                                     Commission_Enabled_Club_Product__c=true);
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Opportunity_Unique_Name__c = op.Id;
        
        insert ol;
        
        Pap_Commission__c pap1 = new Pap_Commission__c();
        pap1.order_id__c = '1706';
        pap1.product_id__c = '506';
        pap1.commission__c = 1.90;
        pap1.affiliate_ref_id__c = 'test';
        pap1.Campaign_name__c = 'test';
        pap1.Contact__c = con1.id;
        pap1.Opportunity__c = op.Id;
        pap1.data5__c = '1706';
        Test.startTest();
        insert pap1;
        Test.stopTest();
    }
    @isTest
    public static void PapCommissionTest1(){
        Contact con = new Contact();
        con.LastName = 'test';
        con.Email = 'test@gmail.com';
        con.PAP_refid__c = 'test';
        insert con;
        Contact con1 = new Contact();
        con1.LastName = 'test';
        con1.Email = 'test@gmail.com';
        insert con1;
        
         
        opportunity op  = new opportunity();
        op.wc_order_id__c = '1706';
        op.WC_Product_Id__c = '506';
        op.name='opclub';
        op.Contact__c =con.id;
       	op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CustomerID__c = 'cus_GsBRLv0RyvXmjg';
        op.Clubbed__c = true;
        op.WC_Order_Number__c = '1706';
        op.CloseDate = System.today();
        Pap_Commission__c pap = new Pap_Commission__c();
        pap.order_id__c = '1706';
        pap.product_id__c = '506';
        pap.commission__c = 1.90;
        pap.affiliate_ref_id__c = 'test';
        pap.Campaign_name__c = 'test';
        pap.Contact__c = con1.id;
        pap.data5__c = '1706';
        insert pap;
        Test.startTest();
        
        insert op;
        Test.stopTest();
    }
}