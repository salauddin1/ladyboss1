@isTest
public class StripeCard_PostUpdateBatch_Test {
static testMethod void testMethod1() 
    {
       Stripe_Profile__c sp = new Stripe_Profile__c();
       sp.Stripe_Customer_Id__c = '12344';
       insert sp;
       
       Card__c cd =new Card__c();
       cd.customerId__c  = '12344';
       insert cd;
        
        Test.startTest();

            StripeCard_PostUpdateBatch  obj = new StripeCard_PostUpdateBatch  ();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}