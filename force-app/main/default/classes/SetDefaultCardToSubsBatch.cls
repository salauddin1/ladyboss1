global class SetDefaultCardToSubsBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful{
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT id,Opportunity.Card__r.Stripe_Card_Id__c,Opportunity.Card__r.Is_Customer_Default_Card__c,Subscription_id__c,OpportunityId FROM OpportunityLineItem where Subscription_id__c like \'sub%\' AND Status__c = \'Active\' AND Opportunity.Card__r.Stripe_Card_Id__c != null AND Opportunity.Default_And_Subscription_Card_Not_Match__c != true AND Opportunity.Default_Card_Set_using_batch__c != true Limit 2000');
    }
    
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
        String recordId = '';
        try{
            Map<ID, String> OppIdMap = new Map<ID, String>();
            for (OpportunityLineItem oli : scope) {
                if (oli.Opportunity.Card__r.Is_Customer_Default_Card__c) {
                    HttpRequest http = new HttpRequest();
                    recordId = oli.Opportunity.Card__r.Stripe_Card_Id__c;
                    http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oli.Subscription_Id__c);
                    http.setMethod('POST');
                    Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                    http.setHeader('Authorization', authorizationHeader);
                    http.setHeader('content-type', 'application/x-www-form-urlencoded');
                    http.setBody('default_payment_method='+oli.Opportunity.Card__r.Stripe_Card_Id__c);
                    Http con = new Http();
                    HttpResponse hs = new HttpResponse();
                    if (!Test.isRunningTest()) {
                        hs = con.send(http);
                    }
                    OppIdMap.put(oli.OpportunityId, 'Default Card Set'); 
                }else {
                    OppIdMap.put(oli.OpportunityId, 'Default Card Not Set');   
                }
                
            }
            List<Opportunity> oppList = [SELECT id,Default_And_Subscription_Card_Not_Match__c,Default_Card_Set_using_batch__c FROM Opportunity WHERE ID IN : OppIdMap.keySet()];
            if (oppList.size()>0) {
                for (Opportunity opp: oppList) {
                    if (OppIdMap.get(opp.Id) == 'Default Card Not Set') {
                        opp.Default_And_Subscription_Card_Not_Match__c = true;
                    }else if (OppIdMap.get(opp.Id) == 'Default Card Set') {
                        opp.Default_Card_Set_using_batch__c = true;
                    }
                }
                update oppList;
            }
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
        }catch (Exception e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    ApexDebugLog apex=new ApexDebugLog();
                    apex.createLog(
                        new ApexDebugLog.Error(
                            'SetDefaultCardToSubsBatch',
                            'Batch',
                            recordId,
                            e
                        )
                    );
                }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    
}