global class NMIScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
      database.executebatch(new CustomerNMIBatch(true));
   }
}