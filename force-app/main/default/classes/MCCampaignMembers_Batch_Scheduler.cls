global class MCCampaignMembers_Batch_Scheduler implements Schedulable {
     global void execute(SchedulableContext ctx){
      MCCampaignMembers_Batch batch = new MCCampaignMembers_Batch();
      Database.executebatch(batch,20);
    }

}