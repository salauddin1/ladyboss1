public class CurrentUserSMSCommunication {
    @AuraEnabled
    public Static List<User> getNumberspic() {
        //===========load all User on the page load===========
        List<User> lst = new List<User> ();
        MasterViewSetting__c profdata = [select UserProfiles__c from MasterViewSetting__c limit 1];
        if(profdata != null) {
        List<String> proflist = profdata.UserProfiles__c.split(',');
        System.debug('======='+proflist+'========'+profdata.UserProfiles__c);
        Set<String> profSet = new Set<String>();
        for (String prf : proflist) {
            profSet.add(prf);
        }
        lst = [SELECT Id, name  FROM User where Profile.Name IN : profSet AND isActive = true order by firstname]; 
        System.debug('============='+lst);
        }
        return lst;
    }
    @AuraEnabled
    public Static List<Event> loadSMSCOntacts(String UserName) {
        Map<String, Event> filtweredMap = new  Map<String, Event> ();
        List<Event> filteredSMS = new List<Event>();
        List<Event> lst;
        //===========load all conversation on the page load and filter it by user agent===========
        if (UserName == 'All'){
            lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c,Sent_By__r.Name, Sent_By__c, Description,Id FROM Event];
        }
        else {
            lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c,Sent_By__r.Name, Sent_By__c, Description,Id FROM Event where Sent_By__c =: UserName]; 
        }
        String phnum;
        for (Event su : lst) {
            phnum = su.Phone__c;
            if(phnum != null) {
                phnum = phnum.trim();
                phnum = phnum.replace('(', '');
                phnum = phnum.replace(')', '');
                phnum = phnum.replace('-', '');
                if(phnum.contains('+1')) {
                    phnum = phnum.replace('+1', '');
                }
                
                if(phnum.length()== 11) {
                    phnum = phnum.removeStart('1');
                    
                }
                phnum = phnum.trim();
                phnum = phnum.replace(' ', '');
                filtweredMap.put(phnum,su);
                System.debug ('===========mapvalues========='+filtweredMap);
            }
        }
        
        //==============return list of all conversation==========
        return filtweredMap.values();
    }
    @AuraEnabled
    public Static List<Event> SMSCommunication(String phnumber, String fromdated, String toDated) {
        Date fromDate1 = Date.valueOf(fromdated);
        Date toDate1 = Date.valueOf(toDated);
        toDate1 = toDate1.addDays(1);
        String phnumform2 = '';
        phnumber = phnumber.trim();
        phnumber = phnumber.replace('(', '');
        phnumber = phnumber.replace(')', '');
        phnumber = phnumber.replace('-', '');
        phnumber = phnumber.replace(' ', '');
        if(phnumber.length()== 11) {
            phnumber = phnumber.removeStart('1');
        }
        if(phnumber.length() == 12 && phnumber.contains('+1')){
            phnumform2 = phnumber.replace('+1', '');
        }
        if(phnumber.length() == 10 && !phnumber.contains('+1')){
            phnumform2 = '+1'+phnumber;
        }
        //====================filter conversation by phone number, from Date, to toDate on masterView=================
        List<Event> lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Phone__c, Sent_By__c,Sent_By__r.Name, contact__r.Name, Description, Id FROM Event where (Phone__c =: phnumber OR Phone__c =: phnumform2) AND createdDate >=: fromdate1 AND createdDate <=: toDate1 Order By Phone__c Desc];
        System.debug('============='+lst);
        return lst;
    }
    @AuraEnabled
    public Static List<Event> sortByUser( String UserName, String fromdated, String toDated) {
        Date fromDate1 = Date.valueOf(fromdated);
        Date toDate1 = Date.valueOf(toDated);
        toDate1 = toDate1.addDays(1);
        //====================filter conversation by  User Agent, from Date, to toDate on masterView=================
        List<Event> lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Phone__c, Sent_By__c,Sent_By__r.Name, Description, Id FROM Event where Sent_By__c=:Username AND createdDate >=: fromdate1 AND createdDate <=: toDate1];
        return lst;
    }
    @AuraEnabled
    public Static List<Event> getNumbers(String searchText) {
        Map<String, Event> filtweredMap = new  Map<String, Event> ();
        List<Event> filteredSMS = new List<Event>();
        String phnum = '';
        // =============Search conversation by on input from search bar and return list of filtered list of conversation===============
        List<Event> lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c,Sent_By__r.Name, Sent_By__c, Description,Id FROM Event where (Phone__c LIKE: '%'+searchText+'%' OR contact__r.Name LIKE: '%'+searchText+'%')];
        for (Event su : lst) {
            phnum = su.Phone__c;
            if(phnum != null) {
                phnum = phnum.trim();
                phnum = phnum.replace('(', '');
                phnum = phnum.replace(')', '');
                phnum = phnum.replace('-', '');
                if(phnum.contains('+1')) {
                    phnum = phnum.replace('+1', '');
                }
                
                if(phnum.length()== 11) {
                    phnum = phnum.removeStart('1');
                    
                }
                phnum = phnum.trim();
                phnum = phnum.replace(' ', '');
                filtweredMap.put(phnum,su);
                System.debug ('===========mapvalues========='+filtweredMap);
            }
        }
        return filtweredMap.values();
    }
    @AuraEnabled
    public Static List<Event> ShowAllOnload() {
        Map<String, Event> filtweredMap = new  Map<String, Event> ();
        List<Event> filteredSMS = new List<Event>();
        // =============Show all conversation on load of page===============
        List<Event> lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c,Sent_By__r.Name, Sent_By__c, Description,Id FROM Event where Phone__c != null order by phone__c DESC];
        for (Event su : lst) {
            filtweredMap.put(su.Phone__c,su);
        }
        return filtweredMap.values();
    }
    @AuraEnabled
    public Static List<Event> allUserSms(String fromdated, String toDated, String UserName) {
        Date fromDate1 = Date.valueOf(fromdated);
        Date toDate1 = Date.valueOf(toDated);
        toDate1 = toDate1.addDays(1);
        List<Event> lst = new List<Event>();
        // ============return all conversation creteria filtered on from date to to-date on change of date===================
        if(UserName == 'All' || UserName == 'undefined' || UserName == null) {
        	lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c, Sent_By__c,Sent_By__r.Name, Description, Id FROM Event where createdDate >=: fromdate1 AND createdDate <=: toDate1];
        }
        else {
           	lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Contact__r.Name, Phone__c, Sent_By__c,Sent_By__r.Name, Description, Id FROM Event where Sent_By__c =: UserName AND createdDate >=: fromdate1 AND createdDate <=: toDate1];
        }
            return lst;
    }
    @AuraEnabled
    public Static List<Event> sortByDateUserNum(String UserName, String phnumber, String fromdated, String toDated) {
        Date fromDate1 = Date.valueOf(fromdated);
        Date toDate1 = Date.valueOf(toDated);
        toDate1 = toDate1.addDays(1);
        String phnumform2 = '';
        phnumber = phnumber.trim();
        phnumber = phnumber.replace('(', '');
        phnumber = phnumber.replace(')', '');
        phnumber = phnumber.replace('-', '');
        phnumber = phnumber.replace(' ', '');
        if(phnumber.length()== 11) {
            phnumber = phnumber.removeStart('1');
        }
        if(phnumber.length() == 12 && phnumber.contains('+1')){
            phnumform2 = phnumber.replace('+1', '');
        }
        if(phnumber.length() == 10 && !phnumber.contains('+1')){
            phnumform2 = '+1'+phnumber;
        }
        //return all conversation creteria filtered on from date to to-date on change of date and by onchange of phone number and User Agent selection===================
        
        List<Event> lst = [SELECT SMS_Initiated_By__c,createdDate, From_Phone_Number__c, Phone__c, Sent_By__c,Sent_By__r.Name, Description, Id FROM Event where Sent_By__c=:Username AND (Phone__c =:phnumber OR Phone__c =: phnumform2) AND createdDate >=: fromdate1 AND createdDate <=: toDate1 Order By Phone__c Desc];
        return lst;
    }
}