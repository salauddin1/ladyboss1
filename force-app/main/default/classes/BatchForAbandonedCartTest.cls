@isTest
public class BatchForAbandonedCartTest {
    @IsTest
    static void methodName(){
        Contact con = new Contact(LastName='test',Wordpress_Contact_Source__c = 'LEAN');
        insert con;
        Test.startTest();
        
        String CRON_EXP = '0 5 * * * ?'; 
        BatchForAbandonedCart sch = new BatchForAbandonedCart(); 
        system.schedule('Example Batch Schedule job', CRON_EXP, sch);
        Test.stopTest();
        
    }
    @IsTest
    static void methodName1(){
        Contact con = new Contact(LastName='test',Wordpress_Contact_Source__c = 'LEAN',Email='tes@tes.com');
        insert con;
        Abandoned_Cart_Contact__c ac = new Abandoned_Cart_Contact__c();
        ac.Contact__c = con.id;
        insert ac;
        Test.startTest();
        
        String CRON_EXP = '0 5 * * * ?'; 
        BatchForAbandonedCart batch = new BatchForAbandonedCart();
        Database.executeBatch(batch,1);
        Test.stopTest();
        
    }
}