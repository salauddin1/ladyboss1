@isTest
private class OmniCaseStatusTriggerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        User user1 = new User();
        user1.id = UserInfo.getUserId();
        user1.Reopen_queue__c = 'Krystal Frick Omni';
        update user1;
        Case c = new Case();
        c.Subject = 'Om Test';  
       	c.Routed_From__c = user1.id;
        c.Status ='Closed';
        c.Priority = 'Medium';
        c.Origin = 'Email';
        insert c;

        //Insert emailmessage for case
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = True;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = c.Id; 
        insert email;
    }
}