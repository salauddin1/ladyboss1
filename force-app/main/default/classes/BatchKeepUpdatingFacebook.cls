global class BatchKeepUpdatingFacebook implements Database.Batchable<sObject>, Database.AllowsCallouts ,Schedulable 
{     
    
    global void execute(SchedulableContext ctx) {
        BatchKeepUpdatingFacebook br = new BatchKeepUpdatingFacebook();
        Database.executeBatch(br,10);
    }

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT Id,Page_Id__c,Page_Token__c,IsActive__c FROM FacebookPage__c where IsActive__c = true';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
        for ( Sobject a : scope)
        {
          FacebookPage__c fp = (FacebookPage__c)a;
            HttpRequest req = new HttpRequest();
          HttpResponse res = new HttpResponse();
          Http http = new Http();
          // Set values to Params

          String endpoint = 'https://graph.facebook.com/'+fp.Page_Id__c+'/subscribed_apps?access_token='+fp.Page_Token__c;

          req.setEndpoint(endpoint);
          req.setMethod('POST');
          if (!Test.isRunningTest()) {      
            res = http.send(req);
            String sJson = res.getBody();
            System.debug('Str:' + res.getBody());
          }             
         
         }
        //update scope;
    }  
    
    global void finish(Database.BatchableContext BC){
        
    }
}