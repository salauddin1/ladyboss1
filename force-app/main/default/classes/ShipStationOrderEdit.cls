public class ShipStationOrderEdit {
    @AuraEnabled
    public static List<ShipStation_Orders__c> getShipOrder (String shipId){
        List<ShipStation_Orders__c> shipList = new List<ShipStation_Orders__c>();
        
        // shipList = [select id,Name,orderId__c,orderStatus__c,customerUsername__c,modifyDate__c,OwnerId,Contact__c,Shipping_Address__c,Ship_To_State__c,Ship_To_City__c,Ship_To_Country__c,Ship_To_Phone__c,Ship_To_Residential__c,Ship_Street1__c,ShipTo_Street2__c,Ship_To_Street3__c,Store_Id__c from ShipStation_Orders__c where Id =: shipId  LIMIT 1 ]; 
        shipList = [select id,Name,name__c,orderKey__c,Order_Number__c,Ship_To_AddressVerified__c,OwnerId,orderStatus__c,phone__c,state__c,city__c,street1__c,Order_notes__c,Shipping_Address__c,Ship_To_State__c,Ship_To_City__c,Ship_To_Country__c,Ship_To_Phone__c,Ship_To_Residential__c,Ship_Street1__c,ShipTo_Street2__c,Ship_To_Street3__c,Store_Id__c ,Ship_To_PostalCode__c,Shipname__c,orderId__c,customerUsername__c,modifyDate__c,Contact__c,customerEmail__c,(select id,ShipStation_Orders__c,name__c,sku__c,quantity__c from ShipStation_Order_Items__r) from ShipStation_Orders__c where Id =: shipId LIMIT 1 ]; 
        
        return shipList;
    }
    @AuraEnabled
    public static String updateShipOrder1(ShipStation_Orders__c orderIds){
        String header;
        String errorMsg;
        System.debug('========orderIds========='+orderIds.Id);
        List<ShipStation_Order_Items__c> orderLineItem = [select Id,orderItemId__c,name__c,quantity__c,sku__c,unitPrice__c,ShipStation_Orders__c from ShipStation_Order_Items__c where ShipStation_Orders__c =:orderIds.Id];
        ShipStation__c ShipStationUser=[select Domain_Name__c,userName__c, password__c from ShipStation__c where isLive__c=true limit 1];
        if(ShipStationUser != null){
            header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
        }
        updateorder ordUpdate = new updateorder();
        ordUpdate.shipTo = new shipInformation();
        ordUpdate.billTo = new BillingInfo();
        if(orderIds != null){
            ordUpdate.orderNumber=orderIds.orderNumber__c;
            ordUpdate.orderKey=orderIds.orderKey__c;
            String OrdDate =String.valueOf(DateTime.now());
            OrdDate = OrdDate.replace(' ', 'T');
            ordUpdate.orderDate=OrdDate;
            ordUpdate.paymentDate=OrdDate;
            ordUpdate.orderStatus=orderIds.orderStatus__c;
            ordUpdate.orderStatus=orderIds.orderStatus__c;
            ordUpdate.customerId=orderIds.customerId__c;
            ordUpdate.customerUsername=orderIds.customerUsername__c;
            ordUpdate.internalNotes = orderIds.Order_notes__c;
            ordUpdate.customerEmail=orderIds.customerEmail__c;
            ordUpdate.shipTo.name = orderIds.Shipname__c;
            ordUpdate.shipTo.street1 = orderIds.Ship_Street1__c;
            ordUpdate.shipTo.city = orderIds.Ship_To_City__c;
            ordUpdate.shipTo.state = orderIds.Ship_To_State__c;
            ordUpdate.shipTo.country = 'US';
            ordUpdate.shipTo.postalCode = orderIds.Ship_To_PostalCode__c;
            ordUpdate.shipTo.company = orderIds.Ship_Company__c;
            ordUpdate.shipTo.phone = orderIds.Ship_To_Phone__c;
            ordUpdate.billTo.name = orderIds.Shipname__c;
            ordUpdate.billTo.street1 = orderIds.Ship_Street1__c;
            ordUpdate.billTo.city = orderIds.Ship_To_City__c;
            ordUpdate.billTo.state = orderIds.Ship_To_State__c;
            ordUpdate.billTo.country = 'US';
            ordUpdate.billTo.postalCode = orderIds.Ship_To_PostalCode__c;
            ordUpdate.billTo.company = orderIds.Ship_Company__c;
            ordUpdate.billTo.phone = orderIds.Ship_To_Phone__c;  
            ordUpdate.items = new List<LineItems>();
                LineItems orditm = new LineItems();
                for(ShipStation_Order_Items__c itm: orderLineItem) {
                    orditm.quantity = Integer.valueOf(itm.quantity__c);
                    orditm.name = itm.name__c;
                    orditm.sku = itm.sku__c;
                    orditm.unitPrice = itm.unitPrice__c;
                    ordUpdate.items.add(orditm);
                }
        }
        String orderBody=JSON.serialize(ordUpdate);
        System.debug('=====orderBody===='+orderBody);
        try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders/createorder');
            req.setMethod('POST');
            req.setHeader('Authorization', 'Basic '+header);
            req.setHeader('Content-Type', 'application/json');
            req.setBody(orderBody);
            HttpResponse res = new Http().send(req);
            String ResponseBody = res.getBody();
            System.debug('=====ResponseBody===='+ResponseBody);
            Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(ResponseBody);
            if(String.valueOf(response.get('orderStatus')) != 'cancelled' && String.valueOf(response.get('orderStatus')) != 'shipped') {
            Map<String, Object> shippingAddress = (Map<String,Object>)response.get('shipTo');
            if(response.containsKey('shipTo') && String.valueOf(shippingAddress.get('addressVerified')) == 'Address validated successfully') {
                orderIds.Ship_To_AddressVerified__c = String.valueOf(shippingAddress.get('addressVerified'));
                orderIds.addressVerified__c = String.valueOf(shippingAddress.get('addressVerified'));
                ShipstationOrderStatusShippedBatch.flag = true;
                update orderIds;
            }
            else {
                errorMsg = 'Please Correct Your Address of shipping : '+String.valueOf(shippingAddress.get('addressVerified'));
            }
            }
            else{
                ShipStation_Orders__c updtstatus = new ShipStation_Orders__c();
                updtstatus.Id = orderIds.Id;
                updtstatus.orderStatus__c = String.valueOf(response.get('orderStatus'));
               errorMsg = 'You can\'t modify shipped/cancelled order\'s, it is locked from modification through the shipstation!';
                update updtstatus;
            }
        }
        catch(Exception e){
            ApexDebugLog apex=new ApexDebugLog();
                                apex.createLog(
                                new ApexDebugLog.Error(
                                 'ShipstationOrderUpdate',
                                 'UpdateshipStationOrder',
                                    orderIds.Id,
                                     e
                                 ));
        }
        return errorMsg;
    }
    
    public class Updateorder{
        public String orderNumber;
        public String orderKey;
        public String orderDate;
        public String paymentDate;
        public String orderStatus;
        public String customerId;
        public String customerUsername;
        public String customerEmail;
        public String internalNotes;
        public BillingInfo billTo;
        public shipInformation shipTo;
        public List<LineItems> items;
    }
    public class BillingInfo {
        public String name;
        public String company;
        public String street1;
        public String street2;
        public String street3;
        public String city;
        public String state;
        public String country;
        public String postalCode;
        public Boolean residential;
        public String phone;
    }
    public class shipInformation {
        public String name;
        public String company;
        public String street1;
        public String street2;
        public String street3;
        public String city;
        public String state;
        public String country;
        public String postalCode;
        public Boolean residential;
        public String phone;
    }
    public class LineItems {
        public String sku;
        public String name;
        public Integer quantity;
        public Decimal unitPrice;   
    }
    
}