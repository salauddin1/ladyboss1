// Developer Name :- Surendra Patidar
// class contains code about create task whenever lead spent more then 24 Hours and 72 hours with organization
// =================================================================================================================
public without sharing class AutometicTaskBatchUponLeadRec implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful, Schedulable  {
    public Database.QueryLocator start(Database.BatchableContext BC) {
       //Query lead records basis of hours and hours flags
        return Database.getQueryLocator('select id, name, Age__c, Hours__c, Email, Status, MobilePhone,Completed24Hours__c,Completed_72_Hours__c,Completed_1_Week__c, Completed_1_Months_And_Above__c from Lead where Hours__c >= 24 AND Age__c < 7 AND (Completed24Hours__c = false OR Completed_72_Hours__c = false OR Completed_1_Week__c = false OR Completed_1_Months_And_Above__c = false)');
    }
    public void execute(SchedulableContext SC) {
    Database.executeBatch(new AutometicTaskBatchUponLeadRec());
   }
    public void execute(Database.BatchableContext BC, List<Lead> scope) {
        System.debug('=========='+scope.size()+'=========='+scope);
        List<task> tskList = new List<task>();
        // extraxts lead records and check for hours between more then or equal 24 hours and 72 hours
        if (scope != null && scope.size()>0 && !scope.isEmpty()) {
            for (lead leadObj : scope) {
                System.debug('==========='+Integer.valueOf(leadObj.Hours__c));
                if(Integer.valueOf(leadObj.Hours__c) > = 24 && Integer.valueOf(leadObj.Hours__c) <72 && leadObj.Completed24Hours__c == false) {
                    task tsk = new task ();
                    leadObj.Completed24Hours__c = true;
                    tsk.whoId = leadObj.Id;
                    tsk.Priority = 'Normal';
                    tsk.status = 'Open';
                    tsk.Subject = leadObj.Name+' spent 24 hours with ladyboss';
                    tsk.Email__c = leadObj.Email;
                    tskList.add(tsk);
                }
                if(Integer.valueOf(leadObj.Hours__c) > = 72 && leadObj.Age__c < 7 && leadObj.Completed_72_Hours__c == false) {
                    leadObj.Completed24Hours__c = true;
                    leadObj.Completed_72_Hours__c = true;
                    task tsk1 = new task ();
                    tsk1.whoId = leadObj.Id;
                    tsk1.Priority = 'Normal';
                    tsk1.status = 'Open';
                    tsk1.Subject = leadObj.Name+' spent 72 hours with ladyboss';
                    tsk1.Email__c = leadObj.Email;
                    tskList.add(tsk1);
                }
        }
            System.debug('========='+tskList.size()+'========'+tskList);
            try {
                if(tskList.size()>0) {
                    insert tskList;
                }
                if(scope.size()>0) {
                    update scope;
                }
            }
            catch (exception e) {}
            }
    }
    public void finish(Database.BatchableContext BC) {
        
    }
}