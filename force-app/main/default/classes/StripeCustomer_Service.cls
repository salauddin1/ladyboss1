@RestResource(urlMapping='/Stripe_CustomerService')
global class StripeCustomer_Service {
    @HttpPost
    global static void createCustomer() {
        String timestamppp = RestContext.request.Headers.get('Stripe-Signature');
        Boolean sigFromHmac = HMAC.generateSignature(timestamppp, 'Stripe_CustomerService');
        System.debug('-------timestamppp----'+timestamppp);
        System.debug('-------sigFromHmac----'+sigFromHmac);
                             
        if(sigFromHmac && sigFromHmac == true || Test.isRunningTest()){
        Stripe_webservice_setting__c op = [select id,StripeCard_Service__c,StripeCardDelete_Service__c,StripeCustomer_Service__c from Stripe_webservice_setting__c];
        string CustomerId;
        if(RestContext.request.requestBody!=null &&op.StripeCustomer_Service__c ){
            try {
                
                
                String str = RestContext.request.requestBody.toString();
                System.debug(str);
                Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(str );
                
                Map<String, Object> data = new map<String, Object>();
                Map<String, Object> sourcesMap = new map<String, Object>();
                List<String> fieldNames = new List<String>();
                Map<String, Object> account = (Map<String, Object>)results.get('data');
                Map<String,Object> strList =(Map<String,Object>)account.get('object');
                CustomerId =string.Valueof(strlist.get('id'));
                sourcesMap = (Map<String,Object>)strList.get('sources');
                List<Object> sourceDataMap = new List<Object>();
                sourceDataMap = (List<Object>)sourcesMap.get('data');
                System.debug('====sourceDataMap ====='+sourceDataMap );
                Decimal balance = Decimal.valueOf(String.valueOf(strList.get('balance')));
                
                Map<String,Object> strMetadata = (Map<String,Object>)strList.get('metadata');
                String conEmail = string.Valueof(strlist.get('email'));
                List<Contact> cnList =[select id, email from contact where email = : conEmail];
                List<Stripe_Profile__c > spList = new List<Stripe_Profile__c >();
                System.debug('----------strMetadata -------'+strMetadata );
                //if ( strMetadata !=null) {!strMetadata.ContainsKey('Integration Initiated From')
                List<Opportunity> lstOppUpdate =  new  List<Opportunity>();
                if(!strMetadata.ContainsKey('Integration Initiated From') || sourceDataMap.isEmpty()) {
                    // if(strMetadata.isEmpty() && sourceDataMap.isEmpty()) {
                    System.debug('----------strMetadata -------'+strMetadata );
                    List<Opportunity> lstOpp = [select id,CustomerID__c,Contact__c,Stripe_Profile__c from opportunity where (Contact__c=null or Stripe_Profile__c =null) and CustomerID__c!=null and CustomerID__c=:CustomerId];
                    String custId='';
                    
                    if (cnList.size() > 0) {
                        List<Stripe_Profile__c> lstProf = [select id from Stripe_Profile__c where Stripe_Customer_Id__c =: string.Valueof(strlist.get('id'))];
                        
                        //if profile not exists and contact already exists
                        if(lstProf.size() <= 0) {
                            Stripe_Profile__c sp = new Stripe_Profile__c();
                            sp.Stripe_Customer_Id__c = string.Valueof(strlist.get('id'));
                            
                            sp.Customer__c = cnList[0].id;
                            insert sp;
                            custId = sp.id;
                            
                            
                        }
                        else
                        {    //if profile exists and contact already exists
                            lstProf[0].Stripe_Customer_Id__c = string.Valueof(strlist.get('id'));
                            lstProf[0].Customer__c = cnList[0].id;
                            update lstProf;
                            custId = lstProf[0].id;
                            
                        }
                        if(lstOpp.size() > 0){
                            for(Opportunity ops : lstOpp) {
                                if(ops.Stripe_Profile__c ==null){
                                    ops.Stripe_Profile__c = custId;
                                }
                                if(ops.Contact__c==null){
                                    ops.Contact__c = cnList[0].id;
                                }
                                
                            }    
                            
                        }
                        
                    }
                    else
                    {
                        //if  contact Not exists create new contact and profile
                        Contact con = new Contact ();
                        List<Contact> lstContact = new List<Contact>();
                        String mail = String.Valueof(strlist.get('email'));
                        Integer[] chars = mail.getChars();
                        boolean emailContainsUpperCase=false;
                        System.debug(emailContainsUpperCase);
                        for(Integer ch : chars){
                            if(ch >= 65 && ch <= 90){
                                emailContainsUpperCase = true;
                                break;
                            }
                        }
                        if(emailContainsUpperCase){
                            updateCustomer(CustomerId,mail.toLowerCase());
                        }
                        con.email = string.Valueof(strlist.get('email'));
                        con.lastName = string.Valueof(strlist.get('description'));
                        
                        if(strMetadata !=null){
                            con.phone= String.Valueof(strMetadata.get('phone'));
                            if(String.Valueof(strMetadata.get('last_name'))!=null)
                                con.lastName = String.Valueof(strMetadata.get('last_name'));
                            if(String.Valueof(strMetadata.get('first_name'))!=null)
                                con.firstName=String.Valueof(strMetadata.get('first_name'));
                            
                            //lastName = tring.Valueof(strMetadata.get('first_Name'));
                            
                        }
                        else
                        {
                            con.lastName = string.Valueof(strlist.get('description'));
                            
                        }
                        con.Stripe_Customer_Id__c= string.Valueof(strlist.get('id'));
                        if(con!= null && con.lastName!=null)
                            insert con;
                        
                        //List<Stripe_Profile__c> lstProf = [select id from Stripe_Profile__c where Stripe_Customer_Id__c =: string.Valueof(strlist.get('id'))];
                        Stripe_Profile__c sp = new Stripe_Profile__c();
                        sp.Stripe_Customer_Id__c = string.Valueof(strlist.get('id'));
                        
                        sp.Customer__c = con.id;
                        if(sp!=null)
                            insert sp;
                        if(lstOpp.size() > 0){
                            for(Opportunity ops: lstOpp) {
                                if(ops.Stripe_Profile__c==null ){
                                    ops.Stripe_Profile__c = sp.id;
                                }
                                if(ops.Contact__c==null){
                                    ops.Contact__c =  con.id;
                                }
                                
                            }
                        }
                        
                        //}
                        
                        
                    }
                    if(lstOpp.size() > 0) {
                        update lstOpp;
                    }
                    
                    
                }
                if (balance != null) {
                    List<Stripe_Profile__c> sp = [SELECT id,Balance__c FROM Stripe_Profile__c WHERE Stripe_Customer_id__c = :CustomerId];
                    sp[0].Balance__c = -balance*0.01;
                    update sp;
                }
                // }
                ApexDebugLog apex=new ApexDebugLog();
                apex.deleteLog(
                    new ApexDebugLog.Deletelogs(
                        'StripeCustomer_Service',
                        'createCustomer',
                        CustomerId
                    )
                ); 
                if(test.isrunningTest()){
                    Integer a=10/0;
                }
            }
            catch(Exception ex) {
                RestResponse res = RestContext.response; 
                res.statusCode = 400;
                System.debug('The following exception has occurred: ' + ex.getMessage());
                System.debug('Exception e '+ex.getLineNumber());
                ApexDebugLog apex=new ApexDebugLog();
                apex.createLog(
                    new ApexDebugLog.Error(
                        'StripeCustomer_Service',
                        'createCustomer',
                        CustomerId,
                        ex
                    )
                );
                
            }    
            
        }
        else
        {
            RestResponse res = RestContext.response; 
            res.statusCode = 400;
        }
        }
    }
    
    public static void updateCustomer(String cusId,String mail){
        String SERVICE_URL = 'https://api.stripe.com/v1/customers';
        String endPoint = SERVICE_URL+'/'+cusId;
        HttpRequest http = new HttpRequest();
        http.setEndpoint(endPoint);
        http.setMethod('POST');
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        mail = EncodingUtil.urlEncode(mail, 'UTF-8');
        http.setBody('email='+mail);
        System.debug('REQUEST BODY:\n'+http.getBody());    
        String response;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        
        if (!Test.isRunningTest()) {
            hs = con.send(http);
        } 
    }
    
}