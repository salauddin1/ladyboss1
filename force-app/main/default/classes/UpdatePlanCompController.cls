public class UpdatePlanCompController {
    @AuraEnabled
    public static String updateSubPlan(String opplId){
        system.debug('Inside Controller '+opplId);
        Map<String, Object> results = new Map<String, Object>();
        String recordId = '';
        String msg = 'Already upgraded';
        try{
            OpportunityLineItem oppitem =[SELECT Id,Subscription_Id__c,Plan_Upgraded__c,Product2.Stripe_Plan_id__C,Status__c,Subscription_Plan_ID__c,Tax_Amount__c,Tax_Percentage_s__c FROM OpportunityLineItem where ID =:opplId ];  
            if (oppitem.Subscription_Plan_ID__c!=oppitem.Product2.Stripe_Plan_Id__c) {
                HttpRequest http = new HttpRequest();
                recordId =oppitem.Subscription_Id__c;
                http.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppitem.Subscription_Id__c);
                http.setMethod('GET');
                Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
                String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
                http.setHeader('Authorization', authorizationHeader);
                Http con = new Http();
                HttpResponse hs = new HttpResponse();
                String response;
                Integer statusCode;
                If (!Test.isRunningTest()) {
                    hs = con.send(http);
                    response = hs.getBody();
                    statusCode = hs.getStatusCode();
                }else{
                    response ='{ "id": "sub_GICoQyUDJiI9Xp", "object": "subscription", "application_fee_percent": null, "billing": "charge_automatically", "billing_cycle_anchor": 1575383446, "billing_thresholds": null, "cancel_at": null, "cancel_at_period_end": false, "canceled_at": null, "collection_method": "charge_automatically", "created": 1575383446, "current_period_end": 1578061846, "current_period_start": 1575383446, "customer": "cus_GI81AUeX701Z1a", "days_until_due": null, "default_payment_method": null, "default_source": null, "default_tax_rates": [ { "id": "txr_1Flbt3FzCf73siP0K7gNGmtm", "object": "tax_rate", "active": true, "created": 1575381477, "description": null, "display_name": "Tax", "inclusive": false, "jurisdiction": null, "livemode": false, "metadata": {}, "percentage": 5.125 } ], "discount": null, "ended_at": null, "invoice_customer_balance_settings": { "consume_applied_balance_on_void": true }, "items": { "object": "list", "data": [ { "id": "si_GICoWELp5G8GzQ", "object": "subscription_item", "billing_thresholds": null, "created": 1575383448, "metadata": {}, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 11726, "amount_decimal": "11726", "billing_scheme": "per_unit", "created": 1571064931, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "LB-LEAN-3 Bags", "product": "prod_FzTuegoLBd817a", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "subscription": "sub_GICoQyUDJiI9Xp", "tax_rates": [] } ], "has_more": false, "total_count": 1, "url": "/v1/subscription_items?subscription=sub_GICoQyUDJiI9Xp" }, "latest_invoice": "in_1FlcOpFzCf73siP0Zw3tNnLA", "livemode": false, "metadata": { "Shipping Address": "PO Box 249 Albuquerque NM US 87103", "Full Name": "Test V11", "Email": "test@shiping.com", "Sales Person": "Jay Patel", "Salesforce Contact Link": "https://ladyboss--tirthbox.cs22.my.salesforce.com/0031700000wUsW3AAK", "Integration Initiated From": "Salesforce", "Street": "PO Box 249", "City": "Albuquerque", "State": "NM", "Postal Code": "87103", "Country": "US" }, "next_pending_invoice_item_invoice": null, "pending_invoice_item_interval": null, "pending_setup_intent": null, "plan": { "id": "ttmembership", "object": "plan", "active": true, "aggregate_usage": null, "amount": 11726, "amount_decimal": "11726", "billing_scheme": "per_unit", "created": 1571064931, "currency": "usd", "interval": "month", "interval_count": 1, "livemode": false, "metadata": {}, "nickname": "LB-LEAN-3 Bags", "product": "prod_FzTuegoLBd817a", "tiers": null, "tiers_mode": null, "transform_usage": null, "trial_period_days": null, "usage_type": "licensed" }, "quantity": 1, "schedule": null, "start": 1575383446, "start_date": 1575383446, "status": "active", "tax_percent": 5.125, "trial_end": null, "trial_start": null }';
                    statusCode = 200;
                }
                If (statusCode == 200) {
                    results = (Map<String, Object>)JSON.deserializeUntyped(response);                    
                    Map<String, Object>  lstCustomers = (Map<String, Object> )results.get('items');
                    List<Object> data = (List<Object>)lstCustomers.get('data');
                    Map<String, Object> obmap = (Map<String, Object>)data[0];
                    Map<String, Object> planmap =  (Map<String, Object>)results.get('plan');
                    HttpRequest http1 = new HttpRequest();
                    http1.setEndpoint('https://api.stripe.com/v1/subscriptions/'+oppitem.Subscription_Id__c);
                    http1.setMethod('POST');
                    Blob headerValuenew = Blob.valueOf(StripeAPI.ApiKey + ':');
                    String authorizationHeadernew = 'BASIC ' + EncodingUtil.base64Encode(headerValuenew);
                    http1.setHeader('Authorization', authorizationHeadernew);
                    http1.setHeader('content-type', 'application/x-www-form-urlencoded');
                    http1.setBody('items[0][id]='+String.valueOf(obmap.get('id'))+'&items[0][plan]='+oppitem.Product2.Stripe_Plan_Id__c+'&prorate=false');
                    Http con1 = new Http();
                    HttpResponse hs1 = new HttpResponse();
                    if (!Test.isRunningTest()) {
                        hs1 = con1.send(http1);
                    }
                    if (hs1.getStatusCode()==200) {
                        if (oppitem.Tax_Percentage_s__c != null && oppitem.Tax_Percentage_s__c != 0.00) {
                            Map<String, Object> results1 = (Map<String, Object>)JSON.deserializeUntyped(hs1.getBody());
                            Map<String, Object> planmap1 =  (Map<String, Object>)results1.get('plan');
                            oppitem.Tax_Amount__c = Decimal.valueOf(String.valueOf(planmap1.get('amount')))*0.01*oppitem.Tax_Percentage_s__c*0.01;
                        }
                        oppitem.Plan_Upgraded__c = true;
                        oppitem.Subscription_Plan_ID__c = oppitem.Product2.Stripe_Plan_Id__c;
                        update oppitem;
                        msg = 'Successfully Updated Plan';
                    }
                }
            }    
            
            if(Test.isRunningTest()){
                Integer i = 1/0;
            }
            return msg;
        }catch(Exception e){
            system.debug('e.getMessage()-->'+e.getMessage());
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog(
                new ApexDebugLog.Error(
                    'UpgradeSpecificPlanId',
                    'Batch',
                    recordId,
                    e
                )
            );
            return msg;
        }
    }
}