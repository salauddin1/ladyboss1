public  class CaseOmniChannelHandler {
    public static void setCaseOwner(List<Case> casesList){
        Map<String, String> ownerIdEmailMap = new Map<String, String>();
        Map<String, String> routedFromowneridMap = new Map<String, String>();
    
        for (Case cs : casesList) {
            if (cs.SuppliedEmail != null && cs.Origin == 'Email') {
                ownerIdEmailMap.put(cs.SuppliedEmail, cs.OwnerId);
            }
        }


        if (ownerIdEmailMap.size()>0) {
            List<String> emailList = new List<Id>();
            emailList.addAll(ownerIdEmailMap.keySet());
            List<Id> omniQueueId = new List<Id>();

            for (Group g : [SELECT id FROM Group WHERE id IN :ownerIdEmailMap.values() AND QueueRoutingConfigId != null]) {
                omniQueueId.add(g.Id);
            }

            if (omniQueueId.size() > 0) {
                List<Case> caseList = [Select id,Routed_From__c,SuppliedEmail,ownerid from Case WHERE Routed_From__c IN : omniQueueId AND SuppliedEmail IN : emailList AND Routed_From__c != null AND SuppliedEmail != null AND LastModifiedDate > Last_Month limit 1000];
                for (Case c : caseList) {
                    if (ownerIdEmailMap.get(c.SuppliedEmail) == c.Routed_From__c) {
                        routedFromowneridMap.put(c.Routed_From__c, c.OwnerId);
                    }
                }

            }
            if (routedFromowneridMap.size()>0) {
                Map<ID,String>  userQueueNameMap = new Map<ID,String>();
                Map<String,Id>  queueNameIdMap = new Map<String,Id>();

                For(User u: [SELECT id,Reopen_queue__c FROM USER where id in:routedFromowneridMap.values() and Reopen_queue__c != null]){
                    userQueueNameMap.put(u.Id, u.Reopen_queue__c); 
                }

                system.debug('userQueueNameMap  '+userQueueNameMap.values());

                for(Group g:[Select id,Name From Group WHERE Name in :userQueueNameMap.values() ]){
                    queueNameIdMap.put(g.Name,g.id);
                }

                for (Case ca : casesList) {
                    if (routedFromowneridMap.containsKey(ca.OwnerId)) {
                        if (userQueueNameMap.containsKey(routedFromowneridMap.get(ca.OwnerId))) {
                            ca.OwnerId =queueNameIdMap.get(userQueueNameMap.get(routedFromowneridMap.get(ca.OwnerId))); 
                        }
                    }
                }

            }
        }
    }
    public static void updateCaseOwner(List<Case> casesList,Map<Id,Case> caseOldMap){
        Map<String, String> emailOwneridMap = new Map<String, String>();
        List<Id> caseId = new List<Id>();
        Map<String, String> emailOwnerMap = new Map<String, String>();
        Map<String,String> emailRoutedFromMap = new Map<String,String>();


        for (Case cs : casesList) {
            Case csOld = caseOldMap.get(cs.Id);
            if ((csOld.Routed_From__c == null || csOld.Routed_From__c == '') && csOld.Routed_From__c != cs.Routed_From__c && cs.SuppliedEmail != null && cs.Origin == 'Email') {
                caseId.add(cs.Id);
                emailRoutedFromMap.put(cs.SuppliedEmail, cs.Routed_From__c);
                emailOwneridMap.put(cs.SuppliedEmail, cs.OwnerId);
            } 
        }

        if (caseId.size()>0) {
            List<String> emailList = new List<String>();
            emailList.addAll(emailOwneridMap.keySet());
            List<Case> caseList =  [Select id,Routed_From__c,SuppliedEmail,ownerid from Case WHERE ownerid IN : emailRoutedFromMap.values() AND SuppliedEmail IN : emailList Order By CreatedDate ASC];
            for (Case casei :caseList) {
                if (emailRoutedFromMap.get(casei.SuppliedEmail) == casei.OwnerId) {
                    emailOwnerMap.put(casei.SuppliedEmail, casei.OwnerId);
                }
            }
            
            if (emailOwnerMap.size()>0) {
                Map<ID,String>  userQueueNameMap = new Map<ID,String>();
                Map<String,Id>  queueNameIdMap = new Map<String,Id>();

                For(User u: [SELECT id,Reopen_queue__c FROM USER where id in:emailOwneridMap.values() and Reopen_queue__c != null]){
                    userQueueNameMap.put(u.Id, u.Reopen_queue__c); 
                }

                for(Group g:[Select id,Name From Group WHERE Name in :userQueueNameMap.values() ]){
                    queueNameIdMap.put(g.Name,g.id);
                }
                for (Case ca : caseList) {
                    if (emailOwnerMap.containsKey(ca.SuppliedEmail)) {
                        if (userQueueNameMap.containsKey(emailOwneridMap.get(ca.SuppliedEmail))) {
                            ca.OwnerId =queueNameIdMap.get(userQueueNameMap.get(emailOwneridMap.get(ca.SuppliedEmail))); 
                        }
                    }
                }

                if (caseList.size()>0) {
                    update caseList;
                }

            }
        }
    }
}