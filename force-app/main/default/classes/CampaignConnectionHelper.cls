/*
* Devloper : Sourabh Badole
* Description : This Helper class is using to link Opportunity to Campaign by the help of Campaign Member.
*/
public class CampaignConnectionHelper {
    public static void getCampaignConnection(Set<id> CamMemberIdSet,Set<id> conIdSet){
        try{ 
            List <CampaignMember> camMembList = new List<CampaignMember>();
            if(CamMemberIdSet != null && !CamMemberIdSet.isEmpty()){
                camMembList = [select id ,ContactId,Campaign.Name ,Campaign.type from CampaignMember where id =: CamMemberIdSet and Campaign.type =: 'Phone Team' and ContactId != null and  Campaign.Name != null  ];
                System.debug('test camMembist ===== '+camMembList);
            }
            Map<id,String> campMembMap =  new Map<id,String>();
            if(camMembList != null && !camMembList.isEmpty()){
                for(CampaignMember camp : camMembList){
                    if(camp != null){
                        if(camp.ContactId != null &&   camp.Campaign.Name != null &&   camp.Campaign.Name != ''){
                            if(campMembMap != null && !campMembMap.keySet().isEmpty() && campMembMap.keySet().contains(camp.ContactId)){
                                String campHolder = '';
                                
                                campHolder  = campMembMap.get(camp.ContactId);
                                if(campHolder != null & campHolder != ''){
                                    campHolder = campHolder+', '+ camp.Campaign.Name ;
                                    campMembMap.put(camp.ContactId , campHolder);    
                                    System.debug('test  camp.Campaign.Name 1 ===== '+ camp.Campaign.Name);
                                }
                                System.debug('test  camp.Campaign.Name 1===== '+ camp.Campaign.Name);
                                System.debug('test  camp.Campaign.Name  ContactId 1 ===== '+ camp.ContactId);
                            }
                            else{
                                campMembMap.put(camp.ContactId , camp.Campaign.Name);    
                                System.debug('test  camp.Campaign.Name ===== '+ camp.Campaign.Name);
                                System.debug('test  camp.Campaign.Name  ContactId ===== '+ camp.ContactId);
                            }
                        }
                    }
                } 
            }
            List<Opportunity> oppListVal = new List<Opportunity>();
            List<Opportunity> oppListUpdate = new List<Opportunity>();
            if(conIdSet != null && !conIdSet.isEmpty()){
                oppListVal = [select id, Contact__c, Campaign__c,createdDate from opportunity where Contact__c =: conIdSet and Contact__c != null limit 500000];
                if(oppListVal != null && !oppListVal.isEmpty()){
                    Map<id,Date> lastCreatedOppDateMap =  new Map<id,Date>();
                    for(Opportunity opp : oppListVal){
                        if(opp.Contact__c != null ){
                            if(lastCreatedOppDateMap != null && !lastCreatedOppDateMap.keySet().isEmpty() && lastCreatedOppDateMap.keySet().contains(opp.Contact__C) ){
                                if( lastCreatedOppDateMap.get(opp.Contact__c) != null){
                                    Date dt  = lastCreatedOppDateMap.get(opp.Contact__c);
                                    if(dt < opp.createdDate.date() ){
                                        lastCreatedOppDateMap.put(opp.Contact__c , opp.CreatedDate.date());   
                                        System.debug('test1  opp.CreatedDate ===== '+  opp.CreatedDate);
										System.debug('test 1 opportunity  ContactId ===== '+ opp.Contact__C);
                                    }
                                }
                            }
                            else{
                                lastCreatedOppDateMap.put(opp.Contact__c , opp.CreatedDate.date());    
                                System.debug('test  opp.CreatedDate ===== '+  opp.CreatedDate);
                                System.debug('test  opportunity  ContactId ===== '+ opp.Contact__C);
                            }
                        }
                        System.debug('test  opp. lastCreatedOppDateMap ===== '+ lastCreatedOppDateMap );
                        
                    }
                    
                    for(Opportunity opp : oppListVal){
                        if(opp.Contact__c != null ){
                            if(lastCreatedOppDateMap != null && !lastCreatedOppDateMap.keySet().isEmpty() && lastCreatedOppDateMap.keySet().contains(opp.Contact__C) ){
                                if( lastCreatedOppDateMap.get(opp.Contact__c) != null){
                                    Date dt  = lastCreatedOppDateMap.get(opp.Contact__c);
                                    if(dt == opp.createdDate.date() ){
                                        if(campMembMap != null && !campMembMap.keySet().isEmpty() && campMembMap.keySet().contains(opp.Contact__c)){
                                            if(campMembMap.get(opp.Contact__c) != null && campMembMap.get(opp.Contact__c) != ''){
                                                if(opp.Campaign__c != null && opp.Campaign__c != ''){    
                                                    if(campMembMap.get(opp.Contact__c).contains(', ')){
                                                        String campNameHolder = '';
                                                        Boolean checkOpp = false ;
                                                        campNameHolder  = campMembMap.get(opp.Contact__c);
                                                        List<String> allCamName = new List<String>();
                                                        allCamName = campNameHolder.split(', ');
                                                        if(allCamName != null && !allCamName.isEmpty()){
                                                            String oppCampVal = '' ;
                                                            oppCampVal = opp.Campaign__c ;
                                                            for(String  str : allCamName){
                                                                if(oppCampVal != null && str != null && oppCampVal != '' && str != '') { 
                                                                    if( !oppCampVal.contains(str)){
                                                                        oppCampVal = oppCampVal +', '+str;
                                                                        checkOpp = true ;
                                                                    }
                                                                }
                                                            }
                                                            opp.Campaign__c = oppCampVal;
                                                        }
                                                        if(checkOpp == true){
                                                            oppListUpdate.add(opp);    
                                                        }
                                                        
                                                    }
                                                    else{
                                                        if(!opp.Campaign__c.contains(campMembMap.get(opp.Contact__c)) ){
                                                            opp.Campaign__c = opp.Campaign__c+', '+campMembMap.get(opp.Contact__c);    
                                                            oppListUpdate.add(opp);
                                                        }
                                                    }
                                                }
                                                else{
                                                    opp.Campaign__c = campMembMap.get(opp.Contact__c);
                                                    oppListUpdate.add(opp);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                    if(oppListUpdate != null && !oppListUpdate.isEmpty()){
                        update oppListUpdate ;
                            System.debug('test  oppListUpdate ===== '+ oppListUpdate );
                    
                    }
                }
            }
        }
        catch(Exception ex){
            String addId = 'Campaign Id = > ',conAddId = 'Contact Id = >';
            if(CamMemberIdSet != null && !CamMemberIdSet.isEmpty() && conIdSet != null && !conIdSet.isEmpty()){
                for(id campId : CamMemberIdSet){ addId = addId  +'   '+campId;}
                for(id conId : conIdSet){ conAddId = conAddId  +'   '+conId;}
            }
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('getCampaignConnection','CampaignConnectionHelper',addId +'  &&&&  '+conAddId,ex));
            System.debug('***Get Exception***'+ex);
        }
       
    }
    public static void getCampOppByContactUpdateTrigger(List<CampaignMember> camMembList,Set<id> conIdSet){
        try{ 
            Map<id,String> campMembMap =  new Map<id,String>();
            if(camMembList != null && !camMembList.isEmpty()){
                for(CampaignMember camp : camMembList){
                    if(camp != null){
                        System.debug('test  camp.Campaign.Name 1==campcampcampcamp=== '+ camp);
                         System.debug('test  camp.Campaign.Name 1 ===== '+ camp.Campaign.Name);       
                        if(camp.ContactId != null &&   camp.Campaign.Name != null &&   camp.Campaign.Name != ''){
                            if(campMembMap != null && !campMembMap.keySet().isEmpty() && campMembMap.keySet().contains(camp.ContactId)){
                                
                                
                                String campHolder = '';
                                
                                campHolder  = campMembMap.get(camp.ContactId);
                                if(campHolder != null & campHolder != ''){
                                    campHolder = campHolder+', '+ camp.Campaign.Name ;
                                    campMembMap.put(camp.ContactId , campHolder);    
                                    System.debug('test  camp.Campaign.Name 1 ===== '+ camp.Campaign.Name);
                                }
                                System.debug('test  camp.Campaign.Name 1===== '+ camp.Campaign.Name);
                                System.debug('test  camp.Campaign.Name  ContactId 1 ===== '+ camp.ContactId);
                            }
                            else{
                                campMembMap.put(camp.ContactId , camp.Campaign.Name);    
                                System.debug('test  camp.Campaign.Name ===== '+ camp.Campaign.Name);
                                System.debug('test  camp.Campaign.Name  ContactId ===== '+ camp.ContactId);
                            }
                        }
                    }
                } 
            }
            List<Opportunity> oppListVal = new List<Opportunity>();
            List<Opportunity> oppListUpdate = new List<Opportunity>();
            if(conIdSet != null && !conIdSet.isEmpty()){
                oppListVal = [select id, Contact__c, Campaign__c,createdDate from opportunity where Contact__c =: conIdSet and Contact__c != null limit 500000];
                if(oppListVal != null && !oppListVal.isEmpty()){
                    Map<id,Date> lastCreatedOppDateMap =  new Map<id,Date>();
                    for(Opportunity opp : oppListVal){
                        if(opp.Contact__c != null ){
                            if(lastCreatedOppDateMap != null && !lastCreatedOppDateMap.keySet().isEmpty() && lastCreatedOppDateMap.keySet().contains(opp.Contact__C) ){
                                if( lastCreatedOppDateMap.get(opp.Contact__c) != null){
                                    Date dt  = lastCreatedOppDateMap.get(opp.Contact__c);
                                    if(dt < opp.createdDate.date() ){
                                        lastCreatedOppDateMap.put(opp.Contact__c , opp.CreatedDate.date());   
                                        System.debug('test1  opp.CreatedDate ===== '+  opp.CreatedDate);
										System.debug('test 1 opportunity  ContactId ===== '+ opp.Contact__C);
                                    }
                                }
                            }
                            else{
                                lastCreatedOppDateMap.put(opp.Contact__c , opp.CreatedDate.date());    
                                System.debug('test  opp.CreatedDate ===== '+  opp.CreatedDate);
                                System.debug('test  opportunity  ContactId ===== '+ opp.Contact__C);
                            }
                        }
                        System.debug('test  opp. lastCreatedOppDateMap ===== '+ lastCreatedOppDateMap );
                        
                    }
                    
                    for(Opportunity opp : oppListVal){
                        if(opp.Contact__c != null ){
                            if(lastCreatedOppDateMap != null && !lastCreatedOppDateMap.keySet().isEmpty() && lastCreatedOppDateMap.keySet().contains(opp.Contact__C) ){
                                if( lastCreatedOppDateMap.get(opp.Contact__c) != null){
                                    Date dt  = lastCreatedOppDateMap.get(opp.Contact__c);
                                    if(dt == opp.createdDate.date() ){
                                        if(campMembMap != null && !campMembMap.keySet().isEmpty() && campMembMap.keySet().contains(opp.Contact__c)){
                                            if(campMembMap.get(opp.Contact__c) != null && campMembMap.get(opp.Contact__c) != ''){
                                                if(opp.Campaign__c != null && opp.Campaign__c != ''){    
                                                    if(campMembMap.get(opp.Contact__c).contains(', ')){
                                                        String campNameHolder = '';
                                                        Boolean checkOpp = false ;
                                                        campNameHolder  = campMembMap.get(opp.Contact__c);
                                                        List<String> allCamName = new List<String>();
                                                        allCamName = campNameHolder.split(', ');
                                                        if(allCamName != null && !allCamName.isEmpty()){
                                                            String oppCampVal = '' ;
                                                            oppCampVal = opp.Campaign__c ;
                                                            for(String  str : allCamName){
                                                                if(oppCampVal != null && str != null && oppCampVal != '' && str != '') { 
                                                                    if( !oppCampVal.contains(str)){
                                                                        oppCampVal = oppCampVal +', '+str;
                                                                        checkOpp = true ;
                                                                    }
                                                                }
                                                            }
                                                            opp.Campaign__c = oppCampVal;
                                                        }
                                                        if(checkOpp == true){
                                                            oppListUpdate.add(opp);    
                                                        }
                                                        
                                                    }
                                                    else{
                                                        if(!opp.Campaign__c.contains(campMembMap.get(opp.Contact__c)) ){
                                                            opp.Campaign__c = opp.Campaign__c+', '+campMembMap.get(opp.Contact__c);    
                                                            oppListUpdate.add(opp);
                                                        }
                                                    }
                                                }
                                                else{
                                                    opp.Campaign__c = campMembMap.get(opp.Contact__c);
                                                    oppListUpdate.add(opp);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                    if(oppListUpdate != null && !oppListUpdate.isEmpty()){
                        update oppListUpdate ;
                            System.debug('test  oppListUpdate ===== '+ oppListUpdate );
                    
                    }
                }
            }
        }
        catch(Exception ex){
            String addId = 'Campaign Id = > ',conAddId = 'Contact Id = >';
            if(CamMembList != null && !CamMembList.isEmpty() && conIdSet != null && !conIdSet.isEmpty()){
                for(CampaignMember campId : CamMembList){ addId = addId  +'   '+campId.id;}
                for(id conId : conIdSet){ conAddId = conAddId  +'   '+conId;}
            }
            ApexDebugLog apex=new ApexDebugLog();
            apex.createLog( new ApexDebugLog.Error('getCampOppByContactUpdateTrigger','CampaignConnectionHelper',addId +'  &&&&  '+conAddId,ex));
            System.debug('***Get Exception***'+ex);
        }
    }
}