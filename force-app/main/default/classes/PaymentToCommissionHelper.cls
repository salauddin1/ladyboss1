public class PaymentToCommissionHelper {
    public static Map<Decimal,Comission_Configuration__c> getTotalCommissionConfiguration(){
        Map<Decimal,Comission_Configuration__c> comiconfig = new Map<Decimal,Comission_Configuration__c>();
        String dQuery = 'SELECT Max_Value__c,Min_Value__c,'+
            'Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,'+
            'Percentage_Paid_to_Vice_President__c,Percentage_Paid_to_Senior_Agent__c '+
            'FROM Comission_Configuration__c where product__c=null AND For_First_Sell__c=false order by Max_Value__c';
        List < Comission_Configuration__c > lstOfRecords = Database.query(dQuery);
        
        for(Comission_Configuration__c cfg : lstOfRecords){
                comiconfig.put(cfg.Max_Value__c,cfg);
        }
        return comiconfig;     
    }
    public static Map<Decimal,Comission_Configuration__c> getFsConfiguration(){
        Map<Decimal,Comission_Configuration__c> comiconfig = new Map<Decimal,Comission_Configuration__c>();
        String dQuery = 'SELECT Max_Value__c,Min_Value__c,'+
            'Percentage_Paid_to_Agent_Making_Sell__c,Percentage_Paid_to_Director__c,'+
            'Percentage_Paid_to_Vice_President__c,Percentage_Paid_to_Senior_Agent__c '+
            'FROM Comission_Configuration__c where product__c=null AND For_First_Sell__c=true order by Max_Value__c';
        List < Comission_Configuration__c > lstOfRecords = Database.query(dQuery);
        
        for(Comission_Configuration__c cfg : lstOfRecords){
                comiconfig.put(cfg.Max_Value__c,cfg);
        }
        return comiconfig;     
    }
    public static Decimal getTotalCommission(Id salesperson, set<id> comConfigProdList,Decimal week){
        Map<id,String> userValueMap = new Map<id,String>();
        List<Integer> inList = new List<Integer>(); 
        System.debug('week is : '+week);
        Integer weekOdd = Integer.valueof(week/2);
        Integer weekEven = weekOdd*2;
        if(weekEven==week){
            inList.add(Integer.valueof(week));
            inList.add(Integer.valueof(week-1));
        }else{
            inList.add(Integer.valueof(week));
            inList.add(Integer.valueof(week+1));
        }
        System.debug('inList : '+inList);
        List<AggregateResult>  aggResult = [SELECT SUM(Total_Commissionable_amount__c)total,SUM(current_commissionable_amount__c)ref FROM OpportunityLineItem  where Opportunity.Sales_Person_Id__c =: salesperson AND Opportunity.week__c IN : inList AND Product2Id not in : comConfigProdList];
        System.debug('aggResult : '+aggResult);
        Decimal totalAmount = Decimal.valueOf(''+aggResult.get(0).get('total')) - Decimal.valueOf(''+aggResult.get(0).get('ref'));
        System.debug('totalAmount '+totalAmount);
        return totalAmount;
    }
    public static Decimal getFsTotal(Id salesperson, set<id> comConfigProdList,Decimal week){
        Map<id,String> userValueMap = new Map<id,String>();
        List<Integer> inList = new List<Integer>(); 
        System.debug('week is : '+week);
        Integer weekOdd = Integer.valueof(week/2);
        Integer weekEven = weekOdd*2;
        if(weekEven==week){
            inList.add(Integer.valueof(week));
            inList.add(Integer.valueof(week-1));
        }else{
            inList.add(Integer.valueof(week));
            inList.add(Integer.valueof(week+1));
        }
        System.debug('inList : '+inList);
        List<AggregateResult>  aggResult = [SELECT SUM(Total_Commissionable_amount__c)total,SUM(current_commissionable_amount__c)ref FROM OpportunityLineItem  where Opportunity.Sales_Person_Id__c =: salesperson AND Opportunity.week__c IN : inList AND Product2Id not in : comConfigProdList];
        System.debug('aggResult : '+aggResult);
        Decimal totalAmount = Decimal.valueOf(''+aggResult.get(0).get('total')) - Decimal.valueOf(''+aggResult.get(0).get('ref'));
        System.debug('totalAmount '+totalAmount);
        return totalAmount;
    }
    public static void insertComission(id userId, id paymentId, Decimal amount,Decimal add_days,Date WeekDate,boolean getweek,List<Comission__c> comList,Comission_Configuration__c com,String hierarchy,Decimal com_percent){
        System.debug('commission added');
        Integer weeks;
        List<first_monday_of_year__c> firstmonday = [SELECT day__c,month__c,year__c FROM first_monday_of_year__c];
        Date myDate = date.newinstance((Integer)firstmonday.get(0).year__c,(Integer)firstmonday.get(0).month__c, (Integer)firstmonday.get(0).day__c);
        Integer day = 0;
        if(getweek){ 
            weeks = Integer.valueOf(add_days);
        }else {
            day = myDate.daysBetween(WeekDate.addDays(Integer.valueOf(add_days)));
            weeks = day/7 + 1;
        }
        Comission__c comAgent = new Comission__c();
        comAgent.User__c = userId;
        comAgent.Amount__c = amount;
        comAgent.Paid__c=true;
        comAgent.Payment__c = paymentId;
        comAgent.weeks__c = weeks;
        comAgent.created_from_batch__c = true;
        comAgent.Comission_Configuration__c = com.id;
        comAgent.hierarchy__c = hierarchy;
        comAgent.commission_percent__c = com_percent;
        comList.add(comAgent);
    }
    public static Comission_Configuration__c getCommissionConfiguration(Decimal amount, Map<Decimal,Comission_Configuration__c> maxValAndComMap){
        for(Decimal amt : maxValAndComMap.keyset()){
            if(amount <= amt){
                return maxValAndComMap.get(amt);
            }else{
                continue;
            }
        }
        return null;
    }
}