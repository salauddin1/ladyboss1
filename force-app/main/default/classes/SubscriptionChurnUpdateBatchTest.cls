@isTest
public class SubscriptionChurnUpdateBatchTest {	
    @isTest
    public static void test1(){
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 0;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Additional_CLUB_Commissionable_Volume__c = 0;
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 100, IsActive = true);
        insert customPrice; 
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Stripe_Charge_Id__c = 'Strid';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        ol1.Subscription_Id__c = 'sub_EvnCmhHIWIdT6F';
        ol1.Dynamic_Item_Count_Cost__c = 0;
        ol1.Product_Number_Count__c = 0;
        oL1.Quantity = 1;
        ol1.Consider_For_Commission__c = true;
        insert oL1;
        
        Test.startTest();
        SubscriptionChurnUpdateBatch sub = new SubscriptionChurnUpdateBatch(' Subscription_Id__c!=null ');
        Database.executeBatch(sub,1);
        Test.stopTest();
    }
}