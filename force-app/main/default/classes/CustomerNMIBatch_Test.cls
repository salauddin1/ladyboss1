@IsTest
public class CustomerNMIBatch_Test {
    public static String CRON_EXP = '0 0 0 15 3 ? 2030';
    
    static{
        NMI_Parameters__c np = new NMI_Parameters__c(User__c='user',Password__c='pwd',Endpoint__c='https://secure.nmi.com/api/');
        insert np;
    }
    
    static testMethod void testScheduler() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today());
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Test.startTest();
            System.schedule('NMIScheduler',CRON_EXP,new NMIScheduler());
        Test.stopTest();
	}
    
	static testMethod void test() {
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new CustomerNMIBatch(false));
        Test.stopTest();
	}
    
    static testMethod void test2() {
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',Created_At__c=system.today());
        insert acc;
        
        system.Test.setMock(HttpCalloutMock.class, new NMIMockImplementation()); 
        Test.startTest();
        Database.executeBatch(new CustomerNMIBatch(true));
        Test.stopTest();
	}
}