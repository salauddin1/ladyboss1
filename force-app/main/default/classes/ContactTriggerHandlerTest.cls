@istest
public class ContactTriggerHandlerTest {
  @isTest
    public static void testmethod1() {
        OrderFormControllerChanges.invoiceflag = false;
        List<Contact> TrigNewList = new List<Contact> ();
        ContactTriggerFlag.isContactBatchRunning = false;
        Contact con = new contact();
        con.FirstName = 'testName';
        con.LastName = 'Lastname';
        con.Email = 'TestEmail@gmail.com';
        con.Phone= '1234567890';
        con.Send_Site_Login_Email__c = false;
        con.Enable_Product_Access_In_Hub__c = '14 Day Rainbow Detox';
        con.Product__c = 'Trial';
        con.Email_2__c = 'test2@gmail.com';
        con.Email_3__c = 'test3@gmail.com';
        con.OtherPhone = '1234567890';
        insert con;
        con.Enable_Product_Access_In_Hub__c = 'Booty Bootcamp';
        con.Send_Site_Login_Email__c = true;
        con.Product__c = 'Book';
        update con;
        
        Coaching_Commissions__c comsn = new Coaching_Commissions__c();
        comsn.Commission_Amount__c = 10;
        comsn.Contact__c = con.Id;
        comsn.Commission_Type__c = 'testType';
        insert comsn;
        TrigNewList.add(con);
        test.startTest(); 
        ContactTriggerHandler.checkAmazonCustomer (TrigNewList);
        Test.stopTest();
    }
    @isTest static void CampMember() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
       
        Campaign_Auto_SMS__mdt cm = new Campaign_Auto_SMS__mdt();
        cm.Campaign_Name__c = 'New Campaign Test';
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Burn Buyer';
        insert cmp;
        
         
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.Email = 'test@test.com';
        cnt.Marketing_Cloud_Tags__c = 'Burn Buyer';
        insert cnt;
        
        
        
    }
     @isTest static void CampMemberV1() {
        
        Account acnt = new Account();
        acnt.Name = 'test';
        insert acnt;
       
        Campaign_Auto_SMS__mdt cm = new Campaign_Auto_SMS__mdt();
        cm.Campaign_Name__c = 'New Campaign Test';
        
        Campaign cmp = new Campaign();
        cmp.Name = 'Burn Buyer';
        insert cmp;
         Campaign cmp1 = new Campaign();
        cmp1.Name = 'Customer List';
        insert cmp1;
         
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.Email = 'test@test.com';
        cnt.Marketing_Cloud_Tags__c = 'Burn Buyer';
        insert cnt;
        cnt.Marketing_Cloud_Tags__c = 'Customer List';
        update cnt;
     }
       @isTest 
       public static void addconToPAPComm() {
         
        Contact cnt = new Contact();
        cnt.LastName = 'TEst';
        cnt.MobilePhone = '9876543210';
        cnt.Email = 'test@test.com';
        cnt.Marketing_Cloud_Tags__c = 'Burn Buyer';
        cnt.PAP_refid__c = 'testaff';
        insert cnt;
        Pap_Commission__c papc = new Pap_Commission__c();
        papc.affiliate_ref_id__c = cnt.PAP_refid__c;
        papc.Campaign_Id__c = 'test';
        papc.Campaign_name__c = 'testcmp';
        papc.commission__c = 2.01;
        papc.order_id__c = 'ijdh';
        insert papc;
        test.startTest();
           ContactTriggerHandler.addPapToContact (new List<Contact>{cnt});   
        test.stopTest();
    }
}