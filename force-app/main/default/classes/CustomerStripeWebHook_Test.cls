@isTest
public class CustomerStripeWebHook_Test {
    @isTest static void test(){
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"wynche5@cloudcreations.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	CustomerStripeWebHook.CustomerStripe();
        Test.stopTest();
    }
    
    @isTest static void test2(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BUOWQlyttHhSXd');
        insert acc;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"wynche5@cloudcreations.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	CustomerStripeWebHook.CustomerStripe();
        Test.stopTest();
    }
    
    @isTest static void test3(){
        ApexUtil.isTriggerInvoked = true;
        Account acc = new Account(Name='Test',External_ID__c='cus_BUOWQlyttHhSXd', DML_From_Salesforce__c=true);
        insert acc;
        
        RestRequest req=new RestRequest();
        RestResponse res=new RestResponse();
        req.requestURI = '/services/apexrest/CustomerStripeWebHook';  //Request URL
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        String body = '{ \"id\": \"evt_1B7Pj6DiFnu7hVq7Eipk9GR7\", \"object\": \"event\", \"api_version\": \"2017-08-15\", \"created\": 1506696268, \"data\": { \"object\": { \"id\": \"cus_BUOWQlyttHhSXd\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1506696268, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": \"des5\", \"discount\": null, \"email\": \"wynche5@cloudcreations.com\", \"livemode\": false, \"metadata\": { }, \"shipping\": {\"address\":{\"city\":\"test city\",\"country\":\"test country\",\"line1\":null,\"line2\":null,\"postal_code\":\"123\",\"state\":\"test state\"},\"name\":\"Test\",\"phone\":null}, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_BUOWQlyttHhSXd/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_pQOlrNvwYLYRko\", \"idempotency_key\": null }, \"type\": \"customer.created\" }';
        req.requestBody = Blob.valueOf(body);
        RestContext.request = req;
        //req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        	CustomerStripeWebHook.CustomerStripe();
        Test.stopTest();
    }
}