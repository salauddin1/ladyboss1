global class UpdateContactDetailsBatch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('select id,Email,MobilePhone,Phone,OtherPhone,Email_2__c,Email_3__c,Last_Mail_Modified_Date__c,MailingStreet,MailingCity,MailingState,MailingCountry,MailingPostalCode,FirstName,LastName from Contact');
    }
    
    global void execute(Database.BatchableContext context, List<SObject> records) { 
        List<Mail_Address__c> lstMailingAddresses = new List<Mail_Address__c>();
        
        List<ContactOld_Email__c> lstContactOldEmail = new List<ContactOld_Email__c>();
        List<ContactOld_Phone__c> lstContactOldPhone = new List<ContactOld_Phone__c>();
        
        for(Sobject sobj : records){
            Contact cnt = (Contact)sobj;
            if(cnt.MailingStreet != null || cnt.MailingCity != null || cnt.MailingState != null || cnt.MailingCountry != null || cnt.MailingPostalCode != null )  {  
                Mail_Address__c oldMail = new Mail_Address__c();  
                oldMail.Last_Mail__c = String.valueOf(cnt.MailingStreet) + ', ' + String.valueOf(cnt.MailingCity) + ', ' + String.valueOf(cnt.MailingState) + ', ' + String.valueOf(cnt.MailingCountry) + ' - ' + String.valueOf(cnt.MailingPostalCode);
                oldMail.Contact__c = cnt.id;
                oldMail.Name = cnt.FirstName+' '+cnt.LastName;
                oldMail.Last_Mail_Modified_Date__c = Datetime.now();
                oldMail.Address_type__c = 'Mailing Address';
                lstMailingAddresses.add(oldMail); 
            }
            if(cnt.Email != null){
            	ContactOld_Email__c oldCntEmail = new ContactOld_Email__c ();
                oldCntEmail.Contact__c = cnt.Id;
                oldCntEmail.Name = cnt.FirstName+' '+cnt.LastName;
                oldCntEmail.Email_Type__c = 'Email';
                oldCntEmail.Last_Email__c = cnt.Email;
                lstContactOldEmail.add(oldCntEmail);
            }
            if(cnt.Email_2__c != null){
             	ContactOld_Email__c oldCntEmail = new ContactOld_Email__c ();
                oldCntEmail.Contact__c = cnt.Id;
                oldCntEmail.Name = cnt.FirstName+' '+cnt.LastName;
                oldCntEmail.Email_Type__c = 'Email 2';
                oldCntEmail.Last_Email__c = cnt.Email_2__c;
                lstContactOldEmail.add(oldCntEmail);   
            }
            if(cnt.Email_3__c != null){
            	ContactOld_Email__c oldCntEmail = new ContactOld_Email__c ();
                oldCntEmail.Contact__c = cnt.Id;
                oldCntEmail.Name = cnt.FirstName+' '+cnt.LastName;
                oldCntEmail.Email_Type__c = 'Email 3';
                oldCntEmail.Last_Email__c = cnt.Email_3__c;
                lstContactOldEmail.add(oldCntEmail);    
            }
            if(cnt.Phone != null){
             	ContactOld_Phone__c oldPhoneCnt = new ContactOld_Phone__c ();  
                oldPhoneCnt.Last_Phone__c = cnt.Phone;
				oldPhoneCnt.Phone__c = 'Phone';
                oldPhoneCnt.Name = cnt.FirstName+' '+cnt.LastName;
                lstContactOldPhone.add(oldPhoneCnt);
            }
            if(cnt.MobilePhone != null){
             	ContactOld_Phone__c oldPhoneCnt = new ContactOld_Phone__c ();  
                oldPhoneCnt.Last_Phone__c = cnt.Phone;
				oldPhoneCnt.Phone__c = 'Mobile';
                oldPhoneCnt.Name = cnt.FirstName+' '+cnt.LastName;
                lstContactOldPhone.add(oldPhoneCnt);   
            }
            if(cnt.OtherPhone != null) {
            	ContactOld_Phone__c oldPhoneCnt = new ContactOld_Phone__c ();  
                oldPhoneCnt.Last_Phone__c = cnt.Phone;
				oldPhoneCnt.Phone__c = 'Other Phone';
                oldPhoneCnt.Name = cnt.FirstName+' '+cnt.LastName;
                lstContactOldPhone.add(oldPhoneCnt);    
            }
        }
        insert lstMailingAddresses;
        insert lstContactOldEmail;
        insert lstContactOldPhone;
    }
    
    global void finish(Database.BatchableContext context) {
        
    }
}