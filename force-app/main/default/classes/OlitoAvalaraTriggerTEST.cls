@isTest
public class OlitoAvalaraTriggerTEST {
    @isTest
    public static void insertMethod1(){
    	Test.startTest();
    	Test.setMock(HttpCalloutMock.class, new StripeGetCard_MockTest()); 
    	contact con = new contact();
        con.lastName='test';
        con.MailingCity = 'testcity';
        con.MailingCountry = 'usa';
        con.MailingPostalCode = '87112';
        //con.MailingStreet = 'teststreet';
        con.MailingState = 'testState';
        insert con;
        
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        prod.Commissionable_amount__c = 5;
        prod.Price__c = 100;
        prod.Shipping_cost_1__c = 0;
        prod.Shipping_cost_2__c = 0;
        prod.Shipping_cost_3__c = 0;
        prod.Shipping_cost_4__c = 0;
        prod.Shipping_cost_5__c = 0;
        prod.Dynamic_Fulfillment_Item_Count__c = 1;
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Opportunity op1 = new Opportunity();
        op1.Amount = 100;
        op1.Name = 'opp1';
        op1.Contact__c =con.id;
        op1.Pricebook2Id = customPB.id;
        op1.Sales_Person_Id__c = userinfo.getuserid();
        op1.StageName = 'Closed Won';
        op1.CloseDate = System.today();
        op1.Created_Using__c = 'V8';
        op1.OwnerId= userinfo.getuserid();
        insert op1;
        
        
        OpportunityLineItem oL1 = new OpportunityLineItem();
        oL1.OpportunityId = op1.id;
        oL1.TotalPrice = 100;
        oL1.Product2Id = prod.id;
        ol1.refund__c = 0;
        oL1.Quantity = 2;
        oL1.Subscription_id__c = 'test';
        oL1.Last_Charge_Id__c = 'test';
        ol1.Dynamic_Item_Count_Cost__c = 1;
        ol1.Tax_Amount__c = 0.5;
        insert oL1;
        
        
        oL1.Last_Charge_Id__c = 'test1';
        update oL1;
        Test.stopTest();
    }
}