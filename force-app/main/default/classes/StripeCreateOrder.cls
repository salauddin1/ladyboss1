global class StripeCreateOrder {
	private static final String SERVICE_URL = 'https://api.stripe.com/v1/orders';
    global String stripeCurrency;
    global String id;
    global StripeChargeError error;
	global String charge; 
    global String selected_shipping_method; 
    global shipping_methods[] shipping_methods;  
    
    global static StripeCreateOrder createOrder(String type,String customerId, Map<String, Decimal> skuID, String stripeCurrency, String shippingName, String shippingStreet, String shippingCity,String shippingState,String shippingPostalCode,String shippingCountry){
        String s = '';
        system.debug('shippingcountry'+shippingCountry);
        HttpRequest http = new HttpRequest();
        http.setEndpoint(SERVICE_URL);
        http.setMethod('POST');
        http.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue);
        http.setHeader('Authorization', authorizationHeader);
        system.debug('create order'+skuID.size());
        if(skuID.size()>0){
            /*for(Integer i=0;i<skuID.keyset().size(); i++){
                s += '&items['+i+'][type]='+type+'&items['+i+'][parent]='+skuID[i];
                system.debug('Mutiple product'+ s);
            }*/
            integer i=0;
            for(String sId : skuID.keyset()){
                
                s += '&items['+i+'][type]='+type+'&items['+i+'][parent]='+sId+'&items['+i+'][quantity]='+skuID.get(sId).intValue();
                system.debug('Mutiple product'+ s);
                i++;
            }

        }
        if(shippingCountry=='United States'){

            shippingCountry='US';
        }
        http.setBody('currency='+stripeCurrency+s+'&shipping[name]='+shippingName+'&shipping[address][line1]='+shippingStreet+
                    '&shipping[address][city]='+shippingCity+'&shipping[address][state]='+shippingState+'&shipping[address][postal_code]='+shippingPostalCode
                    +'&shipping[address][country]='+shippingCountry+'&customer='+customerId);
        String response;
        Integer statusCode;
        Http con = new Http();
        HttpResponse hs = new HttpResponse();
        if(!Test.isRunningTest()){
            try {
                    hs = con.send(http);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }else{
            hs.setStatusCode(200);
        }
         system.debug('#### '+ hs.getBody());
        
        response = hs.getBody();
        statusCode = hs.getStatusCode();
        system.debug('$$statusCode = '+hs.getStatusCode());
         try {
            StripeCreateOrder o = StripeCreateOrder.parse(response);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }
    
    global static StripeCreateOrder payOrder(String orderID, String customerId, String prodDescription,String stripStatementDescriptor,String stripSecStatementDescriptor,String Name,String billingAddress,String email, String phone,String conId,String MailingStreet,String MailingCity,String MailingState,String MailingPostalCode,String MailingCountry,String salesPerson) {
        
        
        Map<String,String> metadata = new Map<String,String>();
        metadata.put('Billing Address',billingAddress);
        metadata.put('Full Name',Name);
        metadata.put('Email',email);
        metadata.put('Phone',phone);
        if(salesPerson != null && salesPerson != ''){
            metadata.put('Sales Person',salesPerson);    
        }else{
            metadata.put('Sales Person',UserInfo.getName());
        }
        
        metadata.put('Integration Initiated From','Salesforce');
        metadata.put('One Time Products Charge','YES');
        metadata.put('Salesforce Contact Link',System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+conId);
        metadata.put('Fulfillment Product Name',stripSecStatementDescriptor);
    
        metadata.put('Street',MailingStreet);
        metadata.put('City',MailingCity);
        metadata.put('State',MailingState);
        metadata.put('Postal Code',MailingPostalCode);
        metadata.put('Country',MailingCountry);
    
        system.debug('metadata'+metadata);
        return StripeCreateOrder.payOrder(orderID, customerId, metadata);
    }
    
    
    global static StripeCreateOrder payOrder(String orderID, String customerID, Map<String,String> metadata ){
        
        String s = orderID;
        HttpRequest http1 = new HttpRequest();
        http1.setEndpoint(SERVICE_URL+'/'+s+'/pay');
        http1.setMethod('POST');
        http1.setHeader('content-type', 'application/x-www-form-urlencoded');
        
        Blob headerValue1 = Blob.valueOf(StripeAPI.ApiKey + ':');
        String authorizationHeader1 = 'BASIC ' +
            EncodingUtil.base64Encode(headerValue1);
        http1.setHeader('Authorization', authorizationHeader1);
          Map<String, String> payload = new Map<String, String>{
            'customer' => customerId,
            'email' => 'test@test.com'
                };
		                    
       if (metadata != null) {
            for (String key : metadata.keySet()) {
                if (metadata.get(key) != null) {
                    payload.put('metadata['+key+']', metadata.get(key));
                }
            }
        }
        
        http1.setBody(StripeUtil.urlify(payload));
        String response1;
        Integer statusCode1;
        Http con1 = new Http();
        HttpResponse hs1 = new HttpResponse();
        if(!Test.isRunningTest()){
            try {
                    hs1 = con1.send(http1);
                } catch (CalloutException e) {
                    system.debug('e.getMessage()-->'+e.getMessage());
                    return null;
                }
        }else{
            hs1.setStatusCode(200);
        }
         system.debug('#### '+ hs1.getBody());
        
        response1 = hs1.getBody();
        statusCode1 = hs1.getStatusCode();
        system.debug('$$statusCode = '+hs1.getStatusCode());
         try {
            StripeCreateOrder o = StripeCreateOrder.parse(response1);
            System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
            return o;
        } catch (System.JSONException e) {
            System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
            return null;
        }
    }

    global static StripeCreateOrder updateOrder(StripeCreateOrder sco, String carrier,String service,Integer deliveryDays){
        if(sco.shipping_methods.size()>0 && carrier!=null && service!=null){
            String selectedShippingMethod;
            String shipping;
            if(deliveryDays!=null){
                shipping=carrier+': '+service+' ('+deliveryDays+' day delivery)';
            }else{
                shipping = carrier+': '+service;
            }
            for(Integer i=0;i<sco.shipping_methods.size();i++){
                if(sco.shipping_methods[i].description==shipping){
                    selectedShippingMethod = sco.shipping_methods[i].id;
                }
            }
                        
            HttpRequest http1 = new HttpRequest();
            http1.setEndpoint(SERVICE_URL+'/'+sco.id);
            http1.setMethod('POST');
            http1.setHeader('content-type', 'application/x-www-form-urlencoded');
            
            Blob headerValue1 = Blob.valueOf(StripeAPI.ApiKey + ':');
            String authorizationHeader1 = 'BASIC ' +
                EncodingUtil.base64Encode(headerValue1);
            http1.setHeader('Authorization', authorizationHeader1);
              Map<String, String> payload = new Map<String, String>{
                'selected_shipping_method' => selectedShippingMethod
                    };
                                
           
            
            http1.setBody(StripeUtil.urlify(payload));
            String response1;
            Integer statusCode1;
            Http con1 = new Http();
            HttpResponse hs1 = new HttpResponse();
            if(!Test.isRunningTest()){
                try {
                        hs1 = con1.send(http1);
                    } catch (CalloutException e) {
                        system.debug('e.getMessage()-->'+e.getMessage());
                        return null;
                    }
            }else{
                hs1.setStatusCode(200);
            }
             system.debug('#### '+ hs1.getBody());
            
            response1 = hs1.getBody();
            statusCode1 = hs1.getStatusCode();
            system.debug('$$statusCode = '+hs1.getStatusCode());
             try {
                StripeCreateOrder o = StripeCreateOrder.parse(response1);
                System.debug(System.LoggingLevel.INFO, '\n**** StripeCreateSKU object: '+o); 
                return o;
            } catch (System.JSONException e) {
                System.debug(System.LoggingLevel.INFO, '\n**** JSONException: '+e); 
                return null;
            }
        }        
        return sco;
    }
    
     public static StripeCreateOrder parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);
        
        return (StripeCreateOrder) System.JSON.deserialize(json, StripeCreateOrder.class);
    }
    global class shipping_methods {
        global String id;   //rate_eab91f0d63e549fd9a3605ff97c88bda
        global Integer amount;  //3652
        global String description;  //USPS: Express
        global Integer delivery_days;
    }
}