@isTest
public class AddressValidationTest {
    static testMethod void test1(){
        HttpResponse res = new HttpResponse();
        res.setBody('{"error":{"code":"ADDRESS.VERIFY.FAILURE","message":"Unable to verify address.","errors":[{"code":"E.ADDRESS.NOT_FOUND","field":"address","message":"Address not found","suggestion":null},{"code":"E.HOUSE_NUMBER.MISSING","field":"street1","message":"House number is missing","suggestion":null}]}}');
        res.setStatusCode(400);
        Test.setMock(HttpCalloutMock.class, new MockTestClassAddressValidation(res));
    	AddressValidation.resolveAddress('abc','test','abc','test','abc');
    }
    static testMethod void test2(){
        HttpResponse res = new HttpResponse();
        res.setBody('{ "id": "adr_435f175d6f7642b9a113aea7b3f432b6", "object": "Address", "created_at": "2019-11-25T07:42:36Z", "updated_at": "2019-11-25T07:42:36Z", "name": null, "company": "LADYBOSS", "street1": "8771 WHITE BEECH DR", "street2": "", "city": "SAGINAW", "state": "MI", "zip": "48603-9609", "country": "US", "phone": null, "email": null, "mode": "test", "carrier_facility": null, "residential": true, "federal_tax_id": null, "state_tax_id": null, "verifications": { "delivery": { "success": true, "errors": [], "details": { "latitude": 43.477, "longitude": -84.08589, "time_zone": "America/Detroit" } } } }');
        res.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, new MockTestClassAddressValidation(res));
    	AddressValidation.resolveAddress('abc','test','abc','test','abc');
    }
}