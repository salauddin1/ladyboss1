@isTest
public class UpdateNullAddressCharge_Test {
    public static TestMethod void testGetCallout(){
        
        Test.startTest(); 
        Map<id,Opportunity> idsAndOpportunityMap = new Map<id,Opportunity>();
        
        contact co = new contact();
        co.FirstName = 'testi';
        //co.Phone='23232';
        co.Email='DSDSD@JJJ.COM';
        co.LastName ='test';
        co.Stripe_Customer_Id__c='cus_DnqJ74SW8eLIZ2';
        insert co;
        
        Address__c ac = new Address__c();
        ac.Shipping_City__c = 'ssd';
        ac.Shipping_Country__c='dsds';
        ac.Shipping_State_Province__c='dsdsd';
        ac.Shipping_Street__c='dsdsdsdee';
        ac.Shipping_Zip_Postal_Code__c='2323';
        ac.Contact__c = co.id;
        ac.Primary__c = true;
        insert ac;
        
        Address__c ac2 = new Address__c();
        ac2.Contact__c = co.id;
        ac2.Primary__c = true;
        insert ac2;
        
        Id SubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity oppSub = new Opportunity();
        oppSub.Name = 'opp0' ;
        oppSub.RecordTypeId = SubRecordTypeId;
        oppSub.CloseDate = Date.Today();
        oppSub.StageName = 'Pending';
        oppSub.Clubbed__c = true;
        oppSub.Contact__c = Co.Id;
        insert oppSub;
        
        List<Opportunity> opList = [SELECT id,name from Opportunity where Name ='opp0'];
        opList[0].Stripe_Charge_Id__c = 'ch_1DRckcBwLSk1v1oh42yxV4KV';
        update opList;
        
        idsAndOpportunityMap.put(oppSub.id,oppSub);
        
        
        Id ChargeRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.Name = 'opp0' ;
        opp.RecordTypeId = ChargeRecordTypeId;
        opp.CloseDate = Date.Today();
        opp.StageName = 'Pending';
        opp.Clubbed__c = true;
        opp.Contact__c = Co.Id;
        insert opp;
        
        idsAndOpportunityMap.put(opp.id,opp);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        Invoice__c inv = new Invoice__c();
        inv.Charge_Id__c='ch_sdh2132h3hsu434nxzurb';
        inv.Subscription_Id__c = 'sub_Dq0IvbH3NVgpVE';
        inv.Invoice_Id__c = 'in-shfb44uwbcdsbzx';
        insert inv;

        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = pbEntry.UnitPrice,
            Subscription_Id__c = 'sub_Dq0IvbH3NVgpVE'
        );
        insert oli;
        
        
        UpdateNullAddressCharge obj = new UpdateNullAddressCharge();
            DataBase.executeBatch(obj);

       
        
        Test.stopTest(); 
        
    }

}