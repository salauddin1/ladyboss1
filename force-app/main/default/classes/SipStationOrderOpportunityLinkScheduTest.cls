@isTest
public class SipStationOrderOpportunityLinkScheduTest {
@isTest
    public static void scheduletestmethod () {
        contact con = new contact();
        con.lastName = 'YTest';
        insert con;
        ShipstationCancel_pages__c cnclpge = new ShipstationCancel_pages__c ();
        cnclpge.End_page__c = 100;
        cnclpge.Start_Page__c=1;
        insert cnclpge;
        ShipStation__c shopOrder = new ShipStation__c();
        shopOrder.userName__c = 'testName';
        shopOrder.password__c = 'password';
        shopOrder.IsLive__c=true;
        shopOrder.Domain_Name__c='http//ssapi9.shipstation.com';
        insert shopOrder;
        ShipStation_Orders__c shipOrder = new ShipStation_Orders__c();
        shipOrder.city__c = 'Indore';
        shipOrder.company__c = 'oak';
        shipOrder.country__c = 'In';
        shipOrder.orderStatus__c ='Draft';
        shipOrder.name__c=  'orderNumber';
        shipOrder.orderNumber__c = 'ch_1FHabFFzCf73siP0ZyFwpP0i';
        shipOrder.orderId__c = '28694680';
        shipOrder.Contact__c = con.Id;
        insert shipOrder;
        Test.startTest();
        System.schedule('ShipstationOrderOpportunityLinkerJob', '0 0 * * * ?', new SipStationOrderOpportunityLinkSchedular());
        Test.stopTest();
    }
}