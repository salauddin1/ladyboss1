public class GetallOrderFromShopify implements Iterator<Integer>{
    public Integer page;
    public shopifyOrdercount__c ord;
    public Integer count;
    public GetallOrderFromShopify(){
        ord = [select End_page__c,Start_Page__c from shopifyOrdercount__c limit 1];
        page =Integer.valueOf(ord.Start_Page__c);
    }
    public boolean hasNext(){
        if(page >=Integer.valueOf(ord.End_page__c)) {
           return false;
       } else {
           return true;
       }
    }
    public Integer next(){
        if(page == Integer.valueOf(ord.End_page__c)){return null;}
        page++;
        return page;
    }
}