@isTest
private class CaseOwnerTrigger_Test {
    public static testmethod void beforeUpdateTest(){
        Test.startTest();
        Case c = new Case();
        c.Status = 'New';
        c.Origin = 'web';
        insert c;
        c.Origin = 'Phone';
        update c;
        Test.stopTest();
    }
}