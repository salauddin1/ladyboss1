global class shipStationOrderUpdateBatch implements Database.Batchable<Integer>,Database.AllowsCallouts,Database.Stateful {
    global Integer pagenumber=0;
    Integer count =0;
    String scope2;
    global Iterable<Integer> start(Database.batchableContext info){
        return new ShipstationOrderUpdateBatchIterable();
    } 
    global void execute(database.batchablecontext bd, List<Integer> scope){
        System.debug('----scope----'+scope);
        String header;
        String FaieldOrderId;
        pagenumber = scope[0];
        // ------- Api details from Custom setting ------//
        ShipStation__c ShipStationUser = new ShipStation__c();
        try{
            ShipStationUser = [select Domain_Name__c,userName__c, password__c,isLive__c from ShipStation__c where isLive__c=true];
            System.debug('---ShipStationUser---'+ShipStationUser);
            if(ShipStationUser != null){
                header=EncodingUtil.base64Encode(blob.valueOf(ShipStationUser.userName__c+':'+ShipStationUser.password__c));
            }   
            System.debug('--header-'+header);
            ShipStationOrderUpdateBatch_Pages__c orddays = [select Pages__c,Number_of_Days__c from ShipStationOrderUpdateBatch_Pages__c limit 1];
            Date StartDate = Date.today();
            String StartDattime = String.valueOf(StartDate.addDays(-(Integer.valueOf(orddays.Number_of_Days__c))));
            StartDattime = StartDattime.replace(' ', 'T');
            String EndDate =String.valueOf(DateTime.now());
            EndDate = EndDate.replace(' ', 'T');
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ShipStationUser.Domain_Name__c+'/orders?modifyDateStart='+StartDattime+'&modifyDateEnd='+EndDate+'&pageSize='+Integer.valueOf(orddays.Pages__c)+'&page='+pagenumber);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Basic '+header);
            req.setTimeout(6000);
            req.setHeader('Accept', 'application/json');
            String ResponseBody;
            try{
                HttpResponse res = new Http().send(req);
                Integer statusCode = res.getStatusCode();
                if(statusCode == 200){
                    ResponseBody = res.getBody();
                }
                System.debug('======ResponseBody===='+ResponseBody);
            }
            catch (Exception e){}
            if(ResponseBody != null) {
                Map<String, Object> ordResult = (Map<String, Object>)JSON.deserializeUntyped(ResponseBody);
                System.debug('--ordResult---'+ordResult);
                List<Object> OrderList = new List<Object>();
                OrderList = (List<Object>)ordResult.get('orders');
                Map<String, String> customerMap = new Map<String, String> ();
                Map<String, Object> OrderMap = new Map<String, Object> ();
                Map<String, Map<String, Object>> OrderIdMap = new Map<String, Map<String, Object>> ();
                Set<String> orderids = new Set<String>();
                Set<String> orderCustomer = new Set<String>();
                List<Contact> contList = new List<Contact>();
                List<ShipStation_Orders__c> shipInsertList = new List<ShipStation_Orders__c>();
                List<ShipStation_Order_Items__c> shipOrdItemInsertList = new List<ShipStation_Order_Items__c>();
                Set<String> delOrdItemId = new Set<string>();
                List<object> shiplist = new List<object>();
                Set<Contact> insConSet = new Set<Contact>();
                List<Contact> insConList = new List<Contact>();
                Map<String,Integer> itemListSize = new Map<String,Integer>();
                List<Object> itemMap = new List<Object>();
                Map<String,ShipStation_Orders__c> shipStatMap = new Map<String,ShipStation_Orders__c>();
                Map<String,List<ShipStation_Order_Items__c>> shipStatItemMap = new Map<String,List<ShipStation_Order_Items__c>>();
                Map<String,List<string>> shipStatItemIds = new Map<String,List<String>>();
                Map<String,ShipStation_Order_Items__c> shipStatItem = new Map<String,ShipStation_Order_Items__c>();
                Map<String,List<Object>> jsonOrdItemMap = new Map<String,List<Object>>();
                Map<String,Object> jsonItemMap = new Map<String,Object>();
                Map<String,List<String>> jsonOrdItemIds = new Map<String,List<String>>();
                Map<String,Object> odmap = new  Map<String,Object>(); 
                Map<String,Object> billMap = new  Map<String,Object>(); 
                Map<String,Object> shipMap = new  Map<String,Object>(); 
                Map<String,Object> fullMap = new Map<String,Object>();
                if(OrderList != null && !OrderList.isEMpty()){
                    for(Object obj : OrderList){
                        System.debug('----obj---'+obj);
                        OrderMap = (Map<String, Object>)obj;// Map of orders form order lists
                        orderids.add(String.valueOf(OrderMap.get('orderId')));//Set for all Order's Id
                        if(OrderMap.get('customerEmail')!= null){
                            orderCustomer.add(String.valueOf(OrderMap.get('customerEmail')));//set of all customer's email 
                            customerMap.put(String.valueOf(OrderMap.get('orderId')),String.valueOf(OrderMap.get('customerEmail')));
                        }
                        OrderIdMap.put(String.valueOf(OrderMap.get('orderId')),OrderMap);//Map for  order Id and order Map
                    }
                }
                System.debug('---OrderMap---'+OrderMap);
                System.debug('---OrderIdMap-'+OrderIdMap);
                System.debug('---orderids--'+orderids);
                System.debug('---orderCustomer-'+orderCustomer);
                System.debug('---customerMap--'+customerMap);
                
                if( orderids != null && !orderids.isEmpty()){
                    if(orderCustomer != null && !orderCustomer.isEmpty()){
                        contList = [Select id,Email from Contact where Email =: orderCustomer  ];
                        System.debug('---contList---'+contList);
                    }
                    
                    //----List of ShipStationOrder Object matched with modified order from shipStation Json -------//
                    
                    List<ShipStation_Orders__c> ShipStordList = new List<ShipStation_Orders__c>();
                    ShipStordList = [select Id, orderNumber__c,Ship_To_City__c,Ship_To_State__c,Ship_To_Country__c,Ship_To_AddressVerified__c,Linked_To_Opportunity__c,Ship_To_PostalCode__c,orderTotal__c,Ship_Street1__c,customerEmail__c,orderStatus__c,orderId__c,orderKey__c,(Select id ,name__c,quantity__c,unitPrice__c,orderItemId__c,ShipStation_Orders__r.orderId__c from ShipStation_Order_Items__r) from ShipStation_Orders__c where orderId__c =:orderids];
                    System.debug('---ShipStordList---'+ShipStordList);
                    if(ShipStordList != null && !ShipStordList.isEmpty()){
                        for(ShipStation_Orders__c shipOrder : ShipStordList){
                            if(shipOrder.ShipStation_Order_Items__r != null && !shipOrder.ShipStation_Order_Items__r.isEmpty()){
                                List<ShipStation_Order_Items__c> shipItemList = (List<ShipStation_Order_Items__c>)shipOrder.ShipStation_Order_Items__r;
                                System.debug('--shipItemList---'+shipItemList);
                                if(shipItemList != null && !shipItemList.isEmpty()){
                                    for(ShipStation_Order_Items__c shipItem :shipItemList){
                                        if( shipOrder.orderId__c != null && shipItem != null ){
                                            if(shipStatItemMap.containsKey(shipOrder.orderId__c)){
                                                shipStatItemMap.get(shipOrder.orderId__c).add(shipItem);// Map Of order Id and Order's Line Item
                                            }
                                            else {
                                                shipStatItemMap.put(shipOrder.orderId__c, new List<ShipStation_Order_Items__c>{shipItem});// Map for order Id and Order's Line Item
                                            }
                                            if(shipStatItemIds.containsKey(shipOrder.orderId__c)){
                                                shipStatItemIds.get(shipOrder.orderId__c).add(shipItem.orderItemId__c);//Map Of Order Id and order's Line Items's Id
                                            }
                                            else {
                                                shipStatItemIds.put(shipOrder.orderId__c, new List<String>{shipItem.orderItemId__c});//Map for Order Id and order's Line Items's Id
                                            }
                                            if(shipItem != null && shipItem.orderItemId__c != null){
                                                shipStatItem.put(shipItem.orderItemId__c,shipItem);
                                            }
                                        }
                                    }
                                }
                            }
                            System.debug('---shipStatItemMap---'+shipStatItemMap);
                            itemListSize.put(shipOrder.orderId__c,shipOrder.ShipStation_Order_Items__r.size());//Map for order Id and size of Item List from Shipstation Object
                            if(shipOrder.orderId__c != null && shipOrder.orderId__c!= ''){
                                shipStatMap.put(shipOrder.orderId__c,shipOrder);//Map of order Id and order details
                            }
                        }
                    }
                    System.debug('---shipStatItemMap---'+shipStatItemMap);
                    System.debug('---shipStatMap---'+shipStatMap);
                    
                    for(String odId : orderids) {
                        if(odId!= null && OrderIdMap!= null && !OrderIdMap.isEmpty()){
                            if(OrderIdMap.containsKey(odId)) {
                                system.debug('---odId---'+odId);
                                if(OrderIdMap.get(odId) != null && !OrderIdMap.get(odId).isEmpty()){
                                    fullMap  = (Map<String,Object>)OrderIdMap.get(odId);
                                    System.debug('-fullMap---'+fullMap);
                                }
                                if(fullMap != null && fullMap.get('advancedOptions') != null){
                                    odmap  = (Map<String,Object>)fullMap.get('advancedOptions');
                                    System.debug('---odmap--'+odmap);
                                }
                                if(fullMap != null && fullMap.get('billTo') != null){
                                    billmap  = (Map<String,Object>)fullMap.get('billTo');
                                    System.debug('---billmap--'+billmap);
                                }
                                if(fullMap != null && fullMap.get('shipTo') != null) {
                                    shipMap  = (Map<String,Object>)fullMap.get('shipTo');
                                    System.debug('---shipMap--'+shipMap);
                                }
                                if(fullMap.get('items') != null){
                                    itemMap  = (List<Object>)fullMap.get('items');
                                    System.debug('----itemMap-----'+itemMap+'-----itemMap.size()----'+itemMap.size()+'-----'+fullMap.get('orderId'));
                                    if(itemMap!= null && !itemMap.isEmpty()){
                                        for(Object items : itemMap) {
                                            Map<String,Object> itMap = (Map<String,Object>)items;
                                            String itemid = String.valueOf(itMap.get('orderItemId'));
                                            if( odId != null && items != null && jsonOrdItemMap.containsKey(odId)){
                                                jsonOrdItemMap.get(odId).add(items);//map of order id and list of order items
                                            }
                                            else if(items != null ){
                                                jsonOrdItemMap.put(odId,new List<Object>{items});//map of order id and list of order items
                                            } 
                                            if( odId != null && itemid != null && jsonOrdItemIds.containsKey(odId)){
                                                jsonOrdItemIds.get(odId).add(itemid);//map of  orderid and item ids
                                            }
                                            else if(itemid != null) {
                                                jsonOrdItemIds.put(odId,new List<String>{itemid});//map of  orderid and item ids
                                            }
                                            if(itemid != null && items != null ) {
                                                jsonItemMap.put(itemid,items);
                                            }
                                        }
                                        System.debug('---jsonOrdItemIds----'+jsonOrdItemIds);
                                        System.debug('---jsonItemMap---'+jsonItemMap);
                                    }
                                }
                                
                                System.debug('--shipStatMap not null 169---');
                                if(shipStatMap != null && !shipStatMap.isEmpty() && shipStatMap.containsKey(odId)){
                                    System.debug('--id matched---'+shipStatMap.keySet());
                                    if(contList != null && !contList.isEmpty()){
                                        for(Contact custEmail : contList) {
                                            System.debug('--custEmail.email--'+custEmail.Email+'--- customerMap.get(odId)---'+ customerMap.get(odId));
                                            if(custEmail!= null &&  custEmail.Email != null && customerMap.get(odId) != null && customerMap.get(odId) == custEmail.Email) {
                                                System.debug('-custEmail enter in if--'+custEmail+'--customerMap.get(odId)---'+customerMap.get(odId));
                                                shipStatMap.get(odId).Contact__c = custEmail.id;
                                                System.debug('----id email 193 match----');
                                            }
                                            
                                        }
                                    }
                                    else {
                                        System.debug('---enterd else----');
                                        if(customerMap.get(odId) != null){
                                            System.debug('----customerMap.get(odId)---'+customerMap.get(odId));
                                            Contact conIns = new Contact();
                                            conIns.Email =  customerMap.get(odId);
                                            String nam = customerMap.get(odId);
                                            if(billmap.get('name') != null && billmap.get('name') != '') {
                                                conIns.LastName = String.valueOf(billmap.get('name'));
                                            }
                                            else {
                                                conIns.LastName = nam.substringBefore('@');
                                            }
                                            insConSet.add(conIns);
                                            System.debug('---insConSet213---'+insConSet);
                                        }
                                    }
                                    
                                    
                                    if(fullMap.get('orderStatus') != null && ((shipStatMap.get(odId).orderStatus__c == null ) || (shipStatMap.get(odId).orderStatus__c != null && shipStatMap.get(odId).orderStatus__c != fullMap.get('orderStatus')))) {
                                        shipStatMap.get(odId).orderStatus__c = String.valueOf(fullMap.get('orderStatus'));
                                    }
                                    if(fullMap.get('customerEmail') != null && ((shipStatMap.get(odId).orderStatus__c == null ) || (shipStatMap.get(odId).orderStatus__c != null && shipStatMap.get(odId).orderStatus__c != fullMap.get('customerEmail')))) {
                                        shipStatMap.get(odId).customerEmail__c = String.valueOf(fullMap.get('customerEmail'));
                                        shipStatMap.get(odId).customerUsername__c = String.valueOf(fullMap.get('customerEmail'));
                                    }
                                    if(shipMap != null && !shipMap.isEmpty()){
                                        if(shipMap.get('street1') != null && ((shipStatMap.get(odId).Ship_Street1__c == null ) || (shipStatMap.get(odId).Ship_Street1__c != null && shipStatMap.get(odId).Ship_Street1__c != shipMap.get('street1')))) {
                                            shipStatMap.get(odId).Ship_Street1__c = String.valueOf(shipMap.get('street1'));
                                        }
                                        if(shipMap.get('city') != null  && ((shipStatMap.get(odId).Ship_To_City__c == null ) || (shipStatMap.get(odId).Ship_To_City__c != null && shipStatMap.get(odId).Ship_To_City__c != shipMap.get('city')))) {
                                            shipStatMap.get(odId).Ship_To_City__c =String.valueOf(shipMap.get('city'));
                                        }
                                        if(shipMap.get('country') != null && ((shipStatMap.get(odId).Ship_To_Country__c == null ) || (shipStatMap.get(odId).Ship_To_Country__c != null && shipStatMap.get(odId).Ship_To_Country__c != shipMap.get('country')))) {
                                            shipStatMap.get(odId).Ship_To_Country__c =String.valueOf(shipMap.get('country'));
                                        }
                                        if(shipMap.get('postalCode') != null && ((shipStatMap.get(odId).Ship_To_PostalCode__c == null ) || (shipStatMap.get(odId).Ship_To_PostalCode__c != null && shipStatMap.get(odId).Ship_To_City__c != shipMap.get('postalCode')))) {
                                            shipStatMap.get(odId).Ship_To_PostalCode__c =String.valueOf(shipMap.get('postalCode'));
                                        }
                                        if(shipMap.get('addressVerified') != null  && ((shipStatMap.get(odId).Ship_To_AddressVerified__c == null ) || (shipStatMap.get(odId).Ship_To_AddressVerified__c != null && shipStatMap.get(odId).Ship_To_AddressVerified__c != shipMap.get('addressVerified')))) {
                                            shipStatMap.get(odId).Ship_To_AddressVerified__c =String.valueOf(shipMap.get('addressVerified'));
                                        }
                                        if(shipMap.get('state') != null && ((shipStatMap.get(odId).Ship_To_State__c == null ) || (shipStatMap.get(odId).Ship_To_State__c != null && shipStatMap.get(odId).Ship_To_State__c != shipMap.get('state')))) {
                                            shipStatMap.get(odId).Ship_To_State__c =String.valueOf(shipMap.get('state'));
                                        }
                                    }
                                    if(fullMap.get('orderTotal') != null) {
                                        shipStatMap.get(odId).orderTotal__c = String.valueOf(fullMap.get('orderTotal'));
                                    }
                                    System.debug('---odId--'+odId+'----itemMapsize----'+itemMap.size()+'--itemListSize-----'+ itemListSize.get(odId));
                                    if(itemMap != null && jsonItemMap != null && itemListSize.get(odId)!= null && itemListSize.get(odId)!= 0 && jsonOrdItemMap!= null && shipStatItemMap != null  && jsonOrdItemMap.get(odId) != null &&  jsonOrdItemIds != null && jsonOrdItemIds.get(odId) != null && shipStatItemIds != null && shipStatItemIds.get(odId) != null){
                                        if(itemMap.size() !=  itemListSize.get(odId)){
                                            String conId ;
                                            for(String salItemId : jsonOrdItemIds.get(odId)) {
                                                System.debug('-salItemId---'+salItemId+'------jsonOrdItemIds.get--------'+jsonOrdItemIds.get(odId));
                                                if(salItemId != null && salItemId != ''){
                                                    if(shipStatItemIds.get(odId).contains(salItemId)){
                                                        System.debug('----salItemId202---'+salItemId);
                                                        conId = shipStatItem.get(salItemId).ShipStation_Orders__c;
                                                        System.debug('-----conId--ooo'+conId);
                                                        System.debug('----shipStatItem.getodId---'+shipStatItem.get(salItemId).ShipStation_Orders__c);
                                                        System.debug('----jsonOrdItemMap213---'+jsonOrdItemMap.get(salItemId));
                                                        Map<String,Object> jsMane = (Map<String,object>)jsonItemMap.get(salItemId);
                                                        if(jsMane != null && shipStatItem.get(salItemId).name__c != jsMane.get('name')){
                                                            shipStatItem.get(salItemId).name__c = String.valueOf(jsMane.get('name'));
                                                        }
                                                        if(jsMane != null && jsMane.get('quantity') != shipStatItem.get(salItemId).quantity__c) {
                                                            shipStatItem.get(salItemId).quantity__c =Integer.valueOf(jsMane.get('quantity'));
                                                            System.debug('-- quantity not match 220 ---');
                                                        }
                                                        if(jsMane != null && jsMane.get('unitPrice') != shipStatItem.get(salItemId).unitPrice__c) {
                                                            shipStatItem.get(salItemId).unitPrice__c =Integer.valueOf(jsMane.get('unitPrice'));
                                                            System.debug('-- price not match 220 ---');
                                                        }
                                                    }
                                                    else if(!shipStatItemIds.get(odId).contains(salItemId)){
                                                        System.debug('---salItemId206---'+salItemId);
                                                        System.debug('----jsonItemMap.get---'+jsonItemMap.get(salItemId));
                                                        Map<String,Object> itemVar = new Map<String,Object>();
                                                        itemVar = (Map<string,Object>)jsonItemMap.get(salItemId);
                                                        System.debug('--itemVar--'+itemVar.get('orderItemId'));
                                                        ShipStation_Order_Items__c shipNewOrdItem = new ShipStation_Order_Items__c();
                                                        System.debug('----conId---'+conId);
                                                        shipNewOrdItem.ShipStation_Orders__c = conId;
                                                        if(itemVar.get('name') != null && itemVar.get('name') != ''){
                                                            shipNewOrdItem.name__c = String.valueOf(itemVar.get('name'));
                                                        }
                                                        if(itemVar.get('orderItemId') != null && itemVar.get('orderItemId') != ''){
                                                            shipNewOrdItem.orderItemId__c = String.valueOf(itemVar.get('orderItemId'));
                                                        }
                                                        if(itemVar.get('orderItemId') != null && itemVar.get('orderItemId') != ''){
                                                            shipNewOrdItem.productId__c = String.valueOf(itemVar.get('orderItemId'));
                                                        }
                                                        if(itemVar.get('sku') != null && itemVar.get('sku') != ''){
                                                            shipNewOrdItem.sku__c = String.valueOf(itemVar.get('sku'));
                                                        }
                                                        if(itemVar.get('quantity') != null && itemVar.get('quantity') != ''){
                                                            shipNewOrdItem.quantity__c = Integer.valueOf(itemVar.get('quantity'));
                                                        }
                                                        if(itemVar.get('unitPrice') != null && itemVar.get('unitPrice') != ''){
                                                            shipNewOrdItem.unitPrice__c = Integer.valueOf(itemVar.get('unitPrice'));
                                                        }
                                                        shipOrdItemInsertList.add(shipNewOrdItem); 
                                                        System.debug('---shipNewOrdItem----'+shipNewOrdItem);
                                                    }
                                                }
                                            }
                                        }
                                        else {
                                            //----item match update items
                                            for(String salItemId : jsonOrdItemIds.get(odId)) {
                                                System.debug('-salItemId---'+salItemId+'------jsonOrdItemIds.get--------'+jsonOrdItemIds.get(odId));
                                                if(salItemId != null && salItemId != ''){
                                                    if(shipStatItemIds.get(odId).contains(salItemId)){
                                                        System.debug('----salItemId202---'+salItemId);
                                                        System.debug('----shipStatItem.getodId---'+shipStatItem.get(salItemId));
                                                        System.debug('----jsonItemMap259---'+jsonItemMap.get(salItemId));
                                                        Map<String,Object> jsMane = (Map<String,object>)jsonItemMap.get(salItemId);
                                                        if(jsMane != null && shipStatItem.get(salItemId).name__c != jsMane.get('name')){
                                                            shipStatItem.get(salItemId).name__c = String.valueOf(jsMane.get('name'));
                                                        }
                                                        if(jsMane != null && jsMane.get('quantity') != shipStatItem.get(salItemId).quantity__c) {
                                                            shipStatItem.get(salItemId).quantity__c =Integer.valueOf(jsMane.get('quantity'));
                                                            System.debug('-- quantity not match 271---');
                                                        }
                                                        if(jsMane != null && jsMane.get('unitPrice') != shipStatItem.get(salItemId).unitPrice__c) {
                                                            shipStatItem.get(salItemId).unitPrice__c =Integer.valueOf(jsMane.get('unitPrice'));
                                                            System.debug('-- quantity not match 271---');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    else if(itemMap.size() == 0 && itemListSize.get(odId) != 0 && jsonOrdItemMap != null  && shipStatItemIds != null && shipStatItemIds.get(odId)!= null){
                                        System.debug('---delete item - -- -'+jsonOrdItemIds.get(odId));
                                        for(String jsItemId : shipStatItemIds.get(odId)) {
                                            if(jsItemId != null && jsonOrdItemIds.get(odId) == null) {
                                                System.debug('----shipStatItem.get(jsItemId)---'+shipStatItem.get(jsItemId).id);
                                                delOrdItemId.add(shipStatItem.get(jsItemId).id);
                                                System.debug('--delOrdItemId249---'+delOrdItemId);
                                            }
                                        }
                                    }
                                    else if(itemMap != null && itemMap.size() != 0 && itemListSize.get(odId) == 0 && jsonOrdItemMap != null && jsonOrdItemIds.get(odId)!= null ){
                                        for(String salItemId : jsonOrdItemIds.get(odId)) {
                                            System.debug('----odId---'+odId+'-salItemId247---'+salItemId+'------jsonOrdItemIds.get--------'+jsonOrdItemIds.get(salItemId));
                                            if(salItemId != null && salItemId != ''){
                                                Map<String,Object> itemVar = new Map<String,Object>();
                                                itemVar = (Map<string,Object>)jsonItemMap.get(salItemId);
                                                System.debug('--itemVar--'+itemVar.get('orderItemId'));
                                                System.debug('------shipStatMap252----'+shipStatMap.get(odId).Id);
                                                String contId;
                                                contId = shipStatMap.get(odId).Id;
                                                ShipStation_Order_Items__c shipNewOrdItem = new ShipStation_Order_Items__c();
                                                if(contId != null){
                                                    shipNewOrdItem.ShipStation_Orders__c = contId;
                                                }
                                                if(itemVar.get('name') != null && itemVar.get('name') != ''){
                                                    shipNewOrdItem.name__c = String.valueOf(itemVar.get('name'));
                                                }
                                                if(itemVar.get('orderItemId') != null && itemVar.get('orderItemId') != ''){
                                                    shipNewOrdItem.orderItemId__c = String.valueOf(itemVar.get('orderItemId'));
                                                }
                                                if(itemVar.get('orderItemId') != null && itemVar.get('orderItemId') != ''){
                                                    shipNewOrdItem.productId__c = String.valueOf(itemVar.get('orderItemId'));
                                                }
                                                if(itemVar.get('sku') != null && itemVar.get('sku') != ''){
                                                    shipNewOrdItem.sku__c = String.valueOf(itemVar.get('sku'));
                                                }
                                                if(itemVar.get('quantity') != null && itemVar.get('quantity') != ''){
                                                    shipNewOrdItem.quantity__c = Integer.valueOf(itemVar.get('quantity'));
                                                }
                                                if(itemVar.get('unitPrice') != null && itemVar.get('unitPrice') != ''){
                                                    shipNewOrdItem.unitPrice__c = Integer.valueOf(itemVar.get('unitPrice'));
                                                }
                                                shipOrdItemInsertList.add(shipNewOrdItem); 
                                                System.debug('---shipNewOrdItem----'+shipNewOrdItem);
                                                
                                            }
                                        } 
                                    }
                                }
                            }
                        }
                    }
                }
                ShipstationOrderStatusShippedBatch.flag = true; //---- flag for stop calling shipstation trigger ----//
                update shipStatMap.values();
                System.debug('----- after  update shipStatMap--------'+shipStatMap.values());
                System.debug('----- before  update shipStatItem--------'+shipStatItem.values());
                update shipStatItem.values();
                System.debug('----- after  update shipStatItem--------'+shipStatItem.values());
                
                //----- If order Item is in Salesforce and not in ShipStation then delete item from salesforce ----
                if(delOrdItemId != null && !delOrdItemId.isEmpty()) {
                    List<ShipStation_Order_Items__c> deleteItemRecords = new List<ShipStation_Order_Items__c>();
                    deleteItemRecords = [Select id from ShipStation_Order_Items__c where id =:delOrdItemId];
                    System.debug('---deleteItemRecords----'+deleteItemRecords);
                    delete deleteItemRecords;
                    System.debug('----deleted success deleteItemRecords----'+deleteItemRecords);
                }
                //----- If order Item is in Salesforce and not in ShipStation then delete item from salesforce ----//
                
            }
        }
        catch(exception e) {
            system.debug(e.getMessage()+e.getLineNumber());
            ApexDebugLog apex=new ApexDebugLog();
            String pgeNum = 'Errorpage   ';
            if(scope.size()>0) {   for(Integer c : scope) { pgeNum += String.valueOf(c)+'     '; } }
            apex.createLog(new ApexDebugLog.Error('ececutemethod','shipStationOrderUpdateBatch',pgeNum, e));
        }
    }
    global void finish(database.batchablecontext bd){
    }
    
}