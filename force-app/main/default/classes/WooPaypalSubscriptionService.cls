@RestResource(urlMapping='/woo_paypalsubsService/*')
global class WooPaypalSubscriptionService {
    @HttpPost
    global static void createOpportunity() {
        String uri = RestContext.request.requestURI.substring(RestContext.request.requestURI.lastIndexOf('/')+1);
        System.debug('uri is : '+uri);
        if(uri == 'create'){
            if(RestContext.request.requestBody!=null){
                String str = RestContext.request.requestBody.toString();
                WooPaypalSubscriptionHandler.create(str);
            }
        }
    }
}