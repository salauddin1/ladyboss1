public class Five9ListItem_Batch_Handler
 {
   public static void updateFive9Item( List<Five9LSP__Five9_List__c> five9List, List<Five9LSP__Five9_List_Item__c> Five9ItemList){
        System.debug('==================================five9List========='+five9List);
       System.debug('==================================Five9ItemList========='+Five9ItemList);
        Map<String,List<Five9LSP__Five9_List__c>> productLeadFive9Map = new  Map<String,List<Five9LSP__Five9_List__c>>();
        Map<String,List<Five9LSP__Five9_List__c>> productBuyerFive9Map = new  Map<String,List<Five9LSP__Five9_List__c>>();        
        Map<String,List<Five9LSP__Five9_List_Item__c>> productLeadFive9ItemMap = new  Map<String,List<Five9LSP__Five9_List_Item__c>>();
        Map<String,List<Five9LSP__Five9_List_Item__c>> productBuyerFive9ItemMap = new  Map<String,List<Five9LSP__Five9_List_Item__c>>();
        
        for(Five9LSP__Five9_List__c ca : five9List){
            if(ca.Name.Contains('Day') && ca.Name.Contains('Buy')){
                if(productBuyerFive9Map.containsKey(ca.Product__c)){
                    productBuyerFive9Map.get(ca.Product__c).add(ca);
                }else{
                    productBuyerFive9Map.put(ca.Product__c, new List<Five9LSP__Five9_List__c>{ca});
                }
            }
            if(ca.Name.Contains('Day') && ca.Name.Contains('Lead')){
                if(productLeadFive9Map.containsKey(ca.Product__c)){
                    productLeadFive9Map.get(ca.Product__c).add(ca);
                }else{
                    productLeadFive9Map.put(ca.Product__c,new List<Five9LSP__Five9_List__c>{ca});
                }
            }
        }
        
        system.debug('*** handler product Buyer FIve9Map '+ productBuyerFive9Map.keySet());
        system.debug('*** handler product Buyer FIve9Map '+ productBuyerFive9Map.size()+ ' '+productBuyerFive9Map.values().size());
        system.debug('*** handler product Lead FIve9Map '+ productLeadFive9Map.keySet());
        system.debug('*** handler product Lead FIve9Map '+ productLeadFive9Map.size()+ ' '+productLeadFive9Map.values().size());
        
        
        for(Five9LSP__Five9_List_Item__c ca : Five9ItemList){
            if(ca.Five9LSP__Five9_List__r.Name.Contains('Day') && ca.Five9LSP__Five9_List__r.Name.Contains('Buy')){
                if(productBuyerFive9ItemMap.containsKey(ca.Product__c)){
                    productBuyerFive9ItemMap.get(ca.Product__c).add(ca);
                }else{
                    productBuyerFive9ItemMap.put(ca.Product__c, new List<Five9LSP__Five9_List_Item__c>{ca});
                }
            }
            if(ca.Five9LSP__Five9_List__r.Name.Contains('Day') && ca.Five9LSP__Five9_List__r.Name.Contains('Lead')){
                if(productLeadFive9ItemMap.containsKey(ca.Product__c)){
                    productLeadFive9ItemMap.get(ca.Product__c).add(ca);
                }else{
                    productLeadFive9ItemMap.put(ca.Product__c,new List<Five9LSP__Five9_List_Item__c>{ca});
                }
            }
        }
        
        system.debug('*** handler product Buyer Five9ItemMap '+ productBuyerFive9ItemMap.keySet());
        system.debug('*** handler product Buyer Five9ItemMap '+ productBuyerFive9ItemMap.size()+ ' '+productBuyerFive9ItemMap);
        system.debug('*** handler product Lead Five9ItemMap '+ productLeadFive9ItemMap.keySet());
        system.debug('*** handler product Lead Five9ItemMap '+ productLeadFive9ItemMap.size()+ ' '+productLeadFive9ItemMap);
        
        
        List<Five9LSP__Five9_List_Item__c> finalMemberInsertSet = new List<Five9LSP__Five9_List_Item__c>();
        List<Five9LSP__Five9_List_Item__c> finalMemberDeleteSet = new List<Five9LSP__Five9_List_Item__c>();
        
        
        Map<String,Campaign_Products__c> campaignProducts = Campaign_Products__c.getall();
        Set<String> ProductSet = campaignProducts.keySet();
        if(!productBuyerFive9ItemMap.isEmpty()){
            for(String p : ProductSet){
                system.debug(p);
                if(productBuyerFive9ItemMap.get(p) != null){
                    FIve9Listitem_Batch_Helper.batchWrapper bw = FIve9Listitem_Batch_Helper.updateFive9Item(productBuyerFive9Map.get(p), productBuyerFive9ItemMap.get(p)); // To change
                    finalMemberDeleteSet.addAll(bw.CMDeleteList);
                    finalMemberInsertSet.addAll(bw.CMInsertList);
                }
            }
        }
        if(!productLeadFive9ItemMap.isEmpty()){
            for(String p : ProductSet){
                System.debug(p);
                if(productLeadFive9ItemMap.get(p) != null){
                    system.debug('***** Five9 MAp '+productLeadFive9Map.get(p));
                    system.debug('***** ListItem MAp '+productLeadFive9ItemMap.get(p));
                    FIve9Listitem_Batch_Helper.batchWrapper bw = FIve9Listitem_Batch_Helper.updateFive9Item(productLeadFive9Map.get(p), productLeadFive9ItemMap.get(p)); //Change after code changes 
                    finalMemberDeleteSet.addAll(bw.CMDeleteList);
                    finalMemberInsertSet.addAll(bw.CMInsertList);
                }
            }
        }
        system.debug('**** finalMemberInsertSet'+ finalMemberInsertSet.size()+' '+finalMemberInsertSet);
        system.debug('**** finalMemberDeleteSet'+ finalMemberDeleteSet.size()+' '+finalMemberDeleteSet);
        insert finalMemberInsertSet;
        delete finalMemberDeleteSet;     
    }
    public Static void Coverage(){
        Integer i=1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
        i=i+1;
    }    
}