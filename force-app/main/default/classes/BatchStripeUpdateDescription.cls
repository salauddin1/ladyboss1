global class BatchStripeUpdateDescription implements Database.Batchable<sObject>, Database.AllowsCallouts {
    private static final String SERVICE_URL1 = 'https://api.stripe.com/v1/charges/';
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string query;
        BatchSetting__c batchLimit = [Select LimitStripe__c from BatchSetting__c];
        if(batchLimit != null){
            integer limitVal = integer.valueOf(batchLimit.LimitStripe__c);    
            query = 'Select ID, Charge_Description__c,IsUpdateProcess__c,Stripe_Charge_Id__c from Opportunity WHERE Stripe_Charge_Id__c!= null AND IsUpdateProcess__c = false limit '+limitVal;
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Opportunity> records) {  
        system.debug('==============records================'+records);
        String endpoint = SERVICE_URL1 + records[0].Stripe_Charge_Id__c;
        try {   
            Http http = new Http();
            HttpResponse res = new HttpResponse();
            HttpRequest req = new HttpRequest();
            Blob headerValue = Blob.valueOf(StripeAPI.ApiKey+ ':');
            System.debug('=====StripeAPI.ApiKey======'+StripeAPI.ApiKey);
            req.setEndpoint(endpoint);
            String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            res.setHeader('Content-Type', 'application/json');
            req.setMethod('GET'); 
            res = http.send(req);
            system.debug('=======res=========='+res);
            system.debug('=======res=res.getBody() !========='+res.getBody());
            if(res.getStatusCode() == 200 && res.getBody() != null) { 
                Map<String, Object> chrgResult = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                records[0].IsUpdateProcess__c = true;
                if(String.valueOf(chrgResult.get('description')) != null) {
                    records[0].Charge_Description__c = String.valueOf(chrgResult.get('description'));
                    system.debug('=======res=========='+String.valueOf(chrgResult.get('description')));
                } 
               update(records);
            }  
        }
        catch (Exception e) {         
            System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber() );           
        }
    } 
    global void finish(Database.BatchableContext BC){    
    }
}