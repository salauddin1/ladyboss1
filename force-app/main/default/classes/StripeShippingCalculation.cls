@RestResource(urlMapping='/Stripe_ShippingService')
global class StripeShippingCalculation {
    @HttpPost
    global static void calculateShipping() {
        String CustomerIde;
        try{
            if(RestContext.request.requestBody!=null){
                String str                              =   RestContext.request.requestBody.toString();
                System.debug('-------res----'+str);
                Map<String, Object> results             =   (Map<String, Object>)JSON.deserializeUntyped(str);
                Map<String, Object> order = (Map<String, Object> )results.get('order');
                Map<String, Object> customers = (Map<String, Object> )order.get('customer');
                List<Object> items          = (List<Object>)order.get('items');
                List<id> prodIdList = new List<id>();
                List<String> strProd = new List<String>();
                for(Object item: items){
                    Map<String, Object>  itemMap       =   (Map<String, Object>)item;
                    Map<String,Object> parent = (Map<String, Object>)itemMap.get('parent');
                    Map<String,Object> product                         =  (Map<String, Object>)parent.get('product');
                    String productID =String.valueOf(product.get('id'));
                    strProd.add(productID);
                }
                string customer=String.valueOf(customers.get('id'));
                Stripe_Profile__c sp = [SELECT Customer__c FROM Stripe_Profile__c WHERE Stripe_Customer_Id__c= :customer Limit 1];
                Decimal shippingCost = 0;
                for(Shipping_Cost_Table__c sc:[SELECT Shipping_Cost__c FROM Shipping_Cost_Table__c WHERE Customer_Id__c=:sp.Customer__c AND Stripe_Product_Id__c IN :strProd]){
                    shippingCost+=sc.Shipping_Cost__c;
                }
                RestContext.response.addHeader('Content-Type', 'application/json');
                Integer shippCost = Integer.valueOf(shippingCost*100);
                RestContext.response.responseBody = Blob.valueOf('{ "order_update": { "order_id": "'+order.get('id')+'",  "shipping_methods": [ { "id": "Dynamic_shipping", "description": "Shipping costs", "amount": '+shippCost+', "currency": "usd"}]}}');
            }else{
                RestContext.response.responseBody = Blob.valueOf('{ "order_update": { "order_id": "",  "shipping_methods": [ { "id": "Dynamic_shipping", "description": "Shipping costs", "amount": 0, "currency": "usd"}]}}');
            }
        }catch(Exception e){
            RestContext.response.responseBody = Blob.valueOf('{ "order_update": { "order_id": "",  "shipping_methods": [ { "id": "Dynamic_shipping", "description": "Shipping costs", "amount": 0, "currency": "usd"}]}}');
            System.debug('Error: '+e.getMessage());
        }
    }
}