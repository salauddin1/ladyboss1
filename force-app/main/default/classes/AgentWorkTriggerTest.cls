@isTest
private class AgentWorkTriggerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        contact con = new contact();
        con.lastName='test';
        insert con;
        
        Case caseRec = new Case();
        caseRec.Subject = 'testCase';
        caseRec.Description = 'TTB0333333';
        caseRec.OwnerId =  UserInfo.getUserId(); 
        caseRec.Status = 'Received';
        insert caseRec;
        
        ID SChannelId= [SELECT Id FROM ServiceChannel Where DeveloperName='Test_omni_service' Limit 1].Id;
        User user1 = new User();
        user1.id = UserInfo.getUserId();
        System.runAs(user1){  
            AgentWork awork = new AgentWork();
            awork.UserId = UserInfo.getUserId();
            awork.ServiceChannelId = SChannelId;
            
            awork.WorkItemId = caseRec.Id;
            insert awork;
            Test.startTest();
            update awork;
            Test.stopTest();
        }
    
    }
}