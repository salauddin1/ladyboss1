@isTest
public class CoachingFinalResultContTest {

   public static testMethod void test(){
              
        Test.startTest();
        Case cas = new Case(Status ='New',Transfer__c=false, Priority = 'Medium', Origin = 'Email', Reason='new');
        insert cas;
        CoachingFinalResultCont.getCaseRecord(cas.id);
        CoachingFinalResultCont.saveCase(cas,'true','Transfer__c');
        CoachingFinalResultCont.saveCase(cas,'true','Cancel__c');
        CoachingFinalResultCont.saveCase(cas,'true','Recovered__c');
        CoachingFinalResultCont.saveCase(cas,'true','Not_recovered__c');
        Test.stopTest();

    }
}