@isTest
public class MoveTaskToNoAnswer_Test {
    @isTest
    public static void test() {
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Donni';
        t.Status='Not Started';
        t.Priority='Normal';
        t.ActivityDate = Date.Today().addDays(5);
        insert t; 
        AddDaysToDueDate__c askd = new AddDaysToDueDate__c();
        askd.Name ='AddDaysToDueDateOfTask';
        askd.Facebook_Message__c = 1;
        askd.Saved_Payment__c = 1;
        askd.SF_Email__c = 1;
        askd.SKA_Video__c = 1;
        askd.Stop_Correspondence__c = 1;
        askd.Stunning_Email_Sent__c = 1;
        askd.Text_Message__c = 1;
        insert askd;
        test.startTest();
        List<Task> taskList = new List<Task>();
        taskList = [Select Id,Owner.Name, Subject,Status,No_Answer__c,Closed_Sale__c,Do_Not_Call__c,Reordered_Online__c From Task];
        MoveTaskToNoAnswer cb = new MoveTaskToNoAnswer();
        Database.executeBatch(cb,200);
        cb.execute(null, taskList);
        String sch = '0 5 * * * ?';
        system.schedule('Test Territory Check', sch, cb);
        test.stopTest();
    }
}