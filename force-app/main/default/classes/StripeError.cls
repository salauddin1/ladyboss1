global class StripeError {
    global String stripeType;
    global String param;
    global String message;
    global String code;
    global String decline_code;
    
    global static StripeError parse(String json) {
        // rough string replacement to simplify json parsing and avoid Apex reserved words
        json = StripeUtil.cleanJson(json);

        return (StripeError) System.JSON.deserialize(json, StripeError.class);
    }
}