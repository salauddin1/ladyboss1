@isTest
public class OliAfterUpdateTest {
    @isTest
    public static void test1(){
        try{
            Async_Apex_Limit__c ap = new Async_Apex_Limit__c();
            ap.Name = 'Async Limit Left';
            ap.Remaining__c = 0;
            insert ap;
            Id opportunityClubRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
                .get('Subscription').getRecordTypeId();
            Test.setMock(HttpCalloutMock.class, new CustomerCheckMock());
            Contact ct = new Contact();
            ct.lastName = 'tirth test';
            ct.Email = 'tirthtest@gmail.com';
            insert ct;
            
            Case cs = new Case();
            cs.ContactId = ct.id;
            cs.Type = 'Account Recovery';
            insert cs;
            
            Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
            prod.Commissionable_amount__c = 0;
            prod.Price__c = 100;
            prod.Shipping_cost_1__c = 0;
            prod.Shipping_cost_2__c = 0;
            prod.Shipping_cost_3__c = 0;
            prod.Shipping_cost_4__c = 0;
            prod.Shipping_cost_5__c = 0;
            prod.Additional_CLUB_Commissionable_Volume__c = 0;
            insert prod;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 100, IsActive = true);
            insert standardPrice;
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 100, IsActive = true);
            insert customPrice; 
            
            Opportunity op1 = new Opportunity();
            op1.Amount = 100;
            op1.Name = 'opp1';
            op1.Pricebook2Id = customPB.id;
            op1.StageName = 'Closed Won';
            op1.CloseDate = System.today();
            op1.Stripe_Charge_Id__c = 'Strid';
            op1.OwnerId= userinfo.getuserid();
            op1.Contact__c = ct.id;
            op1.created_using__c = 'V6';
            op1.RecordTypeId = opportunityClubRecordTypeId;
            insert op1;
            OpportunityLineItem oL1 = new OpportunityLineItem();
            oL1.OpportunityId = op1.id;
            oL1.TotalPrice = 100;
            oL1.Product2Id = prod.id;
            ol1.refund__c = 0;
            ol1.Dynamic_Item_Count_Cost__c = 0;
            ol1.Product_Number_Count__c = 0;
            oL1.Quantity = 1;
            ol1.Consider_For_Commission__c = true;
            ol1.Balance_Applied_on_Last_Invoice__c = 1.20;
            insert oL1;
            oL1.Add_Tax_To_Subscription__c = true;
            ol1.Status__c = 'InActive';
            ol1.Balance_Applied_on_Last_Invoice__c = 1.21;
            update oL1;
        }catch(Exception e){
            
        }
        
    }
    
}