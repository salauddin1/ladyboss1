@istest
public class StripeUtil_Test{
 static testMethod void testParse() {
     contact con = new contact();
     con.lastName = 'Tes';
     con.Stripe_Customer_Id__c = 'card_1CiJbsBwLSk1v1ohyZkWjSEc';
     insert con;
  card__c cd = new card__c();
  //cd.Name='Test';
  cd.Expiry_Month__c='03';
  cd.Expiry_Year__c='2029';
  cd.Card_Type__c = 'tok_visa';
  cd.contact__c = con.id;
  cd.Credit_Card_Number__c = '564';
  cd.Name_On_Card__c = 'dfsfsdf';
  cd.Billing_Street__c = 'CA';
  cd.Billing_City__c = 'dfg';
  cd.Billing_State_Province__c= 'CA' ;
  cd.Billing_Zip_Postal_Code__c = '2738474';
  cd.Billing_Country__c = 'india';
 cd.Stripe_Card_Id__c='card_1CiJbsBwLSk1v1ohyZkWjSEc';
  //insert cd;
  opportunity op = new opportunity();
  op.Name = 'fgdg';
  op.StageName= 'Open';
  op.CloseDate = Date.Today();
 // insert op;
     Product_Discount__c pd = new Product_Discount__c ();
     pd.Discount_Percentage__c = 23;
     insert pd;
     Address__c ad = new Address__c();
     ad.Shipping_Street__c = 'te';
      ad.Shipping_City__c= 'sdg';
      ad.Primary__c = true;
       ad.Shipping_State_Province__c= 'sdg';
        ad.Shipping_Zip_Postal_Code__c= 'sdg';
         ad.Contact__c= con.id;
         insert ad;
         
           Product2 prod = new Product2(Name = 'Laptop X200', IsActive=true ,Subscription__c=false,
                                     Family = 'Hardware');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        //List<opportunity> lstOpp = new List<opportunity>();
        //lstOpp.add(op);
        //lstwrp.Name = 'test';
        //lstwrp.quantity=1;
       //
    // OrderFormController.insertOpptyItems(lstwrp ,lstOpp,true,true,con.id);
    test.starttest();
    
    StripeAPI.coverage();
StripeUtil.coverage();
     test.stopTest();
     
 }
}