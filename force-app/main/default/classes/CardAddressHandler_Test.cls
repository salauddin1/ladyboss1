@isTest
public class CardAddressHandler_Test { 
    static testMethod void myTest() {  
        ContactTriggerFlag.isContactBatchRunning = true;
        Contact con = new Contact();
        con.LastName ='test';
        con.email='test@TEST.COM';
        insert con;
        Stripe_Profile__c sp = new Stripe_Profile__c();
        sp.Customer__c = con.id;
        sp.Stripe_Customer_Id__c = 'cus_test';
        insert sp;
        Card__c card = new Card__c();
        card.contact__c = con.id;
        card.Stripe_Card_id__c = 'card_iuouo';
        card.Stripe_Profile__c = sp.id;
        insert card;
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',Stripe_Plan_Id__c='12345');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        opportunity op  = new opportunity();
        
        op.name='op';
        op.Contact__c =con.id;
        op.Card__c =card.id;
        op.StageName = 'Customer Won';
        op.Amount = 3000;
        op.CloseDate = System.today();
        insert op;
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem ol = new OpportunityLineItem();
        
        ol.Product2Id =prod.id;
        ol.OpportunityId = op.Id;
        ol.Quantity = 1;
        ol.UnitPrice = 2.00;
        ol.PricebookEntryId = customPrice.Id;
        ol.Status__c = 'Active';
        insert ol;
        Address__c ad = new Address__c();
        ad.contact__c = con.id;
        ad.Primary__c =true;
        
        insert ad;
        test.startTest();
        CardAddressHandler.setCardAddress(ad);
        test.stopTest();
     }


}