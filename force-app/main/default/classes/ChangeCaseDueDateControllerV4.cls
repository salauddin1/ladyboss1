//This class used to updatethe case from the component 
public class ChangeCaseDueDateControllerV4 {
    //To stop the recursive call
    public static boolean RunOnce =true;
    //Method to get the case record
    @AuraEnabled
    public static case getCaseRecord(Id caseId){
        //List<case> subsequentList = new List<case>();
        case subsequentList = [select id,Status,Facebook_Message__c,C1PM__c,C2PM__c,C3PM__c,C4PM__c,C5PM__c,Called__c,Call_1_AM__c,Call_2_AM__c,Call_3_AM__c,Call_4_AM__c,Call_5_AM__c,Do_Not_Contact__c,Never_Answered__c,Recovered_Payment__c,Not_recovered__c,Saved_Payment__c,ContactId,DueDate__c, Recovered_Immediate_Payment__c , Recovered_Scheduled_Payment__c ,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Text_Message__c from Case Where id=:caseId Limit 1];
        return subsequentList;
    }  
    //To show the dynamic fields on the component
    @AuraEnabled public static Map<Decimal,MoveCaseDueDate__c> getCallPMApiNames() {
        //Get the custom setting move due date
        List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,Index__c,Column__c,isCaseClosed__c,isDueDateChange__c,Number_Of_Days_To_Move__c from MoveCaseDueDate__c];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        return indexMap;
    }
    //To update the case based on the field checked on the component
    public static Case updateCaseStatusClosed(Case cas,String AddRemoveDays,String fieldNameForDays){
        System.debug('task : '+cas);
        System.debug('AddRemoveDays : '+AddRemoveDays);
        if(AddRemoveDays == 'true' && fieldNameForDays == 'Text_Message__c'){
            cas.Never_Answered__c = true; 
            cas.Status = 'Closed';
        }
        else if(AddRemoveDays == 'true' && fieldNameForDays == 'Recovered_Payment__c'){
            cas.Do_Not_Contact__c = true; 
            cas.Status = 'Closed';
        }
        else if(AddRemoveDays == 'true' && fieldNameForDays == 'Saved_Payment__c'){
            cas.No_Response__c = true; 
            cas.Status = 'Closed';
        }
        update cas;
        return cas;
    }
    //To update the case based on the field checked on the component
    @AuraEnabled 
    public static Case saveCase(String casData,String AddRemoveDays,String fieldNameForDays){
        system.debug('===================cas'+casData);
        system.debug('========fieldNameForDays==========='+fieldNameForDays);
        //Get the case record 
        Case cas = [select id,Status,Called__c,C1PM__c,C2PM__c,C3PM__c,C4PM__c,C5PM__c,Call_1_AM__c,Call_2_AM__c,Call_3_AM__c,Call_4_AM__c,Call_5_AM__c,Do_Not_Contact__c,Not_recovered__c,Never_Answered__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Facebook_Message__c,Text_Message__c from Case Where id=:casData Limit 1];
        
        List<MoveCaseDueDate__c> mcddList = [select id,Field_Api_Name__c,Index__c,isCaseClosed__c,isDueDateChange__c,Number_Of_Days_To_Move__c from MoveCaseDueDate__c];
        Map<Decimal,MoveCaseDueDate__c> indexMap = new Map<Decimal,MoveCaseDueDate__c>();
        Set<String> objectFields = Schema.SObjectType.Case.fields.getMap().keySet();
        for(MoveCaseDueDate__c mcdd : mcddList){
            if(objectFields.contains(mcdd.Field_Api_Name__c.tolowercase())) {
                indexMap.put(mcdd.Index__c,mcdd);
            }
        }
        //AddDaysToDueDate__c myCustomSetting = AddDaysToDueDate__c.getInstance( 'AddDaysToDueDateOfCase' );
        
        
        List<String> callList = new List<String>();
        for(Integer k =1; k <= indexMap.size();k++){
            callList.add(indexMap.get(k).Field_Api_Name__c);
            System.debug(callList);
        }
        //Loop over the list of custom setting fields
       for(String call : callList) {
           //If the field clicked matched with the custom setting field API name then enters into the block
            if(fieldNameForDays == call){
                
               /* if(mcddList.size() > 0){
                    Decimal theFieldValue = (Decimal) myCustomSetting.get(fieldNameForDays);
                    System.debug('theFieldValue : '+theFieldValue);
                    System.debug('AddRemoveDays : '+AddRemoveDays);*/
                    for(Integer i=1; i <= indexMap.size();i++){
                        //If clicked is true and due date of case not equal to null
                        if(AddRemoveDays == 'true' && cas.DueDate__c!=null){
                            //cas.DueDate__c = cas.DueDate__c.addDays(integer.valueof(theFieldValue));
                            if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && indexMap.get(i).Field_Api_Name__c == fieldNameForDays ){
                               if(indexMap.get(i).isDueDateChange__c ==true){
                                //Adds the due date
                                cas.DueDate__c = cas.DueDate__c + (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                                }
                                //to close the case
                                cas.put(fieldNameForDays , true);
                                if(indexMap.get(i).isCaseClosed__c==true){
                                cas.status= 'Closed';
                                }
                            }
                            
                        }else if(AddRemoveDays == 'false' && cas.DueDate__c!=null){
                            if(indexMap.containsKey(i) && indexMap.get(i).Field_Api_Name__c != null && indexMap.get(i).Field_Api_Name__c != '' && indexMap.get(i).Field_Api_Name__c == fieldNameForDays ){
                                 if(indexMap.get(i).isDueDateChange__c ==true){
                                //To negate the due date
                                cas.DueDate__c = cas.DueDate__c - (Integer)indexMap.get(i).Number_Of_Days_To_Move__c;
                                }
                                cas.put(fieldNameForDays ,false);
                            }
                        //}
                    }
                }
            } 
            
        }
         if(fieldNameForDays == 'Recovered_Payment__c'){
            recoveredPayment(cas, AddRemoveDays);
        }
        if(fieldNameForDays == 'Stop_Correspondence__c'){
            updateContact(cas, AddRemoveDays);
        }
        if(fieldNameForDays == 'Saved_Payment__c'){
            //updateCaseStatus(cas, AddRemoveDays);
        }
        if(!test.isRunningtest()){
        ChangeCaseDueDateControllerV4.RunOnce=false;
        }
        //Update the case
        upsert cas;
        
        return cas;
    }  
    //Updates the contact of DNC
    public static void updateContact(Case cas,String flag){
        System.debug('contactId is : '+cas.ContactId);
        System.debug('isTrue : '+flag);
        Boolean value = false;
        if(flag=='true'){
            value = true;
            
        }
        //SOQL for the contact from the contact id in the case
        List<Contact> ctList = [SELECT Id,DoNotCall,Other_Phone_National_DNC__c,Phone_National_DNC__c FROM Contact WHERE Id = : cas.ContactId];
        ctList.get(0).DoNotCall = value; 
        ctList.get(0).Other_Phone_National_DNC__c = value; 
        ctList.get(0).Phone_National_DNC__c = value; 
        update ctList;
    }
    //To update the status of the cacse
    public static void updateCaseStatus(Case cas,String flag){
        System.debug('task : '+cas);
        System.debug('flag : '+flag);
        if(flag == 'true'){
            cas.Status = 'Completed';
        }else{
            cas.Status = 'Open';
        }
        update cas;
    }
    //To update the due date as today 
    public static void recoveredPayment(Case cas,String flag){
        if(flag == 'true'){
            cas.DueDate__c = System.today();
        }
        update cas;
    }
    //Get the case details
    @AuraEnabled
    public static case getCase(Id caseId){
        Case caseOb = [select id,Status,Called__c,C1PM__c,C2PM__c,C3PM__c,C4PM__c,C5PM__c,Call_1_AM__c,Call_2_AM__c,Call_3_AM__c,Call_4_AM__c,Call_5_AM__c,Do_Not_Contact__c,Not_recovered__c,Never_Answered__c,Recovered_Payment__c,Saved_Payment__c,ContactId,DueDate__c,SF_Email__c,SKA_Video__c,Stop_Correspondence__c,Stunning_Email_Sent__c,Facebook_Message__c,Text_Message__c from Case Where id=:caseId];
        
        
        return caseOb;
    }  
    //To refresh the component need custom setting with visibility as true
    @AuraEnabled
    public static List<MoveCaseDueDate__c> getfields(){
        List<MoveCaseDueDate__c> mcddList = new  List<MoveCaseDueDate__c>();
        mcddList = [select id,Field_Api_Name__c,Name,Column__c,Index__c,Number_Of_Days_To_Move__c,isCaseClosed__c,isDueDateChange__c from MoveCaseDueDate__c where isVisible__c=true Order by Index__c ASC];
        return mcddList;
    }  
}