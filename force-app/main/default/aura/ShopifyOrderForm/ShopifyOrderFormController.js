({
    doInit : function(component, event, helper) {
        //debugger
        var contactId = component.get("v.recordId");
        var numberOfRows = [];
        var multipleLookup = [];
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        
        
        component.set('v.multipleLookups',multipleLookup);
        
        numberOfRows.push('0');
        component.set('v.numberOfRows', numberOfRows);
        component.set('v.rowIndex', 1);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        component.set("v.newContact.Modify_Date__c",today); 
        var action = component.get("c.getContact");
        action.setParams({ contactId :contactId});
        
        action.setCallback(this, function(response) {
            debugger
            var a = response.getReturnValue();
            component.set("v.contactVal",a);
            debugger
            component.set("v.newContact.Contact__c",contactId);
            component.set("v.newContact.Email__c",a[0].Email);
            component.set("v.newContact.First_Name__c",a[0].FirstName);
            component.set("v.newContact.Last_Name__c",a[0].LastName);
            component.set("v.newContact.phone__c",a[0].Phone);
            if(a[0].Phone === undefined){
            component.set("v.newContact.phone__c",a[0].MobilePhone);
            }
            if(a[0].Addresses__r !== undefined){
                component.set("v.newContact.address1__c",a[0].Addresses__r[0].Shipping_Street__c);
                component.set("v.newContact.city__c",a[0].Addresses__r[0].Shipping_City__c);
                component.set("v.newContact.zip__c",a[0].Addresses__r[0].Shipping_Zip_Postal_Code__c);
                component.set("v.newContact.province__c",a[0].Addresses__r[0].Shipping_State_Province__c);
                component.set("v.newContact.country__c","US");
            }
            component.set("v.newContact.Send_Receipt__c",false);
            component.set("v.newContact.Send_Fulfillment_Receipt__c",false);
            component.set("v.newContact.Financial_Status__c",'Paid');
            component.set("v.newContact.fulfillment_Status__c",'null');
        });
        $A.enqueueAction(action);
        
    },
    remove :function(component, event, helper) {
        debugger
        if(event.target.parentNode.getElementsByClassName("value_span")[0] !== undefined){
            var selectItemList = component.get("v.selectItemLists");
            var removeVal = event.target.parentNode.getElementsByClassName("value_span")[0].innerHTML;
            var newSel = selectItemList.filter(a => a !== removeVal);
            component.set("v.selectItemLists",newSel);
            var multiVal = component.get("v.multipleLookups");
            console.log(JSON.stringify(multiVal));
           // multiVal = multiVal.filter(mulVal => mulVal.selectedItem["text"] !== removeVal);
            multiVal = multiVal.filter(obj => (obj.selectedItem && obj.selectedItem['text'] !== removeVal) || !obj.selectedItem)
            console.log(JSON.stringify(multiVal));
            component.set("v.multipleLookups",multiVal);           
        } 
        else{
            var a = component.get("v.selectItemLists");
            a.push(" ");
            component.set('v.selectItemLists',a);
            event.target.parentNode.remove();
        }
        
        /*if(event.target.parentNode.getElementsByClassName("value_span")[0] !== undefined){
            var selectItemList = component.get("v.selectItemLists");
            var removeVal = event.target.parentNode.getElementsByClassName("value_span")[0].innerHTML;
            var newSel = selectItemList.filter(a => a !== removeVal);
            component.set("v.selectItemLists",newSel);
            var selectItemList1 = component.get("v.selectItemLists");
            if(selectItemList1.length > 0){
                component.set("v.addButton",false);
            }
            else{component.set("v.addButton",true);}
            
        } 
        
        var rowIndex = event.getSource().get("v.value");
        if(rowIndex>0)
            event.target.parentNode.remove();*/
        
    },
    save :function(component, event, helper){
        var validExpense = component.find('orderform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if(validExpense){
            
            var newContact = component.get("v.newContact");
            console.log("Create newContact: " + JSON.stringify(newContact));
            var newContact1 = JSON.stringify(newContact);
            
            var newProduct = component.get("v.newProduct"); 
            console.log("Create newProduct: " + JSON.stringify(newProduct));
            var items = component.get('v.multipleLookups');
            var idListJSON=JSON.stringify(items);
            items.forEach(function(item) {
                console.log(item.selectedItem);
            });
            
            var contactId = component.get("v.recordId");
            var action = component.get("c.CreateShopifyOrders");
            action.setParams({ OrderId:newContact1,
                              listItems :idListJSON});
            
            action.setCallback(this, function(response) {
                debugger
                component.find("overlayLib").notifyClose();                                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: 'Order succeesful created',
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                
                
            });
            $A.enqueueAction(action);
        } 
        
    },
    show: function(component, event, helper) {
        var items = component.get('v.multipleLookups');
        items.forEach(function(item) {
            console.log(item.selectedItem);
        });
    },
    Add : function(component, event, helper) {
        var multipleLookup = component.get("v.multipleLookups");
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        component.set('v.multipleLookups',multipleLookup);
    },
    checkVal:function(component, event, helper) {
        debugger
        var valCh = event.getSource().get("v.checked");
        component.set("v.newContact.Send_Receipt__c",valCh);
    },
    dateF:function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value");
        //String myDate = value.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX');
        //console.log("==============date"+myDate);
    },
    cancel:function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
})