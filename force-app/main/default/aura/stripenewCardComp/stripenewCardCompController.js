({
    doInit : function(component, event, helper) {
        debugger
        var cardDetails = component.get("v.cardDetail");
        if(cardDetails.action == "Update" || cardDetails.action == "true") component.set("v.isUpdate",true);
        else 
            component.set("v.isUpdate",false);        
      
        var action =  component.get("c.getExpireDateValues");        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var result =  response.getReturnValue();
                var months = [];
                var years =[];
                for(var i in result["months"]){
                    if( parseInt(result["months"][i]) == cardDetails.card.exp_month)
                        months.push({label : result["months"][i], value : parseInt(result["months"][i]),selected : true});
                    else
                        months.push({label : result["months"][i], value : parseInt(result["months"][i]) ,selected : false});
                }
                for(var i in result["years"]){
                    if( parseInt(result["years"][i]) == cardDetails.card.exp_year)
                        years.push({label : result["years"][i], value : result["years"][i],selected : true});
                    else
                        years.push({label : result["years"][i], value : result["years"][i],selected : false});
                }
                
                component.set("v.years",years);
                component.set("v.months",months);
                
            } else {
                
            }
        });
        $A.enqueueAction(action);
    },
    handleevent :function(component, event, helper){
        console.log('event param==>'+JSON.stringify(event.getParams("cardInfo")));
        component.set("v.cardDetail",JSON.stringify(event.getParams("cardInfo"))) ;
        
    },
    updateCard : function(component,event,helper){
        debugger
        var cardDetails = component.get("v.cardDetail");
        if($A.util.isUndefined(cardDetails.card.name) || cardDetails.card.name == null || cardDetails.card.name.length ==0 || $A.util.isUndefined(cardDetails.card.address_line1) || cardDetails.card.address_line1 == null || cardDetails.card.address_line1.length ==0 || $A.util.isUndefined(cardDetails.card.address_city) || cardDetails.card.address_city == null || cardDetails.card.address_city.length ==0 || $A.util.isUndefined(cardDetails.card.address_state) || cardDetails.card.address_state == null || cardDetails.card.address_state.length ==0 || $A.util.isUndefined(cardDetails.card.address_country) || cardDetails.card.address_country == null || cardDetails.card.address_country.length ==0 || $A.util.isUndefined(cardDetails.card.address_zip) || cardDetails.card.address_zip == null || cardDetails.card.address_zip.length ==0) {
            component.find("name").focus();
            component.find("name").reportValidity()
            return;
        }
        else{
            component.set("v.isSuccess",true);
        }
        
	component.set("v.isDisabled",true);
        component.set("v.isLoader",true);
        var isUpate = component.get("v.isUpdate");
        var action =  component.get("c.updateCustomerCard");
        //var strParam = helper.getJsonFromUrl().str;//will get test as str
        var keyVal = helper.getJsonFromUrl().key;//will get abc as id
        
        action.setParams({cardId : cardDetails.card.id,
                          customerId : cardDetails.customerId,
                          address_city : cardDetails.card.address_city,
                          address_country : cardDetails.card.address_country,
                          address_state : cardDetails.card.address_state,
                          address_zip : cardDetails.card.address_zip,
                          exp_month : cardDetails.card.exp_month,
                          exp_year : cardDetails.card.exp_year,
                          name : cardDetails.card.name,
                          tokenId : cardDetails.id,
                          address_line1 : cardDetails.card.address_line1,
                          isUpate : isUpate,
                          keyVal:keyVal
                         });
        console.log('----------add--------'+cardDetails.card.address_line1);
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert("Success!!");
               /* component.set("v.isSuccess",true);
                $A.get("e.c:ShowToastEvent")
                .setParams({
                    type: 'success',
                    title: 'Card Details Updated Successfully',
                    description: '',
                    delay: 5000
                })
                .fire();*/
                debugger
                var successMessage = {};
                  component.set("v.isLoader",false);
                if(component.get("v.isUpdate")) successMessage["header"] = "Thank you for updating your card information.";
                else 
                successMessage["header"] = "Thank you for creating your card information.";
                successMessage["subHeader"] = "Your information is safe and secure. Thank you for being a LadyBoss Customer!";
                $A.createComponent(
                    "c:StripeUpdateCard_SuccessMessageForm",{messageMap : successMessage},
                    function(successForm,status,errorMessage){
                        if(status === "SUCCESS"){
                            let cmpBody = component.find("mainComp");
                            cmpBody.set("v.body",successForm);
                        }
                    });                
            } 
        });
        $A.enqueueAction(action);
    },
    updateCard1: function(component, event, helper) {
        console.log("fired");
    },
    nameField : function(component, event, helper) {
        debugger
        var val= event.getSource().get("v.value");
        if(isNaN(val)){
            component.set("v.NameField" , true);
            // alert("plz enter only number");
        }
        debugger       
        
        
        
    },
    
})