({
    onWorkAccepted: function(component, event, helper) {
        console.log("Work accepted.");
        var workItemId = event.getParam('workItemId');
        var workId = event.getParam('workId');
        var action = component.get("c.onAgentWorkAccept");
        action.setParams({
            "workId": workId,
            "workItemId": workItemId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
        });
        $A.enqueueAction(action);
    },
    onStatusChanged: function(component, event, helper) {
        console.log("Status changed.");
        var statusId = event.getParam('statusId');
        var channels = event.getParam('channels');
        var statusName = event.getParam('statusName');
        var statusApiName = event.getParam('statusApiName');
        console.log(statusId);
        console.log(channels);
        console.log(statusName);
        console.log(statusApiName);
    },
    init: function(cmp, evt, hlp) {
        window.setTimeout(
            $A.getCallback(function() {

                var omniAPI = cmp.find("omniToolkit");
                omniAPI.setServicePresenceStatus({
                    statusId: "0N51F0000004CSf"
                }).then(function(result) {
                    console.log('Current statusId is: ' + result.statusId);
                    console.log('Channel list attached to this status is: ' + result.channels);
                }).catch(function(error) {
                    console.log(error);
                });
            }), 8000)
    },
    onLogout: function(component, event, helper) {
        console.log("Logout success.");
        var action = component.get("c.onAgentLogout");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
        });
        $A.enqueueAction(action);
    },
})