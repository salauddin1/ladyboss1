({
	onRender: function (component, event, helper) {
		console.log('after render');
		var canmem = component.get("v.removedMembership");
		var Elements = component.find('prodone');
		var inside = component.get("v.alreadyRunRender");
		if (Elements != undefined && inside == false) {
			component.set("v.alreadyRunRender", true);
			if (Elements.length != undefined) {
				console.log('elememnt if', Elements);
				for (let index = 0; index < canmem.length; index++) {
					for (var i = 0; i < Elements.length; i++) {
						var val = Elements[i].getElement().getAttribute('data-index');
						if (val == canmem[index].Id) {
							$A.util.addClass(Elements[i], "changeMe");
						}
					}
				}
			} else {
				for (let index = 0; index < canmem.length; index++) {
					var val = Elements.getElement().getAttribute('data-index');
					if (val == canmem[index].Id) {
						$A.util.addClass(Elements, "changeMe");
					}
				}

			}
		}
	},
	init: function (component, event, helper) {
		var today = new Date();
		console.log('today',today);
		var recordId = component.get("v.recordId");
		var cartnumber = component.get("v.addedCartProds");
		var cart = component.get("v.CartProducts");
		var canmem = component.get("v.removedMembership");
		var users = JSON.parse(localStorage.getItem(recordId + "_Added") || "[]");
		users.forEach(function (user, index) {
			cart.push(user);
			cartnumber++;
		});
		var prods = JSON.parse(localStorage.getItem(recordId + "_Removed") || "[]");
		prods.forEach(function (prod, index) {
			canmem.push(prod);
			cartnumber++;
		});
		component.set("v.CartProducts", cart);
		component.set("v.removedMembership", canmem);
		component.set("v.addedCartProds", cartnumber);
		component.set("v.Spinner", false);
		var currentprod = [];
		var pastDueprod = [];
		var prod = [];
		console.log("in manage subscriptions");
		component.set("v.showCard", false);
		component.set("v.verifyDetails", false);
		component.set("v.prodidx", "0");
		component.set("v.showtodaytotal", false);
		component.set("v.totalToday", 0.0);
		window.addEventListener(
			"message",
			function (event) {
				if (event.origin != "https://ladyboss-support.secure.force.com") {
					return;
				}
				component.set("v.refreshCard", true);
				component.set("v.newCard", false);
			},
			false
		);
		var action = component.get("c.getProducts");
		action.setParams({
			conID: recordId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			window.scrollTo(0, 0);
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				var subtotal = 0.0;
				var cancelled = true;
				var purchased = false;
				if (result.length == 0) {
					component.set("v.isSelected", true);
					component.set("v.isEmptyList", true);
				} else {
					for (var i = 0; i < result.length; i++) {
						if (result[i].Name.includes("BURN")) {
							result[i].image = "BURN.png";
						} else if (result[i].Name.includes("REST")) {
							result[i].image = "REST.png";
						} else if (result[i].Name.includes("RECOVER")) {
							result[i].image = "RECOVER.png";
						} else if (result[i].Name.includes("FUEL")) {
							result[i].image = "FUEL.png";
						} else if (result[i].Name.includes("LEAN 1")) {
							result[i].image = "LEAN1.png";
						} else if (result[i].Name.includes("LEAN 2")) {
							result[i].image = "LEAN2.png";
						} else if (result[i].Name.includes("LEAN 3")) {
							result[i].image = "LEAN3.png";
						} else if (result[i].Name.includes("GREENS-1")) {
							result[i].image = "GREENS1.png";
						} else if (result[i].Name.includes("GREENS-2")) {
							result[i].image = "GREENS2.png";
						} else if (result[i].Name.includes("GREENS-3")) {
							result[i].image = "GREENS3.png";
						}

						if (result[i].Name.length > 26) {
							result[i].Names = result[i].Name.substr(0, 26) + "...";
						} else {
							result[i].Names = result[i].Name;
						}
						if (result[i].subStatus == 'Past Due') {
							pastDueprod.push(result[i]);
						} else {
							prod.push(result[i]);
							subtotal += result[i].totalPrice;
						}
						currentprod.push(result[i]);
					}
					for (var i = 0; i < cart.length; i++) {
						var inside = false;
						for (var j = 0; j < result.length; j++) {
							if (cart[i].planId == result[j].planId) {
								purchased = true;
								inside = true;
							}
						}
						if (inside == true) {
							break;
						}
					}
					if (canmem.length == 0) {
						cancelled = false;
					} else {
						for (var i = 0; i < canmem.length; i++) {
							var inside = false;
							for (var j = 0; j < result.length; j++) {
								if (canmem[i].planId == result[j].planId) {
									cancelled = false;
									inside = true;
								}
							}
							if (inside == false) {
								break;
							}
						}
					}
				}
				component.set("v.OppLineItem", prod);
				component.set("v.PastDueList", pastDueprod);
				component.set("v.total", subtotal.toFixed(2));
				component.set("v.addedMemTotal", subtotal.toFixed(2));
				component.set("v.productOne", currentprod);
				if (cancelled == true || purchased == true) {
					console.log('cancelled=' + cancelled + ' purchased=' + purchased);
					canmem = [];
					cart = [];
					cartnumber = 0;
					component.set("v.addedCartProds", cartnumber);
					component.set("v.removedMembership", canmem);
					component.set("v.CartProducts", cart);
					localStorage.removeItem(recordId + "_Removed");
					localStorage.removeItem(recordId + "_Added");
				}
				helper.doInit(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},
	cancelOpp: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		localStorage.removeItem(recordId + "_Removed");
		localStorage.removeItem(recordId + "_Added");
		var a = component.get("c.closemodal");
		$A.enqueueAction(a);
		console.log("First step;- fire event");
		component.set("v.Spinner", true);
		var refreshevent = $A.get("e.c:RefreshSubscriptions");
		refreshevent.setParams({
			data: "CancelSubscriptionFinal"
		});
		refreshevent.fire();
	},
	removeRow: function (component, event, helper) {
		var recordId = component.get("v.recordId");

		var idx = event.target.getAttribute("data-index");
		var nameid;
		var Elements = component.find('prodone');

		if (Elements.length != undefined) {
			for (var i = 0; i < Elements.length; i++) {
				var val = Elements[i].getElement().getAttribute('data-index');
				console.log('inside', val + ' ' + idx);
				if (val == idx) {
					$A.util.addClass(Elements[i], "changeMe");
				}
			}
		} else {
			var val = Elements.getElement().getAttribute('data-index');
			console.log('inside', val + ' ' + idx);
			if (val == idx) {
				$A.util.addClass(Elements, "changeMe");
			}
		}

		var canmem = component.get("v.removedMembership");
		var opplineitem = component.get("v.OppLineItem");
		var prods = component.get("v.Allproducts");
		var planid;
		var nextdate;
		for (var i = 0; i < opplineitem.length; i++) {
			if (idx == opplineitem[i].Id) {
				planid = opplineitem[i].planId;
				nextdate = opplineitem[i].nextChargeDate;
				canmem.push(opplineitem[i]);
				nameid = opplineitem[i].Names;
			}
		}
		component.set("v.removedMembership", canmem);
		localStorage.setItem(recordId + "_Removed", JSON.stringify(canmem));
		component.set("v.OppLineItem", opplineitem);
		component.set("v.Allproducts", prods);
		var prodone = component.get("v.productOne");
		console.log("remove canclled", prodone);
		var cartnumber = component.get("v.addedCartProds");
		component.set("v.addedCartProds", cartnumber + 1);
	},
	closeModel: function (component, event, helper) {
		$A.util.addClass(component.find("toastModel"), "slds-hide");
	},
	finalizeMembership: function (component, event, helper) {
		component.set("v.Spinner", true);
		var cmpTarget = component.find(component.get("v.selectedModal"));
		var cmpBack = component.find("ModalbackdropCancel");
		$A.util.removeClass(cmpBack, "slds-backdrop--open");
		$A.util.removeClass(cmpTarget, "slds-fade-in-open");
		var recordId = component.get("v.recordId");
		var card = component.get("v.cardList")[component.get("v.cardidx")];
		console.log("finalizeMembership ", card.Stripe_Card_Id__c);
		var cancelprods = component.get("v.removedMembership");
		var addprods = component.get("v.CartProducts");
		var action = component.get("c.createPayment");
		action.setParams({
			canceledProds: JSON.stringify(cancelprods),
			addedProds: JSON.stringify(addprods),
			cardId: card.Stripe_Card_Id__c
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			var msg =
				"Unknown Error While personalizing your Memberships. Try again Later or Contact Support.";
			var msgType = "error";
			console.log("finalizeMembership ", state);

			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				console.log("finalizeMembership ", result);
				window.scrollTo(0, 0);
				console.log("First step;- fire event");

				if (result == "success") {
					localStorage.removeItem(recordId + "_Removed");
					localStorage.removeItem(recordId + "_Added");
					var refreshevent = $A.get("e.c:RefreshSubscriptions");
					refreshevent.setParams({
						data: "ShowSubscriptions"
					});
					refreshevent.fire();
				} else {
					msg = result;
					component.set("v.message", msg);
					component.set("v.messageType", msgType);
					$A.util.removeClass(component.find("toastModel"), "slds-hide");
					$A.util.addClass(component.find("toastModel"), "slds-show");
					component.set("v.Spinner", false);
					setTimeout(function () {
						$A.util.addClass(component.find("toastModel"), "slds-hide");
						component.set("v.message", "");
						component.set("v.messageType", "");
					}, 5000);
				}
			} else {
				component.set("v.message", msg);
				component.set("v.messageType", msgType);
				$A.util.removeClass(component.find("toastModel"), "slds-hide");
				$A.util.addClass(component.find("toastModel"), "slds-show");
				component.set("v.Spinner", false);
				setTimeout(function () {
					$A.util.addClass(component.find("toastModel"), "slds-hide");
					component.set("v.message", "");
					component.set("v.messageType", "");
				}, 5000);
			}

		});
		$A.enqueueAction(action);
	},
	onSelectCard: function (component, event, helper) {
		console.log("onSelectCard ", component.get("v.cardidx"));
		var card = component.get("v.cardList")[component.get("v.cardidx")];
		component.set("v.cardLast4", card.Last4__c);
	},
	closemodal: function (component, event, helper) {
		component.set("v.selectedServings", false);
		var selectedModal = component.get("v.selectedModal");
		console.log("openModal", selectedModal);
		var cmpTarget = component.find(selectedModal);
		var cmpBack = component.find("Modalbackdrop");
		$A.util.removeClass(cmpBack, "slds-backdrop--open");
		$A.util.removeClass(cmpTarget, "slds-fade-in-open");
	},
	goToMembership: function (component, event, helper) {
		console.log("First step;- fire event");
		component.set("v.Spinner", true);
		var refreshevent = $A.get("e.c:RefreshSubscriptions");
		refreshevent.setParams({
			data: "SubscriptionsPage"
		});
		refreshevent.fire();
	},
	getCards: function (component, event, helper) {
		helper.getCard(component, event, helper);
		component.set("v.refreshCard", false);
	},
	handleClick: function (component, event, helper) {
		var idx = event.target.getAttribute("data-index");
		var alpro = component.get("v.Allproducts");
		var alprods = component.get("v.completeList");
		var switchproduct = false;
		var select = {};
		var inside = false;
		for (var i = 0; i < alpro.length; i++) {
			if (idx == alpro[i].Id) {
				component.set("v.imageName", alpro[i].image);
				component.set("v.imageurl", '/resource/HubOrderFormImages/HubOrderFormImages/ProductImages/' + alpro[i].image);
				select.Id = alpro[i].Id;
				select.planId = alpro[i].planId;
				select.Name = alpro[i].Name;
				select.totalPrice = alpro[i].totalPrice;
				select.originalCost = alpro[i].originalCost;
				select.image = alpro[i].image;
				select.title = alpro[i].title;
				select.description = alpro[i].description;
				component.set("v.currentProduct", select);
				if (alpro[i].incart != undefined && alpro[i].incart == true) {
					component.set("v.alreadyInCart", true);
				} else {
					component.set("v.alreadyInCart", false);
				}
				switchproduct = alpro[i].switchProduct;
				inside = true;
				break;
			}
		}
		if (switchproduct == true) {
			component.set("v.selectedModal", "ProductDetailsForLeanAndGreen");
		} else {
			component.set("v.selectedModal", "ProductDetails");
		}
		helper.openmodal(component, event, helper);
	},
	addToCart: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var cart = component.get("v.CartProducts");
		var selectprod = component.get("v.currentProduct");
		var prods = component.get("v.Allproducts");
		var idx = selectprod.Id;
		var cartnumber = component.get("v.addedCartProds");
		var prodOne = component.get("v.productOne");
		var opplineItem = component.get("v.OppLineItem");
		var alreadyHad = false;
		var planId;
		var numtoremove;
		var canmem = component.get("v.removedMembership");
		Loop1:
		for (var i = 0; i < prods.length; i++) {
			console.log("insidecart", idx + " " + prods[i].Id);
			if (idx == prods[i].Id) {
				prods[i].incart = true;
				planId = prods[i].planId;
				component.set("v.alreadyInCart", true);
				break Loop1;
			}else if(prods[i].switchProduct == true){
				for(var j = 0; j < prods[i].pwList.length; j++){
					if (idx == prods[i].pwList[j].Id) {
						prods[i].incart = true;
						planId = prods[i].pwList[j].planId;
						component.set("v.alreadyInCart", true);
						break Loop1;
					}
				}
			}

		}
		for (var i = 0; i < prodOne.length; i++) {
			
			if (planId == prodOne[i].planId) {
				
		console.log('alreadyHad',prodOne[i].planId + '  '+planId);
				var idaura = prodOne[i].Id;
				alreadyHad = true;
				var Elements = component.find('prodone');
				if (Elements.length != undefined) {
					for (var i = 0; i < Elements.length; i++) {
						var val = Elements[i].getElement().getAttribute('data-index');
						console.log('inside', val + ' ' + idaura);
						if (val == idaura) {
							$A.util.removeClass(Elements[i], "changeMe");
						}
					}
				} else {
					var val = Elements.getElement().getAttribute('data-index');
					console.log('inside', val + ' ' + idaura);
					if (val == idaura) {
						$A.util.removeClass(Elements, "changeMe");
					}
				}
				break;
			}

		}
		for (var i = 0; i < canmem.length; i++) {
			if (planId == canmem[i].planId) {
				numtoremove = i;
				cartnumber--;
				break;
			}
		}
		if (numtoremove != undefined) {
			var k = canmem.splice(numtoremove, 1);
			localStorage.setItem(recordId + "_Removed", JSON.stringify(canmem));
		}else {
			cart.push(selectprod);
			cartnumber++;
			localStorage.setItem(recordId + "_Added", JSON.stringify(cart));
			component.set("v.CartProducts", cart);
		}
		
		component.set("v.Allproducts", prods);
		component.set("v.addedCartProds", cartnumber);
		component.set("v.removedMembership", canmem);
		var users = JSON.parse(localStorage.getItem(recordId + "_Added") || "[]");
		console.log("# of users: " + users.length);
		users.forEach(function (user, index) {
			console.log("[" + index + "]: " + user.Name);
		});
	},
	openModalCart: function (component, event, helper) {
		var canmem = component.get("v.removedMembership");
		var opplineitem = component.get("v.OppLineItem");
		var CartProducts = component.get("v.CartProducts");
		component.set("v.showCancel", canmem.length);
		component.set("v.showAdded", CartProducts.length);
		component.set("v.showCurrent", opplineitem.length);
		if (opplineitem.length == canmem.length && CartProducts.length == 0 && canmem.length != 0) {
			component.set("v.selectedModal", "ModalboxCancel");
			helper.openmodal(component, event, helper);
		} else if (CartProducts.length == 0 && canmem.length == 0) {
			component.set("v.selectedModal", "ModalboxNoChange");
			helper.openmodal(component, event, helper);
		} else {
			helper.setCartTotal(component, event, helper);
			component.set("v.selectedModal", "MyCart");
			helper.openmodal(component, event, helper);
		}
	},
	removeProdCart: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var numtoremove;
		var idx = event.target.getAttribute("data-index");
		var cartnumber = component.get("v.addedCartProds");
		var cart = component.get("v.CartProducts");
		var prods = component.get("v.Allproducts");
		for (var i = 0; i < prods.length; i++) {
			if (idx == prods[i].Id) {
				prods[i].incart = false;
				cartnumber--;
				break;
			}
		}
		for (var i = 0; i < cart.length; i++) {
			if (idx == cart[i].Id) {
				numtoremove = i;
				break;
			}
		}
		if (numtoremove != undefined) {
			var k = cart.splice(numtoremove, 1);
		}
		console.log(k);
		localStorage.setItem(recordId + "_Added", JSON.stringify(cart));
		component.set("v.addedCartProds", cartnumber);
		component.set("v.Allproducts", prods);
		component.set("v.CartProducts", cart);
		component.set("v.showAdded", cart.length);
		helper.setCartTotal(component, event, helper);
	},
	removeCancelled: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var idx = event.target.getAttribute("data-index");
		var numtoremove;
		var Elements = component.find('prodone');
		var prods = component.get("v.Allproducts");
		var planid;
		if (Elements.length != undefined) {
			for (var i = 0; i < Elements.length; i++) {
				var val = Elements[i].getElement().getAttribute('data-index');
				console.log('inside', val + ' ' + idx);
				if (val == idx) {
					$A.util.removeClass(Elements[i], "changeMe");
				}
			}
		} else {
			var val = Elements.getElement().getAttribute('data-index');
			console.log('inside', val + ' ' + idx);
			if (val == idx) {
				$A.util.removeClass(Elements, "changeMe");
			}
		}
		var canmem = component.get("v.removedMembership");
		var opplineitem = component.get("v.OppLineItem");
		var prodone = component.get("v.productOne");
		console.log('remove canclled', prodone + ' ' + idx);
		/*for (var i = 0; i < prodone.length; i++) {
		    if(idx == prodone[i].Id){
		        opplineitem.push(prodone[i]);
		        break;
		    }
		}*/
		for (var i = 0; i < canmem.length; i++) {
			if (idx == canmem[i].Id) {
				numtoremove = i;
				planid = canmem[i].planId;
				break;
			}
		}
		if (numtoremove != undefined) {
			var k = canmem.splice(numtoremove, 1);
		}
		console.log(k);
		component.set("v.OppLineItem", opplineitem);
		component.set("v.removedMembership", canmem);
		component.set("v.showCancel", canmem.length);
		component.set("v.showCurrent", opplineitem.length);
		var cartnumber = component.get("v.addedCartProds");
		component.set("v.addedCartProds", cartnumber - 1);
		localStorage.setItem(recordId + "_Removed", JSON.stringify(canmem));
		helper.setCartTotal(component, event, helper);
	},
	selectLean: function (component, event, helper) {
		var idx = event.target.id;
		var select = component.get("v.currentProduct");
		var prodid = select.Id;
		var newid = '';
		console.log('hello1', prodid + '  ' + idx);
		Loop1:
			for (var i = 0; i < component.get("v.Allproducts").length; i++) {
				if (prodid == component.get("v.Allproducts")[i].Id) {
					for (var j = 0; j < component.get("v.Allproducts")[i].pwList.length; j++) {
						if (idx == component.get("v.Allproducts")[i].pwList[j].numOfBags) {

							console.log('hello2', component.get("v.Allproducts")[i].pwList[j].numOfBags);
							var orcost = component.get("v.Allproducts")[i].originalCost * idx;
							component.set("v.imageName", component.get("v.Allproducts")[i].pwList[j].image);
							component.set("v.imageurl", '/resource/HubOrderFormImages/HubOrderFormImages/ProductImages/' + component.get("v.Allproducts")[i].pwList[j].image);
							select.Id = component.get("v.Allproducts")[i].pwList[j].Id;
							select.planId = component.get("v.Allproducts")[i].pwList[j].planId;
							select.Name = component.get("v.Allproducts")[i].pwList[j].Name;
							select.totalPrice = component.get("v.Allproducts")[i].pwList[j].totalPrice;
							select.originalCost = orcost.toFixed(2);
							select.image = component.get("v.Allproducts")[i].pwList[j].image;
							component.set("v.currentProduct", select);
							component.set("v.selectedServings", true);
							break Loop1;
						}
					}
				} else {
					for (var j = 0; j < component.get("v.Allproducts")[i].pwList.length; j++) {
						if (prodid == component.get("v.Allproducts")[i].pwList[j].Id) {
							newid = component.get("v.Allproducts")[i].Id;
						}
					}
				}
			}
		if (newid != '') {
			Loop2: for (var i = 0; i < component.get("v.Allproducts").length; i++) {
				if (newid == component.get("v.Allproducts")[i].Id) {
					for (var j = 0; j < component.get("v.Allproducts")[i].pwList.length; j++) {
						if (idx == component.get("v.Allproducts")[i].pwList[j].numOfBags) {

							console.log('hello2', component.get("v.Allproducts")[i].pwList[j].numOfBags);
							var orcost = component.get("v.Allproducts")[i].originalCost * idx;
							component.set("v.imageName", component.get("v.Allproducts")[i].pwList[j].image);
							component.set("v.imageurl", '/resource/HubOrderFormImages/HubOrderFormImages/ProductImages/' + component.get("v.Allproducts")[i].pwList[j].image);
							select.Id = component.get("v.Allproducts")[i].pwList[j].Id;
							select.planId = component.get("v.Allproducts")[i].pwList[j].planId;
							select.Name = component.get("v.Allproducts")[i].pwList[j].Name;
							select.totalPrice = component.get("v.Allproducts")[i].pwList[j].totalPrice;
							select.originalCost = orcost.toFixed(2);
							select.image = component.get("v.Allproducts")[i].pwList[j].image;
							component.set("v.currentProduct", select);
							component.set("v.selectedServings", true);
							break Loop2;
						}
					}
				}
			}
		}

	},
	changeImage: function (component, event, helper) {
		var rectype = event.currentTarget;
		var rectypeid = rectype.getAttribute("title");
		var selectprod = component.get("v.currentProduct");
		if (rectypeid == 'Facts') {
			component.set("v.imageurl", '/resource/HubOrderFormImages/HubOrderFormImages/ProductFactsImages/' + selectprod.image);
		} else {
			component.set("v.imageurl", '/resource/HubOrderFormImages/HubOrderFormImages/ProductImages/' + selectprod.image);
		}

	},
	updateCardandRetry: function (component, event, helper) {
		var idx = event.target.getAttribute("data-index");
		var pastDueSubs = component.get("v.PastDueList");
		for(var i = 0 ; i < pastDueSubs.length ; i++){
			if (idx == pastDueSubs[i].Id) {
				component.set("v.custIdValue",pastDueSubs[i].CustomerID);
			}
		}
		component.set("v.oppUpdateCardIdValue",idx);
		component.set("v.isOpen", true);
	},
	openIframe: function (component, event, helper) {
		var menuCustId = component.get("v.custIdValue");
        var oppliValue = component.get("v.oppUpdateCardIdValue");
		var srcVal = 'https://tirthbox-ladyboss-support.cs22.force.com/LadyBossHub/updateCardMembership?id=' + menuCustId + '&opplineIdval=' + oppliValue;
		component.set("v.iframSrcUrlValue", srcVal);
		component.set("v.isOpen", false);
		component.set("v.Opencard", true);
	},
    closeModelCon: function (component, event, helper) {
		component.set("v.isOpen", false);
		component.set("v.Opencard", false);
    },
});