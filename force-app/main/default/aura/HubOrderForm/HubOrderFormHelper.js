({
    doInit: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAllProducts");
        var productOne = component.get("v.productOne");
        var prod = [];
        var cart = component.get("v.CartProducts");
        
		var canmem = component.get("v.removedMembership");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();

                for (var i = 0; i < result.length; i++) {
                    if (result[i].Name.length > 26) {
                        result[i].Names = result[i].Name.substr(0, 26) + "...";
                    } else {
                        result[i].Names = result[i].Name;
                    }
                    for (var j = 0; j < cart.length; j++) {
                        if (result[i].planId == cart[j].planId) {
                            result[i].incart = true;
                        } else if (result[i].switchProduct) {
                            for (var k = 0; k < result[i].pwList.length; k++) {
                                if (result[i].pwList[k].planId == cart[j].planId) {
                                    result[i].incart = true;
                                }
                            }
                        }
                    }
                    
                }
                component.set("v.Allproducts", result);

                this.getCard(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    getCard: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getCard");
        action.setParams({
            conID: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log("get all", result);
                var cardnum = [];
                var cards = [];
                var namecon = '';
                for (var i = 0; i < result.cardList.length; i++) {
                    if (!cardnum.includes(result.cardList[i].Last4__c)) {
                        result.cardList[i].Credit_Card_Last4 =
                            "********" + result.cardList[i].Last4__c;
                        cards.push(result.cardList[i]);
                        cardnum.push(result.cardList[i].Last4__c);
                    }
                    namecon = result.cardList[i].Contact__r.Name;
                }
                if (result.cardList.length > 0) {
                    component.set("v.cardLast4", result.cardList[0].Last4__c);
                }
                //debugger;
                if (result.AddressValue != null) {
                    component.set("v.ShipStreet", result.AddressValue.Shipping_Street__c);
                    component.set("v.ShipCity", result.AddressValue.Shipping_City__c);
                    component.set("v.ShipState", result.AddressValue.Shipping_State_Province__c);
                    component.set("v.ShipZip", result.AddressValue.Shipping_Zip_Postal_Code__c);
                    component.set("v.displayAddress", result.Address);
                }
                console.log("get all", component.get("v.cardLast4") + ' ' + namecon);
                component.set("v.cardList", cards);
                component.set("v.FullName", namecon);
            }
        });
        $A.enqueueAction(action);
    },
    openmodal: function (component, event, helper) {
        var selectedModal = component.get("v.selectedModal");
        console.log("openModal", selectedModal);
        var cmpTarget = component.find(selectedModal);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.addClass(cmpTarget, "slds-fade-in-open");
        $A.util.addClass(cmpBack, "slds-backdrop--open");
    },
    setCartTotal: function (component, event, helper) {

        var cartProducts = component.get("v.CartProducts");
        var recordId = component.get("v.recordId");
        var action = component.get("c.getTax");
        action.setParams({
            conID: recordId,
            Product: JSON.stringify(cartProducts)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                var taxs = 0.00;
                var taxspercentage = 0.00;
                var count = 0;
                for (var i = 0; i < result.length; i++) {
                    taxs += result[i].taxAmount;
                    taxspercentage += result[i].taxpercentage;
                    count++;
                }
                if (taxspercentage != 0) {
                    taxspercentage = taxspercentage / count;
                }
                var opplineitem = component.get("v.OppLineItem");
                var CartProducts = component.get("v.CartProducts");
                var cancelledprod = component.get("v.removedMembership");
                var carttotal = 0.00;
                var addedmemtotal = 0.00;
                for (var i = 0; i < CartProducts.length; i++) {
                    carttotal += CartProducts[i].totalPrice;
                }
                for (var i = 0; i < opplineitem.length; i++) {
                    addedmemtotal += opplineitem[i].totalPrice;
                }
                for (var i = 0; i < cancelledprod.length; i++) {
                    addedmemtotal -= cancelledprod[i].totalPrice;
                }
                if (carttotal != 0) {
                    component.set("v.showtodaytotal", true);
                } else {
                    component.set("v.showtodaytotal", false);
                }
                addedmemtotal = parseFloat(addedmemtotal) + carttotal;
                component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
                component.set("v.totalToday", carttotal.toFixed(2));
                component.set("v.tax", taxs);
                component.set("v.taxPercentage", taxspercentage);
                var taxplusamount = carttotal + taxs;
                component.set("v.totalWithTax", taxplusamount);
                component.set("v.CartProducts", result);
            }
        });
        $A.enqueueAction(action);
    }
});