({
	init : function(component, event, helper) {
        console.log('init');
        var caseId = component.get("v.recordId");
        console.log("caseId---"+caseId);
        
        var action2 = component.get("c.getCaseRecord"); 
        action2.setParams({ caseId :caseId});
        action2.setCallback(this, function(response) {
            var a = response.getReturnValue();
            component.set("v.description",a.Other_Description__c);
            if(a.Other__c == true){
                var cmpTarget = component.find('description');
                $A.util.addClass(cmpTarget, 'slds-show');
                $A.util.removeClass(cmpTarget, 'slds-hide');
            }
            console.log(response.getReturnValue());
            component.set("v.cas", response.getReturnValue());
        });
        $A.enqueueAction(action2);
    },
    
     rollbackOpp: function( cmp, apexAction, params ) {
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action = cmp.get("c."+apexAction+"");
            action.setParams( params );
            action.setCallback( this , function(callbackResult) {
                if(callbackResult.getState()=='SUCCESS') {
                    resolve( callbackResult.getReturnValue() );
                }
                if(callbackResult.getState()=='ERROR') {
                    console.log('ERROR', callbackResult.getError() ); 
                    reject( callbackResult.getError() );
                }
            });
            $A.enqueueAction( action );
        }));            
        return p;
    },
    
    createOrder: function(component, event,cas,AddRemoveDaysval,fieldNameForDaysVal){
        console.log('=----cas==='+JSON.stringify(cas));
        console.log('=----cas==='+AddRemoveDaysval);
        this.rollbackOpp(component,'saveCase',{"cas": cas,AddRemoveDays : AddRemoveDaysval,fieldNameForDays : fieldNameForDaysVal})
        .then(function(result){
            if(result != null){
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result);
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            }
           
             
        });
        
    },

})