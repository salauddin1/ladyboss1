({
    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        
            var action = component.get("c.getArticles");
            // set param to method  
            action.setParams({
                'searchKeyWord': getInputkeyWord
                //'ObjectName' : component.get("v.objectAPIName")
            });
            // set a callBack    
            action.setCallback(this, function(response) {
                $A.util.removeClass(component.find("mySpinner"), "slds-show");
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var storeResponse = response.getReturnValue();
                    // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                    if (storeResponse.length == 0) {
                        component.set("v.Message", 'No Result Found...');
                    } else {
                        component.set("v.Message", '');
                    }
                    // set searchResult list with return value from server.
                    
                                    var map = response.getReturnValue();
                var parsingWarpper=[];
                console.log(Object.keys(map).length);
                
                Object.keys(map).forEach(function(key) {
                    var individualElement = {};
                    individualElement.Id = key;
                    individualElement.Value =[];
                    var innerMapObject =map[key]; 
                    
                    console.log(innerMapObject);
                    Object.keys(innerMapObject).forEach(function(key2) {
                        console.log(key2);
                        var innerIndivididualElement ={};
                        innerIndivididualElement.Key = key2;
                        innerIndivididualElement.Value = innerMapObject[key2];
                        
                        individualElement.Value.push(innerIndivididualElement);
                    });  
                    parsingWarpper.push(individualElement);
                });
                console.log(parsingWarpper);
                component.set("v.myMap",parsingWarpper);
                    
                    component.set("v.listOfSearchRecords", storeResponse);
                }
                
            });
            // enqueue the Action  
            $A.enqueueAction(action);
       
        
    },
})