({
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        
        component.set("v.isOpen", true);
        var getInputkeyWord = component.get("v.SearchKeyWord");
        helper.searchHelper(component,event,getInputkeyWord);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.SearchKeyWord","");
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    
    doInit: function(component,event,helper){
        var getInputkeyWord = component.get("v.SearchKeyWord");
        helper.searchHelper(component,event,getInputkeyWord);
    },
    
    keyPressController : function(component, event, helper) {

        var getInputkeyWord = component.get("v.SearchKeyWord");
        console.log('-->'+JSON.stringify(event.getParams().keyCode));
        if (event.getParams().keyCode === 13) {
            helper.searchHelper(component,event,getInputkeyWord);
        }
      
    },
    
    clear :function(component,event,helper){
        component.set("v.SearchKeyWord","");
        helper.searchHelper(component,event,"");
    },
    
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');

        if (openSections.length === 0) {
            cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    
    
    handleClick : function (component, event, helper) {
        var KBArticle = event.getSource().get("v.value");
        console.log('-->'+JSON.stringify(KBArticle.Id));
        var action = component.get("c.getArticlesTitle");
        action.setParams({
            'KID': KBArticle.Id
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('storeResponse - '+storeResponse);
                if(storeResponse == null){
                    storeResponse='';
                }
                console.log('storeResponse - '+storeResponse);
                var actionAPI = component.find("quickActionAPI");
                var fields = { HtmlBody : {value: storeResponse, insertType: "cursor"} };
                var args = { actionName :"Case.SendEMAIL_New",entityName:"Case",targetFields : fields};
                actionAPI.setActionFieldValues(args).then(function(result) {
                    component.set("v.isOpen", false);
                }).catch(function(e) {
                    if (e.errors) {
                        
                    }
                });
                component.set("v.isOpen", false);
                
                
                
                
            }
            
        });
        $A.enqueueAction(action);
        
        
        
    },
    
    parentAction : function (component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    createAccount: function (component) {
        var KBArticle = component.get('v.knowledgeArticle');
        var actionAPI = component.find("quickActionAPI");
        var fields = { HtmlBody : {value: KBArticle.Answer__c, insertType: "cursor"} };
        var args = { actionName :"Case.SendEMAIL_New",entityName:"Case",targetFields : fields};
        actionAPI.setActionFieldValues(args).then(function(result) {
            console.log('-->'+JSON.stringify(KBArticle));
            console.log('-->'+JSON.stringify(result));
            console.log('-->'+JSON.stringify(KBArticle.Procedure_Audience__c));
            component.set("v.isOpen", false);
        }).catch(function(e) {
            if (e.errors) {
                
            }
        });
        component.set("v.isOpen", false);
    }
})