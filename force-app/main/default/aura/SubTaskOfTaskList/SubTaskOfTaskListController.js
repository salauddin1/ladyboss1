({
    doInit: function(component, event, helper) {
        var action = component.get('c.getSubTask');
        action.setParams({
            caseId : component.get("v.recordId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                console.debug(actionResult.getReturnValue());
                //set response value in ChildRecordList attribute on component.
                component.set('v.ChildRecordList', actionResult.getReturnValue());
                console.log(actionResult.getReturnValue());
            }
        });
        
        var action2 = component.get('c.getSubTaskNote');
        action2.setParams({
            caseId : component.get("v.recordId")
        });
        action2.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                console.debug(actionResult.getReturnValue());
                //set response value in ChildRecordList attribute on component.
                component.set('v.ChildRecordNoteList', actionResult.getReturnValue());
                console.log(actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
        
    },
    
    navigateToOppty:function(component,event){
        // it returns only first value of Id
        var Ide = event.srcElement.id;
        console.log(Ide);
        
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": Ide  ,
            "slideDevName": "detail"
        });
        sObectEvent.fire(); 
    },
    note:function(component,event,helper){
        debugger
        var val = event.target.innerHTML;
        component.set("v.NoteVal",val);

       var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');  
    },
    remove:function(component,event,helper){
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        
    }
})