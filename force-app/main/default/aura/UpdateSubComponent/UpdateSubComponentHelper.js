({
    refreshingsubs: function (component, event, helper) {
        var contId = component.get("v.recordId");
        var action = component.get("c.RefreshSubs");
        action.setParams({
            conId: contId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                console.log('successfully refreshed subs');
                component.set("v.Spinner", false);
            } else {
                console.log(state);
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
     ToastMsg: function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            title: component.get("v.messageType"),
            message: component.get("v.message"),
            duration: ' 2000',
            key: 'info_alt',
            type: component.get("v.messageType"),
            mode: 'pester'
        });
        toastEvent.fire();
    }
})