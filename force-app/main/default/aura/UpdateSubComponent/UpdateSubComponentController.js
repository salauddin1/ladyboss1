({
    doInit: function (component, event, helper) {
        var contId = component.get("v.recordId");
        var action = component.get("c.getEmail");
        action.setParams({
            conId: contId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result && result != null) {
                    component.set("v.ConEmail", result);
                    component.set("v.error", false);
                } else {
                    component.set("v.message", 'No email Associated with This contact');
                    component.set("v.messageType", 'Error');
                    helper.ToastMsg(component, event, helper);
                }
            } else {
                component.set("v.message", 'Error on UpdateSubscriptions Componentt');

                component.set("v.messageType", 'Error');
                helper.ToastMsg(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    updatesub: function (component, event, helper) {
        component.set("v.Spinner", true);
        var contemail = component.get("v.ConEmail");
        var action = component.get("c.mergeContact");
        action.setParams({
            email: contemail
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var conId = response.getReturnValue();
                var contId = component.get("v.recordId");
                console.log('successfully merged contact');
                helper.refreshingsubs(component, event, helper);
                if (conId != contId) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": conId,
                        "slideDevName": "Detail"
                    });
                    navEvt.fire();
                }

            } else {
                console.log(state);
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    }
})