({
    doInit : function(component, event, helper) {
        debugger
        component.set("v.Spinner",true);
        var action = component.get("c.getContact");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue(); 
                if(result.PAP_refid__c!=null){
                    if(result.Coupon_Code_Id__c!=null){
                        component.set("v.couponCode",result.PAP_refid__c);
                        component.set("v.couponCodeId",result.Coupon_Code_Id__c);
                        component.set("v.discountType",result.Coupon_Discount_Type__c);
                        component.set("v.couponAmount",result.Coupon_Amount__c);
                        component.set("v.isCouponExist",true);
                    }
                    else{
                        component.set("v.isCouponExist",false);
                        component.set("v.couponCode",result.PAP_refid__c);
                    }
                    
                }
                else{
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Please first create Affiliate",
                        "type" : "error"
                    });
                    toastEvent.fire(); 
                    component.find("updateCouponLib").notifyClose(); 
                    $A.get('e.force:refreshView').fire();
                }
                
            }else if(state == "ERROR"){
                component.set("v.Spinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": 'Please contact your Administrator',
                    "type" : "error"
                });
                toastEvent.fire(); 
                component.find("updateCouponLib").notifyClose(); 
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    updateDetails : function(component, event, helper) {
        debugger
        component.set("v.Spinner",true);
        var coupCodeId = component.get("v.couponCodeId");
        var coupCode = component.get("v.couponCode");
                        
        var action = component.get("c.updateCouponCode");
        action.setParams({"conId":component.get("v.recordId"),
                          "coupCodeId":coupCodeId,
                          "coupCode":coupCode
                         });
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                component.set("v.Spinner",false);
                var result = responsetogetUser.getReturnValue();  
                if(result != null){
                    var msgs = result;
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": msgs,
                        "type" : "success"
                    });
                    toastEvent.fire(); 
                    component.find("updateCouponLib").notifyClose(); 
                    $A.get('e.force:refreshView').fire();
                }
                else {
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": 'Please contact your Administrator',
                        "type" : "error"
                    });
                    toastEvent.fire(); 
                    component.find("updateCouponLib").notifyClose(); 
                    $A.get('e.force:refreshView').fire();
                }
            }else if(state == "ERROR"){
                component.set("v.Spinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": 'Please contact your Administrator',
                    "type" : "error"
                });
                toastEvent.fire(); 
                component.find("updateCouponLib").notifyClose(); 
                $A.get('e.force:refreshView').fire();
            }
            component.set("v.Spinner",false);
        });
        $A.enqueueAction(action);
    },
    
    createCoupon : function(component, event, helper) {
        debugger
        component.set("v.Spinner",true);
        var coupCode = component.get("v.couponCode");
        var coupAmount = component.get("v.couponAmount");
        var coupDiscountType = component.get("v.discountType");
        
        
        var action = component.get("c.createCouponCode");
        action.setParams({"conId":component.get("v.recordId"),
                          "coupCode":coupCode
                          });
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                component.set("v.Spinner",false);
                var result = responsetogetUser.getReturnValue();  
                if(result != null){
                    var msgs = result;
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": msgs,
                        "type" : "success"
                    });
                    toastEvent.fire(); 
                    component.find("updateCouponLib").notifyClose(); 
                    $A.get('e.force:refreshView').fire();
                }
                else {
                    component.set("v.Spinner",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": 'Please contact your Administrator',
                        "type" : "error"
                    });
                    toastEvent.fire(); 
                    component.find("updateCouponLib").notifyClose(); 
                    $A.get('e.force:refreshView').fire();
                }
            }else if(state == "ERROR"){
                component.set("v.Spinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": 'Please contact your Administrator',
                    "type" : "error"
                });
                toastEvent.fire(); 
                component.find("updateCouponLib").notifyClose(); 
                $A.get('e.force:refreshView').fire();
            }
            component.set("v.Spinner",false);
        });
        $A.enqueueAction(action);
    }
    
    
})