({
    handleShowModal: function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:UpdateCouponCodeFrom", { recordId : component.get("v.recordId") },
                           function(content, status,errorMessage) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('updateCouponLib').showCustomModal({
                                       header: "Create/Update Coupon",
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "slds-modal_large fullwidth",
                                       closeCallback: function() {
                                       }
                                   })
                               }
                           });
    },
})