({
	itemSelected : function(component, event, helper) {
		helper.itemSelected(component, event, helper);
        console.log("-----itemSelected----");
	}, 
    serverCall :  function(component, event, helper) {
		helper.serverCall(component, event, helper);
        console.log("-----serverCall----");
	},
    clearSelection : function(component, event, helper){
        helper.clearSelection(component, event, helper);
        console.log("-----clearSelection----");
    } 
})