({
    doInit : function(component, event, helper){
        debugger
        window.addEventListener("message", $A.getCallback(function(event) {
            debugger
            if(event.data == 'cardProfileSuccess'){
                console.log("========hellloooo========"+event.data);
                component.set("v.openShipping",false);
                /* setTimeout(function() {
                location.reload();
            },3000);*/
                window.scrollTo(0, 0);
                var msg = 'Success! Your card has been updated';
                var msgType = 'success';
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                    window.location.reload();
                }, 10000);
            }
        }), false);
        var recordId = component.get("v.recordId");
        var action   = component.get("c.getcontact");
        action.setParams({ conId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                
                var res = response.getReturnValue();
                console.log(res);
                // component.set("v.ContactWrapper",res);
                component.set("v.messageflag", false);
                if(res != null){
                    component.set("v.ContactWrapper", res);    
                }
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getAddress");
        action1.setParams({ conId : recordId});
        
        action1.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                // component.set("v.AddressWrapper",res);
                component.set("v.messageflag", false);
                if(res.length > 0){
                    if(res.AddressList.Shipping_Zip_Postal_Code__c != null && res.AddressList.Shipping_Zip_Postal_Code__c != ''){
                        var Zipcode = res.AddressList.Shipping_Zip_Postal_Code__c;
                        component.set("v.finalZipCode",Zipcode);
                        var newZipCode = Zipcode.match(/.{1,5}/g).join('-');
                        console.log('-----newZipCode--'+newZipCode);
                        res.AddressList.Shipping_Zip_Postal_Code__c = newZipCode ;
                    }
                    component.set("v.city", res.AddressList.Shipping_City__c);
                    component.set("v.postalcode", res.AddressList.Shipping_Zip_Postal_Code__c);
                    component.set("v.state", res.AddressList.Shipping_State_Province__c);
                    component.set("v.street", res.AddressList.Shipping_Street__c);
                    component.set("v.country", res.AddressList.Shipping_Country__c);    
                }
               else{
                   window.scrollTo(0, 0);
                var msg = 'No Address Found. Plesae Add Address to Update Your Card';
                var msgType = 'error';
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 10000); 
                }
            }
        });
        $A.enqueueAction(action1);
        helper.openModel(component,event,helper);
        // component.set("v.OpenSubscription",true);
        
    },
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    onSelectCard :function(component, event, helper) {
        debugger
        component.set("v.SelectedCard",'');
        component.set("v.isMultipleSub", false);
        component.set("v.isSingleSub", false);
        var cardId = event.currentTarget.name;
        //var cardId = event.getSource().get('v.value');
        // component.set("v.cardCheck",cardId);
        var recordId = component.get("v.recordId");
        
        var cardlst = component.get("v.SelectcardList");
        for(var i = 0 ; i< cardlst.length ; i++){
            if(cardlst[i].cardList[0].Last4__c == cardId){
                if(cardlst[i].cardList.length > 1){
                    component.set("v.isMultipleSub", true);
                    component.set("v.cardSubscriptionList",cardlst[i].cardList);
                    var cardRecords = component.get("v.cardSubscriptionList");
                    var cardIdlist = [];
                    for(var j = 0 ; j< cardRecords.length ; j++){
                        cardIdlist.push(cardRecords[j].Id);
                    }
                    component.set("v.MultipleSubscriptionList",cardIdlist);
                    var cardabc = component.get("v.MultipleSubscriptionList");
                    var action2 = component.get("c.getAllSubscriptionName");
                    action2.setParams({ cardList : cardabc});
                    action2.setCallback(this, function(response) {
                        debugger
                        var state = response.getState();
                        if(state == "SUCCESS") {
                            var res = response.getReturnValue();
                            console.log(res);
                            component.set("v.MultipleSubscriptionList",res);
                        }
                    });
                    $A.enqueueAction(action2);
                }	
                else{
                    component.set("v.cardCheck",cardlst[i].cardList[0].Id);
                    component.set("v.isSingleSub", true);
                    var action2 = component.get("c.getCardSubscription");
                    action2.setParams({ cardId : cardlst[i].cardList[0].Id,
                                       conId : recordId});
                    action2.setCallback(this, function(response) {
                        debugger
                        var selectedContacts = [];
                        var state = response.getState();
                        if(state == "SUCCESS") {
                            var res = response.getReturnValue();
                            console.log(res);
                            component.set("v.SubscriptionList", res[0].subscriptionList);
                            console.log(res);
                            var c = component.get("v.SubscriptionList");
                            if(c.length > 1){
                                component.set("v.multiRadio", true);
                                component.set("v.singleRadio", false);
                            }
                            else{
                                component.set("v.singleRadio", true);
                                component.set("v.multiRadio", false);
                            }
                        }
                    });
                    $A.enqueueAction(action2);
                }
                cardlst[i].SubscriptionOnCard = true;
            }
            else{
                cardlst[i].SubscriptionOnCard = false;
            }
        }
        
        component.set("v.OpenSubscription", false);
        component.set("v.OpenSubscription", true);
        component.set("v.spinner", false);
    },
    
    onMultipleSelectCard :function(component, event, helper) {
        debugger
        var cardId = event.getSource().get('v.value');
        component.set("v.SelectedCard",cardId);
        component.set("v.cardCheck",cardId);
        var recordId = component.get("v.recordId");
        var action2 = component.get("c.getCardSubscription");
        action2.setParams({ cardId : cardId,
                           conId : recordId});
        action2.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.SubscriptionList", res[0].subscriptionList);
                var c = component.get("v.SubscriptionList");
                if(c.length > 1){
                    component.set("v.multiRadio", true);
                    component.set("v.singleRadio", false);
                }
                else{
                    component.set("v.singleRadio", true);
                    component.set("v.multiRadio", false);
                }
                console.log(res);
            }
        });
        $A.enqueueAction(action2);
        component.set("v.spinner", false);
    },
    openShipping : function(component, event, helper) {
        debugger
        component.set("v.OpenSubscription",false);
        component.set("v.openShipping",true);
        var selectedContacts = [];
        component.set("v.isOpen",false);
        var cardCheck = component.get("v.cardCheck");
        if(cardCheck != '' && cardCheck != null){
            var checkvalue = component.find("checkResult")[0].get("v.value");//To get Checkbox Value in mobile
            var cardcheckvalue = component.find("checkboxCard")[0].get("v.value");
            var Deskcheck = component.find("checkResult")[1].get("v.value");//To get Checkbox Value in mobile
            var Deskcheckvalue = component.find("checkboxCard")[1].get("v.value");
            
            if ((checkvalue == true && cardcheckvalue == false)||(Deskcheck == true && Deskcheckvalue == false)) {
                component.set("v.isShipped",true);
                component.set("v.isUpdateCard",false);
            }  else if ((cardcheckvalue == true && checkvalue == true)||(Deskcheckvalue == true && Deskcheck == true)) {
                component.set("v.isShipped",false);
                component.set("v.isUpdateCard",true);
                component.set("v.isShipping",true);
            } else if ((cardcheckvalue == true && checkvalue == false)||(Deskcheckvalue == true && Deskcheck == false)) {
                component.set("v.isShipped",false);
                component.set("v.isUpdateCard",true);
                component.set("v.isShipping",false);
            }  else{
                component.set("v.OpenSubscription",true);
                component.set("v.openShipping",false);
                window.scrollTo(0, 0);
                var msg = 'Check Any Update Option.';
                var msgType = "error";
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
            }
        }
        else{
            component.set("v.OpenSubscription",true);
            component.set("v.openShipping",false);
            window.scrollTo(0, 0);
            var msg = 'Please Select Card';
            var msgType = "error";
            component.set("v.message", msg);
            component.set("v.messageType", msgType);
            $A.util.removeClass(component.find('toastModel'), 'slds-hide');
            $A.util.addClass(component.find('toastModel'), 'slds-show');
            setTimeout(function () {
                $A.util.addClass(component.find('toastModel'), 'slds-hide');
                component.set("v.message", "");
                component.set("v.messageType", "");
            }, 5000);
        }
        
    },
    
    selectoptionvalue: function(component, event, helper) {
        debugger
        var cardCheck = component.get("v.cardCheck");
        var recordId = component.get("v.recordId");
        var Street = component.get("v.street");
        var City = component.get("v.city");
        var State = component.get("v.state");
        var PostalCode = component.get("v.postalcode");
        var Country = component.get("v.country");
        var action   = component.get("c.AddShipping");
        PostalCode = PostalCode.replace('-','');
        PostalCode = PostalCode.replace(' ','');
        action.setParams({ Street : Street,
                          City:City,
                          State:State,
                          PostalCode:PostalCode,
                          Country:Country,
                          recordId : recordId,
                          cardCheck : cardCheck,
                          recordId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.openShipping", false);
                var msg = 'Address Update Successfully';
                var msgType = "success";
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                    window.location.reload();
                }, 5000);
                
            }
            else{
                var msg = 'Address Update Failed...Please Try Again later';
                var msgType = "success";
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
                var delay=70000; //7 seconds
                setTimeout(function() {
                    debugger
                    window.location.reload();
                }, delay);
            }
        });
        $A.enqueueAction(action);
    },
    
    RecordUpdate : function(component, event, helper){ 
        debugger
        var recordId   = component.get("v.recordId");
        component.set("v.conIdvale",recordId);
        var firstname  = component.get("v.ContactWrapper.contactList.FirstName");
        var LastName   = component.get("v.ContactWrapper.contactList.LastName");
        var phone      = component.get("v.ContactWrapper.contactList.Phone");
        var Street     = component.get("v.street");
        var City       = component.get("v.city");
        var state      = component.get("v.state");
        var zipcode    = component.get("v.postalcode");
        var country    = component.get("v.country");
        var action1    = component.get("c.updateInfoUser");
        zipcode = zipcode.replace('-','');
        zipcode = zipcode.replace(' ','');
        action1.setParams({ firstname : firstname,
                           idparam : recordId,
                           lastName:LastName,
                           phone:phone
                           //street:Street,
                           //city:City,
                           //state:state,
                           //zipCode:zipcode,
                           //country:country
                          });
        
        action1.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var message = 'keymMessageFromComp'+firstname+' '+LastName;
                var vfOrigin = "https://" + component.get("v.vfHost1");
                var vfWindow = component.find("vfFrame").getElement().contentWindow;
                vfWindow.postMessage(message, vfOrigin);
                
                var res = response.getReturnValue();
                component.set("v.registerSuccessMsg",res);
                component.set("v.showcomp",false);
                console.log(res);
                
            }
            else{
                
                component.set("v.showcomp",true);
            }
            
        });
        $A.enqueueAction(action1);
        
    },
    
    chkTerms : function(component, event, helper){
        
        var isChecked = component.find("chkTerms").get("v.checked");
        if(isChecked == true){
            component.set("v.showbtn",false);
        }
        else{
            component.set("v.showbtn",true);
        }
    },
    
    phonehandlechange : function(component, event, helper){
        debugger
        var phoneNo = component.find("Phone");
        var phoneNumber = phoneNo.get('v.value');
        var s = (""+phoneNumber).replace(/\D/g, '');
        var m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        var formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        phoneNo.set('v.value',formattedPhone);
        /*  var phone = component.get("v.ContactWrapper.contactList.Phone");
        if(phone.length == 3){
            var newphoneno = phone+'-';
            component.set("v.ContactWrapper.contactList.Phone",newphoneno);
        }
        if(phone.length == 7){
            var newphoneno = phone+'-';
            component.set("v.ContactWrapper.contactList.Phone",newphoneno);
        }
        if(event.KeyCode == '8'){
           var newphoneno = phone.remove('-');
            component.set("v.ContactWrapper.contactList.Phone",newphoneno);
        }*/
    },
    numberValidation : function(component,event,helper) {
        debugger
        console.log('--------number validation----------');
        var phoneNo = component.get("v.postalcode");
        console.log(phoneNo.length);
        console.log(phoneNo.charCodeAt(phoneNo.length-1));
        if(phoneNo.charCodeAt(phoneNo.length-1) >= 48 && phoneNo.charCodeAt(phoneNo.length-1)<=57){
            var val = phoneNo.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            component.set('v.value',val.trim());
        }
        else{
            component.set('v.postalcode',phoneNo.replace(phoneNo.substr(phoneNo.length-1),""));
        }
    },
    Zipcodehandlechange : function(component, event, helper){
        debugger
        var Zip = component.get("v.postalcode");
        if(Zip.length == 5){
            var Newzip = Zip+'-';
            component.set("v.postalcode",Newzip);
        }
    },
    closeAlert: function (component, event, helper) {
        $A.util.addClass(component.find('toastModel'), 'slds-hide');
    },
    closecard:function(component, event, helper) {
        component.set("v.OpenSubscription", false);
        window.location.reload();
    },
    closeShipping :function(component, event, helper) {
        
        component.set("v.openShipping", false);
        window.location.reload();
    },
    backFunction : function(component, event, helper) {
        debugger
        component.set("v.openShipping",false);
        var a = component.get('c.doInit');
        $A.enqueueAction(a);
    }
});