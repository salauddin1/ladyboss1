({
    updatesub: function (component, event, helper) {
        component.set("v.Spinner", true);
        var contemail = component.get("v.ConEmail");
        var action = component.get("c.mergeContact");
        action.setParams({
            email: contemail
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                console.log('successfully merged contact');
                helper.refreshingsubs(component, event, helper);
            } else {
                console.log(state);
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    }
})