({
    refreshingsubs: function (component, event, helper) {
        var contId = component.get("v.recordId");
        var action = component.get("c.RefreshSubs");
        action.setParams({
            conId: contId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                console.log('successfully refreshed subs');
                var refreshevent = $A.get("e.c:RefreshSubscriptions");
                refreshevent.setParams({
                    "data": 'expense'
                });
                refreshevent.fire();
                component.set("v.Spinner", false);
            } else {
                console.log(state);
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    }
})