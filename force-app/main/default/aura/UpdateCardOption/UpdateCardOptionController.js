({
    doInit : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var action = component.get("c.getCards");
        action.setParams({ contactId :contactId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state); 
            debugger
            var result = response.getReturnValue();
            var b =   result.map(record => record.Id);
            component.set("v.cId",b);
            if( result != null && result.length > 0 ) {
                for(var i in result){
                    var credCardNumber =  result[i].Credit_Card_Number__c;
                    if(!$A.util.isUndefined(credCardNumber)) 
                    {
                        credCardNumber= credCardNumber.replace(/ /g,"0");
                        result[i].numMask = credCardNumber.replace(/\d(?=\d{4})/g, "*");
                    }
                }
                var phoneVal ='';
                component.set("v.cardData", result);
                
                if(result[0].Contact__r.Phone != null){
                    phoneVal = result[0].Contact__r.Phone;   
                }
                else if(result[0].Contact__r.MobilePhone != null){
                    phoneVal = result[0].Contact__r.MobilePhone;   
                }
                    else if(result[0].Contact__r.OtherPhone !=- null){
                        phoneVal = result[0].Contact__r.OtherPhone;   
                    }
                        else{
                            phoneVal ='';
                        }
                
                if(phoneVal !='' && phoneVal != undefined) {
                    if(phoneVal.includes('(')){
                        var phone1 = phoneVal.replace('(','');
                        var phone2 = phone1.replace(')','');
                        var phone3 = phone2.replace('-','');
                        var phone4 = phone3.replace(' ','');
                        component.set("v.phone",phone4);
                    }
                    else{
                        component.set("v.phone",phoneVal);  
                    }
                }
            }
            console.log("-------------------------------=============="+response.getReturnValue());
        });
        var action1 = component.get("c.getCardsOpportunity");
        action1.setParams({ contactId :contactId});
        action1.setCallback(this, function(response) {
            debugger    
            var state = response.getState();
            console.log("Failed with state: " + state);            
            var result1 = response.getReturnValue();
            component.set("v.oppData",result1);
            
            
            /*ar b =   result1.map(record => record.Card__c);
            component.set("v.cardOppId",b);
           
            var cardId = component.get("v.cId");
        var oppCardId = component.get("v.cardOppId");
        for(var i = 0 ; i <= cardId.length; i++){
			debugger
            for(var j=0; j <= oppCardId.length; j++){
                if(cardId[i] == oppCardId[j]){
                    component.set("v.comma",false);
                }
            }
        }*/
            
        });
        var action2 = component.get("c.getCardsOpportunitySub");
        action2.setParams({ contactId :contactId});
        action2.setCallback(this, function(response) {
            debugger    
            var state = response.getState();
            console.log("Failed with state: " + state);            
            var result2 = response.getReturnValue();
            component.set("v.oppSubData",result2);
            
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        $A.enqueueAction(action2);
        //debugger
        
    },
    save : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var cardId =  component.get("v.cardId");
        var emailId = component.get("v.emailId");
        var phoneNum = component.get("v.phoneNum");
        var customerId = component.get("v.customerId");
        var last4Digit = component.get("v.last4Digit");
        component.set("v.loaded",true);
        console.log("-----cardId--------"+cardId);
        console.log("-----emailId--------"+emailId);
        console.log("-----customerId--------"+customerId);
        console.log("-----last4Digit--------"+last4Digit);
        
        var action = component.get("c.sendEmail");
        action.setParams({ CardId: cardId ,
                          EmailId :emailId,
                          customerId:customerId,
                          Last4Digit:last4Digit
                         });
        action.setCallback(this, function(response) { 
            
            var state = response.getState();
            if(state == "SUCCESS" ){
                
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.loaded",false);
                
                component.find("overlayLib").notifyClose();                                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: result,
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                
                //var spinner = component.find('spinner');
                //$A.util.toggleClass(spinner, 'slds-show'); 
                
                // $A.get('e.force:refreshView').fire();
                
            }
            
        } );
        $A.enqueueAction(action);
        
    },
    saveSMS : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var cardId =  component.get("v.cardId");
        var emailId = component.get("v.emailId");
        var phoneNum = component.get("v.phoneNum");
        var customerId = component.get("v.customerId");
        var last4Digit = component.get("v.last4Digit");
        component.set("v.loaded",true);
        console.log("-----cardId--------"+cardId);
        console.log("-----emailId--------"+emailId);
        console.log("-----customerId--------"+customerId);
        console.log("-----last4Digit--------"+last4Digit);
        
        var action = component.get("c.sendSMS");
        action.setParams({ CardId: cardId ,
                          EmailId :emailId,
                          customerId:customerId,
                          Last4Digit:last4Digit,
                          phoneNum,phoneNum
                         });
                          action.setCallback(this, function(response) { 
                          
                          var state = response.getState();
                          if(state == "SUCCESS" ){
                          
                          var result = response.getReturnValue();
                          console.log(result);
                          component.set("v.loaded",false);
                          
                          component.find("overlayLib").notifyClose();                                   
                          var toastEvent = $A.get("e.force:showToast");
                          toastEvent.setParams({
                          title : 'Success ',
                          message: result,
                          duration:' 1000',
                          key: 'info_alt',
                          type: 'success',
                          mode: 'pester'
                         });
        toastEvent.fire();
        
        //var spinner = component.find('spinner');
        //$A.util.toggleClass(spinner, 'slds-show'); 
        
        // $A.get('e.force:refreshView').fire();
        
    }
    
} );
$A.enqueueAction(action);

},
    onCheckRadio1 : function(component, event, helper) {
        debugger
        
        var rLast4Digit = event.getSource().get("v.label");
        var valueData = event.getSource().get("v.value");
        console.log(valueData);
        var value = valueData.split(" ");
        var rCustomerId = value[0];
        var rEmailId = value[1];
        component.set("v.custId",rCustomerId);
        var rPhoneNum = component.get("v.phone") ;
        if(value[2] != null && value[2] != ''){
            rPhoneNum = value[2];   
        }
        else if( value[3] != null && value[2] != ''){
            rPhoneNum = value[3];   
        }
            else if(value[4] != null && value[2] != ''){
                rPhoneNum = value[4];  
            }              
        
        // var rLast4Digit = value[2];
        var rCardId = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.last4Digit" ,rLast4Digit);
        component.set("v.customerId" ,rCustomerId);
        component.set("v.emailId" ,rEmailId);
        component.set("v.cardId",rCardId);
        component.set("v.phoneNum" ,rPhoneNum);
        component.set("v.buttonD", false);
    },
        onChangeEmail :  function(component, event, helper) {
            var value = event.getSource().get("v.value");
            component.set("v.emailId",value);
        },
            updateCard : function(component,event) {
                debugger
                component.set("v.updateCard",true);
                component.set("v.newCard",false);
            },
                newCard : function(component,event) {
                    debugger
                component.set("v.newCard",true);
                    component.set("v.updateCard",false);
            }
})