({
    doInit: function (component, event, helper) {
        var contId = component.get("v.recordId");
        var action = component.get("c.getEmail");
        action.setParams({
            conId: contId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result && result != null) {
                    component.set("v.ConEmail", result);
                    component.set("v.error", false);
                } else {
                    component.set("v.message", 'No email Associated with This contact');
                    component.set("v.messageType", 'Error');
                    helper.ToastMsg(component, event, helper);
                }
            } else {
                component.set("v.message", 'Error on UpdateSubscriptions Componentt');

                component.set("v.messageType", 'Error');
                helper.ToastMsg(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    sendEmailLogin: function (component, event, helper) {
        var contId = component.get("v.recordId");
        var action = component.get("c.sendEmail");
        action.setParams({
            conID: contId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result == 'success') {
                    component.set("v.message", 'Sent Email Successfully');
                    component.set("v.messageType", 'Success');
                    helper.ToastMsg(component, event, helper);
                } else {
                    component.set("v.message", 'No email Associated with This contact');
                    component.set("v.messageType", 'Error');
                    helper.ToastMsg(component, event, helper);
                }
            } else {
                component.set("v.message", 'Error on  Component');

                component.set("v.messageType", 'Error');
                helper.ToastMsg(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    }
})