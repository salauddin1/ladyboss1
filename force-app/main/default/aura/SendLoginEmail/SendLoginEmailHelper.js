({
    ToastMsg: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            title: component.get("v.messageType"),
            message: component.get("v.message"),
            duration: ' 2000',
            key: 'info_alt',
            type: component.get("v.messageType"),
            mode: 'pester'
        });
        toastEvent.fire();
    }
})