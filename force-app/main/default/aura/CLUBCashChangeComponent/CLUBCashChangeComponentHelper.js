({
	helperMethod : function(component, event, conID) {
		 console.log('inside helper');
        component.set("v.Spinner", !component.get('v.Spinner'));
        var action = component.get("c.RefreshBalanceHistory");
        action.setParams({
            conIDwhen: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('inside refresh');
            component.set("v.Spinner", !component.get('v.Spinner'));
            if (state == "SUCCESS") {
                console.log('inside refresh SUCCESS');
                var a = component.get('c.doInit');
                $A.enqueueAction(a);
            }
            component.set("v.value", 'Active');
        });
        $A.enqueueAction(action);
	}
})