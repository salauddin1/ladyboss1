({
    doInit : function(component, event, helper) {
        var recID = component.get("v.recordId");
        var action = component.get("c.getContactHistoryNew");
        action.setParams({
            recordId: recID
        });
        action.setCallback(this, function(response){
            var data = response.getReturnValue();
            for(var i in data)  {
                console.log(JSON.stringify(data[i]));
            }
            console.log(data);
            component.set("v.contactList", data);
        });
        $A.enqueueAction(action);
    },
    onRefreshBalanceHistory: function (component, event, helper) {
       //alert('Button click');
        console.log('inisde controller');
        helper.helperMethod(component, event, component.get("v.recordId"));
    }
})