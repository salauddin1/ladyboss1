({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.ContactId");
        var action = component.get("c.getcaserec");
        action.setParams({ conId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.CaseDetail",res.wrCasevalue);
                component.set("v.messageflag", false);
                if(res != null && res.wrCasevalue.length > 0){
                    component.set("v.ContactNameVal", res.wrCasevalue[0].Contact.Name);    
                }
            }
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.setCsObj");
        action2.setCallback(this, function(response) {
        	var state = response.getState();
            if(state == "SUCCESS") {
              
                var res = response.getReturnValue();
                console.log(res);
               // component.set("v.Caseinsert",res.wrCase);
                component.set("v.Caseinsert",res);
            }
        });
        $A.enqueueAction(action2);
        
    },
    OpenCase : function(component, event, helper) {
        debugger
        var targetelement1 = [];
        targetelement1 = component.find('main');
        for (var cmp in component.find('main')) {
            console.log(cmp);
            if(cmp.charCodeAt(0) >= 48 && cmp.charCodeAt(0) <= 57) {
                var targetelement2 = [];
                var targetelement2 = targetelement1[cmp].getElements();
                for(var cmp1 in targetelement1[cmp].getElements())
                    $A.util.removeClass(targetelement2[cmp1], "selectedRow");
                $A.util.addClass(targetelement2[cmp1],"data-row");
            }
        }
        
        var selected = event.currentTarget.dataset.caseid;
        var contactName = event.currentTarget.dataset.conname;
        console.log('======'+selected);
        var targetElement = event.currentTarget;
        $A.util.addClass(targetElement,"selectedRow");
        $A.util.removeClass(targetElement, "data-row");
        
        component.set('v.commentflag', true);
        component.set('v.CurrentCaseId', selected);
        component.set("v.CaseCommentDetail", []);
        component.set('v.csCommentButtonShowId', selected);
        
        
        var action2 = component.get("c.getCaseComentValues");
        action2.setParams({ caseId : selected ,
                           contactName : contactName
                          });
        
        action2.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var resAllComment = response.getReturnValue();
                console.log(resAllComment);
                component.set("v.CaseCommentDetail", resAllComment);
                
                
            }
        });
        $A.enqueueAction(action2);
    
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
        
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        component.set("v.commentBox", false);
    },
    HideMesage : function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.messageflag", false);
    },
    submitcase : function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        debugger
        component.set("v.Spinner",true);
        
        component.set("v.SuccessMessage",'');
        var csIns = component.get("v.Caseinsert"); 
        var action = component.get("c.InsertNewCase");
        
        action.setParams({ caseDetail : csIns,
                          conId: component.get("v.ContactId")});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.messageflag", true);
                component.set("v.SuccessMessage",'Case successFully Created');
                component.set("v.Caseinsert", res);
                //component.set("v.Caseinsert", res.wrCase);
                $A.get('e.force:refreshView').fire();
                component.set("v.tabId","tabAllCase");
                component.set("v.Spinner",false);
            }
            
        });
        $A.enqueueAction(action);
    },
    opencommentbox : function(component, event, helper) {
        debugger
        var caseId = event.currentTarget.title;
        component.set('v.caseId',caseId);
        component.set('v.csCommentDescValue','');
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.commentBox", true);
    },
    addCaseComment: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        var allValid = component.find('descripId').get('v.value');
        if(allValid != null && allValid != ''){
            component.set("v.Spinner", true);
            var action = component.get("c.setCaseComment");
            var  csComVal     = component.get('v.csCommentDescValue');
            var  csComIdVal   = component.get('v.caseId');
            var   conNameValue = component.get('v.ContactNameVal');
            action.setParams({ "csId" : csComIdVal,
                              "csComment" : csComVal });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS") {
                    var comm = response.getReturnValue();
                    console.log(comm);
                    component.set("v.commentBox", false);
                    component.set("v.Spinner", false);
                    var getList = component.get("v.CaseCommentDetail");
                    component.set("v.CaseCommentDetail", getList);
                   
                    $A.get('e.force:refreshView').fire();
                }
            });
           
            var action2 = component.get("c.getCaseComentValues");
            action2.setParams({ "caseId" : csComIdVal ,
                               "contactName" : conNameValue
                              });
            
            action2.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS") {
                    var resAllComment = response.getReturnValue();
                    console.log(resAllComment);
                    component.set("v.CaseCommentDetail", resAllComment);
                }
            });
             $A.enqueueAction(action);
            $A.enqueueAction(action2);
        } 
        $A.enqueueAction(component.get('c.doInit'));
    },
    
})