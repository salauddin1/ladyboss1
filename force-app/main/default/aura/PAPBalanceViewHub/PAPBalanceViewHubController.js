({
    doInit : function(component, event, helper) {
        debugger
        component.set('v.ThreeFFImageURL', 'https://ladyboss--tirthbox--c.cs22.content.force.com/servlet/servlet.ImageServer?id=01517000000u041&oid=00D17000000DQek&lastMod=1588315950000');
        var conID = component.get("v.ContactId");
        var action = component.get("c.getBalance");
        console
        action.setParams({
            conID: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state:',state);
            
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.name != null) {
                    if (result.name.includes("BURN")) {
                    result.image = "BURN.png";
                    } else if (result.name.includes("REST")) {
                    result.image = "REST.png";
                    } else if (result.name.includes("RECOVER")) {
                    result.image = "RECOVER.png";
                    } else if (result.name.includes("FUEL")) {
                    result.image = "FUEL.png";
                    } else if (result.name.includes("LEAN 1")) {
                    result.image = "LEAN1.png";
                    } else if (result.name.includes("LEAN 2")) {
                    result.image = "LEAN2.png";
                    } else if (result.name.includes("LEAN 3")) {
                    result.image = "LEAN3.png";
                    } else if (result.name.includes("GREENS-1")) {
                    result.image = "GREENS1.png";
                    } else if (result.name.includes("GREENS-2")) {
                    result.image = "GREENS2.png";
                    } else if (result.name.includes("GREENS-3")) {
                    result.image = "GREENS3.png";
                    }else{
                    result.image = "T-System.png";
                    }
                    
                }
                  
                console.log('result=>',result.name+result.image);
                
                component.set("v.Customer", result);
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(action);        
    }
})