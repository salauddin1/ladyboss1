({
    doInit : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var reasonVal = "All";
        var action = component.get("c.getOpportunity");
        action.setParams({ contactId :contactId,
                          reasonFilter : "All" });
        action.setCallback(this, function(response) {
            debugger
            var value = response.getReturnValue();
            component.set("v.oppList",value);
            var b = value.length;
            if(b > 0){
                component.set("v.isOppValue" , true);
                component.set("v.isValue",true);
            }
            else{
                component.set("v.isOppValue" , false);   
            }
            
        });
        var action1 = component.get("c.getOpportunityLineItem");
        action1.setParams({ contactId :contactId,
                           reasonFilter : "All"});
        action1.setCallback(this, function(response) {
            debugger
            var OppLineval = response.getReturnValue();
            component.set("v.oppLineItem",OppLineval);
            var b = OppLineval.length;
            if(b > 0){
                component.set("v.isOppLineValue" , true);
                component.set("v.isValue",true);
            }
            else{
                component.set("v.isOppLineValue" , false);   
            }
            
        });
        $A.enqueueAction(action1);
        $A.enqueueAction(action);
        /*var oppLineVal = component.get("v.isOppLineValue");
        var oppVal = component.get("v.isOppValue");
        if(oppLineVal == true  || oppVal == true ){
           component.set("v.isValue",true); 
        }*/
    },
    reasonSelect : function(component, event, helper) {
        var contactId = component.get("v.recordId");
        var value = event.getSource().get('v.value') ;
        component.set("v.reason",value);
        var action = component.get("c.getOpportunity");
        action.setParams({ contactId :contactId,
                          reasonFilter : value});
        action.setCallback(this, function(response) {
            debugger
            var result = response.getReturnValue();
            component.set("v.oppList",result);
        });
        
        var action1 = component.get("c.getOpportunityLineItem");
        action1.setParams({ contactId :contactId,
                           reasonFilter : value});
        action1.setCallback(this, function(response) {
            debugger
            var OppLineval = response.getReturnValue();
            component.set("v.oppLineItem",OppLineval);
            
        });
        $A.enqueueAction(action1);
        $A.enqueueAction(action);  
    }
})