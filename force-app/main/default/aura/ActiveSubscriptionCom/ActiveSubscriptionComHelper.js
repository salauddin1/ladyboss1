({
    openmodal:function(component,event,helper) {
        $A.get('e.force:refreshView').fire();
        component.set("v.checkValD",true);
        
		var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
    openmodalCancel:function(component,event,helper) {
		var cmpTarget = component.find('ModalboxCancel');
		var cmpBack = component.find('ModalbackdropCancel');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
    	cancelBtn : function(component, event, helper) { 
	// Close the action panel 
	var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
	dismissActionPanel.fire(); 
	}, 
    updateOpportunitySubscription :function(component,event,helper) {
        debugger
        var oppProdId = component.get('v.oppProductId');
        var dateValue = component.get("v.dateVal");
        
        var action = component.get("c.updateOpportunitySubscription");
        console.log('dateValue' + dateValue);
        action.setParams({ oppId :oppProdId,
                          dateVal:dateValue
                           });
        action.setCallback(this, function(response) {
        var msg = response.getReturnValue();
            this.ToastMsg(component,event,helper,msg);
             component.set("v.oppProductId",undefined);
        });
         $A.enqueueAction(action);
        
    },
    cancelOpportunitySubscription :function(component,event,helper) {
        debugger
    var oppProdId = component.get('v.oppProductId');
        
        var action = component.get("c.cancelOpportunitySubscription");
        action.setParams({ oppId :oppProdId,
                           });
        action.setCallback(this, function(response) {
         var msg = response.getReturnValue();
            this.ToastMsg(component,event,helper,msg);
        component.set("v.oppProductId" ,undefined);
        });
         $A.enqueueAction(action);
        
    },
    ToastMsg :function(component,event,helper,msg) {
         var toastEvent = $A.get("e.force:showToast");
            
            toastEvent.setParams({
                title : 'Success ',
                message: msg,
                duration:' 2000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
    }
    
})