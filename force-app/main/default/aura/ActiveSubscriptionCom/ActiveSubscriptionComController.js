({
    doInit: function(component, event, helper) {
        debugger
        console.log("--------do----------");
        var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var arr = a[6];
        console.log("--------arr--------"+arr);
        var action = component.get("c.getAllOpp");
        action.setParams({ recordId :arr});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                component.set("v.OppName", response.getReturnValue());
                debugger
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action1 = component.get("c.getOpportunityLineItem");
        action1.setParams({ contactId :arr});
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                component.set("v.OppLineItem", response.getReturnValue());
                debugger
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        $A.enqueueAction(action);
        
        $A.enqueueAction(action1);
        
    },
    handleSelect : function(component, event, helper) {
        
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");
        var values = event.getParam("value").split(',');
        var oppId = values[0];
        if(values[1] != ''){
        var quantity = parseInt(values[1]);
            component.set("v.quntity",quantity);
        }
        else{
        component.set("v.quntity",0);
        }
       
        component.set("v.oppId" ,oppId);
        if(selectedMenuItemValue =="Update"){
            helper.openmodal(component, event, helper);
            
        }
        else if(selectedMenuItemValue == "Cancel"){
            helper.openmodalCancel(component, event, helper);
            
        }
    },
    handleSelectProd:function(component, event, helper) {
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");
        
        var oppId = event.getParam("value");
        component.set("v.oppProductId" ,oppId);
        if(selectedMenuItemValue =="Update"){
            helper.openmodal(component, event, helper);
            
        }
        else if(selectedMenuItemValue == "Cancel"){
            helper.openmodalCancel(component, event, helper);
            
        } 
    },
    closeModal:function(component,event,helper){ 
        $A.get('e.force:refreshView').fire();
        
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.set("v.dateVal",'');
        component.set("v.checkVal",false);
        component.set("v.checkValD",false);
        
        component.set("v.quntity"," ");
        
        
    },
    closeModalCancel:function(component,event,helper){ 
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
    },
    updateOpp : function(component, event, helper) {
        var dateValue = component.get("v.dateVal");
        var checkVal = component.get("v.checkVal");
        var quntityVal = component.get("v.quntity");
       // var quntityVal = 1;
        var oppIds = component.get("v.oppId");
        if(oppIds != undefined){
            var action = component.get("c.updateSubscription");
            console.log('dateValue' + dateValue);
            action.setParams({LineItemId :oppIds,
                              dateVal:dateValue,
                              checkVal:checkVal
                             });
            action.setCallback(this, function(response) {
                var msg = response.getReturnValue(); 
                console.log('SAVED.');  
                helper.ToastMsg(component,event,helper,msg);
                 component.set("v.oppId",undefined);
            } );
            $A.enqueueAction(action);
        }
        else{
            helper.updateOpportunitySubscription(component,event,helper);
        }
        $A.get('e.force:refreshView').fire();
        
        console.log('save:end');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.set("v.dateVal"," ");
        component.set("v.checkVal",false);
        component.set("v.quntity"," ");
    },
    abc: function(component, event, helper) {
        debugger
        var value = event.currentTarget.value.trim() ;
        component.set("v.quntity",value);
        console.log("------------"+value);
    },
    cancelOpp : function(component, event, helper) {
        debugger
        var oppIds = component.get("v.oppId");
        if(oppIds != undefined){
            var action2 = component.get("c.cancelSubscription"); 
            action2.setParams({ opplineItem : oppIds
                              });
            
            action2.setCallback(this, function(response) {
                var msg = response.getReturnValue();
                console.log("---------re------"+response.getReturnValue());
                helper.ToastMsg(component,event,helper,msg);
                component.set("v.oppId",undefined);
            });
            $A.enqueueAction(action2);
        }
        else{
            helper.cancelOpportunitySubscription(component,event,helper);
        }
        $A.get('e.force:refreshView').fire();
        
        console.log('save:end');
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        
    },
    abc: function(component, event, helper) {
        debugger
        var value = event.getSource().get('v.value') ;
        component.set("v.quntity",value);
        console.log("------------"+value);
    },
    dateUpdate : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        component.set("v.dateVal",value);
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        } 
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.dateVal") != '' && component.get("v.dateVal") < todayFormattedDate){
            debugger
            component.set("v.dateValidationError" , true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------"+dV);
        }else{
            component.set("v.dateValidationError" , false);
            
        }
    },
    checkVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.checked");
        component.set("v.checkVal" , value);
    },
    dateVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        
        //var value = event.currentTarget.value.trim() ;
        component.set("v.dateVal",value);
        console.log("------------"+value);
    },
    
    
})