({
    doInit: function (component, event, helper) {
        
        component.set("v.Spinner1", false);
        window.addEventListener("message", $A.getCallback(function(event) {
            debugger
            if(event.data == 'updateCardSuccess'){
                component.set("v.Opencard",false);
                setTimeout(function() {
                    location.reload();
                },3000);
                window.scrollTo(0, 0);
                var msg = 'Success! Your card has been updated';
                var msgType = 'success';
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
                if(event.data.toString().includes("It all happened so fast. Now, I'm here!")){ 
                    setTimeout(function() {
                        location.reload();
                    },3000);
                }
            }
        }), false);
        var today = new Date();
        var n = today.toString().substring(today.toString().indexOf("(") + 1, today.toString().indexOf(")"));
        if ( n.search( /\W/ ) >= 0 ) {
            n = n.match( /\b\w/g ) // Match first letter at each word boundary.
            .join( "" )
            .toUpperCase();
        }
        console.log('timezone',n);
        component.set("v.timezone",n);
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        component.set("v.value", 'sAll');component.set("v.value1", '15 Days');
        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        component.set("v.myDate", todayFormattedDate);
        var conID = component.get("v.recordId");
        var action = component.get("c.getAllSubscriptions");
        action.setParams({
            conID: conID
        });
        action.setCallback(this, function (response) {
            
            var state = response.getState();
            var prodView = [];
            var PastDueOppLine = "";
            var alertPastDue =  component.get("v.alertPastDue");
            window.scrollTo(0, 0);
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result---', result);
                if (result && result != null) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].SubStatus == 'Past Due') {
                            prodView.push(result[i]);
                            if(PastDueOppLine == ""){
                                PastDueOppLine =result[i].Name;
                            }
                            else{
                                PastDueOppLine =PastDueOppLine + ', '+result[i].Name;
                            }
                        }
                    }
                    if(prodView.length>0){
                        component.set("v.PastDueOppLineItem",PastDueOppLine);
                        if(alertPastDue == "true"){
                            component.set("v.openAlertButton", true);
                        }
                    }
                    component.set("v.OppLineItem", result);
                    component.set("v.PastDueList", prodView);
                    component.set("v.products", result);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    closeAlertModel : function(component, event, helper) {
        
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
        component.set("v.openAlertButton", false); 
        component.set("v.alertPastDue", false);
        component.set("v.openPastDueCard", false);
    },
    ShowPastDueMembership : function(component, event, helper) {
        
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
        component.set("v.openAlertButton", false); 
        component.set("v.openPastDueCard", true);
    },
    handleChange: function (component, event) {
        var l = (event.getParam('value'));
        console.log(event.getParam('value'));
        
        component.set("v.value", l);
        var prodView = [];
        var allProducts = component.get("v.products");
        if (l == "sAll") {
            component.set("v.OppLineItem", allProducts);
        } else {
            for (var i = 0; i < allProducts.length; i++) {
                if (l == "Active" || l == "Past Due") {
                    if (allProducts[i].SubStatus == l) {
                        prodView.push(allProducts[i]);
                    }
                } else if (l == "InActive") {
                    if (allProducts[i].SubStatus == "Cancelled") {
                        prodView.push(allProducts[i]);
                    }
                } else if (l == "trialing") {
                    if (allProducts[i].SubStatus == "Pending") {
                        prodView.push(allProducts[i]);
                    }
                }
            }
            component.set("v.OppLineItem", prodView);
        }
    },
    onRefreshSubscription: function (component, event, helper) {
        //salert('Button click');
        console.log('inisde controller');
        helper.helperMethod(component, event, component.get("v.recordId"));
    },
    handleSelectProd: function (component, event, helper) {
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");
        
        var oppId = event.getParam("value");
        component.set("v.Id", oppId);
        var selectedMenuRecId;
        var selectedMenuCustId;
        var selectedMenuValue = event.detail.menuItem.get("v.value");
        if (selectedMenuValue != "undefined" && selectedMenuValue != '') {
            if (selectedMenuValue.includes(" ### ### ")) {
                var idANdCust = selectedMenuValue.split(" ### ### ");
                selectedMenuRecId = idANdCust[0];
                selectedMenuCustId = idANdCust[1];
            }
        }
        if (selectedMenuItemValue == "Charge me Sooner") {
            component.set("v.selectedModal", "Modalbox");
            helper.openmodal(component, event, helper);
            //   } else if (selectedMenuItemValue == "Retry Charge") {
            //       helper.retrycharge(component, event, helper);
        } else if (selectedMenuItemValue == "Pause My Membership") {
            component.set("v.selectedModal", "ModalboxPause");
            var opplineitem = component.get("v.OppLineItem");
            for (var i = 0; i < opplineitem.length; i++) {
                if (opplineitem[i].Id == oppId) {
                    var date = new Date(opplineitem[i].periodEnd);
                    component.set("v.currentBillingDate", opplineitem[i].periodEnd);
                    component.set("v.newBillingDate", date.setDate(date.getDate() + 15));
                }
            }
            helper.openmodal(component, event, helper);
        } else if (selectedMenuItemValue == "Create a Membership") {
            console.log("First step;- fire event");
            component.set("v.Spinner1", true);
            var refreshevent = $A.get("e.c:RefreshSubscriptions");
            refreshevent.setParams({
                data: "ManageSubscription"
            });
            refreshevent.fire();
        } else if (selectedMenuItemValue == "Update Card and Retry") {
            
            if (selectedMenuCustId != null && selectedMenuCustId != '' && selectedMenuCustId != "undefined" && selectedMenuCustId.includes("cus_")) {
                component.set("v.custIdValue", selectedMenuCustId);
                component.set("v.oppUpdateCardIdValue", selectedMenuRecId);
                component.set("v.isOpen", true);
            } else {
                var msg = 'No customer id found.';
                var msgType = "error";
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
            }
            //    helper.updateCardandRetry(component, event, helper);
        } else if (selectedMenuItemValue == "Retry with Current Card") {
            helper.retrycharge(component, event, helper);
        }
    },
    updateCardandRetry: function (component, event) {
        
        var message = event.getParam("message");
        // set the handler attributes based on event data
        //     cmp.set("v.messageFromEvent", message);
        //     var numEventsHandled = parseInt(cmp.get("v.numEvents")) + 1;
        //     cmp.set("v.numEvents", numEventsHandled);
        component.set("v.Opencard", false);
        var msg = 'No Customer Found.';
        var msgType = "error";
        component.set("v.message", message);
        component.set("v.messageType", msgType);
        $A.util.removeClass(component.find('toastModel'), 'slds-hide');
        $A.util.addClass(component.find('toastModel'), 'slds-show');
        setTimeout(function () {
            $A.util.addClass(component.find('toastModel'), 'slds-hide');
            component.set("v.message", "");
            component.set("v.messageType", "");
        }, 5000);
    },
    associateUsages: function (component, event, helper) {
        var menuCustId = component.get("v.custIdValue");
        var oppliValue = component.get("v.oppUpdateCardIdValue");
        
        if (oppliValue != null && oppliValue != '' && oppliValue != "undefined" && !oppliValue.includes("cus_")) {
            var screenwidth = screen.width;
            
            console.log('----screenwidth---'+screenwidth);
            var srcVal = 'https://tirthbox-ladyboss-support.cs22.force.com/LadyBossHub/updateCardMembership?id=' + menuCustId + '&opplineIdval=' + oppliValue;
            component.set("v.iframSrcUrlValue", srcVal);
            component.set("v.isOpen", false);
            component.set("v.Opencard", true);
        } else {
            component.set("v.isOpen", false);
            component.set("v.Opencard", false);
            var msg = 'No subscription found.';
            var msgType = "error";
            component.set("v.message", msg);
            component.set("v.messageType", msgType);
            $A.util.removeClass(component.find('toastModel'), 'slds-hide');
            $A.util.addClass(component.find('toastModel'), 'slds-show');
            setTimeout(function () {
                $A.util.addClass(component.find('toastModel'), 'slds-hide');
                component.set("v.message", "");
                component.set("v.messageType", "");
            }, 5000);
        }
    },
    openModel: function (component, event, helper) {
        var idx = event.getSource().get("v.value");
        component.set("v.cardid", idx);
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    closeModelCon: function (component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle" 
        component.set("v.isOpen", false);
    },
    closecard: function (component, event, helper) {
        component.set("v.Opencard", false);
    },
    //-----------------------------***------------------------------------------------End 
    changeDate: function (component, event, helper) {
        var oppId = component.get("v.Id");
        var numOfDays = component.get("v.value1");
        var opplineitem = component.get("v.OppLineItem");
        for (var i = 0; i < opplineitem.length; i++) {
            if (opplineitem[i].Id == oppId) {
                var date = new Date(opplineitem[i].periodEnd);
                if (numOfDays == '15 Days') {
                    component.set("v.newBillingDate", date.setDate(date.getDate() + 15));
                } else {
                    component.set("v.newBillingDate", date.setDate(date.getDate() + 30));
                }
            }
        }
    },
    dateUpdate: function (component, event, helper) {
        
        var value = event.getSource().get("v.value");
        component.set("v.dateVal", value);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        console.log('--------'+component.get("v.dateVal"));
        var dateOnly = component.get("v.dateVal");
        if(dateOnly >= todayFormattedDate){
            component.set("v.update", true);
            
        }
        else{
            component.set("v.update", false);
            component.set("v.dateValidationError", true);
        }
        if (component.get("v.dateVal") != '' && component.get("v.dateVal") < todayFormattedDate) {
            
            component.set("v.dateValidationError", true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------" + dV);
        } else {
            component.set("v.dateValidationError", false);
            // component.set("v.update", true);
            
        }
        
        
    },
    closeModal: function (component, event, helper) {
        helper.closeModalHelper(component, event, helper);
    },
    updateOpp: function (component, event, helper) {
        component.set("v.Spinner2", true);
        var dateValue = component.get("v.dateVal");
        var dateonly = dateValue.slice(8,10);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        console.log('----dd----'+dd);
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        console.log('----mm----'+mm);
        var yyyy = today.getFullYear();
        console.log('----yyyy----'+yyyy);
        var todaynew =  yyyy+ '-' + mm + '-' + dd ;
        console.log('----todaynew----'+todaynew);
        if(dateValue >= todaynew) {
            var checkVal = component.get("v.update");
            // component.set("v.Spinner1", true);
            // var quntityVal = 1;
            var oppIds = component.get("v.Id");
            console.log('heloo===> ', component.get('v.Id'));
            var action = component.get("c.updateSubscription");
            console.log('dateValue' + dateValue);
            action.setParams({
                LineItemId: oppIds,
                dateVal: dateValue,
                checkVal: checkVal,
                TimeZone: component.get("v.timezone"),
                isPauseSub: false,
                numOfDays:'zero'
            });
            
            action.setCallback(this, function (response) {
                
                var msg = response.getReturnValue();
                console.log('SAVED.');
                //helper.ToastMsg(component, event, helper, msg);
                var msgType = "error";
                window.scrollTo(0, 0);
                if (msg == 'Subscription updated in stripe') {
                    msg = 'Success! Your membership has been updated';
                    msgType = 'success';
                }
                component.set("v.Spinner2", false);
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
                
                var a = component.get("c.doInit");
                $A.enqueueAction(a);
                
            });
        }
        $A.enqueueAction(action);
        
        
        
        console.log('save:end');
        helper.closeModalHelper(component, event, helper);
        
        
    },
    closeModel: function (component, event, helper) {
        $A.util.addClass(component.find('toastModel'), 'slds-hide');
    },
    pauseSub: function (component, event, helper) {
        component.set("v.Spinner2", true);
        console.log("pauseSub " + component.get("v.Id") + component.get("v.value1"));
        var action = component.get("c.pauseSubscriptions");
        action.setParams({
            LineItemId: component.get("v.Id"),
            numOfDays: component.get("v.value1"),
            TimeZone: component.get("v.timezone")
        });
        action.setCallback(this, function (response) {
            var msg = response.getReturnValue();
            console.log("SAVED.");
            //helper.ToastMsg(component, event, helper, msg);
            var msgType = "error";
            window.scrollTo(0, 0);
            if (msg == "Subscription updated in stripe") {
                msg = "Success! Your membership has been updated";
                msgType = "success";
            }
            component.set("v.Spinner2", false);
            component.set("v.message", msg);
            component.set("v.messageType", msgType);
            component.set("v.messageType", msgType);
            $A.util.removeClass(component.find("toastModel"), "slds-hide");
            $A.util.addClass(component.find("toastModel"), "slds-show");
            setTimeout(function () {
                $A.util.addClass(component.find("toastModel"), "slds-hide");
                component.set("v.message", "");
                component.set("v.messageType", "");
            }, 5000);
            var a = component.get("c.doInit");
            $A.enqueueAction(a);
        });
        $A.enqueueAction(action);
        helper.closeModalHelper(component, event, helper);
    },
    handleClick: function (component, event, helper) {
        debugger
        component.set("v.pauseShipmentFlag", false);
        var idx = event.target.getAttribute("data-index");
        var planIds = event.target.getAttribute("data-PlanIds");
        console.log('============'+planIds);
        var label = event.target.getAttribute("data-label");
        console.log(idx);
        console.log(label);
        component.set("v.Id", idx);
        if (label == 'Manage') {
            var opplineitem = component.get("v.OppLineItem");
            for (var i = 0; i < opplineitem.length; i++) {
                if (opplineitem[i].Id == idx) {
                    if (opplineitem[i].SubStatus == 'Trialing & Paused Memberships' ){
                        component.set("v.pauseShipmentFlag", true);
                    }
                    if (opplineitem[i].HidePause || opplineitem[i].SubStatus == 'Trialing & Paused Memberships' || opplineitem[i].SubStatus == 'Active') {
                        component.set("v.PauseHide", true);
                    }
                    else{
                        component.set("v.PauseHide", false);
                    }
                }
            }
        }
        component.set("v.selectedModalLabel",label);
        component.set("v.openPastDueCard", false);
        var cmpTarget = component.find(label);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.addClass(cmpTarget, "slds-fade-in-open");
        $A.util.addClass(cmpBack, "slds-backdrop--open");
        if(planIds == "true") {
            component.set("v.pauseShipmentFlag", true);
        }
    },
    handleSelect: function (component, event, helper) {
        var selectedModalLabel = component.get("v.selectedModalLabel");
        var cmpTarget = component.find(selectedModalLabel);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.removeClass(cmpBack, "slds-backdrop--open");
        $A.util.removeClass(cmpTarget, "slds-fade-in-open");
        var selectedMenuItemValue = event.target.getAttribute("data-label");
        var oppId = component.get("v.Id");
        var selectedMenuCustId;
        var selectedMenuRecId;
        if (selectedMenuItemValue == "Update Card and Retry") {
            var opplineitem = component.get("v.OppLineItem");
            for (var i = 0; i < opplineitem.length; i++) {
                if (opplineitem[i].Id == oppId) {
                    selectedMenuCustId = opplineitem[i].CustomerID;
                    selectedMenuRecId = oppId;
                }
            }
        }
        if (selectedMenuItemValue == "Ship Sooner") {
            component.set("v.selectedModal", "Modalbox");
            helper.openmodal(component, event, helper);
        } else if (selectedMenuItemValue == "Pause Shipment") {
            component.set("v.selectedModal", "ModalboxPause");
            var opplineitem = component.get("v.OppLineItem");
            for (var i = 0; i < opplineitem.length; i++) {
                if (opplineitem[i].Id == oppId) {
                    var date = new Date(opplineitem[i].periodEnd);
                    component.set("v.currentBillingDate", opplineitem[i].periodEnd);
                    component.set(
                        "v.newBillingDate",
                        date.setDate(date.getDate() + 15)
                    );
                }
            }
            helper.openmodal(component, event, helper);
        }else if (selectedMenuItemValue == "Update Card and Retry") {
            if (selectedMenuCustId != null && selectedMenuCustId != '' && selectedMenuCustId != "undefined" && selectedMenuCustId.includes("cus_")) {
                component.set("v.custIdValue", selectedMenuCustId);
                component.set("v.oppUpdateCardIdValue", selectedMenuRecId);
                component.set("v.isOpen", true);
            } else {
                var msg = 'No customer id found.';
                var msgType = "error";
                component.set("v.message", msg);
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
            }
        } else if (selectedMenuItemValue == "Retry Charge") {
            helper.retrycharge(component, event, helper);
        }
    },
    closeModalLabel: function (component, event, helper) {
        debugger
        var selectedModalLabel = component.get("v.selectedModalLabel");
        var cmpTarget = component.find(selectedModalLabel);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.removeClass(cmpBack, "slds-backdrop--open");
        $A.util.removeClass(cmpTarget, "slds-fade-in-open"); 
    }
})