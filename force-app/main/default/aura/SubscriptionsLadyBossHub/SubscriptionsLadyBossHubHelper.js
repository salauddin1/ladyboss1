({
    helperMethod: function (component, event, conID) {
        console.log('inside helper');
        component.set("v.Spinner", !component.get('v.Spinner'));
        var action = component.get("c.refreshSubscription");
        action.setParams({
            conIDwhen: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('inside refresh');
            component.set("v.Spinner", !component.get('v.Spinner'));
            if (state == "SUCCESS") {
                console.log('inside refresh SUCCESS');
                var a = component.get('c.doInit');
                $A.enqueueAction(a);
            }
            component.set("v.value", 'Active');
        });
        $A.enqueueAction(action);
    },
    openmodal: function (component, event, helper) {
        var selectedModal = component.get("v.selectedModal");
        var oppId = component.get("v.Id");
        var prods = component.get("v.OppLineItem");
        for (var index = 0; index < prods.length; index++) {
            if (prods[index].Id == oppId) {
                var maxd = prods[index].periodEnd;
                component.set("v.maxDate",maxd);
            }
            
        }
        component.set("v.checkValD", true);
        console.log('openModal',selectedModal);
        var cmpTarget = component.find(selectedModal);
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    retrycharge: function (component, event, helper) {
        component.set("v.Spinner1", !component.get('v.Spinner1'));
        var action = component.get("c.payPastDueInvoice");
        action.setParams({
            LineItemId: component.get("v.Id")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set("v.Spinner1", !component.get('v.Spinner1'));
            if (state == "SUCCESS") {
                var msgType = 'success';
                var msg;
                var result = response.getReturnValue();
                if (result == 'Success') {
                    msg = 'Successfully charged for Subscription';
                }else if (result != null) {
                    msgType = 'error';
                    msg = result;
                }else{
                    msgType = 'error';
                    msg = 'Unknown Error';
                }
                component.set("v.message", msg);
                
                component.set("v.messageType", msgType);
                $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                $A.util.addClass(component.find('toastModel'), 'slds-show');
                setTimeout(function () {
                    $A.util.addClass(component.find('toastModel'), 'slds-hide');
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
                if(msgType == 'success'){
                    var a = component.get('c.doInit');
                    $A.enqueueAction(a);
                    component.set("v.value", 'Active');
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    closeModalHelper: function (component, event, helper) {
        var a = component.get('c.doInit');		
        //$A.enqueueAction(a);
        var selectedModal = component.get("v.selectedModal");
        var cmpTarget = component.find(selectedModal);
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        component.set("v.checkValD", false);
    }
})