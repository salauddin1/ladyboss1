({
    getInitValues : function(component, event, recordId) {
        var action = component.get("c.getOpportunityDetails");
        var address = {};
        component.set("v.addAddress",address);
        component.set("v.productOptions",address);
        action.setParams({"conId": recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                console.log('result address',result.address);
                component.set("v.contact",result);
                var subscribedProductList = result.subscribedProductList;
                if(subscribedProductList)
                    component.set("v.subscribedProductList",subscribedProductList);
                var cards = result.cardList; 
                cards.unshift({"Credit_Card_Number__c" : "--- Select Card ---"});
                console.log(cards);
                component.set("v.card",cards);
                var cardsList = component.get("v.card");
                console.log('cards List array',cardsList);
                for(var i=1;i<cardsList.length;i++){
                    if(cardsList[i].Credit_Card_Number__c){
                        console.log('cardsList Number',cardsList[i].Credit_Card_Number__c);
                        var cardNumber = cardsList[i].Credit_Card_Number__c;
                        cardsList[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                    }
                }
                
                component.set("v.card",cardsList);
                
                if(result.address){
                    var displayAddress = result.address.Shipping_Street__c+','+result.address.Shipping_City__c+','+result.address.Shipping_State_Province__c+',';
                    displayAddress+=result.address.Shipping_Country__c+','+result.address.Shipping_Zip_Postal_Code__c+'.';
                    
                    component.set("v.displayAddress",displayAddress);
                    component.set("v.showSameAsShippingCheckbox",true);
                    component.set("v.contactHasPrimaryAddress",true);
                }
                component.set("v.expMonthsList",result.allPickListWrapperList[0].pickListField);
                component.set("v.expYearsList",result.allPickListWrapperList[1].pickListField);
                component.set("v.cardTypeList",result.allPickListWrapperList[2].pickListField);
                console.log('card type list returned',component.get("v.cardTypeList"));
                component.set("v.selectedCard", "--- Select Card ---");
                component.set("v.contactId", result.ContactId);
                component.set("v.productOptions", result.productWrapperList);
                console.log('Contact ID -->',component.get("v.contactId"));
                console.log('Products--->',component.get("v.productOptions"));    
                component.set("v.discount",result.discount);
                console.log('discount',component.get("v.discount"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
                console.log('in do init',component.get("v.addAddress"));
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    setTotalPrice :  function(component, event) {
        console.log('totalpricehelper');
        var discount = component.get("v.discount");
        var prodItems = component.get("v.productOptions");
        var totalPrice = 0;
        var totalClubbedValue = 0;
        var totalNonClubbedValue = 0;
        for(var i = 0; i < prodItems.length; i++){
            // console.log('prodItems',prodItems[i]);
            if(prodItems[i].checkValue == true && prodItems[i].quantity != 0 && prodItems[i].standAloneClubProd==false){
                console.log('totalPrice in step 1',totalPrice);
                totalPrice += prodItems[i].quantity * prodItems[i].price;
                //totalNonClubbedValue += prodItems[i].quantity * prodItems[i].price;
            }
            if(prodItems[i].checkValue == true && prodItems[i].quantity != 0 && prodItems[i].isSwitched==false && prodItems[i].standAloneClubProd==false){
                totalNonClubbedValue += prodItems[i].quantity * prodItems[i].price;
            }
            if(prodItems[i].checkValue == true && prodItems[i].isSwitched==false && prodItems[i].quanititySelector == false && prodItems[i].standAloneClubProd==false){
                totalPrice += (1 * prodItems[i].price);
                totalNonClubbedValue += (1 * prodItems[i].price);
                console.log('totalPrice in else if',totalPrice);
            }
            if(prodItems[i].checkValue == true && prodItems[i].isSwitched==true && prodItems[i].quanititySelector == false && prodItems[i].standAloneClubProd==false){
                totalPrice += (1 * prodItems[i].price);
                totalClubbedValue += (1 * prodItems[i].price);
                console.log('totalPrice in else ifif',totalPrice);
            }
            if(prodItems[i].checkValue == true && prodItems[i].isSwitched==true && prodItems[i].quantity != 0 && prodItems[i].standAloneClubProd==false){
                totalClubbedValue += prodItems[i].quantity * prodItems[i].price;
                console.log('totalClubbedPrice in else ifif',totalClubbedValue);
            }
            if(prodItems[i].checkValue == true && prodItems[i].quanititySelector == false && prodItems[i].standAloneClubProd==true){
                totalPrice += (1 * prodItems[i].price);
                totalClubbedValue += (1 * prodItems[i].price);
                console.log('totalPrice in else ifif',totalPrice);
            }
            if(prodItems[i].checkValue == true && prodItems[i].quantity != 0 && prodItems[i].standAloneClubProd==true){
                totalClubbedValue += prodItems[i].quantity * prodItems[i].price;
                console.log('totalClubbedPrice in else ifif',totalClubbedValue);
            }
            
        } 
        console.log('total price outside',totalPrice);
        component.set("v.totalPriceValue",totalPrice.toFixed(2));
        component.set("v.totalClubbedValue",totalClubbedValue.toFixed(2));
        component.set("v.totalNonClubbedValue",totalNonClubbedValue.toFixed(2));
    },
    getContactOptions: function(component,event) {
        var action = component.get('c.getContacts');
        var search = component.get("v.inputContact");
        action.setParams({
            searchText: search
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ) {
                var results = [];
                var predictions = response.getReturnValue();
                if(predictions.length > 0) {
                    for (var i = 0; i< predictions.length; i++) {
                        results.push(predictions[i]);
                    }
                    console.log('contact results-->',results);
                    component.set("v.isRenderContactResults",true);
                    component.set("v.contactOptions",results); 
                    
                }else{
                    console.log('Contact Query Error');
                    component.set("v.isRenderContactResults",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    getSelectedContact: function(component, event, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var action = component.get('c.getContactDetails');
        action.setParams({
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                component.set("v.contact",result);
                component.set("v.contactId", result.ContactId);
                console.log(component.get("v.contact"));
                console.log(component.get("v.contactId"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateContactInfo: function(component, event) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var contactDetails = JSON.stringify(component.get("v.contact"));
        console.log('in helper updatre info contact Details---> ',contactDetails);
        var action = component.get('c.updateContactDetails');
        action.setParams({
            contactDetails : contactDetails
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var updatec = component.get("v.updateCon");
                component.set("v.updateCon",false);
                var chkBox = component.find("updateConCheck").set("v.checked",false);
                console.log('chkBox',chkBox);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    
    createAddress: function(component, event, address, contactId,isPrimary) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var addressDetails = JSON.stringify(address);
        console.log('in helper updatre info ---> ',addressDetails);
        var action = component.get('c.createNewAddress');
        action.setParams({
            addressDetails : addressDetails,
            contactId: contactId,
            isPrimary : isPrimary
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                var chkBox = component.find("newAddressCheck").set("v.checked",false);
                console.log('chkBox',chkBox);
                component.set("v.newAddress",false);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //if(component.get("v.isPrimaryAddress")){
                if(true){
                    component.set("v.contactHasPrimaryAddress",true);
                    console.log('inside primary address change');
                    this.refreshPrimaryAddress(component,event,contactId);
                }
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    newCardWithBilling: function(component, event, cardDetails, contactId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Details info ---> ',cardDetailString);
        var action = component.get('c.createCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                console.log('Card Created');
                component.set("v.newCard",false);
                component.find("newCardChkBox").set("v.checked",false);
                this.refreshCardList(component,event,contactId);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    updateCardWithBilling: function(component, event, cardDetails, cardId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        component.set("v.isUpdateCardWithError",true);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Update Details info ---> ',cardDetailString);
        var action = component.get('c.updateCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            cardId: cardId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                component.set("v.isUpdateCardWithError",true);
                console.log('Card updated');
                component.set("v.newCard",false);
                //component.find("newCardChkBox").set("v.checked",false);
                this.refreshCardList(component,event,component.get("v.contactId"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    newCardWithShipping: function(component, event, cardDetails, address, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        var addressDetailString = JSON.stringify(address);
        console.log('in helper Card Shipping Details info ---> ',cardDetailString);
        console.log('shipping Address ',addressDetailString);
        var action = component.get('c.createCardWithShipping');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId,
            addressDetails : addressDetailString
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                console.log('Card Created');
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //  $A.get('e.force:refreshView').fire();                
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);   
    },
    createOrder: function(component, event,selectedProd,payment,sd,card,contact,totalPriceValue){
        //console.log('createOpportunity helper',sd);
        //console.log('stringify ',JSON.stringify(selectedProd));
        console.log('selectedProd-->'+selectedProd);
        console.log('payment-->'+payment);
        
       // return;
        var action = component.get('c.insertOpportunity');
        action.setParams({
            selectedProd : JSON.stringify(selectedProd),
            contactDetails: JSON.stringify(contact),
            paymentType : payment,
            scheduledDate: sd,
            CardId : card,
            totalPriceValue:totalPriceValue,
            clubbedPriceValue: component.get("v.totalClubbedValue"),
            nonClubbedPriceValue: component.get("v.totalNonClubbedValue")
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();
            var orderResp = JSON.stringify(response.getReturnValue());
            console.log('----------orderResp---------'+orderResp);
           
           
            if(orderResp!= JSON.stringify("Success")){
                component.set("v.errorMsg",orderResp);
                component.set("v.isError",true);
            }else{
            	if(state == "SUCCESS" ){
                    component.find("overlayLib").notifyClose();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "A new Order has been created.",
                        "type" : "success"
                    });
                    toastEvent.fire();            	
            	} else if (state == "ERROR"){
                    var errors = response.getError();
                    console.log(errors);
                    component.set("v.errorMsg",JSON.stringify(errors));
                    component.set("v.isError",true);
            	}       
            }
            
        });
        $A.enqueueAction(action);   
    },
    
    showToast : function(component, event) {
        console.log('toast called');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Card has been created successfully."
        });
        toastEvent.fire();
    },
    refreshCardList: function(component, event, contactId){
        console.log('card list called');
        console.log('contact Id is ',contactId);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var cardAction = component.get('c.cardListServer');
        cardAction.setParams({
            conId: contactId
        });
        cardAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside card list');
                var resultCard = response.getReturnValue();
                if(resultCard.length>0){
                    if(!resultCard[0].Error_Message__c && resultCard[0].Stripe_Card_Id__c){
                        component.set("v.newCard",false);
                        console.log('first card details',resultCard[0]);
                        console.log('result',resultCard);
                        console.log('card error Message',resultCard[0].Error_Message__c);
                        //var cardsList = resultCard;
                        var cardsList = [];
                        console.log('cards List array',cardsList);
                        for(var i=0;i<resultCard.length;i++){
                            if(resultCard[i].Credit_Card_Number__c && resultCard[i].Stripe_Card_Id__c){
                                console.log('cardsList Number',resultCard[i].Credit_Card_Number__c);
                                var cardNumber = resultCard[i].Credit_Card_Number__c;
                                resultCard[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                                console.log('credit card number',resultCard[i].Credit_Card_Number__c);
                                cardsList.push(resultCard[i]);
                            }
                        }
                        
                        component.set("v.card",cardsList);
                        console.log('default card from cardsList',cardsList[0].Credit_Card_Number__c);
                        component.set("v.defaultCard",cardsList[0]);
                        component.set("v.selectedCard",cardsList[0].Id);
                        var cardNumber = cardsList[0].Credit_Card_Number__c;
                        var displayCardNumber = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                        component.set("v.displayCardNumber",displayCardNumber);
                        
                    }
                    if(resultCard[0].Error_Message__c){
                        this.setCardInEditMode(component,event,resultCard);
                    }
                    
                }
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
            }
        });
        $A.enqueueAction(cardAction);
    },
    
    refreshPrimaryAddress : function(component,event,contactId){
        console.log('card list called');
        var primaryAddressAction = component.get('c.refreshContactAddressServer');
        primaryAddressAction.setParams({
            conId: contactId
        });
        primaryAddressAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside update primary address response');
                component.set("v.contact.address",response.getReturnValue());
                
                component.set("v.showSameAsShippingCheckbox",true);
                
                var displayAddress = response.getReturnValue().Shipping_Street__c+','+response.getReturnValue().Shipping_City__c+','+response.getReturnValue().Shipping_State_Province__c+',';
                displayAddress+=response.getReturnValue().Shipping_Country__c+','+response.getReturnValue().Shipping_Zip_Postal_Code__c+'.';
                
                component.set("v.displayAddress",displayAddress);
                //component.set("v.showSameAsShippingCheckbox",true);
                component.set("v.contactHasPrimaryAddress",true);    
            }
        });
        $A.enqueueAction(primaryAddressAction);
    },
    
    setCardInEditMode : function(component,event,resultCard){
        console.log('inside invalid card with error');
        //component.find("newCardChkBox").set("v.checked",true);
        component.set("v.newCard",true);
        console.log('in else new card name',resultCard[0].Name_On_Card__c);
        var newCardDetails = {};
        var cardId = resultCard[0].Id;
        component.set("v.newCardDetails",newCardDetails);
        component.set("v.newCardDetails.Name_On_Card__c",resultCard[0].Name_On_Card__c);
        component.find("cardType").set("v.value",resultCard[0].Card_Type__c);
        component.find("expMonths").set("v.value",resultCard[0].Expiry_Month__c);
        component.find("expYears").set("v.value",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Credit_Card_Number__c",resultCard[0].Credit_Card_Number__c);
        component.set("v.newCardDetails.Cvc__c",resultCard[0].Cvc__c);
        component.set("v.newCardDetails.Expiry_Month__c",resultCard[0].Expiry_Month__c);
        component.set("v.newCardDetails.Expiry_Year__c",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Card_Type__c",resultCard[0].Card_Type__c);
        component.set("v.newCardDetails.address.Billing_Street__c",resultCard[0].Billing_Street__c);
        component.set("v.newCardDetails.address.Billing_City__c",resultCard[0].Billing_City__c);
        component.set("v.newCardDetails.address.Billing_State_Province__c",resultCard[0].Billing_State_Province__c);
        component.set("v.newCardDetails.address.Billing_Country__c",resultCard[0].Billing_Country__c);
        component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",resultCard[0].Billing_Zip_Postal_Code__c);
        component.set("v.isUpdateCardWithError",true);
        component.set("V.errorCardId",cardId);
        component.set("v.cardErrorMsg",resultCard[0].Error_Message__c);
        //var spinner = component.find("mySpinner");
        //$A.util.toggleClass(spinner, "slds-hide");
        
    },
    
    correctCardDataHelper : function(component,event,errorCardId,newCardDetails){
        console.log('correct Card Detail Helper called');
        console.log('cardID',errorCardId);
        console.log('newcardDetials',newCardDetails.Name_On_Card__c);
    }
})