({
    init : function(component, event, helper) {
        console.log('init called');
        var recId = component.get("v.recordId");
        console.log('opportunity Id ---> '+recId);         
        helper.getInitValues(component,event,recId);
    },
    updateContact : function(component, event, helper) {
        
        var updatec = component.get("v.updateCon");
        console.log("updateCon---",updatec);
        component.set("v.updateCon",!updatec);
    },
    addNewAddress : function(component, event, helper) {
        var newadd = component.get("v.newAddress");
        component.set("v.addAddress.Shipping_Country__c",'US');
        
        console.log("newadd---",newadd);
        component.set("v.newAddress",!newadd);
        console.log("newadd-updated--",component.get("v.newAddress"));
    },
    addNewCard: function(component, event, helper) {
        var newCC = component.get("v.newCard");
        console.log("newCard---",newCC);
        component.set("v.newCard",!newCC);
        var cardDetails = {};
        component.set("v.newCardDetails",cardDetails);
        console.log("newcard-updated--",component.get("v.newCard"),component.get("v.newCardDetails"));
    },
    setBillingAddress: function(component, event, helper) {
        var newadd = component.get("v.newCardAddress");
        console.log('checked',event.getSource().get("v.checked"));
        var isChecked = event.getSource().get("v.checked");
        if(isChecked){
            var address = component.get("v.contact.address");
            component.set("v.newCardDetails.address.Billing_Street__c",address.Shipping_Street__c);
            component.set("v.newCardDetails.address.Billing_City__c",address.Shipping_City__c);
            component.set("v.newCardDetails.address.Billing_State_Province__c",address.Shipping_State_Province__c);
            component.set("v.newCardDetails.address.Billing_Country__c",address.Shipping_Country__c);
            component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",address.Shipping_Zip_Postal_Code__c);
            
        }
        else{
            component.set("v.newCardDetails.address.Billing_Street__c",'');
            component.set("v.newCardDetails.address.Billing_City__c",'');
            component.set("v.newCardDetails.address.Billing_State_Province__c",'');
            component.set("v.newCardDetails.address.Billing_Country__c",'');
            component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",'');
        }
        console.log("newadd---",newadd);
        component.set("v.newCardAddress",!newadd);
        console.log("newadd-updated--",component.get("v.newCardAddress"));        
    },
    closeOpiton: function (component, event, helper) {
        component.set("v.isRenderContactResults",false);
    },
    selectContact: function (component, event, helper) {
        console.log();
        var selectedItem = event.currentTarget.dataset.record;
        var contactId = event.currentTarget.dataset.value;
        console.log('selected Contact',selectedItem);
        console.log('selected Id',contactId);
        component.set("v.inputContact", selectedItem);
        component.set("v.contactId", contactId);
        component.set("v.isRenderContactResults",false);
        component.set("v.contactOptions",null);   
        helper.getSelectedContact(component, event, contactId);
    },
    contactkeyPressController: function(component,event,helper){
        var srchString = component.get("v.inputContact");
        if(srchString.length > 1){
            component.set("v.isRenderContactResults",true); 
            console.log('is render con',component.get("v.isRenderContactResults"))
            helper.getContactOptions(component,event);
        }
    },  
    updateConact : function(component, event, helper) {
        var contact =  component.get("v.contact");
        var cId = component.get("v.contactId");
        console.log('update contact ---> ',contact,cId);
        helper.updateContactInfo(component, event);        
    },
    primaryAddress : function(component, event, helper) {
        var updatec = component.get("v.isPrimaryAddress");
        console.log("updateCon---",updatec);
        component.set("v.isPrimaryAddress",!updatec);
    },
    onSelectCard : function(component, event, helper) {
        console.log('on select card called');
        var selected = component.find("cardlevels").get("v.value");
        component.set("v.selectedCard", selected);
        console.log('Card selected value -->'+component.get("v.selectedCard"));
        var card = [];
        var selectedCardId='';
        card = component.get("v.card");
        for(var i=0;i<card.length;i++){
            if(card[i].Id == selected){
                var cardNumber = card[i].Credit_Card_Number__c;
                var displayCardNumber = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                component.set("v.displayCardNumber",displayCardNumber);
                selectedCardId = card[i].Id;
                break;
            }
        }
        console.log('-------------------Before Calling Update Card------------'+selectedCardId);
        var action = component.get('c.updateCardInStripe');
        action.setParams({
            cardId: selectedCardId
        });
        $A.enqueueAction(action);  
        console.log('-------------------After Calling Update Card------------'+selectedCardId);

    },
    addAddress : function(component, event, helper) {
        console.log('in add address');
        var address =  component.get("v.addAddress");
        console.log("address---> ",address.Shipping_Street__c);
        var cId = component.get("v.contactId");
        var pA = component.get("v.isPrimaryAddress");
        console.log('add new Address --> ',address,cId);
        if(pA){
            if (confirm('Are you sure you want to save this as Primary Address?')) {
                helper.createAddress(component, event, address,cId,pA);
            }
        }else{
            helper.createAddress(component, event, address,cId,pA);
        }
    },
    getIndex: function (component, event) {
        var selectedItem = event.currentTarget; // Get the target object
        var index = selectedItem.dataset.record; 
        console.log('index value if recorded',index);
        component.set("v.indexVar",index);
    },
    handleProductChange: function (component, event,helper) {
        console.log('handle producct change');
        component.set("v.errorMsg",'');
        component.set("v.isError",false);
        var checkedProductList = component.get("v.checkedProductList");
        var productChecked = event.getSource().get("v.checked");
        console.log('productCHeckddd',productChecked);
        if(checkedProductList.length==1 && productChecked){
            event.getSource().set("v.checked",false);
            component.set("v.errorMsg",'You can not add more than 1 products.');
            component.set("v.isError",true);
        }
        else{
            var changeValue = event.getSource();
            console.log(changeValue);
            var name = changeValue.get("v.name");
            var index = component.get("v.indexVar")// Get its value i.e. the index
            console.log('index in product change',index);
            console.log('product checkbox value',event.getSource().get("v.checked"));
            
            if(productChecked && checkedProductList.length<2)
                checkedProductList.push(index);
            if(!productChecked && checkedProductList.length>0)
                checkedProductList.pop();
            
            console.log('checkedProductList',checkedProductList);
            var productList = component.get("v.productOptions");
            var selectedOption = component.get("v.productOptions")[index]; // Use it retrieve the store record 
            selectedOption.checkValue = changeValue.get("v.checked");
            
            console.log('checkbox clicked value ',selectedOption.checkValue);
            if(selectedOption.isSwitched==true && selectedOption.isSelectable==true){
                console.log('inside handle product switched true');
                var productName = selectedOption.Name;
                var productPrice = selectedOption.price;
                var productId = selectedOption.Id;
                var productClub = selectedOption.club;
                var productQuantitySelector = selectedOption.quanititySelector;
                selectedOption.Name = selectedOption.switchedProductName;
                selectedOption.price = selectedOption.switchedProductPrice;
                selectedOption.Id = selectedOption.switchedProductId;
                selectedOption.club = selectedOption.switchedClub;
                selectedOption.switchedProductName = productName;
                selectedOption.switchedProductPrice = productPrice;
                selectedOption.switchedProductId = productId;
                selectedOption.switchedClub = productClub;
                selectedOption.switchedquanititySelector = productQuantitySelector;
                selectedOption.isSwitched = false;
                
            }
            productList[index] = selectedOption;
            component.set("v.productOptions",productList);
            console.log('in handle product',selectedOption);
            helper.setTotalPrice(component, event);
        }
    },
    addQuantity: function (component, event,helper) {
        var changeQuantity = event.getSource();
        var val = changeQuantity.get("v.value");
        var index = component.get("v.indexVar")
        console.log(val,index);
        var selectedOption = component.get("v.productOptions")[index]; 
        selectedOption.quantity = val;        
        selectedOption.quantityPrice = selectedOption.price * selectedOption.quantity;
        console.log('Quantity changed values ',selectedOption.price , selectedOption.quantity);
        console.log('Quantity changed ',selectedOption);
        
        helper.setTotalPrice(component, event);
    },
    handleClub: function (component, event,helper) {
        var changeValue = event.getSource();
        var index = component.get("v.indexVar");
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index]; 
        selectedOption.club = changeValue.get("v.checked");
        var subscribedProductList = component.get("v.subscribedProductList");
        var isSwitchedProductSelectable = true;
        if(subscribedProductList){
            for(var i=0;i<subscribedProductList.length;i++){
                if(subscribedProductList[i]==selectedOption.switchedProductId){
                    console.log('inside found switched list');
                    isSwitchedProductSelectable = false;
                    break;
                }
            }
        }
        console.log('club changed ',selectedOption);
        var discount = component.get("v.discount");
        var price = selectedOption.price * selectedOption.quantity;
        console.log('price value',price);
        var productName = selectedOption.Name;
        var productPrice = selectedOption.price;
        var productId = selectedOption.Id;
        var productClub = selectedOption.club;
        var productQuantitySelector = selectedOption.quanititySelector;
        selectedOption.Name = selectedOption.switchedProductName;
        selectedOption.price = selectedOption.switchedProductPrice;
        selectedOption.Id = selectedOption.switchedProductId;
        selectedOption.club = selectedOption.switchedClub;
        selectedOption.quanititySelector = selectedOption.switchedquanititySelector;
        selectedOption.switchedProductName = productName;
        selectedOption.switchedProductPrice = productPrice;
        selectedOption.switchedProductId = productId;
        selectedOption.switchedClub = productClub;
        selectedOption.switchedquanititySelector = productQuantitySelector;
        selectedOption.isSwitched = true;
        selectedOption.isSelectable = isSwitchedProductSelectable;
        if(selectedOption.isSelectable==false){
            console.log('make check false');
           // console.log('checkbox id value', component.find(productId).set("v.checked",false));
            selectedOption.checkValue = false;
            var checkedProductList = component.get("v.checkedProductList");
            checkedProductList.pop();
            component.set("v.checkedProductList",checkedProductList);
        }
        productList[index] = selectedOption;
        component.set("v.productOptions",productList);
        console.log('in handle club',selectedOption);
        helper.setTotalPrice(component, event);
    },
    updateCard: function(component, event, helper) {
        console.log('in add billing address');
        console.log('new card details',component.get("v.newCardDetails"));
        var cId = component.get("v.contactId");
        var card = component.get("v.newCardDetails");
        console.log('card details in update card',card);
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var hasError = false;
        if(!card.Name_On_Card__c || !card.Credit_Card_Number__c || !card.Expiry_Month__c && 
           !card.Expiry_Year__c || !card.Card_Type__c || !card.Cvc__c){
            console.log('fields are missing');
            component.set("v.cardErrorMsg","Please fill all the required fields before adding the card.");
            component.set("v.isCardError",true);
            hasError = true;
        }
        if(card.Credit_Card_Number__c && card.Card_Type__c){
            if(card.Card_Type__c=="tok_amex"){
                if(card.Credit_Card_Number__c.length!=15){
                    console.log('amex card length not 15');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 15 digits for American Express.");
                    component.set("v.isCardError",true);
                    hasError = true;
                }
                if(card.Cvc__c.length!=4 && !hasError){
                	console.log('amex card CVC not 4');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 4 digits for American Express.");
                    component.set("v.isCardError",true);
                    hasError = true;    
                }
            }else if(card.Card_Type__c=="tok_diners"){
            	if(card.Credit_Card_Number__c.length!=14){
                    console.log('dinners card length not 14');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 14 digits for Dinners Club.");
                    component.set("v.isCardError",true);
                    hasError = true;
                } 
                if(card.Cvc__c.length!=3 && !hasError){
                	console.log(card.Card_Type__c.length);
                    
                    console.log('Dinner,s club  card CVC  not 3');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 3 digits for Card Type Selected.");
                    component.set("v.isCardError",true);
                    hasError = true;    
                }
            }
            else{
                if(card.Credit_Card_Number__c.length!=16){
                    console.log('other card length errorMsg');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 16 digits.");
                    component.set("v.isCardError",true);
                    hasError = true;
                }
                if(card.Cvc__c.length!=3 && !hasError){
                	console.log('All Other  card CVC  not 3');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 3 digits for card Type Selected.");
                    component.set("v.isCardError",true);
                    hasError = true;    
                }
            }
        }
        
        if(!hasError)
            helper.newCardWithBilling(component,event,card,cId);
    },
    
    setScheduleDate :  function(component, event) {
        console.log(component.get("v.scheduledDate"));  
    },
    
    verifyDetails: function(component, event, helper) {
        component.set("v.isError",false);
        component.get("v.isCardError",false);
        component.set("v.erroMsg",'');
        component.set("v.cardErrorMsg",'');
        var payment = component.get("v.selectedPay"); 
        var isNewCard = component.get("v.newCard");
        var sd = component.get("v.scheduledDate");
        var isPrimaryAddress =  component.get("v.contact.address.Shipping_City__c");
        console.log('------------isPrimaryAddress----------'+isPrimaryAddress);
        if(isPrimaryAddress == undefined){
        	 component.set("v.errorMsg",'Please add Primary shipping address');
             component.set("v.isError",true);    
            
        }
        
        console.log('isNewCard',isNewCard);
        console.log('selected pay method',payment);
        if(payment == undefined){
            component.set("v.errorMsg",'Select Payment');
            component.set("v.isError",true);
        }
        console.log('------------payment----------'+payment);
        if(payment==true){
            console.log('Inside scheduled payment');
            console.log('selected date ',sd);
            if(!sd){
                component.set("v.errorMsg",'Enter Date to Schedule Payment');
                component.set("v.isError",true);
            }
        }
        
        var selectedProd = [];
        var prodtypes = [];
        var prodItems = component.get("v.productOptions");
        console.log('prodItem',prodItems);
        for(var i = 0; i < prodItems.length; i++){
            console.log('prodItems',prodItems[i]);
            if(prodItems[i].checkValue == true && prodItems[i].quantity == 0 && prodItems[i].quanititySelector){
                component.set("v.errorMsg",'Enter Quantity for Selected Products');
                component.set("v.isError",true);
                break;
            }
            if(prodItems[i].checkValue == true ){
                console.log(prodItems[i].checkValue);
                selectedProd.push(prodItems[i]);
            }            
        }
        console.log('sel items',selectedProd);
        var contact = component.get("v.contact");
        var card = component.get("v.selectedCard");
        console.log('card value',card);
        if(card == '--- Select Card ---' && !isNewCard){
            console.log('inside card error');
            component.set("v.cardErrorMsg",'Please Select a Card');
            component.set("v.isCardError",true);
        }
        var totalPrice = component.get("v.totalPriceValue");
        if(!totalPrice > 0 ){
            console.log('selectedProd',selectedProd.length);
            component.set("v.errorMsg",'Select products to Order');
            component.set("v.isError",true);
        }
        
        var totalPriceValue = component.get("v.totalPriceValue");
        var isCardError = component.get("v.isCardError");
        var isError = component.get("v.isError");
        
        if(totalPrice > 0 && payment != undefined && !isCardError && !isError && isPrimaryAddress != undefined){
            helper.createOrder(component, event,selectedProd,payment,sd,card,contact,totalPriceValue);
        }
    },
    
    onSelectPickList : function(component,event,helper){
        var auraId = event.getSource().getLocalId();
        var pickListValue = component.find(auraId).get("v.value");
        console.log('auraId',auraId);
        console.log('picklistValue',pickListValue);
        
        if(auraId=='expMonths'){
            
            if(pickListValue=='--None--')
                component.set("v.newCardDetails.Expiry_Month__c",'')
                else
                    component.set("v.newCardDetails.Expiry_Month__c",pickListValue);
        }
        else if(auraId=='expYears'){
            
            if(pickListValue=='--None--')
                component.set("v.newCardDetails.Expiry_Year__c ",'')
                else
                    component.set("v.newCardDetails.Expiry_Year__c",pickListValue);
        }
            else if(auraId=='cardType'){
                if(pickListValue=='--None--')
                    component.set("v.newCardDetails.Card_Type__c",'');
                else
                    component.set("v.newCardDetails.Card_Type__c",pickListValue);
            }
        
        console.log('new card details months',component.get("v.newCardDetails.Expiry_Month__c"));
        console.log('new card details years',component.get("v.newCardDetails.Expiry_Year__c"));
        console.log('new card details card type',component.get("v.newCardDetails.Card_Type__c"));
    },
    
    onRefreshCard : function(component,event,helper){
        console.log('on click refresh');
        helper.refreshCardList(component,event,component.get("v.contactId"));
    },
    
    correctCardData : function(component,event,helper){
        console.log('correct card data called');
        console.log('cardId',component.get("v.errorCardId"));
        console.log('cardDetail omonth',component.get("v.newCardDetails.Expiry_Month__c"));
        var cardDetails = component.get("v.newCardDetails");
        helper.updateCardWithBilling(component,event,cardDetails,component.get("v.errorCardId"));
    },
    onCancel : function(component,event,helper){
        debugger
        component.find("newCardChkBox").set("v.checked" , false);
        var newCC = component.get("v.newCard");
        console.log("newCard---",newCC);
        component.set("v.newCard",!newCC);

    },
    cvcInput :function(component,event,helper){
        var val = event.getSource().get("v.value");
        debugger
        if(isNaN(val)){
            component.set("v.NumValidationError", true);
        }
    }
})