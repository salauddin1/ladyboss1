({
    transferToRecovery : function (component, event, helper) {
        console.log('inside do closeCaseManually');
        var action = component.get("c.transferOwnership");
        action.setParams({
            caseId : component.get("v.caseId")
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
})