({
    doInit: function (component, event, helper) {
        var action = component.get("c.getConDetails");
        action.setParams({
            conID: component.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            var totalBal = 0.00;
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.StripeProfiles", result);
                for (var i = 0; i < result.length; i++) {
                    totalBal += result[i].balance;
                }
                component.set("v.totalBalance", totalBal.toFixed(2));
                component.set("v.Spinner1", false);
                component.set("v.Spinner", false);
            }

            console.log(response);
        }));
        $A.enqueueAction(action);
    },
    onSelectProfile: function (cmp, event, helper) {
        var idparam = event.getSource().get("v.value");
        console.log("hello", idparam);
        var profiles = cmp.get("v.StripeProfiles");
        for (var i = 0; i < profiles.length; i++) {
            if (profiles[i].id == idparam) {
                cmp.set("v.StripeProfile", profiles[i]);
                cmp.set("v.newBalance", profiles[i].balance);
            }
        }
        cmp.set("v.isSelected", true);
    },
    updateBalance: function (cmp, event, helper) {
        cmp.set("v.openUpdate", true);
    },
    closeUpdate: function (cmp, event, helper) {
        cmp.set("v.openUpdate", false);
        cmp.set("v.balanceToAdd", null);
        cmp.set("v.isUpdated", false);
    },
    changeBalance: function (cmp, event, helper) {
        var curBalance = parseFloat(cmp.get("v.StripeProfile").balance);
        var addedBalance = parseFloat(cmp.get("v.balanceToAdd"));
        console.log('addedbal', addedBalance);

        if (isNaN(addedBalance) || addedBalance == null) {
            addedBalance = 0.00;
        }
        if (addedBalance != 0.00) {
            cmp.set("v.isUpdated", true);
        } else {
            cmp.set("v.isUpdated", false);
        }
        var newBalance = curBalance + addedBalance;
        cmp.set("v.newBalance", newBalance);
    },
    balanceUpdate: function (cmp, event, helper) {
        cmp.set("v.Spinner1", true);
        console.log('toupdate', cmp.get("v.StripeProfile"));
        console.log('toupdate1', cmp.get("v.balanceToAdd"));
        var action = cmp.get("c.UpdateBalance");
        action.setParams({
            con: JSON.stringify(cmp.get("v.StripeProfile")),
            amount: cmp.get("v.balanceToAdd"),
            newBalance: cmp.get("v.newBalance")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            var title = 'Error';
            var type = 'error';
            var msg = 'Unknown error Occured contact your system administrator';
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result == 'Success') {
                    title = 'Success';
                    type = 'success';
                    msg = 'Successfully Updated Customer Balance';
                }

            }
            var toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({
                title: title,
                message: msg,
                duration: ' 2000',
                key: 'info_alt',
                type: type,
                mode: 'pester'
            });
            toastEvent.fire();
            console.log(response);
            cmp.set("v.openUpdate", false);
            var a = cmp.get('c.doInit');
            $A.enqueueAction(a);
        }));
        $A.enqueueAction(action);
    },
    balanceRefresh: function (cmp, event, helper) {
        cmp.set("v.Spinner", true);
        var action = cmp.get("c.refreshBalance");
        action.setParams({
            conID: cmp.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            var title = 'Error';
            var type = 'error';
            var msg = 'Unknown error Occured contact your system administrator';
            if (state == "SUCCESS") {
                title = 'Success';
                type = 'success';
                msg = 'Successfully Refreshed Customer Balance';
            }
            var toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({
                title: title,
                message: msg,
                duration: ' 2000',
                key: 'info_alt',
                type: type,
                mode: 'pester'
            });
            toastEvent.fire();
            console.log(response);
            var a = cmp.get('c.doInit');
            $A.enqueueAction(a);
        }));
        $A.enqueueAction(action);
    }
})