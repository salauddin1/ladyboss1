({
    doInit : function(component, event, helper) {
        //debugger
         var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var contactId = a[6];
        var numberOfRows = [];
        var multipleLookup = [];
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        
        component.set('v.multipleLookups',multipleLookup);
        
        numberOfRows.push('0');
        component.set('v.numberOfRows', numberOfRows);
        component.set('v.rowIndex', 1);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        component.set("v.newContact.Modify_Date__c",today); 
        var action = component.get("c.getContact");
        action.setParams({ contactId :contactId});
        
        action.setCallback(this, function(response) {
            debugger
            var a = response.getReturnValue();
            component.set("v.contactVal",a);
            debugger
            component.set("v.newContact.Contact__c",contactId);
            component.set("v.newContact.Email__c",a[0].Email);
            component.set("v.newContact.First_Name__c",a[0].FirstName);
            component.set("v.newContact.Last_Name__c",a[0].LastName);
            component.set("v.newContact.phone__c",a[0].Phone);
            if(a[0].Phone === undefined){
                component.set("v.newContact.phone__c",a[0].MobilePhone);
            }
            if(a[0].Addresses__r !== undefined){
                component.set("v.newContact.address1__c",a[0].Addresses__r[0].Shipping_Street__c);
                component.set("v.newContact.city__c",a[0].Addresses__r[0].Shipping_City__c);
                component.set("v.newContact.zip__c",a[0].Addresses__r[0].Shipping_Zip_Postal_Code__c);
                component.set("v.newContact.province__c",a[0].Addresses__r[0].Shipping_State_Province__c);
                
            }
            component.set("v.newContact.country__c","US");
            component.set("v.newContact.Send_Receipt__c",false);
            component.set("v.newContact.Send_Fulfillment_Receipt__c",false);
            component.set("v.newContact.Financial_Status__c",'Paid');
            component.set("v.newContact.fulfillment_Status__c",'null');
        });
        $A.enqueueAction(action);
    },
    remove :function(component, event, helper) {
        debugger
        if(event.target.parentNode.getElementsByClassName("value_span")[0] !== undefined){
            var selectItemList = component.get("v.selectItemLists");
            var removeVal = event.target.parentNode.getElementsByClassName("value_span")[0].innerHTML;
            var newSel = selectItemList.filter(a => a !== removeVal);
            component.set("v.selectItemLists",newSel);
            var multiVal = component.get("v.multipleLookups");
            console.log(JSON.stringify(multiVal));
            
            multiVal = multiVal.filter(obj => (obj.selectedItem && obj.selectedItem['text'] !== removeVal) || !obj.selectedItem)
           console.log(JSON.stringify(multiVal));
            component.set("v.multipleLookups",multiVal);           
        } 
        else{
            var a = component.get("v.selectItemLists");
            a.push(" ");
            component.set('v.selectItemLists',a);
            event.target.parentNode.remove();
        }
    },
    save :function(component, event, helper){
        debugger
        var validExpense = component.find('orderform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if(validExpense){
            
            var newContact = component.get("v.newContact");
            console.log("Create newContact: " + JSON.stringify(newContact));
            var newContact1 = JSON.stringify(newContact);
            var currentcard =component.get("v.currentCard")
            var newProduct = component.get("v.newProduct");
            console.log("Create newProduct: " + JSON.stringify(newProduct));
            var items = component.get('v.multipleLookups');
            var idListJSON=JSON.stringify(items);
            items.forEach(function(item) {
                console.log(item.selectedItem);
            });
            var contactId = component.get("v.recordId");
            var action = component.get("c.CreateShopifyOrders");
            action.setParams({ OrderId:newContact1,
                              listItems :idListJSON,
                              cardId : currentcard.Id});
            debugger
            action.setCallback(this, function(response) { 
                 var result = response.getReturnValue();
                 console.log(result);
               if(result == null) {
                 component.find("overlayLib1").notifyClose();
                 var toastEvent = $A.get("e.force:showToast");
                 toastEvent.setParams({
                    title : 'Success ',
                    message: 'Order succeesful created',
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
               }
                else{
                    component.set("v.FailedCardPayment",result);
                }
            });
            $A.enqueueAction(action);
        } 
    },
    show: function(component, event, helper) {
        debugger
        var items = component.get('v.multipleLookups');
        items.forEach(function(item) {
            console.log(item.selectedItem);
        });
        
    },
    Add : function(component, event, helper) {
        var multipleLookup = component.get("v.multipleLookups");
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        component.set('v.multipleLookups',multipleLookup);
    },
    checkVal:function(component, event, helper) {
        debugger
        var valCh = event.getSource().get("v.checked");
        component.set("v.newContact.Send_Receipt__c",valCh);
    },
    dateF:function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value");
    },
    cancel:function(component, helper) {
		component.find("overlayLib1").notifyClose();
    },
    showSpinner : function(component,event,helper){
      // display spinner when aura:waiting (server waiting)
        component.set("v.toggleSpinner", true); 
      },
    hideSpinner : function(component,event,helper){
   // hide when aura:downwaiting
        component.set("v.toggleSpinner", false);
    },
    onRefreshCard : function(component,event,helper){
        console.log('on click refresh');
        helper.refreshCardList(component,event,component.get("v.recordId"));
    },
    cardUpdate : function(component,event,helper){
        debugger
        var indexvar = event.getSource().get("v.value");
        component.set("v.currentCard", null);
        console.log('===='+indexvar);
        component.set("v.currentCard", indexvar);
        console.log('===='+JSON.stringify(indexvar));
        //helper.updateCardHelper(component,event,indexvar);
    },
    productData : function(component, event) {
        debugger
        var total = component.get("v.totalAmt");
        var ShowResultValue = event.getParam("Price");
        var ShowRemovableValue = event.getParam("RemovePrice");
        console.log('========price from lookup'+ShowResultValue);
        console.log('========price from lookup'+ShowRemovableValue);
        console.log('========price from lookup'+parseInt(total));
        console.log('========price from lookup'+parseInt(ShowRemovableValue));
        
        if(ShowResultValue != undefined) {
        total = parseFloat(total) + parseFloat(ShowResultValue);
        component.set("v.totalAmt", parseFloat(total).toFixed(2));
        console.log('total'+total);
       }
        if(ShowRemovableValue != undefined) {
            if(Math.round(parseFloat(total)) > Math.round(parseFloat(ShowRemovableValue))) {
            total = parseFloat(total) - parseFloat(ShowRemovableValue);
            component.set("v.totalAmt", '');
            component.set("v.totalAmt", parseFloat(total).toFixed(2));
            }
            else {
                
                component.set("v.totalAmt", '0');
            }
        }
        if(Math.round(parseInt(total)) === Math.round(parseInt(ShowRemovableValue))) {
            component.set("v.totalAmt", '0');
        }
    }
})