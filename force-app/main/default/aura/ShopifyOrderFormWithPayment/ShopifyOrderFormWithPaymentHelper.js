({
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    refreshCardList: function(component, event, contactId){
        console.log(contactId);
       var action = component.get("c.getCard");
        action.setParams({ contactId :contactId});
        
        action.setCallback(this, function(response) {
            var card =response.getReturnValue(); 
         if(card != null && response.getState() == "SUCCESS"){
            console.log("===card====>"+JSON.stringify(card));
            var size = card.length;
                console.log(size+"====>"+JSON.stringify(card));
            //component.set("v.currentCard", card[size-1]);
             component.set("v.cards",card); 
         }
            });
        $A.enqueueAction(action);
       
    },
   /* updateCardHelper : function(component, event, card){
         console.log(card);
       var action = component.get("c.updateCard");
        action.setParams({ cardId :card.Id});
        
        action.setCallback(this, function(response) {
            var carddata =response.getReturnValue();
            if(carddata != null && response.getState() == "SUCCESS"){
             
            console.log("===card====>"+JSON.stringify(carddata));
            }   
            });
        $A.enqueueAction(action);
    }*/
})