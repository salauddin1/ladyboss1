({
    openmodal:function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    subscriptionData :function(component,event,helper){
        var recordId = component.get('v.recordId');
        var recordName = component.get('v.recordName');
        var action = component.get("c.getStripeLineItem");
        action.setParams({ recordId :recordId,
                          recordName:recordName});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            console.log("Failed with state: " + state);
            var productData = response.getReturnValue();
            component.set("v.productValue",productData);
        }); 
        $A.enqueueAction(action);
    },
    checkButtonTag :function(component,event,helper){
        var recordId = component.get('v.recordId');
        var recordName = component.get('v.recordName');
        var action2 = component.get("c.setButtonTag");
        action2.setParams({ recordId :recordId,
                           recordName: recordName});
        debugger
        action2.setCallback(this, function(response) {
            var checkVal = response.getReturnValue();
            if(checkVal == true){
                component.set('v.lifeButtonCheck',true);
                component.set('v.removeLifeButtonCheck',false);
                component.set('v.lifeButtonTag','Already Added Backdoor Metadata Tag');
            }
            else{
                component.set('v.lifeButtonCheck',false );
                component.set('v.removeLifeButtonCheck',true);
                component.set('v.lifeButtonTag','Enable Trainer Access');
            }
            
            
        });
       
        $A.enqueueAction(action2);
        
        
    },
    toastMsg :function(component,event,helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success ',
            message: 'Note successfuly added',
            duration:' 2000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        
    },
    showNote :function(component,event,helper){
        debugger
        var recordName = component.get('v.recordName');
        component.set('v.showNotes',true);
        var recordId = component.get('v.recordId');
        var action = component.get("c.getAllCustomer");
        action.setParams({ conId :recordId,
                          recordName: recordName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            var noteLength = response.getReturnValue().length;
            if(noteLength == 0){
                component.set('v.noteLength',true);  
            }
            else if(noteLength > 0){
                component.set('v.noteLength',true);  
                component.set('v.NoteVal',response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
    }
})