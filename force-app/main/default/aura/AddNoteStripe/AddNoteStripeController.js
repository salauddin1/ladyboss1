({
    doInit:function(component,event,helper){
        debugger
        var recordId = component.get('v.recordId');
        var action = component.get("c.findObjectAPIName");
        action.setParams({ recordId :recordId});
        action.setCallback(this, function(response) {
            var chec = response.getReturnValue();
            component.set('v.recordName',chec);
            helper.checkButtonTag(component,event,helper);
        });
        
        $A.enqueueAction(action);
        
        
    },
    showNote:function(component,event,helper){
        debugger
        helper.showNote(component,event,helper);
    },
    handleEdit : function(component,event,helper){
        debugger
        var indexId = event.getSource().get('v.value');
        component.set('v.indexId',indexId);
    },
    
    addNote : function(component, event, helper) {
        var validExpense = component.find('noteId').get('v.value');
        if(validExpense != undefined && validExpense != '' ){
            var recordId = component.get('v.recordId');
            var recordName = component.get('v.recordName');
            debugger
            var action = component.get("c.getStripeCustomer");
            action.setParams({ recordId :recordId,
                              recordName: recordName});
            action.setCallback(this, function(response) {
                debugger
                var state = response.getState();
                console.log("Failed with state: " + state);
                if (state === "SUCCESS") {
                    component.set("v.stripeName", response.getReturnValue());
                    helper.subscriptionData(component,event,helper);
                    if(response.getReturnValue().length == 1){
                        var StripeId = response.getReturnValue()[0].Id;
                        var note = component.find('noteId').get("v.value");
                        var stripeProfile = response.getReturnValue()[0].Stripe_Customer_Id__c;
                        var action = component.get("c.updateCustomerMetdata");
                        action.setParams({ metaVal :note,
                                          custId :stripeProfile,
                                          recordId: recordId,
                                          StripeId: StripeId,
                                          recordName: recordName});
                        
                        action.setCallback(this, function(response) {
                            var state = response.getState();
                            console.log("Failed with state: " + state);
                            helper.toastMsg(component,event,helper);
                            component.find('noteId').set("v.value",'');
                            if(component.get("v.showNotes") == true){
                                $A.get('e.force:refreshView').fire();
                            }
                        });
                        
                        $A.enqueueAction(action);
                        
                    }
                    else if(response.getReturnValue().length == 0){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: 'There is no stripe customer',
                            duration:' 2000',
                            key: 'info_alt',
                            type: 'Error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                        
                    }
                        else{
                            helper.openmodal(component,event,helper);
                        }
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });
            
            $A.enqueueAction(action);
        }   
    },
    lifeTime : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var recordName = component.get('v.recordName');
        debugger
        var action = component.get("c.setLifeTimeCust");
        action.setParams({ recordId :recordId,
                          recordName: recordName});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success',
                        message: 'Successfully Enabled Lifetime...',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'Success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    
                    component.set('v.lifeButtonCheck',true);
                    component.set('v.removeLifeButtonCheck',false);
                    component.set('v.lifeButtonTag','Already Added Backdoor Metadata Tag');
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message: 'There is no stripe customer',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'Error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: 'There is some issue please contact your Administrator',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'Error',
                    mode: 'pester'
                });
                toastEvent.fire();
                console.log("Failed with state: " + state);
            }
        });
        
        $A.enqueueAction(action);
    },
    removeLifeTime : function(component, event, helper) {
        var recordId = component.get('v.recordId');
        var recordName = component.get('v.recordName');
        debugger
        var action = component.get("c.removeLifeTimeCust");
        action.setParams({ recordId :recordId,
                          recordName: recordName});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success',
                        message: 'Successfully Removed Lifetime...',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'Success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    
                    component.set('v.lifeButtonCheck',false);
                    component.set('v.removeLifeButtonCheck',true);
                    component.set('v.lifeButtonTag','Enable Trainer Access');
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message: 'There is no stripe customer',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'Error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message: 'There is some issue please contact your Administrator',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'Error',
                    mode: 'pester'
                });
                toastEvent.fire();
                console.log("Failed with state: " + state);
            }
        });
        
        $A.enqueueAction(action);
    },
    done:function(component,event,helper){
        debugger
        var recordName = component.get('v.recordName');
        var recordId = component.get('v.recordId');
        var StripeId = component.get('v.StripeId');
        var note = component.find('noteId').get("v.value");
        var stripeProfile = component.get("v.valueRadio");
        var action = component.get("c.updateCustomerMetdata");
        action.setParams({ metaVal :note,
                          custId :stripeProfile,
                          recordId: recordId,
                          StripeId: StripeId,
                          recordName: recordName});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            var cmpTarget = component.find('Modalbox');
            $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
            helper.toastMsg(component,event,helper);
            component.find('noteId').set("v.value",'');
            if(component.get("v.showNotes") == true){
                $A.get('e.force:refreshView').fire();
            }
            
        });
        
        $A.enqueueAction(action);
        
    },
    closing:function(component,event,helper){ 
        var cmpTarget = component.find('Modalbox');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
    },
    RadioCheck:function(component,event,helper){
        debugger
        var rVal = event.getSource().get('v.value');
        var stripeId =  event.getSource().get('v.id');
        component.set('v.StripeId',stripeId);
        component.set('v.valueRadio',rVal);
    },
    updateValue:function(component,event,helper){
        var value = event.getSource().get('v.value');
        component.set("v.updatedVal",value);
    },
    updateNote:function(component,event,helper){
        var noteId = event.getSource().get('v.value');
        var recordName = component.get('v.recordName');
        var StripeId = event.getSource().get('v.title');
        var recordId = component.get('v.recordId');
        var updateNote = component.get('v.updatedVal');
        var action = component.get("c.updateObject");
        action.setParams({ metaVal :updateNote,
                          recordId: recordId,
                          custNoteId: noteId,
                          StripeId: StripeId,
                          recordName:recordName});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            component.set('v.indexId','ff');
            component.set('v.updatedVal',' ');
            helper.showNote(component,event,helper);
        });
        $A.enqueueAction(action);
        
    }
    
    
})