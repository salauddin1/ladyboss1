({
    createAdd : function(component, event, helper) {
        var addCaseId = component.get("v.recordId");
        var value = component.find("textId").get("v.value");
        if(value != ""){
            var text = component.find("textId").get("v.value");
            debugger
            var action = component.get("c.getTask");
            action.setParams({ "caseId" :addCaseId, "noteData":text});
            
            action.setCallback(this, function(response) {
                debugger
                var a = response.getReturnValue()
                
            });
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
    	        "type":'success',
                "title": "Success!",
                "message": "The Note has been Added successfully."
            });
            toastEvent.fire();
            $A.enqueueAction(action);
            $A.get('e.force:refreshView').fire();
            component.find("overlayLib").notifyClose();
        }
	},
    closeModel:function(component, event, helper) {
		component.find("overlayLib").notifyClose();	
	}
})