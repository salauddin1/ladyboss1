({
	init : function(component, event, helper) {
        console.log('init');
        var taskId = component.get("v.recordId");
        console.log("taskId---"+taskId);
        
        var action2 = component.get("c.getTask"); 
        action2.setParams({ taskId :taskId});
        action2.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            component.set("v.task", response.getReturnValue());
        });
        $A.enqueueAction(action2);
    },
    
     rollbackOpp: function( cmp, apexAction, params ) {
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action                          = cmp.get("c."+apexAction+"");
            action.setParams( params );
            action.setCallback( this , function(callbackResult) {
                if(callbackResult.getState()=='SUCCESS') {
                    resolve( callbackResult.getReturnValue() );
                }
                if(callbackResult.getState()=='ERROR') {
                    console.log('ERROR', callbackResult.getError() ); 
                    reject( callbackResult.getError() );
                }
            });
            $A.enqueueAction( action );
        }));            
        return p;
    },
    
    createOrder: function(component, event,task,AddRemoveDaysval,fieldNameForDaysVal){
        console.log('=----task==='+JSON.stringify(task));
        console.log('=----task==='+AddRemoveDaysval);
        this.rollbackOpp(component,'saveTask',{"task": task,AddRemoveDays : AddRemoveDaysval,fieldNameForDays : fieldNameForDaysVal })
        .then(function(result){
            if(result != null){
                console.log('result====2'+JSON.stringify(result))
                component.set("v.task", result);
                $A.get('e.force:refreshView').fire();
				//this.init(component, event, helper);
            }
           
             
        });
        
    }

})