({
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    
    /*doInit: function(component,event,helper){
        console.log('hereeeee');
        var action = component.get('c.getArticlesdummy');
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS' && component.isValid()){
                var map = response.getReturnValue();
                var parsingWarpper=[];
                console.log(map);
                
                Object.keys(map).forEach(function(key) {
                    var individualElement = {};
                    individualElement.Id = key;
                    individualElement.Value =[];
                    var innerMapObject =map[key]; 
                    
                    console.log(innerMapObject);
                    Object.keys(innerMapObject).forEach(function(key2) {
                        console.log(key2);
                        var innerIndivididualElement ={};
                        innerIndivididualElement.Key = key2;
                        innerIndivididualElement.Value = innerMapObject[key2];
                        
                        individualElement.Value.push(innerIndivididualElement);
                    });  
                    parsingWarpper.push(individualElement);
                });
                console.log(parsingWarpper);
                component.set("v.myMap",parsingWarpper);
                
                //component.set('v.conList', response.getReturnValue());
            }else{
                console.debug(response.error[0].message);
            }
        });
        $A.enqueueAction(action);
    },*/
    
    
    /*likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('thanks for like Us :)');
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getAllTabInfo().then(function(response) {
            console.log('----workspace'+JSON.stringify(response));
        })
        .catch(function(error) {
            console.log(error);
        });
        var navigationItemAPI = component.find("navigationItemAPI");
        navigationItemAPI.getNavigationItems().then(function(response) {
            console.log(response);
        })
        .catch(function(error) {
            console.log(error);
        });
        console.log('--->');
        component.set("v.isOpen", false);
    },*/
    
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');

        if (openSections.length === 0) {
            cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    
    
    handleClick : function (component, event, helper) {
        var KBArticle = event.getSource().get("v.value");
        console.log('-->'+JSON.stringify(KBArticle.Id));
        var action = component.get("c.getArticlesTitle");
        action.setParams({
            'KID': KBArticle.Id
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('storeResponse - '+storeResponse);
                if(storeResponse == null){
                    storeResponse='';
                }
                console.log('storeResponse - '+storeResponse);
                var actionAPI = component.find("quickActionAPI");
                var fields = { HtmlBody : {value: storeResponse, insertType: "cursor"} };
                var args = { actionName :"Case.SendEMAIL_New",entityName:"Case",targetFields : fields};
                actionAPI.setActionFieldValues(args).then(function(result) {
                    component.set("v.isOpen", false);
                }).catch(function(e) {
                    if (e.errors) {
                        
                    }
                });
                component.set("v.isOpen", false);
                
                
                
                
            }
            
        });
        $A.enqueueAction(action);
        
        
        
    },
    
    parentAction : function (component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    createAccount: function (component) {
        var KBArticle = component.get('v.knowledgeArticle');
        var actionAPI = component.find("quickActionAPI");
        var fields = { HtmlBody : {value: KBArticle.Answer__c, insertType: "cursor"} };
        var args = { actionName :"Case.SendEMAIL_New",entityName:"Case",targetFields : fields};
        actionAPI.setActionFieldValues(args).then(function(result) {
            console.log('-->'+JSON.stringify(KBArticle));
            console.log('-->'+JSON.stringify(result));
            console.log('-->'+JSON.stringify(KBArticle.Procedure_Audience__c));
            component.set("v.isOpen", false);
        }).catch(function(e) {
            if (e.errors) {
                
            }
        });
        component.set("v.isOpen", false);
    }
})