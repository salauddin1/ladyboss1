({
    setWeekDayMethod : function(component , dayVal) {
        
        var action = component.get("c.getWeekDay");
        action.setParams({ dayValget :dayVal  });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                var fullDayVal = response.getReturnValue();
                component.set("v.dayName",fullDayVal);
            }
        });
        
        $A.enqueueAction(action);
    },
    setStEndMethod : function(component , dayVal ,evtEndVal) {
        
        var action = component.get("c.getStEndTimeVal");
        action.setParams({ dayValget :dayVal,
                          evtEndVal : evtEndVal
                         });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var fullTimeVal = response.getReturnValue();
                if(fullTimeVal != null && fullTimeVal.length >1){
                    component.set("v.eventStDate",fullTimeVal[0]);
                    component.set("v.eventEndDate",fullTimeVal[1]);
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})