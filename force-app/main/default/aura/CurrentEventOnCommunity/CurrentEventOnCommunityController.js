({
    doInit : function(component, event, helper) {
       // debugger
        var action = component.get("c.getCurrentEvent");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                var evt = response.getReturnValue();
             //   debugger
                var dayVal = evt.StartDateTime;
                var evtEndVal = evt.EndDateTime;
                console.log('   test '+dayVal);
                console.log(evt);
                component.set("v.eventRecordVal",evt);
                helper.setWeekDayMethod(component ,dayVal);
                helper.setStEndMethod(component ,dayVal,evtEndVal);
                
            }
        });
        var head = document.getElementsByTagName('head')[0];

        //Create link or script tag dynamically
        var link = document.createElement('script');

        //Add appropriate attributes
        link.src = "https://addevent.com/libs/atc/1.6.1/atc.min.js"; 
        link.type = "text/javascript";
        head.appendChild(link);
        $A.enqueueAction(action);
    },
    
})