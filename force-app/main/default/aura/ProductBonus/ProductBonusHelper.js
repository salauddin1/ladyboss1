({
    checkProduct : function(component,event,check,productId) {
        var total = 0;
        var elementRemove ;
        var checkProduct = component.get('v.checkProduct');
        var bonusTotal = component.get('v.bonusTotal');
        var action = component.get("c.getCheckProduct");
        action.setParams({'ProductId': productId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            var result = response.getReturnValue();
            if(check == true) {
                if(response.getReturnValue().Price__c != undefined){
                    total = response.getReturnValue().Price__c + bonusTotal;
                    component.set('v.bonusTotal',total);
                     checkProduct.push(result);
               
                }
                else{
                    component.set('v.bonusTotal',bonusTotal);  
                     checkProduct.push(result);
               
                }
                component.set('v.checkProduct',checkProduct);
            }
            else if(check == false) {
                //checkProduct.pop(result);
               // var newCheckProduct = checkProduct.filter(a => a.Id !== productId);
                if(response.getReturnValue().Price__c != undefined){
                    total = Math.abs(response.getReturnValue().Price__c - bonusTotal);
                    component.set('v.bonusTotal',total);
                    var newCheckProduct = checkProduct.filter(a => a.Id !== productId);
                
                }
                else{
                    component.set('v.bonusTotal',bonusTotal);
                    var newCheckProduct = checkProduct.filter(a => a.Id !== productId);
                
                }
                component.set('v.checkProduct',newCheckProduct);
            }
            
            
            
        });
        $A.enqueueAction(action);
    }
})