({
    doInit : function(component, event, helper) {
        var contactId = component.get('v.recordId');
        var FamilyVal = event.getParam("FamilyVal");
        if(FamilyVal != null){
            component.set('v.ShowSpinner',true);
            component.set('v.totalfield',true);
        }
        component.set('v.bonusTotal',0);
        var checkProduct = [];
        var multiEmail = [];
        var productName = [];
        component.set('v.checkProduct',checkProduct);
        component.set('v.multiEmail',multiEmail);
        component.set('v.FamilyVal',FamilyVal);
        component.set('v.productName',productName);
        console.log('------FamilyVal==========='+FamilyVal);
        var action = component.get("c.getProduct");
        action.setParams({"FamilyVal": FamilyVal});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            if(response.getReturnValue().length == 0){
                component.set('v.isprodData',true);
            }
            else{
                component.set('v.prodData',response.getReturnValue());
                component.set('v.isprodData',false); 
            }
            component.set('v.ShowSpinner',false);
            
        });
        $A.enqueueAction(action);
    },
    sendMailF:function(component, event, helper) {
        component.set('v.ShowSpinner',true);
        var check = event.getSource().get('v.checked');
        var productName = event.getSource().get('v.label');
        var productId = event.getSource().get("v.name");
        var multiEmail = component.get('v.multiEmail');
        var productNamevalue = component.get('v.productName');
        var action = component.get("c.sendMail");
        action.setParams({'ProductId': productId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            component.set('v.emailData',response.getReturnValue()[0]);
            var emailData = component.get('v.emailData');
            if(check == true) {
                multiEmail.push(emailData);
                productNamevalue.push(productName);
                component.set('v.multiEmail',multiEmail);
                component.set('v.productName',productNamevalue);
                helper.checkProduct(component,event,check,productId);
            }
            else if(check == false ){
                //var newMultiEmail = multiEmail.filter(a => a !== emailData);
                // var newProductNamevalue = productNamevalue.filter(a => a !== productName);
                var newMultiEmail = multiEmail.filter((value, index) => index !== multiEmail.indexOf(emailData));
                var newProductNamevalue = productNamevalue.filter((value, index) => index !== productNamevalue.indexOf(productName));
                component.set('v.multiEmail',newMultiEmail);
                component.set('v.productName',newProductNamevalue);
                helper.checkProduct(component,event,check,productId);
            }
            
            
            var applicationEvent = $A.get("e.c:ProductBonusEmailEvent");
            applicationEvent.setParams({"EmailBody" : component.get('v.multiEmail'),
                                        "productName":component.get('v.productName')})
            applicationEvent.fire();
             component.set('v.isDisable',false);
            component.set('v.ShowSpinner',false);
        });
        $A.enqueueAction(action);
        component.set('v.isDisable',true);
    },
    getCreatedToken:function(component, event, helper) {
        var EmailBody = event.getParam("EmailBody");
        var productName = event.getParam("productName");
        console.log('------token'+EmailBody);
        
    }
    
})