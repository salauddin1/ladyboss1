({
    itemSelected : function(component, event, helper) {
        //debugger
        var selectItemList = component.get("v.selectItemLists");
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");  
        if(SelIndex){
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            if(selItem.val){
                component.set("v.selItem",selItem);
                 selectItemList.push(selItem.text);
                component.set("v.selectItemLists",selectItemList);
                if(selectItemList.length > 0){
                component.set("v.addButton",false);
                }
                console.log("========"+selectItemList);
                component.set("v.last_ServerResult",serverResult);
            } 
            component.set("v.server_result",null); 
        } 
    }, 
    serverCall : function(component, event, helper) { 
        debugger
        var target = event.target;  
        var searchText = target.value; 
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelection1(component, event, helper);
        }else if(searchText.trim() != last_SearchText){ 
            //Save server call, if last text not changed
            //Search only when space character entered
            console.log("-----------------")
            var objectName = component.get("v.objectName");
            var field_API_text = component.get("v.field_API_text");
            var field_API_val = component.get("v.field_API_val");
            var field_API_SKU = component.get("v.field_API_SKU");
            var field_API_search = component.get("v.field_API_search");
            var limit = component.get("v.limit");
            var field_API_price =component.get("v.field_API_price");
            var action = component.get('c.searchDB');
            action.setStorable();
            
            action.setParams({
                objectName : objectName,
                fld_API_Text : field_API_text,
                fld_API_Val : field_API_val,
                fld_API_SKU : field_API_SKU,
                fld_API_price:field_API_price,
                lim : limit, 
                fld_API_Search : field_API_search,
                searchText : searchText
            });
            
            action.setCallback(this,function(a){
                var state = a.getState();
                console.log("Failed with state: " + state);
                
                if (state === "SUCCESS") {
                    this.handleResponse(a,component,helper);
                }
            });
            
            //component.set("v.last_SearchText",searchText.trim());
            console.log('Server call made');
            $A.enqueueAction(action); 
        }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){ 
            component.set("v.server_result",component.get("v.last_ServerResult"));
            console.log('Server call saved');
        }         
    },
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            debugger
             var selectedItem = component.get("v.selectItemLists");
              var newSel = retObj.filter(a => !selectedItem.includes(a.text));
                
           // debugger
            if(newSel.length <= 0){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
                component.set("v.last_ServerResult",noResult);
            }else{
               /* var selectedItem = component.get("v.selectItemLists");
                var values = selectedItem.map(a => a.text)
                console.log("========="+values);
                var newObj = retObj.filter(a => !values.includes(a.text))
                console.log("========="+newObj);*/
                //itrate to check already ex
                 /* var resultVal = []; //indexup
                for(var i = 0; i< retObj.length;i++){
                    if(retObj[i].val != selectedItem[0]){
                      resultVal.push(retObj[i]);  
                    }
                }
                component.set("v.server_result",resultVal);*/
                debugger
                 component.set("v.server_result",newSel);
                component.set("v.last_ServerResult",newSel);
            }  
        }else if (res.getState() === 'ERROR'){
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    alert(errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
         var selectItemList = component.get("v.selectItemLists");
       	 var canVal = event.target.previousElementSibling.getElementsByTagName("span")[0].innerHTML;
         //debugger
         var newSel = selectItemList.filter(a => a !== canVal);
              component.set("v.selectItemLists",newSel);  
        component.set("v.selItem",null);
        component.set("v.server_result",null);
         var selectItemList1 = component.get("v.selectItemLists");
         if(selectItemList1.length > 0){
                component.set("v.addButton",false);
                }
        else{component.set("v.addButton",true);}
      
    } ,
    clearSelection1: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.server_result",null);
  
    }
    
})