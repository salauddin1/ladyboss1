({
    
    getInitValues : function(component, event, recordId) {
        var action = component.get("c.getOpportunityDetails");
        var address = {};
        component.set("v.addAddress",address);
        component.set("v.productOptions",address);
        action.setParams({"conId": recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                console.log('result address',result.address);
                component.set("v.contact",result);
                console.log('result asyncError',result.asyncError);
                if (result.asyncError) {
                    component.set("v.isAsyncError",true);
                    component.set("v.asyncErrorMsg","Automations in Salesforce are nearing their limit. You can still charge successfully but the email receipt will be delayed for a short time. Please let the System Administrator know 'Async Calls are an issue.");
                }
                
                var subscribedProductList = result.subscribedProductList;
                var productList = result.productWrapperList;
                console.log('productlist', result.productWrapperList);
                for(var j=0;j<productList.length;j++){
                        console.log('Cancel ',productList[j]);
                    if(productList[j].autoCancelDays){
                        console.log('Cancel days',productList[j].autoCancelDays);
                        //component.set("v.cancelDays", productList[j].autoCancelDays);
                        //var cardNumber = cardsList[i].Credit_Card_Number__c;
                        //cardsList[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                    }
                    
                }
                if(subscribedProductList)
                    component.set("v.subscribedProductList",subscribedProductList);
                var cards = result.cardList; 
                cards.unshift({"Credit_Card_Number__c" : "--- Select Card ---"});
                console.log(cards);
                component.set("v.card",cards);
                var cardsList = component.get("v.card");
                console.log('cards List array',cardsList);
                for(var i=1;i<cardsList.length;i++){
                    if(cardsList[i].Credit_Card_Number__c){
                        console.log('cardsList Number',cardsList[i].Credit_Card_Number__c);
                        var cardNumber = cardsList[i].Credit_Card_Number__c;
                        cardsList[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                    }
                }
                
                component.set("v.card",cardsList);
                
                if(result.address){
                    var displayAddress = result.address.Shipping_Street__c+','+result.address.Shipping_City__c+','+result.address.Shipping_State_Province__c+',';
                    displayAddress+=result.address.Shipping_Country__c+','+result.address.Shipping_Zip_Postal_Code__c+'.';
                    
                    component.set("v.displayAddress",displayAddress);
                    component.set("v.showSameAsShippingCheckbox",true);
                    component.set("v.contactHasPrimaryAddress",true);
                }
                component.set("v.expMonthsList",result.allPickListWrapperList[0].pickListField);
                component.set("v.expYearsList",result.allPickListWrapperList[1].pickListField);
                component.set("v.cardTypeList",result.allPickListWrapperList[2].pickListField);
                console.log('card type list returned',component.get("v.cardTypeList"));
                component.set("v.selectedCard", "--- Select Card ---");
                component.set("v.contactId", result.ContactId);
                component.set("v.productOptions", result.productWrapperList);
                console.log('Contact ID -->',component.get("v.contactId"));
                console.log('Products--->',component.get("v.productOptions"));    
                component.set("v.discount",result.discount);
                console.log('discount',component.get("v.discount"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
                console.log('in do init',component.get("v.addAddress"));
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
        
        var actiontogetUser = component.get("c.getUserDetails");
        actiontogetUser.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue();
                console.log("result---",result);
                component.set("v.salesPerson",result);
            }
        });
        $A.enqueueAction(actiontogetUser);
        
        var actiontogetUserList = component.get("c.getUserList");
        actiontogetUserList.setParams({"userFieldValue": "available_for_coaching_products__c"});
        actiontogetUserList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
                var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userList",result);
            }
        });
        $A.enqueueAction(actiontogetUserList);
        
        var actiontogetUserDigitalList = component.get("c.getUserList");
        actiontogetUserDigitalList.setParams({"userFieldValue": "Available_For_Credit__c"});
        actiontogetUserDigitalList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
             /*   var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);  */
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userCreditList",result);
            }
        });
        $A.enqueueAction(actiontogetUserDigitalList);
        
        var actiontogetUserkickstartList = component.get("c.getUserList");
        actiontogetUserkickstartList.setParams({"userFieldValue": "Available_for_Coaching_Product_Sales__c"});
        actiontogetUserkickstartList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
                var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userkickstartList",result);
            }
        });
        $A.enqueueAction(actiontogetUserkickstartList);
        
        
        this.refreshACHList(component,event,recordId,'c.ACHVerifiedListServer');
        this.refreshACHList(component,event,recordId,'c.ACHNonVerifiedListServer');
    },
    
    setTotalPrice :  function(component, event) {
        console.log('totalpricehelper');
        var discount = component.get("v.discount");
        var prodItems = component.get("v.productOptions");
        var totalPrice = 0;
        var totalClubbedValue = 0;
        var totalNonClubbedValue = 0;
        var monthTotalMap ={};
        var totalvalue = [];
        
        var selectedCardList = component.get("v.selectedCardList"); 
        
        for(var i = 0; i < prodItems.length; i++){
            if(prodItems[i].checkValue == true){
                
                if(prodItems[i].quantity == 0){
                    prodItems[i].quantity = 1;
                }
                
                totalPrice += prodItems[i].price * prodItems[i].quantity;
                
                if(prodItems[i].checkedForClub == true || prodItems[i].standAloneClubProd == true){
                    totalClubbedValue += prodItems[i].quantity * prodItems[i].price;    
                }else{
                    totalNonClubbedValue += prodItems[i].quantity * prodItems[i].price;
                }
            }
        } 
        //console.log(selectedCardList[0]['amount']);
        /*if (selectedCardList[0] != 'undefined' && selectedCardList[0] && selectedCardList[1] != 'undefined' && selectedCardList[1]){
            
            var tot = 0;
            var int1 = selectedCardList[0]['amount'];
            var int2 = selectedCardList[1]['amount'];
            console.log('int1'+int1);
            console.log('int2'+int2);
            console.log(int1+int2);
            tot = parseFloat(selectedCardList[0]['amount']) + parseFloat(selectedCardList[1]['amount']);
            console.log(tot);
            if(tot > totalPrice){
                component.set("v.errorMsg","you can't add more amount than total value combined");
                component.set("v.isError",true);      
            }else{
                component.set("v.isError",false);      
            }
            
        }
        */
        
        console.log('totalClubbedValue',totalClubbedValue);
        console.log('totalNonClubbedValue',totalNonClubbedValue);
        console.log('totalPriceWithMonths',totalPrice);
        component.set('v.selectedCardList',selectedCardList);
        component.set("v.totalPriceValue",totalPrice.toFixed(2));
        component.set("v.totalClubbedValue",totalClubbedValue.toFixed(2));
        component.set("v.totalNonClubbedValue",totalNonClubbedValue.toFixed(2));
    },
    getContactOptions: function(component,event) {
        var action = component.get('c.getContacts');
        var search = component.get("v.inputContact");
        action.setParams({
            searchText: search
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ) {
                var results = [];
                var predictions = response.getReturnValue();
                if(predictions.length > 0) {
                    for (var i = 0; i< predictions.length; i++) {
                        results.push(predictions[i]);
                    }
                    console.log('contact results-->',results);
                    component.set("v.isRenderContactResults",true);
                    component.set("v.contactOptions",results); 
                    
                }else{
                    console.log('Contact Query Error');
                    component.set("v.isRenderContactResults",false);
                }
            }else if(state == "ERROR" ){
                var errors = data.getError();
                console.log('contact errors-->',errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleClub: function(component,event,source) {
        debugger
        console.log('entered handleclub');
        var changeValue = event.getSource();
        var index = component.get("v.indexVar");
        console.log('club index ',index);
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index]; 
        var subscribedProductList = component.get("v.subscribedProductList");
        var isSwitchedProductSelectable = true;
        if(subscribedProductList){
            for(var i=0;i<subscribedProductList.length;i++){
                if(subscribedProductList[i]==selectedOption.switchedProductId){
                    console.log('inside found switched list');
                    isSwitchedProductSelectable = false;
                    break;
                }
            }
        }
        console.log('club changed ',selectedOption);
        var discount = component.get("v.discount");
        var price = selectedOption.price * selectedOption.quantity;
        console.log('price value',price);
        var productName = selectedOption.Name;
        var productPrice = selectedOption.price;
        var productId = selectedOption.Id;
        var productClub = selectedOption.club;
        var productQuantitySelector = selectedOption.quanititySelector;
        var autoCancelDays = selectedOption.autoCancelDays;
        
        //we can add our logic to switch product according to month here
        console.log('selectedOption.checkedForClub' + selectedOption.checkedForClub);
        console.log('selectedOption.productsToSwitchMap'+selectedOption.productsToSwitchMap);
        console.log('selectedOption.months'+selectedOption.months);
        if(selectedOption.productsToSwitchMap && selectedOption.productsToSwitchMap.hasOwnProperty(selectedOption.months)){
            if(selectedOption.productsToSwitchMap[selectedOption.months].isSelectable==false){
                ///show error
                component.set("v.errorMsg",selectedOption.productsToSwitchMap[selectedOption.months].Name + ' already subscribed');
                component.set("v.isErrorForProduct",true);
                component.set("v.productErrorIndex",index);
                selectedOption.checkedForClub = false;
            }else{
                selectedOption.Name = selectedOption.productsToSwitchMap[selectedOption.months].Name;
                selectedOption.price = selectedOption.productsToSwitchMap[selectedOption.months].price;
                selectedOption.Id = selectedOption.productsToSwitchMap[selectedOption.months].Id;
                selectedOption.isSelectable = selectedOption.productsToSwitchMap[selectedOption.months].isSelectable;
                selectedOption.quanititySelector = selectedOption.productsToSwitchMap[selectedOption.months].quanititySelector;
                selectedOption.autoCancelDays = selectedOption.productsToSwitchMap[selectedOption.months].autoCancelDays;
                component.set("v.isErrorForProduct",false);
            }
        }
        //selectedOption.isSelectable = isSwitchedProductSelectable;
        console.log('inside handle club hanlder method, selectedOption' + selectedOption);
        if(selectedOption.isSelectable==false){
            console.log('make check false');
            selectedOption.checkValue = false;
            var checkedProductList = component.get("v.checkedProductList");
            checkedProductList.pop();
            component.set("v.checkedProductList",checkedProductList);
        }
        productList[index] = selectedOption;
        component.set("v.productOptions",productList);
        console.log('in handle club',selectedOption);
    },
    
    getSelectedContact: function(component, event, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var action = component.get('c.getContactDetails');
        action.setParams({
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                component.set("v.contact",result);
                component.set("v.contactId", result.ContactId);
                console.log(component.get("v.contact"));
                console.log(component.get("v.contactId"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateContactInfo: function(component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var contactDetails = JSON.stringify(component.get("v.contact"));
        console.log('in helper updatre info contact Details---> ',contactDetails);
        var action = component.get('c.updateContactDetails');
        action.setParams({
            contactDetails : contactDetails
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var updatec = component.get("v.updateCon");
                component.set("v.updateCon",false);
                var chkBox = component.find("updateConCheck").set("v.checked",false);
                console.log('chkBox',chkBox);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                this.sendToVF(component,event,helper);
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    
    createAddress: function(component, event, address, contactId,isPrimary) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var addressDetails = JSON.stringify(address);
        console.log('in helper updatre info ---> ',addressDetails);
        var action = component.get('c.createNewAddress');
        action.setParams({
            addressDetails : addressDetails,
            contactId: contactId,
            isPrimary : isPrimary
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                var chkBox = component.find("newAddressCheck").set("v.checked",false);
                console.log('chkBox',chkBox);
                component.set("v.newAddress",false);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //if(component.get("v.isPrimaryAddress")){
                if(true){
                    component.set("v.contactHasPrimaryAddress",true);
                    console.log('inside primary address change');
                    this.refreshPrimaryAddress(component,event,contactId);
                }
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    newCardWithBilling: function(component, event, cardDetails, contactId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Details info ---> ',cardDetailString);
        var action = component.get('c.createCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                console.log('Card Created');
                component.set("v.newCard",false);
                component.find("newCardChkBox").set("v.checked",false);
                var intervalInMilliseconds = 5000;
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.visible", true);
                    }), intervalInMilliseconds
                );
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    
    valid_credit_card: function(value) {
        // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value)) return false;
        
        // The Luhn Algorithm. It's so pretty.
        var nCheck = 0,
            nDigit = 0,
            bEven = false;
        value = value.replace(/\D/g, "");
        
        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);
            
            if (bEven) {
                if ((nDigit *= 2) > 9)
                    nDigit -= 9;
            }
            
            nCheck += nDigit;
            bEven = !bEven;
        }
        
        return (nCheck % 10) == 0;
    },
    
    updateCardWithBilling: function(component, event, cardDetails, cardId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        component.set("v.isUpdateCardWithError",true);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Update Details info ---> ',cardDetailString);
        var action = component.get('c.updateCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            cardId: cardId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                component.set("v.isUpdateCardWithError",true);
                console.log('Card updated');
                component.set("v.newCard",false);
                //component.find("newCardChkBox").set("v.checked",false);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    newCardWithShipping: function(component, event, cardDetails, address, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        var addressDetailString = JSON.stringify(address);
        console.log('in helper Card Shipping Details info ---> ',cardDetailString);
        console.log('shipping Address ',addressDetailString);
        var action = component.get('c.createCardWithShipping');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId,
            addressDetails : addressDetailString
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                console.log('Card Created');
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //  $A.get('e.force:refreshView').fire();                
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);   
    },
    rollbackOpp: function( cmp, apexAction, params ) {
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action                          = cmp.get("c."+apexAction+"");
            action.setParams( params );
            action.setCallback( this , function(callbackResult) {
                if(callbackResult.getState()=='SUCCESS') {
                    resolve( callbackResult.getReturnValue() );
                }
                if(callbackResult.getState()=='ERROR') {
                    console.log('ERROR', callbackResult.getError() ); 
                    reject( callbackResult.getError() );
                }
            });
            $A.enqueueAction( action );
        }));            
        return p;
    },
    
    createOrder: function(component, event,selectedProd,payment,sd,cardlist,contact,totalPriceValue,salesPerson,isACHvalue,userr,user,userKick ){
        //console.log('createOpportunity helper',sd);
        //console.log('stringify ',JSON.stringify(selectedProd));
         
        component.set("v.Spinner", true);
        console.log('selectedProd-->'+selectedProd);
        console.log('selectedProd-->'+selectedProd[0].productWithSite);
        console.log('payment-->'+payment);
        console.log('cardlist-->'+cardlist);
        if(cardlist && cardlist.length==1){
            cardlist[0].usedForClub=true;
            cardlist[0].nonClubamount=component.get('v.totalNonClubbedValue');
        console.log('cardlist-->'+cardlist);
        }
        
        
        
        var triggerCharge = false;
        var sd2;
        if(sd){
            sd2 = sd;
        }
        sd=sd2;
        var action = component.get('c.insertOpportunity');
        action.setParams({
                salesPerson : salesPerson,
                selectedProd : JSON.stringify(selectedProd),
                contactDetails: JSON.stringify(contact),
                paymentType : payment,
                scheduledDate: sd,
                CardJson : JSON.stringify(cardlist),
                totalPriceValue:totalPriceValue,
                clubbedPriceValue: component.get("v.totalClubbedValue"),
                nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                calloutNow : false,
                isACHvalue : JSON.stringify(isACHvalue),
                AgentInitializingSale : userr,
                CreditUser : user,
                KickstartUser :userKick,
                isErrorOrderForm:component.get("v.isErrorOrderForm"),
                isAsyncError:component.get("v.isAsyncError")
            }); 
        action.setCallback(this, function(response){
            debugger
            var action1 = component.get('c.insertOpportunity');
            var state = response.getState();
            console.log('---result--'+state);
            if(state.toLowerCase() == "success"){
                triggerCharge = true;
            
            action1.setParams({
                salesPerson : salesPerson,
                selectedProd : JSON.stringify(selectedProd),
                contactDetails: JSON.stringify(contact),
                paymentType : payment,
                scheduledDate: sd,
                CardJson : JSON.stringify(cardlist),
                totalPriceValue:totalPriceValue,
                clubbedPriceValue: component.get("v.totalClubbedValue"),
                nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                calloutNow : true,
                isACHvalue : JSON.stringify(isACHvalue),
                AgentInitializingSale : userr,
                CreditUser : user,
                KickstartUser :userKick,
                isErrorOrderForm:component.get("v.isErrorOrderForm"),
                isAsyncError:component.get("v.isAsyncError")
            }); 
            action1.setCallback(this, function(response1){
                var state = response1.getState();
                var orderResp = JSON.stringify(response1.getReturnValue());
                console.log('----------orderResp---------'+orderResp);
                console.log('----------triggerCharge-----3----'+triggerCharge);
                
                if(!orderResp || orderResp==null || orderResp=='null'){
                    component.set("v.Spinner", false);
                    component.set("v.errorMsg",'This card failed, please check with your administrator');
                    component.set("v.isError",true);
                    component.set("v.disableOrderButton",true);
                  
                }else if(orderResp && orderResp!= JSON.stringify("Success")){
                        try{
                            if(JSON.parse( response1.getReturnValue() )){
                            var jsonDes = JSON.parse( response1.getReturnValue() );
                            component.set("v.errorMsg",jsonDes.Error);
                            console.log("inside");
                            if(jsonDes.Card != null && jsonDes.Card != '' && jsonDes.CardNumber != null && jsonDes.CardNumber != ''){
                                component.set("v.isOpen", true);
                                component.set("v.chIdValNumber", jsonDes.CardNumber);
                                //component.set("v.chIdVal", jsonDes.Card);
                                component.set("v.chIdVal", jsonDes.Card1);
                                component.set("v.chIdValAmount", jsonDes.AmountSucceeded);
                                component.set("v.chIdValAmountFailed", jsonDes.AmountFailed);
                                component.set("v.chIdOppId", jsonDes.Opportunity);
                                component.set("v.chIdValType", jsonDes.Type);
                                console.log(component.get("v.chIdValType")+component.get("v.chIdVal")+component.get("v.chIdValAmount")+component.get("v.chIdValAmountFailed")+component.get("v.chIdOppId"))
                            }
                        }
                        }catch(e){
                    component.set("v.errorMsg",orderResp+e.name + e.message);
                        }
                        component.set("v.Spinner", false);
                    component.set("v.isError",true);
                    component.set("v.isErrorOrderForm",true);
                    
                }else{
                    if(state == "SUCCESS" ){
                        component.set("v.Spinner", false);
                        //component.find("overlayLib").notifyClose();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "A new Order has been created.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                          this.sendMail(component,event,helper);
                        var allComponentCreated = false;
                        var totalPopupToBeOpen = 0;
                        var totalPopupOpened = 0;
                        var siteToBePoped = [];
                        for (var i in selectedProd){
                            console.log('---before---'+selectedProd[i].productWithSite);
                            if(selectedProd[i].productWithSite){
                                if(siteToBePoped.includes(selectedProd[i].productWithSite) === false){
                                    siteToBePoped.push(selectedProd[i].productWithSite);
                                    totalPopupToBeOpen = totalPopupToBeOpen + 1;
                                    console.log('selectedProd[i].productWithSite'+selectedProd[i].productWithSite);
                                }
                            }
                        }
                        console.log(siteToBePoped);
                        for (var i in siteToBePoped){
                            if(siteToBePoped[i]){
                                var modalBody;
                                console.log('lib called'+i);
                                $A.createComponent("c:PopUpPageAfterOrder", { recordId : siteToBePoped[i]},
                                                   function(content, status) {
                                                       if (status === "SUCCESS") {
                                                           modalBody = content;
                                                           component.find('overlayLibPopup').showCustomModal({
                                                               body: modalBody, 
                                                               showCloseButton: true,
                                                               cssClass: "slds-modal_large fullwidth",
                                                               closeCallback: function() {
                                                               }
                                                           })
                                                       }
                                                       totalPopupOpened = totalPopupOpened + 1;
                                                       if(totalPopupToBeOpen==totalPopupOpened){
                                                           component.find("overlayLib").notifyClose();                                   
                                                       }
                                                   }
                                                  );
                            }
                        }
                        if(totalPopupToBeOpen==0){
                            component.find("overlayLib").notifyClose();
                        }
                    } else if (state == "ERROR"){
                        component.set("v.Spinner", false);
                        var errors = response1.getError();
                        console.log(errors); 
                        if(JSON.stringify(errors) == null){
                            component.set("v.errorMsg",'Internal Server Error');
                        }else{
                            component.set("v.errorMsg",JSON.stringify(errors));
                        }
                        
                        component.set("v.isError",true);
                    }       
                }
               
            });
            $A.enqueueAction(action1);   }
        });
         $A.enqueueAction(action);
        /*this.rollbackOpp(component,'insertOpportunity',{ salesPerson : salesPerson,
                                                        selectedProd : JSON.stringify(selectedProd),
                                                        contactDetails: JSON.stringify(contact),
                                                        paymentType : payment,
                                                        scheduledDate: sd,
                                                        CardJson : JSON.stringify(cardlist),
                                                        totalPriceValue:totalPriceValue,
                                                        clubbedPriceValue: component.get("v.totalClubbedValue"),
                                                        nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                                                        calloutNow : triggerCharge, 
                                                        isACHvalue : JSON.stringify(isACHvalue),
                                                        AgentInitializingSale : userr,
                                                        CreditUser : user,
                                                        KickstartUser :userKick})
        .then(function(result){
            var action = component.get('c.insertOpportunity');
            console.log('---result--'+result);
            if(result == "Success"){
                triggerCharge = true;
            }
            action.setParams({
                salesPerson : salesPerson,
                selectedProd : JSON.stringify(selectedProd),
                contactDetails: JSON.stringify(contact),
                paymentType : payment,
                scheduledDate: sd,
                CardJson : JSON.stringify(cardlist),
                totalPriceValue:totalPriceValue,
                clubbedPriceValue: component.get("v.totalClubbedValue"),
                nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                calloutNow : triggerCharge,
                isACHvalue : JSON.stringify(isACHvalue),
                AgentInitializingSale : userr,
                CreditUser : user,
                KickstartUser :userKick
            }); 
            action.setCallback(this, function(response){
                var state = response.getState();
                var orderResp = JSON.stringify(response.getReturnValue());
                console.log('----------orderResp---------'+orderResp);
                console.log('----------triggerCharge-----3----'+triggerCharge);
                
                if(!orderResp || orderResp==null || orderResp=='null'){
                    component.set("v.Spinner", false);
                    component.set("v.errorMsg",'This card failed, please check with your administrator');
                    component.set("v.isError",true);
                    component.set("v.disableOrderButton",true);
                }else if(orderResp && orderResp!= JSON.stringify("Success")){
                    try{
                        if(JSON.parse( response.getReturnValue() )){
                        var jsonDes = JSON.parse( response.getReturnValue() );
                        component.set("v.errorMsg",jsonDes.Error);
                        if(jsonDes.Card != null && jsonDes.Card != '' && jsonDes.CardNumber != null && jsonDes.CardNumber != ''){
                            component.set("v.isOpen", true);
                            component.set("v.chIdValNumber", jsonDes.CardNumber);
                            //component.set("v.chIdVal", jsonDes.Card);
                            component.set("v.chIdVal", jsonDes.Card1);
                            component.set("v.chIdValAmount", jsonDes.AmountSucceeded);
                            component.set("v.chIdValAmountFailed", jsonDes.AmountFailed);
                            component.set("v.chIdOppId", jsonDes.Opportunity);
                        }
                    }
                    }catch(e){
                    component.set("v.errorMsg",orderResp);
                    }
                    component.set("v.Spinner", false);
                    component.set("v.isError",true);
                    
                }else{
                    if(state == "SUCCESS" ){
                        component.set("v.Spinner", false);
                        //component.find("overlayLib").notifyClose();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "A new Order has been created.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                        var allComponentCreated = false;
                        var totalPopupToBeOpen = 0;
                        var totalPopupOpened = 0;
                        var siteToBePoped = [];
                        for (var i in selectedProd){
                            console.log('---before---'+selectedProd[i].productWithSite);
                            if(selectedProd[i].productWithSite){
                                if(siteToBePoped.includes(selectedProd[i].productWithSite) === false){
                                    siteToBePoped.push(selectedProd[i].productWithSite);
                                    totalPopupToBeOpen = totalPopupToBeOpen + 1;
                                    console.log('selectedProd[i].productWithSite'+selectedProd[i].productWithSite);
                                }
                            }
                        }
                        console.log(siteToBePoped);
                        for (var i in siteToBePoped){
                            if(siteToBePoped[i]){
                                var modalBody;
                                console.log('lib called'+i);
                                $A.createComponent("c:PopUpPageAfterOrder", { recordId : siteToBePoped[i]},
                                                   function(content, status) {
                                                       if (status === "SUCCESS") {
                                                           modalBody = content;
                                                           component.find('overlayLibPopup').showCustomModal({
                                                               body: modalBody, 
                                                               showCloseButton: true,
                                                               cssClass: "slds-modal_large fullwidth",
                                                               closeCallback: function() {
                                                               }
                                                           })
                                                       }
                                                       totalPopupOpened = totalPopupOpened + 1;
                                                       if(totalPopupToBeOpen==totalPopupOpened){
                                                           component.find("overlayLib").notifyClose();                                   
                                                       }
                                                   }
                                                  );
                            }
                        }
                        if(totalPopupToBeOpen==0){
                            component.find("overlayLib").notifyClose();
                        }
                    } else if (state == "ERROR"){
                        component.set("v.Spinner", false);
                        var errors = response.getError();
                        console.log(errors); 
                        if(JSON.stringify(errors) == null){
                            component.set("v.errorMsg",'Internal Server Error');
                        }else{
                            component.set("v.errorMsg",JSON.stringify(errors));
                        }
                        
                        component.set("v.isError",true);
                    }       
                }
                
            });
            $A.enqueueAction(action);   
        });*/
        
    },
    
    showToast : function(component, event) {
        console.log('toast called');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Card has been created successfully."
        });
        toastEvent.fire();
    },
    refreshCardList: function(component, event, contactId){
        console.log('card list called');
        console.log('contact Id is ',contactId);
        /*var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");*/
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var cardAction = component.get('c.cardListServer');
        cardAction.setParams({
            conId: contactId
            
        });
        cardAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside card list');
                console.log('inside resultCard'+response.getReturnValue());
                
                var resultCard = response.getReturnValue();
                if(resultCard.length>0){
                    console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c'+resultCard[0].Stripe_Card_Id__c);
                    console.log('resultCard.length>0 resultCard[0].Error_Message__c'+resultCard[0].Error_Message__c);
                    if(!resultCard[0].Error_Message__c && resultCard[0].Stripe_Card_Id__c){
                        component.set("v.newCard",false);
                        console.log('first card details',resultCard[0]);
                        console.log('result',resultCard);
                        console.log('card error Message',resultCard[0].Error_Message__c);
                        //var cardsList = resultCard;
                        var cardsList = [];
                        console.log('cards List array',cardsList);
                        for(var i=0;i<resultCard.length;i++){
                            if(resultCard[i].Credit_Card_Number__c && resultCard[i].Stripe_Card_Id__c){
                                console.log('cardsList Number',resultCard[i].Credit_Card_Number__c);
                                var cardNumber = resultCard[i].Credit_Card_Number__c;
                                resultCard[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                                console.log('credit card number',resultCard[i].Credit_Card_Number__c);
                                cardsList.push(resultCard[i]);
                            }
                        }
                        
                        component.set("v.card",cardsList);
                        console.log('default card from cardsList',cardsList[0].Credit_Card_Number__c);
                        component.set("v.defaultCard",cardsList[0]);
                        component.set("v.selectedCard",cardsList[0].Id);
                        var cardNumber = cardsList[0].Credit_Card_Number__c;
                        var displayCardNumber = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                        component.set("v.displayCardNumber",displayCardNumber);
                        
                        var selectedCardList = component.get("v.selectedCardList");        
                        
                        if(selectedCardList && selectedCardList.length==0){
                            selectedCardList.push({'card':cardsList[0].Id,'nonClubamount':component.get('v.totalNonClubbedValue'), 'usedForClub':true});
                            component.set("v.selectedCardList",selectedCardList);
                        }
                        
                    }
                    if(resultCard[0].Error_Message__c && !resultCard[0].Stripe_Card_Id__c){
                        console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c final'+resultCard[0].Stripe_Card_Id__c);
                        console.log('resultCard.length>0 resultCard[0].Error_Message__c final'+resultCard[0].Error_Message__c);
                        this.setCardInEditMode(component,event,resultCard);
                    }
                    
                }
                /*var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                */
            }
        });
        $A.enqueueAction(cardAction);
    },
    
     refreshACHList: function(component, event, contactId,methodName){
        console.log('methodName ',methodName);
        console.log('contact Id is ',contactId);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        component.set("v.ACHErrorMsg",'');
        component.set("v.isACHError",false);
        var ACHAction = component.get(methodName);
        ACHAction.setParams({
            conId: contactId
            
        });
        ACHAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside resultCard'+response.getReturnValue());
                
                var resultCard = response.getReturnValue();
                if(resultCard.length>0){
                    console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c'+resultCard[0].Stripe_ACH_Id__c);
                    console.log('resultCard.length>0 resultCard[0].Error_Message__c'+resultCard[0].Error_Message__c);
                    if(!resultCard[0].Error_Message__c && resultCard[0].Stripe_ACH_Id__c){
                        component.set("v.newACH",false);
                        console.log('first ACH details',resultCard[0]);
                        console.log('result',resultCard);
                        console.log('card error Message',resultCard[0].Error_Message__c);
                        //var cardsList = resultCard;
                        var cardsList = [];
                        console.log('cards List array',cardsList);
                        for(var i=0;i<resultCard.length;i++){
                            if(resultCard[i].Last4__c){
                                var Last4__c = resultCard[i].Last4__c;
                                resultCard[i].Last4__c = '********'+ Last4__c;
                                console.log('credit card number',resultCard[i].Last4__c);
                                cardsList.push(resultCard[i]);
                            }
                        }
                        cardsList.unshift({"Last4__c" : "--- Select Card ---"});
                        
                        if(methodName == 'c.ACHVerifiedListServer'){
                            component.set("v.isVerifiedACHListEmpty",false);
                            console.log('ACHVerifiedListServer cardsList',cardsList[0].isVerified__c);
                            console.log('ACHVerifiedListServer cardsList',cardsList[0]);
                            component.set("v.ACH",cardsList);
                            component.set("v.defaultACH",cardsList[0]);
                            component.set("v.selectedVerifiedACH",cardsList[0].Id);
                        }else if(methodName == 'c.ACHNonVerifiedListServer'){
                            component.set("v.isNonVerifiedACHListEmpty",false);
                            console.log('ACHNonVerifiedListServer cardsList',cardsList[0].isVerified__c);
                            component.set("v.nonVerifiedACH",cardsList);
                            component.set("v.defaultNonVerifiedACH",cardsList[0]);
                            component.set("v.selectedNACH",cardsList[0].Id);
                            component.set("v.disableVerifyACHButton",false);
                        }
                        component.set("v.selectedACH", "--- Select Card ---");
                        component.set("v.selectedVerifiedACH", "--- Select Card ---");
                        
                        /*var selectedCardList = component.get("v.selectedCardList");        
                        
                        if(selectedCardList && selectedCardList.length==0){
                            selectedCardList.push({'card':cardsList[0].Id,'nonClubamount':component.get('v.totalNonClubbedValue'), 'usedForClub':true});
                            component.set("v.selectedCardList",selectedCardList);
                        }*/
                        
                    }
                    /*if(resultCard[0].Error_Message__c && !resultCard[0].Stripe_ACH_Id__c){
                        console.log('resultCard.length>0 resultCard[0].Stripe_ACH_Id__c final'+resultCard[0].Stripe_ACH_Id__c);
                        console.log('resultCard.length>0 resultCard[0].Error_Message__c final'+resultCard[0].Error_Message__c);
                        this.setCardInEditMode(component,event,resultCard);
                    }*/
                    
                }
                else{
                    cardsList = [];
                    cardsList.unshift({"Last4__c" : "--- Select Card ---"});
                    var emptylist ={};
                    if(methodName == 'c.ACHVerifiedListServer'){
                        component.set("v.isVerifiedACHListEmpty",true);
                            component.set("v.ACH",cardsList);
                            component.set("v.defaultACH",emptylist);
                            component.set("v.selectedACH",emptylist);
                        }else if(methodName == 'c.ACHNonVerifiedListServer'){
                            component.set("v.isNonVerifiedACHListEmpty",true);
                            component.set("v.nonVerifiedACH",cardsList);
                            component.set("v.defaultNonVerifiedACH",emptylist);
                            component.set("v.selectedNonVerifiedACH",emptylist);
                            component.set("v.disableVerifyACHButton",true);
                        }
                    component.set("v.selectedACH", "--- Select Card ---");
                    component.set("v.selectedVerifiedACH", "--- Select Card ---");
                }
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
            }
        });
        $A.enqueueAction(ACHAction);
    },
    
    refreshPrimaryAddress : function(component,event,contactId){
        console.log('card list called');
        var primaryAddressAction = component.get('c.refreshContactAddressServer');
        primaryAddressAction.setParams({
            conId: contactId
        });
        primaryAddressAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside update primary address response');
                component.set("v.contact.address",response.getReturnValue());
                
                component.set("v.showSameAsShippingCheckbox",true);
                
                var displayAddress = response.getReturnValue().Shipping_Street__c+','+response.getReturnValue().Shipping_City__c+','+response.getReturnValue().Shipping_State_Province__c+',';
                displayAddress+=response.getReturnValue().Shipping_Country__c+','+response.getReturnValue().Shipping_Zip_Postal_Code__c+'.';
                
                component.set("v.displayAddress",displayAddress);
                //component.set("v.showSameAsShippingCheckbox",true);
                component.set("v.contactHasPrimaryAddress",true);    
            }
        });
        $A.enqueueAction(primaryAddressAction);
    },
    
    setCardInEditMode : function(component,event,resultCard){
        console.log('inside invalid card with error');
        //component.find("newCardChkBox").set("v.checked",true);
        component.set("v.newCard",true);
        console.log('in else new card name',resultCard[0].Name_On_Card__c);
        var newCardDetails = {};
        var cardId = resultCard[0].Id;
        component.set("v.newCardDetails",newCardDetails);
        component.set("v.newCardDetails.Name_On_Card__c",resultCard[0].Name_On_Card__c);
        component.find("cardType").set("v.value",resultCard[0].Card_Type__c);
        component.find("expMonths").set("v.value",resultCard[0].Expiry_Month__c);
        component.find("expYears").set("v.value",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Credit_Card_Number__c",resultCard[0].Credit_Card_Number__c);
        component.set("v.newCardDetails.Cvc__c",resultCard[0].Cvc__c);
        component.set("v.newCardDetails.Expiry_Month__c",resultCard[0].Expiry_Month__c);
        component.set("v.newCardDetails.Expiry_Year__c",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Card_Type__c",resultCard[0].Card_Type__c);
        component.set("v.newCardDetails.address.Billing_Street__c",resultCard[0].Billing_Street__c);
        component.set("v.newCardDetails.address.Billing_City__c",resultCard[0].Billing_City__c);
        component.set("v.newCardDetails.address.Billing_State_Province__c",resultCard[0].Billing_State_Province__c);
        component.set("v.newCardDetails.address.Billing_Country__c",resultCard[0].Billing_Country__c);
        component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",resultCard[0].Billing_Zip_Postal_Code__c);
        component.set("v.isUpdateCardWithError",true);
        component.set("V.errorCardId",cardId);
        component.set("v.cardErrorMsg",resultCard[0].Error_Message__c);
        //var spinner = component.find("mySpinner");
        //$A.util.toggleClass(spinner, "slds-hide");
        
    },
    
    correctCardDataHelper : function(component,event,errorCardId,newCardDetails){
        console.log('correct Card Detail Helper called');
        console.log('cardID',errorCardId);
        console.log('newcardDetials',newCardDetails.Name_On_Card__c);
    },
    sendMail:function(component,event,helper){
        //debugger
        var EmailBody = component.get('v.EmailBodyVal');
        var productName = component.get('v.productName');
        var contactId = component.get('v.recordId');
        var action = component.get("c.sendEmail");
        action.setParams({'contactId': contactId,
                          'emailBody':EmailBody,
                          'productName':productName});
        action.setCallback(this, function(response) {
           // debugger
            var state = response.getState();
            console.log("Failed with state: " + state);
            });
        $A.enqueueAction(action);
    
    },
    sendToVF : function(component, event, helper) {
        var message = "success";
        console.log('sendToVF',message);
        var vfOrigin = "https://" + component.get("v.vfHost");
        if (component.find("vfFrame")) {
            var vfWindow = component.find("vfFrame").getElement().contentWindow;
            vfWindow.postMessage(message, vfOrigin);
        }
        
    }
})