({
    doInit : function(component, event, helper) {
        component.set("v.errorMsg",'');
        component.set("v.isError",false);
        component.set("v.buttonD", true);
        var contactId = component.get("v.recordId");
        var action = component.get("c.getCards");
        action.setParams({ contactId :contactId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state); 
            debugger
            var result = response.getReturnValue();
            var b =   result.map(record => record.Id);
            component.set("v.cId",b);
            for(var i in result){
                var credCardNumber =  result[i].Credit_Card_Number__c;
                if(!$A.util.isUndefined(credCardNumber)) 
                {
                    credCardNumber= credCardNumber.replace(/ /g,"0");
                    result[i].numMask = credCardNumber.replace(/\d(?=\d{4})/g, "*");
                }
            }
            
            component.set("v.cardData", result);
            var spinner = component.find("mySpinner");
            $A.util.toggleClass(spinner, "slds-hide");
            console.log("-------------------------------=============="+response.getReturnValue());
        });
        
        var action1 = component.get("c.getCardsOpportunity");
        action1.setParams({ contactId :contactId});
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);            
            var result1 = response.getReturnValue();
            component.set("v.oppData",result1);
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        
        //debugger
        
    }
})