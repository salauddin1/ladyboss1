({
    doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
        component.find("overlayLib").notifyClose();                                   
    },
    
    deleteFromSalesforce: function(component, event, helper) {
        var valueData = event.getSource().get("v.value");
        console.log('------valueData---'+JSON.stringify(valueData));
        component.set("v.isOpen", false);
         var action = component.get("c.deleteCardFromSaleforce");
        action.setParams({singleCard: valueData});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if(state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log(result);
                if(result && result!=null && result!= '' && result !='null' && result == 'Card Deleted'){
                    helper.doInit(component, event, helper);
                   var spinner = component.find("mySpinner");
            $A.util.toggleClass(spinner, "slds-hide");
                }else{
                    component.set("v.errorMsg",result);
                    component.set("v.isError",true);
                }
            }else{
                var errors = response.getError();
                console.log(errors); 
                if(JSON.stringify(errors) == null){
                    component.set("v.errorMsg",'Internal Server Error');
                }else{
                    component.set("v.errorMsg",JSON.stringify(errors));
                }
                component.set("v.isError",true);
            }
        } );
        $A.enqueueAction(action);
    },
    
    deleteCardFromStripe : function(component, event, helper) {
        var contactId = component.get("v.recordId");
        var singleCardData = component.get("v.singleCardData");
        console.log('delete card call --- '+singleCardData);
        component.set("v.loaded",true);
        component.set("v.errorMsg",'');
                    component.set("v.isError",false);
        var action = component.get("c.deleteCard");
        action.setParams({singleCard: singleCardData});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if(state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log(result);
                if(result && result!=null && result!= '' && result !='null' && result == 'Card Deleted'){
                    component.set("v.loaded",false);
                    component.find("overlayLib").notifyClose();                                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success ',
                        message: result,
                        duration:' 1000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }else{
                    if(result.includes('No such source') || result.includes('No Stripe Card Id Found.')){
                        component.set("v.isOpen", true);
                    }
                    component.set("v.errorMsg",result);
                    component.set("v.isError",true);
                }
            }else{
                var errors = response.getError();
                console.log(errors); 
                if(JSON.stringify(errors) == null){
                    component.set("v.errorMsg",'Internal Server Error');
                }else{
                    component.set("v.errorMsg",JSON.stringify(errors));
                }
                component.set("v.isError",true);
            }
        } );
        $A.enqueueAction(action);
    },
    
    onCheckRadio1 : function(component, event, helper) {
        component.set("v.isError",false);
        var rLast4Digit = event.getSource().get("v.label");
        var valueData = event.getSource().get("v.value");
        console.log('------valueData---'+JSON.stringify(valueData));
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        component.set("v.singleCardData",valueData);
        component.set("v.buttonD", false);
    },
    
    onChangeEmail :  function(component, event, helper) {
        var value = event.getSource().get("v.value");
        component.set("v.emailId",value);
        
    },
    
})