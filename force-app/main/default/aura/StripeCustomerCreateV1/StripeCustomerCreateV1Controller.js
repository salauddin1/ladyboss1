({
    doInit : function (cmp, event, helper) {
        var conid = cmp.get("v.recordId");
        var action = cmp.get("c.getConDetails");
        action.setParams({
            conID : conid
        });
        action.setCallback(this, $A.getCallback(function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();  
                var title = 'Click to create customer in stripe';  
                if (result == 'Success') {
                    cmp.set("v.disableButton",false);
                }else{
                    title = result;
                }
                cmp.set("v.buttonTitle",title);
            }
            
        }));
        $A.enqueueAction(action);
    },
    createStripeProfile: function (cmp, event, helper) {
        cmp.set("v.Spinner",true);
        var conid = cmp.get("v.recordId");
        var action = cmp.get("c.createCustomer");
        action.setParams({
            conID : conid
        });
        action.setCallback(this, $A.getCallback(function(response) {
            var state = response.getState();
            var msg = 'Unknown error Occured please contact System Administrator';
            var type = 'error';
            var title = 'Error';
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();    
                if (result == 'Success') {
                    cmp.set("v.disableButton",true);
                    title = 'Success';
                    type = 'success';
                    msg = 'Successfully created Customer on Stripe';
                }
            }
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: title,
                message: msg,
                duration: ' 2000',
                key: 'info_alt',
                type: type,
                mode: 'pester'
            });
            toastEvent.fire();
            cmp.set("v.Spinner",false);
        }));
        $A.enqueueAction(action);
    }
})