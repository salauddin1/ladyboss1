({
    doInit : function(component, event, helper) {
        debugger
        var caseId = component.get("v.recordId");
        var action = component.get("c.getCase");
        action.setParams({ caseId :caseId});
        //  $A.get('e.force:refreshView').fire();
        action.setCallback(this, function(response) {
            var state = response.getState();
            var a = response.getReturnValue();
            component.set("v.dateVal",a.DueDate__c);
            component.set("v.email",a.SF_Email__c);
            component.set("v.video",a.SKA_Video__c);
            component.set("v.payment",a.Saved_Payment__c);
            component.set("v.correspondence",a.Stop_Correspondence__c);
            component.set("v.email_sent",a.Stunning_Email_Sent__c);
            component.set("v.message",a.Text_Message__c);
            component.set("v.recoverPayment",a.Recovered_Payment__c);
            component.set("v.called",a.Called__c);
            component.set("v.fbMessage",a.Facebook_Message__c);
            
        });
        $A.enqueueAction(action);
        
    },
    checkval: function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
        var checkVal = event.getSource().get("v.checked");
        
    }
})