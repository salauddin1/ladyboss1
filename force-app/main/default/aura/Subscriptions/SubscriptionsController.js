({
    doInit: function (component, event, helper) {
        component.set("v.Spinner1", false);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        var todayFormattedDate = yyyy + "-" + mm + "-" + dd;
        dd++;
        var todayFormattedDate2 = yyyy + "-" + mm + "-" + dd;
        component.set("v.myDate", todayFormattedDate);
        component.set("v.myDate2", todayFormattedDate2);
        var conID = component.get("v.recordId");
        var action = component.get("c.getAllSubscriptions");
        action.setParams({
            conID: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                var prods = [];
                console.log('result---', result);
                if (result && result != null) {
                    component.set("v.AllSubscriptions", result);
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].status == 'Active') {
                            prods.push(result[i]);
                        }
                    }
                    component.set("v.OppLineItem", prods);
                    component.set("v.isScheduled", false);
                    helper.init(component, event, helper);
                }
            }
        });
        $A.enqueueAction(action);

    },
    handleChange: function (component, event) {
        var l = event.getParam("value");
        console.log(event.getParam("value"));

        component.set("v.value", l);
        var listView = l;
        var prods = [];
        var allProds = component.get("v.AllSubscriptions");
        if (listView == "sAll") {
            component.set("v.OppLineItem", allProds);
        } else {
            for (var i = 0; i < allProds.length; i++) {
                if (allProds[i].status == listView) {
                    prods.push(allProds[i]);
                }
            }
            component.set("v.OppLineItem", prods);
        }
        if (listView == 'Scheduled') {
            component.set("v.isScheduled", true);
        } else {
            component.set("v.isScheduled", false);
        }
        /*var l = (event.getParam('value'));
        console.log(event.getParam('value'));

        component.set("v.value", l);
        var listView = l;
        console.log('Listview---', listView);
        var conID = component.get("v.recordId");
        var action = component.get("c.getSubscriptions");
        action.setParams({
            conID: conID,
            listView: listView
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                if (result && result != null) {
                    console.log('inside++');
                    component.set("v.OppLineItem", result);
                }
                if (listView == 'Past Due') {
                    component.set("v.isPastDue", true);
                } else if (component.get("v.isPastDue")) {
                    component.set("v.isPastDue", false);
                }
                console.log('outside++');
            }
        });
        $A.enqueueAction(action);*/
    },
    onRefreshSubscription: function (component, event, helper) {
        //salert('Button click');
        console.log('inisde controller');
        helper.helperMethod(component, event, component.get("v.recordId"));
    },
    cancelScheduleOpp: function (component, event, helper) {
        var oppIds = component.get("v.Id");
        console.log('oppIDSSS', oppIds);
        component.set("v.Spinner1", true);
        var action2 = component.get("c.setCancelDateOnSchedulePayment");
        action2.setParams({
            opplineItem: oppIds,
            cancelDate: component.get("v.cancelDate1")
        });

        action2.setCallback(this, function (response) {
            var msg = response.getReturnValue();
            console.log("---------re------" + response.getReturnValue());
            //helper.ToastMsg(component, event, helper, msg);
            component.set("v.Spinner1", false);

            helper.ToastMsg(component, event, helper, msg);

            $A.get('e.force:refreshView').fire();

        });
        $A.enqueueAction(action2);


        console.log('save:end');
        var cmpTarget = component.find(component.get("v.selectedModal"));
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    handleSchedulePayment: function (component, event, helper) {
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");

        var oppId = event.getParam("value");
        component.set("v.Id", oppId);
        var oppline = component.get("v.OppLineItem");
        if (selectedMenuItemValue == "Update") {
            var cmpTarget = component.find("ModalboxSchedule");
            var cmpBack = component.find("Modalbackdrop");
            $A.util.addClass(cmpTarget, "slds-fade-in-open");
            $A.util.addClass(cmpBack, "slds-backdrop--open");
            component.set("v.selectedModal", "ModalboxSchedule");

        } else if (selectedMenuItemValue == "Cancel") {
            var cmpTarget = component.find("ModalboxScheduleCancel");
            var cmpBack = component.find("Modalbackdrop");
            $A.util.addClass(cmpTarget, "slds-fade-in-open");
            $A.util.addClass(cmpBack, "slds-backdrop--open");
            component.set("v.selectedModal", "ModalboxScheduleCancel");
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
            component.set("v.cancelDate1", todayFormattedDate);
        } else if (selectedMenuItemValue == "Cancel After Payment") {
            for (var i = 0; i < oppline.length; i++) {
                if (oppline[0].Id == oppId) {
                    var dateop = new Date(oppline[0].ScheduledPaymentDate);
                    console.log('date',dateop);
                    var dd = dateop.getDate()+1;
                    var mm = dateop.getMonth() + 1; //January is 0!
                    var yyyy = dateop.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }

                    var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
                    /*newdate.setDate(newdate.getDate() + 1);*/
                    component.set("v.myDate1", todayFormattedDate);
                }
            }
            var cmpTarget = component.find("ModalboxScheduleCancelDate");
            var cmpBack = component.find("Modalbackdrop");
            $A.util.addClass(cmpTarget, "slds-fade-in-open");
            $A.util.addClass(cmpBack, "slds-backdrop--open");
            component.set("v.selectedModal", "ModalboxScheduleCancelDate");
        }
    },
    handleSelectProd: function (component, event, helper) {
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");
        var conInfo = component.get('v.ContactInfo');
        var oppId = event.getParam("value");
        component.set("v.Id", oppId);

        if (selectedMenuItemValue == "Update") {
            helper.openmodal(component, event, helper);
            component.set("v.selectedModal", "Modalbox");
        } else if (selectedMenuItemValue == "Cancel") {
            if (conInfo.isLastSub && conInfo.hasCommission) {                
                var cmpTarget = component.find('ModalboxWarning');
                var cmpBack = component.find('ModalbackdropCancel');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open');
                component.set("v.selectedModal", "ModalboxWarning");
            }else{                
                helper.openmodalCancel(component, event, helper);
                component.set("v.selectedModal", "ModalboxCancel");
            }
        } else if (selectedMenuItemValue == "Retry Charge") {
            helper.retrycharge(component, event, helper);
        }
    },
    dateUpdate: function (component, event, helper) {
        var value = event.getSource().get("v.value");
        component.set("v.dateVal", value);
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.dateVal") != '' && component.get("v.dateVal") < todayFormattedDate) {
            component.set("v.dateValidationError", true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------" + dV);
        } else {
            component.set("v.dateValidationError", false);
            component.set("v.update", true);

        }
    },
    dateUpdates: function (component, event, helper) {
        var value = event.getSource().get("v.value");
        component.set("v.dateVal", value);
        
        if (component.get("v.dateVal") != '' && component.get("v.dateVal") < component.get("v.myDate1")) {
            component.set("v.dateValidationError", true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------" + dV);
        } else {
            component.set("v.dateValidationError", false);
            component.set("v.update", true);

        }
    },
    closeModal: function (component, event, helper) {
        var cmpTarget = component.find(component.get("v.selectedModal"));
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        component.set("v.checkValD", false);

    },
    updateScheduleOpp: function (component, event, helper) {
        var dateValue = component.get("v.dateValSchedule");
        component.set("v.Spinner1", true);
        // var quntityVal = 1;
        var oppIds = component.get("v.Id");
        console.log('heloo===> ', component.get('v.Id'));
        var action = component.get("c.updateSchedulePayment");
        console.log('dateValue' + dateValue);
        action.setParams({
            LineItemId: oppIds,
            dateVal: dateValue
        });
        action.setCallback(this, function (response) {
            var msg = response.getReturnValue();
            console.log('SAVED.');
            //helper.ToastMsg(component, event, helper, msg);
            component.set("v.Spinner1", false);

            helper.ToastMsg(component, event, helper, msg);

            $A.get('e.force:refreshView').fire();

        });
        $A.enqueueAction(action);



        console.log('save:end');
        var cmpTarget = component.find("ModalboxSchedule");
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    updateOpp: function (component, event, helper) {
        var dateValue = component.get("v.dateVal");
        var checkVal = component.get("v.update");
        component.set("v.Spinner1", true);
        // var quntityVal = 1;
        var oppIds = component.get("v.Id");
        console.log('heloo===> ', component.get('v.Id'));
        var action = component.get("c.updateSubscription");
        console.log('dateValue' + dateValue);
        action.setParams({
            LineItemId: oppIds,
            dateVal: dateValue,
            checkVal: checkVal
        });
        action.setCallback(this, function (response) {
            var msg = response.getReturnValue();
            console.log('SAVED.');
            //helper.ToastMsg(component, event, helper, msg);
            component.set("v.Spinner1", false);
            if (msg == 'Subscription updated in stripe' || msg == 'Subscription Updated in PayPal') {
                helper.ToastMsg(component, event, helper, msg);
            } else {
                helper.ToastMsgError(component, event, helper, msg);
            }

            $A.get('e.force:refreshView').fire();

        });
        $A.enqueueAction(action);



        console.log('save:end');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        component.set("v.checkVal", false);


    },
    closeModel: function (component, event, helper) {
        $A.util.addClass(component.find('toastModel'), 'slds-hide');
    },
    cancelOpp: function (component, event, helper) {
        var nullbal = false;
        var coninfo = component.get('v.ContactInfo');
        if (coninfo.isLastSub && coninfo.hasCommission) {
            nullbal = true;
        }
        component.set("v.Spinner1", true);
        var oppIds = component.get("v.Id");
        console.log('oppIDSSS', oppIds);
        var action2 = component.get("c.setCancelDate");
        action2.setParams({
            LineItemId: oppIds,
            cancelDate: component.get("v.cancelDate"),
            nullBalance: nullbal
        });

        action2.setCallback(this, function (response) {
            var msg = response.getReturnValue();
            console.log("---------re------" + response.getReturnValue());
            //helper.ToastMsg(component, event, helper, msg);
            component.set("v.Spinner1", false);
            if (msg == 'Succesfully set cancel date' || msg == 'Subscription cancelled in stripe') {
                helper.ToastMsg(component, event, helper, msg);
            } else {
                helper.ToastMsgError(component, event, helper, msg);
            }

            $A.get('e.force:refreshView').fire();

        });
        $A.enqueueAction(action2);


        console.log('save:end');
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
    cancelOppConfirm: function (component, event, helper) {  
        var cmpTarget = component.find(component.get("v.selectedModal"));
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack, 'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');      
        helper.openmodalCancel(component, event, helper);
        component.set("v.selectedModal", "ModalboxCancel");
    }
})