({
    init: function (component, event, helper) {
        var conID = component.get("v.recordId");
        var action = component.get("c.checkIfAffiliate");
        action.setParams({
            conID: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state',state);
            
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result---', result);
                if (result && result != null) {
                    component.set('v.ContactInfo',result);
                }
            }
        });
        $A.enqueueAction(action);
    },
    helperMethod: function (component, event, conID) {
        console.log('inside helper');
        component.set("v.Spinner", !component.get('v.Spinner'));
        var action = component.get("c.refreshSubscription");
        action.setParams({
            conIDwhen: conID
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('inside refresh');
            component.set("v.Spinner", !component.get('v.Spinner'));
            if (state == "SUCCESS") {
                console.log('inside refresh SUCCESS');
                var a = component.get('c.doInit');
                $A.enqueueAction(a);
            }
            component.set("v.value", 'Active');
        });
        $A.enqueueAction(action);
    },
    openmodalCancel: function (component, event, helper) {
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    ToastMsg: function (component, event, helper, msg) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            title: 'Success ',
            message: msg,
            duration: ' 2000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        var a = component.get('c.doInit');
        $A.enqueueAction(a);
        component.set("v.value", 'Active');
    },
    openmodal: function (component, event, helper) {

        component.set("v.checkValD", true);

        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    retrycharge: function (component, event, helper) {
        component.set("v.Spinner1", !component.get('v.Spinner1'));
        var action = component.get("c.payPastDueInvoice");
        action.setParams({
            LineItemId: component.get("v.Id")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set("v.Spinner1", !component.get('v.Spinner1'));
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result == 'Success') {
                    this.ToastMsg(component, event, helper, 'Successfully charged for Subscription');
                }else if (result != null) {
                    this.ToastMsgError(component, event, helper, result);
                }else{
                    this.ToastMsgError(component, event, helper, 'Unknown Error');
                }
                
            }
            
        });
        $A.enqueueAction(action);
    },
    ToastMsgError: function (component, event, helper, msg) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            title: 'Error',
            message: msg,
            duration: ' 2000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    }
})