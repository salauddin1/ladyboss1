({

	init: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var action = component.get("c.getProducts");
		console.log('init called' + recordId);
		action.setParams({
			conID: recordId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			console.log('init ' + state);
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				var subtotal = 0.00;
				var tax = 0.00;
				var shipping = 0.00;
				var total = 0.00;
				console.log('init ' + result);
				if (result.length == 0) {
					component.set("v.isEmptyList", true);
				} else {
					for (var i = 0; i < result.length; i++) {
						subtotal += result[i].totalPrice;
						tax += result[i].taxAmount;
						shipping += result[i].shippingCost;
					}
				}
				subtotal -= shipping;
				total = subtotal + tax + shipping;
				component.set("v.OppLineItem", result);
				component.set("v.subTotal", subtotal.toFixed(2));
				component.set("v.tax", tax.toFixed(2));
				component.set("v.shipping", shipping.toFixed(2));
				component.set("v.total", total.toFixed(2));
				component.set("v.remainingMemTotal", total.toFixed(2));
			}
		})
		$A.enqueueAction(action);

	},
	handleProductChange: function (component, event, helper) {
		console.log('handleProductChange called');
		var selectedProdOne = [];
		component.set("v.isSelected", false);
		var prodItems = component.get("v.OppLineItem");
		console.log('prodItem', prodItems);
		var currentTotal = 0.00;
		var remainingTotal = component.get("v.total");
		var shipping = 0.00;
		for (var i = 0; i < prodItems.length; i++) {
			console.log('prodItems', prodItems[i]);
			if (prodItems[i].checkValue == true) {
				console.log('amountToPayFromCard1', prodItems[i].checkValue);
				selectedProdOne.push(prodItems[i]);
				currentTotal += prodItems[i].totalPrice;
			}
		}
		if (selectedProdOne.length < 1) {
			component.set("v.isSelected", false);
			component.set("v.verifyDetails", false);
		} else {
			component.set("v.isSelected", true);
		}
		remainingTotal = remainingTotal - currentTotal;
		component.set("v.currentMemTotal", currentTotal.toFixed(2));
		component.set("v.remainingMemTotal", remainingTotal.toFixed(2));
		component.set("v.productOne", selectedProdOne);
		console.log('selected products ', selectedProdOne);
	},
	sendEmail: function (component, event, helper) {
		component.set("v.Spinner", true);
		var selectedProd = component.get("v.productOne");
		console.log('selected prod', selectedProd);
		var action = component.get("c.EmailReminder");
		action.setParams({
			prod: JSON.stringify(selectedProd)
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			var msgType = 'success';
			var msg = 'Successfully set Reminder for your memberships';
			console.log('init ' + state);
			if (state == "SUCCESS") {
                var refreshevent = $A.get("e.c:RefreshSubscriptions");
				refreshevent.setParams({
					"data": 'expense'
				});
				refreshevent.fire();
			} else {
				msg = 'Issue processing your request right now please consult support.';
				msgType = 'error';
			}
			component.set("v.message", msg);
			component.set("v.messageType", msgType);
			$A.util.removeClass(component.find('toastModel'), 'slds-hide');
			$A.util.addClass(component.find('toastModel'), 'slds-show');
			setTimeout(function () {
				$A.util.addClass(component.find('toastModel'), 'slds-hide');
				component.set("v.message", "");
				component.set("v.messageType", "");
			}, 5000);
			component.set("v.Spinner", false);
		})
		$A.enqueueAction(action);
		
		component.set("v.isSelected", false);
		component.set("v.verifyDetails", false);
	}
})