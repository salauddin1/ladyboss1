({
    doInit : function(component, event, helper) {
        var contactId = component.get("v.recordId");
        var action = component.get("c.getOpportunity");
        action.setParams({ contactId :contactId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                if(a != null){
                    var b = a.length;
                    if(b > 0){
                        component.set("v.OppValue" , true);
                    }
                    else{
                        component.set("v.OppValue" , false);
                    }
                }
                component.set("v.Opplist", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action1 = component.get("c.getOpportunityLineItemRefund");
        action1.setParams({ contactId :contactId});
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                component.set("v.OppLineNameRefund", response.getReturnValue());
                var opplineRefund = component.get("v.OppLineNameRefund").length;
                var oppRefund = component.get("v.OppNameRefund").length;
                if((opplineRefund || oppRefund) > 0 ){
                    component.set("v.OppValueRefund" , true);
                }
                else{
                    component.set("v.OppValueRefund" , false);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action2 = component.get("c.getOpportunityLineItem");
        action2.setParams({ contactId :contactId});      
        action2.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                component.set("v.OppLineItem", a);
                if(a != null && a.length > 0){
                    component.set("v.OppValue1" , true);
                }
                else{
                    component.set("v.OppValue1" , false);
                }
                
                var dateVal = component.get("v.OppLineName");
                console.log("____BLine Item_____________"+dateVal);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                var oppline = component.get("v.OppLineItem").length;
                var opplist = component.get("v.Opplist").length;
                if((oppline || opplist) > 0 ){
                    component.set("v.OppValueAll" , true);
                }else{
                    component.set("v.OppValueAll" , false);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action3 = component.get("c.getPartiallyPaidOpportunity");
        action3.setParams({ contactId :contactId});
        
        action3.setCallback(this, function(response) {
             var state = response.getState();
            console.log("Failed with state: " + state);
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                if(a != null){
                    var b = a.length;
                    if(b > 0){
                        component.set("v.OppPPValue" , true);
                    }
                    else{
                        component.set("v.OppPPValue" , false);
                    }
                }
                component.set("v.OppPPlist", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action4 = component.get("c.getrefundedOpportunity");
        action4.setParams({ contactId :contactId});
        
        action4.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                console.log("____refund return val_____________"+a);
                component.set("v.OppValueRefund" , true);
                component.set("v.OppNameRefund", a);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
        $A.enqueueAction(action4);
    },
    openmodal:function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    closeModal:function(component,event,helper){ 
        helper.closeModal1(component,event);
    },
    abc: function(component, event, helper) {
        var placeho;
        var value = event.getSource().get('v.value') ;
        component.set("v.drop", value);
       var cmpTarget = component.find('addressform2');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');

        
        if(value == 'other'){
            component.set("v.placeholder", 'Add more detail about refund. ');
            component.set("v.req", true);
        }
        else if (value == 'duplicate'){
            component.set("v.placeholder", 'Add more detail about duplicate stripe refund. ');
            component.set("v.req", false);
        }
            else if (value == 'fraudulent'){
                component.set("v.placeholder", 'Add more detail about fraudulent stripe refund. ');
                component.set("v.req", false);
            }
                else if (value == 'Requested_by_customer'){
                    component.set("v.placeholder", 'Add more detail about requested by customer stripe refund. ');
                    component.set("v.req", false);
                }
        console.log("------------"+value);
    },
    abcInternal:function(component, event, helper) {
        var placeho;
        var value = event.getSource().get('v.value') ;
        component.set("v.dropInt", value);
        var cmpTarget = component.find('addressform4');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');

        
        if(value == 'Other'){
            component.set("v.placeholderInt", 'Add more detail about Other.');
            component.set("v.req1", false);
        }
        else if (value == 'Did not know CLUB was active'){
            component.set("v.placeholderInt", 'Add more detail about Did not know CLUB was active.  ');
            component.set("v.req1", false);
        }
 		else if (value == 'Charged for upgrade when I selected one product'){
            component.set("v.placeholderInt", 'Add more detail about Charged for upgrade when I selected one product.  ');
            component.set("v.req1", false);
        }
        else if (value == 'Did not submit form on time'){
            component.set("v.placeholderInt", 'Add more detail about Did not submit form on time.  ');
            component.set("v.req1", false);
        }
        else if (value == 'Did not know Trainer Subscription was active'){
            component.set("v.placeholderInt", 'Add more detail about Did not know Trainer Subscription was active  ');
            component.set("v.req1", false);
        }
        else if (value == 'Customer not cancelled'){
            component.set("v.placeholderInt", 'Add more detail about Customer not cancelled  ');
            component.set("v.req1", false);
        }
       else if (value == 'Product not what I want any more'){
            component.set("v.placeholderInt", 'Add more detail about Product not what I want any more ');
            component.set("v.req1", false);
        }
         else if (value == 'Product broken/packaging open'){
            component.set("v.placeholderInt", 'Add more detail about Product broken/packaging open ');
            component.set("v.req1", false);
        }
         else if (value == 'Allergic to product'){
            component.set("v.placeholderInt", 'Add more detail about Allergic to product ');
            component.set("v.req1", false);
        }
         else if (value == 'Did not like the taste/flavor'){
            component.set("v.placeholderInt", 'Add more detail about Did not like the taste/flavor ');
            component.set("v.req1", false);
        }
        else if (value == 'Too expensive'){
            component.set("v.placeholderInt", 'Add more detail about Too expensive ');
            component.set("v.req1", false);
        }
        else if (value == 'Duplicate charge'){
            component.set("v.placeholderInt", 'Add more detail about Duplicate charge ');
            component.set("v.req1", false);
        }
         else if (value == 'Wrong product sent'){
            component.set("v.placeholderInt", 'Add more detail about  Wrong product sent. ');
            component.set("v.req1", false);
        }
        else if (value == 'Product took too long to get to me'){
            component.set("v.placeholderInt", 'Add more detail about  Product took too long to get to me. ');
            component.set("v.req1", false);
        }
                  
        console.log("------------"+value);
    },
    description:function(component, event, helper) {
        var desValue = event.getSource().get('v.value') ;
        component.set("v.description", desValue);
    },
    intDescription: function(component, event, helper) {
        var desValue = event.getSource().get('v.value') ;
        component.set("v.intDescription", desValue);
    },
    
    save : function(component, event, helper) {
        
        component.set("v.isError",false);
        component.set("v.errorMsg",'');
        component.set("v.isProductError",false);
        component.set("v.errorProductMsg",'');
        var contactId = component.get("v.recordId");
        var oppId =  component.get("v.radioId");
        var oppLineItemId = component.get("v.OppLineId");
        var OppLineChargeId= component.get("v.OppLineChargeId");
        //var OppLineSubId = component.get("v.OppLineSubId");
        var OppLineUnitAmt = component.get("v.OppUnitPrice");
        var OppLineUnitAmtInt = parseInt(OppLineUnitAmt *100);
        var DisclaimerValue = component.get("v.DisclaimerValue");
        var reason =  component.get("v.drop");
        var errorMsg = component.get("v.errorMsg"); 
        var internalReason = component.get("v.dropInt");
        var irDetails = component.get("v.intDescription");
        
        console.log("-----internalReason--------"+internalReason);
        console.log("-----irDetails--------"+irDetails);
        console.log("-----dropVal--------"+reason);
        console.log("-----oppId--------"+oppId);
        console.log("-----oppLineItemId--------"+oppLineItemId);
        console.log("-----OppLineUnitAmt--------"+OppLineUnitAmtInt);
        console.log("-----OppLineChargeId--------"+OppLineChargeId);
        //debugger
        if(!reason || reason == '--Select--'){
            console.log("-----reason--------"+reason);
            component.set("v.isError",true);
            component.set("v.errorMsg",'Please Select Reason.');
        }
        if(!OppLineUnitAmtInt || !(OppLineUnitAmtInt > 0)){
            console.log("-----OppLineUnitAmtInt--------"+reason);
            component.set("v.isError",true);
            component.set("v.errorMsg",'Please Enter Amount Greater Than 0.');
        }
        if((!oppLineItemId || oppLineItemId == null || oppLineItemId == 'null') && (!oppId || oppId == null || oppId == 'null' ) && (!OppLineChargeId || OppLineChargeId == null || OppLineChargeId == 'null' )){

            component.set("v.isProductError",true);
            component.set("v.errorProductMsg",'Please Select Atleast One Item.');
        }
        if(!DisclaimerValue){
            console.log("-----DisclaimerValue--------"+reason);
            component.set("v.isError",true);
            component.set("v.errorMsg",'To Proceed Check "Are You Sure..." box');
        }
        
        var isError = component.get("v.isError"); 
        var isProductError = component.get("v.isProductError");
        var NumValidationError = component.get("v.NumValidationError"); 
        console.log("-----NumValidationError--------"+NumValidationError);
        console.log("-----isProductError--------"+isProductError);
        console.log("-----isError--------"+isError);
        console.log("-----DisclaimerValue--------"+DisclaimerValue);
        if(!NumValidationError && !isProductError && !isError && OppLineUnitAmt && oppLineItemId && OppLineUnitAmtInt && DisclaimerValue && reason && contactId ){
            var spinner = component.find("mySpinner");
            $A.util.toggleClass(spinner, "slds-hide");
            var action = component.get("c.getUpdate");
            action.setParams({ contactId: contactId ,
                              opporunityId :oppId,
                              oppLineItemId:oppLineItemId,
                              Amount:OppLineUnitAmtInt,
                              reason:reason,
                              oppChargeId:OppLineChargeId,
                              internalReason:internalReason,
                              irDetails:irDetails});
            action.setCallback(this, function(response) { 
                
                var state = response.getState();
                if(state == "SUCCESS" ){
                    
                    var result = response.getReturnValue();
                    console.log(result);
                    if(result=='Refund succeeded in stripe'){
                        component.find("overlayLib").notifyClose();                                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success ',
                        message: result,
                        duration:' 1000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    }else{
                        component.set("v.errorPresent",true);
                        component.set("v.errorMsgNew",result);
                    }
                    
                    var spinner = component.find("mySpinner");
                    $A.util.toggleClass(spinner, "slds-hide");
                }else if (state == "ERROR"){
                    
                    var errors = response.getError();
                    console.log(errors); 
                    if(JSON.stringify(errors) == null){
                        component.set("v.errorMainMsg",'Internal Server Error');
                    }else{
                        component.set("v.errorMainMsg",JSON.stringify(errors));
                    }
                    
                    component.set("v.isMainError",true);
                    var spinner = component.find("mySpinner");
                    $A.util.toggleClass(spinner, "slds-hide");
                }
                
            } );
            $A.enqueueAction(action);
            
        }
        
    },
    
    onCheckRadio :function(component, event, helper) {
        // debugger
        helper.clearValues(component,event,helper);
        var OppNameVal = event.getSource().get("v.label");
        var OppId = event.getSource().get("v.value");
        var OppAmt = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.OppLineUnitAmt" ,OppAmt);
        component.set("v.OppUnitPrice" ,OppAmt.toFixed(2));
        component.set("v.AmountVal" ,OppAmt);
        component.set("v.radioVal",OppNameVal);
        component.set("v.radioId",OppId);
        
    },
    
    onCheckRadioWithCharge :function(component, event, helper) {
        helper.clearValues(component,event,helper);
        var OppNameVal = event.getSource().get("v.label");
        var OppId = event.getSource().get("v.value");
        var OppAmt = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.OppLineUnitAmt" ,OppAmt);
        component.set("v.OppUnitPrice" ,OppAmt.toFixed(2));
        component.set("v.AmountVal" ,OppAmt);
        component.set("v.radioVal",OppNameVal);
        component.set("v.OppLineChargeId",OppId);
        
    },
    
    onCheckRadio1 :function(component, event, helper) {
        helper.clearValues(component,event,helper);
        var OppLineNameVal = event.getSource().get("v.label");
        var OppLineId = event.getSource().get("v.value");
        var  OppLineUnitAmt= event.getSource().get("v.id");
         //var OppLineUnitAmt = math.fraction(oAmount, 3) ; 
        var Oppcheckradio = event.getSource().get("v.checked");
        component.set("v.OppUnitPrice" ,OppLineUnitAmt.toFixed(2));
        
        component.set("v.checkRadioLine" ,Oppcheckradio);
        console.log("-------------"+Oppcheckradio);
        component.set("v.OppLineUnitAmt" ,OppLineUnitAmt);
        component.set("v.OppLineItem.UnitPrice" ,OppLineUnitAmt);
        component.set("v.OppLineNameVal",OppLineNameVal);
        component.set("v.OppLineId",OppLineId);
    },
    
    onChangeAmount :function(component, event, helper) {
        var amt = component.get("v.OppLineUnitAmt");
        console.log("-------"+amt);
        var value = event.getSource().get("v.value");
       
        component.set("v.OppUnitPrice",value);
        
        if(amt < parseFloat(value) ){
            component.set("v.NumValidationError" , true); 
        }
        else{
            component.set("v.NumValidationError" , false); 
            
        }
    },
    
})