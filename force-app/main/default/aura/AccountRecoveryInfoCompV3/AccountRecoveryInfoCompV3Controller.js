({
    doInit : function(component, event, helper) {
        debugger
        var caseId = component.get("v.recordId");
        var action = component.get("c.getCase");
        action.setParams({ caseId :caseId});
        //  $A.get('e.force:refreshView').fire();
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            var a = response.getReturnValue();
            component.set("v.dateVal",a.DueDate__c);
            component.set("v.Call1AM",a.Call_1_AM__c);
            component.set("v.Call2AM",a.Call_2_AM__c);
            component.set("v.Call3AM",a.Call_3_AM__c);
            component.set("v.Call4AM",a.Call_4_AM__c);
            component.set("v.Call5AM",a.Call_5_AM__c);
            component.set("v.call1PM",a.C1PM__c);
            component.set("v.call2PM",a.C2PM__c);
            component.set("v.call3PM",a.C3PM__c);
            component.set("v.call4PM",a.C4PM__c);
            component.set("v.call5PM",a.C5PM__c);
            component.set("v.payment",a.Saved_Payment__c);
            component.set("v.message",a.Text_Message__c);
            component.set("v.recoverPayment",a.Recovered_Payment__c);
            component.set("v.email",a.SF_Email__c);
            component.set("v.video",a.SKA_Video__c);
            component.set("v.email_sent",a.Stunning_Email_Sent__c);
            component.set("v.called",a.Called__c);
            component.set("v.fbMessage",a.Facebook_Message__c);
            component.set("v.correspondence",a.Stop_Correspondence__c);
            component.set("v.NotRecovered",a.Not_recovered__c);
            component.set("v.NeverAnswered",a.Never_Answered__c);
            component.set("v.DoNotContact",a.Do_Not_Contact__c);
            
        });
        $A.enqueueAction(action);
        
         var action3 = component.get("c.getCaseRecord"); 
        action3.setParams({ caseId :caseId});
        action3.setCallback(this, function(response) {
            debugger
            console.log(response.getReturnValue());
            component.set("v.cas", response.getReturnValue());
        });
       
         $A.enqueueAction(action3);
        
        
        var action2 = component.get("c.getfields");
        action2.setCallback(this, function(response) {
            debugger
            var customFieldVal = [];
            
            var columnFieldVal = [];
            var rep = response.getReturnValue();
            console.log('------rep------'+rep);
            console.log('---------fieldsName--------'+response.getReturnValue());
            component.set("v.fieldsName", response.getReturnValue());
            var caseVal = component.get('v.cas');
            debugger
            var fieldVal = component.get('v.fieldsName');
            for(var i =0; i < fieldVal.length ; i++){
              if(fieldVal[i].Column__c == '1') {
                    columnFieldVal.push({fieldsName :fieldVal[i],
                                         isboolean:caseVal[fieldVal[i].Field_Api_Name__c]});
                }
                else if(fieldVal[i].Column__c == '2'){
                    customFieldVal.push({fieldsName :fieldVal[i],
                                         isboolean:caseVal[fieldVal[i].Field_Api_Name__c]});
                }                
            }
            component.set('v.customFieldData',customFieldVal);
            component.set('v.columnFieldData',columnFieldVal);
        });
        $A.enqueueAction(action2);
    },
    checkval: function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
        var checkVal = event.getSource().get("v.checked");
        
    }
})