({
	init : function(component, event, helper) {
        console.log('init called popup');
        var recId = component.get("v.accId");
        var productName = component.get("v.productName");
        var site = component.get("v.recordId");
        console.log('--------new recId----'+site);
        var action = component.get("c.siteConfig");
        
        action.setParams({"siteURL": site, "cntId":recId,product : productName});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                if(result && result != null && result != ''){
                    console.log('call back Result--->',site+'?fldString='+result);
                    component.set("v.recordId",site+'?fldString='+result);
                }
            }
        });
        $A.enqueueAction(action);
    }
})