({
    init : function(component, event, helper) {
        debugger
        console.log('init');
        console.log('init');
        var taskId = component.get("v.recordId");
        console.log("--------taskId---"+taskId);
        
        var action2 = component.get("c.getAllTask"); 
        action2.setParams({recordId : taskId});
        action2.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            component.set("v.tas", response.getReturnValue());
            var tar = response.getReturnValue()[0].Not_Time_Yet__c;
            if(tar == true) {
                component.set("v.ishide",true);
            }
        else{
           component.set("v.ishide",false); 
        }
        });
        $A.enqueueAction(action2);
        
    }, 
    save : function(component,event) {
        debugger
        var taskId = component.get("v.recordId");
        console.log("---------taskId---------"+taskId);
        var checked = event.getSource().get('v.value');
        var name = event.getSource().get('v.name');
        if(name == 'Not_Time_Yet__c'){
            if(checked == true) {
                component.set("v.ishide",true);
            }
            else{
               component.set("v.ishide",false);  
            }
        }
        var action = component.get("c.Changestatus");
        action.setParams({
            recordId : taskId,
            fieldName : name,
            CheckedValue :checked.toString()
        });
         action.setCallback(this, function(response) {
             debugger
           
            console.log(response.getReturnValue());
             component.set("v.tas", response.getReturnValue());
             
         $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
})