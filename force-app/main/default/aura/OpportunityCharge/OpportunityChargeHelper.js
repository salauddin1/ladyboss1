({
	init : function(component, event, helper) {
        var conID =component.get("v.recordId");
        console.log('hello');
        var action = component.get("c.getOpportunity");
        action.setParams({conID:conID});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if(state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('result---', result);
                if(result && result!=null){
                    component.set("v.OppLineItem", result);
                }
            }
        } );
        $A.enqueueAction(action);
    }
})