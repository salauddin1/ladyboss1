({
    doInit : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var numberOfRows = [];
        numberOfRows.push('0');
        var productList = [];
        var multipleLookup = [];
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        
        
        component.set('v.multipleLookups',multipleLookup);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        var action = component.get("c.getContact");
        action.setParams({ contactId :contactId});
        
        action.setCallback(this, function(response) {
            debugger
            var a = response.getReturnValue();
            component.set("v.contactVal",a);
            component.set("v.contactId1",a[0].Id);
            var conId = component.get("v.contactId1");
            var contactId = component.get("v.recordId");
            
            if(contactId ==	conId){
                
                component.set("v.newContact",a[0]);
                component.set("v.newContact.customerEmail__c",a[0].Email);
                component.set("v.newContact.Name",a[0].Name);
                component.set("v.newContact.phone__c",a[0].Phone);
                //component.set("v.newContact.orderStatus__c","Awaiting shipment");
                component.set("v.newContact.orderStatus__c",'Draft');
                if(a[0].Addresses__r !== undefined){
                    component.set("v.newContact.street1__c",a[0].Addresses__r[0].Shipping_Street__c);
                    component.set("v.newContact.city__c",a[0].Addresses__r[0].Shipping_City__c);
                    component.set("v.newContact.Zip",a[0].Addresses__r[0].Shipping_Zip_Postal_Code__c);
                    component.set("v.newContact.state__c",a[0].Addresses__r[0].Shipping_State_Province__c);
                    component.set("v.newContact.country__c",a[0].Addresses__r[0].Shipping_Country__c);
                }
            }
            else{
                 component.set("v.newContact.Contact__c",contactId);
                component.set("v.newContact.orderStatus__c","awaiting_shipment");
                component.set("v.newContact.Name",a[0].name__c);
                component.set("v.newContact.customerEmail__c",a[0].customerEmail__c);     
                component.set("v.newContact.orderNumber__c",a[0].Name);
                component.set("v.newContact.phone__c",a[0].phone__c);
                component.set("v.newContact.street1__c",a[0].street1__c);
                component.set("v.newContact.city__c",a[0].city__c);
                component.set("v.newContact.Zip",a[0].postalCode__c);
                component.set("v.newContact.state__c",a[0].state__c);
                
            }
            var newContact = component.get("v.newContact"); 
            console.log("Create newContact: " + JSON.stringify(newContact));
            var newContact1 = JSON.stringify(newContact);
            var items = component.get('v.multipleLookups');
            var idListJSON=JSON.stringify(items);
            items.forEach(function(item) {
                console.log(item.selectedItem);
            });
            //var conId = component.get("v.contactId1");
            if(contactId ==conId){
                debugger
                
                var contactId = component.get("v.recordId");
                var action1 = component.get("c.getOrderNew");
                console.log('=================newContact==================='+JSON.stringify(newContact));
                console.log('=================idListJSON==================='+idListJSON);
                action1.setParams({OrderIds:newContact1,
                                   contactId:contactId});
                
                action1.setCallback(this, function(response) {
                });
                
                var action2 = component.get("c.getShip");
                console.log('=================newContact==================='+JSON.stringify(newContact));
                console.log('=================idListJSON==================='+idListJSON);
                action2.setCallback(this, function(response) {
                    var a = response.getReturnValue();
                    component.set("v.newContact.orderNumber__c",a[0].Name);
                });
                $A.enqueueAction(action1);
                
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    remove :function(component, event, helper) {   
        debugger
        if(event.target.parentNode.getElementsByClassName("value_span")[0] !== undefined){
            var selectItemList = component.get("v.selectItemLists");
            var removeVal = event.target.parentNode.getElementsByClassName("value_span")[0].innerHTML;
            var newSel = selectItemList.filter(a => a !== removeVal);
            component.set("v.selectItemLists",newSel);
            var multiVal = component.get("v.multipleLookups");
            console.log(JSON.stringify(multiVal));
            //multiVal = multiVal.filter(mulVal => mulVal.selectedItem["text"] !== removeVal);
             multiVal = multiVal.filter(obj => (obj.selectedItem && obj.selectedItem['text'] !== removeVal) || !obj.selectedItem);
            console.log(JSON.stringify(multiVal));
            component.set("v.multipleLookups",multiVal);           
        } 
        else{
            var a = component.get("v.selectItemLists");
            a.push(" ");
            component.set('v.selectItemLists',a);
            event.target.parentNode.remove();
        }
        var rowIndex = event.getSource().get("v.value");
        
        // if(rowIndex>0)
        
        /* var multiVal = component.get("v.multipleLookups");
        console.log(JSON.stringify(multiVal[1]));
         var keys = Object.keys(multiVal);
        var newKeys = keys.filter(b => b != rowIndex);
        var bln = {};
       newKeys.forEach(b => bln[b] = multiVal[b]);
        var ctr =0;
        newKeys.map(entry => {
			debugger
            if(Object.entries(entry.selectedItem || {}).length === 0) {
                         // component.set("v.addButton",true);
                         ctr++; 
        }
        });
            if(ctr > 1){
            component.set("v.addButton",true);
        }
            else{
            component.set("v.addButton",false);
        }*/
        
        /*var selectItemList1 = component.get("v.selectItemLists");
        if(selectItemList1.length > 0){
            component.set("v.addButton",false);
        }
        else{component.set("v.addButton",true);}*/
        
    },
    save :function(component, event, helper){
        debugger
        var contactId = component.get("v.recordId");
        var conId = component.get("v.contactId1");
        if(contactId != conId){
            
            var validExpense = component.find('orderform').reduce(function (validSoFar, inputCmp) {
                // Displays error messages for invalid fields
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if(validExpense){
                var newContact = component.get("v.newContact"); 
                console.log("Create newContact: " + JSON.stringify(newContact));
                var newContact1 = JSON.stringify(newContact);
                var items = component.get('v.multipleLookups');
                var idListJSON=JSON.stringify(items);
                items.forEach(function(item) {
                    console.log(item.selectedItem);
                });
                var newProduct = component.get("v.newProduct"); 
                console.log("Create newProduct: " + JSON.stringify(newProduct));
                var contactId = component.get("v.recordId");
                var action = component.get("c.getOrderShip1");
                console.log('=================newContact==================='+JSON.stringify(newContact));
                console.log('=================idListJSON==================='+idListJSON);
                action.setParams({ContactId: contactId,
                                  OrderIds:newContact1,
                                  LineItems :idListJSON});
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    var a = response.getReturnValue();
                    if(a === undefined || a === null){
                        
                        component.find("overlayLib").notifyClose();                                   
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Success ',
                            message: 'Order succeesful created',
                            duration:' 1000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                    else {
                        component.set("v.errormsg", a);
                    }
                });
            } 
            $A.enqueueAction(action); 
        }
        else{
            var newContact = component.get("v.newContact"); 
            var newContact1 = JSON.stringify(newContact);
            var items = component.get('v.multipleLookups');
            var idListJSON=JSON.stringify(items);
            
            var action1 = component.get("c.updateOrderShip");
            action1.setParams({ContactId:contactId,
                               OrderIds:newContact1,
                               LineItems:idListJSON});
            
            action1.setCallback(this, function(response) {
                var state = response.getState();
                    var a = response.getReturnValue();
                    if(a === undefined || a === null){
                component.find("overlayLib").notifyClose();                                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: 'Order succeesful created',
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                }
                    else {
                        component.set("v.errormsg", a);
                    }
            });
            $A.enqueueAction(action1); 
        }
        
    },
    show: function(component, event, helper) {
        var items = component.get('v.multipleLookups');
        items.forEach(function(item) {
            console.log(item.selectedItem);
        });
    },
    Add : function(component, event, helper) {
        debugger
        var multipleLookup = component.get("v.multipleLookups");
        multipleLookup.push({'Quantity' : '1',
                             'selectedItem': {}                             
                            });
        component.set('v.multipleLookups',multipleLookup);
        component.set("v.addButton",true);
    },
    cancel:function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
})