({
    
    init : function(component, event, helper) {
        console.log('init called');
        var recId = component.get("v.recordId");
        console.log('opportunity Id ---> '+recId);    
        helper.getInitValues(component,event,recId);
        component.set("v.Spinner", false);
        //debugger;
        
    },
   
    updateContact : function(component, event, helper) {
        
        var updatec = component.get("v.updateCon");
        console.log("updateCon---",updatec);
        component.set("v.updateCon",!updatec);
    },
    addNewAddress : function(component, event, helper) {
        var newadd = component.get("v.newAddress");
        component.set("v.addAddress.Shipping_Country__c",'US');
        
        console.log("newadd---",newadd);
        component.set("v.newAddress",!newadd);
        console.log("newadd-updated--",component.get("v.newAddress"));
    },
    addNewCard: function(component, event, helper) {
        var newCC = component.get("v.newCard");
        console.log("newCard---",newCC);
        component.set("v.newCard",!newCC);
        var cardDetails = {};
        component.set("v.newCardDetails",cardDetails);
        console.log("newcard-updated--",component.get("v.newCard"),component.get("v.newCardDetails"));
    },
    
    setBillingAddress: function(component, event, helper) {
        var newadd = component.get("v.newCardAddress");
        console.log('checked',event.getSource().get("v.checked"));
        var isChecked = event.getSource().get("v.checked");
        if(isChecked){
            var address = component.get("v.contact.address");
            component.set("v.newCardDetails.address.Billing_Street__c",address.Shipping_Street__c);
            component.set("v.newCardDetails.address.Billing_City__c",address.Shipping_City__c);
            component.set("v.newCardDetails.address.Billing_State_Province__c",address.Shipping_State_Province__c);
            component.set("v.newCardDetails.address.Billing_Country__c",address.Shipping_Country__c);
            component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",address.Shipping_Zip_Postal_Code__c);
            
        }
        else{
            component.set("v.newCardDetails.address.Billing_Street__c",'');
            component.set("v.newCardDetails.address.Billing_City__c",'');
            component.set("v.newCardDetails.address.Billing_State_Province__c",'');
            component.set("v.newCardDetails.address.Billing_Country__c",'');
            component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",'');
        }
        console.log("newadd---",newadd);
        component.set("v.newCardAddress",!newadd);
        console.log("newadd-updated--",component.get("v.newCardAddress"));        
    },
    closeOpiton: function (component, event, helper) {
        component.set("v.isRenderContactResults",false);
    },
    selectContact: function (component, event, helper) {
        console.log();
        var selectedItem = event.currentTarget.dataset.record;
        var contactId = event.currentTarget.dataset.value;
        console.log('selected Contact',selectedItem);
        console.log('selected Id',contactId);
        component.set("v.inputContact", selectedItem);
        component.set("v.contactId", contactId);
        component.set("v.isRenderContactResults",false);
        component.set("v.contactOptions",null);   
        helper.getSelectedContact(component, event, contactId);
    },
    contactkeyPressController: function(component,event,helper){
        var srchString = component.get("v.inputContact");
        if(srchString.length > 1){
            component.set("v.isRenderContactResults",true); 
            console.log('is render con',component.get("v.isRenderContactResults"))
            helper.getContactOptions(component,event);
        }
    },  
    updateConact : function(component, event, helper) {
        var contact =  component.get("v.contact");
        var cId = component.get("v.contactId");
        console.log('update contact ---> ',contact,cId);
        helper.updateContactInfo(component, event);        
    },
    primaryAddress : function(component, event, helper) {
        var updatec = component.get("v.isPrimaryAddress");
        console.log("updateCon---",updatec);
        component.set("v.isPrimaryAddress",!updatec);
    },
    splitPayment: function (component, event,helper) {
        //helper.setTotalPrice(component, event);
        var changeQuantity = event.getSource();
        var val =  parseFloat(changeQuantity.get("v.value"));
        
        if(!val){
            val = 0;   
        }

        console.log('val' + val);
        console.log('totalNonClubbedValue' + component.get('v.totalNonClubbedValue'))
        var cardDetail = changeQuantity.get("v.name");
        var selectedOption = component.get("v.selectedCardList"); 

        for(var i in selectedOption){

            if(val > component.get('v.totalNonClubbedValue')){
                component.set("v.errorMsg","you can't add more amounnt than total value");
                component.set("v.isError",true);
                selectedOption[i]['nonClubamount']=component.get('v.totalNonClubbedValue');
                //break;
            }else{
                component.set("v.isError",false);
            }

            if(selectedOption[i]['card'] == cardDetail){
                console.log('selectedOption[i]nonClubamount'+ i + '-' + val);
                if(selectedOption.length==2){
                    var newAmount = component.get('v.totalNonClubbedValue') - val;
                    if(newAmount<0){
                        component.set("v.errorMsg","you can't add more nonClubamount than total value");
                        component.set("v.isError",true);
                        selectedOption[i]['nonClubamount'] = component.get('v.totalNonClubbedValue');
                        if(i==0){
                            selectedOption[1].nonClubamount = 0;
                        }
                        if(i==1){
                            selectedOption[0].nonClubamount = 0;
                        }
                        break;
                    }else{
                        selectedOption[i]['nonClubamount']=val;
                        if(i==0){
                            selectedOption[1].nonClubamount = newAmount;
                            console.log('selectedOption[1].nonClubamount' + selectedOption[1].nonClubamount);
                        }
                        if(i==1){
                            selectedOption[0].nonClubamount = newAmount;
                            console.log('selectedOption[0].nonClubamount' + selectedOption[0].nonClubamount);
                        }
                    }
                }else{
                    selectedOption[0].nonClubamount = component.get('v.totalNonClubbedValue');
                }
            }
        }
        component.set('v.selectedCardList',selectedOption);
    },

    splitPaymentCheckedForClub: function (component, event,helper) {
        var productChecked = event.getSource().get("v.checked");
        var changeQuantity = event.getSource();
        var cardDetail = changeQuantity.get("v.name");
        var selectedOption = component.get("v.selectedCardList"); 
        for(var i in selectedOption){
            if(selectedOption[i]['card'] == cardDetail){
                if(selectedOption.length==2){
                    if(i==0){
                        if(productChecked==selectedOption[1].usedForClub){
                            selectedOption[1].usedForClub = !productChecked;
                        }
                    }
                    if(i==1){
                        if(productChecked==selectedOption[0].usedForClub){
                            selectedOption[0].usedForClub = !productChecked;
                        }
                    }
                }else{
                    selectedOption[0].usedForClub = true;
                }
                component.set('v.selectedCardList',selectedOption);
            }
        }
        helper.setTotalPrice(component, event);
    },
    onSelectCard : function(component, event, helper) {
        console.log('on select card called');
        var selected = component.find("cardlevels").get("v.value");
        if(selected){
            var selectedCardList2 = [];
            var mapOfCardValue = {};
            var selectedCardList = component.get("v.selectedCardList");
            console.log('selectedCardList before',selectedCardList);
            var selectedArray = selected.split(';');
            for(var selectedCard in selectedArray){
                console.log(JSON.stringify(selectedCardList).indexOf(selectedArray[selectedCard]));
                if(JSON.stringify(selectedCardList).indexOf(selectedArray[selectedCard])<0){
                    console.log('inside');
                    console.log('selectedCardList inside'+JSON.stringify(selectedCardList));
                    if(selectedCardList && selectedCardList.length==0){
                        selectedCardList.push({'card':selectedArray[selectedCard],'nonClubamount':component.get('v.totalNonClubbedValue'), 'usedForClub':true});
                    }else{
                        selectedCardList.push({'card':selectedArray[selectedCard],'nonClubamount':0, 'usedForClub':false});
                    }
                    
                }
            }
            
            console.log('selectedCardList after'+JSON.stringify(selectedCardList));
            
            if(selectedCardList && selectedCardList.length>2){
                selectedCardList.pop();
                
                var cardIds = [];
                for(var selectedCard in selectedCardList){
                    cardIds.push(selectedCardList[selectedCard].card);
                }   
                component.find("cardlevels").set("v.value",cardIds.join(';'));
                
                component.set("v.errorMsg",'You can not selecte more than 2 card');
                component.set("v.isError",true);
                return false;
            }else {
                var cardIds = [];
                for(var selectedCard in selectedArray){
                    cardIds.push(selectedArray[selectedCard]);
                }

                var selectedCardList2 = selectedCardList;
                for(var i in selectedCardList2){
                    var isremoved = true;
                    for(var selectedCard in selectedArray){
                        if((selectedCardList2[i]['card'] == selectedArray[selectedCard])){
                            isremoved = false;
                        }
                    }
                    
                    if(isremoved == true){
                        var index;
                        for(var j in selectedCardList){
                            if(selectedCardList[j]['card']==selectedCardList2[i]['card']){
                                index = j;
                            }
                        }
                        if(index){
                            selectedCardList.splice(index, 1);
                        }else{
                            selectedCardList.push({'card':selectedArray[selectedCard],'nonClubamount':0, 'usedForClub':false});
                        }
                    }
                }
                
                if(selectedCardList && selectedCardList.length==1){
                    selectedCardList[0].usedForClub=true;
                    selectedCardList[0].nonClubamount=component.get('v.totalNonClubbedValue');
                }

                component.set("v.selectedCardList",selectedCardList);
                component.find("cardlevels").set("v.value",cardIds.join(';'));
                component.set("v.errorMsg",'');
                component.set("v.isError",false);
            }
            /*if(selected.split(';').length>2) {
            	component.set("v.errorMsg",'You can not select more than 2 cards.');
                component.set("v.isError",true);
                console.log(selected.substring(0,selected.lastIndexOf(';')));
                component.find("cardlevels").set("v.value",selected.substring(0,selected.lastIndexOf(';')));
                return false;
            }else{
                component.set("v.errorMsg",'');
                component.set("v.isError",false);
            }*/
        }
        component.set("v.selectedCard", selected);
        console.log('Card selected value -->'+component.get("v.selectedCard"));
        var card = [];
        var selectedCardId='';
        card = component.get("v.card");
        for(var i=0;i<card.length;i++){
            if(card[i].Id == selected){
                var cardNumber = card[i].Credit_Card_Number__c;
                var displayCardNumber = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                component.set("v.displayCardNumber",displayCardNumber);
                selectedCardId = card[i].Id;
                break;
            }
        }
        console.log('-------------------Before Calling Update Card------------'+selectedCardId);
        var action = component.get('c.updateCardInStripe');
        action.setParams({
            cardId: selectedCardId
        });
        $A.enqueueAction(action);  
        console.log('-------------------After Calling Update Card------------'+selectedCardId);
        
    },
    addAddress : function(component, event, helper) {
        console.log('in add address');
        var address =  component.get("v.addAddress");
        console.log("address---> ",address.Shipping_Street__c);
        var cId = component.get("v.contactId");
        var pA = component.get("v.isPrimaryAddress");
        console.log('add new Address --> ',address,cId);
        if(pA){
            if (confirm('Are you sure you want to save this as Primary Address?')) {
                helper.createAddress(component, event, address,cId,pA);
            }
        }else{
            helper.createAddress(component, event, address,cId,pA);
        }
    },
    getIndex: function (component, event) {
        var selectedItem = event.currentTarget; // Get the target object
        var index = selectedItem.dataset.record; 
        console.log('index value if recorded',index);
        component.set("v.indexVar",index);
    },
    handleProductChange: function (component, event,helper) {
        console.log('handle producct change');
        component.set("v.errorMsg",'');
        component.set("v.isError",false);
        var checkedProductList = component.get("v.checkedProductList");
        var productChecked = event.getSource().get("v.checked");
        console.log('productCHeckddd',productChecked);
        console.log('checkedProductList',checkedProductList.length);
        if(checkedProductList.length==5 && productChecked){
            event.getSource().set("v.checked",false);
            component.set("v.errorMsg",'You can not add more than 5 products.');
            component.set("v.isError",true);
            return false;
        }
        
        var changeValue = event.getSource();
        console.log(changeValue);
        var name = changeValue.get("v.name");
        var index = component.get("v.indexVar")// Get its value i.e. the index
        console.log('index in product change',index);
        console.log('product checkbox value',event.getSource().get("v.checked"));
        
        if(productChecked && checkedProductList.length<5)
            checkedProductList.push(index);
        if(!productChecked && checkedProductList.length>0)
            checkedProductList.pop();
        
        console.log('checkedProductList',checkedProductList);
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index]; // Use it retrieve the store record 
        selectedOption.checkValue = changeValue.get("v.checked");
        
        if(productChecked){
            /*we need to remove other select option based on configuration*/
            var productIdIndexMap = {};
            console.log('product list');
            console.log(productList);
            if(productList){
                for(var i in productList){
                    console.log(productList[i]);
                    productIdIndexMap[productList[i].Id+'']=i;
                }
            }
            var toBeHideProductIds = selectedOption.productsToHide;
            if(toBeHideProductIds && toBeHideProductIds.length>0){
                for(var i=0;i<toBeHideProductIds.length;i++){
                    if(productIdIndexMap[toBeHideProductIds[i]]){
                        console.log('index of to be hide item' + productIdIndexMap[toBeHideProductIds[i]]);
                        var hidedOption = component.get("v.productOptions")[productIdIndexMap[toBeHideProductIds[i]]]; // Use it retrieve the store record 
                        hidedOption.isDisabled = true;
                        productList[productIdIndexMap[toBeHideProductIds[i]]] = hidedOption;
                    }
                }
            }
        }else{
            /*we need to remove other select option based on configuration*/
            var productIdIndexMap = {};
            console.log('product list');
            console.log(productList);
            if(productList){
                for(var i in productList){
                    console.log(productList[i]);
                    productIdIndexMap[productList[i].Id+'']=i;
                }
            }
            var toBeHideProductIds = selectedOption.productsToHide;
            if(toBeHideProductIds && toBeHideProductIds.length>0){
                for(var i=0;i<toBeHideProductIds.length;i++){
                    if(productIdIndexMap[toBeHideProductIds[i]]){
                        console.log('index of to be not hide item' + productIdIndexMap[toBeHideProductIds[i]]);
                        var hidedOption = component.get("v.productOptions")[productIdIndexMap[toBeHideProductIds[i]]]; // Use it retrieve the store record 
                        hidedOption.isDisabled = false;
                        productList[productIdIndexMap[toBeHideProductIds[i]]] = hidedOption;
                    }
                }
            }
            selectedOption.quantity = 0;
            selectedOption.months = 1;
            if(selectedOption.club){
                selectedOption.checkedForClub = false;
                helper.handleClub(component, event,helper);
            }
        }
        component.set("v.productOptions",productList);
        console.log('checkbox clicked value ',selectedOption.checkValue);
        helper.setTotalPrice(component, event);

        var selectedCardList = component.get("v.selectedCardList"); 
        if(selectedCardList && selectedCardList.length==1){
            selectedCardList[0].usedForClub=true;
            selectedCardList[0].nonClubamount=component.get('v.totalNonClubbedValue');
        }
        component.set("v.selectedCardList",selectedCardList);
    },
    addQuantity: function (component, event,helper) {
        
        var changeQuantity = event.getSource();
        var val = changeQuantity.get("v.value");
        var index = component.get("v.indexVar")
        console.log(val,index);
        var selectedOption = component.get("v.productOptions")[index]; 
        if(selectedOption.quanititySelector == true){
            selectedOption.quantity = val;        
            selectedOption.quantityPrice = selectedOption.price * selectedOption.quantity;
            console.log('Quantity changed values ',selectedOption.price , selectedOption.quantity);
            console.log('Quantity changed ',selectedOption);
            
            helper.setTotalPrice(component, event);
        }
        
    },
    addMonth: function (component, event,helper) {
        
        var changeMonth = event.getSource();
        var val = changeMonth.get("v.label");
        var index = component.get("v.indexVar")
        
        index = event.getSource().get('v.title');
        component.set("v.indexVar",index);
        
        console.log(val,index);
        
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index]; 
        selectedOption.months = val;
        
        console.log('Month changed values ',selectedOption.months );
        
        productList[index] = selectedOption;
        console.log('selectedOption changed ',selectedOption);
        component.set("v.productOptions",productList);
        if(val){
            // logic for replacing product based on val
            selectedOption.checkedForClub = false;    
            helper.handleClub(component,event,helper);
        }
        helper.setTotalPrice(component, event);
    },
    
    handleClub: function (component, event,helper) {
        var index = component.get("v.indexVar")
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index];
        selectedOption.months = 1;
        
        var isChecked = event.getSource().get("v.checked");
        if(isChecked && !(selectedOption.Name.indexOf("CLUB") >= 0)){
            selectedOption.checkedForClub = true;
            selectedOption.months = 0;
            productList[index] = selectedOption;
            component.set("v.productOptions",productList);
            helper.handleClub(component, event,helper);
            helper.setTotalPrice(component, event);
        }
        
        if(!isChecked && selectedOption.Name.indexOf("CLUB") >= 0){
            selectedOption.checkedForClub = false;
            selectedOption.months = 1;
            productList[index] = selectedOption;
            component.set("v.productOptions",productList);
            helper.handleClub(component, event,helper);
            helper.setTotalPrice(component, event);
        }
    },
    updateCard: function(component, event, helper) {
        console.log('in add billing address');
        console.log('new card details',component.get("v.newCardDetails"));
        var cId = component.get("v.contactId");
        var card = component.get("v.newCardDetails");
        console.log('card details in update card',card);
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var hasError = false;
        if(!card.Name_On_Card__c || !card.Credit_Card_Number__c || !card.Expiry_Month__c && 
           !card.Expiry_Year__c || !card.Card_Type__c || !card.Cvc__c){
            console.log('fields are missing');
            component.set("v.cardErrorMsg","Please fill all the required fields before adding the card.");
            component.set("v.isCardError",true);
            hasError = true;
        }
        if(card.Credit_Card_Number__c && card.Card_Type__c){
            if(card.Card_Type__c=="tok_amex"){
                if(card.Credit_Card_Number__c.length!=15){
                    console.log('amex card length not 15');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 15 digits for American Express.");
                    component.set("v.isCardError",true);
                    hasError = true;
                }
                if(card.Cvc__c.length!=4 && !hasError){
                    console.log('amex card CVC not 4');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 4 digits for American Express.");
                    component.set("v.isCardError",true);
                    hasError = true;    
                }
            }else if(card.Card_Type__c=="tok_diners"){
                if(card.Credit_Card_Number__c.length!=14){
                    console.log('dinners card length not 14');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 14 digits for Dinners Club.");
                    component.set("v.isCardError",true);
                    hasError = true;
                } 
                if(card.Cvc__c.length!=3 && !hasError){
                    console.log(card.Card_Type__c.length);
                    
                    console.log('Dinner,s club  card CVC  not 3');
                    component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 3 digits for Card Type Selected.");
                    component.set("v.isCardError",true);
                    hasError = true;    
                }
            }
                else{
                    if (helper.valid_credit_card(card.Credit_Card_Number__c) == false) {
                        console.log('other card length errorMsg');
                        component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 16 digits.");
                        component.set("v.isCardError",true);
                        hasError = true;
                    }
                    /*if(card.Credit_Card_Number__c.length!=16){
                        console.log('other card length errorMsg');
                        component.set("v.cardErrorMsg","Please ensure to fill a valid credit card number of 16 digits.");
                        component.set("v.isCardError",true);
                        hasError = true;
                    }*/
                    if(card.Cvc__c.length!=3 && !hasError){
                        console.log('All Other  card CVC  not 3');
                        component.set("v.cardErrorMsg","Please ensure to fill a valid CVC of 3 digits for card Type Selected.");
                        component.set("v.isCardError",true);
                        hasError = true;    
                    }
                }
        }
        
        if(!hasError){
            console.log('call new card with billing');
            //var spinner = component.find("mySpinner");
            //$A.util.toggleClass(spinner, "slds-show");
            helper.newCardWithBilling(component,event,card,cId);
            
            window.setTimeout(
                $A.getCallback(function() {
                    helper.refreshCardList(component,event,cId);
                    
                }), 5000
            );
            //var spinner = component.find("mySpinner");
            //$A.util.toggleClass(spinner, "slds-hide");
            
            
        }
        
    },
    
    setScheduleDate :  function(component, event) {
        console.log(component.get("v.scheduledDate"));  
    },
    
    /*showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },*/
    
    verifyDetails: function(component, event, helper) {
        
        component.set("v.isError",false);
        //component.get("v.isCardError",false);
        component.set("v.erroMsg",'');
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        
        var Salesperson = component.get("v.sendSalesperson");
        
        var payment = component.get("v.selectedPay"); 
        var isNewCard = component.get("v.newCard");
        var sd = component.get("v.scheduledDate");
        var isPrimaryAddress =  component.get("v.contact.address.Shipping_City__c");
        console.log('------------isPrimaryAddress----------'+isPrimaryAddress);
        if(isPrimaryAddress == undefined){
            component.set("v.errorMsg",'Please add Primary shipping address');
            component.set("v.isError",true);    
            
        }
        
        console.log('isNewCard',isNewCard);
        console.log('selected pay method',payment);
        if(payment == undefined){
            component.set("v.errorMsg",'Select Payment');
            component.set("v.isError",true);
        }
        console.log('------------payment----------'+payment);
        if(payment=='true'){
            console.log('Inside scheduled payment');
            console.log('selected date ',sd);
            if(!sd){
                component.set("v.errorMsg",'Enter Date to Schedule Payment');
                component.set("v.isError",true);
            }
        }
        
        var selectedOption = component.get("v.selectedCardList"); 
        var isAnyChecked = false;
        for(var i in selectedOption){
            
            console.log(selectedOption[i]['usedForClub']);
            if(selectedOption[i]['usedForClub'] == true){
                isAnyChecked= true;
            }
        }
        if(isAnyChecked==false){
            component.set("v.errorMsg",'please select payment for club items');
            component.set("v.isError",true);
        }
        
        var selectedProd = [];

        var prodtypes = [];
        var prodItems = component.get("v.productOptions");
        console.log('prodItem',prodItems);
        for(var i = 0; i < prodItems.length; i++){
            console.log('prodItems',prodItems[i]);
            if(prodItems[i].checkValue == true && prodItems[i].quantity == 0 && prodItems[i].quanititySelector){
                component.set("v.errorMsg",'Enter Quantity for Selected Products');
                component.set("v.isError",true);
                break;
            }
            if(prodItems[i].checkValue == true ){
                console.log(prodItems[i].checkValue);
                selectedProd.push(prodItems[i]);
                /*if(prodItems[i].productWithSite != null){
                    selectedProdSite.push(prodItems[i].productWithSite);
                }*/
            }            
        }
        console.log('sel items',selectedProd);
        var contact = component.get("v.contact");
        var card = component.get("v.selectedCard");
        
        var selectedCardList = component.get("v.selectedCardList"); 
        console.log('selectedCardList value',selectedCardList);
        if(!(selectedCardList.length > 0)){
            console.log('inside selectedCardList error');
            component.set("v.cardErrorMsg",'Please Select a Card from CardList');
            component.set("v.isCardError",true);
        }
        
        console.log('card value',card);
        if(card == '--- Select Card ---' && !isNewCard){
            console.log('inside card error');
            component.set("v.cardErrorMsg",'Please Select a Card');
            component.set("v.isCardError",true);
            document.documentElement.scrollTop = 0;
        }
        var totalPrice = component.get("v.totalPriceValue");
        if((!totalPrice > 0) || (totalPrice == 0) ){
            console.log('selectedProd',selectedProd.length);
            component.set("v.errorMsg",'Select products to Order');
            component.set("v.isError",true);
            document.documentElement.scrollTop = 0;
        }
        
        var totalPriceValue = component.get("v.totalPriceValue");
        var isCardError = component.get("v.isCardError");
        var isError = component.get("v.isError");
        


        var salesPerson = component.get("v.salesPerson.Name");
        console.log("salesPerson---",salesPerson);
        if(!salesPerson){
            console.log('salesPerson',salesPerson);
            component.set("v.salesPerson",null);
        }
 
        console.log('out',totalPrice+payment+isCardError+isError+isPrimaryAddress);
        if(totalPrice > 0 && payment != undefined && !isCardError && !isError && isPrimaryAddress != undefined ){
            helper.createOrder(component, event,selectedProd,payment,sd,selectedCardList,contact,totalPriceValue,salesPerson);
        }
    },
    
    onSelectPickList : function(component,event,helper){
        var auraId = event.getSource().getLocalId();
        var pickListValue = component.find(auraId).get("v.value");
        console.log('auraId',auraId);
        console.log('picklistValue',pickListValue);
        
        if(auraId=='expMonths'){
            
            if(pickListValue=='--None--')
                component.set("v.newCardDetails.Expiry_Month__c",'')
                else
                    component.set("v.newCardDetails.Expiry_Month__c",pickListValue);
        }
        else if(auraId=='expYears'){
            
            if(pickListValue=='--None--')
                component.set("v.newCardDetails.Expiry_Year__c ",'')
                else
                    component.set("v.newCardDetails.Expiry_Year__c",pickListValue);
        }
            else if(auraId=='cardType'){
                if(pickListValue=='--None--')
                    component.set("v.newCardDetails.Card_Type__c",'');
                else
                    component.set("v.newCardDetails.Card_Type__c",pickListValue);
            }
        
        console.log('new card details months',component.get("v.newCardDetails.Expiry_Month__c"));
        console.log('new card details years',component.get("v.newCardDetails.Expiry_Year__c"));
        console.log('new card details card type',component.get("v.newCardDetails.Card_Type__c"));
    },
    
    onRefreshCard : function(component,event,helper){
        console.log('on click refresh');
        helper.refreshCardList(component,event,component.get("v.contactId"));
    },
      onCancel : function(component,event,helper){
        debugger
        component.find("newCardChkBox").set("v.checked" , false);
        var newCC = component.get("v.newCard");
        console.log("newCard---",newCC);
        component.set("v.newCard",!newCC);

    },
    correctCardData : function(component,event,helper){
        console.log('correct card data called');
        console.log('cardId',component.get("v.errorCardId"));
        console.log('cardDetail omonth',component.get("v.newCardDetails.Expiry_Month__c"));
        var cardDetails = component.get("v.newCardDetails");
        helper.updateCardWithBilling(component,event,cardDetails,component.get("v.errorCardId"));
        window.setTimeout(
            $A.getCallback(function() {
                helper.refreshCardList(component,event,component.get("v.contactId"));
            }), 5000
        );
    },
    cvcInput :function(component,event,helper){
        var val = event.getSource().get("v.value");
        debugger
        if(isNaN(val)){
            component.set("v.NumValidationError", true);
        }
    }
})