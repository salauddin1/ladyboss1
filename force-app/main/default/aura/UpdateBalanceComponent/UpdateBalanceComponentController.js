({
    doInit : function(component, event, helper) {
        var action = component.get("c.getContact");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue();  
                if(result.Available_Commission__c != null && result.Available_Commission__c != '' ){
                    component.set("v.currentBal",result.Available_Commission__c);
                }else{
                    component.set("v.currentBal","0");
                }
            }else if(state == "ERROR"){}
        });
        $A.enqueueAction(action);
    },
    creditDebitChange : function(component, event, helper) {
        if(component.get("v.creditDebit") == 'credit'){
            component.set("v.isCredit",true);
            component.set("v.newBal",0);
            component.set("v.updatedBal",0);
        }else{
            console.log('false');
            component.set("v.isCredit",false);
            component.set("v.newBal",0);
            component.set("v.updatedBal",0);
            
        }
    },
    updateBalance : function(component, event, helper) {
        if(Math.sign(component.get("v.newBal")) == -1){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "message": "Please Enter Positive value.",
                "type" : "Warning"
            });
            toastEvent.fire();
            return;
        }
        var cb = component.get("v.currentBal");
        if(component.get("v.creditDebit") == 'credit'){
            if(typeof Number(cb) == 'number'){
                var add = parseFloat(Number(component.get("v.currentBal")).toFixed(2));
                add+= parseFloat(Number(component.get("v.newBal")).toFixed(2));
                add = parseFloat(Number(add).toFixed(2));
                component.set("v.updatedBal",add);
            }
        }else{
            if(typeof Number(cb) == 'number'){
                var sub = parseFloat(Number(component.get("v.currentBal")).toFixed(2));
                sub-= parseFloat(Number(component.get("v.newBal")).toFixed(2));
                sub = parseFloat(Number(sub).toFixed(2));
                component.set("v.updatedBal",sub);
            }
        }
        if(isNaN(component.get("v.updatedBal"))){
            component.set("v.updatedBal",'');
        }
    },
    updateBalanceOnContact : function(component, event, helper) {
        if(!component.get("v.reason") || (component.get("v.updatedBal") != 0 && !component.get("v.updatedBal"))){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "message": "Please Enter The reason and Amount first.",
                "type" : "Warning"
            });
            toastEvent.fire();
        }
        if(component.get("v.reason") && (component.get("v.updatedBal")==0 || component.get("v.updatedBal"))){
            if(Number(component.get("v.newBal")) > 300){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning!",
                    "message": "You can not credit or debit more than $300.",
                    "type" : "Warning"
                });
                toastEvent.fire();
                return;
            }
            //if(Math.sign(component.get("v.updatedBal")) == -1 || component.get("v.updatedBal")==0 || component.get("v.newBal")==0){
            if(Math.sign(component.get("v.updatedBal")) < 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning!",
                    "message": "The Amount should not be Negative.",
                    "type" : "Warning"
                });
                toastEvent.fire();
                return null;
            }
            
            var rs = component.get("v.reason");
            var action = component.get("c.updateContact");
            action.setParams({"bal":component.get("v.updatedBal"),"reasonText":rs,"conId":component.get("v.recordId")});
            action.setCallback(this, function(responsetogetUser) {
                var state = responsetogetUser.getState();
                var result = responsetogetUser.getReturnValue();  
                component.set("v.Spinner",false);
                if (state === 'SUCCESS') {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Your Balance has been updated Successfully!",
                        "type" : "success"
                    });
                    toastEvent.fire(); 
                    component.find("overlayLib").notifyClose(); 
                }else if(state == "ERROR"){}
            });
            $A.enqueueAction(action);
        }
    },
})