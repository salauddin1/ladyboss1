({
    doInit : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var action = component.get("c.getShip");
        action.setParams({ contactId :contactId});
        action.setCallback(this, function(response) {
            debugger
            var res = response.getReturnValue();
            component.set("v.multipleLookups",res);
            console.log(component.get("v.multipleLookups"));
        });
        $A.enqueueAction(action);
    }
})