({
    
	cancelBtn : function(component, event, helper) { 
        // Close the action panel 
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire(); 
    },
    closeModal1:function(component,event){ 
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.find("addressform").set("v.value", " ");
        component.find("ProofDisclaimer").set("v.checked" ,false);
        component.set("v.description", "");
        component.set("v.drop", " ");
        var radioLen = component.find("radioId");
        if(radioLen != null){
            for (var i = 0; i < radioLen.length; i++) {
                if (radioLen[i].get("v.checked") == true ) {
                    radioLen[i].set('v.checked', false); 
                }
            }
        }

        var divCom = component.find("div1");
        $A.util.addClass(divCom, 'slds-hide');
        var closeB = component.find("close");
        $A.util.removeClass(closeB, 'slds-hide');
        $A.get('e.force:refreshView').fire();
        //component.find("radioId").set("v.checked" ,false);
        
        
    },
    clearValues: function (component, event, helper) {
        component.set("v.OppLineUnitAmt" ,null);
        component.set("v.OppUnitPrice" ,null);
        component.set("v.AmountVal" ,null);
        component.set("v.radioVal",null);
        component.set("v.radioId",null);        
        component.set("v.OppLineChargeId",null);
        component.set("v.checkRadioLine" ,null);
        component.set("v.OppLineNameVal",null);
    }
})