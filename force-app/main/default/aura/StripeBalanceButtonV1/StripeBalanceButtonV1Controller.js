({
    handleShowModal: function (component, evt, helper) {
        var modalBody;
        console.log('lib called');
        $A.createComponent("c:StripeBalanceComponentV1", {
                recordId: component.get("v.recordId")
            },
            function (content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: "Stripe Balance Console",
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "slds-modal_medium fullwidth",
                        closeCallback: function () {}
                    })
                }
            });
    },
})