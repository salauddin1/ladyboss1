({
	intMethod : function(component,event,helper,selectVal) {
		var action1 = component.get("c.getValues");
        var picklistVals = [];
       // var selectVal = component.get("v.comboVal");
        
        action1.setParams({ selectedVal :selectVal});
        action1.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            var a = response.getReturnValue();
            //var keys = Object.keys(a);
            if(a != null){
                for(var i = 0; i < a.length; i++){
                    var picklistVal = {
                        "label":a[i] ,
                        "value":a[i],
                    };
                    picklistVals.push(picklistVal);
                }
            }
            component.set("v.optionDual",picklistVals);
        });
        
        $A.enqueueAction(action1);
	}
})