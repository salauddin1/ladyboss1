({
    doInit: function (component, event,helper) {
        var caseId = component.get("v.recordId");
        
        var action = component.get("c.getCase");
        action.setParams({caseId :caseId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var values = response.getReturnValue();
            debugger
            if(values[0].Tags_Support__c != undefined){
                component.set("v.description",values[0].Other_Description__c);
                component.set("v.comboVal",values[0].Tags_Support__c);
                var multiList = values[0].LadyBoss_Tags__c;
                if(multiList != undefined){
                    if(multiList.includes(';')){
                        component.set("v.multiListVal",multiList.split(';'));
                    }
                    else{
                        component.set("v.multiListVal",multiList);   
                    }
                
                var comboVal = component.get("v.comboVal");
                if(multiList.includes('Other')){
                    var cmpBack = component.find('textId');
                    $A.util.addClass(cmpBack, 'slds-show');
                    $A.util.removeClass(cmpBack, 'slds-hide'); 
                    
                }
                }
                helper.intMethod(component,event,helper,comboVal);
            }
        });
        $A.enqueueAction(action); 
        
    },
    selectVal:function(component,event,helper){
        debugger
        var value = event.getSource().get("v.value");
        component.set("v.picVal",value);
        var action = component.get("c.getValues");
        var picklistVals = [];
        action.setParams({ selectedVal :value});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var a = response.getReturnValue();
            //var keys = Object.keys(a);
            for(var i = 0; i < a.length; i++){
                var picklistVal = {
                    "label":a[i] ,
                    "value":a[i],
                };
                picklistVals.push(picklistVal);
            }
            component.set("v.optionDual",picklistVals);
        });
        $A.enqueueAction(action); 
    },
    multiSelect:function(component,event,helper){
        var value = event.getSource().get("v.value");
        component.set("v.dualVal",value);
        var pickVal = component.get("v.picVal");
        if(value.includes('Other')){
            var cmpBack = component.find('textId');
            $A.util.addClass(cmpBack, 'slds-show');
            $A.util.removeClass(cmpBack, 'slds-hide'); 
        }
        else{
            var cmpBack = component.find('textId');
            $A.util.addClass(cmpBack, 'slds-hide');
            $A.util.removeClass(cmpBack, 'slds-show'); 
            
        }
    },
    description:function(component,event,helper){
        var value = event.getSource().get("v.value");
        component.set("v.description",value);
    },
    save :function(component,event,helper){
        debugger
        var desVal = component.get("v.description");
        var caseId = component.get("v.recordId");
        var pickVal = component.get("v.picVal");
        var dualVal = component.get("v.dualVal");
        var val ='';
        for(var i = 0; i < dualVal.length; i++){
            val = dualVal[i] + ';'+val;
            
        }
        var multiVal = val.substr(0,val.length-1);
        var dualValString =  JSON.stringify(dualVal);
        var action = component.get("c.saveCase");
        
        action.setParams({ csId  :caseId,
                          tagVal:pickVal,
                          MultiPicVal:multiVal,
                          otherval:desVal});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            var a = response.getReturnValue();
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success ',
                message: "Saved Successfully",
                duration:' 1000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
            
        });
        $A.enqueueAction(action); 
    }
    
})