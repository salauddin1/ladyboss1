({
	doInit1 : function(component, event, helper) {
        var action = component.get("c.getContact");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                
                var result = responsetogetUser.getReturnValue();   
                console.log("result---",result.Email);
                var em = result.Email;
                console.log("em---",em);
                if(em === undefined){
                    console.log("true8888---");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": 'Invalid Email',
                        "type" : "Error"
                    });
                    toastEvent.fire(); 
                    component.find("overlayLib").notifyClose(); 
                }
                component.set("v.Username",result.Email);
                component.set("v.affiliateId",result.PAP_refid__c);
                console.log(result.PAP_refid__c!=null);
                if(result.PAP_refid__c!=null){
                    component.set("v.isAffiliateExist",true);
                }
                
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    },
    doInit2 : function(component, event, helper) {
        var action = component.get("c.checkRefId");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue(); 
                console.log('result:-'+result);
                if(component.get("v.affiliateId")==null){
                	component.set("v.affiliateId",result);
                }
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    }
})