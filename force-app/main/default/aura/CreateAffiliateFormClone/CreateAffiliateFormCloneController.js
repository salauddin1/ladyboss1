({
    doInit : function(component, event, helper) {
        debugger
        helper.doInit1(component);
        helper.doInit2(component);
    },
    verifyDetails : function(component, event, helper) {
        debugger
        component.set("v.Spinner",true);
        var action = component.get("c.createAffiliate");
        action.setParams({"conId":component.get("v.recordId"),"refId":component.get("v.affiliateId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                debugger
                component.set("v.Spinner",false);
                var result = responsetogetUser.getReturnValue();  
                var msgs = result;
                console.log("result---",result);
                //component.set("v.affiliateId",result.PAP_refid__c);
                
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": msgs,
                    "type" : "success"
                });
                toastEvent.fire(); 
                component.find("overlayLib").notifyClose(); 
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    },
    
    modifyDetails : function(component, event, helper) {
        debugger
        component.set("v.Spinner",true);
        var action = component.get("c.updateAffiliate");
        action.setParams({"conId":component.get("v.recordId"),"newrefId":component.get("v.affiliateId")});
        action.setCallback(this, function(responsetogetUser) {
            debugger
            var state = responsetogetUser.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                debugger
                component.set("v.Spinner",false);
                var result = responsetogetUser.getReturnValue();  
                var msgs = result;
                console.log("result---",result);
                //component.set("v.affiliateId",result.PAP_refid__c);
                
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": msgs,
                    "type" : "success"
                });
                toastEvent.fire(); 
                component.find("overlayLib").notifyClose(); 
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    }
    
})