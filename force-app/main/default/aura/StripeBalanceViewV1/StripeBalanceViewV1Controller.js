({
    doInit: function (component, event, helper) {
        var action = component.get("c.getConDetails");
        action.setParams({
            conID: component.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            var totalBal = 0.00;
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                for (var i = 0; i < result.length; i++) {
                    totalBal += result[i].balance;
                }
                component.set("v.totalBalance", totalBal.toFixed(2));
                component.set("v.Spinner",false);
            }

            console.log(response);
        }));
        $A.enqueueAction(action);
    },
})