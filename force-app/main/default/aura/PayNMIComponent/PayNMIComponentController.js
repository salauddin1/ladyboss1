({
   init : function(component, event, helper) {
      var action = component.get("c.payLightning");
      action.setParams({"oppId": component.get("v.recordId")});
      action.setCallback(this, function(response) {
         var state = response.getState();
         if(component.isValid() && state == "SUCCESS" && response.getReturnValue() == "Ok"){
            component.set("v.messageError", 'Payment Success.');
			component.set("v.messageErrorBoolean", false);
			$A.get('e.force:refreshView').fire();
         } else {
            component.set("v.messageError", response.getReturnValue());
            component.set("v.messageErrorBoolean", true);
         }
      });
      $A.enqueueAction(action);
   }
})