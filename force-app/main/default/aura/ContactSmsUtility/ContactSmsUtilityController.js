({
    doInit:function(component, event, helper){
        component.set("v.isError",false);
        component.set('v.showUpdate',false);
        var action2 = component.get("c.getTwilioNumbers");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            var res = response.getReturnValue();
            console.log("res: " + res);
            component.set('v.phoneList',response.getReturnValue());
        });
        $A.enqueueAction(action2);
        
        var action = component.get("c.getAllUser");
        action.setCallback(this, function(response) {
            
            var res = response.getReturnValue();
            component.set('v.userList',res);
        });
        $A.enqueueAction(action);
    },
    
    update : function(component, event, helper) {
        debugger
        component.set("v.isError",false);
        component.set("v.Spinner", true);
        var phone = component.get("v.UserData.Twillio_From_phone__c");
        var action = component.get("c.updateNum");
        action.setParams({ UserName :component.get('v.UserData.Name'),
                          Phone:component.get('v.UserData.Twillio_From_phone__c')});
        action.setCallback(this, function(response) {
            debugger 
            let state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                
                // $A.get('e.force:refreshView').fire();
                component.set('v.Spinner',true);
                component.set('v.showtable',true);
                component.set('v.showUpdate',false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The record has been updated successfully.",
                    "type": "success"
                });
                toastEvent.fire();
            }else if (state === "ERROR") {
                let errors = response.getError();
                let message = 'Unknown error';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                    component.set("v.errorMessage",message);
                    component.set("v.isError",true);
                    console.log(message);
                }
                // Process error returned by server
            }
        });
        var action2 = component.get("c.getAllUser");
        action2.setCallback(this, function(response) {
            
            component.set('v.Spinner',false);
            var res = response.getReturnValue();
            console.log(res);
            component.set('v.userList',res);
            
            
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
        component.set("v.Spinner", false);
    },
    
    Cancel : function(component, event, helper) {
        component.set('v.showUpdate',false);
    },
    
    radioClick : function(component, event, helper) {
        component.set('v.showUpdate',true);
        var value = event.getSource().get("v.value");
        var action = component.get("c.getSelectedUser");
        action.setParams({ Id :value });
        action.setCallback(this, function(response) {
            
            var res = response.getReturnValue();
            component.set('v.UserData',res);
        });
        $A.enqueueAction(action);
    }
})