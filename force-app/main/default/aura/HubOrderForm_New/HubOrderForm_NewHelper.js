({
    doInit: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAllProducts");
        var productOne = component.get("v.productOne");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                for (var i = 0; i < result.length; i++) {
                    if (result[i].Name.length > 26) {
                        result[i].Names = result[i].Name.substr(0, 26) + "...";
                    } else {
                        result[i].Names = result[i].Name;
                    }
                    for (var j = 0; j < productOne.length; j++) {
                        if (result[i].planId == productOne[j].planId) {
                            result[i].incart = true;
                        } else if (result[i].switchProduct) {
                            for (var k = 0; k < result[i].pwList.length; k++) {
                                if (result[i].pwList[k].planId == productOne[j].planId) {
                                    result[i].incart = true;
                                }
                            }
                        }
                    }
                }
                component.set("v.Allproducts", result);
                this.getCard(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    getAll: function (component, event, helper) {
        var action = component.get("c.getAllProducts");
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                for (var i = 0; i < result.length; i++) {
                    if (result[i].Name.length > 26) {
                        result[i].Names = result[i].Name.substr(0, 26) + "...";
                    } else {
                        result[i].Names = result[i].Name;
                    }
                }
                component.set("v.Allproducts", result);
            }
        });
        $A.enqueueAction(action);
    },
    personalizeMembership: function (component, event, helper) {
        var productOne = component.get("v.productOne");
        var opplineitems = component.get("v.OppLineItem");
        var len = [];
        var addedprods = [];
        var canceledprods = [];
        var modalInfoAdd = "";
        var modalInfoCancel = "";
        var showTotal = "";
        var todayTotal = 0.0;
        for (var i = 0; i < productOne.length; i++) {
            for (var j = 0; j < opplineitems.length; j++) {
                if (
                    productOne[i].planId == opplineitems[j].planId &&
                    !len.includes(opplineitems[j])
                ) {
                    len.push(opplineitems[j]);
                }
            }
        }
        if (len.length == productOne.length && len.length >= opplineitems.length) {
            component.set("v.selectedModal", "ModalboxNoChange");
            this.openmodal(component, event, helper);
            component.set("v.Spinner", false);
        } else {
            var action = component.get("c.checkPayment");
            action.setParams({
                prod: JSON.stringify(opplineitems),
                alreadySubscribed: JSON.stringify(productOne)
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == "SUCCESS") {
                    var result = response.getReturnValue();
                    
                    console.log("personalizeMembership " + result.length);
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].listStatus == "cancel") {
                            canceledprods.push(result[i]);
                        } else if (result[i].listStatus == "add") {
                            addedprods.push(result[i]);
                        }
                    }
                    if (addedprods.length > 0) {
                        component.set("v.selectedModal", "ModalboxValidate");
                        modalInfoAdd = "You have added this membership(s): ";
                        showTotal = "This order will charge immediately on card ending in ";
                        for (var i = 0; i < addedprods.length; i++) {
                            if (i != addedprods.length - 1) {
                                modalInfoAdd = modalInfoAdd + addedprods[i].Name + ", ";
                            } else {
                                modalInfoAdd = modalInfoAdd + addedprods[i].Name;
                            }
                            todayTotal += addedprods[i].totalPrice;
                        }
                        // showTotal = showTotal + todayTotal;
                    }
                    if (canceledprods.length > 0 && addedprods.length <= 0) {
                        component.set("v.selectedModal", "CancelModalboxValidate");
                        modalInfoCancel = "You have removed this Membership(s): ";
                        for (var i = 0; i < canceledprods.length; i++) {
                            if (i != canceledprods.length - 1) {
                                modalInfoCancel =
                                    modalInfoCancel + canceledprods[i].Name + ", ";
                            } else {
                                modalInfoCancel = modalInfoCancel + canceledprods[i].Name;
                            }
                        }
                    }
                    if (canceledprods.length > 0) {
                        modalInfoCancel = "You have removed this Membership(s): ";
                        for (var i = 0; i < canceledprods.length; i++) {
                            if (i != canceledprods.length - 1) {
                                modalInfoCancel =
                                    modalInfoCancel + canceledprods[i].Name + ", ";
                            } else {
                                modalInfoCancel = modalInfoCancel + canceledprods[i].Name;
                            }
                        }
                    }
                    if (showTotal != "") {
                        showTotal += component.get("v.cardLast4") + " and will ship to: ";
                        component.set("v.todaysTotal", showTotal);
                        //debugger;
                        var addLine1 = component.get("v.AddressValue");
                        var addLine = addLine1.split(",");
                        component.set("v.addressLine1", addLine[0]);
                        component.set("v.newAdd", addLine[1] + "," + addLine[2]);
                    }
                    component.set("v.messageAdd", modalInfoAdd);
                    component.set("v.messageCancel", modalInfoCancel);
                    component.set("v.addedProducts", addedprods);
                    component.set("v.canceledProducts", canceledprods);
                    this.openmodal(component, event, helper);
                }
                component.set("v.Spinner", false);
            });
            $A.enqueueAction(action);
        }
    },
    getCard: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getCard");
        action.setParams({
            conID: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log("get all", result);
                var cardnum = [];
                var cards = [];
                var namecon = '';
                for (var i = 0; i < result.cardList.length; i++) {
                    if (!cardnum.includes(result.cardList[i].Last4__c)) {
                        result.cardList[i].Credit_Card_Last4 =
                            "********" + result.cardList[i].Last4__c;
                        cards.push(result.cardList[i]);
                        cardnum.push(result.cardList[i].Last4__c);
                    }
                    namecon =result.cardList[i].Contact__r.Name;
                }
                if (result.cardList.length > 0) {
                    component.set("v.cardLast4", result.cardList[0].Last4__c);
                }
                //debugger;
                if (result.AddressValue != null) {
                    component.set("v.ShipStreet", result.AddressValue.Shipping_Street__c);
                    component.set("v.ShipCity", result.AddressValue.Shipping_City__c);
                    component.set("v.ShipState", result.AddressValue.Shipping_State_Province__c);
                    component.set("v.ShipZip", result.AddressValue.Shipping_Zip_Postal_Code__c);
                    component.set("v.displayAddress",result.Address);
                }
                console.log("get all", component.get("v.cardLast4")+ ' ' +namecon);
                component.set("v.cardList", cards);
                component.set("v.FullName", namecon);
            }
        });
        $A.enqueueAction(action);
    },
    openmodal: function (component, event, helper) {
        var selectedModal = component.get("v.selectedModal");
        console.log("openModal", selectedModal);
        var cmpTarget = component.find(selectedModal);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.addClass(cmpTarget, "slds-fade-in-open");
        $A.util.addClass(cmpBack, "slds-backdrop--open");
    },
    setCartTotal: function (component,event,helper){
        
        var cartProducts = component.get("v.CartProducts");
        var recordId = component.get("v.recordId");
        var action = component.get("c.getTax");
        action.setParams({
            conID: recordId,
            Product: JSON.stringify(cartProducts)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                var opplineitem = component.get("v.OppLineItem");
                var CartProducts = component.get("v.CartProducts");
                var carttotal = 0.00;
                var addedmemtotal = 0.00;
                for (var i = 0; i < CartProducts.length; i++) {
                    carttotal += CartProducts[i].totalPrice;
                }
                for (var i = 0; i < opplineitem.length; i++) {
                    addedmemtotal += opplineitem[i].totalPrice;
                }
                if (carttotal != 0) {
                    component.set("v.showtodaytotal", true);
                }else{
                    component.set("v.showtodaytotal", false);
                }
                addedmemtotal = parseFloat(addedmemtotal) + carttotal;
                component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
                component.set("v.totalToday", carttotal.toFixed(2));
                component.set("v.tax",result.Tax);
                component.set("v.taxPercentage",result.TaxPercentage);
                var taxplusamount  = carttotal + result.Tax;
                component.set("v.totalWithTax",taxplusamount);
            }
        });
        $A.enqueueAction(action);
    }
});