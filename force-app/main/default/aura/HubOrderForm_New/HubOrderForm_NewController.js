({
    scriptsLoaded: function (component, event, helper) {
        console.log("javaScript files loaded successful");
        
        $(".carousel").carousel();
        window.setTimeout(function () {
            $(".slick-images").slick({
                dots: false,
                autoplay:false,
                slidesToShow: 3,
                slidesToScroll: 3,
                nextArrow: '<i class="fa slide-arrow fa-arrow-right"></i>',
                prevArrow: '<i class="fa slide-arrow fa-arrow-left"></i>',
                arrows:true,
            });
        },1000);       
    },
    init: function (component, event, helper) {
        component.set("v.Spinner", false);
        var currentprod = [];
        console.log("in manage subscriptions");
        component.set("v.showCard", false);
        component.set("v.verifyDetails", false);
        component.set("v.prodidx", "0");
        component.set("v.showtodaytotal", false);
        component.set("v.totalToday", 0.0);
        window.addEventListener(
            "message",
            function (event) {
                if (event.origin != "https://ladyboss-support.secure.force.com") {
                    return;
                }
                component.set("v.refreshCard", true);
                component.set("v.newCard", false);
            },
            false
        );
        var recordId = component.get("v.recordId");
        var action = component.get("c.getProducts");
        action.setParams({
            conID: recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            window.scrollTo(0, 0);
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                var subtotal = 0.0;
                if (result.length == 0) {
                    component.set("v.isSelected", true);
                    component.set("v.isEmptyList", true);
                } else {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].Name.includes("BURN")) {
                            result[i].image = "BURN.png";
                        } else if (result[i].Name.includes("REST")) {
                            result[i].image = "REST.png";
                        } else if (result[i].Name.includes("RECOVER")) {
                            result[i].image = "RECOVER.png";
                        } else if (result[i].Name.includes("FUEL")) {
                            result[i].image = "FUEL.png";
                        } else if (result[i].Name.includes("LEAN 1")) {
                            result[i].image = "LEAN1.png";
                        } else if (result[i].Name.includes("LEAN 2")) {
                            result[i].image = "LEAN2.png";
                        } else if (result[i].Name.includes("LEAN 3")) {
                            result[i].image = "LEAN3.png";
                        } else if (result[i].Name.includes("GREENS-1")) {
                            result[i].image = "GREENS1.png";
                        } else if (result[i].Name.includes("GREENS-2")) {
                            result[i].image = "GREENS2.png";
                        } else if (result[i].Name.includes("GREENS-3")) {
                            result[i].image = "GREENS3.png";
                        }
                        
                        if (result[i].Name.length > 26) {
                            result[i].Names = result[i].Name.substr(0, 26) + "...";
                        } else {
                            result[i].Names = result[i].Name;
                        }
                        subtotal += result[i].totalPrice;
                        currentprod.push(result[i]);
                    }
                }
                component.set("v.OppLineItem", result);
                component.set("v.total", subtotal.toFixed(2));
                component.set("v.addedMemTotal", subtotal.toFixed(2));
                component.set("v.productOne", currentprod);
                helper.doInit(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    checkProds: function (component, event, helper) {
        var selected = component.get("v.OppLineItem");
        if (selected.length <= 0) {
            component.set("v.selectedModal", "CancelModalboxValidate");
            helper.openmodal(component, event, helper);
        } else {
            component.set("v.Spinner", true);
            helper.personalizeMembership(component, event, helper);
        }
    },
    cancelOpp: function (component, event, helper) {
        var a = component.get("c.closemodal");
        $A.enqueueAction(a);
        console.log("First step;- fire event");
        component.set("v.Spinner", true);
        var refreshevent = $A.get("e.c:RefreshSubscriptions");
        refreshevent.setParams({
            data: "CancelSubscriptionFinal"
        });
        refreshevent.fire();
    },
    removeRow: function (component, event, helper) {
        
        var idx = event.target.getAttribute("data-index");
        var nameid;
        var Elements = component.find('prodone');
        
        if(Elements.length != undefined){
            for (var i = 0; i < Elements.length; i++) {
                var val = Elements[i].getElement().getAttribute('data-index');
                console.log('inside',val + ' ' + idx);
                if(val == idx){
                    $A.util.addClass(Elements[i], "changeMe");
                }
            }
        }else{
            var val = Elements.getElement().getAttribute('data-index');
            console.log('inside',val + ' ' + idx);
            if(val == idx){
                $A.util.addClass(Elements, "changeMe");
            }
        }
        
        var canmem = component.get("v.removedMembership");
        var opplineitem = component.get("v.OppLineItem");
        var prods = component.get("v.Allproducts");
        var planid;
        var nextdate;
        for (var i = 0; i < opplineitem.length; i++) {
            if (idx == opplineitem[i].Id) {
                planid = opplineitem[i].planId;
                nextdate= opplineitem[i].nextChargeDate;
                canmem.push(opplineitem[i]);
                nameid = opplineitem[i].Names;
            }
        }
        for (var i = 0; i < prods.length; i++) {
            if (planid == prods[i].planId) {
                prods[i].incart = false;
            }else if (prods[i].switchProduct) {
                for (var j = 0; j < prods[i].pwList.length; j++) {
                    if (planid == prods[i].pwList[j].planId) {
                        prods[i].incart = false;
                    }
                }
            }
        }
        /*for (var i = 0; i < opplineitem.length; i++) {
            if (idx == opplineitem[i].Id) {
                opplineitem.splice(i, 1);
            }
        }*/
        component.set("v.removedMembership", canmem);
        component.set("v.OppLineItem", opplineitem);
        component.set("v.Allproducts", prods);
        var prodone = component.get("v.productOne");
        console.log("remove canclled", prodone);
    },
    closeModel: function (component, event, helper) {
        $A.util.addClass(component.find("toastModel"), "slds-hide");
    },
    finalizeMembership: function (component, event, helper) {
        component.set("v.Spinner", true);
        var cmpTarget = component.find(component.get("v.selectedModal"));
        var cmpBack = component.find("ModalbackdropCancel");
        $A.util.removeClass(cmpBack, "slds-backdrop--open");
        $A.util.removeClass(cmpTarget, "slds-fade-in-open");
        var recordId = component.get("v.recordId");
        var card = component.get("v.cardList")[component.get("v.cardidx")];
        console.log("finalizeMembership ", card.Stripe_Card_Id__c);
        var cancelprods = component.get("v.removedMembership");
        var addprods = component.get("v.CartProducts");
        var action = component.get("c.createPayment");
        action.setParams({
            canceledProds: JSON.stringify(cancelprods),
            addedProds: JSON.stringify(addprods),
            cardId: card.Stripe_Card_Id__c
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var msg =
                "Unknown Error While personalizing your Memberships. Try again Later or Contact Support.";
            var msgType = "error";
            console.log("finalizeMembership ", state);
            
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                console.log("finalizeMembership ", result);
                window.scrollTo(0, 0);
                if (result == "success") {
                    msg = "Successfully Personalized Your Memberships";
                    msgType = "success";
                } else {
                    msg = result;
                }
            }
            component.set("v.message", msg);
            component.set("v.messageType", msgType);
            $A.util.removeClass(component.find("toastModel"), "slds-hide");
            $A.util.addClass(component.find("toastModel"), "slds-show");
            let scrollObj = document.getElementById("scrollId");
            if (scrollObj) {
                console.log("height============" + scrollObj.textContent);
                //scrollObj.scrollIntoView(true)
                scrollObj.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                    inline: "start"
                });
            }
            console.log("First step;- fire event");
            
            if (result == "success") {
                
                component.set("v.Spinner", true);
                var refreshevent = $A.get("e.c:RefreshSubscriptions");
                refreshevent.setParams({
                    data: "ShowSubscriptions"
                });
                refreshevent.fire();
                var a = component.get("c.init");
                $A.enqueueAction(a);
                
                setTimeout(function () {
                    $A.util.addClass(component.find("toastModel"), "slds-hide");
                    component.set("v.message", "");
                    component.set("v.messageType", "");
                }, 5000);
            } else {
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    resetProds: function (component, event, helper) {
        component.set("v.Spinner", true);
        var a = component.get("c.init");
        $A.enqueueAction(a);
    },
    onSelectCard: function (component, event, helper) {
        console.log("onSelectCard ", component.get("v.cardidx"));
        var card = component.get("v.cardList")[component.get("v.cardidx")];
        component.set("v.cardLast4", card.Last4__c);
    },
    closemodal: function (component, event, helper) {
        component.set("v.selectedServings",false);
        var selectedModal = component.get("v.selectedModal");
        console.log("openModal", selectedModal);
        var cmpTarget = component.find(selectedModal);
        var cmpBack = component.find("Modalbackdrop");
        $A.util.removeClass(cmpBack, "slds-backdrop--open");
        $A.util.removeClass(cmpTarget, "slds-fade-in-open");
    },
    splitProd: function (component, event, helper) {
        component.set("v.customProductSet", false);
        var names = ["burn", "lean", "fuel", "rest", "recover"];
        var prodName = event.getSource().get("v.name");
        var prod = component.find(prodName);
        var totalProds = 0;
        for (var i = 0; i < names.length; i++) {
            var prod1 = component.find(names[i]);
            totalProds += parseFloat(prod1.get("v.value"));
        }
        if (totalProds > 5) {
            component.set("v.errorProd", true);
            component.set("v.errorMsg", "Only Five Products Can be Choosen");
            var int = totalProds - prod.get("v.value");
            prod.set("v.value", 5 - int);
            component.set("v.customProductSet", true);
        } else {
            component.set("v.errorProd", false);
        }
        
        console.log("splitProd", totalProds);
        if (totalProds == 5) {
            component.set("v.customProductSet", true);
        }
    },
    goToMembership: function (component, event, helper) {
        console.log("First step;- fire event");
        component.set("v.Spinner", true);
        var refreshevent = $A.get("e.c:RefreshSubscriptions");
        refreshevent.setParams({
            data: "SubscriptionsPage"
        });
        refreshevent.fire();
    },
    getCards: function (component, event, helper) {
        helper.getCard(component, event, helper);
        component.set("v.refreshCard", false);
    },
    handleClick: function (component, event, helper) {
        var idx = event.target.getAttribute("data-index");
        var alpro = component.get("v.Allproducts");
        var switchproduct = false;
        for (var i = 0; i < alpro.length; i++) {
            console.log('insidecart', idx + ' ' + alpro[i].incart);
            if (idx == alpro[i].Id) {
                component.set("v.productName", alpro[i].Name);
                component.set("v.productPrice", alpro[i].totalPrice);
                component.set("v.productDescription", alpro[i].description);
                component.set("v.imageurl", '/resource/LadybossHubProductImage/' + alpro[i].image);
                component.set("v.currentProduct", alpro[i]);
                if (alpro[i].incart != undefined && alpro[i].incart == true) {
                    component.set("v.alreadyInCart", true);
                } else {
                    component.set("v.alreadyInCart", false);
                }
                if(alpro[i].switchProduct == true){
                    switchproduct = true; 
                }
                break;
            }
        }
        if(switchproduct == true){
            component.set("v.selectedModal", "ProductDetailsForLeanAndGreen");
        }else{
            component.set("v.selectedModal", "ProductDetails");
        }
        helper.openmodal(component, event, helper);
    },
    addToCart: function (component, event, helper) {
        var cart = component.get("v.CartProducts");
        var selectprod = component.get("v.currentProduct");
        var prods = component.get("v.Allproducts");
        var idx = selectprod.Id;
        var cartnumber = component.get("v.addedCartProds");
        var prodOne = component.get("v.productOne");
        var opplineItem = component.get("v.OppLineItem");
        var alreadyHad = false;
        var planId;
        var numtoremove;
        var canmem = component.get("v.removedMembership");
        
        for (var i = 0; i < prods.length; i++) {
            console.log("insidecart", idx + " " + prods[i].Id);
            if (idx == prods[i].Id) {
                prods[i].incart = true;
                planId = prods[i].planId;
                component.set("v.alreadyInCart", true);
                cartnumber++;
                break;
            }
            
        }
        for (var i = 0; i < prodOne.length; i++) {
            if (planId == prodOne[i].planId) {
                var idaura = prodOne[i].Id;
                alreadyHad = true;   
                var Elements = component.find('prodone');
                
                console.log('inside', idx +' '+Elements.getElement().getAttribute('data-index'));
                if(Elements.length != undefined){
                    for (var i = 0; i < Elements.length; i++) {
                        var val = Elements[i].getElement().getAttribute('data-index');
                        console.log('inside',val + ' ' + idaura);
                        if(val == idaura){
                            $A.util.removeClass(Elements[i], "changeMe");
                        }
                    }
                }else{
                    var val = Elements.getElement().getAttribute('data-index');
                    console.log('inside',val + ' ' + idaura);
                    if(val == idaura){
                        $A.util.removeClass(Elements, "changeMe");
                    }
                }
                cartnumber--;
                break;
            }
            
        }
        
        if(alreadyHad == false){
            cart.push(selectprod);
            component.set("v.CartProducts", cart);            
        }else{
            for (var i = 0; i < canmem.length; i++) {
                if(planId == canmem[i].planId){
                    numtoremove = i;
                    break;
                }
            }
            if (numtoremove != undefined) {
                var k = canmem.splice(numtoremove,1);
            }
        }
        component.set("v.Allproducts", prods);
        component.set("v.addedCartProds", cartnumber);
        component.set("v.removedMembership",canmem);
    },
    openModalCart: function (component, event, helper) {
        var canmem = component.get("v.removedMembership");
        var opplineitem = component.get("v.OppLineItem");
        var CartProducts = component.get("v.CartProducts");
        component.set("v.showCancel",canmem.length);
        component.set("v.showAdded", CartProducts.length);
        component.set("v.showCurrent", opplineitem.length);
        if(opplineitem.length == canmem.length && CartProducts.length == 0 && canmem.length != 0){
            component.set("v.selectedModal", "ModalboxCancel");
            helper.openmodal(component, event, helper);
        }else if(CartProducts.length == 0 && canmem.length == 0){
            component.set("v.selectedModal", "ModalboxNoChange");
            helper.openmodal(component, event, helper);
        }else {
            helper.setCartTotal(component,event,helper);
            component.set("v.selectedModal", "MyCart");
            helper.openmodal(component, event, helper);
        }
    },
    removeProdCart: function (component, event, helper) {
        var numtoremove ;
        var idx = event.target.getAttribute("data-index");
        var cartnumber = component.get("v.addedCartProds");
        var cart = component.get("v.CartProducts");
        var prods = component.get("v.Allproducts");
        for (var i = 0; i < prods.length; i++) {
            if(idx == prods[i].Id){
                prods[i].incart = false;
                cartnumber--;
                break;
            }
        }
        for (var i = 0; i < cart.length; i++) {
            if(idx == cart[i].Id){
                numtoremove = i;
                break;
            }
        }
        if (numtoremove != undefined) {
            var k = cart.splice(numtoremove,1);
        }
        console.log(k);
        component.set("v.addedCartProds",cartnumber);
        component.set("v.Allproducts", prods);
        component.set("v.CartProducts", cart);
        component.set("v.showAdded", cart.length);
        helper.setCartTotal(component,event,helper);
    },
    removeCancelled: function (component, event, helper) {
        var idx = event.target.getAttribute("data-index");
        var numtoremove;
        var Elements = component.find('prodone');
        
        console.log('inside', idx +' '+Elements.getElement().getAttribute('data-index'));
        if(Elements.length != undefined){
            for (var i = 0; i < Elements.length; i++) {
                var val = Elements[i].getElement().getAttribute('data-index');
                console.log('inside',val + ' ' + idx);
                if(val == idx){
                    $A.util.removeClass(Elements[i], "changeMe");
                }
            }
        }else{
            var val = Elements.getElement().getAttribute('data-index');
            console.log('inside',val + ' ' + idx);
            if(val == idx){
                $A.util.removeClass(Elements, "changeMe");
            }
        }
        var canmem = component.get("v.removedMembership");
        var opplineitem = component.get("v.OppLineItem");
        var prodone = component.get("v.productOne");
        console.log('remove canclled',prodone+' '+idx);
        /*for (var i = 0; i < prodone.length; i++) {
            if(idx == prodone[i].Id){
                opplineitem.push(prodone[i]);
                break;
            }
        }*/
        for (var i = 0; i < canmem.length; i++) {
            if(idx == canmem[i].Id){
                numtoremove = i;
                break;
            }
        }
        if (numtoremove != undefined) {
            var k = canmem.splice(numtoremove,1);
        }
        console.log(k);
        component.set("v.OppLineItem", opplineitem);
        component.set("v.removedMembership", canmem);
        component.set("v.showCancel", canmem.length);
        component.set("v.showCurrent", opplineitem.length);
        
        helper.setCartTotal(component,event,helper);
    },
    selectLean: function (component, event, helper) {
        var idx = event.target.id;  
        var prods = component.get("v.Allproducts");
        var selectprod = component.get("v.currentProduct");
        var prodid = selectprod.Id;
        Loop1:
        for(var i = 0; i < prods.length; i++) {
            if(prodid == prods[i].Id){
                console.log('hello1',prods[i].pwList);
                for(var j = 0; j < prods[i].pwList.length; j++) {
                    console.log('hello2');
                    if(idx == prods[i].pwList[j].numOfBags){
                        component.set("v.productName", prods[i].pwList[j].Name);
                        component.set("v.productPrice", prods[i].pwList[j].totalPrice);
                        component.set("v.productDescription", prods[i].pwList[j].description);
                        component.set("v.imageurl", '/resource/LadybossHubProductImage/' + prods[i].pwList[j].image);
                        selectprod.Id = prods[i].pwList[j].Id;
                        selectprod.planId = prods[i].pwList[j].planId;
                        selectprod.Name = prods[i].pwList[j].Name;
                        selectprod.totalPrice = prods[i].pwList[j].totalPrice;
                        component.set("v.currentProduct",selectprod);
                        component.set("v.selectedServings",true);
                        break Loop1;
                    }
                } 
            }else{
                for(var j = 0; j < prods[i].pwList.length; j++) {
                    console.log('hello4');
                    if(prodid == prods[i].pwList[j].Id){
                        if(idx == prods[i].pwList[j].numOfBags){
                            component.set("v.productName", prods[i].pwList[j].Name);
                            component.set("v.productPrice", prods[i].pwList[j].totalPrice);
                            component.set("v.productDescription", prods[i].pwList[j].description);
                            component.set("v.imageurl", '/resource/LadybossHubProductImage/' + prods[i].pwList[j].image);
                            selectprod.Id = prods[i].pwList[j].Id;
                            selectprod.planId = prods[i].pwList[j].planId;
                            selectprod.Name = prods[i].pwList[j].Name;
                            selectprod.totalPrice = prods[i].pwList[j].totalPrice;
                            component.set("v.currentProduct",selectprod);
                            component.set("v.selectedServings",true);
                            break Loop1;
                        }
                    }
                } 
            }
        }
    }
});