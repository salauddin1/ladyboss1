({
    updatePlan : function(component, event, helper) {
        console.log("reocrdId ", component.get("v.recordId"));
        component.set("v.Spinner", true);
        var action = component.get("c.updateSubPlan");
        action.setParams({ opplId: component.get("v.recordId") });
        action.setCallback(this, function(response) {
            console.log('state',response.getState());
            var state = response.getState();
            var result = response.getReturnValue();
            if(component.isValid() && state == "SUCCESS" && result != null){
                component.set("v.Spinner", false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                  title: "Success!",
                  message: result,
                  type: "success"
                });
                toastEvent.fire();
            }else{
                component.set("v.Spinner", false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                  title: "Error!",
                  message: "Error Processing request",
                  type: "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }

})