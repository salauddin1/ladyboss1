({
    doInit : function(component, event, helper){
        debugger
        // var vfOrigin = "https://" + component.get("v.vfHost1");
        window.addEventListener("message", function(event) {
            debugger
            console.log("========hellloooo========"+event.data);
            if(event.data == true){
               location.reload();
            }
        }, false);
        var SelectcardList =[];
        component.set('v.SelectcardList',SelectcardList);
        component.set("v.showcomp",true);
        component.set("v.hidehref",true);
        component.set("v.isShippedAddress",true);
        component.set("v.showhref",false);
        component.set("v.showbtn",true);  
        
        var recordId = component.get("v.recordId");
        component.set("v.conIdvale",recordId);
        var action   = component.get("c.getcontact");
        action.setParams({ conId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                
                var res = response.getReturnValue();
                console.log(res);
                // component.set("v.ContactWrapper",res);
                component.set("v.messageflag", false);
                if(res != null){
                    var phone = res.contactList.Phone;
                    var newphoneno = helper.formatPhoneNumber(component,phone);   
                    res.contactList.Phone = newphoneno ;
                    component.set("v.ContactWrapper", res);    
                }
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getAddress");
        action1.setParams({ conId : recordId});
        
        action1.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                
                var res = response.getReturnValue();
                console.log(res);
                // component.set("v.AddressWrapper",res);
                component.set("v.messageflag", false);
                if(res != null){
                    component.set("v.AddressWrapper", res);    
                }
            }
        });
        $A.enqueueAction(action1);
        
    },
    // common reusable function for toggle sections
    toggleSection : function(component, event, helper) {
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    openModel: function(component, event, helper) {
        debugger
        // for Display Model,set the "isOpen" attribute to "true"
        var recordId = component.get("v.recordId");
        var action3 = component.get("c.getAllCards");
        component.set("v.SelectcardList",[]);
        var cardList = component.set("v.SelectcardList");
        action3.setParams({ conId : recordId});
        
        action3.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.SelectcardList", res);
                console.log(res);
                // }
            }
        });
        $A.enqueueAction(action3);
        component.set("v.OpenSubscription",true);
    },
    
    onSelectCard :function(component, event, helper) {
        debugger
        component.set("v.isMultipleSub", false);
        component.set("v.isSingleSub", false);
        var cardId = event.getSource().get('v.value');
        //component.set("v.cardCheck",cardId);
        var recordId = component.get("v.recordId");
        
        var cardlst = component.get("v.SelectcardList");
        for(var i = 0 ; i< cardlst.length ; i++){
            if(cardlst[i].cardList[0].Last4__c == cardId){
                if(cardlst[i].cardList.length > 1){
                    component.set("v.isMultipleSub", true);
                    component.set("v.MultipleSubscriptionList",cardlst[i].cardList);
                }	
                else{
                    component.set("v.cardCheck",cardlst[i].cardList[0].Id);
                    component.set("v.isSingleSub", true);
                    var action2 = component.get("c.getCardSubscription");
                    action2.setParams({ cardId : cardlst[i].cardList[0].Id,
                                       conId : recordId});
                    action2.setCallback(this, function(response) {
                        debugger
                        var selectedContacts = [];
                        var state = response.getState();
                        if(state == "SUCCESS") {
                            var res = response.getReturnValue();
                            console.log(res);
                            component.set("v.SubscriptionList", res[0].subscriptionList);
                            console.log(res);
                        }
                    });
                    $A.enqueueAction(action2);
                }
                cardlst[i].SubscriptionOnCard = true;
            }
            else{
                cardlst[i].SubscriptionOnCard = false;
            }
        }
        component.set("v.OpenSubscription", false);
        component.set("v.OpenSubscription", true);
        component.set("v.spinner", false);
    },
    numberValidation : function(component,event,helper) {
       debugger
       console.log('--------number validation----------');
        var phoneNo = component.get("v.AddressWrapper.AddressList.Shipping_Zip_Postal_Code__c");
        console.log(phoneNo.length);
        console.log(phoneNo.charCodeAt(phoneNo.length-1));
        if(phoneNo.charCodeAt(phoneNo.length-1) >= 48 && phoneNo.charCodeAt(phoneNo.length-1)<=57){
            var val = phoneNo.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            component.set('v.value',val.trim());
        }
        else{
            component.set('v.AddressWrapper.AddressList.Shipping_Zip_Postal_Code__c',phoneNo.replace(phoneNo.substr(phoneNo.length-1),""));
        }
    },
    onMultipleSelectCard :function(component, event, helper) {
        debugger
        var cardId = event.getSource().get('v.value');
        component.set("v.SelectedCard",cardId);
        component.set("v.cardCheck",cardId);
        var recordId = component.get("v.recordId");
        var action2 = component.get("c.getCardSubscription");
        action2.setParams({ cardId : cardId,
                           conId : recordId});
        action2.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.SubscriptionList", res[0].subscriptionList);
                var c = component.get("v.SubscriptionList");
                console.log(res);
            }
        });
        $A.enqueueAction(action2);
        component.set("v.spinner", false);
    },
    openShipping : function(component, event, helper) {
        debugger
        component.set("v.OpenSubscription",false);
		component.set("v.openShipping",true);
        var selectedContacts = [];
        component.set("v.isOpen",false);
        var cardCheck = component.get("v.cardCheck");
        if(cardCheck != '' && cardCheck != null){
             var checkvalue = component.find("checkResult");
            var cardcheckvalue = component.find("checkboxCard");
           
               if (checkvalue[0].get("v.value") == true && cardcheckvalue[0].get("v.value") == false) {
                    component.set("v.isShipped",true);
                    component.set("v.isUpdateCard",false);
                }  else if (cardcheckvalue[0].get("v.value") == true && checkvalue[0].get("v.value") == true) {
                    component.set("v.isShipped",false);
                    component.set("v.isUpdateCard",true);
                    component.set("v.isShipping",true);
                } else if (cardcheckvalue[0].get("v.value") == true && checkvalue[0].get("v.value") == false) {
                    component.set("v.isShipped",false);
                    component.set("v.isUpdateCard",true);
                    component.set("v.isShipping",false);
                }  else{
                    component.set("v.OpenSubscription",true);
                    component.set("v.openShipping",false);
                    var msg = 'Check Any Update Option.';
                    var msgType = "error";
                    component.set("v.message", msg);
                    component.set("v.messageType", msgType);
                    $A.util.removeClass(component.find('toastModel'), 'slds-hide');
                    $A.util.addClass(component.find('toastModel'), 'slds-show');
                    setTimeout(function () {
                        $A.util.addClass(component.find('toastModel'), 'slds-hide');
                        component.set("v.message", "");
                        component.set("v.messageType", "");
                    }, 5000);
                }
        }
        else{
            component.set("v.OpenSubscription",true);
            component.set("v.openShipping",false);
            var msg = 'Please Select Card';
            var msgType = "error";
            component.set("v.message", msg);
            component.set("v.messageType", msgType);
            $A.util.removeClass(component.find('toastModel'), 'slds-hide');
            $A.util.addClass(component.find('toastModel'), 'slds-show');
            setTimeout(function () {
                $A.util.addClass(component.find('toastModel'), 'slds-hide');
                component.set("v.message", "");
                component.set("v.messageType", "");
            }, 5000);
        }
        
    },
    
    selectoptionvalue: function(component, event, helper) {
        debugger
        var cardCheck = component.get("v.cardCheck");
        var recordId = component.get("v.recordId");
        var Street = component.get("v.AddressWrapper.AddressList.Shipping_Street__c");
        var City = component.get("v.AddressWrapper.AddressList.Shipping_City__c");
        var State = component.get("v.AddressWrapper.AddressList.Shipping_State_Province__c");
        var PostalCode = component.get("v.AddressWrapper.AddressList.Shipping_Zip_Postal_Code__c");
        var Country = component.get("v.AddressWrapper.AddressList.Shipping_Country__c");
        var action   = component.get("c.AddShipping");
        action.setParams({ Street : Street,
                          City:City,
                          State:State,
                          PostalCode:PostalCode,
                          Country:Country,
                          recordId : recordId,
                          cardCheck : cardCheck,
                          recordId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                console.log(res);
            }
        });
       component.set("v.isShipped", false);
        component.set("v.addressMsg", true);
        $A.enqueueAction(action);
        var delay=7000; //7 seconds
        setTimeout(function() {
            component.set("v.openShipping", false);
        }, delay);
    },
    
    RecordUpdate : function(component, event, helper){ 
        
        var recordId   = component.get("v.recordId");
        component.set("v.conIdvale",recordId);
        var firstname  = component.get("v.ContactWrapper.contactList.FirstName");
        var LastName   = component.get("v.ContactWrapper.contactList.LastName");
        var Phone      = component.get("v.ContactWrapper.contactList.Phone");
        var Street     = component.get("v.AddressWrapper.AddressList.Shipping_Street__c");
        var City       = component.get("v.AddressWrapper.AddressList.Shipping_City__c");
        var state      = component.get("v.AddressWrapper.AddressList.Shipping_State_Province__c");
        var zipcode    = component.get("v.AddressWrapper.AddressList.Shipping_Zip_Postal_Code__c");
        var country    = component.get("v.AddressWrapper.AddressList.Shipping_Country__c");
        var action1    = component.get("c.updateInfoUser");
        action1.setParams({ firstname : firstname,
                           idparam : recordId,
                           lastName:LastName,
                           phone:Phone,
                           street:Street,
                           city:City,
                           state:state,
                           zipCode:zipcode,
                           country:country
                          });
        
        action1.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                var message = firstname+' '+LastName;
                var vfOrigin = "https://" + component.get("v.vfHost1");
                var vfWindow = component.find("vfFrame").getElement().contentWindow;
                vfWindow.postMessage(message, vfOrigin);
                
                var res = response.getReturnValue();
                component.set("v.registerSuccessMsg",res);
                component.set("v.showcomp",false);
                console.log(res);
                
            }
            else{
                
                component.set("v.showcomp",true);
            }
            
        });
        $A.enqueueAction(action1);
        
    },
    
    chkTerms : function(component, event, helper){
        var isChecked = component.find("chkTerms").get("v.value");
        if(isChecked == false){
            component.set("v.showbtn",true);
        }
        var addrValid='';
        if(isChecked){
        var Street     = component.get("v.AddressWrapper.AddressList.Shipping_Street__c");
        var City       = component.get("v.AddressWrapper.AddressList.Shipping_City__c");
        var state      = component.get("v.AddressWrapper.AddressList.Shipping_State_Province__c");
        var zipcode    = component.get("v.AddressWrapper.AddressList.Shipping_Zip_Postal_Code__c");
        var country    = component.get("v.AddressWrapper.AddressList.Shipping_Country__c");
        var action1    = component.get("c.validateAddress");    
        action1.setParams({street:Street,
                           city:City,
                           state:state,
                           country:country,
                           zipCode:zipcode
                          });
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS") {        
                var res = response.getReturnValue();
                addrValid = res;
                if(res != 'No error'){
                    component.set("v.isAddrError",true);
                	component.set("v.addrErrorMsg",res);
                    component.find("chkTerms").set("v.value", false);
                    component.set("v.showbtn",true);
                }else{
                    component.set("v.isAddrError",false);
                    component.set("v.showbtn",false);
                }
                console.log(res);
            }
        });
        $A.enqueueAction(action1); 
        }
        
    },
    
    phonehandlechange : function(component, event, helper){
        
        var phone = component.get("v.ContactWrapper.contactList.Phone");
        if(phone.length == 3){
            var newphoneno = phone+'-';
            component.set("v.ContactWrapper.contactList.Phone",newphoneno);
        }
        if(phone.length == 7){
            var newphoneno = phone+'-';
            component.set("v.ContactWrapper.contactList.Phone",newphoneno);
        }
        if(phone.length > 12){
            
        }
    },
    closeAlert: function (component, event, helper) {
        $A.util.addClass(component.find('toastModel'), 'slds-hide');
    },
    
    closecard:function(component, event, helper) {
        component.set("v.OpenSubscription", false);
        window.location.reload();
    },
    closeShipping :function(component, event, helper) {
        
        component.set("v.openShipping", false);
        window.location.reload();
    },
    backFunction : function(component, event, helper) {
        debugger
        component.set("v.openShipping",false);
        var a = component.get('c.openModel');
        $A.enqueueAction(a);
    }
});