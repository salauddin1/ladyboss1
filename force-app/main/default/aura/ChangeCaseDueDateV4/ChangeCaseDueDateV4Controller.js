({
    
    init : function(component, event, helper) {
        debugger
        console.log('init');
        helper.doInit(component, event, helper);
        var action = component.get("c.getfields");
        action.setCallback(this, function(response) {
            debugger
            var customFieldVal = [];
            var columnFieldVal = [];
            var rep = response.getReturnValue();
            console.log('------rep------'+rep);
            console.log('---------fieldsName--------'+response.getReturnValue());
            component.set("v.fieldsName", response.getReturnValue());
            var caseVal = component.get('v.cas');
            var fieldVal = component.get('v.fieldsName');
            for(var i =0; i < fieldVal.length ; i++){
                debugger
                if(fieldVal[i].Column__c == '1') {
                    columnFieldVal.push({fieldsName :fieldVal[i],
                                         isboolean:caseVal[fieldVal[i].Field_Api_Name__c]});
                    
                }
                else if(fieldVal[i].Column__c == '2'){
                    customFieldVal.push({fieldsName :fieldVal[i],
                                         isboolean:caseVal[fieldVal[i].Field_Api_Name__c]});
                }
            }
            component.set('v.customFieldData',customFieldVal);
            component.set('v.columnFieldData',columnFieldVal);
        });
        $A.enqueueAction(action);
    }, 
    save : function(component, event, helper) {
        debugger
        var capturedCheckboxName = event.getSource().get("v.checked");
        var fieldNameForDays = event.getSource().get("v.name");
        console.log('----fieldNameForDays-->'+fieldNameForDays + JSON.stringify(fieldNameForDays));
        console.log('taskold---'+JSON.stringify(capturedCheckboxName));
        var cas = component.get("v.cas");
        helper.createOrder(component, event, cas,JSON.stringify(capturedCheckboxName),fieldNameForDays);
        console.log('updated cas : '+JSON.stringify(component.get("v.cas")));
        $A.get('e.force:refreshView').fire();
    },
    updateCase : function(component, event, helper) {
        debugger
        var capturedCheckboxName = event.getSource().get("v.checked");
        var fieldNameForDays = event.getSource().get("v.name");
        console.log('----fieldNameForDays-->'+fieldNameForDays + JSON.stringify(fieldNameForDays));
        console.log('taskold---'+JSON.stringify(capturedCheckboxName));
        var cas = component.get("v.cas");
        helper.updateCase(component, event, cas,JSON.stringify(capturedCheckboxName),fieldNameForDays);
        console.log('updated cas : '+JSON.stringify(component.get("v.cas")));
        $A.get('e.force:refreshView').fire();
    },
    updateCheck : function(component, event, helper) {
        debugger
        var capturedCheckboxName = event.getSource().get("v.checked");
        var fieldNameForDays = event.getSource().get("v.name");
        console.log('----fieldNameForDays-->'+fieldNameForDays + JSON.stringify(fieldNameForDays));
        console.log('taskold---'+JSON.stringify(capturedCheckboxName));
        var cas = component.get("v.cas");
        helper.updateCheck(component, event, cas,JSON.stringify(capturedCheckboxName),fieldNameForDays);
        console.log('updated cas : '+JSON.stringify(component.get("v.cas")));
        $A.get('e.force:refreshView').fire();
    },
})