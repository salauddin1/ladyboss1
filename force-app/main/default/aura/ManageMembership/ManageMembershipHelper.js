({
	doInit: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var action = component.get("c.getAllProducts");
		var productOne = component.get("v.productOne");
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				for (var i = 0; i < result.length; i++) {
					if (result[i].Name.length > 26) {
						result[i].Names = result[i].Name.substr(0, 26) + "...";
					} else {
						result[i].Names = result[i].Name;
					}
				}
				component.set("v.Allproducts", result);
				for (var i = 0; i < productOne.length; i++) {
					Loop1: for (var j = 0; j < result.length; j++) {
						if (result[j] != undefined) {
							if (result[j].planId == productOne[i].planId) {
								result.splice(j, 1);
								break Loop1;
							}
						}
					}
				}
				for (var i = 0; i < result.length; i++) {
					if (result[i].switchProduct) {
						Loop2: for (var j = 0; j < productOne.length; j++) {
							for (var k = 0; k < result[i].pwList.length; k++) {
								if (result[i].pwList[k].planId == productOne[j].planId) {
									result.splice(i, 1);
									break Loop2;
								}
							}
						}
					}
				}
				for (var i = 0; i < result.length; i++) {
					console.log("=====vrify cross data========" + result[i].Name + ' ' + result[i].switchProduct);
					if (result[i].customTansformationProduct) {
						Loop3: for (var j = 0; j < productOne.length; j++) {
							for (var k = 0; k < result[i].pwList.length; k++) {
								if (result[i].pwList[k].planId == productOne[j].planId) {
									result.splice(i, 1);
									break Loop3;
								}
							}
						}
					}
				}
				var Temparr2 = [];
				Loop4: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("LEAN")) {
						result[i].showProduct = false;
						for (var j = 0; j < result[i].pwList.length; j++) {
							if (result[i].pwList[j].numOfBags == '1') {
								result[i].Id = result[i].pwList[j].Id;
								result[i].tax = result[i].pwList[j].tax;
								result[i].Name = result[i].pwList[j].Name;
								result[i].totalPrice = result[i].pwList[j].totalPrice;
								result[i].taxCode = result[i].pwList[j].taxCode;
								result[i].image = result[i].pwList[j].image;
								result[i].planId = result[i].pwList[j].planId;
							}
						}
						Temparr2.push(result[i]);
						break Loop4;
					}
				}
				Loop5: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("GREENS")) {
						result[i].showProduct = false;
						for (var j = 0; j < result[i].pwList.length; j++) {
							if (result[i].pwList[j].numOfBags == '1') {
								result[i].Id = result[i].pwList[j].Id;
								result[i].Name = result[i].pwList[j].Name;
								result[i].tax = result[i].pwList[j].tax;
								result[i].totalPrice = result[i].pwList[j].totalPrice;
								result[i].taxCode = result[i].pwList[j].taxCode;
								result[i].image = result[i].pwList[j].image;
								result[i].planId = result[i].pwList[j].planId;
							}
						}
						Temparr2.push(result[i]);
						break Loop5;
					}
				}
				Loop6: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("Transformation System")) {
						Temparr2.push(result[i]);
						break Loop6;
					}
				}
				Loop7: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("BURN")) {
						Temparr2.push(result[i]);
						break Loop7;
					}
				}
				Loop8: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("FUEL")) {
						Temparr2.push(result[i]);
						break Loop8;
					}
				}
				Loop9: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("RECOVER")) {
						Temparr2.push(result[i]);
						break Loop9;
					}
				}
				Loop10: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("REST")) {
						Temparr2.push(result[i]);
						break Loop10;
					}
				}
				Loop11: for (var i = 0; i < result.length; i++) {
					if (result[i].Name.includes("Trainer Monthly Membership")) {
						Temparr2.push(result[i]);
						break Loop11;
					}
				}

				component.set("v.productThree", Temparr2);
				this.getCard(component, event, helper);
				component.set("v.Spinner", false);
			}
		});
		$A.enqueueAction(action);
	},
	getAll: function (component, event, helper) {
		var action = component.get("c.getAllProducts");

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				for (var i = 0; i < result.length; i++) {
					if (result[i].Name.length > 26) {
						result[i].Names = result[i].Name.substr(0, 26) + "...";
					} else {
						result[i].Names = result[i].Name;
					}
				}
				component.set("v.Allproducts", result);
			}
		});
		$A.enqueueAction(action);
	},
	personalizeMembership: function (component, event, helper) {

		var productOne = component.get("v.productOne");
		var opplineitems = component.get("v.OppLineItem");
		var len = [];
		var addedprods = [];
		var canceledprods = [];
		var modalInfoAdd = "";
		var modalInfoCancel = "";
		var showTotal = "";
		var todayTotal = 0.0;
		for (var i = 0; i < productOne.length; i++) {
			for (var j = 0; j < opplineitems.length; j++) {
				if (
					productOne[i].planId == opplineitems[j].planId &&
					!len.includes(opplineitems[j])
				) {
					len.push(opplineitems[j]);
				}
			}
		}
		if (len.length == productOne.length && len.length >= opplineitems.length) {
			component.set("v.selectedModal", "ModalboxNoChange");
			this.openmodal(component, event, helper);
			component.set("v.Spinner", false);
		} else {
			var action = component.get("c.checkPayment");
			action.setParams({
				prod: JSON.stringify(opplineitems),
				alreadySubscribed: JSON.stringify(productOne)
			});
			action.setCallback(this, function (response) {
				var state = response.getState();
				if (state == "SUCCESS") {
					var result = response.getReturnValue();

					console.log("personalizeMembership " + result.length);
					for (var i = 0; i < result.length; i++) {
						if (result[i].listStatus == "cancel") {
							canceledprods.push(result[i]);
						} else if (result[i].listStatus == "add") {
							addedprods.push(result[i]);
						}
					}
					if (addedprods.length > 0) {
						component.set("v.selectedModal", "ModalboxValidate");
						modalInfoAdd = "You have added this membership(s): ";
						showTotal = "This order will charge immediately on card ending in ";
						for (var i = 0; i < addedprods.length; i++) {
							if (i != addedprods.length - 1) {
								modalInfoAdd = modalInfoAdd + addedprods[i].Name + ", ";
							} else {
								modalInfoAdd = modalInfoAdd + addedprods[i].Name;
							}
							todayTotal += addedprods[i].totalPrice;
						}
						// showTotal = showTotal + todayTotal;
					}
					if (canceledprods.length > 0 && addedprods.length <= 0) {
						component.set("v.selectedModal", "CancelModalboxValidate");
						modalInfoCancel = "You have removed this Membership(s): ";
						for (var i = 0; i < canceledprods.length; i++) {
							if (i != canceledprods.length - 1) {
								modalInfoCancel =
									modalInfoCancel + canceledprods[i].Name + ", ";
							} else {
								modalInfoCancel = modalInfoCancel + canceledprods[i].Name;
							}
						}
					}
					if (canceledprods.length > 0) {
						modalInfoCancel = "You have removed this Membership(s): ";
						for (var i = 0; i < canceledprods.length; i++) {
							if (i != canceledprods.length - 1) {
								modalInfoCancel =
									modalInfoCancel + canceledprods[i].Name + ", ";
							} else {
								modalInfoCancel = modalInfoCancel + canceledprods[i].Name;
							}
						}
					}
					if (showTotal != "") {
						showTotal += component.get("v.cardLast4") + " and will ship to: ";
						component.set("v.todaysTotal", showTotal);

						var addLine1 = component.get("v.AddressValue");
						var addLine = addLine1.split(",");
						component.set("v.addressLine1", addLine[0]);
						component.set("v.newAdd", addLine[1] + "," + addLine[2]);
					}
					component.set("v.messageAdd", modalInfoAdd);
					component.set("v.messageCancel", modalInfoCancel);
					component.set("v.addedProducts", addedprods);
					component.set("v.canceledProducts", canceledprods);
					this.openmodal(component, event, helper);
				}
				component.set("v.Spinner", false);
			});
			$A.enqueueAction(action);
		}
	},
	getCard: function (component, event, helper) {
		var recordId = component.get("v.recordId");
		var action = component.get("c.getCard");
		action.setParams({
			conID: recordId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				console.log("get all", result);
				var cardnum = [];
				var cards = [];
				for (var i = 0; i < result.cardList.length; i++) {
					if (!cardnum.includes(result.cardList[i].Last4__c)) {
						result.cardList[i].Credit_Card_Last4 =
							"********" + result.cardList[i].Last4__c;
						cards.push(result.cardList[i]);
						cardnum.push(result.cardList[i].Last4__c);
					}
				}
				if (result.cardList.length > 0) {
					component.set("v.cardLast4", result.cardList[0].Last4__c);
				}

				if (result.Address != null) {
					component.set("v.AddressValue", result.Address);
				}
				console.log("get all", component.get("v.cardLast4"));
				component.set("v.cardList", cards);
			}
		});
		$A.enqueueAction(action);
	},
	openmodal: function (component, event, helper) {
		var selectedModal = component.get("v.selectedModal");
		console.log("openModal", selectedModal);
		var cmpTarget = component.find(selectedModal);
		var cmpBack = component.find("Modalbackdrop");
		$A.util.addClass(cmpTarget, "slds-fade-in-open");
		$A.util.addClass(cmpBack, "slds-backdrop--open");
	}
});