({
	init: function (component, event, helper) {
		console.log("in manage subscriptions");
		component.set("v.showCard", false);
		component.set("v.verifyDetails", false);
		component.set("v.prodidx", "0");
		component.set("v.showtodaytotal", false);
		component.set("v.totalToday", 0.0);
		window.addEventListener(
			"message",
			function (event) {
				if (event.origin != "https://ladyboss-support.secure.force.com") {
					return;
				}
				component.set("v.refreshCard", true);
				component.set("v.newCard", false);
			},
			false
		);
		var recordId = component.get("v.recordId");
		var action = component.get("c.getProducts");
		action.setParams({
			conID: recordId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			window.scrollTo(0, 0);
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				var subtotal = 0.0;
				if (result.length == 0) {

					component.set("v.isSelected", true);
					component.set("v.isEmptyList", true);
				} else {
					for (var i = 0; i < result.length; i++) {
						if (result[i].Name.includes("BURN")) {
							result[i].image = "BURN.png";
						} else if (result[i].Name.includes("REST")) {
							result[i].image = "REST.png";
						} else if (result[i].Name.includes("RECOVER")) {
							result[i].image = "RECOVER.png";
						} else if (result[i].Name.includes("FUEL")) {
							result[i].image = "FUEL.png";
						} else if (result[i].Name.includes("LEAN 1")) {
							result[i].image = "LEAN1.png";
						} else if (result[i].Name.includes("LEAN 2")) {
							result[i].image = "LEAN2.png";
						} else if (result[i].Name.includes("LEAN 3")) {
							result[i].image = "LEAN3.png";
						} else if (result[i].Name.includes("GREENS-1")) {
							result[i].image = "GREENS1.png";
						} else if (result[i].Name.includes("GREENS-2")) {
							result[i].image = "GREENS2.png";
						} else if (result[i].Name.includes("GREENS-3")) {
							result[i].image = "GREENS3.png";
						}

						if (result[i].Name.length > 26) {
							result[i].Names = result[i].Name.substr(0, 26) + "...";
						} else {
							result[i].Names = result[i].Name;
						}
						subtotal += result[i].totalPrice;
					}
				}
				component.set("v.OppLineItem", result);
				component.set("v.total", subtotal.toFixed(2));
				component.set("v.addedMemTotal", subtotal.toFixed(2));
				component.set("v.productOne", result);
				helper.doInit(component, event, helper);
			}
		});
		$A.enqueueAction(action);

		var action2 = component.get("c.getAddress");
		action2.setParams({
			conID: recordId
		});
		action2.setCallback(this, function (response) {
			var state = response.getState();
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				var subtotal = 0.0;
				if (result != null) {

					component.set("v.addressDetails", result.address);
					component.set("v.ContacthasCommission",result.hasCommission);
					component.set("v.AffiliateCommisssion",result.commission);
				}
			}
		});
		$A.enqueueAction(action2);


	},
	checkProds: function (component, event, helper) {

		var selected = component.get("v.OppLineItem");
		if (selected.length <= 0) {
			component.set("v.selectedModal", "ModalboxCancel");
			helper.openmodal(component, event, helper);
			component.set("v.isSelected", false);
		} else {

			component.set("v.Spinner", true);
			helper.personalizeMembership(component, event, helper);
		}
	},
	cancelOpp: function (component, event, helper) {
		var a = component.get("c.closemodal");
		$A.enqueueAction(a);
		console.log("First step;- fire event");
		component.set("v.Spinner", true);
		var refreshevent = $A.get("e.c:RefreshSubscriptions");
		refreshevent.setParams({
			data: "CancelSubscriptionFinal"
		});
		refreshevent.fire();
	},
	/*removeRow: function(component, event, helper) {
	    var addedmemtotal = component.get("v.addedMemTotal");
	    var idx = event.target.getAttribute("data-index");
	    var opplineitems = component.get("v.OppLineItem");
	    var prodThree = component.get("v.productThree");
	    var alpro = component.get("v.Allproducts");
	    var prodOne = component.get("v.productOne");
	    var planProd = "";
	    var transProd = [];
	    var todtotal = component.get("v.totalToday");
	    var taxAmountValue = component.get("v.taxAmountValue");
	    var taxOppLineItem = component.get("v.taxOppLineItem");
	    var taxOppLineItem1 = component.get("v.taxOppLineItem");
	    var totalPriceforAg = component.get("v.totalPriceforAg");
	    
	    for (var i = 0; i < opplineitems.length; i++) {
	        if (idx == opplineitems[i].Id) {
	            addedmemtotal = addedmemtotal - opplineitems[i].totalPrice - parseFloat(opplineitems[i].tax);
	            if (!prodOne.includes(opplineitems[i])) {
	            todtotal =
	                parseFloat(todtotal) - parseFloat(opplineitems[i].totalPrice) - parseFloat(opplineitems[i].tax);
	            totalPriceforAg =
	                parseFloat(totalPriceforAg) - parseFloat(opplineitems[i].totalPrice) ;
	                                                     component.set("v.totalToday", todtotal.toFixed(2));
	                                                     taxAmountValue = parseFloat(taxAmountValue) - parseFloat(opplineitems[i].tax); ;
	                                                     component.set("v.taxAmountValue",taxAmountValue.toFixed(2));
	                                                     component.set("v.totalPriceforAg",totalPriceforAg.toFixed(2));
	                                                     
	                                                     for (var k = 0; k < taxOppLineItem.length; k++) {
	                                                         if (idx == taxOppLineItem[k].Id) {
	                                                             taxOppLineItem.splice(k, 1);
	                                                         }
	                                                         //  taxOppLineItem.remove(opplineitems[i]);
	                                                         component.set("v.taxOppLineItem", taxOppLineItem);
	                                                     }
	                                                    }
	            planProd = opplineitems[i].planId;
	            if (
	                !opplineitems[i].Name.includes("LEAN") &&
	                idx != "customtrans" &&
	                !opplineitems[i].transformationProd && !opplineitems[i].Name.includes("GREENS")
	            ) {
	                prodThree.push(opplineitems[i]);
	            }
	            opplineitems.splice(i, 1);
	            break;
	        }
	    }
	    if (opplineitems.length <= 0) {
	        
	        component.set("v.isSelected", true);
	        component.set("v.verifyDetails", false);
	        component.set("v.newCard", false);
	        component.set("v.showtodaytotal", false);
	        if (prodOne.length <= 0) {
	            component.set("v.isEmptyList", true);
	        }
	        component.set("v.showCard", false);
	    }
	    
	    for (var i = 0; i < alpro.length; i++) {
	        if (!prodThree.includes(alpro[i])) {
	            for (var j = 0; j < alpro[i].pwList.length; j++) {
	                if (planProd == alpro[i].pwList[j].planId) {
	                    alpro[i].Id = idx;
	                    alpro.showProduct = true;
	                    prodThree.push(alpro[i]);
	                    console.log("remove lean", alpro[i].Names);
	                    
	                    if (alpro[i].switchProduct) {
	                        component.set("v.showProduct", true);
	                    }
	                    break;
	                }
	            }
	        }
	    }
	    var Temparr2 = [];
	    Loop4: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("LEAN")) {
	            prodThree[i].showProduct = false;
	            for (var j = 0; j < prodThree[i].pwList.length; j++) {
	                if (prodThree[i].pwList[j].numOfBags == '1') {
	                    prodThree[i].Id = prodThree[i].pwList[j].Id;
	                    prodThree[i].Name = prodThree[i].pwList[j].Name;
	                    prodThree[i].totalPrice = prodThree[i].pwList[j].totalPrice;
	                    prodThree[i].taxCode = prodThree[i].pwList[j].taxCode;
	                    prodThree[i].image = prodThree[i].pwList[j].image;
	                    prodThree[i].planId = prodThree[i].pwList[j].planId;
	                }
	            } 
	            
	            Temparr2.push(prodThree[i]);
	            break Loop4;
	        }
	    }
	    Loop5: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("GREENS")) {
	            prodThree[i].showProduct = false;
	            for (var j = 0; j < prodThree[i].pwList.length; j++) {
	                if (prodThree[i].pwList[j].numOfBags == '1') {
	                    prodThree[i].Id = prodThree[i].pwList[j].Id;
	                    prodThree[i].Name = prodThree[i].pwList[j].Name;
	                    prodThree[i].totalPrice = prodThree[i].pwList[j].totalPrice;
	                    prodThree[i].taxCode = prodThree[i].pwList[j].taxCode;
	                    prodThree[i].image = prodThree[i].pwList[j].image;
	                    prodThree[i].planId = prodThree[i].pwList[j].planId;
	                }
	            } 
	            
	            Temparr2.push(prodThree[i]);
	            break Loop5;
	        }
	    }
	    Loop6: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("Transformation System")) {
	            Temparr2.push(prodThree[i]);
	            break Loop6;
	        }
	    }
	    Loop7: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("BURN")) {
	            Temparr2.push(prodThree[i]);
	            break Loop7;
	        }
	    }
	    Loop8: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("FUEL")) {
	            Temparr2.push(prodThree[i]);
	            break Loop8;
	        }
	    }
	    Loop9: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("RECOVER")) {
	            Temparr2.push(prodThree[i]);
	            break Loop9;
	        }
	    }
	    Loop10: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("REST")) {
	            Temparr2.push(prodThree[i]);
	            break Loop10;
	        }
	    }
	    Loop11: for (var i = 0; i < prodThree.length; i++) {
	        if (prodThree[i].Name.includes("Trainer Monthly Membership")) {
	            Temparr2.push(prodThree[i]);
	            break Loop11;
	        }
	    }
	    component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
	    component.set("v.OppLineItem", opplineitems);
	    component.set("v.productThree", Temparr2);
	    component.set("v.verifyDetails", false);
	},
	*/
	removeRow: function (component, event, helper) {
		debugger
		var addedmemtotal = component.get("v.addedMemTotal");
		var idx = event.target.getAttribute("data-index");
		var opplineitems = component.get("v.OppLineItem");
		var prodThree = component.get("v.productThree");
		var alpro = component.get("v.Allproducts");
		var prodOne = component.get("v.productOne");
		var planProd = "";
		var transProd = [];
		var transProdtemp = [];
		var todtotal = component.get("v.totalToday");
		var taxAmountValue = component.get("v.taxAmountValue");
		var taxOppLineItem = component.get("v.taxOppLineItem");
		var taxOppLineItem1 = component.get("v.taxOppLineItem");
		var totalPriceforAg = component.get("v.totalPriceforAg");

		for (var i = 0; i < opplineitems.length; i++) {
			if (idx == opplineitems[i].Id) {
				addedmemtotal = addedmemtotal - opplineitems[i].totalPrice - parseFloat(opplineitems[i].tax);
				if (!prodOne.includes(opplineitems[i])) {
					todtotal =
						parseFloat(todtotal) - parseFloat(opplineitems[i].totalPrice) - parseFloat(opplineitems[i].tax);
					totalPriceforAg =
						parseFloat(totalPriceforAg) - parseFloat(opplineitems[i].totalPrice);
					component.set("v.totalToday", todtotal.toFixed(2));
					taxAmountValue = parseFloat(taxAmountValue) - parseFloat(opplineitems[i].tax);;
					component.set("v.taxAmountValue", taxAmountValue.toFixed(2));
					component.set("v.totalPriceforAg", totalPriceforAg.toFixed(2));

					for (var k = 0; k < taxOppLineItem.length; k++) {
						if (idx == taxOppLineItem[k].Id) {
							taxOppLineItem.splice(k, 1);
						}
						//  taxOppLineItem.remove(opplineitems[i]);
						component.set("v.taxOppLineItem", taxOppLineItem);
					}
				}

				planProd = opplineitems[i].planId;
				var opplineitemsnew = component.get("v.OppLineItem");
				if (
					!opplineitems[i].Name.includes("LEAN") &&
					idx != "customtrans" &&
					!opplineitems[i].transformationProd && !opplineitems[i].Name.includes("GREENS")
				) {

					prodThree.push(opplineitems[i]);
				}

				opplineitems.slice(i, 1);
				break;
			}
		}
		if (opplineitems.length <= 0) {

			component.set("v.isSelected", true);
			component.set("v.verifyDetails", false);
			component.set("v.newCard", false);
			component.set("v.showtodaytotal", false);
			if (prodOne.length <= 0) {
				component.set("v.isEmptyList", true);
			}
			component.set("v.showCard", false);
		}
		debugger
		for (var i = 0; i < alpro.length; i++) {
			if (!prodThree.includes(alpro[i])) {
				for (var j = 0; j < alpro[i].pwList.length; j++) {
					if (planProd == alpro[i].pwList[j].planId) {
						alpro[i].Id = idx;
						alpro.showProduct = true;
						prodThree.push(alpro[i]);
						console.log("remove lean", alpro[i].Names);

						if (alpro[i].switchProduct) {
							component.set("v.showProduct", false);
						}
						break;
					}
				}
			}
		}
		debugger
		var Temparr2 = [];
		Loop4: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("LEAN")) {
				prodThree[i].showProduct = false;
				console.log('--opplineitems---' + opplineitemsnew);
				for (var j = 0; j < opplineitemsnew.length; j++) {
					if (opplineitemsnew[j].Name.includes("LEAN")) {
						prodThree[i].Id = opplineitemsnew[j].Id;
						prodThree[i].Name = opplineitemsnew[j].Name;
						prodThree[i].totalPrice = opplineitemsnew[j].totalPrice;
						prodThree[i].taxCode = opplineitemsnew[j].taxCode;
						prodThree[i].image = opplineitemsnew[j].image;
						prodThree[i].planId = opplineitemsnew[j].planId;
					}
				}
				Temparr2.push(prodThree[i]);
				break Loop4;
			}
		}
		Loop5: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("GREENS")) {
				prodThree[i].showProduct = false;
				for (var j = 0; j < opplineitemsnew.length; j++) {
					if (opplineitemsnew[j].Name.includes("GREENS")) {
						prodThree[i].Id = opplineitemsnew[j].Id;
						prodThree[i].Name = opplineitemsnew[j].Name;
						prodThree[i].totalPrice = opplineitemsnew[j].totalPrice;
						prodThree[i].taxCode = opplineitemsnew[j].taxCode;
						prodThree[i].image = opplineitemsnew[j].image;
						prodThree[i].planId = opplineitemsnew[j].planId;
					}
				}
				Temparr2.push(prodThree[i]);
				break Loop5;
			}
		}
		Loop6: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("Transformation System")) {
				Temparr2.push(prodThree[i]);
				break Loop6;
			}
		}
		Loop7: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("BURN")) {
				Temparr2.push(prodThree[i]);
				break Loop7;
			}
		}
		Loop8: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("FUEL")) {
				Temparr2.push(prodThree[i]);
				break Loop8;
			}
		}
		Loop9: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("RECOVER")) {
				Temparr2.push(prodThree[i]);
				break Loop9;
			}
		}
		Loop10: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("REST")) {
				Temparr2.push(prodThree[i]);
				break Loop10;
			}
		}
		Loop11: for (var i = 0; i < prodThree.length; i++) {
			if (prodThree[i].Name.includes("Trainer Monthly Membership")) {
				Temparr2.push(prodThree[i]);
				break Loop11;
			}
		}
		debugger
		for (var i = 0; i < opplineitems.length; i++) {
			if (planProd == opplineitems[i].planId) {
				console.log('opplineitems----' + opplineitems[i]);
				opplineitems.splice(i, 1);
				break;
			}
		}

		component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
		component.set("v.OppLineItem", opplineitems);
		debugger
		component.set("v.productThree", Temparr2);
		component.set("v.verifyDetails", false);

	},


	closeModel: function (component, event, helper) {
		$A.util.addClass(component.find("toastModel"), "slds-hide");
	},
	addProdTransformation: function (component, event, helper) {
		component.set("v.selectedModal", "ModalboxCustomTrans");
		helper.openmodal(component, event, helper);
		let scrollObj = document.getElementById("scrollhere");
		if (scrollObj) {
			console.log("height============" + scrollObj.textContent);
			//scrollObj.scrollIntoView(true)
			scrollObj.scrollIntoView({
				behavior: "smooth",
				block: "start",
				inline: "start"
			});
		}
	},
	/*addProds: function(component, event, helper) {
	    
	    
	    var idx = event.target.getAttribute("data-index");
	    var addedmemtotal = component.get("v.addedMemTotal");
	    var opplineitems = component.get("v.OppLineItem");
	    var prod = component.get("v.productThree");
	    var alpro = component.get("v.Allproducts");
	    var prodOne = component.get("v.productOne");
	    var planProd = "";
	    var todtotal = component.get("v.totalToday");
	    var totalPriceforAg = component.get("v.totalPriceforAg");
	    var check = false;
	    
	    for (var i = 0; i < prod.length; i++) {
	        if (idx == prod[i].Id) {
	            prod[i].checkValue = true;
	            planProd = prod[i].planId;
	            console.log("addProds row", prod[i].cardList);
	            opplineitems.push(prod[i]);  
	            addedmemtotal =
	                parseFloat(addedmemtotal) + parseFloat(prod[i].totalPrice);
	            if (!prodOne.includes(prod[i])) {
	                todtotal = parseFloat(todtotal) + parseFloat(prod[i].totalPrice);
	                totalPriceforAg = parseFloat(totalPriceforAg) + parseFloat(prod[i].totalPrice);
	                component.set("v.showtodaytotal", true);
	                component.set("v.totalToday", todtotal.toFixed(2));
	                component.set("v.totalPriceforAg", totalPriceforAg.toFixed(2));
	                check = true ;
	            }
	            component.set("v.showCard", true);
	            prod.splice(i, 1);
	            
	            component.set("v.isSelected", false);
	            component.set("v.isEmptyList", false);
	            component.set("v.verifyDetails", false);
	            break;
	        }
	    }
	    
	    //   component.set("v.OppLineItem", opplineitems);
	    component.set("v.productThree", prod);
	    let scrollObj = document.getElementById("scrollhere");
	    if (scrollObj) {
	        console.log("height============" + scrollObj.textContent);
	        //scrollObj.scrollIntoView(true)
	        scrollObj.scrollIntoView({
	            behavior: "smooth",
	            block: "start",
	            inline: "start"
	        });
	    }
	    
	    var addressDetailsValue = component.get("v.addressDetails");
	    var postalCode = addressDetailsValue.Shipping_Zip_Postal_Code__c;
	    var line1 = addressDetailsValue.Shipping_Street__c;
	    var city = addressDetailsValue.Shipping_City__c;
	    var region = addressDetailsValue.Shipping_State_Province__c;
	    var country = addressDetailsValue.Shipping_Country__c;
	    
	    
	    for (var i = 0; i < opplineitems.length; i++) {
	        if (opplineitems[i].checkValue == true) {
	            if (idx == opplineitems[i].Id) { 
	                if (opplineitems[i].quantity == 0) {
	                    opplineitems[i].quantity = 1;
	                }
	            }
	        }
	    }    
	    var clubTax = 0.00;
	    var TaxToPay = 0;
	    var taxPercentageToPay = 0;
	    var priceWithTaxToPay = 0;
	    var prodItems3 = [];   
	    var taxOppLineItem = component.get("v.taxOppLineItem");
	    var taxAmountValue = component.get("v.taxAmountValue");
	                        
	    for (var j = 0; j < opplineitems.length; j++) {
	       // if (opplineitems[j].checkValue == true && idx == prod[j].Id) {
	              if ( idx == opplineitems[j].Id) {
	            var action = component.get('c.getTaxForProduct');
	            if(country.includes('United States') || country == 'USA'){
	                country = 'US';
	            }
	            action.setParams({
	                products: JSON.stringify(opplineitems[j]),
	                line1: line1,
	                city: city,
	                region: region,
	                country: country,
	                postalCode: postalCode
	            });
	            action.setCallback(this, function (response) {
	                var state = response.getState();
	                if ( state == "SUCCESS") {
	                    var result = response.getReturnValue();
	                    console.log("get all", result);
	                    
	                    TaxToPay  = result.tax;
	                    taxPercentageToPay = result.taxPercentage;
	                    priceWithTaxToPay = result.priceWithTax;
	                    
	                    
	                    opplineitems[j].tax = result.tax;
	                    opplineitems[j].taxPercentage = result.taxPercentage;
	                    opplineitems[j].totalPrice = result.totalPrice;
	                    opplineitems[j].priceWithTax = result.priceWithTax;
	                    
	                    if(check == true){
	                        taxAmountValue = parseFloat(taxAmountValue) + parseFloat(TaxToPay);
	                        component.set("v.taxAmountValue",taxAmountValue.toFixed(2));
	                        taxOppLineItem.push(opplineitems[j]);
	                        
	                        todtotal = parseFloat(todtotal) + parseFloat(opplineitems[j].tax);
	                        addedmemtotal = parseFloat(addedmemtotal) + parseFloat(opplineitems[j].tax);
	                        component.set("v.showtodaytotal", true);
	                        component.set("v.totalToday", todtotal.toFixed(2));
	                        
	                    }
	                    
	                    
	                    
	                }
	                component.set("v.OppLineItem", opplineitems);
	                component.set("v.taxOppLineItem", taxOppLineItem);
	                component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
	                
	            });
	            $A.enqueueAction(action);
	            break;
	        }
	    }
	}, */
	addProds: function (component, event, helper) {

		debugger
		var idx = event.target.getAttribute("data-index");
		var addedmemtotal = component.get("v.addedMemTotal");
		var opplineitems = component.get("v.OppLineItem");
		var prod = component.get("v.productThree");
		var alpro = component.get("v.Allproducts");
		var prodOne = component.get("v.productOne");
		var planProd = "";
		var todtotal = component.get("v.totalToday");
		var totalPriceforAg = component.get("v.totalPriceforAg");
		var check = false;

		var calculateTaxNew = false;
		debugger
		for (var j = 0; j < prodOne.length; j++) {
			if (prodOne[j].Id.includes(idx)) {
				calculateTaxNew = true;
			}
		}


		for (var i = 0; i < prod.length; i++) {
			if (idx == prod[i].Id) {
				prod[i].checkValue = true;
				planProd = prod[i].planId;
				console.log("addProds row", prod[i].cardList);
				opplineitems.push(prod[i]);
				addedmemtotal =
					parseFloat(addedmemtotal) + parseFloat(prod[i].totalPrice);
				if (!prodOne.includes(prod[i]) && calculateTaxNew == false) {
					todtotal = parseFloat(todtotal) + parseFloat(prod[i].totalPrice);
					totalPriceforAg = parseFloat(totalPriceforAg) + parseFloat(prod[i].totalPrice);
					component.set("v.showtodaytotal", true);
					component.set("v.totalToday", todtotal.toFixed(2));
					component.set("v.totalPriceforAg", totalPriceforAg.toFixed(2));
					check = true;
				}
				component.set("v.showCard", true);
				prod.splice(i, 1);

				component.set("v.isSelected", false);
				component.set("v.isEmptyList", false);
				component.set("v.verifyDetails", false);
				break;
			}
		}

		//   component.set("v.OppLineItem", opplineitems);
		component.set("v.productThree", prod);
		let scrollObj = document.getElementById("scrollhere");
		if (scrollObj) {
			console.log("height============" + scrollObj.textContent);
			//scrollObj.scrollIntoView(true)
			scrollObj.scrollIntoView({
				behavior: "smooth",
				block: "start",
				inline: "start"
			});
		}

		var addressDetailsValue = component.get("v.addressDetails");
		var postalCode = addressDetailsValue.Shipping_Zip_Postal_Code__c;
		var line1 = addressDetailsValue.Shipping_Street__c;
		var city = addressDetailsValue.Shipping_City__c;
		var region = addressDetailsValue.Shipping_State_Province__c;
		var country = addressDetailsValue.Shipping_Country__c;


		for (var i = 0; i < opplineitems.length; i++) {
			if (opplineitems[i].checkValue == true) {
				if (idx == opplineitems[i].Id) {
					if (opplineitems[i].quantity == 0) {
						opplineitems[i].quantity = 1;
					}
				}
			}
		}

		var clubTax = 0.00;
		var TaxToPay = 0;
		var taxPercentageToPay = 0;
		var priceWithTaxToPay = 0;
		var prodItems3 = [];
		var taxOppLineItem = component.get("v.taxOppLineItem");
		var taxAmountValue = component.get("v.taxAmountValue");
		if (calculateTaxNew == false) {

			for (var j = 0; j < opplineitems.length; j++) {
				// if (opplineitems[j].checkValue == true && idx == prod[j].Id) {
				if (idx == opplineitems[j].Id) {
					var action = component.get('c.getTaxForProduct');
					if (country.includes('United States') || country == 'USA') {
						country = 'US';
					}
					action.setParams({
						products: JSON.stringify(opplineitems[j]),
						line1: line1,
						city: city,
						region: region,
						country: country,
						postalCode: postalCode
					});
					action.setCallback(this, function (response) {
						var state = response.getState();
						if (state == "SUCCESS") {
							var result = response.getReturnValue();
							console.log("get all", result);

							TaxToPay = result.tax;
							taxPercentageToPay = result.taxPercentage;
							priceWithTaxToPay = result.priceWithTax;


							opplineitems[j].tax = result.tax;
							opplineitems[j].taxPercentage = result.taxPercentage;
							opplineitems[j].totalPrice = result.totalPrice;
							opplineitems[j].priceWithTax = result.priceWithTax;

							if (check == true) {
								taxAmountValue = parseFloat(taxAmountValue) + parseFloat(TaxToPay);
								component.set("v.taxAmountValue", taxAmountValue.toFixed(2));
								taxOppLineItem.push(opplineitems[j]);
								debugger
								todtotal = parseFloat(todtotal) + parseFloat(opplineitems[j].tax);
								addedmemtotal = parseFloat(addedmemtotal) + parseFloat(opplineitems[j].tax);
								component.set("v.showtodaytotal", true);
								component.set("v.totalToday", todtotal.toFixed(2));

							}



						}
						component.set("v.OppLineItem", opplineitems);
						component.set("v.taxOppLineItem", taxOppLineItem);
						component.set("v.addedMemTotal", addedmemtotal.toFixed(2));

					});
					$A.enqueueAction(action);
					break;
				}
			}
		} else {
			component.set("v.OppLineItem", opplineitems);
			component.set("v.addedMemTotal", addedmemtotal.toFixed(2));

		}
	},

	selectLean: function (component, event, helper) {
		var idx = event.target.getAttribute("data-index");
		console.log("selectLean", idx);
		var prod = component.get("v.productThree");
		var alpro = component.get("v.Allproducts");
		var changeMonth = event.getSource();
		var val = changeMonth.get("v.label");
		for (var i = 0; i < prod.length; i++) {
			if (idx == prod[i].Id) {
				prod[i].showProduct = false;
				for (var j = 0; j < prod[i].pwList.length; j++) {
					if (val == prod[i].pwList[j].numOfBags) {
						prod[i].Id = prod[i].pwList[j].Id;
						prod[i].Name = prod[i].pwList[j].Name;
						prod[i].totalPrice = prod[i].pwList[j].totalPrice;
						prod[i].taxCode = prod[i].pwList[j].taxCode;
						if (
							prod[i].pwList[j].numOfBags == 3 &&
							prod[i].title == "GREENS CLUB"
						) {
							prod[i].originalCost = (137.95).toFixed(2);
						} else {
							prod[i].originalCost = (
								57.95 * prod[i].pwList[j].numOfBags
							).toFixed(2);
						}
						prod[i].image = prod[i].pwList[j].image;
						prod[i].planId = prod[i].pwList[j].planId;
						break;
					}
				}
			}
		}

		component.set("v.productThree", prod);
		helper.getAll(component, event, helper);
		component.set("v.showProduct", false);
	},
	addCustomProds: function (component, event, helper) {
		component.set("v.Spinner", true);
		var mapProd = [{}];
		var names = ["burn", "lean", "fuel", "rest", "recover"];
		for (var i = 0; i < names.length; i++) {
			var prd = component.find(names[i]);
			mapProd.push({
				Name: names[i],
				quantity: prd.get("v.value")
			});
		}
		var action = component.get("c.getCustomProduct");
		action.setParams({
			mapProd: JSON.stringify(mapProd)
		});
		action.setCallback(this, function (response) {
			component.set("v.Spinner", false);
			var state = response.getState();

			var addedmemtotal = component.get("v.addedMemTotal");
			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				var prod = component.get("v.productThree");
				var opplineitems = component.get("v.OppLineItem");
				var prodOne = component.get("v.productOne");
				var todtotal = component.get("v.totalToday");
				console.log("addCustomProds", prod);
				if (result.Id != null && result.Id != undefined) {
					component.set("v.errorProd", false);
					opplineitems.push(result);
					addedmemtotal = parseFloat(addedmemtotal) + result.totalPrice;
					for (var j = 0; j < prod.length; j++) {
						if (prod[j].Id == "customtrans") {
							prod.splice(j, 1);
							break;
						}
					}
					if (!prodOne.includes(result)) {
						todtotal = parseFloat(todtotal) + parseFloat(result.totalPrice);
						component.set("v.showtodaytotal", true);
						component.set("v.totalToday", todtotal.toFixed(2));
						console.log("notin prodOne");
					} else {
						console.log("in prodOne");
					}
					component.set("v.productThree", prod);
					component.set("v.OppLineItem", opplineitems);
					component.set("v.showCard", true);
					component.set("v.addedMemTotal", addedmemtotal.toFixed(2));
					var a = component.get("c.closemodal");
					$A.enqueueAction(a);
				} else {
					component.set("v.errorProd", true);
					component.set(
						"v.errorMsg",
						"This Product Combination is not Available Right now!"
					);
				}
			}
		});
		$A.enqueueAction(action);
	},
	finalizeMembership: function (component, event, helper) {
		component.set("v.Spinner", true);
		var cmpTarget = component.find(component.get("v.selectedModal"));
		var cmpBack = component.find("ModalbackdropCancel");
		$A.util.removeClass(cmpBack, "slds-backdrop--open");
		$A.util.removeClass(cmpTarget, "slds-fade-in-open");
		var recordId = component.get("v.recordId");
		var card = component.get("v.cardList")[component.get("v.cardidx")];
		console.log("finalizeMembership ", card.Stripe_Card_Id__c);
		var cancelprods = component.get("v.canceledProducts");
		var addprods = component.get("v.addedProducts");

		for (var i = 0; i < addprods.length; i++) {
			if (addprods[i].Id == "customtrans") {
				addprods[i].Id = component.get("v.prodidx");
				console.log("finalizeMembership ", addprods[i].Id);
			}
		}
		var action = component.get("c.createPayment");
		action.setParams({
			canceledProds: JSON.stringify(cancelprods),
			addedProds: JSON.stringify(addprods),
			cardId: card.Stripe_Card_Id__c
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			var msg =
				"Unknown Error While personalizing your Memberships. Try again Later or Contact Support.";
			var msgType = "error";
			console.log("finalizeMembership ", state);

			if (state == "SUCCESS") {
				var result = response.getReturnValue();
				console.log("finalizeMembership ", result);
				window.scrollTo(0, 0);
				if (result == "success") {
					msg = "Successfully Personalized Your Memberships";
					msgType = "success";
				} else {
					msg = result;
				}
			}
			component.set("v.message", msg);
			component.set("v.messageType", msgType);
			$A.util.removeClass(component.find("toastModel"), "slds-hide");
			$A.util.addClass(component.find("toastModel"), "slds-show");
			let scrollObj = document.getElementById("scrollId");
			if (scrollObj) {
				console.log("height============" + scrollObj.textContent);
				//scrollObj.scrollIntoView(true)
				scrollObj.scrollIntoView({
					behavior: "smooth",
					block: "start",
					inline: "start"
				});
			}
			console.log("First step;- fire event");

			if (result == "success") {
				var refreshevent = $A.get("e.c:RefreshSubscriptions");
				refreshevent.setParams({
					data: "ShowSubscriptions"
				});
				refreshevent.fire();
				setTimeout(function () {
					$A.util.addClass(component.find("toastModel"), "slds-hide");
					component.set("v.message", "");
					component.set("v.messageType", "");
				}, 5000);
			} else {
				component.set("v.Spinner", false);
			}
		});
		$A.enqueueAction(action);
	},
	resetProds: function (component, event, helper) {
		component.set("v.Spinner", true);
		var a = component.get("c.init");
		$A.enqueueAction(a);
	},
	onSelectCard: function (component, event, helper) {
		console.log("onSelectCard ", component.get("v.cardidx"));
		var card = component.get("v.cardList")[component.get("v.cardidx")];
		component.set("v.cardLast4", card.Last4__c);
	},
	closemodal: function (component, event, helper) {
		var selectedModal = component.get("v.selectedModal");
		console.log("openModal", selectedModal);
		var cmpTarget = component.find(selectedModal);
		var cmpBack = component.find("Modalbackdrop");
		$A.util.removeClass(cmpBack, "slds-backdrop--open");
		$A.util.removeClass(cmpTarget, "slds-fade-in-open");
	},
	splitProd: function (component, event, helper) {
		component.set("v.customProductSet", false);
		var names = ["burn", "lean", "fuel", "rest", "recover"];
		var prodName = event.getSource().get("v.name");
		var prod = component.find(prodName);
		var totalProds = 0;
		for (var i = 0; i < names.length; i++) {
			var prod1 = component.find(names[i]);
			totalProds += parseFloat(prod1.get("v.value"));
		}
		if (totalProds > 5) {
			component.set("v.errorProd", true);
			component.set("v.errorMsg", "Only Five Products Can be Choosen");
			var int = totalProds - prod.get("v.value");
			prod.set("v.value", 5 - int);
			component.set("v.customProductSet", true);
		} else {
			component.set("v.errorProd", false);
		}

		console.log("splitProd", totalProds);
		if (totalProds == 5) {
			component.set("v.customProductSet", true);
		}
	},
	goToMembership: function (component, event, helper) {
		console.log("First step;- fire event");
		component.set("v.Spinner", true);
		var refreshevent = $A.get("e.c:RefreshSubscriptions");
		refreshevent.setParams({
			data: "SubscriptionsPage"
		});
		refreshevent.fire();
	},
	getCards: function (component, event, helper) {
		helper.getCard(component, event, helper);
		component.set("v.refreshCard", false);
	}
});