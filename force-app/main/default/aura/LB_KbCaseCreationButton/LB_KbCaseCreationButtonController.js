({    
    handleShowModal: function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:LB_KbCaseCreation", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "How can our amazing support team help you today? \n Please Submit Your Request Below.",
                                      
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "mymodal",
                                       closeCallback: function() {
                                       }
                                   })
                                   
                               }
                               
                           });
    }
})