({
	init : function(component, event, helper) {
        console.log('init');
        var caseId = component.get("v.recordId");
        console.log("caseId---"+caseId);
        
        var action2 = component.get("c.getCaseRecord"); 
        action2.setParams({ caseId :caseId});
        action2.setCallback(this, function(response) {
            debugger
            console.log(response.getReturnValue());
            component.set("v.cas", response.getReturnValue());
        });
        $A.enqueueAction(action2);
    },
    
     rollbackOpp: function( cmp, apexAction, params ) {
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action                          = cmp.get("c."+apexAction+"");
            action.setParams( params );
            action.setCallback( this , function(callbackResult) {
                if(callbackResult.getState()=='SUCCESS') {
                    resolve( callbackResult.getReturnValue() );
                }
                if(callbackResult.getState()=='ERROR') {
                    console.log('ERROR', callbackResult.getError() ); 
                    reject( callbackResult.getError() );
                }
            });
            $A.enqueueAction( action );
        }));            
        return p;
    },
    
    createOrder: function(component, event,cas,AddRemoveDaysval,fieldNameForDaysVal){
        console.log('=----cas==='+JSON.stringify(cas));
        console.log('=----cas==='+AddRemoveDaysval);
        this.rollbackOpp(component,'saveCase',{"cas": cas,AddRemoveDays : AddRemoveDaysval,fieldNameForDays : fieldNameForDaysVal })
        .then(function(result){
            if(result != null){
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result);
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            }
           
             
        });
        
    },
    //for green checkbox
    updateCase: function(component, event,cas,AddRemoveDaysval,fieldNameForDaysVal){
        console.log('=----cas==='+JSON.stringify(cas));
        console.log('=----cas==='+AddRemoveDaysval);
        
        var action = component.get("c.updateCaseStatusClosed");
        action.setParams({ cas: cas,
                          AddRemoveDays : AddRemoveDaysval,
                          fieldNameForDays:fieldNameForDaysVal});
            action.setCallback(this ,function(result) {
                debugger
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result.getReturnValue());
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            });
            $A.enqueueAction(action);
        
        
       /* this.rollbackOpp(component,'updateCaseStatusClosed',{"cas": cas,AddRemoveDays : AddRemoveDaysval })
        .then(function(result){
            if(result != null){
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result);
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            }*/
           
             
       
        
    },
    //for 1st column checkbox
     updateCheck: function(component, event,cas,AddRemoveDaysval,fieldNameForDaysVal){
        console.log('=----cas==='+JSON.stringify(cas));
        console.log('=----cas==='+AddRemoveDaysval);
          var action = component.get("c.updateCase");
        action.setParams({ cas: cas,
                          AddRemoveDays : AddRemoveDaysval,
                          fieldNameForDays:fieldNameForDaysVal});
            action.setCallback(this ,function(result) {
                debugger
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result.getReturnValue());
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            });
            $A.enqueueAction(action);
        
       
        /*this.rollbackOpp(component,'updateCase',{"cas": cas,AddRemoveDays : AddRemoveDaysval,fieldNameForDays : fieldNameForDaysVal })
        .then(function(result){
            if(result != null){
                console.log('result====2'+JSON.stringify(result))
                component.set("v.cas", result);
                $A.get('e.force:refreshView').fire(); 
				//this.init(component, event, helper);
            }
           
             
        });*/
        
    },


})