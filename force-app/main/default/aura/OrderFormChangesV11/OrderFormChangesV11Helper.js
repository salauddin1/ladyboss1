({
    
    getInitValues : function(component, event, recordId) {
        var action = component.get("c.getOpportunityDetails");
        var address = {};
        component.set("v.addAddress",address);
        component.set("v.productOptions",address);
        action.setParams({"conId": recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back Result--->',result);
                console.log('result address',result.address);
                console.log('resut error',result.addressError);
                component.set("v.contact",result);
                component.set("v.addressError",result.addressError);
                if(result.addressError==true){
                    console.log('updateCon',component.get("v.updateCon"));
                    component.set("v.updateCon", true);
                    console.log('updateCon',component.get("v.updateCon"));
                }
                component.set("v.addressErrorMessage",result.addressErrorMessage);
                //component.set("v.cancelDays", result.)
                var subscribedProductList = result.subscribedProductList;
                var productList = result.productWrapperList;
                console.log('productlist', result.productWrapperList);
                for(var j=0;j<productList.length;j++){
                        console.log('Cancel ',productList[j]);
                    if(productList[j].autoCancelDays){
                        console.log('Cancel days',productList[j].autoCancelDays);
                        component.set("v.cancelDays", true);
                        //var cardNumber = cardsList[i].Credit_Card_Number__c;
                        //cardsList[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                    }
                    
                }
                if(subscribedProductList)
                    component.set("v.subscribedProductList",subscribedProductList);
                var cards = result.cardList; 
                cards.unshift({"Credit_Card_Number__c" : "--- Select Card ---"});
                console.log(cards);
                component.set("v.card",cards);
                var cardsList = component.get("v.card");
                console.log('cards List array',cardsList);
                for(var i=1;i<cardsList.length;i++){
                    if(cardsList[i].Credit_Card_Number__c){
                        console.log('cardsList Number',cardsList[i].Credit_Card_Number__c);
                        var cardNumber = cardsList[i].Credit_Card_Number__c;
                        cardsList[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                    }
                }
                
                component.set("v.card",cardsList);
                
                if(result.address){
                    var displayAddress = result.address.Shipping_Street__c+','+result.address.Shipping_City__c+','+result.address.Shipping_State_Province__c+',';
                    displayAddress+=result.address.Shipping_Country__c+','+result.address.Shipping_Zip_Postal_Code__c+'.';
                    console.log('+-++0',displayAddress);
                    component.set("v.displayAddress",displayAddress);
                    component.set("v.showSameAsShippingCheckbox",true);
                    if(displayAddress!=''||displayAddress!=null){
                        component.set("v.contactHasPrimaryAddress",true);
                    }
                }
                component.set("v.expMonthsList",result.allPickListWrapperList[0].pickListField);
                component.set("v.expYearsList",result.allPickListWrapperList[1].pickListField);
                component.set("v.cardTypeList",result.allPickListWrapperList[2].pickListField);
                console.log('card type list returned',component.get("v.cardTypeList"));
                component.set("v.selectedCard", "--- Select Card ---");
                component.set("v.contactId", result.ContactId);
                component.set("v.productOptions", result.productWrapperList);
                component.set("v.productOption", result.productWrapperList);
                console.log('Contact ID -->',component.get("v.contactId"));
                console.log('Products--->',component.get("v.productOptions"));    
                component.set("v.discount",result.discount);
                console.log('discount',component.get("v.discount"));
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
                console.log('in do init',component.get("v.addAddress"));
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
        
        var actiontogetUser = component.get("c.getUserDetails");
        actiontogetUser.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue();
                console.log("result---",result);
                component.set("v.salesPerson",result);
            }
        });
        $A.enqueueAction(actiontogetUser);
        
        var actiontogetUserList = component.get("c.getUserList");
        actiontogetUserList.setParams({"userFieldValue": "available_for_coaching_products__c"});
        actiontogetUserList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
                var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userList",result);
            }
        });
        $A.enqueueAction(actiontogetUserList);
        
        var actiontogetUserDigitalList = component.get("c.getUserList");
        actiontogetUserDigitalList.setParams({"userFieldValue": "Available_For_Credit__c"});
        actiontogetUserDigitalList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
                /*var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);*/
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userCreditList",result);
            }
        });
        $A.enqueueAction(actiontogetUserDigitalList);
        var actiontogetUserkickstartList = component.get("c.getUserList");
        actiontogetUserkickstartList.setParams({"userFieldValue": "Available_for_Coaching_Product_Sales__c"});
        actiontogetUserkickstartList.setCallback(this, function(responsetogetUserList) {
            var state = responsetogetUserList.getState();
            if (state === 'SUCCESS') {
                var result = responsetogetUserList.getReturnValue();
                var otherObj = {};
                otherObj.Id = 'Other';
                otherObj.Name = 'Other';
                result.push(otherObj);
                var nullObj = {};
                nullObj.Id = null;
                nullObj.Name = 'None';
                result.unshift(nullObj);
                console.log("result---",result);
                component.set("v.userkickstartList",result);
            }
        });
        $A.enqueueAction(actiontogetUserkickstartList);
        
        
        this.refreshACHList(component,event,recordId,'c.ACHVerifiedListServer');
        this.refreshACHList(component,event,recordId,'c.ACHNonVerifiedListServer');
    },
    
    setTotalPrice :  function(component, event) {
        console.log('totalpricehelper');
        var totalAmount = 0.00;
        var totalTax = 0.00;
        var discount = component.get("v.discount");
        var prodItems = component.get("v.productOptions");
        var totalPrice = 0.00;
        var amountToPay = 0.00;
        var totalClubbedValue = 0;
        var totalNonClubbedValue = 0;
        var monthTotalMap ={};
        var totalvalue = [];
        var postalCode = component.get("v.contact.address.Shipping_Zip_Postal_Code__c");
        var line1 = component.get("v.contact.address.Shipping_Street__c");
        var city = component.get("v.contact.address.Shipping_City__c");
        var region = component.get("v.contact.address.Shipping_State_Province__c");
        var country = component.get("v.contact.address.Shipping_Country__c");
        var clubItemTax = 0.00;
        var amountToPayFromCard1 = 0.00;
        var amountToPayFromCard2 = 0.00;
        var totalShipping = 0.00;
        var totalPriceOne = 0.00;
        var prodsel =[];
        var prodForTax=[];

        //console.log('productOptionsOne',prodOneItems );
        var selectedCardList = component.get("v.selectedCardList"); 
        
        
        for(var i = 0; i < prodItems.length; i++){
            if(prodItems[i].checkValue == true){
                
                if(prodItems[i].quantity == 0){
                    prodItems[i].quantity = 1;
                }
               console.log('totalPriceWithMonths',prodItems[i].taxCode);
               
                totalPrice += prodItems[i].price * prodItems[i].quantity;
                
                if(prodItems[i].checkedForClub == true || prodItems[i].standAloneClubProd == true){
                    totalClubbedValue += prodItems[i].quantity * prodItems[i].price;    
                }else{
                    totalNonClubbedValue += prodItems[i].quantity * prodItems[i].price;
                }
                
                
                
            }
        } 
      
        
        
        console.log('totalClubbedValue',totalClubbedValue);
        console.log('totalNonClubbedValue',totalNonClubbedValue);
        console.log('totalPriceWithMonths',totalPrice);
        console.log('prodsel',prodsel);
        
        
        component.set("v.totalClubbedValue",totalClubbedValue.toFixed(2));
        component.set("v.totalNonClubbedValue",totalNonClubbedValue.toFixed(2));
        
       

        


var clubTax=0.00;
var prodIdList=[];

var results=[];
var result2=[];
        var amountToPay = totalPrice;
        for(var i = 0; i < prodItems.length; i++){
            if(prodItems[i].checkValue == true){
                prodIdList.push(prodItems[i].Id);
                var prodId=prodItems[i].Id;
                 var action = component.get('c.getTax1');

                action.setParams({
                    products: JSON.stringify(prodItems[i]),
                    line1: line1,
                    city: city,
                    region: region,
                    country: country,
                    postalCode: postalCode
                });

                action.setCallback(this, function(response){
                    var state = response.getState();
                    
                    console.log('State',state);
                    if(component.isValid() && state == "SUCCESS" ){
                        var result = response.getReturnValue();
                        var salesTax = result.tax;
                        console.log('salestax',salesTax);
                        var taxPercentage=0.00;
                        if(salesTax == 0 ){
                            taxPercentage = 0.00;
                        }else{
                           taxPercentage= (salesTax/result.totalPrice)*100; 
                        }
                        amountToPay +=  salesTax;
                        if(totalClubbedValue==0.00){
                            clubItemTax = 0;
                            component.set("v.salesTaxPercentage",result.taxPercentage);
                           
                        }else{
                            if(result.checkedForClub==true || result.standAloneClubProd==true){
                                    clubItemTax = totalClubbedValue+salesTax;
                                    clubTax += salesTax
                                    component.set("v.salesTaxPercentage",result.taxPercentage);
                                }
                                
                            
                        }
                        if(result.checkedForClubCard==true&&result.checkedForClub==false && result.standAloneClubProd==false){
                            amountToPayFromCard1 += result.priceWithTax;
                        }else if(result.checkedForClubCard==false&&result.checkedForClub==false && result.standAloneClubProd==false){
                            amountToPayFromCard2 += result.priceWithTax;
                        }

                        
                        
                        totalTax+=salesTax;
                        console.log('totalTax',totalTax);
                        //console.log('salestax',salesTax);
                        
                        results.push(result);
                        component.set("v.productOptionsTwo",results); 

                      
                        
                        component.set("v.amountToPay",parseFloat(amountToPay).toFixed(2));
                        console.log("v.productOptionsTwo",component.get("v.productOptionsTwo"));
                        component.set("v.ClubbedTax", clubTax);
                        component.set("v.ClubbedTaxValue", clubItemTax);
                        component.set("v.totalSalesTax",totalTax.toFixed(2));
                        
                        component.set("v.amountToPayFromCard1",amountToPayFromCard1.toFixed(2));
                        component.set("v.amountToPayFromCard2",amountToPayFromCard2.toFixed(2));
                        this.shippingCalculation(component,event,helper);

                        
                    }
                });
                $A.enqueueAction(action);
            }
                component.set("v.productOptionsTwo",results);
                        
                        component.set("v.totalShipping", totalShipping.toFixed(2));
                        //console.log("v.productOptionsTwo",component.get("v.productOptionsOne"));
                        component.set("v.ClubbedTax", clubTax);
                        component.set("v.ClubbedTaxValue", clubItemTax);
                        component.set("v.totalSalesTax",totalTax.toFixed(2));
                        component.set("v.amountToPay",amountToPay.toFixed(2));
                        component.set("v.amountToPayFromCard1",amountToPayFromCard1.toFixed(2));
                        component.set("v.amountToPayFromCard2",amountToPayFromCard2.toFixed(2));
        }
        console.log('amountToPayFromCard2' + component.get('v.amountToPayFromCard2'));
        component.set("v.totalPriceValue",totalPrice.toFixed(2));
        var p = component.get("v.productOptionsTwo");
        console.log("++--p--++",p);
    
         
    },
    shippingCalculation :function (component, event,helper) {
        var prodID = [];
        var conID = component.get("v.recordId");
        console.log('conID',conID);
        var amountToPay = parseFloat(component.get("v.amountToPay"));
        console.log('amountToPay',amountToPay);
        var amountToPayFromCard1 = parseFloat(component.get("v.amountToPayFromCard1"));
        var prod= component.get("v.productOptions");
        var totalClubbedValue = parseFloat(component.get("v.totalClubbedValue"));
        var clubItemShipping =parseFloat(component.get("v.ClubbedTaxValue"));
        for(var i = 0; i < prod.length; i++){
            if(prod[i].checkValue == true){
                console.log("prod[i].quantity", prod[i].quantity);
                for(var j=0; j<prod[i].quantity;j++){
                    prodID.push(prod[i]);
                }
            }
        }
        console.log("prod[i].quantity", prodID);
        if(prodID.length>0){
            var action2 = component.get('c.getShipping');
            action2.setParams({
                prodID: JSON.stringify(prodID)
            });
            action2.setCallback(this, function(response){
                var state = response.getState();
                console.log('state--',state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();

                    console.log("resultship---",result);
                    amountToPay += result;
                    component.set("v.totalShipping", result);
                    component.set("v.amountToPay",parseFloat(amountToPay).toFixed(2));
                    
                        amountToPayFromCard1 += result;
                     
                    component.set("v.ClubbedTaxValue", clubItemShipping);
                    console.log("resultship---",amountToPay);
                    component.set("v.amountToPayFromCard1",parseFloat(amountToPayFromCard1).toFixed(2));
                    this.splitPayment(component,event,helper);
            }
            });
            $A.enqueueAction(action2);
        }else{
            component.set("v.totalShipping", 0);
        }
            /*var shippingCost=0;
            var con = component.get("v.contact");
            var shippingName = con.FirstName + con.LastName;
            var email = con.Email;
            var phone = con.Phone;
            var shippingStreet=con.address.Shipping_Street__c;
            var shippingCity = con.address.Shipping_City__c;
            var shippingPostalCode= con.address.Shipping_Zip_Postal_Code__c;
            var shippingCountry = con.address.Shipping_Country__c;

            var action2 = component.get('c.getShipping1');
            action2.setParams({
                shippingName:shippingName,
                shippingStreet:shippingStreet,
                shippingCity:shippingCity,
                shippingPostalCode:shippingPostalCode,
                shippingCountry:shippingCountry,
                email:email,
                phone:phone,
                prodID: prodID
            });
            action2.setCallback(this, function(response){
                var state = response.getState();
                console.log('state--',state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    component.set("v.shippingMethods",result.rateWrapperList);
                    console.log("resultship---",result.rateWrapperList);
                    var shiprates = result.rateWrapperList;
                    for(var i = 0; i < shiprates.length; i++){
                        if(shiprates[i].carrier=='USPS'&&shiprates[i].service=='Priority'){
                          shippingCost = parseFloat(shiprates[i].rate);
                          shiprates[i].defaultShip=true;
                          component.set("v.selectedShippingMethod",shiprates[i]);
                          console.log('inside if',shiprates[i]);
                        }
                    }
                    amountToPay += shippingCost;
                    component.set("v.totalShipping", shippingCost);
                    component.set("v.amountToPay",parseFloat(amountToPay).toFixed(2));
                    
                        amountToPayFromCard1 += shippingCost;
                     
                    component.set("v.ClubbedTaxValue", clubItemShipping);
                    console.log("resultship---",amountToPay);
                    component.set("v.amountToPayFromCard1",parseFloat(amountToPayFromCard1).toFixed(2));
                    this.splitPayment(component,event,helper);
            }
            });
            $A.enqueueAction(action2);*/
    },
    splitPayment: function (component, event,helper) {
        //helper.setTotalPrice(component, event);
        var changeQuantity = event.getSource();
        var val =  parseFloat(changeQuantity.get("v.value"));
        
        if(!val){
            val = 0;   
        }
        
        console.log('val' + val);
        console.log('totalNonClubbedValue' + component.get('v.totalNonClubbedValue'));
        console.log('totalNonClubbedValue' + component.get('v.amountToPayFromCard1'));
        console.log('totalNonClubbedValue' + component.get('v.amountToPayFromCard2'));
        var cardDetail = changeQuantity.get("v.name");
        var selectedOption = component.get("v.selectedCardList"); 



        
        for(var i in selectedOption){
            
           
            if(i==0){
                selectedOption[0].nonClubamount = component.get('v.totalNonClubbedValue');
            }
            if (i == 1) {
                selectedOption[1].nonClubamount = 0;
            }
           
        }
        component.set('v.selectedCardList',selectedOption);
    },
    getContactOptions: function(component,event) {
        var action = component.get('c.getContacts');
        var search = component.get("v.inputContact");
        action.setParams({
            searchText: search
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ) {
                var results = [];
                var predictions = response.getReturnValue();
                if(predictions.length > 0) {
                    for (var i = 0; i< predictions.length; i++) {
                        results.push(predictions[i]);
                    }
                    console.log('contact results-->',results);
                    component.set("v.isRenderContactResults",true);
                    component.set("v.contactOptions",results); 
                    
                }else{
                    console.log('Contact Query Error');
                    component.set("v.isRenderContactResults",false);
                }
            }else if(state == "ERROR" ){
                var errors = data.getError();
                console.log('contact errors-->',errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleClub: function(component,event,source) {
        console.log('entered handleclub');
        var changeValue = event.getSource();
        var index = component.get("v.indexVar");
        console.log('club index ',index);
        var productList = component.get("v.productOptions");
        var selectedOption = component.get("v.productOptions")[index]; 
        var subscribedProductList = component.get("v.subscribedProductList");
        var isSwitchedProductSelectable = true;
        if(subscribedProductList){
            for(var i=0;i<subscribedProductList.length;i++){
                if(subscribedProductList[i]==selectedOption.switchedProductId){
                    console.log('inside found switched list');
                    isSwitchedProductSelectable = false;
                    break;
                }
            }
        }
        console.log('club changed ',selectedOption);
        var discount = component.get("v.discount");
        var price = selectedOption.price * selectedOption.quantity;
        console.log('price value',price);
        var productName = selectedOption.Name;
        var productPrice = selectedOption.price;
        var productId = selectedOption.Id;
        var productClub = selectedOption.club;
        var productQuantitySelector = selectedOption.quanititySelector;
        var autoCancelDays = selectedOption.autoCancelDays;
        
        //we can add our logic to switch product according to month here
        console.log('selectedOption.checkedForClub' + selectedOption.checkedForClub);
        console.log('selectedOption.productsToSwitchMap'+selectedOption.productsToSwitchMap);
        console.log('selectedOption.months'+selectedOption.months);
        if(selectedOption.productsToSwitchMap && selectedOption.productsToSwitchMap.hasOwnProperty(selectedOption.months)){
            if(selectedOption.productsToSwitchMap[selectedOption.months].isSelectable==false){
                ///show error
                component.set("v.errorMsg",selectedOption.productsToSwitchMap[selectedOption.months].Name + ' already subscribed');
                component.set("v.isErrorForProduct",true);
                component.set("v.productErrorIndex",index);
                selectedOption.checkedForClub = false;
            }else{
                selectedOption.Name = selectedOption.productsToSwitchMap[selectedOption.months].Name;
                selectedOption.price = selectedOption.productsToSwitchMap[selectedOption.months].price;
                selectedOption.Id = selectedOption.productsToSwitchMap[selectedOption.months].Id;
                selectedOption.isSelectable = selectedOption.productsToSwitchMap[selectedOption.months].isSelectable;
                selectedOption.quanititySelector = selectedOption.productsToSwitchMap[selectedOption.months].quanititySelector;
                selectedOption.autoCancelDays = selectedOption.productsToSwitchMap[selectedOption.months].autoCancelDays;
                component.set("v.isErrorForProduct",false);
            }
        }
        //selectedOption.isSelectable = isSwitchedProductSelectable;
        console.log('inside handle club hanlder method, selectedOption' + selectedOption);
        if(selectedOption.isSelectable==false){
            console.log('make check false');
            selectedOption.checkValue = false;
            var checkedProductList = component.get("v.checkedProductList");
            checkedProductList.pop();
            component.set("v.checkedProductList",checkedProductList);
        }
        productList[index] = selectedOption;
        component.set("v.productOptions",productList);
        console.log('in handle club',selectedOption);
        var selectedProdOne = [];
        var prodItems2 = component.get("v.productOptions");
        console.log('prodItem',prodItems2);
        for(var i = 0; i < prodItems2.length; i++){
            console.log('prodItems',prodItems2[i]);
            
            if(prodItems2[i].checkValue == true  && prodItems2[i].checkedForClub==false && prodItems2[i].standAloneClubProd==false){
                prodItems2[i].checkedForClubCard = true;
                console.log(prodItems2[i].checkValue);
                selectedProdOne.push(prodItems2[i]);
            }            
        }
        component.set("v.productOptionsOne",selectedProdOne);
    },
    
    getSelectedContact: function(component, event, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var action = component.get('c.getContactDetails');
        console.log('call back getSelectedContact--->',contactId);
        action.setParams({
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('call back getSelectedContact--->',result);
                console.log('result getSelectedContact',result.address);
                console.log('resut getSelectedContact',result.addressError);
                component.set("v.contact",result);
                component.set("v.addressError",result.addressError);
                if(result.addressError==true){
                    console.log('updateCon',component.get("v.updateCon"));
                    component.set("v.updateCon", true);
                    console.log('updateCon',component.get("v.updateCon"));
                }
                component.set("v.addressErrorMessage",result.addressErrorMessage);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            } else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateContactInfo: function(component, event,helper) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var contactDetails = JSON.stringify(component.get("v.contact"));
        var conDetails = component.get("v.contact");

        console.log('in helper updatre info contact Details---> ',contactDetails);
        var action = component.get('c.updateContactDetails');
        action.setParams({
            contactDetails : contactDetails
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" ){

                var result =response.getReturnValue();
                component.set("v.addressError", result.addressError);
                component.set("v.addressErrorMessage", result.addressErrorMessage);
                  if(result.address){
                    var displayAddress = result.address.Shipping_Street__c+','+result.address.Shipping_City__c+','+result.address.Shipping_State_Province__c+',';
                    displayAddress+=result.address.Shipping_Country__c+','+result.address.Shipping_Zip_Postal_Code__c+'.';
                    
                    component.set("v.displayAddress",displayAddress);
                    component.set("v.showSameAsShippingCheckbox",true);
                    component.set("v.contactHasPrimaryAddress",true);
                }
                 if(result.addressError==true){
                    console.log('updateCon',component.get("v.updateCon"));
                    component.set("v.updateCon", true);
                    console.log('updateCon',component.get("v.updateCon"));
                }else{
                    component.set("v.updateCon",false);
                    var chkBox = component.find("updateConCheck").set("v.checked",false);
                    console.log('chkBox',chkBox);
                }
                var updatec = component.get("v.updateCon");
                console.log('updateCon',component.get("v.updateCon"));
                
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                this.sendToVF(component, event, helper);
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);
        
       

    },
    
    createAddress: function(component, event, address, contactId,isPrimary,contactDetails) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var addressDetails = JSON.stringify(address);
        console.log('in helper updatre info ---> ',addressDetails);
        var action = component.get('c.createNewAddress');
        action.setParams({
            addressDetails : addressDetails,
            contactId: contactId,
            isPrimary : isPrimary,
            contactDetails: contactDetails
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                var result =response.getReturnValue();
                component.set("v.addressError", result.addressError);
                component.set("v.addressErrorMessage", result.addressErrorMessage);
                console.log('newAddressCheck',result.addressError);
                if(result.addressError==true){
                    console.log('updateCon',component.get("v.updateCon"));
                    component.set("v.updateCon", true);
                    console.log('updateCon',component.get("v.updateCon"));
                }else{
                    component.set("v.updateCon", false);
                    var chkBox = component.find("newAddressCheck")[0].set("v.checked",false);
                    console.log('chkBox',chkBox);
                }
                component.set("v.newAddress",false);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //if(component.get("v.isPrimaryAddress")){
                if(true){
                    component.set("v.contactHasPrimaryAddress",true);
                    console.log('inside primary address change');
                    this.refreshPrimaryAddress(component,event,contactId);
                }
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        


        $A.enqueueAction(action);


        
    },
    newCardWithBilling: function(component, event, cardDetails, contactId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Details info ---> ',cardDetailString);
        var action = component.get('c.createCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                console.log('Card Created');
                component.set("v.newCard",false);
                component.find("newCardChkBox").set("v.checked",false);
                var intervalInMilliseconds = 5000;
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.visible", true);
                    }), intervalInMilliseconds
                );
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    
    valid_credit_card: function(value) {
        // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value)) return false;
        
        // The Luhn Algorithm. It's so pretty.
        var nCheck = 0,
            nDigit = 0,
            bEven = false;
        value = value.replace(/\D/g, "");
        
        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                nDigit = parseInt(cDigit, 10);
            
            if (bEven) {
                if ((nDigit *= 2) > 9)
                    nDigit -= 9;
            }
            
            nCheck += nDigit;
            bEven = !bEven;
        }
        
        return (nCheck % 10) == 0;
    },
    
    updateCardWithBilling: function(component, event, cardDetails, cardId) {
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        component.set("v.isUpdateCardWithError",true);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        console.log('in helper Card Billing Update Details info ---> ',cardDetailString);
        var action = component.get('c.updateCardWithBilling');
        action.setParams({
            CardDetails : cardDetailString,
            cardId: cardId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                component.set("v.cardErrorMsg",'');
                component.set("v.isCardError",false);
                component.set("v.isUpdateCardWithError",true);
                console.log('Card updated');
                component.set("v.newCard",false);
                //component.find("newCardChkBox").set("v.checked",false);
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);        
    },
    newCardWithShipping: function(component, event, cardDetails, address, contactId) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        var cardDetailString = JSON.stringify(cardDetails);
        var addressDetailString = JSON.stringify(address);
        console.log('in helper Card Shipping Details info ---> ',cardDetailString);
        console.log('shipping Address ',addressDetailString);
        var action = component.get('c.createCardWithShipping');
        action.setParams({
            CardDetails : cardDetailString,
            contactId: contactId,
            addressDetails : addressDetailString
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                console.log('Card Created');
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                //  $A.get('e.force:refreshView').fire();                
            }else {
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                console.log('in else ',state);
            }
        });
        $A.enqueueAction(action);   
    },
    rollbackOpp: function( cmp, apexAction, params ) {
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action                          = cmp.get("c."+apexAction+"");
            action.setParams( params );
            action.setCallback( this , function(callbackResult) {
                if(callbackResult.getState()=='SUCCESS') {
                    resolve( callbackResult.getReturnValue() );
                }
                if(callbackResult.getState()=='ERROR') {
                    console.log('ERROR', callbackResult.getError() ); 
                    reject( callbackResult.getError() );
                }
            });
            $A.enqueueAction( action );
        }));            
        return p;
    },
    createShipping :function(component, event,selectedProd,productOptionsOne,payment,sd,cardlist,contact,totalPriceValue,salesPerson,isACHvalue,userr,user,userKick,salesTaxPercentage){
        
        var p = new Promise( $A.getCallback( function( resolve , reject ) { 
        var action = component.get('c.createShipping');
        action.setParams({
            prodID:JSON.stringify(selectedProd),
            conID:component.get("v.recordId")
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            console.log(state);
            if(state == "SUCCESS" ){
                var result = response.getReturnValue();
                console.log('totalshipping in ---->',result);
                 resolve( response.getReturnValue() );
            }
             if(state=='ERROR') {
                     reject( response.getError() );
             }   
        });
          $A.enqueueAction( action );
        })); 

        p.then(this.createOrder(component, event,selectedProd,productOptionsOne,payment,sd,cardlist,contact,totalPriceValue,salesPerson,isACHvalue,userr,user,userKick,salesTaxPercentage));

       },

    createOrder: function(component, event,selectedProd,productOptionsOne,payment,sd,cardlist,contact,totalPriceValue,salesPerson,isACHvalue,userr,user,userKick,salesTaxPercentage){
        //console.log('createOpportunity helper',sd);
        //console.log('stringify ',JSON.stringify(selectedProd));
        var salesTaxPercentage = component.get("v.salesTaxPercentage");
        console.log(salesTaxPercentage);
        component.set("v.Spinner", true);
        console.log('selectedProd-->'+JSON.stringify(selectedProd));
        console.log('selectedProd-->'+selectedProd[0].productWithSite);
        console.log('payment-->'+payment);
        console.log('cardlist-->'+cardlist);
        var recId = component.get("v.recordId");
        /* if(cardlist && cardlist.length==1){
            cardlist[0].usedForClub=true;
            cardlist[0].nonClubamount=component.get('v.totalNonClubbedValue');
        console.log('cardlist-->'+cardlist);
        }*/
        console.log('--------new recId----'+recId);
        console.log('--------new tag----'+isACHvalue);
        
        var sd2;
        if(sd){
            sd2 = sd;
        }
        sd=sd2;
        
        var triggerCharge = false;
        console.log('selectedShippingMethod',component.get("v.selectedShippingMethod"));
        var beforeInsertAction = component.get('c.insertOpportunity');
        beforeInsertAction.setParams({
            salesPerson : salesPerson,
            selectedProd : JSON.stringify(selectedProd),
            contactDetails: JSON.stringify(contact),
            paymentType : payment,
            scheduledDate: sd,
            CardJson : JSON.stringify(cardlist),
            totalPriceValue:totalPriceValue,
            clubbedPriceValue: component.get("v.totalClubbedValue"),
            nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
            calloutNow : triggerCharge, 
            isACHvalue : JSON.stringify(isACHvalue),
            AgentInitializingSale : userr,
            CreditUser : user,
            KickstartUser :userKick,
            salesTaxPercentage : salesTaxPercentage,
            productOptionsOne: JSON.stringify(productOptionsOne),
            isErrorOrderForm: component.get("v.isErrorOrderForm"),
            isAsyncError: component.get("v.isAsyncError")
        });
        beforeInsertAction.setCallback(this,function(beforeInsertActionResponse){
            var state = beforeInsertActionResponse.getState();
            console.log('---result--'+state);
            if(state.toLowerCase() == "success"){
               triggerCharge = true;
            }
            var action = component.get('c.insertOpportunity');
            action.setParams({
                salesPerson : salesPerson,
                selectedProd : JSON.stringify(selectedProd),
                contactDetails: JSON.stringify(contact),
                paymentType : payment,
                scheduledDate: sd,
                CardJson : JSON.stringify(cardlist),
                totalPriceValue:totalPriceValue,
                clubbedPriceValue: component.get("v.totalClubbedValue"),
                nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                calloutNow : triggerCharge,
                isACHvalue : JSON.stringify(isACHvalue),
                AgentInitializingSale : userr,
                CreditUser : user,
                KickstartUser :userKick,
                salesTaxPercentage : salesTaxPercentage,
                productOptionsOne: JSON.stringify(productOptionsOne),
                    isErrorOrderForm: component.get("v.isErrorOrderForm"),
                    isAsyncError: component.get("v.isAsyncError")
            });
            action.setCallback(this, function(response){
                var action2 = component.get('c.deleteShipping');
                var contactId2 =  component.get("v.recordId");
                 action2.setParams({
                     conID: contactId2
                 });
                 action2.setCallback(this, function(response2){
                 
                  });
                 $A.enqueueAction(action2);

                var state = response.getState();
                var orderResp = JSON.stringify(response.getReturnValue());
                console.log('----------orderResp---------'+orderResp);
                console.log('----------triggerCharge-----3----'+triggerCharge);
                
                if(!orderResp || orderResp==null || orderResp=='null'){
                    component.set("v.Spinner", false);
                    component.set("v.errorMsg",'This card failed, please check with your administrator');
                    component.set("v.isError",true);
                    component.set("v.disableOrderButton",true);
                }else if(orderResp && orderResp!= JSON.stringify("Success")){
                    try{
                        if(JSON.parse( response.getReturnValue() )){
                        var jsonDes = JSON.parse( response.getReturnValue() );
                        component.set("v.errorMsg",jsonDes.Error);
                        if(jsonDes.Card != null && jsonDes.Card != '' && jsonDes.CardNumber != null && jsonDes.CardNumber != ''){
                            component.set("v.isOpen", true);
                            component.set("v.chIdValNumber", jsonDes.CardNumber);
                            //component.set("v.chIdVal", jsonDes.Card);
                            component.set("v.chIdVal", jsonDes.Card1);
                            component.set("v.chIdValAmount", jsonDes.AmountSucceeded);
                            component.set("v.chIdValAmountFailed", jsonDes.AmountFailed);
                            component.set("v.chIdOppId", jsonDes.Opportunity);
                        }
                    }
                    }catch(e){

                        component.set("v.errorMsg",orderResp);

                    }
                    
                    component.set("v.Spinner", false);
                    component.set("v.isError",true);
                    component.set("v.isErrorOrderForm",true);
                    
                }else{
                    if(state == "SUCCESS" ){
                        component.set("v.Spinner", false);
                        //component.find("overlayLib").notifyClose();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "A new Order has been created.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                        var allComponentCreated = false;
                        var totalPopupToBeOpen = 0;
                        var totalPopupOpened = 0;
                        var siteToBePoped = [];
                        var siteDupCheck = [];
                        for (var i in selectedProd){
                            console.log('---before---'+selectedProd);
                            console.log('---before---'+selectedProd[i].productWithSite);
                            if(selectedProd[i].productWithSite){
                                
                                if(siteDupCheck.includes(selectedProd[i].productWithSite) === false){
                                    siteDupCheck.push(selectedProd[i].productWithSite);
                                    siteToBePoped.push({'site':selectedProd[i].productWithSite,'product':selectedProd[i].Name});
                                    totalPopupToBeOpen = totalPopupToBeOpen + 1;
                                    console.log('selectedProd[i].productWithSite'+selectedProd[i].productWithSite);
                                }
                            }
                        }
                        console.log(siteToBePoped);
                        for (var i in siteToBePoped){
                            if(siteToBePoped[i]){
                                var modalBody;
                                console.log('lib called'+i+'----site---'+siteToBePoped[i].site);
                                console.log('lib called'+i+'----site---'+siteToBePoped[i].product);
                                $A.createComponent("c:PopUpPageAfterOrder", { recordId : siteToBePoped[i].site , productName:siteToBePoped[i].product, accId:recId},
                                                   function(content, status) {
                                                       if (status === "SUCCESS") {
                                                           modalBody = content;
                                                           component.find('overlayLibPopup').showCustomModal({
                                                               body: modalBody, 
                                                               showCloseButton: true,
                                                               cssClass: "slds-modal_large fullwidth",
                                                               closeCallback: function() {
                                                               }
                                                           })
                                                       }
                                                       totalPopupOpened = totalPopupOpened + 1;
                                                       if(totalPopupToBeOpen==totalPopupOpened){
                                                           component.find("overlayLib").notifyClose();                                   
                                                       }
                                                   }
                                                  );
                            }
                        }
                        if(totalPopupToBeOpen==0){
                            component.find("overlayLib").notifyClose();
                        }
                    } else if (state == "ERROR"){
                        component.set("v.Spinner", false);
                        var errors = response.getError();
                        console.log(errors); 
                        if(JSON.stringify(errors) == null){
                            component.set("v.errorMsg",'Internal Server Error');
                        }else{
                            component.set("v.errorMsg",JSON.stringify(errors));
                        }
                        
                        component.set("v.isError",true);
                    }       
                }
            });
            $A.enqueueAction(action);  
        });
        $A.enqueueAction(beforeInsertAction);  
        /*this.rollbackOpp(component,'insertOpportunity',{ salesPerson : salesPerson,
                                                        selectedProd : JSON.stringify(selectedProd),
                                                        contactDetails: JSON.stringify(contact),
                                                        paymentType : payment,
                                                        scheduledDate: sd,
                                                        CardJson : JSON.stringify(cardlist),
                                                        totalPriceValue:totalPriceValue,
                                                        clubbedPriceValue: component.get("v.totalClubbedValue"),
                                                        nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                                                        calloutNow : triggerCharge, 
                                                        isACHvalue : JSON.stringify(isACHvalue),
                                                        AgentInitializingSale : userr,
                                                        CreditUser : user,
                                                        KickstartUser :userKick,
                                                        salesTaxPercentage : salesTaxPercentage,
                                                        productOptionsOne: JSON.stringify(productOptionsOne)
                                                       })
        .then(function(result){
            var action = component.get('c.insertOpportunity');
            console.log('---result--'+result);
            if(result == "Success"){
                triggerCharge = true;
            }
            action.setParams({
                salesPerson : salesPerson,
                selectedProd : JSON.stringify(selectedProd),
                contactDetails: JSON.stringify(contact),
                paymentType : payment,
                scheduledDate: sd,
                CardJson : JSON.stringify(cardlist),
                totalPriceValue:totalPriceValue,
                clubbedPriceValue: component.get("v.totalClubbedValue"),
                nonClubbedPriceValue: component.get("v.totalNonClubbedValue"),
                calloutNow : triggerCharge,
                isACHvalue : JSON.stringify(isACHvalue),
                AgentInitializingSale : userr,
                CreditUser : user,
                KickstartUser :userKick,
                salesTaxPercentage : salesTaxPercentage,
                productOptionsOne: JSON.stringify(productOptionsOne)
                
            }); 
            action.setCallback(this, function(response){

                var action2 = component.get('c.deleteShipping');
                var contactId2 =  component.get("v.recordId");
                 action2.setParams({
                     conID: contactId2
                 });
                 action2.setCallback(this, function(response2){
                 
                  });
                 $A.enqueueAction(action2);

                var state = response.getState();
                var orderResp = JSON.stringify(response.getReturnValue());
                console.log('----------orderResp---------'+orderResp);
                console.log('----------triggerCharge-----3----'+triggerCharge);
                
                if(!orderResp || orderResp==null || orderResp=='null'){
                    component.set("v.Spinner", false);
                    component.set("v.errorMsg",'This card failed, please check with your administrator');
                    component.set("v.isError",true);
                    component.set("v.disableOrderButton",true);
                }else if(orderResp && orderResp!= JSON.stringify("Success")){
                    try{
                        if(JSON.parse( response.getReturnValue() )){
                        var jsonDes = JSON.parse( response.getReturnValue() );
                        component.set("v.errorMsg",jsonDes.Error);
                        if(jsonDes.Card != null && jsonDes.Card != '' && jsonDes.CardNumber != null && jsonDes.CardNumber != ''){
                            component.set("v.isOpen", true);
                            component.set("v.chIdValNumber", jsonDes.CardNumber);
                            //component.set("v.chIdVal", jsonDes.Card);
                            component.set("v.chIdVal", jsonDes.Card1);
                            component.set("v.chIdValAmount", jsonDes.AmountSucceeded);
                            component.set("v.chIdValAmountFailed", jsonDes.AmountFailed);
                            component.set("v.chIdOppId", jsonDes.Opportunity);
                        }
                    }
                    }catch(e){

                        component.set("v.errorMsg",orderResp);

                    }
                    
                    component.set("v.Spinner", false);
                    component.set("v.isError",true);
                    
                }else{
                    if(state == "SUCCESS" ){
                        component.set("v.Spinner", false);
                        //component.find("overlayLib").notifyClose();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "A new Order has been created.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                        var allComponentCreated = false;
                        var totalPopupToBeOpen = 0;
                        var totalPopupOpened = 0;
                        var siteToBePoped = [];
                        var siteDupCheck = [];
                        for (var i in selectedProd){
                            console.log('---before---'+selectedProd);
                            console.log('---before---'+selectedProd[i].productWithSite);
                            if(selectedProd[i].productWithSite){
                                
                                if(siteDupCheck.includes(selectedProd[i].productWithSite) === false){
                                    siteDupCheck.push(selectedProd[i].productWithSite);
                                    siteToBePoped.push({'site':selectedProd[i].productWithSite,'product':selectedProd[i].Name});
                                    totalPopupToBeOpen = totalPopupToBeOpen + 1;
                                    console.log('selectedProd[i].productWithSite'+selectedProd[i].productWithSite);
                                }
                            }
                        }
                        console.log(siteToBePoped);
                        for (var i in siteToBePoped){
                            if(siteToBePoped[i]){
                                var modalBody;
                                console.log('lib called'+i+'----site---'+siteToBePoped[i].site);
                                console.log('lib called'+i+'----site---'+siteToBePoped[i].product);
                                $A.createComponent("c:PopUpPageAfterOrder", { recordId : siteToBePoped[i].site , productName:siteToBePoped[i].product, accId:recId},
                                                   function(content, status) {
                                                       if (status === "SUCCESS") {
                                                           modalBody = content;
                                                           component.find('overlayLibPopup').showCustomModal({
                                                               body: modalBody, 
                                                               showCloseButton: true,
                                                               cssClass: "slds-modal_large fullwidth",
                                                               closeCallback: function() {
                                                               }
                                                           })
                                                       }
                                                       totalPopupOpened = totalPopupOpened + 1;
                                                       if(totalPopupToBeOpen==totalPopupOpened){
                                                           component.find("overlayLib").notifyClose();                                   
                                                       }
                                                   }
                                                  );
                            }
                        }
                        if(totalPopupToBeOpen==0){
                            component.find("overlayLib").notifyClose();
                        }
                    } else if (state == "ERROR"){
                        component.set("v.Spinner", false);
                        var errors = response.getError();
                        console.log(errors); 
                        if(JSON.stringify(errors) == null){
                            component.set("v.errorMsg",'Internal Server Error');
                        }else{
                            component.set("v.errorMsg",JSON.stringify(errors));
                        }
                        
                        component.set("v.isError",true);
                    }       
                }
                
            });
            $A.enqueueAction(action);   
        });*/
        
    },
    
    showToast : function(component, event) {
        console.log('toast called');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Card has been created successfully."
        });
        toastEvent.fire();
    },
    
    refreshCardList: function(component, event, contactId){
        console.log('card list called');
        console.log('contact Id is ',contactId);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        component.set("v.cardErrorMsg",'');
        component.set("v.isCardError",false);
        var cardAction = component.get('c.cardListServer');
        cardAction.setParams({
            conId: contactId
            
        });
        cardAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside card list');
                console.log('inside resultCard'+response.getReturnValue());
                
                var resultCard = response.getReturnValue();
                if(resultCard.length>0){
                    console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c'+resultCard[0].Stripe_Card_Id__c);
                    console.log('resultCard.length>0 resultCard[0].Error_Message__c'+resultCard[0].Error_Message__c);
                    if(!resultCard[0].Error_Message__c && resultCard[0].Stripe_Card_Id__c){
                        component.set("v.newCard",false);
                        console.log('first card details',resultCard[0]);
                        console.log('result',resultCard);
                        console.log('card error Message',resultCard[0].Error_Message__c);
                        //var cardsList = resultCard;
                        var cardsList = [];
                        console.log('cards List array',cardsList);
                        for(var i=0;i<resultCard.length;i++){
                            if(resultCard[i].Credit_Card_Number__c && resultCard[i].Stripe_Card_Id__c){
                                console.log('cardsList Number',resultCard[i].Credit_Card_Number__c);
                                var cardNumber = resultCard[i].Credit_Card_Number__c;
                                resultCard[i].Credit_Card_Number__c = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                                console.log('credit card number',resultCard[i].Credit_Card_Number__c);
                                cardsList.push(resultCard[i]);
                            }
                        }
                        
                        component.set("v.card",cardsList);
                        console.log('default card from cardsList',cardsList[0].Credit_Card_Number__c);
                        /*component.set("v.defaultCard",cardsList[0]);
                        component.set("v.selectedCard",cardsList[0].Id);
                        var cardNumber = cardsList[0].Credit_Card_Number__c;
                        var displayCardNumber = '********'+ cardNumber.substr((cardNumber.length)-4,(cardNumber.length)-1);
                        component.set("v.displayCardNumber",displayCardNumber);
                        
                        var selectedCardList = component.get("v.selectedCardList");        
                        
                        if(selectedCardList && selectedCardList.length==0){
                            selectedCardList.push({'card':cardsList[0].Id,'nonClubamount':component.get('v.totalNonClubbedValue'), 'usedForClub':true});
                            component.set("v.selectedCardList",selectedCardList);
                        }*/
                        
                    }
                    if(resultCard[0].Error_Message__c && !resultCard[0].Stripe_Card_Id__c){
                        console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c final'+resultCard[0].Stripe_Card_Id__c);
                        console.log('resultCard.length>0 resultCard[0].Error_Message__c final'+resultCard[0].Error_Message__c);
                        this.setCardInEditMode(component,event,resultCard);
                    }
                    
                }
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
            }
        });
        $A.enqueueAction(cardAction);
    },
    
    refreshACHList: function(component, event, contactId,methodName){
        console.log('methodName ',methodName);
        console.log('contact Id is ',contactId);
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        component.set("v.ACHErrorMsg",'');
        component.set("v.isACHError",false);
        var ACHAction = component.get(methodName);
        ACHAction.setParams({
            conId: contactId
            
        });
        ACHAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside resultCard'+response.getReturnValue());
                
                var resultCard = response.getReturnValue();
                if(resultCard.length>0){
                    console.log('resultCard.length>0 resultCard[0].Stripe_Card_Id__c'+resultCard[0].Stripe_ACH_Id__c);
                    console.log('resultCard.length>0 resultCard[0].Error_Message__c'+resultCard[0].Error_Message__c);
                    if(!resultCard[0].Error_Message__c && resultCard[0].Stripe_ACH_Id__c){
                        component.set("v.newACH",false);
                        console.log('first ACH details',resultCard[0]);
                        console.log('result',resultCard);
                        console.log('card error Message',resultCard[0].Error_Message__c);
                        //var cardsList = resultCard;
                        var cardsList = [];
                        console.log('cards List array',cardsList);
                        for(var i=0;i<resultCard.length;i++){
                            if(resultCard[i].Stripe_ACH_Id__c){
                                //var Last4__c = resultCard[i].Last4__c;
                                //resultCard[i].Last4__c = '********'+ i;
                                console.log('credit card number',resultCard[i].Last4__c);
                                cardsList.push(resultCard[i]);
                            }
                        }
                        cardsList.unshift({"Bank_Name__c" : "--- Select ACH ---"});
                        
                        if(methodName == 'c.ACHVerifiedListServer'){
                            component.set("v.isVerifiedACHListEmpty",false);
                            console.log('ACHVerifiedListServer cardsList',cardsList[0].isVerified__c);
                            console.log('ACHVerifiedListServer cardsList',cardsList[0]);
                            component.set("v.ACH",cardsList);
                            component.set("v.defaultACH",cardsList[0]);
                            component.set("v.selectedVerifiedACH",cardsList[0].Id);
                        }else if(methodName == 'c.ACHNonVerifiedListServer'){
                            component.set("v.isNonVerifiedACHListEmpty",false);
                            console.log('ACHNonVerifiedListServer cardsList',cardsList[0].isVerified__c);
                            component.set("v.nonVerifiedACH",cardsList);
                            component.set("v.defaultNonVerifiedACH",cardsList[0]);
                            component.set("v.selectedNACH",cardsList[0].Id);
                            component.set("v.disableVerifyACHButton",false);
                        }
                        component.set("v.selectedACH", "--- Select Card ---");
                        component.set("v.selectedVerifiedACH", "--- Select Card ---");
                        
                        /*var selectedCardList = component.get("v.selectedCardList");        
                        
                        if(selectedCardList && selectedCardList.length==0){
                            selectedCardList.push({'card':cardsList[0].Id,'nonClubamount':component.get('v.totalNonClubbedValue'), 'usedForClub':true});
                            component.set("v.selectedCardList",selectedCardList);
                        }*/
                        
                    }
                    /*if(resultCard[0].Error_Message__c && !resultCard[0].Stripe_ACH_Id__c){
                        console.log('resultCard.length>0 resultCard[0].Stripe_ACH_Id__c final'+resultCard[0].Stripe_ACH_Id__c);
                        console.log('resultCard.length>0 resultCard[0].Error_Message__c final'+resultCard[0].Error_Message__c);
                        this.setCardInEditMode(component,event,resultCard);
                    }*/
                    
                }
                else{
                    cardsList = [];
                    cardsList.unshift({"Last4__c" : "--- Select Card ---"});
                    var emptylist ={};
                    if(methodName == 'c.ACHVerifiedListServer'){
                        component.set("v.isVerifiedACHListEmpty",true);
                        component.set("v.ACH",cardsList);
                        component.set("v.defaultACH",emptylist);
                        component.set("v.selectedACH",emptylist);
                    }else if(methodName == 'c.ACHNonVerifiedListServer'){
                        component.set("v.isNonVerifiedACHListEmpty",true);
                        component.set("v.nonVerifiedACH",cardsList);
                        component.set("v.defaultNonVerifiedACH",emptylist);
                        component.set("v.selectedNonVerifiedACH",emptylist);
                        component.set("v.disableVerifyACHButton",true);
                    }
                    component.set("v.selectedACH", "--- Select Card ---");
                    component.set("v.selectedVerifiedACH", "--- Select Card ---");
                }
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
                
            }
        });
         $A.enqueueAction(ACHAction);
     },
    
    refreshPrimaryAddress : function(component,event,contactId){
        console.log('card list called');
        var primaryAddressAction = component.get('c.refreshContactAddressServer');
        primaryAddressAction.setParams({
            conId: contactId
        });
        primaryAddressAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS" ){
                console.log('inside update primary address response');
                component.set("v.contact.address",response.getReturnValue());
                
                component.set("v.showSameAsShippingCheckbox",true);
                
                var displayAddress = response.getReturnValue().Shipping_Street__c+','+response.getReturnValue().Shipping_City__c+','+response.getReturnValue().Shipping_State_Province__c+',';
                displayAddress+=response.getReturnValue().Shipping_Country__c+','+response.getReturnValue().Shipping_Zip_Postal_Code__c+'.';
                
                component.set("v.displayAddress",displayAddress);
                //component.set("v.showSameAsShippingCheckbox",true);
                component.set("v.contactHasPrimaryAddress",true);    
            }
        }); 
        $A.enqueueAction(primaryAddressAction);
       
    },
    
    setCardInEditMode : function(component,event,resultCard){
        console.log('inside invalid card with error');
        //component.find("newCardChkBox").set("v.checked",true);
        component.set("v.newCard",true);
        console.log('in else new card name',resultCard[0].Name_On_Card__c);
        var newCardDetails = {};
        var cardId = resultCard[0].Id;
        component.set("v.newCardDetails",newCardDetails);
        component.set("v.newCardDetails.Name_On_Card__c",resultCard[0].Name_On_Card__c);
        component.find("cardType").set("v.value",resultCard[0].Card_Type__c);
        component.find("expMonths").set("v.value",resultCard[0].Expiry_Month__c);
        component.find("expYears").set("v.value",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Credit_Card_Number__c",resultCard[0].Credit_Card_Number__c);
        component.set("v.newCardDetails.Cvc__c",resultCard[0].Cvc__c);
        component.set("v.newCardDetails.Expiry_Month__c",resultCard[0].Expiry_Month__c);
        component.set("v.newCardDetails.Expiry_Year__c",resultCard[0].Expiry_Year__c);
        component.set("v.newCardDetails.Card_Type__c",resultCard[0].Card_Type__c);
        component.set("v.newCardDetails.address.Billing_Street__c",resultCard[0].Billing_Street__c);
        component.set("v.newCardDetails.address.Billing_City__c",resultCard[0].Billing_City__c);
        component.set("v.newCardDetails.address.Billing_State_Province__c",resultCard[0].Billing_State_Province__c);
        component.set("v.newCardDetails.address.Billing_Country__c",resultCard[0].Billing_Country__c);
        component.set("v.newCardDetails.address.Billing_Zip_Postal_Code__c",resultCard[0].Billing_Zip_Postal_Code__c);
        component.set("v.isUpdateCardWithError",true);
        component.set("V.errorCardId",cardId);
        component.set("v.cardErrorMsg",resultCard[0].Error_Message__c);
        //var spinner = component.find("mySpinner");
        //$A.util.toggleClass(spinner, "slds-hide"); 
        
    },
    
    correctCardDataHelper : function(component,event,errorCardId,newCardDetails){
        console.log('correct Card Detail Helper called');
        console.log('cardID',errorCardId);
        console.log('newcardDetials',newCardDetails.Name_On_Card__c);
    },
    setproductOptions : function (component,event) {

        console.log('hellomotto',component.get("v.product"));
        var action = component.get('c.setProductWrapper');
        action.setParams({
            prodList: component.get("v.product")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state--', state);
            if(state == "SUCCESS" ){
                var result = response.getReturnValue();
                var productOption = component.get("v.productOption");
                component.set("v.productOptions",productOption);
                var productOptions = component.get("v.productOptions");
                productOptions.push.apply(productOptions,result);
                component.set("v.productOptions",productOptions);
                console.log('hellomotto',component.get("v.productOptions"));
                this.handleProductSearch(component,event);
            }
        });
        $A.enqueueAction(action);
    },
    handleProductSearch : function (component,event) {
        console.log('checkedProductList',component.get("v.checkedProductList"));
        if(component.get("v.product").length <1&&component.get("v.checkedProductList").length<1){
            component.set("v.disableCardList",true);
        }else{
            component.set("v.disableCardList",false);
        }
        var selectedProdOne = [];
        var prodItems = component.get("v.productOptions");
        console.log('prodItem',prodItems);
         
        
        for(var i = 0; i < prodItems.length; i++){
            console.log('prodItems',prodItems[i]);
            if(prodItems[i].checkedForClub){}
                else{
                prodItems[i].checkedForClub=false;
                }
            if(prodItems[i].checkValue == true && prodItems[i].checkedForClub==false && prodItems[i].standAloneClubProd==false){
                console.log('amountToPayFromCard1',prodItems[i].checkValue);
                selectedProdOne.push(prodItems[i]);
                 
                 prodItems[i].checkedForClubCard=true;
            }   
        }
        component.set("v.productOptionsOne",selectedProdOne);
        this.setTotalPrice(component,event);
    },
    sendToVF : function(component, event, helper) {
        var message = "success";
        console.log('sendToVF',message);
        var vfOrigin = "https://" + component.get("v.vfHost");
        if(component.find("vfFrame")){
        	var vfWindow = component.find("vfFrame").getElement().contentWindow;
            vfWindow.postMessage(message, vfOrigin);
        }
    }
})