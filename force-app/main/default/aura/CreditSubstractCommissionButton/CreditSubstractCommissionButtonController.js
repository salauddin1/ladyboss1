({
    handleShowModal: function(component, evt, helper) {
        var modalBody;
        console.log('lib called');
        $A.createComponent("c:CreditSubstractCommission", { recordId : component.get("v.recordId") },
                           function(content, status,errorMessage) {
                            console.log('status',errorMessage);
                            console.log('content',content);
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Manual Adjustments",
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "slds-modal_large fullwidth",
                                       closeCallback: function() {
                                       }
                                   })
                               }
                           });
    },
})