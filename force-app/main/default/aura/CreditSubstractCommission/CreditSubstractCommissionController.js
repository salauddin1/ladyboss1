({
    closeModel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();                                   
    },
    verifyDetails: function(component, event, helper){
        component.set("v.isError",false);
            component.set("v.errormsg",'');
        console.log(component.get("v.value"));
        var isDirectVal = component.get("v.value");
        var AmountThatManupulateVal = component.get("v.AmountThatManupulate");
        var weekNumVal = component.get("v.weekNum");
        var yearValue = component.find("currentYear").get("v.value");
        var defaultUser = '';
        var isRefund = true;
        var msgs;
        if(event.getSource().get("v.name")=="btn1"){
            isRefund = false;
            msgs = "Commission created Successfully";
        }else if(event.getSource().get("v.name")=="btn2"){
            isRefund = true;
            msgs = "Amount refunded Successfully";
        }
        if($A.util.isEmpty(component.get("v.defaultUser")) || $A.util.isUndefined(component.get("v.defaultUser"))){
            component.set("v.isError",true);
            component.set("v.errormsg",'Select Agent');
            return;
        }else{
            defaultUser = component.get("v.defaultUser").Id;
        }
        var ReasonForAdjustmentVal = component.get("v.ReasonForAdjustment");
        console.log(isDirectVal);
        console.log(AmountThatManupulateVal);
        console.log(weekNumVal);
        console.log(JSON.stringify(defaultUser));
        console.log(ReasonForAdjustmentVal);
        console.log("Year is ",yearValue);
        
        if($A.util.isEmpty(AmountThatManupulateVal) || $A.util.isUndefined(AmountThatManupulateVal)){
            component.set("v.isError",true);
            component.set("v.errormsg",'Enter Amount');
            return;
        }
        if($A.util.isEmpty(weekNumVal) || $A.util.isUndefined(weekNumVal)){
            component.set("v.isError",true);
            component.set("v.errormsg",'Enter Week');
            return;
        }
        if($A.util.isEmpty(ReasonForAdjustmentVal) || $A.util.isUndefined(ReasonForAdjustmentVal)){
            component.set("v.isError",true);
            component.set("v.errormsg",'Enter Reason');
            return;
        }
        if(component.get("v.disableBtn")==true || component.get("v.disableBtn1")==true){
            return;
        }
        var action = component.get("c.submitCommission");
        action.setParams({"isDirect": isDirectVal,"AmountThatManupulate":AmountThatManupulateVal,"weekNum":weekNumVal,"AgentId":defaultUser,"ReasonForAdjustment":ReasonForAdjustmentVal,"isRefund":isRefund,"conId":component.get("v.recordId"),"currentYear":yearValue});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue(); 
                var result2='No';
                console.log("result---",result);
                if(result == result2){
                    component.set("v.isError",true);
                    component.set("v.errormsg","No Opportunities to refund");
                }else if(result == 'no_hierarchy' ){
                    component.set("v.isError",true);
                    component.set("v.errormsg","No User Hierarchy Found For This User");
                }else{
                    component.find("overlayLib").notifyClose();                                   
                    component.set("v.salesPerson",result);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": msgs,
                        "type" : "success"
                    });
                    toastEvent.fire();
                }
            }else if(state == "ERROR"){
                var errors = response.getError();
                component.set("v.isError",true);
                component.set("v.errormsg",errors[0].message);
                
            }
        });
        $A.enqueueAction(action);
        
    },
    doInit : function(component,event,helper){
        console.log("Do In It");
        var action = component.get("c.currentWeek");
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            var result = responsetogetUser.getReturnValue();
            console.log(result);
            if (state === 'SUCCESS') {
                component.set("v.maxWeek",result);
            }
        });
        $A.enqueueAction(action);
    },
    checkWeek : function(component,event,helper){
        var inpWeek = component.get("v.weekNum");
        var curWeek = component.get("v.maxWeek");
        if(inpWeek < 0){
            component.set("v.isFutureWeek",true);
            component.set("v.futureWeekMsg","Week can not be less than zero");
            component.set("v.disableBtn",true);
        }else if(inpWeek > curWeek){
            component.set("v.isFutureWeek",true);
            component.set("v.futureWeekMsg","You can not select future week");
            component.set("v.disableBtn",true);
        }else{
            component.set("v.isFutureWeek",false);
            component.set("v.disableBtn",false);
        }
    } ,
    checkType : function(component,event,helper){
        console.log(component.get("v.value"));
        var isDirectVal = component.get("v.value");
        console.log(isDirectVal);
        if(isDirectVal=="true"){
            component.set("v.CommissionTypeMsg","These are directly-credited SPIFFs, Infractions, or Commissions Adjustments for the selling agent alone");
        }else{
            component.set("v.CommissionTypeMsg","These are \"sales\" or \"refunds\" that will add or subtract from the VP, Director, Senior Agent, and Selling Agent based on the tier in which the \"week's\" sale was made. ");
        }
    },
    checkAmount : function(component,event,helper){
        var amt = component.get("v.AmountThatManupulate");
        if(amt < 0){
            component.set("v.isAmountPos",true);
            component.set("v.amountMsg","please enter positive amount");
            component.set("v.disableBtn1",true);
        }else{
            component.set("v.isAmountPos",false);
            component.set("v.disableBtn1",false);
        }
    }
})