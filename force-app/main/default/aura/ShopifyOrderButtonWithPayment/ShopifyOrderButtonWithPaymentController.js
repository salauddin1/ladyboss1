({ 
    handleShowModal: function(component, evt, helper) {
        var modalBody;    
        $A.createComponent("c:ShopifyOrderFormWithPayment", { recordId : component.get("v.recordId") },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       //header: "New Shopify Order",
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "slds-modal_large fullwidth",
                                       closeCallback: function() {
                                       }
                                   })
                                   
                               }
                               
                           });
    },
});