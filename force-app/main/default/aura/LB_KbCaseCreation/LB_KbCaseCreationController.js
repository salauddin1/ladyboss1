({
    doInit : function(component, event) {
        var subjectList = ['Fitness','Nutrition','LadyBoss LIVE','LABS Supplements',
                           'Digital Programs','Mentality','SWAG','Coaching','Technical Support','Miscellaneous'];
        component.set("v.subjectList",subjectList);
        var caseObj = component.get("v.caseObj");
        caseObj = { };
        component.set("v.caseObj",caseObj);
    },
    
    closeModal : function(component, event, helper) {
        component.find("overlayLib").notifyClose();	
    },
    
    submitcaseObj : function(component, event, helper) {
        var sub = component.get("v.selectedSubject");
        console.log('subject value',sub);
        if(sub) 
        component.set("v.caseObj.subject",sub);
        var self = this;
        var caseObj = component.get("v.caseObj");
        console.log('case obj val',caseObj);
        console.log('case Obj sub',caseObj.subject);
       // if(caseObj.fname != null && caseObj.lname != null && caseObj.email != null && caseObj.subject != null && caseObj.description != null){
          if( caseObj.lname != null && caseObj.email != null && caseObj.subject != null && caseObj.description != null){   
        console.log("caseObj--->",caseObj);
            var action = component.get("c.doCreateContact");
            action.setParams({ cw : JSON.stringify(caseObj)});
            console.log("sent------");
            action.setCallback(this, function(a) {
                console.log('ret ',a); 
                console.log('ret '+a);
                var state = a.getState();
                if(state == "SUCCESS"){
                    console.log('ret success--->',a.getReturnValue());
                    component.find("overlayLib").notifyClose();	
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode" : "sticky",
                        "type" : "success",
                        "title": "Success!",
                        "message": "The Case has been Submitted. You will get a reply at the email address you provided within 24 hours. Your Case Number is "+ a.getReturnValue()
                    });
                    toastEvent.fire();
                    
                }else{
                    console.log('ret fail--->',a.getReturnValue());
                    component.set("v.errorMsg",a.getReturnValue());
                    component.set("v.isError", true);                
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set("v.errorMsg","Please fill all the fields");
            component.set("v.isError", true);  
        }
    }
    
})