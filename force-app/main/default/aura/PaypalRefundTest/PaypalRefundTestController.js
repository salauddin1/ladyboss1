({
    handleShowModal : function(component, event, helper) {
        debugger
        var conID = component.get("v.ContactId");
        var action = component.get("c.refundPaypal");
        var amount = component.get("v.amount");
        action.setParams({ conID : conID,
                          amount : amount});
        
        action.setCallback(this, function (response) {
            debugger
            var state = response.getState();
            console.log('state:',state);
            
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                
            }
            
        });
        $A.enqueueAction(action);
    },
    doInit: function(component, event, helper) {
        debugger
        console.log("--------do----------");
        var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var arr = a[6];
        console.log("--------arr--------"+arr);
        var action = component.get("c.getOpportunity");
        action.setParams({ contactId :arr});
        
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                if(a != null){
                    var b = a.length;
                    if(b > 0){
                        
                        component.set("v.OppValue" , true);
                    }
                    else{
                        //debugger
                        component.set("v.OppValue" , false);
                        
                    }
                }
                component.set("v.OppName", response.getReturnValue());
                
                var b =   a.map(record => record.Name);
                console.log("____B_____________"+b);
                component.set("v.OppName1", b);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action1 = component.get("c.getOpportunityRefund");
        action1.setParams({ contactId :arr});
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                var b = a.length;
                if(b > 0){
                    //debugger
                    component.set("v.OppValueRefund" , true);
                }
                else{
                    //debugger
                    component.set("v.OppValueRefund" , false);
                    
                }
                
                //debugger
                component.set("v.OppNameRefund", response.getReturnValue());
                var a = response.getReturnValue();
                var b =   a.map(record => record.Name);
                console.log("____B_____________"+b);
                component.set("v.OppName1", b);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        debugger
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
    },
    openmodal:function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    openmodalCancel:function(component,event,helper) {
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    closeModal:function(component,event,helper){ 
        /*var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.find("addressform").set("v.value", " ");
         // component.find("addressform2").set("v.value", " ");
        component.set("v.description", "");
        component.set("v.drop", " ");
         var divCom = component.find("div1");
        debugger
        $A.util.addClass(divCom, 'slds-hide');
		 var closeB = component.find("close");
         $A.util.removeClass(closeB, 'slds-hide');
        component.find("radioId").set("v.checked" ,false);*/
        helper.closeModal1(component,event);
    },
    closeModalCancel:function(component,event,helper){ 
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
    },
    
    
    abc: function(component, event, helper) {
        var placeho;
        var value = event.getSource().get('v.value') ;
        component.set("v.drop", value);
        debugger
        if(value == 'other'){
            component.set("v.placeholder", 'Add more detail about refund. ');
            component.set("v.req", true);
        }
        else if (value == 'duplicate'){
            component.set("v.placeholder", 'Add more detail about duplicate stripe refund. ');
            component.set("v.req", false);
        }
            else if (value == 'fraudulent'){
                component.set("v.placeholder", 'Add more detail about fraudulent stripe refund. ');
                component.set("v.req", false);
            }
                else if (value == 'Requested_by_customer'){
                    component.set("v.placeholder", 'Add more detail about requested by customer stripe refund. ');
                    component.set("v.req", false);
                }
        
        
        console.log("------------"+value);
        /* var action = component.get("c.getDropVal");
        console.log("-------"+action);
        action.setParams({ dropVal: value});
        action.setCallback(this, function() {  console.log('SAVED.');  } );
        $A.enqueueAction(action);
        console.log('save:end');
 
        
         debugger*/
    },
    description:function(component, event, helper) {
        var desValue = event.getSource().get('v.value') ;
        component.set("v.description", desValue);
        //debugger
        
    },
    
    save : function(component, event, helper) {
        debugger
        component.set("v.Spinner", true);
        var checkVal = component.get("v.DisclaimerValue");
        console.log("-------------"+checkVal);
        // var radio = component.get("v.radioVal");
        console.log('save:1');
        var dropVal =  component.get("v.drop");
        var description = component.get("v.description");
        var oppId =  component.get("v.radioId");
        var amount = component.get("v.OppName");
        var amt = component.get("v.OppName.Amount");
        console.log("-----dropVal--------"+dropVal);
        console.log("-----oppId--------"+oppId);
        console.log("-----description--------"+description);
        console.log("-----amount--------"+amount);
        console.log("-----amt--------"+amt);
        if(checkVal == true){
            var action = component.get("c.getAmount");
            console.log("-------"+action);
            debugger
            action.setParams({dropVal :dropVal,
                              opprId:oppId,
                              description:description,
                              amt:amt});
            action.setCallback(this, function(response) { 
                debugger
                var msg =   response.getReturnValue();
                component.set("v.Spinner", false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: msg,
                    duration:' 10000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                console.log('SAVED.'); 
                
            } );
            $A.enqueueAction(action);
            console.log('save:end');
            var cmpTarget = component.find('Modalbox');
            var cmpBack = component.find('Modalbackdrop');
            $A.util.removeClass(cmpBack,'slds-backdrop--open');
            $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
            component.set("v.description", " ");
            component.set("v.OppName.Amount", " ");
            component.set("v.drop", " ");
            component.find("ProofDisclaimer").set("v.checked" ,false);
            var radioLen = component.find("radioId");
            for (var i = 0; i < radioLen.length; i++) {
                if (radioLen[i].get("v.checked") == true ) {
                    radioLen[i].set('v.checked', false); 
                }
            }
            //component.find("radioId1").set("v.checked" ,false);
            
            //helper.cancelBtn(component,event,helper);	
        }
    },
    
    check1: function(component, event, helper) {
        var checkVal = event.getSource().get("v.checked");
        console.log("-------------"+checkVal);
        debugger
        if(checkVal == true){
            debugger
            component.set("v.DisclaimerValue", true);
        }
        else{
            component.set("v.DisclaimerValue", false);
            
        }
    },
    onCheckRadio :function(component, event, helper) {
        debugger
        var radioVal = event.getSource().get("v.label");
        var radioId = event.getSource().get("v.value");
        var radioAmount = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.AmountVal" ,radioAmount);
        component.set("v.OppName.Amount" ,radioAmount);
        debugger
        //alert(event.getSource().get("v.label"));
        //alert(event.getSource().get("v.value"));
        var divCom = component.find("div1");
        $A.util.removeClass(divCom, 'slds-hide');
        var closeB = component.find("close");
        $A.util.addClass(closeB, 'slds-hide');
        
        component.set("v.radioVal",radioVal);
        component.set("v.radioId",radioId);
        
    },
    onChangeAmount :function(component, event, helper) {
        //var amt = component.find("radioId").get("v.id");
        var amt = component.get("v.AmountVal");
        console.log("-------"+amt);
        var value = event.getSource().get("v.value");
        debugger
        if(amt < value ){
            component.set("v.NumValidationError" , true); 
        }
        else{
            component.set("v.NumValidationError" , false); 
            
        }
    },
    cancelOpp : function(component, event, helper) {
        var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var arr = a[6];
        console.log("--------arr--------"+arr);
        var oppIds = component.get("v.recordId");
        //debugger
        var action2 = component.get("c.getOppData"); 
        action2.setParams({ OppId : arr
                          });
        
        action2.setCallback(this, function(response) {
            console.log("---------re------"+response.getReturnValue());
            component.set("v.opp", response.getReturnValue());
            //var a = response.getReturnValue();
            //var b =   a.map(record => record.Id);
            //console.log("____B_____________"+b);
            //component.set("v.oppId", b);
        });
        $A.enqueueAction(action2);
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            title : 'Success ',
            message: 'Subscription cancelled in stripe ',
            duration:' 2000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        
        console.log('save:end');
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        
    },
})