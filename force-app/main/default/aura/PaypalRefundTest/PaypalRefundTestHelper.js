({
    cancelBtn : function(component, event, helper) { 
        // Close the action panel 
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire(); 
    },
    closeModal1:function(component,event){ 
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.find("addressform").set("v.value", " ");
        component.find("ProofDisclaimer").set("v.checked" ,false);
        component.set("v.description", "");
        component.set("v.drop", " ");
        var radioLen = component.find("radioId");
        if(radioLen != null){
            for (var i = 0; i < radioLen.length; i++) {
                if (radioLen[i].get("v.checked") == true ) {
                    radioLen[i].set('v.checked', false); 
                }
            }
        }
        var divCom = component.find("div1");
        debugger
        $A.util.addClass(divCom, 'slds-hide');
        var closeB = component.find("close");
        $A.util.removeClass(closeB, 'slds-hide');
        //component.find("radioId").set("v.checked" ,false);
        
        
    },
    
})