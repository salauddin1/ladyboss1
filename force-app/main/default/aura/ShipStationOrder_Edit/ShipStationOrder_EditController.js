({
    doInit : function(component, event, helper) {
        var shipId = component.get("v.recordId");
        if(shipId == undefined){
            var a = window.location.href.split("/");
            console.log("------a----------"+a);
             shipId = a[6];
        }
        console.log("--------shipId--------"+shipId);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);
        var action = component.get("c.getShipOrder");
        action.setParams({ shipId :shipId});
        
        action.setCallback(this, function(response) {
            debugger
            var a = response.getReturnValue();
            component.set("v.contactVal",a);
            component.set("v.newContact",a[0]);
            //component.set("v.newContact.orderId__c",a[0].orderId__c);
            component.set("v.newContact.orderStatus__c",a[0].orderStatus__c);
            component.set("v.newContact.modifyDate__c",a[0].modifyDate__c);
            component.set("v.newContact.OwnerId",a[0].OwnerId);
            component.set("v.newContact.Contact__c",a[0].Contact__c);
            /*component.set("v.newContact.Ship_To_City__c",a[0].Ship_To_City__c);
            component.set("v.newContact.Ship_To_Country__c",a[0].Ship_To_Country__c);
            component.set("v.newContact.Ship_To_State__c",a[0].Ship_To_State__c);
            component.set("v.newContact.Ship_To_Phone__c",a[0].Ship_To_Phone__c);
            component.set("v.newContact.Ship_To_Residential__c",a[0].Ship_To_Residential__c);
            component.set("v.newContact.Ship_Street1__c",a[0].Ship_Street1__c);*/
            
            component.set("v.newContact.orderNumber__c",a[0].Order_Number__c);
            component.set("v.newContact.orderStatus__c",a[0].orderStatus__c);
            component.set("v.newContact.Name",a[0].name__c);
            component.set("v.newContact.customerEmail__c",a[0].customerEmail__c);     
            component.set("v.newContact.phone__c",a[0].phone__c);
            component.set("v.newContact.street1__c",a[0].street1__c);
            component.set("v.newContact.city__c",a[0].city__c);
            component.set("v.newContact.Zip",a[0].Ship_To_PostalCode__c);
            component.set("v.newContact.state__c",a[0].state__c);
            if(a[0].orderStatus__c == "shipped") {
                component.set('v.buttonVal', true);
            }
            
        });
        $A.enqueueAction(action);
        
        
    },
    cancel:function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
    update: function(component, event, helper) {
        debugger
        var validExpense = component.find('orderform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if(validExpense){
            //var shipId = component.get("v.recordId");
            var a = window.location.href.split("/");
            console.log("------a----------"+a);
            var shipId = a[6];
            console.log("--------shipId--------"+shipId);
            
            var newContact = component.get("v.newContact"); 
            var newContact1 = JSON.stringify(newContact);
            var action = component.get("c.updateShipOrder1");
            action.setParams({orderIds: newContact});
            
            action.setCallback(this, function(response) {
                var result = response.getReturnValue();
                debugger
                if(result === null) {
                    component.find("overlayLib").notifyClose();                                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success ',
                        message: 'Order succeesful updated',
                        duration:' 1000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    // var a = response.getReturnValue();
                }
                else {
                    component.set("v.ErrorMsg", result)
                }
            });
            $A.enqueueAction(action);
        }
    },
})