({
   
    handleShowModal: function(component, evt, helper) {
        var modalBody;
        console.log('lib called');
        $A.createComponent("c:UpdateCardCaseComp", { recordId : component.get("v.recordId") },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Notify Expiry Card",
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "slds-modal_large fullwidth",
                                       closeCallback: function() {
                                       }
                                   })
                                   
                               }
                               
                           });
    },
    
})