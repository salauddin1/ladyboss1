({
	doInit : function(component, event, helper) {
		helper.init(component,event,helper);
	},handleKeyUp: function (component, evt) {
        
            var queryTerm = component.find('enter-search').get('v.value');
            console.log('Searched for "' + queryTerm );
            var conID =component.get("v.recordId");
            console.log('hello');
            var action = component.get("c.searchOpportunity");
            action.setParams({conID:conID, searchText:queryTerm});
            action.setCallback(this, function(response) { 
                var state = response.getState();
                if(state == "SUCCESS" ){
                    var result = response.getReturnValue();
                    console.log('result---', result);
                    if(result && result!=null){
                        component.set("v.OppLineItem", result);
                    }
                }
            } );
            $A.enqueueAction(action);
        
    }
})