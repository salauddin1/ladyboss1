({
    doInit : function(component, event, helper) {
        
        var contactId = component.get("v.recordId");
        console.log(contactId);
        
        var action = component.get("c.getshopOrder");
        action.setParams({ conId :contactId});
        
        action.setCallback(this, function(response) {
            debugger
            var a = response.getReturnValue();
            console.log(a);
            component.set("v.allOrders",a);
        });
        $A.enqueueAction(action);
    },
    valview : function(component, event, helper) {
        var indexvar = event.getSource().get("v.value");
        console.log("indexvar:::" + indexvar);
        console.log("indexvar:::" + indexvar.Order_Id__c);
        var action = component.get("c.getshopItems");
        action.setParams({ ordId :indexvar.Id});
        
        action.setCallback(this, function(response) {
            debugger
            if(response.getState() == "SUCCESS"){
                var a = response.getReturnValue();
                component.set("v.status",true);
                component.set("v.item",a);
                component.set("v.orderId",indexvar.Id);
                component.set('v.amount', indexvar.Refundable_Amount__c);
            }
        });
        $A.enqueueAction(action);
    },
    refund : function(component, event, helper) {
        debugger
        var indexvar = component.get("v.orderId");
        var Reason = component.get("v.merchntResoanVal");
        console.log(indexvar);
        var amount = component.get("v.amount");
        var action = component.get("c.refunds");
        action.setParams({ conId : indexvar,
                          amount : amount,
                          reason: Reason});
        
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if(result == null || result == '') {
                component.find("overlayLib").notifyClose();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: 'Refunded Successful',
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
            else{
                component.set("v.RefundFailed",result);
            }
        });
        $A.enqueueAction(action);
    },
    cancel : function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    }
})