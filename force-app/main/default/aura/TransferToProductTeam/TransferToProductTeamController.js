({
    transferToProductTeam : function (component, event, helper) {
        console.log('inside do transferToProductTeam');
        var action = component.get("c.transferOwnership");
        var qName = event.getSource();
        var val = qName.get("v.label");
        var devName;
        if(val == "Transfer To Support Team"){
            devName = "Support_Team"
        }else if(val == "Transfer To Coaching Team"){
            devName = "Coaching_Queue"
        }else if(val == "Transfer To Phone Team"){
            devName = "Phone_Team"
        }else if(val == "Transfer To Merchandise Team"){
            devName = "Merchandise_Team"
        }else if(val=="Transfer To Retention Team"){
            devName="Retention_Team"
        }
        console.log('val'+val+'----'+devName);
        action.setParams({
            caseId : component.get("v.caseId"),
            queueName : devName
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
            }else if (a.getState() == "ERROR"){
                var errors = a.getError();
              
                let toastParams = {
                    title: "Error",
                    message: "Unknown error", // Default error message
                    type: "error"
                };
                
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    toastParams.message = errors[0].message;
                }

                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
                
            }    
        });
        $A.enqueueAction(action);
    },
})