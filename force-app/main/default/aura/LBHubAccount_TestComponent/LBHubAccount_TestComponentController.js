({
    // common reusable function for toggle sections
    toggleSection : function(component, event, helper) {
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    
    doInit : function(component, event, helper){
        debugger
        component.set("v.showcomp",true);
        component.set("v.hidehref",true);
        component.set("v.showhref",false);
        component.set("v.showbtn",true);  
        component.set("v.CardCloneurl","https://ladyboss--devs--c.cs90.visual.force.com/apex/UpdateCardClone?id=");  
        var recordId = component.get("v.recordId");
        var action = component.get("c.getcontact");
        action.setParams({ conId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.ContactDetails",res);
                component.set("v.messageflag", false);
                if(res != null && res.length > 0){
                    component.set("v.ContactDetails", res);    
                }
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getAddress");
        action1.setParams({ conId : recordId});
        
        action1.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.AddressDetails",res);
                component.set("v.messageflag", false);
                if(res != null && res.length > 0){
                    component.set("v.AddressDetails", res);    
                }
            }
        });
        $A.enqueueAction(action1);
        
        var action2 = component.get("c.getCards");
        action2.setParams({ conId : recordId});
        
        action2.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.cardDetails",res);
                var card=component.get("v.cardDetails");
                component.set("v.messageflag", false);
                if(res != null && res.length > 0){
                    component.set("v.cardDetails", res);
                    console.log(res);
                }
            }
        });
        $A.enqueueAction(action2);
    },
    
    RecordUpdate : function(component, event, helper){
        debugger
        var recordId = component.get("v.recordId");
        var firstname = component.get("v.ContactDetails.FirstName");
        var LastName = component.get("v.ContactDetails.LastName");
        var Phone = component.get("v.ContactDetails.Phone");
        var Street = component.get("v.AddressDetails.Shipping_Street__c");
        var City = component.get("v.AddressDetails.Shipping_City__c");
        var state = component.get("v.AddressDetails.Shipping_State_Province__c");
        var zipcode = component.get("v.AddressDetails.Shipping_Zip_Postal_Code__c");
        var country = component.get("v.AddressDetails.Shipping_Country__c");
        
        var action1 = component.get("c.updateInfoUser");
        action1.setParams({ firstname : firstname,
                           idparam : recordId,
                           lastName:LastName,
                           phoneNumTxt:Phone,
                           street:Street,
                           city:City,
                           state:state,
                           zipCode:zipcode,
                           country:country
                          });
        
        action1.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                component.set("v.registerSuccessMsg",res);
                component.set("v.showcomp",false);
                console.log(res);
            }
        });
        $A.enqueueAction(action1);
    },
    
    chkTerms : function(component, event, helper){
        debugger
        var isChecked = component.find("chkTerms").get("v.checked");
        // component.set("v.chkTerms", isChecked);
        if(isChecked == true){
            component.set("v.showbtn",false);
            component.set("v.showhref",true);
            component.set("v.hidehref",false);
        }
        else{
            component.set("v.showbtn",true);
            component.set("v.showhref",false);
            component.set("v.hidehref",true);
        }
    }
})