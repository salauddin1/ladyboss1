({
    openmodal:function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    openmodalCancel:function(component,event,helper) {
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    closeModal:function(component,event,helper){ 
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
         component.set("v.checkVal" , false);
        component.set("v.dateVal ", null);
       
        
    },
    closeModalCancel:function(component,event,helper){ 
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
    },
    doInit:function(component, event, helper) {
        
        var oppIds = component.get("v.recordId");
        console.log("______oppIds_______"+oppIds);
        
        var action = component.get("c.getQuantity1");
        console.log("-------"+action);
        //alert(oppId);
        action.setParams({ oppIds :oppIds
                         });
        
        action.setCallback(this, function(response) { 
            
            
            var state = response.getState();
            if(state == "SUCCESS" ){
                component.set("v.prodName", response.getReturnValue());
                console.log("________"+response.getReturnValue());
                var result = response.getReturnValue();
            	console.log("____result____"+JSON.stringify(result));
                var a = component.get("v.prodName")[0].Quantity;
            	console.log("________"+a);
            	component.set("v.quntity" , a);
                var subId = component.get("v.prodName")[0].Subscription_Id__c;
                var successFailureMessage = component.get("v.prodName")[0].Success_Failure_Message__c;
                if(!subId){
                    console.log('subId---'+subId);
                    component.set("v.disableButton" , true);
                }else if(successFailureMessage && successFailureMessage !='' && successFailureMessage != null){
                    console.log('successFailureMessage---'+successFailureMessage);
                    component.set("v.disableButton" , true);
                }else{
                    component.set("v.disableButton" , false);
                }
                console.log('subId---'+subId);
            	console.log('SAVED.');
            }
            
              } );
        $A.enqueueAction(action);
    },
    
    updateOpp : function(component, event, helper) {
        console.log('save:1');
        var dateVal =  component.get("v.dateVal");
        console.log("-----dropVal--------"+dateVal);
        var checkVal =  component.get("v.checkVal");
        console.log("-----checkVal--------"+checkVal);
        
        //var quntityVal =  component.get("v.quntity");
        var quntityVal = parseInt(component.get("v.quntity"));
        console.log("-----quntityVal--------"+quntityVal);
        var oppIds = component.get("v.recordId");
        console.log("______oppIds_______"+oppIds);
        var currId =window.location.href.split("/");
        console.log("______currId_______"+currId);
        var arr = currId[6];
        console.log("------arr-------"+arr);
        var action = component.get("c.getQuantity");
        console.log("-------"+action);
        //alert(oppId);
        action.setParams({ dropVal :quntityVal,
                          OppId : oppIds,
                          oppIds :oppIds,
                          currOppId: arr,
                          dateVal:dateVal,
                          checkVal: checkVal});
        
        action.setCallback(this, function() { 
            console.log('SAVED.');  } );
        $A.enqueueAction(action);
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            title : 'Success ',
            message: 'Subscription updated  in stripe',
            duration:' 2000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        
        console.log('save:end');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.set("v.checkVal" , false);
        component.set("v.dateVal ", null);
        
    },
    abc: function(component, event, helper) {
        debugger
       // var value = event.currentTarget.value.trim() ;
        var value = event.getSource().get('v.value') ;
        //var value =component.find('quntityId').get("v.value");
        component.set("v.quntity",value);
        console.log("------------"+value);
    },
    dateVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        
        //var value = event.currentTarget.value.trim() ;
        component.set("v.dateVal",value);
        console.log("------------"+value);
    },
    checkVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.checked");
        component.set("v.checkVal" , value);
    },
    dateUpdate : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        
        //var value = event.currentTarget.value.trim() ;
        component.set("v.dateVal",value);
        
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.dateVal") != '' && component.get("v.dateVal") < todayFormattedDate){
            debugger
            component.set("v.dateValidationError" , true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------"+dV);
        }else{
            component.set("v.dateValidationError" , false);
            
        }
    },
    
    cancelOpp : function(component, event, helper) {
        var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var arr = a[6];
        console.log("--------arr--------"+arr);
        var oppIds = component.get("v.recordId");
        debugger
        var action2 = component.get("c.getOppData"); 
        action2.setParams({ OppId : arr
                          });
        
        action2.setCallback(this, function(response) {
            console.log("---------re------"+response.getReturnValue());
            component.set("v.opp", response.getReturnValue());
            //var a = response.getReturnValue();
            //var b =   a.map(record => record.Id);
            //console.log("____B_____________"+b);
            //component.set("v.oppId", b);
        });
        $A.enqueueAction(action2);
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            title : 'Success ',
            message: 'Subscription cancelled in stripe ',
            duration:' 2000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        
        console.log('save:end');
        var cmpTarget = component.find('ModalboxCancel');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        
    },
})