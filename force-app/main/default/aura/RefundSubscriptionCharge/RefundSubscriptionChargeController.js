({
    doInit : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var action = component.get("c.getOpportunity");
        action.setParams({ contactId :contactId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                if(a != null){
                    var b = a.length;
                    if(b > 0){
                        
                        component.set("v.OppValue" , true);
                    }
                    else{
                        //debugger
                        component.set("v.OppValue" , false);
                        
                    }
                }
                component.set("v.OppName", response.getReturnValue());
                
                var b =   a.map(record => record.Name);
                console.log("____B_____________"+b);
                component.set("v.OppName1", b);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action1 = component.get("c.getOpportunityLineItemRefund");
        action1.setParams({ contactId :contactId});
        debugger
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                var b = a.length;
                if(b > 0){
                    //debugger
                    component.set("v.OppValueRefund" , true);
                }
                else{
                    //debugger
                    component.set("v.OppValueRefund" , false);
                    
                }
                
                //debugger
                component.set("v.OppNameRefund", response.getReturnValue());
                var a = response.getReturnValue();
                var b =   a.map(record => record.Refunded_Date__c);
                console.log("____Refunded_Date__c_____________"+b);
                component.set("v.OppName1", b);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        var action2 = component.get("c.getOpportunityLineItem");
        action2.setParams({ contactId :contactId});      
        action2.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            debugger
            if (state === "SUCCESS") {
                var a = response.getReturnValue();
                component.set("v.OppLineItem", response.getReturnValue());
                var b =   a.map(record => record.CreatedDate);
                var arr = [];
                for(var i=0; i< b.length; i++){
                 var dateV = b[i].split("T")[0];
                    arr.push(dateV);
                                    
                }
                component.set("v.OppLineName", arr);
                
					var dateVal = component.get("v.OppLineName");
                console.log("____BLine Item_____________"+dateVal);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        debugger
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        $A.enqueueAction(action2);
    },
    openmodal:function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    closeModal:function(component,event,helper){ 
        helper.closeModal1(component,event);
    },
    abc: function(component, event, helper) {
        var placeho;
        var value = event.getSource().get('v.value') ;
        component.set("v.drop", value);
        debugger
        if(value == 'other'){
            component.set("v.placeholder", 'Add more detail about refund. ');
            component.set("v.req", true);
        }
        else if (value == 'duplicate'){
            component.set("v.placeholder", 'Add more detail about duplicate stripe refund. ');
            component.set("v.req", false);
        }
            else if (value == 'fraudulent'){
                component.set("v.placeholder", 'Add more detail about fraudulent stripe refund. ');
                component.set("v.req", false);
            }
                else if (value == 'Requested_by_customer'){
                    component.set("v.placeholder", 'Add more detail about requested by customer stripe refund. ');
                    component.set("v.req", false);
                }
        
        
        console.log("------------"+value);
        },
    description:function(component, event, helper) {
        var desValue = event.getSource().get('v.value') ;
        component.set("v.description", desValue);
        //debugger
        
    },
    
    save : function(component, event, helper) {
        debugger
        var contactId = component.get("v.recordId");
        var oppId =  component.get("v.radioId");
		var oppLineItemId = component.get("v.OppLineId");
        //var OppLineSubId = component.get("v.OppLineSubId");
        var OppLineUnitAmt = component.get("v.OppUnitPrice");
        var OppLineUnitAmtInt = parseFloat(OppLineUnitAmt) *100;
       // var OppLineUnitAmtData = 
        console.log('save:1');
        var reason =  component.get("v.drop");
        console.log("-----dropVal--------"+reason);
        console.log("-----oppId--------"+oppId);
        console.log("-----oppLineItemId--------"+oppLineItemId);
        //console.log("-----OppLineSubId--------"+OppLineSubId);
        console.log("-----OppLineUnitAmt--------"+OppLineUnitAmtInt);
       var checkVal = component.get("v.DisclaimerValue");
        if(checkVal == true){
            var action = component.get("c.getUpdate");
            console.log("-------"+action);
            debugger
            action.setParams({ contactId: contactId ,
                              opporunityId :oppId,
                              oppLineItemId:oppLineItemId,
                              Amount:OppLineUnitAmtInt,
                              reason:reason});
            action.setCallback(this, function(response) { 
                var msg = response.getReturnValue();
                console.log("--------msg----------"+msg);
                console.log('SAVED.');  
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success ',
                message: msg,
                duration:' 1000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
            } );
            $A.enqueueAction(action);
            $A.get('e.force:refreshView').fire();

            console.log('save:end');
           
            component.set("v.description", " ");
            component.set("v.OppLineItem.UnitPrice", " ");
            component.set("v.drop", " ");
            component.find("ProofDisclaimer").set("v.checked" ,false);
            var radioLen = component.find("radioId");
            for (var i = 0; i < radioLen.length; i++) {
                if (radioLen[i].get("v.checked") == true ) {
                    radioLen[i].set('v.checked', false); 
                }
            }
             
            //component.find("radioId1").set("v.checked" ,false);
            var cmpTarget = component.find('Modalbox');
            var cmpBack = component.find('Modalbackdrop');
            $A.util.removeClass(cmpBack,'slds-backdrop--open');
            $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
            //this.doInit(component, event);
            //helper.cancelBtn(component,event,helper);	
            $A.get('e.force:refreshView').fire();

        }
        
        else{
            //alert("Refund unsuccessful");
            /* var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Unsuccess ',
                message: 'Refund unsuccessful',
                duration:' 2000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();*/
        }
        $A.get('e.force:refreshView').fire();

    },
    
    
    check1: function(component, event, helper) {
        var checkVal = event.getSource().get("v.checked");
        console.log("-------------"+checkVal);
        //debugger
        if(checkVal == true){
            debugger
            component.set("v.DisclaimerValue", true);
        }
        else{
            component.set("v.DisclaimerValue", false);
            
        }
    },
    onCheckRadio :function(component, event, helper) {
        // debugger
        var radioVal = event.getSource().get("v.label");
        var radioId = event.getSource().get("v.value");
        var radioAmount = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.OppUnitPrice" ,radioAmount);
        component.set("v.AmountVal" ,radioAmount);
        component.set("v.OppLineItem.UnitPrice" ," ");
        //debugger
        //alert(event.getSource().get("v.label"));
        //alert(event.getSource().get("v.value"));
        var divCom = component.find("div1");
        $A.util.removeClass(divCom, 'slds-hide');
        var closeB = component.find("close");
        $A.util.addClass(closeB, 'slds-hide');
        
        component.set("v.radioVal",radioVal);
        component.set("v.radioId",radioId);
        
    },
     onCheckRadio1 :function(component, event, helper) {
        // debugger
        var OppLineNameVal = event.getSource().get("v.label");
        var OppLineId = event.getSource().get("v.value");
        var OppLineUnitAmt = event.getSource().get("v.id");
        //var OppLineSubId = event.getSource().get("v.Name");
        var Oppcheckradio = event.getSource().get("v.checked");
         component.set("v.OppUnitPrice" ,OppLineUnitAmt);
        
        component.set("v.checkRadioLine" ,Oppcheckradio);
        console.log("-------------"+Oppcheckradio);
        component.set("v.OppLineUnitAmt" ,OppLineUnitAmt);
        component.set("v.OppLineItem.UnitPrice" ,OppLineUnitAmt);
        debugger
        component.set("v.OppLineNameVal",OppLineNameVal);
         //component.set("v.OppLineSubId",OppLineSubId);
        component.set("v.OppLineId",OppLineId);
          var divCom = component.find("div1");
        $A.util.removeClass(divCom, 'slds-hide');
        var closeB = component.find("close");
        $A.util.addClass(closeB, 'slds-hide');
       
        
    },
    onChangeAmount :function(component, event, helper) {
        //var amt = component.find("radioId").get("v.id");
        var amt = component.get("v.OppLineUnitAmt");
        console.log("-------"+amt);
        var value = event.getSource().get("v.value");
        component.set("v.OppUnitPrice",value);
        
        if(amt < parseFloat(value) ){
            component.set("v.NumValidationError" , true); 
        }
        else{
            component.set("v.NumValidationError" , false); 
            
        }
    },
    cancelOpp : function(component, event, helper) {
        //var toastEvent = $A.get("e.force:showToast");
        
        //toastEvent.setParams({
         //   title : 'Success ',
          //  message: 'Subscription cancelled in stripe ',
           //// duration:' 2000',
           // key: 'info_alt',
           // type: 'success',
           // mode: 'pester'
      //  });
       // toastEvent.fire();
        console.log('save:end');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('ModalbackdropCancel');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        $A.get('e.force:refreshView').fire();

    },

    
    
})