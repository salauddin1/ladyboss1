({
    init : function(component, event, helper) {
        var action = component.get("c.getLoginDetails");
        action.setParams({
            conId : component.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function(response) {
            console.log('state',response.getState());
            var state = response.getState();
            var result = response.getReturnValue();
            if(component.isValid() && state == "SUCCESS" && result != null){
                component.set("v.Spinner", false);
                component.set("v.ConDetails",result);
                console.log('result',result);
                if (result.cryptoKey != null && result.password != null) {
                    component.set("v.disableButton", false);
                }
            }else{
                component.set("v.Spinner", false);
                component.set("v.disableButton", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                  title: "Error!",
                  message: "Error in getting Contact Details",
                  type: "error"
                });
                toastEvent.fire();
            }
        }));
        $A.enqueueAction(action);
    },
    login: function (component, event, helper) {
        var conDetails = component.get("v.ConDetails");
        console.log('we are testing---------',conDetails);

        var conID = conDetails.Id;
        var conName = conDetails.name;
        window.open('https://ladyboss--tirthbox--c.cs22.visual.force.com/apex/Redirecttohub?conid='+conID+'&conname='+conName);
        //window.open('https://ladyboss--c.na100.visual.force.com/apex/Redirecttohub?conid='+conID+'&conname='+conName);
        
    }
})