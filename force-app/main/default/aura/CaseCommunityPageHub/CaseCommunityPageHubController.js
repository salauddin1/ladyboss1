({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.ContactId");
        var action = component.get("c.getcaserec");
        action.setParams({ conId : recordId});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                debugger
                var res = response.getReturnValue();
                console.log(res);
                component.set("v.CaseDetail",res);
                if(res != null && res.length > 0){
                    component.set("v.CurrntCase",res[0]);
                }
                component.set("v.messageflag", false);
                
            }
        });
        $A.enqueueAction(action);
        
    },
    OpenCase : function(component, event, helper) {
        debugger
        var targetelement1 = [];
        targetelement1 = component.find('main');
        for (var cmp in component.find('main')) {
            console.log(cmp);
            if(cmp.charCodeAt(0) >= 48 && cmp.charCodeAt(0) <= 57) {
                var targetelement2 = [];
                var targetelement2 = targetelement1[cmp].getElements();
                for(var cmp1 in targetelement1[cmp].getElements())
                    $A.util.removeClass(targetelement2[cmp1], "selectedRow");
                $A.util.addClass(targetelement2[cmp1],"data-row");
            }
        }
        
        var selected = event.currentTarget.dataset.caseid;
        console.log('======'+selected);
        var targetElement = event.currentTarget;
        $A.util.addClass(targetElement,"selectedRow");
        $A.util.removeClass(targetElement, "data-row");
        
        component.set('v.commentflag', true);
        component.set('v.CurrentCaseId', selected);
        component.set("v.CaseCommentDetail", []);
        component.set('v.csCommentButtonShowId', selected);
        
        var action = component.get("c.getCurrentcase");
        action.setParams({ caseId : selected});
        
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var a = response.getReturnValue();
                console.log(a);
                component.set("v.CurrntCase",a);
            }
        });
        $A.enqueueAction(action);
        
        
        var action2 = component.get("c.getCaseComentValues");
        action2.setParams({ caseId : selected });
        
        action2.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var resAllComment = response.getReturnValue();
                console.log(resAllComment);
                component.set("v.CaseCommentDetail", resAllComment);
                
               
            }
        });
        $A.enqueueAction(action2);
        
        
        
        
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
        
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        component.set("v.commentBox", false);
    },
    HideMesage : function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.messageflag", false);
    },
    submitcase : function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        debugger
        component.set("v.Spinner",true);
        
        component.set("v.SuccessMessage",'');
        var action = component.get("c.InsertNewCase");
        action.setParams({ caseDetail : component.get("v.Caseinsert"),
                          conId: component.get("v.ContactId")});
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var a = response.getReturnValue();
                console.log(a);
                component.set("v.messageflag", true);
                component.set("v.SuccessMessage",'Case successFully Created');
                component.set("v.Caseinsert.Customer_Name__c", '');
                component.set("v.Caseinsert.ContactEmail", '');
                component.set("v.Caseinsert.Subject", '--None--');
                component.set("v.Caseinsert.Description", '');
              //  document.location.reload();
                $A.get('e.force:refreshView').fire();
                component.set("v.tabId","tabAllCase");
				component.set("v.Spinner",false);
            }
            
        });
    //        var spinner = cmp.find("mySpinner");
    //		    $A.util.toggleClass(spinner, "slds-hide");
            
        $A.enqueueAction(action);
    },
    opencommentbox : function(component, event, helper) {
        debugger
        var caseId = event.currentTarget.title;
        component.set('v.caseId',caseId);
        component.set('v.csCommentDescValue','');
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.commentBox", true);
    },
    addCaseComment: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        var action = component.get("c.setCaseComment");
        var  csComVal     = component.get('v.csCommentDescValue');
        var  csComIdVal   = component.get('v.caseId');
        action.setParams({ csId : csComIdVal,
                          csComment : csComVal });
        
        action.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var comm = response.getReturnValue();
                console.log(comm);
                component.set("v.commentBox", false);
                var getList = component.get("v.CaseCommentDetail");
                component.set("v.CaseCommentDetail", getList);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The comment save successfully."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
                // component.set("v.CaseDetail",a);
            }
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.getCaseComentValues");
        action2.setParams({ caseId : csComIdVal });
        
        action2.setCallback(this, function(response) {
            debugger
            var state = response.getState();
            if(state == "SUCCESS") {
                var resAllComment = response.getReturnValue();
                console.log(resAllComment);
                component.set("v.CaseCommentDetail", resAllComment);
                
               
            }
        });
        $A.enqueueAction(action2);
        
    },
    
    
    
})