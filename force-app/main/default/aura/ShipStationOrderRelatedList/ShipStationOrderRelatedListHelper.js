({
    openmodal:function(component,event,helper) {
        $A.get('e.force:refreshView').fire();
        component.set("v.checkValD",true);
        
		var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
    openmodalCancel:function(component,event,helper) {
		var cmpTarget = component.find('ModalboxCancel');
		var cmpBack = component.find('ModalbackdropCancel');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
    	cancelBtn : function(component, event, helper) { 
	// Close the action panel 
	var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
	dismissActionPanel.fire(); 
	}, 
    sortVal : function (component,event,helper,ary) {
        debugger
	return ary.map(obj => {
		obj['CreateDate__c'] = (obj.CreateDate__c || '').split('T')[0]
		return obj
	})
    
}

})