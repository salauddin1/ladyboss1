({
    doInit: function(component, event, helper) {
        debugger
             /*   console.log("--------do----------");
        var a = window.location.href.split("/");
        console.log("------a----------"+a);
        var arr = a[6];
        console.log("--------arr--------"+arr);*/
        var contactId = component.get("v.recordId") ;
        var action = component.get("c.getShipStationOrder");
        action.setParams({ contactId :contactId});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state);
            
            if (state === "SUCCESS") {
                debugger
                var result =response.getReturnValue();
               var resultVal =  helper.sortVal(component, event, helper,result);
                component.set("v.ShipStationOrderItem", resultVal);
                
                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        
    },
    handleSelect : function(component, event, helper) {
        debugger
        var selectedMenuItemValue = event.detail.menuItem.get("v.label");
        var values = event.getParam("value").split(',');
        var oppId = values[0];
        //var quantity = values[1];
        //component.set("v.quntity",quantity);
        debugger
        component.set("v.oppId" ,oppId);
        if(selectedMenuItemValue =="Edit"){
            //helper.openmodal(component, event, helper);
             //var evt = $A.get("e.c:ShipStationOrder_Edit");
            $A.createComponent("c:ShipStationOrder_EditButton",{orderId : oppId},
                               function(newCmp){
                if (component.isValid()) {
                    var body = component.get("v.body");
                    body.push(newCmp);
                    component.set("v.body", body);
                }
            }
        );          
            
        }
        else if(selectedMenuItemValue == "Cancel"){
            helper.openmodalCancel(component, event, helper);
            
        }
    },
    closeModal:function(component,event,helper){ 
        $A.get('e.force:refreshView').fire();
        
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.set("v.dateVal",'');
        component.set("v.checkVal",false);
        component.set("v.checkValD",false);
        
        component.set("v.quntity"," ");
        
        
    },
    updateOpp : function(component, event, helper) {
        console.log('save:1');
        debugger
        // var oppId = event.getParam("value");
        var dateValue = component.get("v.dateVal");
        var checkVal = component.get("v.checkVal");
        var quntityVal = component.get("v.quntity");
        
        //var Val =  component.get("v.quntity");
        //console.log("-----dropVal--------"+Val);
        var oppIds = component.get("v.oppId");
        var action = component.get("c.updateSubscription");
        console.log("-------"+action);
        //alert(oppId);
        action.setParams({ prodQuant :quntityVal,
                          LineItemId :oppIds,
                          dateVal:dateValue,
                          checkVal:checkVal
                         });
        action.setCallback(this, function(response) {
            var msg = response.getReturnValue(); 
            console.log('SAVED.');  
            var toastEvent = $A.get("e.force:showToast");
            
            toastEvent.setParams({
                title : 'Success ',
                message: msg,
                duration:' 2000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
        } );
        $A.enqueueAction(action);
        $A.get('e.force:refreshView').fire();
        
        console.log('save:end');
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        component.set("v.dateVal"," ");
        component.set("v.checkVal",false);
        component.set("v.quntity"," ");
        
        
        
    },
    abc: function(component, event, helper) {
        debugger
        var value = event.currentTarget.value.trim() ;
        component.set("v.quntity",value);
        console.log("------------"+value);
    },
    abc: function(component, event, helper) {
        debugger
        var value = event.getSource().get('v.value') ;
        component.set("v.quntity",value);
        console.log("------------"+value);
    },
    dateUpdate : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        component.set("v.dateVal",value);
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        } 
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.dateVal") != '' && component.get("v.dateVal") < todayFormattedDate){
            debugger
            component.set("v.dateValidationError" , true);
            var dV = component.get("v.dateValidationError");
            console.log("--------v.dateValidationError---------"+dV);
        }else{
            component.set("v.dateValidationError" , false);
            
        }
    },
    checkVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.checked");
        component.set("v.checkVal" , value);
    },
    dateVal : function(component, event, helper) {
        debugger
        var value = event.getSource().get("v.value") ;
        
        //var value = event.currentTarget.value.trim() ;
        component.set("v.dateVal",value);
        console.log("------------"+value);
    },
    
    
})