({
    doInit : function(component, event, helper) {
        
        var CaseId = component.get("v.recordId");
        var action = component.get("c.getCards");
        action.setParams({ caseId :CaseId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Failed with state: " + state); 
            debugger
            var result = response.getReturnValue();
            if(result != null || result != undefined){
                var b =   result.map(record => record.Id);
                component.set("v.cId",b);
                for(var i in result){
                    var credCardNumber =  result[i].Credit_Card_Number__c;
                    if(!$A.util.isUndefined(credCardNumber) ) 
                    {
                        credCardNumber= credCardNumber.replace(/ /g,"0");
                        result[i].numMask = credCardNumber.replace(/\d(?=\d{4})/g, "*");
                    }
                }
            }
            component.set("v.cardData", result);
            console.log("-------------------------------=============="+response.getReturnValue());
        });
        var action1 = component.get("c.getCardsOpportunity");
        action1.setParams({ caseId :CaseId});
        action1.setCallback(this, function(response) {
            debugger    
            var state = response.getState();
            console.log("Failed with state: " + state);            
            var result1 = response.getReturnValue();
            component.set("v.oppData",result1);
            /*ar b =   result1.map(record => record.Card__c);
            component.set("v.cardOppId",b);
           
            var cardId = component.get("v.cId");
        var oppCardId = component.get("v.cardOppId");
        for(var i = 0 ; i <= cardId.length; i++){
			debugger
            for(var j=0; j <= oppCardId.length; j++){
                if(cardId[i] == oppCardId[j]){
                    component.set("v.comma",false);
                }
            }
        }*/
            
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
        //debugger
        
    },
    saveSMS : function(component, event, helper) {
        debugger
        var CaseId = component.get("v.recordId");
        var cardId =  component.get("v.cardId");
        var emailId = component.get("v.emailId");
        var phoneNum = component.get("v.phoneNum");
        var customerId = component.get("v.customerId");
        var last4Digit = component.get("v.last4Digit");
        component.set("v.loaded",true);
        console.log("-----cardId--------"+cardId);
        console.log("-----emailId--------"+emailId);
        console.log("-----customerId--------"+customerId);
        console.log("-----last4Digit--------"+last4Digit);
        
        var action = component.get("c.sendSMS");
        action.setParams({ CardId: cardId ,
                          EmailId :emailId,
                          customerId:customerId,
                          Last4Digit:last4Digit,
                          caseId:CaseId,
                          phoneNum:phoneNum
                         });
        action.setCallback(this, function(response) { 
            
            var state = response.getState();
            if(state == "SUCCESS" ){
                
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.loaded",false);
                
                component.find("overlayLib").notifyClose();                                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: result,
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                
                //var spinner = component.find('spinner');
                //$A.util.toggleClass(spinner, 'slds-show'); 
                
                // $A.get('e.force:refreshView').fire();
                
            }
            
        } );
        $A.enqueueAction(action);
        
    },
    save : function(component, event, helper) {
        debugger
        var CaseId = component.get("v.recordId");
        var cardId =  component.get("v.cardId");
        var emailId = component.get("v.emailId");
        var phoneNum = component.get("v.phoneNum");
        var customerId = component.get("v.customerId");
        var last4Digit = component.get("v.last4Digit");
        component.set("v.loaded",true);
        console.log("-----cardId--------"+cardId);
        console.log("-----emailId--------"+emailId);
        console.log("-----customerId--------"+customerId);
        console.log("-----last4Digit--------"+last4Digit);
        
        var action = component.get("c.sendEmail");
        action.setParams({ CardId: cardId ,
                          EmailId :emailId,
                          customerId:customerId,
                          Last4Digit:last4Digit,
                          caseId:CaseId
                         });
        action.setCallback(this, function(response) { 
            
            var state = response.getState();
            if(state == "SUCCESS" ){
                
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.loaded",false);
                
                component.find("overlayLib").notifyClose();                                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success ',
                    message: result,
                    duration:' 1000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                
                //var spinner = component.find('spinner');
                //$A.util.toggleClass(spinner, 'slds-show'); 
                
                // $A.get('e.force:refreshView').fire();
                
            }
            
        } );
        $A.enqueueAction(action);
        
    },
    onCheckRadio1 : function(component, event, helper) {
        debugger
        var rLast4Digit = event.getSource().get("v.label");
        var valueData = event.getSource().get("v.value");
        var value = valueData.split(" ");
        var rCustomerId = value[0];
        var rEmailId = value[1];
        var rPhoneNum = value[2];
        // var rLast4Digit = value[2];
        var rCardId = event.getSource().get("v.id");
        var checkradio = event.getSource().get("v.checked");
        component.set("v.checkRadio" ,checkradio);
        console.log("-------------"+checkradio);
        component.set("v.last4Digit" ,rLast4Digit);
        component.set("v.customerId" ,rCustomerId);
        component.set("v.emailId" ,rEmailId);
        component.set("v.phoneNum" ,rPhoneNum);
        
        component.set("v.cardId",rCardId);
        component.set("v.buttonD", false);
    },
    onChangeEmail :  function(component, event, helper) {
        var value = event.getSource().get("v.value");
        component.set("v.emailId",value);
        
    },
    
})