({
	doInit1 : function(component, event, helper) {
        
        var action = component.get("c.getContact");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                
                var result = responsetogetUser.getReturnValue();   
                console.log("result---",result.Email);
                component.set("v.Username",result.Email);
                component.set("v.affiliateId",result.PAP_refid__c);
                console.log(result.PAP_refid__c!=null);
                if(result.PAP_refid__c!=null){
                    component.set("v.isAffiliateExist",true);
                    if(result.PAPUserId__c == null || result.PAPRPassword__c == null){
                        component.set("v.isUserAffiliateIdExist",true);
                        if(result.PAPUserId__c == null){
                            component.set("v.isUserAffiliateIdError",'You can not modify Affiliate Personal Referral Code if PAPuserId is blank');
                        }
                        if(result.PAPRPassword__c == null){
                            component.set("v.isUserAffiliateIdError",'You can not modify Affiliate Personal Referral Code if PAPRPassword is blank');
                        }
                    }
                }
                
                
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    },
    doInit2 : function(component, event, helper) {
        var action = component.get("c.checkRefId");
        action.setParams({"conId":component.get("v.recordId")});
        action.setCallback(this, function(responsetogetUser) {
            var state = responsetogetUser.getState();
            console.log(state);
            component.set("v.Spinner",false);
            if (state === 'SUCCESS') {
                var result = responsetogetUser.getReturnValue(); 
                console.log('result:-'+result);
                if(component.get("v.affiliateId")==null){
                	component.set("v.affiliateId",result);
                }
            }else if(state == "ERROR"){
                
            }
        });
        $A.enqueueAction(action);
    }
})