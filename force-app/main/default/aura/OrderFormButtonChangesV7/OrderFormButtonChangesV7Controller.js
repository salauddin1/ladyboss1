({
    handleShowModal: function(component, evt, helper) {
        var modalBody;
        console.log('lib called');
        $A.createComponent("c:OrderFormChangesV7", { recordId : component.get("v.recordId") },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "ORDER CONSOLE",
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "slds-modal_large fullwidth",
                                       closeCallback: function() {
                                       }
                                   })
                               }
                           });
    },
    createOrderComponent: function(component, evt, helper) {
        console.log('id in button',component.get("v.recordId"))
        $A.createComponent(
            "c:OrderFormChanges",
            {    recordId : component.get("v.recordId")     },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
    }
})